/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.lib;

/**
 * Objects implementing this interface can be copied using a copy context.
 */
public interface ICopyable<A> {

	/**
	 * Creates a copy of the current object. If a copy of current object is
	 * already registered in the context, this element needs to be returned.
	 * Otherwise the object needs to be copied and registered in the context.
	 * 
	 * @param context
	 * @return
	 */
	A copy(MappingContext context);

}
