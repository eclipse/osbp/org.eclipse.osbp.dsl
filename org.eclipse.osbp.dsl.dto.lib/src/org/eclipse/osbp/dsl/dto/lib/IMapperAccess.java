/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.lib;

public interface IMapperAccess {

	/**
	 * Returns the best mapper available to map from the given entity to the
	 * given DTO.
	 * 
	 * @param dto
	 * @param entity
	 * @return
	 */
	<D, E> IMapper<D, E> getToDtoMapper(Class<D> dto, Class<E> entity);

	/**
	 * Returns the best mapper available to map from the given DTO to the given
	 * entity.
	 * 
	 * @param dto
	 * @param entity
	 * @return
	 */
	<D, E> IMapper<D, E> getToEntityMapper(Class<D> dto, Class<E> entity);

}
