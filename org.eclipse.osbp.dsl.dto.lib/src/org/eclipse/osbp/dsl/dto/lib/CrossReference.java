/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.lib;

@SuppressWarnings("serial")
public class CrossReference implements ICrossReferenceInfo {

	private String target;
	private Object id;
	private String number;
	private String description;

	@Override
	public String getTarget() {
		return target;
	}

	@Override
	public Object getId() {
		return id;
	}

	@Override
	public String getNumber() {
		return number;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
