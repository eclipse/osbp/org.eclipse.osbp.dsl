/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.lib.services.impl;

import java.sql.SQLException;
import java.util.Iterator;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.eclipse.osbp.runtime.common.filter.DtoServiceException;

/**
 * Converts SQLExceptions.
 * <p>
 * TODO make it an OSGi service and support XOPEN SQLstate conventions and the
 * SQL:2003 conventions
 */
public class ExceptionConverter {

	/**
	 * Converts SQL Exceptions.
	 * 
	 * @param e
	 * @return
	 */
	public DtoServiceException convertException(Exception e) {
		if (e instanceof ConstraintViolationException) {
			Iterator<ConstraintViolation<?>> iter = ((ConstraintViolationException)e).getConstraintViolations().iterator();
			StringBuilder strBuild = new StringBuilder();
			while (iter.hasNext()) {
				ConstraintViolation<?> item = iter.next();
				strBuild.append(item.getLeafBean().getClass().getName());
				strBuild.append(" - ").append(item.getPropertyPath().toString()).append(" - ").append(item.getMessage()).append("\n");
			} 
			return new DtoServiceException("Constraint violation", strBuild.toString(), e);
		}
		Throwable cause = e.getCause();
		if (cause instanceof RuntimeException) {
			Throwable innercause = cause.getCause();
			if (innercause instanceof SQLException) {
				SQLException sqlEx = (SQLException) innercause;
				String state = sqlEx.getSQLState();
				if (state.equals("23503")) {
					return new DtoServiceException(state,
							"Delete not possible. The record is used.", e);
				}
			}
		}
		return new DtoServiceException("unknown", e.getMessage(), e);
	}
}
