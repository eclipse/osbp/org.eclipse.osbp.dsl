/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.dsl.dto.lib;

import java.beans.PropertyChangeListener;
import java.util.function.Supplier;

import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.DtoUtils;

@SuppressWarnings("serial")
public class OppositeContainmentDtoList<D extends IDto> extends AbstractOppositeDtoList<D> {

	private IDto containerDto;

	public OppositeContainmentDtoList(MappingContext mappingContext, Class<?> dtoType, IDto containerDto,
			String parentProperty, Supplier<Object> idSupplier, PropertyChangeListener containerListener) {
		super(mappingContext, dtoType, parentProperty, idSupplier, containerListener);
		this.containerDto = containerDto;
	}

	protected void notifyContainer(D changedDto) {
		DtoUtils.invokeDirtySetter(containerDto, true);
	}

	@Override
	protected OppositeContainmentDtoList<D> newInstance() {
		return new OppositeContainmentDtoList<D>(mappingContext, dtoType, containerDto, parentProperty, idSupplier,
				containerListener);
	}
}
