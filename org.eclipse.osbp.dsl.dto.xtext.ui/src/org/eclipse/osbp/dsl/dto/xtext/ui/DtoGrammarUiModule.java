

/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.editor.contentassist.AbstractJavaBasedContentProposalProvider;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.ISemanticHighlightingCalculator;
import org.eclipse.xtext.xbase.ui.contentassist.ReplacingAppendable;
import org.eclipse.xtext.xbase.ui.quickfix.JavaTypeQuickfixes;
import org.eclipse.osbp.dsl.dto.xtext.ui.labeling.DtoGrammarLabelProvider;
import org.eclipse.osbp.dsl.dto.xtext.ui.DtoGrammarHighlightingCalculator;
import org.eclipse.osbp.dsl.dto.xtext.ui.DtoHighlightingConfiguration;
import org.eclipse.osbp.xtext.oxtype.imports.IUnresolvedEObjectResolver;
import org.eclipse.osbp.xtext.oxtype.ui.contentassist.OXTypeReplacingAppendable;
import org.eclipse.osbp.xtext.oxtype.ui.contentassist.OXtypeProposalProvider;
import org.eclipse.osbp.xtext.oxtype.ui.imports.InteractiveUnresolvedEClassResolver;
import org.eclipse.osbp.xtext.oxtype.ui.quickfix.CustomJavaTypeQuickfixes;

/**
 * Use this class to register components to be used within the IDE.
 */
public class DtoGrammarUiModule extends
		org.eclipse.osbp.dsl.dto.xtext.ui.AbstractDtoGrammarUiModule {
	public DtoGrammarUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
	
	@Override
	public Class<? extends AbstractJavaBasedContentProposalProvider.ReferenceProposalCreator> bindAbstractJavaBasedContentProposalProvider$ReferenceProposalCreator() {
		return OXtypeProposalProvider.CustomReferenceProposalCreator.class;
	}

	public Class<? extends IHighlightingConfiguration> bindIHighlightingConfiguration() {
		return DtoHighlightingConfiguration.class;
	}

	public Class<? extends ISemanticHighlightingCalculator> bindISemanticHighlightingCalculator() {
		return DtoGrammarHighlightingCalculator.class;
	}

	public Class<? extends org.eclipse.jface.viewers.ILabelProvider> bindILabelProvider() {
		return DtoGrammarLabelProvider.class;
	}

	@SuppressWarnings("restriction")
	public Class<? extends ReplacingAppendable.Factory> bindReplacingAppendable$Factory() {
		return OXTypeReplacingAppendable.Factory.class;
	}
	
	public Class<? extends IUnresolvedEObjectResolver> bindIUnresolvedEObjectResolver() {
		return InteractiveUnresolvedEClassResolver.class;
	}
	
	public Class<? extends JavaTypeQuickfixes> bindJavaTypeQuickfixes() {
		return CustomJavaTypeQuickfixes.class;
	}
	
}
