/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.ui.labeling;

import static com.google.common.collect.Iterables.filter;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoMapper;
import org.eclipse.osbp.dsl.semantic.dto.LDtoOperation;
import org.eclipse.osbp.dsl.semantic.dto.LDtoReference;
import org.eclipse.osbp.xtext.oxtype.ui.labeling.OXtypeLabelProvider;
import org.eclipse.xtext.common.types.JvmParameterizedTypeReference;
import org.eclipse.xtext.common.types.JvmVisibility;

//git@github.com/osbp/osbp-dsl.git
import com.google.inject.Inject;

/**
 * Provides labels for a EObjects.
 */
@SuppressWarnings("restriction")
public class DtoGrammarLabelProvider extends OXtypeLabelProvider {

	@Inject
	private DtoGrammarImages images;

	@Inject
	public DtoGrammarLabelProvider(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}

	public ImageDescriptor image(LDto element) {
		return images.forClass(JvmVisibility.PUBLIC, 0);
	}

	public ImageDescriptor image(LEnum element) {
		return images.forEnum(JvmVisibility.PUBLIC);
	}

	public ImageDescriptor image(LEnumLiteral element) {
		return images.forEnumLiteral();
	}

	public String image(LDtoMapper element) {
		return "converter.gif";
	}

	public String image(LDtoInheritedReference element) {
		return "inheritedfeature.gif";
	}

	public ImageDescriptor image(LDtoReference element) {
		if (!element.isCascading()) {
			return images.forField(JvmVisibility.PUBLIC, 0);
		} else {
			return images.forCascading(JvmVisibility.PUBLIC);
		}
	}

	public String image(LDtoInheritedAttribute element) {
		return "inheritedfeature.gif";
	}

	public ImageDescriptor image(LDtoOperation element) {
		return images.forOperation(JvmVisibility.PUBLIC, 0);
	}

	public ImageDescriptor image(LDtoAttribute element) {
		return images.forField(JvmVisibility.PUBLIC, 0);
	}

	public ImageDescriptor image(LDataType element) {
		return images.forDatatype();
	}

	public ImageDescriptor image(LPackage element) {
		return images.forPackage();
	}

	public ImageDescriptor image(JvmParameterizedTypeReference typeRef) {
		return images.forTypeParameter(0);
	}

	public String text(LClass element) {
		return element.getName();
	}

	public String text(LFeature element) {
		// JvmField inferredField = getFirstOrNull(
		// associations.getJvmElements(element), JvmField.class);
		// if (inferredField != null) {
		// JvmTypeReference type = inferredField.getType();
		// if (type != null) {
		// return element.getName() + " : " + type.getSimpleName();
		// }
		// }
		return element.getName();
	}

	public String text(LDtoMapper element) {
		return "Mapper";
	}

	public String text(LDtoInheritedAttribute element) {
		// JvmField inferredField = getFirstOrNull(
		// associations.getJvmElements(element), JvmField.class);
		// if (inferredField != null) {
		// JvmTypeReference type = inferredField.getType();
		// if (type != null) {
		// return extension.inheritedFeature(element).getName() + " : "
		// + type.getSimpleName();
		// }
		// }
		return element.getInheritedFeature().getName();
	}

	public String text(LDtoInheritedReference element) {
		// JvmField inferredField = getFirstOrNull(
		// associations.getJvmElements(element), JvmField.class);
		// if (inferredField != null) {
		// JvmTypeReference type = inferredField.getType();
		// if (type != null) {
		// return extension.inheritedFeature(element).getName() + " : "
		// + type.getSimpleName();
		// }
		// }
		return element.getInheritedFeature().getName();
	}

	public String text(JvmParameterizedTypeReference typeRef) {
		return typeRef.getType().getSimpleName();
	}

	protected <T> T getFirstOrNull(Iterable<EObject> elements, Class<T> type) {
		Iterator<T> iterator = filter(elements, type).iterator();
		return iterator.hasNext() ? iterator.next() : null;
	}
}
