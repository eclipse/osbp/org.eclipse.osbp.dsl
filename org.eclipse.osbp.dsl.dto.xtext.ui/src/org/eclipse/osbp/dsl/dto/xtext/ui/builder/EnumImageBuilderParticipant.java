/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.ui.builder;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;
import org.eclipse.osbp.dsl.semantic.common.types.LState;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.osbp.dsl.semantic.dto.LDtoModel;
import org.eclipse.osbp.xtext.oxtype.resource.ISemanticLoadingResource;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess;
import org.eclipse.xtext.builder.IXtextBuilderParticipant;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

@SuppressWarnings("deprecation")
public class EnumImageBuilderParticipant implements
		IXtextBuilderParticipant {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(EnumImageBuilderParticipant.class);

	@Inject
	private Provider<EclipseResourceFileSystemAccess> fileAccessProvider;

	public void build(IBuildContext context, IProgressMonitor monitor)
			throws CoreException {
		final IProject builtProject = context.getBuiltProject();
		IJavaProject javaProject = JavaCore.create(builtProject);
		if (!javaProject.exists()) {
			return;
		}

		// create the icon structure
		buildImageStructure(context.getBuiltProject(), context.getResourceSet(), monitor);
	}

	protected IFolder getIconFolder(final IProject builtProject) {
		return builtProject.getFolder("enums");
	}

	protected IFolder getIconSubFolder(IFolder iconsFolder, String subFolder) {
		return iconsFolder.getFolder(subFolder);
	}

	protected IFile getIconFile(IFolder iconsSubFolder, String iconFile) {
		return iconsSubFolder.getFile(iconFile);
	}

	protected IFileSystemAccess getConfiguredFileSystemAccess(
			IFolder srcGenFolder) {
		EclipseResourceFileSystemAccess access = fileAccessProvider.get();
		access.setOutputPath(srcGenFolder.getFullPath().toString());
		return access;
	}

	/**
	 * Builds the icons structure.
	 * 
	 * @param builtProject
	 * @param resourceSet
	 * @return
	 */
	private void buildImageStructure(IProject builtProject,
			ResourceSet resourceSet, IProgressMonitor monitor) {

		List<URI> uris = collectFiles(builtProject);
		for (URI uri : uris) {
			ISemanticLoadingResource resource = (ISemanticLoadingResource) resourceSet.getResource(uri, true);
			LDtoModel m = (LDtoModel) resource.getSemanticElement();
			if (m == null) {
				continue;
			}
			IFolder enumsFolder = getIconFolder(builtProject);
			if(!enumsFolder.exists()) {
				try {
					enumsFolder.create(true, true, monitor);
				} catch (CoreException e) {
					LOGGER.error("{}", e);
				}
			}			
			for (LTypedPackage pkg : m.getPackages()) {
				for (LType type : pkg.getTypes()) {
					if (type instanceof LEnum) {
						LEnum enumType = ((LEnum) type);
						IFolder subFolder = getIconSubFolder(enumsFolder, enumType.getName());
						if(!subFolder.exists()) {
							try {
								subFolder.create(true, true, monitor);
							} catch (CoreException e) {
								LOGGER.error("{}", e);
							}
						}			
						for(LEnumLiteral literal:enumType.getLiterals()) {
							IFile file = getIconFile(subFolder, literal.getName().toLowerCase()+".png");
							if(!file.exists()) {
								ByteArrayOutputStream os = new ByteArrayOutputStream();
								try {
									ImageIO.write(getEmptyBitmap(), "png", os);
									InputStream is = new ByteArrayInputStream(os.toByteArray());
									file.create(is, 0, monitor);
								} catch (IOException | CoreException e) {
									LOGGER.error("{}", e);
								}
							}
						}
					}
					else if (type instanceof LStateClass) {
						LStateClass stateClassType = ((LStateClass) type);
						IFolder subFolder = getIconSubFolder(enumsFolder, stateClassType.getName());
						if(!subFolder.exists()) {
							try {
								subFolder.create(true, true, monitor);
							} catch (CoreException e) {
								LOGGER.error("{}", e);
							}
						}			
						for(LState state:stateClassType.getStates()) {
							IFile file = getIconFile(subFolder, state.getName().toLowerCase()+".png");
							if(!file.exists()) {
								ByteArrayOutputStream os = new ByteArrayOutputStream();
								try {
									ImageIO.write(getEmptyBitmap(), "png", os);
									InputStream is = new ByteArrayInputStream(os.toByteArray());
									file.create(is, 0, monitor);
								} catch (IOException | CoreException e) {
									LOGGER.error("{}", e);
								}
							}
						}
					}
				}
			}
		}
	}
	
	private BufferedImage getEmptyBitmap() {
		BufferedImage bitmap = new BufferedImage(16, 16, BufferedImage.TRANSLUCENT);
		Graphics2D gfx = bitmap.createGraphics();
		gfx.setColor(new Color(0f,0f,0f,0f ));
		gfx.fillRect(0, 0, 1, 1);
		gfx.dispose();
		return bitmap;
	}

	/**
	 * Collects all "entity" URIs contained in the given project.
	 * 
	 * @param project
	 * @return
	 */
	protected List<URI> collectFiles(IProject project) {
		List<URI> uris = new ArrayList<>();
		try {
			project.accept(new IResourceVisitor() {
				public boolean visit(IResource curr) throws CoreException {
					if (curr instanceof IContainer) {
						String folder = curr.getProjectRelativePath()
								.toString();
						if (folder.equals("bin")
								|| folder.equals("target/classes")) {
							return false;
						}
						return true;
					} else if (curr instanceof IFile) {
						if ("dto".equals(curr.getProjectRelativePath()
								.getFileExtension())) {
							uris.add(URI.createPlatformResourceURI(curr
									.getFullPath().toString(), true));
							return false;
						}
					}
					return true;
				}
			}, IResource.DEPTH_INFINITE, false);
		} catch (CoreException e) {
			LOGGER.error("{}", e);
		}
		return uris;
	}

}
