/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.builder.metadata.services;

import java.net.URL;
import java.util.List;

import org.osgi.framework.Bundle;

public abstract class AbstractBuilderParticipant implements IBuilderParticipant {

	@Override
	public List<URL> getModels(Bundle suspect) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void notifyLifecyle(LifecycleEvent event) {
		throw new UnsupportedOperationException();
	}

}
