/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.builder.metadata.services;

import java.net.URL;
import java.util.List;

import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpaceTypeProvider;
import org.osgi.framework.Bundle;

// TODO: Auto-generated Javadoc
/**
 * Builder participants can participate to the model builder.<br>
 * Attention: The runtime builder needs to use the
 * {@link BundleSpaceTypeProvider}. Attached {@link IBuilderParticipant} for
 * implementation details.
 * 
 * <b>Attention:</b> This interface should not be implemented by clients. Use
 * {@link AbstractBuilderParticipant} instead.
 */
@SuppressWarnings("restriction")
public interface IBuilderParticipant {

	/**
	 * Returns a list with all models that should be loaded into the builder. To
	 * become part of model providers, the {@link IMetadataBuilderService} Guice
	 * Inject may be used.
	 * 
	 * @param suspect
	 *            The bundle which might contain model files.
	 * 
	 * @return urls - never <code>null</code>
	 */
	List<URL> getModels(Bundle suspect);

	/**
	 * Registers the participant about the lifecyle.<br>
	 * The participant needs to do different issues. On ACTIVATE OSGi-services
	 * should be registered. On DEACTIVATE they should be unregistered.
	 *
	 * @param event
	 *            the event
	 */
	void notifyLifecyle(LifecycleEvent event);

	/**
	 * The lifecycle event for the participant.
	 */
	class LifecycleEvent {

		/** The Constant INITIALIZE. */
		public static final int INITIALIZE = 10;

		/** The Constant ACTIVATED. */
		public static final int ACTIVATED = 20;

		/** The Constant BUNDLES_SCANNED. */
		public static final int BUNDLES_SCANNED = 30;

		/** The Constant DEACTIVATED. */
		public static final int DEACTIVATED = 40;

		/** The state. */
		private int state;

		/**
		 * Instantiates a new lifecycle event.
		 *
		 * @param state
		 *            the state
		 */
		public LifecycleEvent(int state) {
			super();
			this.state = state;
		}

		/**
		 * Gets the state.
		 *
		 * @return the state
		 */
		public int getState() {
			return state;
		}

		/**
		 * Sets the state.
		 *
		 * @param state
		 *            the new state
		 */
		public void setState(int state) {
			this.state = state;
		}

	}

}
