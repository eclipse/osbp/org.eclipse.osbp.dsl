/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.builder.metadata.services;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.resource.EObjectDescription;

import com.google.common.collect.Iterables;

/**
 * A unit of work acts as a transaction block, which can be passed to the
 * metadata service. The passed resourceSet may be used to load and / or resolve
 * resources.
 * <p>
 * The resources resolved and loaded may become unloaded after the
 * {@link #exec(ResourceSet, Object)} methode was invoked.
 */
public interface IUnitOfWork<P> {
	/**
	 * @param service
	 *            The service.
	 * @param state
	 *            The state, which is synchronized while this method is executed
	 * @throws Exception
	 */
	void exec(IMetadataBuilderService service, P state) throws Exception;

	/**
	 * This unit of work will resolve the passed {@link EObject} before
	 * delegating to {@link #process(EObject)}.
	 */
	public abstract class ResolvingUnitOfWork implements IUnitOfWork<EObject> {

		@Override
		public void exec(IMetadataBuilderService service, EObject state)
				throws Exception {
			if (state.eIsProxy()) {
				process(EcoreUtil.resolve(state, service.getResourceSet()));
			} else {
				process(state);
			}
		}

		/**
		 * Ensures, that the passed {@link EObject} was resolved by this method
		 * is invoked.
		 * 
		 * @param resolved
		 */
		protected abstract void process(EObject resolved);
	}

	/**
	 * This unit of work will extract the {@link EObject} from the passed
	 * {@link EObjectDescription} and ensures, that the {@link EObject} was
	 * resolved before delegating to {@link #process(EObject)}.
	 */
	public abstract class EObjectDescriptionUnitOfWork implements
			IUnitOfWork<EObjectDescription> {

		@Override
		public void exec(IMetadataBuilderService service,
				EObjectDescription description) throws Exception {

			EObject state = description.getEObjectOrProxy();
			if (state.eIsProxy()) {
				process(EcoreUtil.resolve(state, service.getResourceSet()));
			} else {
				process(state);
			}
		}

		/**
		 * Ensures, that the passed {@link EObject} was resolved by this method
		 * is invoked.
		 * 
		 * @param resolved
		 */
		protected abstract void process(EObject resolved);
	}

	/**
	 * This unit of work calls
	 * {@link IMetadataBuilderService#getAll(org.eclipse.emf.ecore.EClass)} in a
	 * controlled environment. And passes its result to the
	 * {@link #process(EObject)} method.
	 */
	public abstract class GetAllUnitOfWork implements IUnitOfWork<EClass> {

		@Override
		public void exec(IMetadataBuilderService service, EClass eClass)
				throws Exception {

			// TODO goingEclipse - if the #resolve() call in getAll() is
			// removed, then it needs to become added here.
			process(service.getAll(eClass));
		}

		/**
		 * Ensures, that the passed {@link objects} were resolved before this
		 * method is invoked.
		 * 
		 * @param resolved
		 */
		protected abstract void process(Iterable<EObject> objects);
	}
}
