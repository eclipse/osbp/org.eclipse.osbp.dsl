/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.builder.metadata.services;

import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpace;
import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpaceTypeProvider;
import org.eclipse.osbp.runtime.common.types.IBundleSpace;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.osgi.framework.Bundle;

/**
 * A service that handles the runtime build of Xtext models or Ecore models. It
 * uses delegates to extend the types of models that may become handled.
 * {@link IBuilderParticipant} have to become registered as an OSGi service.
 * Guice injection is enabled. After a participant was added to the service,
 * injections will be done.
 * 
 * <p>
 * <b>Attention:</b> This interface should not be implemented by clients!
 */
@SuppressWarnings("restriction")
public interface IMetadataBuilderService {

	/**
	 * Bundles that add this header to their MANIFEST are added to the
	 * BundleSpace of the builder. So class loading issues are forwareded to
	 * this bundle.
	 */
	static final String LUN_RUNTIME_BUILDER_BUNDLE_SPACE = "OSBP-RuntimeBuilder-BundleSpace";

	/**
	 * Returns the resolved model or <code>null</code> if the model could not be
	 * found.<br>
	 * <b>Attention:</b> Avoid using this method directly. Better use it in an
	 * active transaction using {@link IUnitOfWork} and
	 * {@link #execute(Object, IUnitOfWork)}. For thread safety and
	 * "Xtext in the Web" calls to this method may require an active transaction
	 * using {@link IUnitOfWork} in future versions.
	 *
	 * @param qualifiedName
	 *            the qualified name
	 * @param type
	 *            the type
	 * @return the metadata
	 */
	EObject getMetadata(String qualifiedName, EClass type);

	/**
	 * Returns all {@link IEObjectDescription}s found for the given type.<br>
	 * <b>Attention:</b> Avoid using this method directly. Better use it in an
	 * active transaction using {@link IUnitOfWork} and
	 * {@link #execute(Object, IUnitOfWork)}. For thread safety and
	 * "Xtext in the Web" calls to this method may require an active transaction
	 * using {@link IUnitOfWork} in future versions.
	 *
	 * @param type
	 *            the type
	 * @return the all descriptions
	 */
	Iterable<IEObjectDescription> getAllDescriptions(EClass type);

	/**
	 * Adds the given bundle to the {@link BundleSpace}, so it may be used to
	 * resolved classes. Note, that also
	 * {@link #LUN_RUNTIME_BUILDER_BUNDLE_SPACE} MANIFEST header may be used, to
	 * add a bundle to the {@link BundleSpace}. This method allows
	 * {@link IBuilderParticipant} to add additional bundles that do not provide
	 * the header.
	 *
	 * @param bundle
	 *            the bundle
	 */
	void addToBundleSpace(Bundle bundle);

	/**
	 * Removes the given bundle from the {@link BundleSpace}.
	 *
	 * @param bundle
	 *            the bundle
	 */
	void removeFromBundleSpace(Bundle bundle);

	/**
	 * Returns the underlying resource set. Be carefully if using directly.
	 * Maybe you should work with {@link IUnitOfWork} to have a controlled
	 * transaction.<br>
	 * <b>Attention:</b> Avoid using this method directly. Better use it in an
	 * active transaction using {@link IUnitOfWork} and
	 * {@link #execute(Object, IUnitOfWork)}. For thread safety and
	 * "Xtext in the Web" calls to this method may require an active transaction
	 * using {@link IUnitOfWork} in future versions.
	 * 
	 * @return
	 */
	XtextResourceSet getResourceSet();

	/**
	 * Returns the bundle space.
	 *
	 * @return the bundle space
	 */
	IBundleSpace getBundleSpace();

	/**
	 * Returns all resolved EObjects provided by
	 * {@link #getAllDescriptions(EClass)}.<br>
	 * <b>Attention:</b> Avoid using this method directly. Better use it in an
	 * active transaction using {@link IUnitOfWork} and
	 * {@link #execute(Object, IUnitOfWork)}. For thread safety and
	 * "Xtext in the Web" calls to this method may require an active transaction
	 * using {@link IUnitOfWork} in future versions.
	 *
	 * @param eClass
	 *            the e class
	 * @return the all EObjects
	 */
	Iterable<EObject> getAll(EClass eClass);

	/**
	 * Resolves the given proxy using the internal resource set.<br>
	 * <b>Attention:</b> Avoid using this method directly. Better use it in an
	 * active transaction using {@link IUnitOfWork} and
	 * {@link #execute(Object, IUnitOfWork)}. For thread safety and
	 * "Xtext in the Web" calls to this method may require an active transaction
	 * using {@link IUnitOfWork} in future versions.
	 * 
	 * @param proxy
	 *            the proxy instance
	 * @return resolved EObject
	 * 
	 *         TODO goingEclipse - should be wrapped by a transaction to unload
	 *         the resource savely
	 */
	EObject resolve(EObject proxy);

	/**
	 * Executes the given unit of work in a transaction. Changes applied to any
	 * of the resources contained in the resource set, may become undone after
	 * the execution.
	 * 
	 * @param the
	 *            state that should become processed
	 * @param unitOfWork
	 */
	<T> void execute(T state, IUnitOfWork<T> unitOfWork);

	/**
	 * Returns a set with all sub types of the given type. The JvmTypes need to
	 * be contained in the index. Otherwise no subtypes can be found.
	 * 
	 * @param type
	 * @return
	 */
	Set<JvmDeclaredType> findSubTypes(JvmDeclaredType type);

	/**
	 * Gets the bundle space type provider.
	 *
	 * @return the type provider
	 */
	BundleSpaceTypeProvider getTypeProvider();
	
	/**
	 * Unload all resources with the given extension.
	 *
	 * @param extension the extension
	 */
	void unloadResource(String extension);
}
