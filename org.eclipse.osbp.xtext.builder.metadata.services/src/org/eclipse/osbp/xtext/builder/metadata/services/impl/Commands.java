/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.builder.metadata.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.service.command.Descriptor;
import org.apache.felix.service.command.Parameter;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.osbp.runtime.common.shell.ICommandConstants;
import org.eclipse.osbp.xtext.builder.metadata.services.IMetadataBuilderService;
import org.eclipse.osbp.xtext.builder.metadata.services.IUnitOfWork;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

@Component(service = Object.class, property = {
		ICommandConstants.COMMAND_SCOPE + "=osbp",
		ICommandConstants.COMMAND_FUNCTION + "=modelindex" })
public class Commands {

	IMetadataBuilderService service;

	@Descriptor("Lists all resources contained in the model index.")
	public List<String> modelindex(
			@Parameter(absentValue = "false", presentValue = "true", names = {
					"-ul", "--unload" }) boolean unload) {
		return modelindex(unload, new String[0]);
	}

	@Descriptor("Lists all resources contained in the model index. The result String will be filtered by the filters using contains.")
	public List<String> modelindex(
			@Descriptor("unloads the filtered resources") @Parameter(absentValue = "false", presentValue = "true", names = {
					"-ul", "--unload" }) boolean unload,
			@Descriptor("filter") String... filters) {

		// selected filtered resources
		List<Resource> filtered = new ArrayList<>();
		List<Resource> resources = new ArrayList<>(service.getResourceSet()
				.getResources());
		for (Resource resource : resources) {
			String line = toResourceStatusLine(resource);
			if (filters == null || filters.length == 0
					|| contains(line, filters)) {
				filtered.add(resource);
			}
		}

		// unload resources
		if (unload) {
			service.execute(null, new IUnitOfWork<Void>() {
				@Override
				public void exec(IMetadataBuilderService service, Void state)
						throws Exception {
					for (Resource resource : filtered) {
						resource.unload();
					}
				}
			});
		}

		// create the output
		List<String> result = new ArrayList<>();
		for (Resource resource : filtered) {
			result.add(toResourceStatusLine(resource));
		}

		return result;
	}

	private String toResourceStatusLine(Resource resource) {
		String line = String.format("uri=%s, loaded=%s", resource.getURI()
				.toString(), Boolean.toString(resource.isLoaded()));
		return line;
	}

	private boolean contains(String line, String... filters) {
		for (String filter : filters) {
			if (!line.toLowerCase().contains(filter.toLowerCase())) {
				return false;
			}
		}
		return true;
	}

	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	void bindIMetadataBuilderService(IMetadataBuilderService service) {
		this.service = service;
	}

	void unbindIMetadataBuilderService(IMetadataBuilderService service) {
		this.service = service;
	}

}