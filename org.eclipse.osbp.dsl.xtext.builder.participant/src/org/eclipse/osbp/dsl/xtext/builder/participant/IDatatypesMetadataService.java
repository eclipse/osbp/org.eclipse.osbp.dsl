/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.xtext.builder.participant;

import org.eclipse.osbp.dsl.semantic.common.types.LDataType;

// TODO: Auto-generated Javadoc
/**
 * @noimplement Should not be implemented by clients. 
 */
public interface IDatatypesMetadataService {

	/**
	 * Returns the datatypes model for the given datatype name.
	 *
	 * @param datatypeName
	 *            the datatype name
	 * @return the metadata
	 */
	LDataType getMetadata(String datatypeName);

}
