/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.xtext.builder.participant;

import java.util.List;

import org.eclipse.osbp.dsl.semantic.dto.LDto;

// TODO: Auto-generated Javadoc
/**
 * Should not implemented by clients.
 */
public interface IDtoMetadataService {

	/**
	 * Returns the dto model for the given class.
	 *
	 * @param dtoClass
	 *            the dto class
	 * @return the metadata
	 */
	LDto getMetadata(Class<?> dtoClass);

	/**
	 * Returns the dto model for the given class name.
	 *
	 * @param className
	 *            the class name
	 * @return the metadata
	 */
	LDto getMetadata(String className);

	/**
	 * Returns all dtos.
	 *
	 * @return the all
	 */
	List<LDto> getAll();

}
