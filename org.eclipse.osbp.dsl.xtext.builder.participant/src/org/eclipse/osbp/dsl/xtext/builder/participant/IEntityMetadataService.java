/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.xtext.builder.participant;

import java.util.List;

import org.eclipse.osbp.dsl.semantic.entity.LEntity;

// TODO: Auto-generated Javadoc
/**
 * Should not implemented by clients.
 */
public interface IEntityMetadataService {

	/**
	 * Returns the entity model for the given class.
	 *
	 * @param entityClass
	 *            the entity class
	 * @return the metadata
	 */
	LEntity getMetadata(Class<?> entityClass);

	/**
	 * Returns the entity model for the given class name.
	 *
	 * @param className
	 *            the class name
	 * @return the metadata
	 */
	LEntity getMetadata(String className);

	/**
	 * Returns all entities.
	 *
	 * @return the all
	 */
	List<LEntity> getAll();

}
