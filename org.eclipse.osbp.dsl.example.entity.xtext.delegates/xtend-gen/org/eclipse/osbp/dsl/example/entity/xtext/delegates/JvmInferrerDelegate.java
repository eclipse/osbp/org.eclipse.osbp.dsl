/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.dsl.example.entity.xtext.delegates;

import com.google.inject.Inject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.entity.xtext.extensions.EntityTypesBuilder;
import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("restriction")
public class JvmInferrerDelegate /* implements IInferrerDelegate  */{
  @Inject
  private ModelExtensions modelExt;
  
  @Inject
  @Extension
  private EntityTypesBuilder typesBuilder;
  
  @Inject
  @Extension
  private TypeReferences references;
  
  @Inject
  private /* IndexDerivedStateHelper */Object stateHelper;
  
  @Override
  public void inferTypesOnly(final EObject semanticElement, final IJvmDeclaredTypeAcceptor acceptor, final boolean preIndexingPhase) {
    throw new Error("Unresolved compilation problems:"
      + "\nThe field JvmInferrerDelegate.stateHelper refers to the missing type IndexDerivedStateHelper"
      + "\nmarkAsToBeDerivedLater cannot be resolved");
  }
  
  @Override
  public void inferFullState(final JvmDeclaredType jvmType, final EObject semanticElement, final IJvmDeclaredTypeAcceptor acceptor, final boolean preIndexingPhase, final String selector) {
    throw new Error("Unresolved compilation problems:"
      + "\nThe field JvmInferrerDelegate.stateHelper refers to the missing type IndexDerivedStateHelper"
      + "\nmarkAsDerived cannot be resolved");
  }
}
