/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.dsl.example.entity.xtext.delegates;

import java.util.Set;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.OutputConfiguration;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

@SuppressWarnings("restriction")
public class SampleGeneratorDelegate /* implements IGeneratorDelegate  */{
  @Override
  public void generate(final Resource input, final IFileSystemAccess fsa) {
    fsa.deleteFile("ResourceInfo");
    fsa.generateFile("ResourceInfo", "OSBPSample", this.toContent(input));
  }
  
  @Override
  public Set<OutputConfiguration> getOutputConfigurations() {
    final Set<OutputConfiguration> configs = CollectionLiterals.<OutputConfiguration>newHashSet();
    final OutputConfiguration servicesOutput = new OutputConfiguration("OSBPSample");
    servicesOutput.setDescription("OSBP Sample");
    servicesOutput.setOutputDirectory("./OSBPSample");
    servicesOutput.setOverrideExistingResources(true);
    servicesOutput.setCreateOutputDirectory(true);
    servicesOutput.setCleanUpDerivedResources(true);
    servicesOutput.setSetDerivedProperty(true);
    servicesOutput.setKeepLocalHistory(Boolean.valueOf(true));
    configs.add(servicesOutput);
    return configs;
  }
  
  public CharSequence toContent(final Resource input) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("The resources ");
    URI _uRI = input.getURI();
    _builder.append(_uRI);
    _builder.append(" contains ");
    int _size = input.getContents().size();
    _builder.append(_size);
    _builder.append(" entries..");
    _builder.newLineIfNotEmpty();
    return _builder;
  }
}
