/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.dsl.example.entity.xtext.delegates

import java.util.Set
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.OutputConfiguration
import org.eclipse.osbp.dsl.xtext.lazyresolver.api.hook.IGeneratorDelegate

@SuppressWarnings("restriction")
class SampleGeneratorDelegate implements IGeneratorDelegate {

	override generate(Resource input, IFileSystemAccess fsa) {
		fsa.deleteFile("ResourceInfo")
		fsa.generateFile("ResourceInfo", "OSBPSample", input.toContent)
	}

	override getOutputConfigurations() {
		val Set<OutputConfiguration> configs = newHashSet();

		val OutputConfiguration servicesOutput = new OutputConfiguration("OSBPSample")
		servicesOutput.setDescription("OSBP Sample")
		servicesOutput.setOutputDirectory("./OSBPSample")
		servicesOutput.setOverrideExistingResources(true)
		servicesOutput.setCreateOutputDirectory(true)
		servicesOutput.setCleanUpDerivedResources(true)
		servicesOutput.setSetDerivedProperty(true)
		servicesOutput.setKeepLocalHistory(true)
		configs.add(servicesOutput)

		return configs
	}

	def toContent(Resource input) '''
		The resources «input.URI» contains «input.contents.size» entries..
	'''

}
