/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.dsl.example.entity.xtext.delegates

import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.common.types.JvmGenericType
import org.eclipse.xtext.common.types.util.TypeReferences
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor
import org.eclipse.osbp.dsl.entity.xtext.extensions.EntityTypesBuilder
import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions
import org.eclipse.osbp.dsl.semantic.entity.LEntity
import org.eclipse.osbp.dsl.xtext.lazyresolver.IndexDerivedStateHelper
import org.eclipse.osbp.dsl.xtext.lazyresolver.api.hook.IInferrerDelegate

@SuppressWarnings("restriction")
class JvmInferrerDelegate implements IInferrerDelegate {

	@Inject ModelExtensions modelExt
	@Inject extension EntityTypesBuilder typesBuilder
	@Inject extension TypeReferences references

	@Inject
	IndexDerivedStateHelper stateHelper;

	override void inferTypesOnly(EObject semanticElement, IJvmDeclaredTypeAcceptor acceptor, boolean preIndexingPhase) {
		if (semanticElement instanceof LEntity) {
			val LEntity lEntity = semanticElement as LEntity;

			// create a generic type
			val JvmGenericType type = typesBuilder.toClass(lEntity,
				"org.eclipse.osbp.samples.inferred.delegate." + lEntity.getName());
			stateHelper.markAsToBeDerivedLater(type, semanticElement, false, "osbpSample");
			acceptor.accept(type);
		}
	}

	override void inferFullState(JvmDeclaredType jvmType, EObject semanticElement, IJvmDeclaredTypeAcceptor acceptor,
		boolean preIndexingPhase, String selector) {

		if (selector != null && selector.equals("osbpSample")) {
			acceptor.accept(jvmType).initializeLater [
				stateHelper.markAsDerived(jvmType)
				abstract = true
				members +=
					semanticElement.toField("sample", references.getTypeForName(Integer.TYPE, semanticElement, null))
			]
		}
	}
}
