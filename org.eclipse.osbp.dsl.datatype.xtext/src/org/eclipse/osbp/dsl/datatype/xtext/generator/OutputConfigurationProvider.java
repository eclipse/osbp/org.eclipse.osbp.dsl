/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.dsl.datatype.xtext.generator;

import java.util.Set;

import org.eclipse.xtext.generator.OutputConfiguration;

public class OutputConfigurationProvider extends
		org.eclipse.xtext.generator.OutputConfigurationProvider {

	/**
	 * @return a set of {@link OutputConfiguration} available for the generator
	 */
	public Set<OutputConfiguration> getOutputConfigurations() {
		Set<OutputConfiguration> configs = super.getOutputConfigurations();

		OutputConfiguration componentOutput = new OutputConfiguration(
				"ModelBin");
		componentOutput.setDescription("ModelBin");
		componentOutput.setOutputDirectory("./modelsbin");
		componentOutput.setOverrideExistingResources(true);
		componentOutput.setCreateOutputDirectory(true);
		componentOutput.setCleanUpDerivedResources(true);
		componentOutput.setSetDerivedProperty(true);
		componentOutput.setKeepLocalHistory(true);
		configs.add(componentOutput);

		return configs;
	}

}
