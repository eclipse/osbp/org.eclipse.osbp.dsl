/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.datatype.xtext;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

/**
 * Initialization support for running Xtext languages without equinox extension
 * registry
 */
public class DatatypeGrammarStandaloneSetup extends
		DatatypeGrammarStandaloneSetupGenerated {

	public static void doSetup() {
		if (!EPackage.Registry.INSTANCE
				.containsKey("http://osbp.eclipse.org/dsl/common/types/v1")) {
			EPackage.Registry.INSTANCE.put(
					"http://osbp.eclipse.org/dsl/common/types/v1",
					OSBPTypesPackage.eINSTANCE);
		}

		new DatatypeGrammarStandaloneSetup()
				.createInjectorAndDoEMFRegistration();
	}
}
