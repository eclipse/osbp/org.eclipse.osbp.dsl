/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.datatype.xtext.formatter;

import org.eclipse.osbp.dsl.common.xtext.formatting.CommonGrammarFormatter;
import org.eclipse.osbp.dsl.common.xtext.services.CommonGrammarGrammarAccess;
import org.eclipse.osbp.dsl.common.xtext.services.CommonGrammarGrammarAccess.DataTypeConstraintElements;
import org.eclipse.osbp.dsl.common.xtext.services.CommonGrammarGrammarAccess.KeyAndValueElements;
import org.eclipse.osbp.dsl.datatype.xtext.services.DatatypeGrammarGrammarAccess;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.formatting.impl.FormattingConfig;

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation/latest/xtext.html#formatting
 * on how and when to use it
 * 
 * Also see org.eclipse.xtext.xtext.XtextFormattingTokenSerializer as an
 * example
 */
public class DatatypeGrammarFormatter extends CommonGrammarFormatter {

	@Override
	protected void configureFormatting(FormattingConfig c) {
		configure(c, (DatatypeGrammarGrammarAccess) getGrammarAccess());
	}

	public void configure(FormattingConfig c, DatatypeGrammarGrammarAccess ga) {
		DatatypeGrammarGrammarAccess f = (DatatypeGrammarGrammarAccess) getGrammarAccess();

		super.configure(c, ga.getCommonGrammarGrammarAccess());

		configureDatatypeDef(c, f.getDataTypeAccess());
		configureConstraintsDef(c, f.getDataTypeConstraintAccess());
		configureKeyAndValueDef(c, f.getKeyAndValueAccess());

		c.setAutoLinewrap(160);
		
		c.setLinewrap(0, 1, 2).before(f.getSL_COMMENTRule());
		c.setLinewrap(0, 1, 2).after(f.getSL_COMMENTRule());
		c.setLinewrap(0, 1, 2).before(f.getML_COMMENTRule());
		c.setLinewrap(1, 1, 1).after(f.getML_COMMENTRule());

		for (Keyword kw : ga.findKeywords("@")) {
			c.setNoSpace().after(kw);
		}
		for (Keyword kw : ga.findKeywords("(")) {
			c.setNoSpace().after(kw);
		}
		for (Keyword kw : ga.findKeywords("(")) {
			c.setNoSpace().before(kw);
		}
		for (Keyword kw : ga.findKeywords(")")) {
			c.setNoSpace().before(kw);
		}
		for (Keyword kw : ga.findKeywords(";")) {
			c.setNoSpace().before(kw);
		}
		for (Keyword kw : ga.findKeywords(",")) {
			c.setNoSpace().before(kw);
		}
	}

	@SuppressWarnings("restriction")
	private void configureKeyAndValueDef(FormattingConfig c,
			KeyAndValueElements ele) {
//		c.setLinewrap().before(ele.getRule());
//		c.setIndentationIncrement().before(ele.getRule());
//		c.setIndentationDecrement().after(ele.getRule());
	}

	@SuppressWarnings("restriction")
	private void configureConstraintsDef(FormattingConfig c,
			DataTypeConstraintElements ele) {
		c.setLinewrap().before(ele.getRule());
		c.setIndentationIncrement().before(ele.getRule());
		c.setIndentationDecrement().after(ele.getRule());
	}

	@SuppressWarnings("restriction")
	protected void configureDatatypeDef(FormattingConfig c,
			CommonGrammarGrammarAccess.DataTypeElements ele) {
		c.setLinewrap(1, 1, 2).around(ele.getRule());
		c.setLinewrap().before(ele.getPropertiesKeyword_2_0_4_0());
		c.setLinewrap().before(ele.getPropertiesKeyword_2_1_3_0());
		c.setLinewrap().before(ele.getPropertiesKeyword_2_2_2_0());
		
		c.setIndentationIncrement().before(ele.getPropertiesKeyword_2_0_4_0());
		c.setIndentationIncrement().before(ele.getPropertiesKeyword_2_1_3_0());
		c.setIndentationIncrement().before(ele.getPropertiesKeyword_2_2_2_0());
		c.setIndentationDecrement().after(ele.getRightParenthesisKeyword_2_0_4_4());
		c.setIndentationDecrement().after(ele.getRightParenthesisKeyword_2_1_3_4());
		c.setIndentationDecrement().after(ele.getRightParenthesisKeyword_2_2_2_4());
		
		c.setIndentationIncrement().before(ele.getRule());
		
	}
}
