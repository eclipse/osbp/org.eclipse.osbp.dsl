/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.datatype.xtext.jvmmodel;

import com.google.inject.Inject;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.xtext.common.types.JvmEnumerationLiteral;
import org.eclipse.xtext.common.types.JvmEnumerationType;
import org.eclipse.xtext.common.types.JvmMember;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

/**
 * <p>Infers a JVM model from the source model.</p>
 * 
 * <p>The JVM model should contain all elements that would appear in the Java code
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>
 */
@SuppressWarnings("all")
public class DatatypeGrammarJvmModelInferrer extends AbstractModelInferrer {
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  protected void _infer(final LEnum enumX, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    final JvmEnumerationType type = this._jvmTypesBuilder.toEnumerationType(enumX, this._iQualifiedNameProvider.getFullyQualifiedName(enumX).toString(), null);
    final Procedure1<JvmEnumerationType> _function = (JvmEnumerationType it) -> {
      EObject _eContainer = enumX.eContainer();
      this._jvmTypesBuilder.setFileHeader(it, this._jvmTypesBuilder.getDocumentation(((LTypedPackage) _eContainer)));
      this._jvmTypesBuilder.setDocumentation(it, this._jvmTypesBuilder.getDocumentation(enumX));
      EList<LEnumLiteral> _literals = enumX.getLiterals();
      for (final LEnumLiteral f : _literals) {
        {
          this._jvmTypesBuilder.setDocumentation(it, this._jvmTypesBuilder.getDocumentation(f));
          EList<JvmMember> _members = it.getMembers();
          JvmEnumerationLiteral _enumerationLiteral = this._jvmTypesBuilder.toEnumerationLiteral(f, f.getName());
          this._jvmTypesBuilder.<JvmEnumerationLiteral>operator_add(_members, _enumerationLiteral);
        }
      }
    };
    acceptor.<JvmEnumerationType>accept(type, _function);
  }
  
  public void infer(final EObject enumX, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    if (enumX instanceof LEnum) {
      _infer((LEnum)enumX, acceptor, isPreIndexingPhase);
      return;
    } else if (enumX != null) {
      _infer(enumX, acceptor, isPreIndexingPhase);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(enumX, acceptor, isPreIndexingPhase).toString());
    }
  }
}
