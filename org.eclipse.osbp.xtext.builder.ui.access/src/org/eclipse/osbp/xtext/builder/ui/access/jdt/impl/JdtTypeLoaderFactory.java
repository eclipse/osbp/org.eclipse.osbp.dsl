/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.builder.ui.access.jdt.impl;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.xtext.common.types.access.jdt.IJavaProjectProvider;
import org.eclipse.osbp.xtext.builder.ui.access.jdt.IJdtTypeLoader;
import org.eclipse.osbp.xtext.builder.ui.access.jdt.IJdtTypeLoaderFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class JdtTypeLoaderFactory implements IJdtTypeLoaderFactory {

	@Inject
	private IJavaProjectProvider javaProjectProvider;

	public IJdtTypeLoader createJdtTypeLoader(ResourceSet resourceSet) {
		if (resourceSet == null)
			throw new IllegalArgumentException("resourceSet may not be null.");
		IJavaProject javaProject = javaProjectProvider
				.getJavaProject(resourceSet);
		IJdtTypeLoader result = createJdtTypeLoader(javaProject);
		return result;
	}

	public IJdtTypeLoader createJdtTypeLoader(IJavaProject javaProject) {
		return new JdtTypeLoader(javaProject);
	}

	@Override
	public IJdtTypeLoader createTypeLoader(ResourceSet resourceSet) {
		return createJdtTypeLoader(resourceSet);
	}

}
