/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.builder.ui.access;

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.resource.impl.ResourceDescriptionsProvider;

// TODO: Auto-generated Javadoc
/**
 * The Interface IXtextUtilService.
 */
public interface IXtextUtilService {

	/**
	 * Returns the project for the given qualified name that contains the java
	 * class. Returns <code>null</code> if no project is involved.
	 *
	 * @param qualifiedName
	 *            the qualified name
	 * @return the project
	 */
	IProject getProject(String qualifiedName);

	/**
	 * Returns the project for the given description that contains the element.
	 * Returns <code>null</code> if no project is involved.
	 *
	 * @param description
	 *            the description
	 * @return the project
	 */
	IProject getProject(IEObjectDescription description);

	/**
	 * Returns the project for the given description that contains the element.
	 * Returns <code>null</code> if no project is involved.
	 *
	 * @param description
	 *            the description
	 * @return the project
	 */
	IProject getProject(IResourceDescription description);

	/**
	 * Returns the project for the given eClass and qualified name that contains
	 * the xtext model. Returns <code>null</code> if no project is involved.
	 *
	 * @param eClass
	 *            the e class
	 * @param qualifiedName
	 *            the qualified name
	 * @return the project
	 */
	IProject getProject(EClass eClass, String qualifiedName);

	/**
	 * Returns a fully configured XtextResourceSet for the uri defined by eClass
	 * and qualifiedName. It will hook up the java project from workspace and
	 * installs a properly configured IJdtTypeProvider.
	 *
	 * @param eClass
	 *            the e class
	 * @param qualifiedName
	 *            the qualified name
	 * @return the configured xtext resource for
	 */
	XtextResourceSet getConfiguredXtextResourceFor(EClass eClass,
			String qualifiedName);

	/**
	 * Returns a fully configured XtextResourceSet for the project. It will
	 * install a properly configured IJdtTypeProvider.
	 *
	 * @param project
	 *            the project
	 * @return the configured xtext resource for
	 */
	XtextResourceSet getConfiguredXtextResourceFor(IProject project);

	/**
	 * Returns a fully configured XtextResourceSet for the project. It will
	 * install a properly configured IJdtTypeProvider and uses the given scope
	 * for loading resource descriptions.
	 *
	 * @param project
	 *            the project
	 * @param scope
	 *            the scope
	 * @return the configured xtext resource for
	 */
	XtextResourceSet getConfiguredXtextResourceFor(IProject project, Scope scope);

	/**
	 * Loads the class for the given qualifiedName. Therefore the java project
	 * containing the java class is determined and the class is loaded from the
	 * workspace. To load the class an URLClassLoader is used. The parent
	 * classloader is the bundle classloader from
	 * org.eclipse.osbp.xtext.builder.ui.access. Since it defines a
	 * dynamic-import: "*" all classes contained in the running IDE are loaded
	 * properly.
	 *
	 * @param qualifiedName
	 *            the qualified name
	 * @return the class
	 */
	Class<?> loadClass(String qualifiedName);

	/**
	 * See {@link #loadClass(String)}. The main difference is, that this method
	 * uses a newly prepared class loader.
	 *
	 * @param qualifiedName
	 *            the qualified name
	 * @return the class
	 */
	Class<?> reloadClass(String qualifiedName);

	/**
	 * Returns the {@link EObjectDescription}s for the given eClass and
	 * qualifiedName.
	 *
	 * @param eClass
	 *            the e class
	 * @param qualifiedName
	 *            the qualified name
	 * @return the e object descriptions
	 */
	Iterable<IEObjectDescription> getEObjectDescriptions(EClass eClass,
			String qualifiedName);

	/**
	 * Defines the different scopes of Xtext. See
	 * {@link ResourceDescriptionsProvider} for details.
	 */
	enum Scope {
		
		/** The named builder scope. */
		NAMED_BUILDER_SCOPE(ResourceDescriptionsProvider.NAMED_BUILDER_SCOPE), 
 /** The live scope. */
 LIVE_SCOPE(
				ResourceDescriptionsProvider.LIVE_SCOPE), 
 /** The persisted descriptions. */
 PERSISTED_DESCRIPTIONS(
				ResourceDescriptionsProvider.PERSISTED_DESCRIPTIONS);

		/** The xtext scope. */
		private String xtextScope;

		/**
		 * Instantiates a new scope.
		 *
		 * @param xtextScope
		 *            the xtext scope
		 */
		Scope(String xtextScope) {
			this.xtextScope = xtextScope;
		}

		/**
		 * Put load option.
		 *
		 * @param options
		 *            the options
		 */
		public void putLoadOption(Map<Object, Object> options) {
			options.put(xtextScope, Boolean.TRUE);
		}
	}

}
