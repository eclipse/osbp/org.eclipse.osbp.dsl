/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.builder.ui.access.jdt;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoaderFactory;

@SuppressWarnings("restriction")
public interface IJdtTypeLoaderFactory extends ITypeLoaderFactory {

	IJdtTypeLoader createJdtTypeLoader(ResourceSet resourceSet);

	IJdtTypeLoader createJdtTypeLoader(IJavaProject javaProject);

}
