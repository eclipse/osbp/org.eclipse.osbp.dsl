
/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */



package org.eclipse.osbp.xtext.builder.participant.dmodelsample.impl;

import org.eclipse.xtext.example.domainmodel.DomainmodelStandaloneSetup;
import org.eclipse.osbp.xtext.builder.xbase.setups.XbaseBundleSpaceStandaloneSetup;

import com.google.inject.Guice;
import com.google.inject.Injector;

@SuppressWarnings("restriction")
public class DomainmodelBundleSpaceStandaloneSetup extends
		DomainmodelStandaloneSetup {

	public static void doSetup() {
		new DomainmodelBundleSpaceStandaloneSetup()
				.createInjectorAndDoEMFRegistration();
	}

	public Injector createInjector() {
		return Guice.createInjector(new DomainmodelBundleSpaceRuntimeModule());
	}

	public Injector createInjectorAndDoEMFRegistration() {
		XbaseBundleSpaceStandaloneSetup.doSetup();

		Injector injector = createInjector();
		register(injector);
		return injector;
	}
}
