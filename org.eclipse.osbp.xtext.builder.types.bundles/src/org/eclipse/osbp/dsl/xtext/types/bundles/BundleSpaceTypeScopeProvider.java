/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 * 
 * Based on Xtext org.eclipse.xtext.common.types.access.reflect.ReflectionTypeScopeProvider
 * 
 */
package org.eclipse.osbp.dsl.xtext.types.bundles;

import org.eclipse.xtext.common.types.access.IJvmTypeProvider;
import org.eclipse.xtext.common.types.xtext.AbstractConstructorScope;
import org.eclipse.xtext.common.types.xtext.AbstractTypeScopeProvider;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.resource.IEObjectDescription;

import com.google.common.base.Predicate;
import com.google.inject.Inject;

@SuppressWarnings("restriction")
public class BundleSpaceTypeScopeProvider extends AbstractTypeScopeProvider {

	@Inject
	private BundleSpaceTypeProviderFactory typeProviderFactory;

	@Inject
	private IQualifiedNameConverter qualifiedNameConverter;

	@Inject
	public BundleSpaceTypeScopeProvider() {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BundleSpaceTypeScope createTypeScope(IJvmTypeProvider typeProvider,
			Predicate<IEObjectDescription> filter) {
		return new BundleSpaceTypeScope((BundleSpaceTypeProvider) typeProvider,
				qualifiedNameConverter, filter);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AbstractConstructorScope createConstructorScope(
			IJvmTypeProvider typeProvider, Predicate<IEObjectDescription> filter) {
		BundleSpaceTypeScope typeScope = createTypeScope(typeProvider, filter);
		return new BundleSpaceConstructorScope(typeScope);
	}

	/**
	 * Sets the type provider factory.
	 * 
	 * @param typeProviderFactory
	 */
	public void setTypeProviderFactory(
			BundleSpaceTypeProviderFactory typeProviderFactory) {
		this.typeProviderFactory = typeProviderFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BundleSpaceTypeProviderFactory getTypeProviderFactory() {
		return typeProviderFactory;
	}

}
