/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.services.xtext.ui;

import org.eclipse.osbp.xtext.oxtype.imports.IUnresolvedEObjectResolver;
import org.eclipse.osbp.xtext.oxtype.ui.contentassist.OXTypeReplacingAppendable;
import org.eclipse.osbp.xtext.oxtype.ui.imports.InteractiveUnresolvedEClassResolver;
import org.eclipse.osbp.xtext.oxtype.ui.quickfix.CustomJavaTypeQuickfixes;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.xbase.ui.contentassist.ReplacingAppendable;
import org.eclipse.xtext.xbase.ui.quickfix.JavaTypeQuickfixes;

/**
 * Use this class to register components to be used within the IDE.
 */
public class ServicesGrammarUiModule extends
		org.eclipse.osbp.dsl.services.xtext.ui.AbstractServicesGrammarUiModule {
	public ServicesGrammarUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}

    @SuppressWarnings("restriction")
    public Class<? extends ReplacingAppendable.Factory> bindReplacingAppendable$Factory() {
        return OXTypeReplacingAppendable.Factory.class;
    }
    
    public Class<? extends IUnresolvedEObjectResolver> bindIUnresolvedEObjectResolver() {
        return InteractiveUnresolvedEClassResolver.class;
    }
    
    public Class<? extends JavaTypeQuickfixes> bindJavaTypeQuickfixes() {
        return CustomJavaTypeQuickfixes.class;
    }

}
