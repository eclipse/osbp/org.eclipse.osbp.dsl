/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.osbp.dsl.entity.xtext.tests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.spi.PersistenceProvider;
import javax.persistence.spi.PersistenceProviderResolver;
import javax.persistence.spi.PersistenceProviderResolverHolder;

import org.eclipse.persistence.config.PersistenceUnitProperties;

public class AbstractJPATest {

	protected Map<String, Object> properties = new HashMap<String, Object>();

	public void setUp() throws Exception {
		PersistenceProviderResolverHolder
				.setPersistenceProviderResolver(new PersistenceProviderResolver() {
					private List<PersistenceProvider> providers = new ArrayList<PersistenceProvider>();

					@Override
					public List<PersistenceProvider> getPersistenceProviders() {
						org.eclipse.persistence.jpa.PersistenceProvider provider = new org.eclipse.persistence.jpa.PersistenceProvider();
						providers.add(provider);
						return providers;
					}

					@Override
					public void clearCachedProviders() {
						providers.clear();
					}
				});
		properties.put(PersistenceUnitProperties.CLASSLOADER, getClass()
				.getClassLoader());
	}

}
