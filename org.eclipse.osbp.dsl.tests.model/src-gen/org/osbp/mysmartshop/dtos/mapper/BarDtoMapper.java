package org.osbp.mysmartshop.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.BarDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.Bar;

/**
 * This class maps the dto {@link BarDto} to and from the entity {@link Bar}.
 * 
 */
@SuppressWarnings("all")
public class BarDtoMapper<DTO extends BarDto, ENTITY extends Bar> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public Bar createEntity() {
    return new Bar();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public BarDto createDto() {
    return new BarDto();
  }
  
  /**
   * Maps the entity {@link Bar} to the dto {@link BarDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final BarDto dto, final Bar entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setFooxxx(toDto_fooxxx(entity, context));
  }
  
  /**
   * Maps the dto {@link BarDto} to the entity {@link Bar}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final BarDto dto, final Bar entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setFooxxx(toEntity_fooxxx(dto, entity, context));
  }
  
  /**
   * Maps the property fooxxx from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_fooxxx(final Bar in, final MappingContext context) {
    return in.getFooxxx();
  }
  
  /**
   * Maps the property fooxxx from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_fooxxx(final BarDto in, final Bar parentEntity, final MappingContext context) {
    return in.getFooxxx();
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(BarDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(Bar.class, in);
  }
}
