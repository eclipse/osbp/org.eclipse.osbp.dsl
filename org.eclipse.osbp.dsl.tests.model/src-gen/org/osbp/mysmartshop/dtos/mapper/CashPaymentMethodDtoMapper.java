/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.osbp.mysmartshop.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.CashPaymentDto;
import org.osbp.mysmartshop.dtos.CashPaymentMethodDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.CashPayment;
import org.osbp.mysmartshop.entities.CashPaymentMethod;

/**
 * This class maps the dto {@link CashPaymentMethodDto} to and from the entity {@link CashPaymentMethod}.
 * 
 */
@SuppressWarnings("all")
public class CashPaymentMethodDtoMapper<DTO extends CashPaymentMethodDto, ENTITY extends CashPaymentMethod> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public CashPaymentMethod createEntity() {
    return new CashPaymentMethod();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CashPaymentMethodDto createDto() {
    return new CashPaymentMethodDto();
  }
  
  /**
   * Maps the entity {@link CashPaymentMethod} to the dto {@link CashPaymentMethodDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CashPaymentMethodDto dto, final CashPaymentMethod entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setNum(toDto_num(entity, context));
    dto.setName(toDto_name(entity, context));
    dto.setCredit(toDto_credit(entity, context));
    dto.setImageName(toDto_imageName(entity, context));
    dto.setLowerLimit(toDto_lowerLimit(entity, context));
  }
  
  /**
   * Maps the dto {@link CashPaymentMethodDto} to the entity {@link CashPaymentMethod}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CashPaymentMethodDto dto, final CashPaymentMethod entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setNum(toEntity_num(dto, entity, context));
    entity.setName(toEntity_name(dto, entity, context));
    entity.setCredit(toEntity_credit(dto, entity, context));
    entity.setImageName(toEntity_imageName(dto, entity, context));
    entity.setLowerLimit(toEntity_lowerLimit(dto, entity, context));
    toEntity_payments(dto, entity, context);
  }
  
  /**
   * Maps the property num from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_num(final CashPaymentMethod in, final MappingContext context) {
    return in.getNum();
  }
  
  /**
   * Maps the property num from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_num(final CashPaymentMethodDto in, final CashPaymentMethod parentEntity, final MappingContext context) {
    return in.getNum();
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_name(final CashPaymentMethod in, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_name(final CashPaymentMethodDto in, final CashPaymentMethod parentEntity, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property credit from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Boolean toDto_credit(final CashPaymentMethod in, final MappingContext context) {
    return in.getCredit();
  }
  
  /**
   * Maps the property credit from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Boolean toEntity_credit(final CashPaymentMethodDto in, final CashPaymentMethod parentEntity, final MappingContext context) {
    return in.getCredit();
  }
  
  /**
   * Maps the property imageName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_imageName(final CashPaymentMethod in, final MappingContext context) {
    return in.getImageName();
  }
  
  /**
   * Maps the property imageName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_imageName(final CashPaymentMethodDto in, final CashPaymentMethod parentEntity, final MappingContext context) {
    return in.getImageName();
  }
  
  /**
   * Maps the property lowerLimit from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected double toDto_lowerLimit(final CashPaymentMethod in, final MappingContext context) {
    return in.getLowerLimit();
  }
  
  /**
   * Maps the property lowerLimit from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected double toEntity_lowerLimit(final CashPaymentMethodDto in, final CashPaymentMethod parentEntity, final MappingContext context) {
    return in.getLowerLimit();
  }
  
  /**
   * Maps the property payments from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<CashPaymentDto> toDto_payments(final CashPaymentMethod in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property payments from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<CashPayment> toEntity_payments(final CashPaymentMethodDto in, final CashPaymentMethod parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<CashPaymentDto, CashPayment> mapper = getToEntityMapper(CashPaymentDto.class, CashPayment.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashPaymentDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashPaymentDto>) in.internalGetPayments();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToPayments,
    		parentEntity::internalRemoveFromPayments);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashPaymentMethodDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashPaymentMethod.class, in);
  }
}
