package org.osbp.mysmartshop.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.CompanyDto;
import org.osbp.mysmartshop.dtos.CompanyGroupDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.Company;
import org.osbp.mysmartshop.entities.CompanyGroup;

/**
 * This class maps the dto {@link CompanyGroupDto} to and from the entity {@link CompanyGroup}.
 * 
 */
@SuppressWarnings("all")
public class CompanyGroupDtoMapper<DTO extends CompanyGroupDto, ENTITY extends CompanyGroup> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public CompanyGroup createEntity() {
    return new CompanyGroup();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CompanyGroupDto createDto() {
    return new CompanyGroupDto();
  }
  
  /**
   * Maps the entity {@link CompanyGroup} to the dto {@link CompanyGroupDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CompanyGroupDto dto, final CompanyGroup entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setName(toDto_name(entity, context));
    dto.setDescription(toDto_description(entity, context));
    dto.setMain_company(toDto_main_company(entity, context));
  }
  
  /**
   * Maps the dto {@link CompanyGroupDto} to the entity {@link CompanyGroup}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CompanyGroupDto dto, final CompanyGroup entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setName(toEntity_name(dto, entity, context));
    entity.setDescription(toEntity_description(dto, entity, context));
    entity.setMain_company(toEntity_main_company(dto, entity, context));
    toEntity_companies(dto, entity, context);
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_name(final CompanyGroup in, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_name(final CompanyGroupDto in, final CompanyGroup parentEntity, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property description from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_description(final CompanyGroup in, final MappingContext context) {
    return in.getDescription();
  }
  
  /**
   * Maps the property description from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_description(final CompanyGroupDto in, final CompanyGroup parentEntity, final MappingContext context) {
    return in.getDescription();
  }
  
  /**
   * Maps the property main_company from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected CompanyDto toDto_main_company(final CompanyGroup in, final MappingContext context) {
    if(in.getMain_company() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CompanyDto, Company> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CompanyDto, Company>) getToDtoMapper(CompanyDto.class, in.getMain_company().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	CompanyDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getMain_company()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getMain_company(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getMain_company(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property main_company from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected Company toEntity_main_company(final CompanyGroupDto in, final CompanyGroup parentEntity, final MappingContext context) {
    if(in.getMain_company() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CompanyDto, Company> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CompanyDto, Company>) getToEntityMapper(in.getMain_company().getClass(), Company.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	Company entity = null;
    	entity = context.get(mapper.createEntityHash(in.getMain_company()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (Company) context
    			.findEntityByEntityManager(Company.class, in.getMain_company().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getMain_company()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getMain_company(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property companies from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<CompanyDto> toDto_companies(final CompanyGroup in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property companies from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<Company> toEntity_companies(final CompanyGroupDto in, final CompanyGroup parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<CompanyDto, Company> mapper = getToEntityMapper(CompanyDto.class, Company.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CompanyDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CompanyDto>) in.internalGetCompanies();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToCompanies,
    		parentEntity::internalRemoveFromCompanies);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CompanyGroupDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CompanyGroup.class, in);
  }
}
