/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.osbp.mysmartshop.dtos.mapper;

import java.util.Date;
import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.CashPaymentDto;
import org.osbp.mysmartshop.dtos.CashPositionDto;
import org.osbp.mysmartshop.dtos.CashRegisterDto;
import org.osbp.mysmartshop.dtos.CashSlipDto;
import org.osbp.mysmartshop.dtos.McustomerDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.CashPayment;
import org.osbp.mysmartshop.entities.CashPosition;
import org.osbp.mysmartshop.entities.CashRegister;
import org.osbp.mysmartshop.entities.CashSlip;
import org.osbp.mysmartshop.entities.Mcustomer;

/**
 * This class maps the dto {@link CashSlipDto} to and from the entity {@link CashSlip}.
  * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 * 
 */
@SuppressWarnings("all")
public class CashSlipDtoMapper<DTO extends CashSlipDto, ENTITY extends CashSlip> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public CashSlip createEntity() {
    return new CashSlip();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CashSlipDto createDto() {
    return new CashSlipDto();
  }
  
  /**
   * Maps the entity {@link CashSlip} to the dto {@link CashSlipDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CashSlipDto dto, final CashSlip entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setCurrentDay(toDto_currentDay(entity, context));
    dto.setNow(toDto_now(entity, context));
    dto.setCashier(toDto_cashier(entity, context));
    dto.setTotal(toDto_total(entity, context));
    dto.setSerial(toDto_serial(entity, context));
    dto.setPayed(toDto_payed(entity, context));
    dto.setCustomer(toDto_customer(entity, context));
    dto.setRegister(toDto_register(entity, context));
  }
  
  /**
   * Maps the dto {@link CashSlipDto} to the entity {@link CashSlip}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CashSlipDto dto, final CashSlip entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setCurrentDay(toEntity_currentDay(dto, entity, context));
    entity.setNow(toEntity_now(dto, entity, context));
    entity.setCashier(toEntity_cashier(dto, entity, context));
    entity.setTotal(toEntity_total(dto, entity, context));
    entity.setSerial(toEntity_serial(dto, entity, context));
    entity.setPayed(toEntity_payed(dto, entity, context));
    toEntity_positions(dto, entity, context);
    entity.setCustomer(toEntity_customer(dto, entity, context));
    entity.setRegister(toEntity_register(dto, entity, context));
    toEntity_payments(dto, entity, context);
  }
  
  /**
   * Maps the property currentDay from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_currentDay(final CashSlip in, final MappingContext context) {
    return in.getCurrentDay();
  }
  
  /**
   * Maps the property currentDay from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_currentDay(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    return in.getCurrentDay();
  }
  
  /**
   * Maps the property now from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Date toDto_now(final CashSlip in, final MappingContext context) {
    return in.getNow();
  }
  
  /**
   * Maps the property now from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Date toEntity_now(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    return in.getNow();
  }
  
  /**
   * Maps the property cashier from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_cashier(final CashSlip in, final MappingContext context) {
    return in.getCashier();
  }
  
  /**
   * Maps the property cashier from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_cashier(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    return in.getCashier();
  }
  
  /**
   * Maps the property total from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Double toDto_total(final CashSlip in, final MappingContext context) {
    return in.getTotal();
  }
  
  /**
   * Maps the property total from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Double toEntity_total(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    return in.getTotal();
  }
  
  /**
   * Maps the property serial from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected long toDto_serial(final CashSlip in, final MappingContext context) {
    return in.getSerial();
  }
  
  /**
   * Maps the property serial from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected long toEntity_serial(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    return in.getSerial();
  }
  
  /**
   * Maps the property payed from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_payed(final CashSlip in, final MappingContext context) {
    return in.getPayed();
  }
  
  /**
   * Maps the property payed from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_payed(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    return in.getPayed();
  }
  
  /**
   * Maps the property positions from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<CashPositionDto> toDto_positions(final CashSlip in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property positions from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<CashPosition> toEntity_positions(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<CashPositionDto, CashPosition> mapper = getToEntityMapper(CashPositionDto.class, CashPosition.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashPositionDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashPositionDto>) in.internalGetPositions();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToPositions,
    		parentEntity::internalRemoveFromPositions);
    return null;
  }
  
  /**
   * Maps the property customer from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected McustomerDto toDto_customer(final CashSlip in, final MappingContext context) {
    if(in.getCustomer() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<McustomerDto, Mcustomer> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<McustomerDto, Mcustomer>) getToDtoMapper(McustomerDto.class, in.getCustomer().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	McustomerDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getCustomer()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getCustomer(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getCustomer(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property customer from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected Mcustomer toEntity_customer(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    if(in.getCustomer() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<McustomerDto, Mcustomer> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<McustomerDto, Mcustomer>) getToEntityMapper(in.getCustomer().getClass(), Mcustomer.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	Mcustomer entity = null;
    	entity = context.get(mapper.createEntityHash(in.getCustomer()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (Mcustomer) context
    			.findEntityByEntityManager(Mcustomer.class, in.getCustomer().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getCustomer()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getCustomer(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property register from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected CashRegisterDto toDto_register(final CashSlip in, final MappingContext context) {
    if(in.getRegister() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CashRegisterDto, CashRegister> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CashRegisterDto, CashRegister>) getToDtoMapper(CashRegisterDto.class, in.getRegister().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	CashRegisterDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getRegister()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getRegister(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getRegister(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property register from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected CashRegister toEntity_register(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    if(in.getRegister() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CashRegisterDto, CashRegister> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CashRegisterDto, CashRegister>) getToEntityMapper(in.getRegister().getClass(), CashRegister.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	CashRegister entity = null;
    	entity = context.get(mapper.createEntityHash(in.getRegister()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (CashRegister) context
    			.findEntityByEntityManager(CashRegister.class, in.getRegister().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getRegister()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getRegister(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property payments from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<CashPaymentDto> toDto_payments(final CashSlip in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property payments from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<CashPayment> toEntity_payments(final CashSlipDto in, final CashSlip parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<CashPaymentDto, CashPayment> mapper = getToEntityMapper(CashPaymentDto.class, CashPayment.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashPaymentDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashPaymentDto>) in.internalGetPayments();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToPayments,
    		parentEntity::internalRemoveFromPayments);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashSlipDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashSlip.class, in);
  }
}
