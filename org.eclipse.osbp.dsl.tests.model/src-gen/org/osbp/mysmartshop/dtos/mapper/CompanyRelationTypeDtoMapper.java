/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.osbp.mysmartshop.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.CompanyRelationTypeDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.CompanyRelationType;

/**
 * This class maps the dto {@link CompanyRelationTypeDto} to and from the entity {@link CompanyRelationType}.
 * 
 */
@SuppressWarnings("all")
public class CompanyRelationTypeDtoMapper<DTO extends CompanyRelationTypeDto, ENTITY extends CompanyRelationType> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public CompanyRelationType createEntity() {
    return new CompanyRelationType();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CompanyRelationTypeDto createDto() {
    return new CompanyRelationTypeDto();
  }
  
  /**
   * Maps the entity {@link CompanyRelationType} to the dto {@link CompanyRelationTypeDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CompanyRelationTypeDto dto, final CompanyRelationType entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setName(toDto_name(entity, context));
    dto.setDescription(toDto_description(entity, context));
  }
  
  /**
   * Maps the dto {@link CompanyRelationTypeDto} to the entity {@link CompanyRelationType}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CompanyRelationTypeDto dto, final CompanyRelationType entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setName(toEntity_name(dto, entity, context));
    entity.setDescription(toEntity_description(dto, entity, context));
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_name(final CompanyRelationType in, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_name(final CompanyRelationTypeDto in, final CompanyRelationType parentEntity, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property description from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_description(final CompanyRelationType in, final MappingContext context) {
    return in.getDescription();
  }
  
  /**
   * Maps the property description from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_description(final CompanyRelationTypeDto in, final CompanyRelationType parentEntity, final MappingContext context) {
    return in.getDescription();
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CompanyRelationTypeDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CompanyRelationType.class, in);
  }
}
