package org.osbp.mysmartshop.dtos.mapper;

import java.util.Date;
import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.CashPositionDto;
import org.osbp.mysmartshop.dtos.CashSlipDto;
import org.osbp.mysmartshop.dtos.CashSubPositionDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.CashPosition;
import org.osbp.mysmartshop.entities.CashSlip;
import org.osbp.mysmartshop.entities.CashSubPosition;

/**
 * This class maps the dto {@link CashPositionDto} to and from the entity {@link CashPosition}.
 * 
 */
@SuppressWarnings("all")
public class CashPositionDtoMapper<DTO extends CashPositionDto, ENTITY extends CashPosition> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public CashPosition createEntity() {
    return new CashPosition();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CashPositionDto createDto() {
    return new CashPositionDto();
  }
  
  /**
   * Maps the entity {@link CashPosition} to the dto {@link CashPositionDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CashPositionDto dto, final CashPosition entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setSlip(toDto_slip(entity, context));
    dto.setNow(toDto_now(entity, context));
    dto.setQuantity(toDto_quantity(entity, context));
    dto.setPrice(toDto_price(entity, context));
    dto.setAmount(toDto_amount(entity, context));
  }
  
  /**
   * Maps the dto {@link CashPositionDto} to the entity {@link CashPosition}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CashPositionDto dto, final CashPosition entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setSlip(toEntity_slip(dto, entity, context));
    entity.setNow(toEntity_now(dto, entity, context));
    entity.setQuantity(toEntity_quantity(dto, entity, context));
    entity.setPrice(toEntity_price(dto, entity, context));
    entity.setAmount(toEntity_amount(dto, entity, context));
    toEntity_subPositions(dto, entity, context);
  }
  
  /**
   * Maps the property slip from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected CashSlipDto toDto_slip(final CashPosition in, final MappingContext context) {
    if(in.getSlip() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CashSlipDto, CashSlip> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CashSlipDto, CashSlip>) getToDtoMapper(CashSlipDto.class, in.getSlip().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	CashSlipDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getSlip()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getSlip(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getSlip(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property slip from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected CashSlip toEntity_slip(final CashPositionDto in, final CashPosition parentEntity, final MappingContext context) {
    if(in.getSlip() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CashSlipDto, CashSlip> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CashSlipDto, CashSlip>) getToEntityMapper(in.getSlip().getClass(), CashSlip.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	CashSlip entity = null;
    	entity = context.get(mapper.createEntityHash(in.getSlip()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (CashSlip) context
    			.findEntityByEntityManager(CashSlip.class, in.getSlip().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getSlip()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getSlip(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property now from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Date toDto_now(final CashPosition in, final MappingContext context) {
    return in.getNow();
  }
  
  /**
   * Maps the property now from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Date toEntity_now(final CashPositionDto in, final CashPosition parentEntity, final MappingContext context) {
    return in.getNow();
  }
  
  /**
   * Maps the property quantity from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected double toDto_quantity(final CashPosition in, final MappingContext context) {
    return in.getQuantity();
  }
  
  /**
   * Maps the property quantity from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected double toEntity_quantity(final CashPositionDto in, final CashPosition parentEntity, final MappingContext context) {
    return in.getQuantity();
  }
  
  /**
   * Maps the property price from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Double toDto_price(final CashPosition in, final MappingContext context) {
    return in.getPrice();
  }
  
  /**
   * Maps the property price from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Double toEntity_price(final CashPositionDto in, final CashPosition parentEntity, final MappingContext context) {
    return in.getPrice();
  }
  
  /**
   * Maps the property amount from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Double toDto_amount(final CashPosition in, final MappingContext context) {
    return in.getAmount();
  }
  
  /**
   * Maps the property amount from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Double toEntity_amount(final CashPositionDto in, final CashPosition parentEntity, final MappingContext context) {
    return in.getAmount();
  }
  
  /**
   * Maps the property subPositions from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<CashSubPositionDto> toDto_subPositions(final CashPosition in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property subPositions from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<CashSubPosition> toEntity_subPositions(final CashPositionDto in, final CashPosition parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<CashSubPositionDto, CashSubPosition> mapper = getToEntityMapper(CashSubPositionDto.class, CashSubPosition.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashSubPositionDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashSubPositionDto>) in.internalGetSubPositions();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToSubPositions,
    		parentEntity::internalRemoveFromSubPositions);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashPositionDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashPosition.class, in);
  }
}
