/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.osbp.mysmartshop.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.DtoTestChildContainmentDto;
import org.osbp.mysmartshop.dtos.DtoTestParentDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.DtoTestChildContainment;
import org.osbp.mysmartshop.entities.DtoTestParent;

/**
 * This class maps the dto {@link DtoTestChildContainmentDto} to and from the entity {@link DtoTestChildContainment}.
 * 
 */
@SuppressWarnings("all")
public class DtoTestChildContainmentDtoMapper<DTO extends DtoTestChildContainmentDto, ENTITY extends DtoTestChildContainment> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public DtoTestChildContainment createEntity() {
    return new DtoTestChildContainment();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public DtoTestChildContainmentDto createDto() {
    return new DtoTestChildContainmentDto();
  }
  
  /**
   * Maps the entity {@link DtoTestChildContainment} to the dto {@link DtoTestChildContainmentDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final DtoTestChildContainmentDto dto, final DtoTestChildContainment entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setContainer(toDto_container(entity, context));
  }
  
  /**
   * Maps the dto {@link DtoTestChildContainmentDto} to the entity {@link DtoTestChildContainment}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final DtoTestChildContainmentDto dto, final DtoTestChildContainment entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setContainer(toEntity_container(dto, entity, context));
  }
  
  /**
   * Maps the property container from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected DtoTestParentDto toDto_container(final DtoTestChildContainment in, final MappingContext context) {
    if(in.getContainer() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<DtoTestParentDto, DtoTestParent> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<DtoTestParentDto, DtoTestParent>) getToDtoMapper(DtoTestParentDto.class, in.getContainer().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	DtoTestParentDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getContainer()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getContainer(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getContainer(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property container from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected DtoTestParent toEntity_container(final DtoTestChildContainmentDto in, final DtoTestChildContainment parentEntity, final MappingContext context) {
    if(in.getContainer() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<DtoTestParentDto, DtoTestParent> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<DtoTestParentDto, DtoTestParent>) getToEntityMapper(in.getContainer().getClass(), DtoTestParent.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	DtoTestParent entity = null;
    	entity = context.get(mapper.createEntityHash(in.getContainer()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (DtoTestParent) context
    			.findEntityByEntityManager(DtoTestParent.class, in.getContainer().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getContainer()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getContainer(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(DtoTestChildContainmentDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(DtoTestChildContainment.class, in);
  }
}
