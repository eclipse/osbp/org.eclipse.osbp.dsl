package org.osbp.mysmartshop.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.CashDrawerCurrencyDto;
import org.osbp.mysmartshop.dtos.CashDrawerSumDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.CashDrawerCurrency;
import org.osbp.mysmartshop.entities.CashDrawerSum;

/**
 * This class maps the dto {@link CashDrawerCurrencyDto} to and from the entity {@link CashDrawerCurrency}.
 * 
 */
@SuppressWarnings("all")
public class CashDrawerCurrencyDtoMapper<DTO extends CashDrawerCurrencyDto, ENTITY extends CashDrawerCurrency> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public CashDrawerCurrency createEntity() {
    return new CashDrawerCurrency();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CashDrawerCurrencyDto createDto() {
    return new CashDrawerCurrencyDto();
  }
  
  /**
   * Maps the entity {@link CashDrawerCurrency} to the dto {@link CashDrawerCurrencyDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CashDrawerCurrencyDto dto, final CashDrawerCurrency entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
  }
  
  /**
   * Maps the dto {@link CashDrawerCurrencyDto} to the entity {@link CashDrawerCurrency}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CashDrawerCurrencyDto dto, final CashDrawerCurrency entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    toEntity_sums(dto, entity, context);
  }
  
  /**
   * Maps the property sums from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<CashDrawerSumDto> toDto_sums(final CashDrawerCurrency in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property sums from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<CashDrawerSum> toEntity_sums(final CashDrawerCurrencyDto in, final CashDrawerCurrency parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<CashDrawerSumDto, CashDrawerSum> mapper = getToEntityMapper(CashDrawerSumDto.class, CashDrawerSum.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashDrawerSumDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashDrawerSumDto>) in.internalGetSums();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToSums,
    		parentEntity::internalRemoveFromSums);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashDrawerCurrencyDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashDrawerCurrency.class, in);
  }
}
