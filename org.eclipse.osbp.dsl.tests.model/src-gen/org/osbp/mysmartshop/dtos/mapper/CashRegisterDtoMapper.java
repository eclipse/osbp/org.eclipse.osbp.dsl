/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.osbp.mysmartshop.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.CashRegisterDto;
import org.osbp.mysmartshop.dtos.CashSlipDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.CashRegister;
import org.osbp.mysmartshop.entities.CashSlip;

/**
 * This class maps the dto {@link CashRegisterDto} to and from the entity {@link CashRegister}.
 * 
 */
@SuppressWarnings("all")
public class CashRegisterDtoMapper<DTO extends CashRegisterDto, ENTITY extends CashRegister> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public CashRegister createEntity() {
    return new CashRegister();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CashRegisterDto createDto() {
    return new CashRegisterDto();
  }
  
  /**
   * Maps the entity {@link CashRegister} to the dto {@link CashRegisterDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CashRegisterDto dto, final CashRegister entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setNum(toDto_num(entity, context));
    dto.setIp(toDto_ip(entity, context));
    dto.setLocation(toDto_location(entity, context));
    dto.setCurrentDay(toDto_currentDay(entity, context));
  }
  
  /**
   * Maps the dto {@link CashRegisterDto} to the entity {@link CashRegister}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CashRegisterDto dto, final CashRegister entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setNum(toEntity_num(dto, entity, context));
    entity.setIp(toEntity_ip(dto, entity, context));
    entity.setLocation(toEntity_location(dto, entity, context));
    entity.setCurrentDay(toEntity_currentDay(dto, entity, context));
    toEntity_slips(dto, entity, context);
  }
  
  /**
   * Maps the property num from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_num(final CashRegister in, final MappingContext context) {
    return in.getNum();
  }
  
  /**
   * Maps the property num from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_num(final CashRegisterDto in, final CashRegister parentEntity, final MappingContext context) {
    return in.getNum();
  }
  
  /**
   * Maps the property ip from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_ip(final CashRegister in, final MappingContext context) {
    return in.getIp();
  }
  
  /**
   * Maps the property ip from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_ip(final CashRegisterDto in, final CashRegister parentEntity, final MappingContext context) {
    return in.getIp();
  }
  
  /**
   * Maps the property location from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_location(final CashRegister in, final MappingContext context) {
    return in.getLocation();
  }
  
  /**
   * Maps the property location from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_location(final CashRegisterDto in, final CashRegister parentEntity, final MappingContext context) {
    return in.getLocation();
  }
  
  /**
   * Maps the property currentDay from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_currentDay(final CashRegister in, final MappingContext context) {
    return in.getCurrentDay();
  }
  
  /**
   * Maps the property currentDay from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_currentDay(final CashRegisterDto in, final CashRegister parentEntity, final MappingContext context) {
    return in.getCurrentDay();
  }
  
  /**
   * Maps the property slips from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<CashSlipDto> toDto_slips(final CashRegister in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property slips from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<CashSlip> toEntity_slips(final CashRegisterDto in, final CashRegister parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<CashSlipDto, CashSlip> mapper = getToEntityMapper(CashSlipDto.class, CashSlip.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashSlipDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashSlipDto>) in.internalGetSlips();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToSlips,
    		parentEntity::internalRemoveFromSlips);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashRegisterDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashRegister.class, in);
  }
}
