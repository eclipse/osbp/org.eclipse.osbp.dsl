package org.osbp.mysmartshop.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.CashDrawerCurrencyDto;
import org.osbp.mysmartshop.dtos.CashDrawerSumDto;
import org.osbp.mysmartshop.dtos.CashPaymentDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.CashDrawerCurrency;
import org.osbp.mysmartshop.entities.CashDrawerSum;
import org.osbp.mysmartshop.entities.CashPayment;

/**
 * This class maps the dto {@link CashDrawerSumDto} to and from the entity {@link CashDrawerSum}.
 * 
 */
@SuppressWarnings("all")
public class CashDrawerSumDtoMapper<DTO extends CashDrawerSumDto, ENTITY extends CashDrawerSum> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public CashDrawerSum createEntity() {
    return new CashDrawerSum();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CashDrawerSumDto createDto() {
    return new CashDrawerSumDto();
  }
  
  /**
   * Maps the entity {@link CashDrawerSum} to the dto {@link CashDrawerSumDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CashDrawerSumDto dto, final CashDrawerSum entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setDrawer(toDto_drawer(entity, context));
  }
  
  /**
   * Maps the dto {@link CashDrawerSumDto} to the entity {@link CashDrawerSum}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CashDrawerSumDto dto, final CashDrawerSum entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setDrawer(toEntity_drawer(dto, entity, context));
    toEntity_payments(dto, entity, context);
  }
  
  /**
   * Maps the property drawer from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected CashDrawerCurrencyDto toDto_drawer(final CashDrawerSum in, final MappingContext context) {
    if(in.getDrawer() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CashDrawerCurrencyDto, CashDrawerCurrency> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CashDrawerCurrencyDto, CashDrawerCurrency>) getToDtoMapper(CashDrawerCurrencyDto.class, in.getDrawer().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	CashDrawerCurrencyDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getDrawer()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getDrawer(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getDrawer(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property drawer from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected CashDrawerCurrency toEntity_drawer(final CashDrawerSumDto in, final CashDrawerSum parentEntity, final MappingContext context) {
    if(in.getDrawer() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CashDrawerCurrencyDto, CashDrawerCurrency> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CashDrawerCurrencyDto, CashDrawerCurrency>) getToEntityMapper(in.getDrawer().getClass(), CashDrawerCurrency.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	CashDrawerCurrency entity = null;
    	entity = context.get(mapper.createEntityHash(in.getDrawer()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (CashDrawerCurrency) context
    			.findEntityByEntityManager(CashDrawerCurrency.class, in.getDrawer().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getDrawer()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getDrawer(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property payments from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<CashPaymentDto> toDto_payments(final CashDrawerSum in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property payments from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<CashPayment> toEntity_payments(final CashDrawerSumDto in, final CashDrawerSum parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<CashPaymentDto, CashPayment> mapper = getToEntityMapper(CashPaymentDto.class, CashPayment.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashPaymentDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CashPaymentDto>) in.internalGetPayments();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToPayments,
    		parentEntity::internalRemoveFromPayments);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashDrawerSumDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashDrawerSum.class, in);
  }
}
