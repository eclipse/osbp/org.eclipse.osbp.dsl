package org.osbp.mysmartshop.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.osbp.mysmartshop.dtos.CashPositionDto;
import org.osbp.mysmartshop.dtos.CashSubPositionDto;
import org.osbp.mysmartshop.dtos.mapper.BaseUUIDDtoMapper;
import org.osbp.mysmartshop.entities.CashPosition;
import org.osbp.mysmartshop.entities.CashSubPosition;

/**
 * This class maps the dto {@link CashSubPositionDto} to and from the entity {@link CashSubPosition}.
 * 
 */
@SuppressWarnings("all")
public class CashSubPositionDtoMapper<DTO extends CashSubPositionDto, ENTITY extends CashSubPosition> extends BaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public CashSubPosition createEntity() {
    return new CashSubPosition();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CashSubPositionDto createDto() {
    return new CashSubPositionDto();
  }
  
  /**
   * Maps the entity {@link CashSubPosition} to the dto {@link CashSubPositionDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CashSubPositionDto dto, final CashSubPosition entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setQuantity(toDto_quantity(entity, context));
    dto.setPrice(toDto_price(entity, context));
    dto.setParent(toDto_parent(entity, context));
  }
  
  /**
   * Maps the dto {@link CashSubPositionDto} to the entity {@link CashSubPosition}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CashSubPositionDto dto, final CashSubPosition entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setQuantity(toEntity_quantity(dto, entity, context));
    entity.setPrice(toEntity_price(dto, entity, context));
    entity.setParent(toEntity_parent(dto, entity, context));
  }
  
  /**
   * Maps the property quantity from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected double toDto_quantity(final CashSubPosition in, final MappingContext context) {
    return in.getQuantity();
  }
  
  /**
   * Maps the property quantity from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected double toEntity_quantity(final CashSubPositionDto in, final CashSubPosition parentEntity, final MappingContext context) {
    return in.getQuantity();
  }
  
  /**
   * Maps the property price from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Double toDto_price(final CashSubPosition in, final MappingContext context) {
    return in.getPrice();
  }
  
  /**
   * Maps the property price from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Double toEntity_price(final CashSubPositionDto in, final CashSubPosition parentEntity, final MappingContext context) {
    return in.getPrice();
  }
  
  /**
   * Maps the property parent from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected CashPositionDto toDto_parent(final CashSubPosition in, final MappingContext context) {
    if(in.getParent() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CashPositionDto, CashPosition> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CashPositionDto, CashPosition>) getToDtoMapper(CashPositionDto.class, in.getParent().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	CashPositionDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getParent()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getParent(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getParent(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property parent from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected CashPosition toEntity_parent(final CashSubPositionDto in, final CashSubPosition parentEntity, final MappingContext context) {
    if(in.getParent() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CashPositionDto, CashPosition> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CashPositionDto, CashPosition>) getToEntityMapper(in.getParent().getClass(), CashPosition.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	CashPosition entity = null;
    	entity = context.get(mapper.createEntityHash(in.getParent()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (CashPosition) context
    			.findEntityByEntityManager(CashPosition.class, in.getParent().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getParent()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getParent(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashSubPositionDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CashSubPosition.class, in);
  }
}
