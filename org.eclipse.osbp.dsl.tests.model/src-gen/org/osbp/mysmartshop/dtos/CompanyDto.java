package org.osbp.mysmartshop.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.osbp.mysmartshop.dtos.AddressDto;
import org.osbp.mysmartshop.dtos.BaseUUIDDto;
import org.osbp.mysmartshop.dtos.CompanyGroupDto;
import org.osbp.mysmartshop.dtos.CompanyRelationTypeDto;
import org.osbp.mysmartshop.dtos.DepartmentDto;

@SuppressWarnings("all")
public class CompanyDto extends BaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  @DomainReference
  @FilterDepth(depth = 0)
  private CompanyGroupDto company_group;
  
  @DomainKey
  private String name;
  
  private String description;
  
  @DomainReference
  @FilterDepth(depth = 0)
  private CompanyRelationTypeDto relation_type;
  
  @DomainReference
  @Valid
  @FilterDepth(depth = 0)
  private List<AddressDto> address;
  
  @DomainReference
  @Valid
  @FilterDepth(depth = 0)
  private List<DepartmentDto> departments;
  
  public CompanyDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link Company} to the dto {@link CompanyDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
    address = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(
    				org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),
    				AddressDto.class, "company.id",
    				(java.util.function.Supplier<Object> & Serializable) () -> this.getId(), this);
    departments = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(
    				org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),
    				DepartmentDto.class, "company.id",
    				(java.util.function.Supplier<Object> & Serializable) () -> this.getId(), this);
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the company_group property or <code>null</code> if not present.
   */
  public CompanyGroupDto getCompany_group() {
    return this.company_group;
  }
  
  /**
   * Sets the <code>company_group</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>CompanyGroupDto#
   * companies</code> of the <code>company_group</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link CompanyGroupDto#setCompanies(CompanyGroupDto)
   * 
   * @param company_group - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCompany_group(final CompanyGroupDto company_group) {
    checkDisposed();
    if (this.company_group != null) {
    	this.company_group.internalRemoveFromCompanies(this);
    }
    
    internalSetCompany_group(company_group);
    
    if (this.company_group != null) {
    	this.company_group.internalAddToCompanies(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetCompany_group(final CompanyGroupDto company_group) {
    firePropertyChange("company_group", this.company_group, this.company_group = company_group);
  }
  
  /**
   * Returns the name property or <code>null</code> if not present.
   */
  public String getName() {
    return this.name;
  }
  
  /**
   * Sets the <code>name</code> property to this instance.
   * 
   * @param name - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setName(final String name) {
    firePropertyChange("name", this.name, this.name = name );
  }
  
  /**
   * Returns the description property or <code>null</code> if not present.
   */
  public String getDescription() {
    return this.description;
  }
  
  /**
   * Sets the <code>description</code> property to this instance.
   * 
   * @param description - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setDescription(final String description) {
    firePropertyChange("description", this.description, this.description = description );
  }
  
  /**
   * Returns the relation_type property or <code>null</code> if not present.
   */
  public CompanyRelationTypeDto getRelation_type() {
    return this.relation_type;
  }
  
  /**
   * Sets the <code>relation_type</code> property to this instance.
   * 
   * @param relation_type - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setRelation_type(final CompanyRelationTypeDto relation_type) {
    checkDisposed();
    firePropertyChange("relation_type", this.relation_type, this.relation_type = relation_type);
  }
  
  /**
   * Returns an unmodifiable list of address.
   */
  public List<AddressDto> getAddress() {
    return Collections.unmodifiableList(internalGetAddress());
  }
  
  /**
   * Returns the list of <code>AddressDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<AddressDto> internalGetAddress() {
    if (this.address == null) {
      this.address = new java.util.ArrayList<AddressDto>();
    }
    return this.address;
  }
  
  /**
   * Adds the given addressDto to this object. <p>
   * Since the reference is a composition reference, the opposite reference <code>AddressDto#company</code> of the <code>addressDto</code> will be handled automatically and no further coding is required to keep them in sync.<p>
   * See {@link AddressDto#setCompany(AddressDto)}.
   * 
   * @param addressDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToAddress(final AddressDto addressDto) {
    checkDisposed();
    
    addressDto.setCompany(this);
  }
  
  /**
   * Removes the given addressDto from this object. <p>
   * 
   * @param addressDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void removeFromAddress(final AddressDto addressDto) {
    checkDisposed();
    
    addressDto.setCompany(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToAddress(final AddressDto addressDto) {
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<AddressDto> oldList = null;
    		if(internalGetAddress() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetAddress()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetAddress());
    		}
    		internalGetAddress().add(addressDto);
    		firePropertyChange("address", oldList, internalGetAddress());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromAddress(final AddressDto addressDto) {
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<AddressDto> oldList = null;
    	if(internalGetAddress() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetAddress()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetAddress());
    	}
    	internalGetAddress().remove(addressDto);
    	firePropertyChange("address", oldList, internalGetAddress());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetAddress().remove(addressDto);
    }
  }
  
  /**
   * Sets the <code>address</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>AddressDto#
   * company</code> of the <code>address</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link AddressDto#setCompany(AddressDto)
   * 
   * @param address - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setAddress(final List<AddressDto> address) {
    checkDisposed();
    for (AddressDto dto : internalGetAddress().toArray(new AddressDto[this.address.size()])) {
    	removeFromAddress(dto);
    }
    
    if(address == null) {
    	return;
    }
    
    for (AddressDto dto : address) {
    	addToAddress(dto);
    }
  }
  
  /**
   * Returns an unmodifiable list of departments.
   */
  public List<DepartmentDto> getDepartments() {
    return Collections.unmodifiableList(internalGetDepartments());
  }
  
  /**
   * Returns the list of <code>DepartmentDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<DepartmentDto> internalGetDepartments() {
    if (this.departments == null) {
      this.departments = new java.util.ArrayList<DepartmentDto>();
    }
    return this.departments;
  }
  
  /**
   * Adds the given departmentDto to this object. <p>
   * Since the reference is a composition reference, the opposite reference <code>DepartmentDto#company</code> of the <code>departmentDto</code> will be handled automatically and no further coding is required to keep them in sync.<p>
   * See {@link DepartmentDto#setCompany(DepartmentDto)}.
   * 
   * @param departmentDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToDepartments(final DepartmentDto departmentDto) {
    checkDisposed();
    
    departmentDto.setCompany(this);
  }
  
  /**
   * Removes the given departmentDto from this object. <p>
   * 
   * @param departmentDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void removeFromDepartments(final DepartmentDto departmentDto) {
    checkDisposed();
    
    departmentDto.setCompany(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToDepartments(final DepartmentDto departmentDto) {
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<DepartmentDto> oldList = null;
    		if(internalGetDepartments() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetDepartments()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetDepartments());
    		}
    		internalGetDepartments().add(departmentDto);
    		firePropertyChange("departments", oldList, internalGetDepartments());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromDepartments(final DepartmentDto departmentDto) {
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<DepartmentDto> oldList = null;
    	if(internalGetDepartments() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetDepartments()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetDepartments());
    	}
    	internalGetDepartments().remove(departmentDto);
    	firePropertyChange("departments", oldList, internalGetDepartments());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetDepartments().remove(departmentDto);
    }
  }
  
  /**
   * Sets the <code>departments</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>DepartmentDto#
   * company</code> of the <code>departments</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link DepartmentDto#setCompany(DepartmentDto)
   * 
   * @param departments - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setDepartments(final List<DepartmentDto> departments) {
    checkDisposed();
    for (DepartmentDto dto : internalGetDepartments().toArray(new DepartmentDto[this.departments.size()])) {
    	removeFromDepartments(dto);
    }
    
    if(departments == null) {
    	return;
    }
    
    for (DepartmentDto dto : departments) {
    	addToDepartments(dto);
    }
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
