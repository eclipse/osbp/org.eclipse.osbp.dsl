package org.osbp.mysmartshop.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.eclipse.osbp.runtime.common.annotations.Properties;
import org.eclipse.osbp.runtime.common.annotations.Property;
import org.osbp.mysmartshop.dtos.BaseUUIDDto;
import org.osbp.mysmartshop.dtos.CashSlipDto;
import org.osbp.mysmartshop.dtos.CashSubPositionDto;

@SuppressWarnings("all")
public class CashPositionDto extends BaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  @DomainReference
  @FilterDepth(depth = 0)
  private CashSlipDto slip;
  
  @Valid
  private Date now;
  
  private double quantity;
  
  @Properties(properties = @Property(key = "decimalformat", value = "###,##0.00 &curren"))
  private Double price;
  
  @Properties(properties = @Property(key = "decimalformat", value = "###,##0.00 &curren"))
  private Double amount;
  
  @DomainReference
  @Valid
  @FilterDepth(depth = 0)
  private List<CashSubPositionDto> subPositions;
  
  public CashPositionDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link CashPosition} to the dto {@link CashPositionDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
    subPositions = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(
    				org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),
    				CashSubPositionDto.class, "parent.id",
    				(java.util.function.Supplier<Object> & Serializable) () -> this.getId(), this);
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the slip property or <code>null</code> if not present.
   */
  public CashSlipDto getSlip() {
    return this.slip;
  }
  
  /**
   * Sets the <code>slip</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>CashSlipDto#
   * positions</code> of the <code>slip</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link CashSlipDto#setPositions(CashSlipDto)
   * 
   * @param slip - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setSlip(final CashSlipDto slip) {
    checkDisposed();
    if (this.slip != null) {
    	this.slip.internalRemoveFromPositions(this);
    }
    
    internalSetSlip(slip);
    
    if (this.slip != null) {
    	this.slip.internalAddToPositions(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetSlip(final CashSlipDto slip) {
    firePropertyChange("slip", this.slip, this.slip = slip);
  }
  
  /**
   * Returns the now property or <code>null</code> if not present.
   */
  public Date getNow() {
    return this.now;
  }
  
  /**
   * Sets the <code>now</code> property to this instance.
   * 
   * @param now - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setNow(final Date now) {
    firePropertyChange("now", this.now, this.now = now );
  }
  
  /**
   * Returns the quantity property or <code>null</code> if not present.
   */
  public double getQuantity() {
    return this.quantity;
  }
  
  /**
   * Sets the <code>quantity</code> property to this instance.
   * 
   * @param quantity - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setQuantity(final double quantity) {
    firePropertyChange("quantity", this.quantity, this.quantity = quantity );
  }
  
  /**
   * Returns the price property or <code>null</code> if not present.
   */
  public Double getPrice() {
    return this.price;
  }
  
  /**
   * Sets the <code>price</code> property to this instance.
   * 
   * @param price - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPrice(final Double price) {
    firePropertyChange("price", this.price, this.price = price );
  }
  
  /**
   * Returns the amount property or <code>null</code> if not present.
   */
  public Double getAmount() {
    return this.amount;
  }
  
  /**
   * Sets the <code>amount</code> property to this instance.
   * 
   * @param amount - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setAmount(final Double amount) {
    firePropertyChange("amount", this.amount, this.amount = amount );
  }
  
  /**
   * Returns an unmodifiable list of subPositions.
   */
  public List<CashSubPositionDto> getSubPositions() {
    return Collections.unmodifiableList(internalGetSubPositions());
  }
  
  /**
   * Returns the list of <code>CashSubPositionDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<CashSubPositionDto> internalGetSubPositions() {
    if (this.subPositions == null) {
      this.subPositions = new java.util.ArrayList<CashSubPositionDto>();
    }
    return this.subPositions;
  }
  
  /**
   * Adds the given cashSubPositionDto to this object. <p>
   * Since the reference is a composition reference, the opposite reference <code>CashSubPositionDto#parent</code> of the <code>cashSubPositionDto</code> will be handled automatically and no further coding is required to keep them in sync.<p>
   * See {@link CashSubPositionDto#setParent(CashSubPositionDto)}.
   * 
   * @param cashSubPositionDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToSubPositions(final CashSubPositionDto cashSubPositionDto) {
    checkDisposed();
    
    cashSubPositionDto.setParent(this);
  }
  
  /**
   * Removes the given cashSubPositionDto from this object. <p>
   * 
   * @param cashSubPositionDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void removeFromSubPositions(final CashSubPositionDto cashSubPositionDto) {
    checkDisposed();
    
    cashSubPositionDto.setParent(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToSubPositions(final CashSubPositionDto cashSubPositionDto) {
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<CashSubPositionDto> oldList = null;
    		if(internalGetSubPositions() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetSubPositions()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetSubPositions());
    		}
    		internalGetSubPositions().add(cashSubPositionDto);
    		firePropertyChange("subPositions", oldList, internalGetSubPositions());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromSubPositions(final CashSubPositionDto cashSubPositionDto) {
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<CashSubPositionDto> oldList = null;
    	if(internalGetSubPositions() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetSubPositions()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetSubPositions());
    	}
    	internalGetSubPositions().remove(cashSubPositionDto);
    	firePropertyChange("subPositions", oldList, internalGetSubPositions());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetSubPositions().remove(cashSubPositionDto);
    }
  }
  
  /**
   * Sets the <code>subPositions</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>CashSubPositionDto#
   * parent</code> of the <code>subPositions</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link CashSubPositionDto#setParent(CashSubPositionDto)
   * 
   * @param subPositions - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setSubPositions(final List<CashSubPositionDto> subPositions) {
    checkDisposed();
    for (CashSubPositionDto dto : internalGetSubPositions().toArray(new CashSubPositionDto[this.subPositions.size()])) {
    	removeFromSubPositions(dto);
    }
    
    if(subPositions == null) {
    	return;
    }
    
    for (CashSubPositionDto dto : subPositions) {
    	addToSubPositions(dto);
    }
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
