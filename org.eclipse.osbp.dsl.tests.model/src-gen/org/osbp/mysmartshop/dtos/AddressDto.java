package org.osbp.mysmartshop.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.osbp.mysmartshop.dtos.BaseUUIDDto;
import org.osbp.mysmartshop.dtos.CompanyDto;
import org.osbp.mysmartshop.dtos.DepartmentDto;

@SuppressWarnings("all")
public class AddressDto extends BaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  @DomainReference
  @FilterDepth(depth = 0)
  private CompanyDto company;
  
  @DomainReference
  @FilterDepth(depth = 0)
  private DepartmentDto department;
  
  private String address1;
  
  private String address2;
  
  private String city;
  
  private String state_province;
  
  private String postal_code;
  
  private String country;
  
  private String email;
  
  private String landline;
  
  private String mobile;
  
  public AddressDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link Address} to the dto {@link AddressDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the company property or <code>null</code> if not present.
   */
  public CompanyDto getCompany() {
    return this.company;
  }
  
  /**
   * Sets the <code>company</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>CompanyDto#
   * address</code> of the <code>company</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link CompanyDto#setAddress(CompanyDto)
   * 
   * @param company - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCompany(final CompanyDto company) {
    checkDisposed();
    if (this.company != null) {
    	this.company.internalRemoveFromAddress(this);
    }
    
    internalSetCompany(company);
    
    if (this.company != null) {
    	this.company.internalAddToAddress(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetCompany(final CompanyDto company) {
    firePropertyChange("company", this.company, this.company = company);
  }
  
  /**
   * Returns the department property or <code>null</code> if not present.
   */
  public DepartmentDto getDepartment() {
    return this.department;
  }
  
  /**
   * Sets the <code>department</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>DepartmentDto#
   * address</code> of the <code>department</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link DepartmentDto#setAddress(DepartmentDto)
   * 
   * @param department - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setDepartment(final DepartmentDto department) {
    checkDisposed();
    if (this.department != null) {
    	this.department.internalRemoveFromAddress(this);
    }
    
    internalSetDepartment(department);
    
    if (this.department != null) {
    	this.department.internalAddToAddress(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetDepartment(final DepartmentDto department) {
    firePropertyChange("department", this.department, this.department = department);
  }
  
  /**
   * Returns the address1 property or <code>null</code> if not present.
   */
  public String getAddress1() {
    return this.address1;
  }
  
  /**
   * Sets the <code>address1</code> property to this instance.
   * 
   * @param address1 - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setAddress1(final String address1) {
    firePropertyChange("address1", this.address1, this.address1 = address1 );
  }
  
  /**
   * Returns the address2 property or <code>null</code> if not present.
   */
  public String getAddress2() {
    return this.address2;
  }
  
  /**
   * Sets the <code>address2</code> property to this instance.
   * 
   * @param address2 - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setAddress2(final String address2) {
    firePropertyChange("address2", this.address2, this.address2 = address2 );
  }
  
  /**
   * Returns the city property or <code>null</code> if not present.
   */
  public String getCity() {
    return this.city;
  }
  
  /**
   * Sets the <code>city</code> property to this instance.
   * 
   * @param city - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCity(final String city) {
    firePropertyChange("city", this.city, this.city = city );
  }
  
  /**
   * Returns the state_province property or <code>null</code> if not present.
   */
  public String getState_province() {
    return this.state_province;
  }
  
  /**
   * Sets the <code>state_province</code> property to this instance.
   * 
   * @param state_province - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setState_province(final String state_province) {
    firePropertyChange("state_province", this.state_province, this.state_province = state_province );
  }
  
  /**
   * Returns the postal_code property or <code>null</code> if not present.
   */
  public String getPostal_code() {
    return this.postal_code;
  }
  
  /**
   * Sets the <code>postal_code</code> property to this instance.
   * 
   * @param postal_code - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPostal_code(final String postal_code) {
    firePropertyChange("postal_code", this.postal_code, this.postal_code = postal_code );
  }
  
  /**
   * Returns the country property or <code>null</code> if not present.
   */
  public String getCountry() {
    return this.country;
  }
  
  /**
   * Sets the <code>country</code> property to this instance.
   * 
   * @param country - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCountry(final String country) {
    firePropertyChange("country", this.country, this.country = country );
  }
  
  /**
   * Returns the email property or <code>null</code> if not present.
   */
  public String getEmail() {
    return this.email;
  }
  
  /**
   * Sets the <code>email</code> property to this instance.
   * 
   * @param email - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setEmail(final String email) {
    firePropertyChange("email", this.email, this.email = email );
  }
  
  /**
   * Returns the landline property or <code>null</code> if not present.
   */
  public String getLandline() {
    return this.landline;
  }
  
  /**
   * Sets the <code>landline</code> property to this instance.
   * 
   * @param landline - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setLandline(final String landline) {
    firePropertyChange("landline", this.landline, this.landline = landline );
  }
  
  /**
   * Returns the mobile property or <code>null</code> if not present.
   */
  public String getMobile() {
    return this.mobile;
  }
  
  /**
   * Sets the <code>mobile</code> property to this instance.
   * 
   * @param mobile - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setMobile(final String mobile) {
    firePropertyChange("mobile", this.mobile, this.mobile = mobile );
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
