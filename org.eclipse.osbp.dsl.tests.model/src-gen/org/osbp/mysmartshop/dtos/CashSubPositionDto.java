package org.osbp.mysmartshop.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.eclipse.osbp.runtime.common.annotations.Properties;
import org.eclipse.osbp.runtime.common.annotations.Property;
import org.osbp.mysmartshop.dtos.BaseUUIDDto;
import org.osbp.mysmartshop.dtos.CashPositionDto;

@SuppressWarnings("all")
public class CashSubPositionDto extends BaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  private double quantity;
  
  @Properties(properties = @Property(key = "decimalformat", value = "###,##0.00 &curren"))
  private Double price;
  
  @DomainReference
  @FilterDepth(depth = 0)
  private CashPositionDto parent;
  
  public CashSubPositionDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link CashSubPosition} to the dto {@link CashSubPositionDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the quantity property or <code>null</code> if not present.
   */
  public double getQuantity() {
    return this.quantity;
  }
  
  /**
   * Sets the <code>quantity</code> property to this instance.
   * 
   * @param quantity - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setQuantity(final double quantity) {
    firePropertyChange("quantity", this.quantity, this.quantity = quantity );
  }
  
  /**
   * Returns the price property or <code>null</code> if not present.
   */
  public Double getPrice() {
    return this.price;
  }
  
  /**
   * Sets the <code>price</code> property to this instance.
   * 
   * @param price - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPrice(final Double price) {
    firePropertyChange("price", this.price, this.price = price );
  }
  
  /**
   * Returns the parent property or <code>null</code> if not present.
   */
  public CashPositionDto getParent() {
    return this.parent;
  }
  
  /**
   * Sets the <code>parent</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>CashPositionDto#
   * subPositions</code> of the <code>parent</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link CashPositionDto#setSubPositions(CashPositionDto)
   * 
   * @param parent - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setParent(final CashPositionDto parent) {
    checkDisposed();
    if (this.parent != null) {
    	this.parent.internalRemoveFromSubPositions(this);
    }
    
    internalSetParent(parent);
    
    if (this.parent != null) {
    	this.parent.internalAddToSubPositions(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetParent(final CashPositionDto parent) {
    firePropertyChange("parent", this.parent, this.parent = parent);
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
