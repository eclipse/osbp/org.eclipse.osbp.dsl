package org.osbp.mysmartshop.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.osbp.mysmartshop.dtos.BaseUUIDDto;

@SuppressWarnings("all")
public class BarDto extends BaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  private String fooxxx;
  
  public BarDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link Bar} to the dto {@link BarDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the fooxxx property or <code>null</code> if not present.
   */
  public String getFooxxx() {
    return this.fooxxx;
  }
  
  /**
   * Sets the <code>fooxxx</code> property to this instance.
   * 
   * @param fooxxx - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setFooxxx(final String fooxxx) {
    firePropertyChange("fooxxx", this.fooxxx, this.fooxxx = fooxxx );
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
