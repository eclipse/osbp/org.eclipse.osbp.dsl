package org.osbp.mysmartshop.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.osbp.mysmartshop.dtos.CashSubPositionDto;
import org.osbp.mysmartshop.entities.CashSubPosition;

@SuppressWarnings("all")
public class CashSubPositionDtoService extends AbstractDTOServiceWithMutablePersistence<CashSubPositionDto, CashSubPosition> {
  public CashSubPositionDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<CashSubPositionDto> getDtoClass() {
    return CashSubPositionDto.class;
  }
  
  public Class<CashSubPosition> getEntityClass() {
    return CashSubPosition.class;
  }
  
  public Object getId(final CashSubPositionDto dto) {
    return dto.getId();
  }
}
