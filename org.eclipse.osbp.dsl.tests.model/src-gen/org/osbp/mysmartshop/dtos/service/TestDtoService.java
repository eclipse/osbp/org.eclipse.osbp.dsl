package org.osbp.mysmartshop.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.osbp.mysmartshop.dtos.TestDto;
import org.osbp.mysmartshop.entities.Test;

@SuppressWarnings("all")
public class TestDtoService extends AbstractDTOServiceWithMutablePersistence<TestDto, Test> {
  public TestDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<TestDto> getDtoClass() {
    return TestDto.class;
  }
  
  public Class<Test> getEntityClass() {
    return Test.class;
  }
  
  public Object getId(final TestDto dto) {
    return dto.getId();
  }
}
