package org.osbp.mysmartshop.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.osbp.mysmartshop.dtos.BarDto;
import org.osbp.mysmartshop.entities.Bar;

@SuppressWarnings("all")
public class BarDtoService extends AbstractDTOServiceWithMutablePersistence<BarDto, Bar> {
  public BarDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<BarDto> getDtoClass() {
    return BarDto.class;
  }
  
  public Class<Bar> getEntityClass() {
    return Bar.class;
  }
  
  public Object getId(final BarDto dto) {
    return dto.getId();
  }
}
