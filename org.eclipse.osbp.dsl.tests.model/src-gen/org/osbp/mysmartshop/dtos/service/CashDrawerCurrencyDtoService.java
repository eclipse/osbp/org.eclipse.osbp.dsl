package org.osbp.mysmartshop.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.osbp.mysmartshop.dtos.CashDrawerCurrencyDto;
import org.osbp.mysmartshop.entities.CashDrawerCurrency;

@SuppressWarnings("all")
public class CashDrawerCurrencyDtoService extends AbstractDTOServiceWithMutablePersistence<CashDrawerCurrencyDto, CashDrawerCurrency> {
  public CashDrawerCurrencyDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<CashDrawerCurrencyDto> getDtoClass() {
    return CashDrawerCurrencyDto.class;
  }
  
  public Class<CashDrawerCurrency> getEntityClass() {
    return CashDrawerCurrency.class;
  }
  
  public Object getId(final CashDrawerCurrencyDto dto) {
    return dto.getId();
  }
}
