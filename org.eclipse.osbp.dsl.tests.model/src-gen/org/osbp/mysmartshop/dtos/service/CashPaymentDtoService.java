/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.osbp.mysmartshop.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.osbp.mysmartshop.dtos.CashPaymentDto;
import org.osbp.mysmartshop.entities.CashPayment;

@SuppressWarnings("all")
public class CashPaymentDtoService extends AbstractDTOServiceWithMutablePersistence<CashPaymentDto, CashPayment> {
  public CashPaymentDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<CashPaymentDto> getDtoClass() {
    return CashPaymentDto.class;
  }
  
  public Class<CashPayment> getEntityClass() {
    return CashPayment.class;
  }
  
  public Object getId(final CashPaymentDto dto) {
    return dto.getId();
  }
}
