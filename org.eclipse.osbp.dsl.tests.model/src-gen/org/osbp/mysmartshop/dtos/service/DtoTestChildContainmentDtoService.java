/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.osbp.mysmartshop.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.osbp.mysmartshop.dtos.DtoTestChildContainmentDto;
import org.osbp.mysmartshop.entities.DtoTestChildContainment;

@SuppressWarnings("all")
public class DtoTestChildContainmentDtoService extends AbstractDTOServiceWithMutablePersistence<DtoTestChildContainmentDto, DtoTestChildContainment> {
  public DtoTestChildContainmentDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<DtoTestChildContainmentDto> getDtoClass() {
    return DtoTestChildContainmentDto.class;
  }
  
  public Class<DtoTestChildContainment> getEntityClass() {
    return DtoTestChildContainment.class;
  }
  
  public Object getId(final DtoTestChildContainmentDto dto) {
    return dto.getId();
  }
}
