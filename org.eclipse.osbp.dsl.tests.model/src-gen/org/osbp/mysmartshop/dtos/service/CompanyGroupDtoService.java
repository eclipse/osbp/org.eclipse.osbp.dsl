/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.osbp.mysmartshop.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.osbp.mysmartshop.dtos.CompanyGroupDto;
import org.osbp.mysmartshop.entities.CompanyGroup;

@SuppressWarnings("all")
public class CompanyGroupDtoService extends AbstractDTOServiceWithMutablePersistence<CompanyGroupDto, CompanyGroup> {
  public CompanyGroupDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<CompanyGroupDto> getDtoClass() {
    return CompanyGroupDto.class;
  }
  
  public Class<CompanyGroup> getEntityClass() {
    return CompanyGroup.class;
  }
  
  public Object getId(final CompanyGroupDto dto) {
    return dto.getId();
  }
}
