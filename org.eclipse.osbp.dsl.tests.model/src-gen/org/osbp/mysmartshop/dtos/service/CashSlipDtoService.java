package org.osbp.mysmartshop.dtos.service;

import java.util.Collections;
import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;
import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.jpa.services.JPQL;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.Pair;
import org.osbp.mysmartshop.dtos.CashSlipDto;
import org.osbp.mysmartshop.entities.CashSlip;

@SuppressWarnings("all")
public class CashSlipDtoService extends AbstractDTOServiceWithMutablePersistence<CashSlipDto, CashSlip> {
  public CashSlipDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<CashSlipDto> getDtoClass() {
    return CashSlipDto.class;
  }
  
  public Class<CashSlip> getEntityClass() {
    return CashSlip.class;
  }
  
  public Object getId(final CashSlipDto dto) {
    return dto.getId();
  }
  
  public Double calcSum(final CashSlipDto slip) {
    String _id = slip.getId();
    Pair<String, String> _mappedTo = Pair.<String, String>of("slipUUID", _id);
    final JPQL ql = AbstractDTOService.createJPQL("select sum(p.price) from CashPosition p where p.slip.id = :slipUUID", Collections.<String, Object>unmodifiableMap(CollectionLiterals.<String, Object>newHashMap(_mappedTo)));
    InputOutput.<String>print("Foo");
    final List<?> resultList = this.findValues(ql);
    InputOutput.<String>print("Bar");
    Object _get = resultList.get(00);
    return ((Double) _get);
  }
}
