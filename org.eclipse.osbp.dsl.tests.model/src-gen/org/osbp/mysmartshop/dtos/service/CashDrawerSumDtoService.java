package org.osbp.mysmartshop.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.osbp.mysmartshop.dtos.CashDrawerSumDto;
import org.osbp.mysmartshop.entities.CashDrawerSum;

@SuppressWarnings("all")
public class CashDrawerSumDtoService extends AbstractDTOServiceWithMutablePersistence<CashDrawerSumDto, CashDrawerSum> {
  public CashDrawerSumDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<CashDrawerSumDto> getDtoClass() {
    return CashDrawerSumDto.class;
  }
  
  public Class<CashDrawerSum> getEntityClass() {
    return CashDrawerSum.class;
  }
  
  public Object getId(final CashDrawerSumDto dto) {
    return dto.getId();
  }
}
