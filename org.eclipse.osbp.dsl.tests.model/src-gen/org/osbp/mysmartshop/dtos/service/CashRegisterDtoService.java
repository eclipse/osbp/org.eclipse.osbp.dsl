/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.osbp.mysmartshop.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.osbp.mysmartshop.dtos.CashRegisterDto;
import org.osbp.mysmartshop.entities.CashRegister;

@SuppressWarnings("all")
public class CashRegisterDtoService extends AbstractDTOServiceWithMutablePersistence<CashRegisterDto, CashRegister> {
  public CashRegisterDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<CashRegisterDto> getDtoClass() {
    return CashRegisterDto.class;
  }
  
  public Class<CashRegister> getEntityClass() {
    return CashRegister.class;
  }
  
  public Object getId(final CashRegisterDto dto) {
    return dto.getId();
  }
}
