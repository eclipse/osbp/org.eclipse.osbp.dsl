package org.osbp.mysmartshop.dtos.service;

import java.util.Collections;
import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;
import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.jpa.services.JPQL;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pair;
import org.osbp.mysmartshop.dtos.CashPositionDto;
import org.osbp.mysmartshop.entities.CashPosition;

@SuppressWarnings("all")
public class CashPositionDtoService extends AbstractDTOServiceWithMutablePersistence<CashPositionDto, CashPosition> {
  public CashPositionDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<CashPositionDto> getDtoClass() {
    return CashPositionDto.class;
  }
  
  public Class<CashPosition> getEntityClass() {
    return CashPosition.class;
  }
  
  public Object getId(final CashPositionDto dto) {
    return dto.getId();
  }
  
  public CashPositionDto findLast(final String slipUUID) {
    Pair<String, String> _mappedTo = Pair.<String, String>of("slipUUID", slipUUID);
    final JPQL ql = AbstractDTOService.createJPQL("select p from CashPosition p where p.slip.id = :slipUUID order by p.price", Collections.<String, Object>unmodifiableMap(CollectionLiterals.<String, Object>newHashMap(_mappedTo)));
    return IterableExtensions.<CashPositionDto>last(this.findDtos(ql));
  }
}
