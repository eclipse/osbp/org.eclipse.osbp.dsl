package org.osbp.mysmartshop.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.osbp.mysmartshop.dtos.BaseUUIDDto;
import org.osbp.mysmartshop.dtos.CashDrawerCurrencyDto;
import org.osbp.mysmartshop.dtos.CashPaymentDto;

@SuppressWarnings("all")
public class CashDrawerSumDto extends BaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  @DomainReference
  @FilterDepth(depth = 0)
  private CashDrawerCurrencyDto drawer;
  
  @DomainReference
  @FilterDepth(depth = 0)
  private List<CashPaymentDto> payments;
  
  public CashDrawerSumDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link CashDrawerSum} to the dto {@link CashDrawerSumDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
    payments = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(
    				org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),
    				CashPaymentDto.class, "close.id",
    				(java.util.function.Supplier<Object> & Serializable) () -> this.getId(), this);
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the drawer property or <code>null</code> if not present.
   */
  public CashDrawerCurrencyDto getDrawer() {
    return this.drawer;
  }
  
  /**
   * Sets the <code>drawer</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>CashDrawerCurrencyDto#
   * sums</code> of the <code>drawer</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link CashDrawerCurrencyDto#setSums(CashDrawerCurrencyDto)
   * 
   * @param drawer - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setDrawer(final CashDrawerCurrencyDto drawer) {
    checkDisposed();
    if (this.drawer != null) {
    	this.drawer.internalRemoveFromSums(this);
    }
    
    internalSetDrawer(drawer);
    
    if (this.drawer != null) {
    	this.drawer.internalAddToSums(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetDrawer(final CashDrawerCurrencyDto drawer) {
    firePropertyChange("drawer", this.drawer, this.drawer = drawer);
  }
  
  /**
   * Returns an unmodifiable list of payments.
   */
  public List<CashPaymentDto> getPayments() {
    return Collections.unmodifiableList(internalGetPayments());
  }
  
  /**
   * Returns the list of <code>CashPaymentDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<CashPaymentDto> internalGetPayments() {
    if (this.payments == null) {
      this.payments = new java.util.ArrayList<CashPaymentDto>();
    }
    return this.payments;
  }
  
  /**
   * Adds the given cashPaymentDto to this object. <p>
   * Since the reference is a composition reference, the opposite reference <code>CashPaymentDto#close</code> of the <code>cashPaymentDto</code> will be handled automatically and no further coding is required to keep them in sync.<p>
   * See {@link CashPaymentDto#setClose(CashPaymentDto)}.
   * 
   * @param cashPaymentDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToPayments(final CashPaymentDto cashPaymentDto) {
    checkDisposed();
    
    cashPaymentDto.setClose(this);
  }
  
  /**
   * Removes the given cashPaymentDto from this object. <p>
   * 
   * @param cashPaymentDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void removeFromPayments(final CashPaymentDto cashPaymentDto) {
    checkDisposed();
    
    cashPaymentDto.setClose(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToPayments(final CashPaymentDto cashPaymentDto) {
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<CashPaymentDto> oldList = null;
    		if(internalGetPayments() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetPayments()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetPayments());
    		}
    		internalGetPayments().add(cashPaymentDto);
    		firePropertyChange("payments", oldList, internalGetPayments());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromPayments(final CashPaymentDto cashPaymentDto) {
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<CashPaymentDto> oldList = null;
    	if(internalGetPayments() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetPayments()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetPayments());
    	}
    	internalGetPayments().remove(cashPaymentDto);
    	firePropertyChange("payments", oldList, internalGetPayments());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetPayments().remove(cashPaymentDto);
    }
  }
  
  /**
   * Sets the <code>payments</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>CashPaymentDto#
   * close</code> of the <code>payments</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link CashPaymentDto#setClose(CashPaymentDto)
   * 
   * @param payments - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPayments(final List<CashPaymentDto> payments) {
    checkDisposed();
    for (CashPaymentDto dto : internalGetPayments().toArray(new CashPaymentDto[this.payments.size()])) {
    	removeFromPayments(dto);
    }
    
    if(payments == null) {
    	return;
    }
    
    for (CashPaymentDto dto : payments) {
    	addToPayments(dto);
    }
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
