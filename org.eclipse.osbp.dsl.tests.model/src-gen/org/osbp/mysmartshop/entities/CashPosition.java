/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.Properties;
import org.eclipse.osbp.runtime.common.annotations.Property;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.CashSlip;
import org.osbp.mysmartshop.entities.CashSubPosition;

@Entity
@Table(name = "CASH_POSITION")
@SuppressWarnings("all")
public class CashPosition extends BaseUUID implements IEntity {
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "SLIP_ID")
  private CashSlip slip;
  
  @Column(name = "NOW")
  @Temporal(value = TemporalType.TIMESTAMP)
  @Valid
  private Date now;
  
  @Column(name = "QUANTITY")
  private double quantity;
  
  @Column(name = "PRICE")
  @Properties(properties = @Property(key = "decimalformat", value = "###,##0.00 &curren"))
  private Double price;
  
  @Column(name = "AMOUNT")
  @Properties(properties = @Property(key = "decimalformat", value = "###,##0.00 &curren"))
  private Double amount;
  
  @JoinColumn(name = "SUB_POSITIONS_ID")
  @OneToMany(mappedBy = "parent", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<CashSubPosition> subPositions;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    try {
      // Dispose all the composition references.
      if (this.subPositions != null) {
        for (CashSubPosition cashSubPosition : this.subPositions) {
          cashSubPosition.dispose();
        }
        this.subPositions = null;
      }
      
    }
    finally {
      super.dispose();
    }
    
  }
  
  /**
   * @return Returns the slip property or <code>null</code> if not present.
   */
  public CashSlip getSlip() {
    checkDisposed();
    return this.slip;
  }
  
  /**
   * Sets the slip property to this instance.
   * Since the reference is a container reference, the opposite reference (CashSlip.positions)
   * of the slip will be handled automatically and no further coding is required to keep them in sync.
   * See {@link CashSlip#setPositions(CashSlip)}.
   */
  public void setSlip(final CashSlip slip) {
    checkDisposed();
    if (this.slip != null) {
      this.slip.internalRemoveFromPositions(this);
    }
    internalSetSlip(slip);
    if (this.slip != null) {
      this.slip.internalAddToPositions(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetSlip(final CashSlip slip) {
    this.slip = slip;
  }
  
  /**
   * @return Returns the now property or <code>null</code> if not present.
   */
  public Date getNow() {
    checkDisposed();
    return this.now;
  }
  
  /**
   * Sets the now property to this instance.
   */
  public void setNow(final Date now) {
    checkDisposed();
    this.now = now;
  }
  
  /**
   * @return Returns the quantity property or <code>null</code> if not present.
   */
  public double getQuantity() {
    checkDisposed();
    return this.quantity;
  }
  
  /**
   * Sets the quantity property to this instance.
   */
  public void setQuantity(final double quantity) {
    checkDisposed();
    this.quantity = quantity;
  }
  
  /**
   * @return Returns the price property or <code>null</code> if not present.
   */
  public Double getPrice() {
    checkDisposed();
    return this.price;
  }
  
  /**
   * Sets the price property to this instance.
   */
  public void setPrice(final Double price) {
    checkDisposed();
    this.price = price;
  }
  
  /**
   * @return Returns the amount property or <code>null</code> if not present.
   */
  public Double getAmount() {
    checkDisposed();
    return this.amount;
  }
  
  /**
   * Sets the amount property to this instance.
   */
  public void setAmount(final Double amount) {
    checkDisposed();
    this.amount = amount;
  }
  
  /**
   * @return Returns an unmodifiable list of subPositions.
   */
  public List<CashSubPosition> getSubPositions() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetSubPositions());
  }
  
  /**
   * Sets the given subPositions to the object. Currently contained subPositions instances will be removed.
   * 
   * @param subPositions the list of new instances
   */
  public void setSubPositions(final List<CashSubPosition> subPositions) {
    // remove the old cashSubPosition
    for(CashSubPosition oldElement : new ArrayList<CashSubPosition>(this.internalGetSubPositions())){
      removeFromSubPositions(oldElement);
    }
    
    // add the new cashSubPosition
    for(CashSubPosition newElement : subPositions){
      addToSubPositions(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>CashSubPosition</code>s thereby lazy initializing it.
   */
  public List<CashSubPosition> internalGetSubPositions() {
    if (this.subPositions == null) {
      this.subPositions = new ArrayList<CashSubPosition>();
    }
    return this.subPositions;
  }
  
  /**
   * Adds the given cashSubPosition to this object. <p>
   * Since the reference is a composition reference, the opposite reference (CashSubPosition.parent)
   * of the cashSubPosition will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link CashSubPosition#setParent(CashSubPosition)}.
   * 
   */
  public void addToSubPositions(final CashSubPosition cashSubPosition) {
    checkDisposed();
    cashSubPosition.setParent(this);
  }
  
  /**
   * Removes the given cashSubPosition from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (CashSubPosition.parent)
   * of the cashSubPosition will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link CashSubPosition#setParent(CashSubPosition)}.
   * 
   */
  public void removeFromSubPositions(final CashSubPosition cashSubPosition) {
    checkDisposed();
    cashSubPosition.setParent(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToSubPositions(final CashSubPosition cashSubPosition) {
    if(cashSubPosition == null) {
    	return;
    }
    
    		internalGetSubPositions().add(cashSubPosition);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromSubPositions(final CashSubPosition cashSubPosition) {
    internalGetSubPositions().remove(cashSubPosition);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
