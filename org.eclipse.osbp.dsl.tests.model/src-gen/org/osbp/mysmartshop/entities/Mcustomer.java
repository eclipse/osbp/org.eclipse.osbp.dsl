/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.osbp.mysmartshop.entities.BaseID;
import org.osbp.mysmartshop.entities.CashSlip;

@Entity
@Table(name = "MCUSTOMER")
@SuppressWarnings("all")
public class Mcustomer extends BaseID implements IEntity {
  @Column(name = "ACCOUNT_NUM")
  private long account_num;
  
  @Column(name = "LNAME")
  private String lname;
  
  @Column(name = "FNAME")
  private String fname;
  
  @Column(name = "MI")
  private String mi;
  
  @Column(name = "ADDRESS1")
  private String address1;
  
  @Column(name = "ADDRESS2")
  private String address2;
  
  @Column(name = "ADDRESS3")
  private String address3;
  
  @Column(name = "ADDRESS4")
  private String address4;
  
  @Column(name = "CITY")
  private String city;
  
  @Column(name = "STATE_PROVINCE")
  private String state_province;
  
  @Column(name = "POSTAL_CODE")
  private String postal_code;
  
  @Column(name = "COUNTRY")
  private String country;
  
  @Column(name = "PHONE1")
  private String phone1;
  
  @Column(name = "PHONE2")
  private String phone2;
  
  @Column(name = "BIRTHDATE")
  @Temporal(value = TemporalType.DATE)
  @Valid
  private Date birthdate;
  
  @Column(name = "MARITAL_STATUS")
  private String marital_status;
  
  @Column(name = "YEARLY_INCOME")
  private String yearly_income;
  
  @Column(name = "GENDER")
  private String gender;
  
  @Column(name = "TOTAL_CHILDREN")
  private int total_children;
  
  @Column(name = "NUM_CHILDREN_AT_HOME")
  private int num_children_at_home;
  
  @Column(name = "EDUCATION")
  private String education;
  
  @Column(name = "DATE_ACCNT_OPENED")
  private String date_accnt_opened;
  
  @Column(name = "MEMBER_CARD")
  private String member_card;
  
  @Column(name = "OCCUPATION")
  private String occupation;
  
  @Column(name = "HOUSEOWNER")
  private String houseowner;
  
  @Column(name = "NUM_CARS_OWNED")
  private int num_cars_owned;
  
  @DomainKey
  @Column(name = "FULLNAME")
  private String fullname;
  
  @Column(name = "TEST")
  private String test;
  
  @JoinColumn(name = "SLIPS_ID")
  @OneToMany(mappedBy = "customer")
  private List<CashSlip> slips;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the account_num property or <code>null</code> if not present.
   */
  public long getAccount_num() {
    checkDisposed();
    return this.account_num;
  }
  
  /**
   * Sets the account_num property to this instance.
   */
  public void setAccount_num(final long account_num) {
    checkDisposed();
    this.account_num = account_num;
  }
  
  /**
   * @return Returns the lname property or <code>null</code> if not present.
   */
  public String getLname() {
    checkDisposed();
    return this.lname;
  }
  
  /**
   * Sets the lname property to this instance.
   */
  public void setLname(final String lname) {
    checkDisposed();
    this.lname = lname;
  }
  
  /**
   * @return Returns the fname property or <code>null</code> if not present.
   */
  public String getFname() {
    checkDisposed();
    return this.fname;
  }
  
  /**
   * Sets the fname property to this instance.
   */
  public void setFname(final String fname) {
    checkDisposed();
    this.fname = fname;
  }
  
  /**
   * @return Returns the mi property or <code>null</code> if not present.
   */
  public String getMi() {
    checkDisposed();
    return this.mi;
  }
  
  /**
   * Sets the mi property to this instance.
   */
  public void setMi(final String mi) {
    checkDisposed();
    this.mi = mi;
  }
  
  /**
   * @return Returns the address1 property or <code>null</code> if not present.
   */
  public String getAddress1() {
    checkDisposed();
    return this.address1;
  }
  
  /**
   * Sets the address1 property to this instance.
   */
  public void setAddress1(final String address1) {
    checkDisposed();
    this.address1 = address1;
  }
  
  /**
   * @return Returns the address2 property or <code>null</code> if not present.
   */
  public String getAddress2() {
    checkDisposed();
    return this.address2;
  }
  
  /**
   * Sets the address2 property to this instance.
   */
  public void setAddress2(final String address2) {
    checkDisposed();
    this.address2 = address2;
  }
  
  /**
   * @return Returns the address3 property or <code>null</code> if not present.
   */
  public String getAddress3() {
    checkDisposed();
    return this.address3;
  }
  
  /**
   * Sets the address3 property to this instance.
   */
  public void setAddress3(final String address3) {
    checkDisposed();
    this.address3 = address3;
  }
  
  /**
   * @return Returns the address4 property or <code>null</code> if not present.
   */
  public String getAddress4() {
    checkDisposed();
    return this.address4;
  }
  
  /**
   * Sets the address4 property to this instance.
   */
  public void setAddress4(final String address4) {
    checkDisposed();
    this.address4 = address4;
  }
  
  /**
   * @return Returns the city property or <code>null</code> if not present.
   */
  public String getCity() {
    checkDisposed();
    return this.city;
  }
  
  /**
   * Sets the city property to this instance.
   */
  public void setCity(final String city) {
    checkDisposed();
    this.city = city;
  }
  
  /**
   * @return Returns the state_province property or <code>null</code> if not present.
   */
  public String getState_province() {
    checkDisposed();
    return this.state_province;
  }
  
  /**
   * Sets the state_province property to this instance.
   */
  public void setState_province(final String state_province) {
    checkDisposed();
    this.state_province = state_province;
  }
  
  /**
   * @return Returns the postal_code property or <code>null</code> if not present.
   */
  public String getPostal_code() {
    checkDisposed();
    return this.postal_code;
  }
  
  /**
   * Sets the postal_code property to this instance.
   */
  public void setPostal_code(final String postal_code) {
    checkDisposed();
    this.postal_code = postal_code;
  }
  
  /**
   * @return Returns the country property or <code>null</code> if not present.
   */
  public String getCountry() {
    checkDisposed();
    return this.country;
  }
  
  /**
   * Sets the country property to this instance.
   */
  public void setCountry(final String country) {
    checkDisposed();
    this.country = country;
  }
  
  /**
   * @return Returns the phone1 property or <code>null</code> if not present.
   */
  public String getPhone1() {
    checkDisposed();
    return this.phone1;
  }
  
  /**
   * Sets the phone1 property to this instance.
   */
  public void setPhone1(final String phone1) {
    checkDisposed();
    this.phone1 = phone1;
  }
  
  /**
   * @return Returns the phone2 property or <code>null</code> if not present.
   */
  public String getPhone2() {
    checkDisposed();
    return this.phone2;
  }
  
  /**
   * Sets the phone2 property to this instance.
   */
  public void setPhone2(final String phone2) {
    checkDisposed();
    this.phone2 = phone2;
  }
  
  /**
   * @return Returns the birthdate property or <code>null</code> if not present.
   */
  public Date getBirthdate() {
    checkDisposed();
    return this.birthdate;
  }
  
  /**
   * Sets the birthdate property to this instance.
   */
  public void setBirthdate(final Date birthdate) {
    checkDisposed();
    this.birthdate = birthdate;
  }
  
  /**
   * @return Returns the marital_status property or <code>null</code> if not present.
   */
  public String getMarital_status() {
    checkDisposed();
    return this.marital_status;
  }
  
  /**
   * Sets the marital_status property to this instance.
   */
  public void setMarital_status(final String marital_status) {
    checkDisposed();
    this.marital_status = marital_status;
  }
  
  /**
   * @return Returns the yearly_income property or <code>null</code> if not present.
   */
  public String getYearly_income() {
    checkDisposed();
    return this.yearly_income;
  }
  
  /**
   * Sets the yearly_income property to this instance.
   */
  public void setYearly_income(final String yearly_income) {
    checkDisposed();
    this.yearly_income = yearly_income;
  }
  
  /**
   * @return Returns the gender property or <code>null</code> if not present.
   */
  public String getGender() {
    checkDisposed();
    return this.gender;
  }
  
  /**
   * Sets the gender property to this instance.
   */
  public void setGender(final String gender) {
    checkDisposed();
    this.gender = gender;
  }
  
  /**
   * @return Returns the total_children property or <code>null</code> if not present.
   */
  public int getTotal_children() {
    checkDisposed();
    return this.total_children;
  }
  
  /**
   * Sets the total_children property to this instance.
   */
  public void setTotal_children(final int total_children) {
    checkDisposed();
    this.total_children = total_children;
  }
  
  /**
   * @return Returns the num_children_at_home property or <code>null</code> if not present.
   */
  public int getNum_children_at_home() {
    checkDisposed();
    return this.num_children_at_home;
  }
  
  /**
   * Sets the num_children_at_home property to this instance.
   */
  public void setNum_children_at_home(final int num_children_at_home) {
    checkDisposed();
    this.num_children_at_home = num_children_at_home;
  }
  
  /**
   * @return Returns the education property or <code>null</code> if not present.
   */
  public String getEducation() {
    checkDisposed();
    return this.education;
  }
  
  /**
   * Sets the education property to this instance.
   */
  public void setEducation(final String education) {
    checkDisposed();
    this.education = education;
  }
  
  /**
   * @return Returns the date_accnt_opened property or <code>null</code> if not present.
   */
  public String getDate_accnt_opened() {
    checkDisposed();
    return this.date_accnt_opened;
  }
  
  /**
   * Sets the date_accnt_opened property to this instance.
   */
  public void setDate_accnt_opened(final String date_accnt_opened) {
    checkDisposed();
    this.date_accnt_opened = date_accnt_opened;
  }
  
  /**
   * @return Returns the member_card property or <code>null</code> if not present.
   */
  public String getMember_card() {
    checkDisposed();
    return this.member_card;
  }
  
  /**
   * Sets the member_card property to this instance.
   */
  public void setMember_card(final String member_card) {
    checkDisposed();
    this.member_card = member_card;
  }
  
  /**
   * @return Returns the occupation property or <code>null</code> if not present.
   */
  public String getOccupation() {
    checkDisposed();
    return this.occupation;
  }
  
  /**
   * Sets the occupation property to this instance.
   */
  public void setOccupation(final String occupation) {
    checkDisposed();
    this.occupation = occupation;
  }
  
  /**
   * @return Returns the houseowner property or <code>null</code> if not present.
   */
  public String getHouseowner() {
    checkDisposed();
    return this.houseowner;
  }
  
  /**
   * Sets the houseowner property to this instance.
   */
  public void setHouseowner(final String houseowner) {
    checkDisposed();
    this.houseowner = houseowner;
  }
  
  /**
   * @return Returns the num_cars_owned property or <code>null</code> if not present.
   */
  public int getNum_cars_owned() {
    checkDisposed();
    return this.num_cars_owned;
  }
  
  /**
   * Sets the num_cars_owned property to this instance.
   */
  public void setNum_cars_owned(final int num_cars_owned) {
    checkDisposed();
    this.num_cars_owned = num_cars_owned;
  }
  
  /**
   * @return Returns the fullname property or <code>null</code> if not present.
   */
  public String getFullname() {
    checkDisposed();
    return this.fullname;
  }
  
  /**
   * Sets the fullname property to this instance.
   */
  public void setFullname(final String fullname) {
    checkDisposed();
    this.fullname = fullname;
  }
  
  /**
   * @return Returns the test property or <code>null</code> if not present.
   */
  public String getTest() {
    checkDisposed();
    return this.test;
  }
  
  /**
   * Sets the test property to this instance.
   */
  public void setTest(final String test) {
    checkDisposed();
    this.test = test;
  }
  
  /**
   * @return Returns an unmodifiable list of slips.
   */
  public List<CashSlip> getSlips() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetSlips());
  }
  
  /**
   * Sets the given slips to the object. Currently contained slips instances will be removed.
   * 
   * @param slips the list of new instances
   */
  public void setSlips(final List<CashSlip> slips) {
    // remove the old cashSlip
    for(CashSlip oldElement : new ArrayList<CashSlip>(this.internalGetSlips())){
      removeFromSlips(oldElement);
    }
    
    // add the new cashSlip
    for(CashSlip newElement : slips){
      addToSlips(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>CashSlip</code>s thereby lazy initializing it.
   */
  public List<CashSlip> internalGetSlips() {
    if (this.slips == null) {
      this.slips = new ArrayList<CashSlip>();
    }
    return this.slips;
  }
  
  /**
   * Adds the given cashSlip to this object. <p>
   * Since the reference is a composition reference, the opposite reference (CashSlip.customer)
   * of the cashSlip will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link CashSlip#setCustomer(CashSlip)}.
   * 
   */
  public void addToSlips(final CashSlip cashSlip) {
    checkDisposed();
    cashSlip.setCustomer(this);
  }
  
  /**
   * Removes the given cashSlip from this object. <p>
   * 
   */
  public void removeFromSlips(final CashSlip cashSlip) {
    checkDisposed();
    cashSlip.setCustomer(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToSlips(final CashSlip cashSlip) {
    if(cashSlip == null) {
    	return;
    }
    
    		internalGetSlips().add(cashSlip);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromSlips(final CashSlip cashSlip) {
    internalGetSlips().remove(cashSlip);
  }
  
  /**
   * Calculates the value for the derived property foo
   *  
   * @return foo The derived property value
   */
  public String getFoo() {
    return ((this.country + "_") + this.city);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    // remove the slips
    for(CashSlip oldElement : new ArrayList<CashSlip>(this.internalGetSlips())){
      removeFromSlips(oldElement);
    }
  }
}
