/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.DtoTestBean;
import org.osbp.mysmartshop.entities.DtoTestChildContainment;
import org.osbp.mysmartshop.entities.DtoTestChildCrossRef;

@Entity
@Table(name = "DTO_TEST_PARENT")
@SuppressWarnings("all")
public class DtoTestParent extends BaseUUID implements IEntity {
  @Column(name = "STRING")
  private String string;
  
  @JoinColumn(name = "CONTAINMENT_CHILDS_ID")
  @OneToMany(mappedBy = "container", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<DtoTestChildContainment> containmentChilds;
  
  @JoinColumn(name = "CROSS_REF_CHILDS_ID")
  @OneToMany(mappedBy = "container")
  private List<DtoTestChildCrossRef> crossRefChilds;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CROSS_REF_CHILD_ID")
  private DtoTestChildCrossRef crossRefChild;
  
  @Embedded
  @AttributeOverrides(value = @AttributeOverride(name = "foo", column = @Column(name = "BEANX_FOO")))
  @AssociationOverrides(value = @AssociationOverride(name = "crossRefChild", joinColumns = @JoinColumn(name = "BEANX_CROSSREFCHILD")))
  @Column(name = "BEANX")
  @Valid
  private DtoTestBean beanx;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    try {
      // Dispose all the composition references.
      if (this.containmentChilds != null) {
        for (DtoTestChildContainment dtoTestChildContainment : this.containmentChilds) {
          dtoTestChildContainment.dispose();
        }
        this.containmentChilds = null;
      }
      
    }
    finally {
      super.dispose();
    }
    
  }
  
  /**
   * @return Returns the string property or <code>null</code> if not present.
   */
  public String getString() {
    checkDisposed();
    return this.string;
  }
  
  /**
   * Sets the string property to this instance.
   */
  public void setString(final String string) {
    checkDisposed();
    this.string = string;
  }
  
  /**
   * @return Returns an unmodifiable list of containmentChilds.
   */
  public List<DtoTestChildContainment> getContainmentChilds() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetContainmentChilds());
  }
  
  /**
   * Sets the given containmentChilds to the object. Currently contained containmentChilds instances will be removed.
   * 
   * @param containmentChilds the list of new instances
   */
  public void setContainmentChilds(final List<DtoTestChildContainment> containmentChilds) {
    // remove the old dtoTestChildContainment
    for(DtoTestChildContainment oldElement : new ArrayList<DtoTestChildContainment>(this.internalGetContainmentChilds())){
      removeFromContainmentChilds(oldElement);
    }
    
    // add the new dtoTestChildContainment
    for(DtoTestChildContainment newElement : containmentChilds){
      addToContainmentChilds(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>DtoTestChildContainment</code>s thereby lazy initializing it.
   */
  public List<DtoTestChildContainment> internalGetContainmentChilds() {
    if (this.containmentChilds == null) {
      this.containmentChilds = new ArrayList<DtoTestChildContainment>();
    }
    return this.containmentChilds;
  }
  
  /**
   * Adds the given dtoTestChildContainment to this object. <p>
   * Since the reference is a composition reference, the opposite reference (DtoTestChildContainment.container)
   * of the dtoTestChildContainment will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link DtoTestChildContainment#setContainer(DtoTestChildContainment)}.
   * 
   */
  public void addToContainmentChilds(final DtoTestChildContainment dtoTestChildContainment) {
    checkDisposed();
    dtoTestChildContainment.setContainer(this);
  }
  
  /**
   * Removes the given dtoTestChildContainment from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (DtoTestChildContainment.container)
   * of the dtoTestChildContainment will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link DtoTestChildContainment#setContainer(DtoTestChildContainment)}.
   * 
   */
  public void removeFromContainmentChilds(final DtoTestChildContainment dtoTestChildContainment) {
    checkDisposed();
    dtoTestChildContainment.setContainer(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToContainmentChilds(final DtoTestChildContainment dtoTestChildContainment) {
    if(dtoTestChildContainment == null) {
    	return;
    }
    
    		internalGetContainmentChilds().add(dtoTestChildContainment);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromContainmentChilds(final DtoTestChildContainment dtoTestChildContainment) {
    internalGetContainmentChilds().remove(dtoTestChildContainment);
  }
  
  /**
   * @return Returns an unmodifiable list of crossRefChilds.
   */
  public List<DtoTestChildCrossRef> getCrossRefChilds() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetCrossRefChilds());
  }
  
  /**
   * Sets the given crossRefChilds to the object. Currently contained crossRefChilds instances will be removed.
   * 
   * @param crossRefChilds the list of new instances
   */
  public void setCrossRefChilds(final List<DtoTestChildCrossRef> crossRefChilds) {
    // remove the old dtoTestChildCrossRef
    for(DtoTestChildCrossRef oldElement : new ArrayList<DtoTestChildCrossRef>(this.internalGetCrossRefChilds())){
      removeFromCrossRefChilds(oldElement);
    }
    
    // add the new dtoTestChildCrossRef
    for(DtoTestChildCrossRef newElement : crossRefChilds){
      addToCrossRefChilds(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>DtoTestChildCrossRef</code>s thereby lazy initializing it.
   */
  public List<DtoTestChildCrossRef> internalGetCrossRefChilds() {
    if (this.crossRefChilds == null) {
      this.crossRefChilds = new ArrayList<DtoTestChildCrossRef>();
    }
    return this.crossRefChilds;
  }
  
  /**
   * Adds the given dtoTestChildCrossRef to this object. <p>
   * Since the reference is a composition reference, the opposite reference (DtoTestChildCrossRef.container)
   * of the dtoTestChildCrossRef will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link DtoTestChildCrossRef#setContainer(DtoTestChildCrossRef)}.
   * 
   */
  public void addToCrossRefChilds(final DtoTestChildCrossRef dtoTestChildCrossRef) {
    checkDisposed();
    dtoTestChildCrossRef.setContainer(this);
  }
  
  /**
   * Removes the given dtoTestChildCrossRef from this object. <p>
   * 
   */
  public void removeFromCrossRefChilds(final DtoTestChildCrossRef dtoTestChildCrossRef) {
    checkDisposed();
    dtoTestChildCrossRef.setContainer(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToCrossRefChilds(final DtoTestChildCrossRef dtoTestChildCrossRef) {
    if(dtoTestChildCrossRef == null) {
    	return;
    }
    
    		internalGetCrossRefChilds().add(dtoTestChildCrossRef);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromCrossRefChilds(final DtoTestChildCrossRef dtoTestChildCrossRef) {
    internalGetCrossRefChilds().remove(dtoTestChildCrossRef);
  }
  
  /**
   * @return Returns the crossRefChild property or <code>null</code> if not present.
   */
  public DtoTestChildCrossRef getCrossRefChild() {
    checkDisposed();
    return this.crossRefChild;
  }
  
  /**
   * Sets the crossRefChild property to this instance.
   */
  public void setCrossRefChild(final DtoTestChildCrossRef crossRefChild) {
    checkDisposed();
    this.crossRefChild = crossRefChild;
  }
  
  /**
   * @return Returns the beanx property or <code>null</code> if not present.
   */
  public DtoTestBean getBeanx() {
    checkDisposed();
    return this.beanx;
  }
  
  /**
   * Sets the beanx property to this instance.
   */
  public void setBeanx(final DtoTestBean beanx) {
    checkDisposed();
    this.beanx = beanx;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    // remove the crossRefChilds
    for(DtoTestChildCrossRef oldElement : new ArrayList<DtoTestChildCrossRef>(this.internalGetCrossRefChilds())){
      removeFromCrossRefChilds(oldElement);
    }
  }
}
