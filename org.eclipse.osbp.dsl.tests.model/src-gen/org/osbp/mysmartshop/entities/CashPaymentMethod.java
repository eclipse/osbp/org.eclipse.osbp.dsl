/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainDescription;
import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.CashPayment;

@Entity
@Table(name = "CASH_PAYMENT_METHOD")
@SuppressWarnings("all")
public class CashPaymentMethod extends BaseUUID implements IEntity {
  @DomainKey
  @Column(name = "NUM")
  private String num;
  
  @DomainDescription
  @Column(name = "NAME")
  private String name;
  
  @Column(name = "CREDIT")
  private Boolean credit;
  
  @Column(name = "IMAGE_NAME")
  private String imageName;
  
  @Column(name = "LOWER_LIMIT")
  private double lowerLimit;
  
  @JoinColumn(name = "PAYMENTS_ID")
  @OneToMany(mappedBy = "methodOfPayment")
  private List<CashPayment> payments;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the num property or <code>null</code> if not present.
   */
  public String getNum() {
    checkDisposed();
    return this.num;
  }
  
  /**
   * Sets the num property to this instance.
   */
  public void setNum(final String num) {
    checkDisposed();
    this.num = num;
  }
  
  /**
   * @return Returns the name property or <code>null</code> if not present.
   */
  public String getName() {
    checkDisposed();
    return this.name;
  }
  
  /**
   * Sets the name property to this instance.
   */
  public void setName(final String name) {
    checkDisposed();
    this.name = name;
  }
  
  /**
   * @return Returns the credit property or <code>null</code> if not present.
   */
  public Boolean getCredit() {
    checkDisposed();
    return this.credit;
  }
  
  /**
   * Sets the credit property to this instance.
   */
  public void setCredit(final Boolean credit) {
    checkDisposed();
    this.credit = credit;
  }
  
  /**
   * @return Returns the imageName property or <code>null</code> if not present.
   */
  public String getImageName() {
    checkDisposed();
    return this.imageName;
  }
  
  /**
   * Sets the imageName property to this instance.
   */
  public void setImageName(final String imageName) {
    checkDisposed();
    this.imageName = imageName;
  }
  
  /**
   * @return Returns the lowerLimit property or <code>null</code> if not present.
   */
  public double getLowerLimit() {
    checkDisposed();
    return this.lowerLimit;
  }
  
  /**
   * Sets the lowerLimit property to this instance.
   */
  public void setLowerLimit(final double lowerLimit) {
    checkDisposed();
    this.lowerLimit = lowerLimit;
  }
  
  /**
   * @return Returns an unmodifiable list of payments.
   */
  public List<CashPayment> getPayments() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetPayments());
  }
  
  /**
   * Sets the given payments to the object. Currently contained payments instances will be removed.
   * 
   * @param payments the list of new instances
   */
  public void setPayments(final List<CashPayment> payments) {
    // remove the old cashPayment
    for(CashPayment oldElement : new ArrayList<CashPayment>(this.internalGetPayments())){
      removeFromPayments(oldElement);
    }
    
    // add the new cashPayment
    for(CashPayment newElement : payments){
      addToPayments(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>CashPayment</code>s thereby lazy initializing it.
   */
  public List<CashPayment> internalGetPayments() {
    if (this.payments == null) {
      this.payments = new ArrayList<CashPayment>();
    }
    return this.payments;
  }
  
  /**
   * Adds the given cashPayment to this object. <p>
   * Since the reference is a composition reference, the opposite reference (CashPayment.methodOfPayment)
   * of the cashPayment will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link CashPayment#setMethodOfPayment(CashPayment)}.
   * 
   */
  public void addToPayments(final CashPayment cashPayment) {
    checkDisposed();
    cashPayment.setMethodOfPayment(this);
  }
  
  /**
   * Removes the given cashPayment from this object. <p>
   * 
   */
  public void removeFromPayments(final CashPayment cashPayment) {
    checkDisposed();
    cashPayment.setMethodOfPayment(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToPayments(final CashPayment cashPayment) {
    if(cashPayment == null) {
    	return;
    }
    
    		internalGetPayments().add(cashPayment);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromPayments(final CashPayment cashPayment) {
    internalGetPayments().remove(cashPayment);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    // remove the payments
    for(CashPayment oldElement : new ArrayList<CashPayment>(this.internalGetPayments())){
      removeFromPayments(oldElement);
    }
  }
}
