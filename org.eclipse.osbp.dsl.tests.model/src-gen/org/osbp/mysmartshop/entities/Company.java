/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.osbp.mysmartshop.entities.Address;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.CompanyGroup;
import org.osbp.mysmartshop.entities.CompanyRelationType;
import org.osbp.mysmartshop.entities.Department;

/**
 * a company
 */
@Entity
@Table(name = "COMPANY")
@SuppressWarnings("all")
public class Company extends BaseUUID implements IEntity {
  /**
   * the company group if any
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "COMPANY_GROUP_ID")
  private CompanyGroup company_group;
  
  /**
   * name of the company
   */
  @DomainKey
  @Column(name = "NAME")
  private String name;
  
  /**
   * more detailed description with usable information for the PIM owner
   */
  @Column(name = "DESCRIPTION")
  private String description;
  
  /**
   * relation of the company to the PIM owner
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "RELATION_TYPE_ID")
  private CompanyRelationType relation_type;
  
  /**
   * any number of addresses
   */
  @JoinColumn(name = "ADDRESS_ID")
  @OneToMany(mappedBy = "company", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<Address> address;
  
  /**
   * departments of this company
   */
  @JoinColumn(name = "DEPARTMENTS_ID")
  @OneToMany(mappedBy = "company", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<Department> departments;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    try {
      // Dispose all the composition references.
      if (this.address != null) {
        for (Address address : this.address) {
          address.dispose();
        }
        this.address = null;
      }
      if (this.departments != null) {
        for (Department department : this.departments) {
          department.dispose();
        }
        this.departments = null;
      }
      
    }
    finally {
      super.dispose();
    }
    
  }
  
  /**
   * @return Returns the company_group property or <code>null</code> if not present.
   */
  public CompanyGroup getCompany_group() {
    checkDisposed();
    return this.company_group;
  }
  
  /**
   * Sets the company_group property to this instance.
   * Since the reference is a container reference, the opposite reference (CompanyGroup.companies)
   * of the company_group will be handled automatically and no further coding is required to keep them in sync.
   * See {@link CompanyGroup#setCompanies(CompanyGroup)}.
   */
  public void setCompany_group(final CompanyGroup company_group) {
    checkDisposed();
    if (this.company_group != null) {
      this.company_group.internalRemoveFromCompanies(this);
    }
    internalSetCompany_group(company_group);
    if (this.company_group != null) {
      this.company_group.internalAddToCompanies(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetCompany_group(final CompanyGroup company_group) {
    this.company_group = company_group;
  }
  
  /**
   * @return Returns the name property or <code>null</code> if not present.
   */
  public String getName() {
    checkDisposed();
    return this.name;
  }
  
  /**
   * Sets the name property to this instance.
   */
  public void setName(final String name) {
    checkDisposed();
    this.name = name;
  }
  
  /**
   * @return Returns the description property or <code>null</code> if not present.
   */
  public String getDescription() {
    checkDisposed();
    return this.description;
  }
  
  /**
   * Sets the description property to this instance.
   */
  public void setDescription(final String description) {
    checkDisposed();
    this.description = description;
  }
  
  /**
   * @return Returns the relation_type property or <code>null</code> if not present.
   */
  public CompanyRelationType getRelation_type() {
    checkDisposed();
    return this.relation_type;
  }
  
  /**
   * Sets the relation_type property to this instance.
   */
  public void setRelation_type(final CompanyRelationType relation_type) {
    checkDisposed();
    this.relation_type = relation_type;
  }
  
  /**
   * @return Returns an unmodifiable list of address.
   */
  public List<Address> getAddress() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetAddress());
  }
  
  /**
   * Sets the given address to the object. Currently contained address instances will be removed.
   * 
   * @param address the list of new instances
   */
  public void setAddress(final List<Address> address) {
    // remove the old address
    for(Address oldElement : new ArrayList<Address>(this.internalGetAddress())){
      removeFromAddress(oldElement);
    }
    
    // add the new address
    for(Address newElement : address){
      addToAddress(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>Address</code>s thereby lazy initializing it.
   */
  public List<Address> internalGetAddress() {
    if (this.address == null) {
      this.address = new ArrayList<Address>();
    }
    return this.address;
  }
  
  /**
   * Adds the given address to this object. <p>
   * Since the reference is a composition reference, the opposite reference (Address.company)
   * of the address will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Address#setCompany(Address)}.
   * 
   */
  public void addToAddress(final Address address) {
    checkDisposed();
    address.setCompany(this);
  }
  
  /**
   * Removes the given address from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (Address.company)
   * of the address will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Address#setCompany(Address)}.
   * 
   */
  public void removeFromAddress(final Address address) {
    checkDisposed();
    address.setCompany(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToAddress(final Address address) {
    if(address == null) {
    	return;
    }
    
    		internalGetAddress().add(address);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromAddress(final Address address) {
    internalGetAddress().remove(address);
  }
  
  /**
   * @return Returns an unmodifiable list of departments.
   */
  public List<Department> getDepartments() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetDepartments());
  }
  
  /**
   * Sets the given departments to the object. Currently contained departments instances will be removed.
   * 
   * @param departments the list of new instances
   */
  public void setDepartments(final List<Department> departments) {
    // remove the old department
    for(Department oldElement : new ArrayList<Department>(this.internalGetDepartments())){
      removeFromDepartments(oldElement);
    }
    
    // add the new department
    for(Department newElement : departments){
      addToDepartments(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>Department</code>s thereby lazy initializing it.
   */
  public List<Department> internalGetDepartments() {
    if (this.departments == null) {
      this.departments = new ArrayList<Department>();
    }
    return this.departments;
  }
  
  /**
   * Adds the given department to this object. <p>
   * Since the reference is a composition reference, the opposite reference (Department.company)
   * of the department will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Department#setCompany(Department)}.
   * 
   */
  public void addToDepartments(final Department department) {
    checkDisposed();
    department.setCompany(this);
  }
  
  /**
   * Removes the given department from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (Department.company)
   * of the department will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Department#setCompany(Department)}.
   * 
   */
  public void removeFromDepartments(final Department department) {
    checkDisposed();
    department.setCompany(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToDepartments(final Department department) {
    if(department == null) {
    	return;
    }
    
    		internalGetDepartments().add(department);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromDepartments(final Department department) {
    internalGetDepartments().remove(department);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
