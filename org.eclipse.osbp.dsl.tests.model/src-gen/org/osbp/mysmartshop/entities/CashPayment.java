/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.Properties;
import org.eclipse.osbp.runtime.common.annotations.Property;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.CashDrawerSum;
import org.osbp.mysmartshop.entities.CashPaymentMethod;
import org.osbp.mysmartshop.entities.CashSlip;

@Entity
@Table(name = "CASH_PAYMENT")
@SuppressWarnings("all")
public class CashPayment extends BaseUUID implements IEntity {
  @Column(name = "NOW")
  @Temporal(value = TemporalType.TIMESTAMP)
  @Valid
  private Date now;
  
  @Column(name = "PAYED")
  @Properties(properties = @Property(key = "decimalformat", value = "###,##0.00 &curren"))
  private Double payed;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "SLIP_ID")
  private CashSlip slip;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "METHOD_OF_PAYMENT_ID")
  private CashPaymentMethod methodOfPayment;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CLOSE_ID")
  private CashDrawerSum close;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the now property or <code>null</code> if not present.
   */
  public Date getNow() {
    checkDisposed();
    return this.now;
  }
  
  /**
   * Sets the now property to this instance.
   */
  public void setNow(final Date now) {
    checkDisposed();
    this.now = now;
  }
  
  /**
   * @return Returns the payed property or <code>null</code> if not present.
   */
  public Double getPayed() {
    checkDisposed();
    return this.payed;
  }
  
  /**
   * Sets the payed property to this instance.
   */
  public void setPayed(final Double payed) {
    checkDisposed();
    this.payed = payed;
  }
  
  /**
   * @return Returns the slip property or <code>null</code> if not present.
   */
  public CashSlip getSlip() {
    checkDisposed();
    return this.slip;
  }
  
  /**
   * Sets the slip property to this instance.
   * Since the reference is a container reference, the opposite reference (CashSlip.payments)
   * of the slip will be handled automatically and no further coding is required to keep them in sync.
   * See {@link CashSlip#setPayments(CashSlip)}.
   */
  public void setSlip(final CashSlip slip) {
    checkDisposed();
    if (this.slip != null) {
      this.slip.internalRemoveFromPayments(this);
    }
    internalSetSlip(slip);
    if (this.slip != null) {
      this.slip.internalAddToPayments(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetSlip(final CashSlip slip) {
    this.slip = slip;
  }
  
  /**
   * @return Returns the methodOfPayment property or <code>null</code> if not present.
   */
  public CashPaymentMethod getMethodOfPayment() {
    checkDisposed();
    return this.methodOfPayment;
  }
  
  /**
   * Sets the methodOfPayment property to this instance.
   * Since the reference is a container reference, the opposite reference (CashPaymentMethod.payments)
   * of the methodOfPayment will be handled automatically and no further coding is required to keep them in sync.
   * See {@link CashPaymentMethod#setPayments(CashPaymentMethod)}.
   */
  public void setMethodOfPayment(final CashPaymentMethod methodOfPayment) {
    checkDisposed();
    if (this.methodOfPayment != null) {
      this.methodOfPayment.internalRemoveFromPayments(this);
    }
    internalSetMethodOfPayment(methodOfPayment);
    if (this.methodOfPayment != null) {
      this.methodOfPayment.internalAddToPayments(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetMethodOfPayment(final CashPaymentMethod methodOfPayment) {
    this.methodOfPayment = methodOfPayment;
  }
  
  /**
   * @return Returns the close property or <code>null</code> if not present.
   */
  public CashDrawerSum getClose() {
    checkDisposed();
    return this.close;
  }
  
  /**
   * Sets the close property to this instance.
   * Since the reference is a container reference, the opposite reference (CashDrawerSum.payments)
   * of the close will be handled automatically and no further coding is required to keep them in sync.
   * See {@link CashDrawerSum#setPayments(CashDrawerSum)}.
   */
  public void setClose(final CashDrawerSum close) {
    checkDisposed();
    if (this.close != null) {
      this.close.internalRemoveFromPayments(this);
    }
    internalSetClose(close);
    if (this.close != null) {
      this.close.internalAddToPayments(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetClose(final CashDrawerSum close) {
    this.close = close;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
