/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import org.eclipse.osbp.dsl.common.datatypes.IBean;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.osbp.mysmartshop.entities.DtoTestChildCrossRef;

@Embeddable
@SuppressWarnings("all")
public class DtoTestBean implements Serializable, IBean {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @Basic
  private String foo;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "crossRefChild")
  private DtoTestChildCrossRef crossRefChild;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    this.dirty = dirty;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the foo property or <code>null</code> if not present.
   */
  public String getFoo() {
    checkDisposed();
    return this.foo;
  }
  
  /**
   * Sets the foo property to this instance.
   */
  public void setFoo(final String foo) {
    checkDisposed();
    this.foo = foo;
  }
  
  /**
   * @return Returns the crossRefChild property or <code>null</code> if not present.
   */
  public DtoTestChildCrossRef getCrossRefChild() {
    checkDisposed();
    return this.crossRefChild;
  }
  
  /**
   * Sets the crossRefChild property to this instance.
   */
  public void setCrossRefChild(final DtoTestChildCrossRef crossRefChild) {
    checkDisposed();
    this.crossRefChild = crossRefChild;
  }
}
