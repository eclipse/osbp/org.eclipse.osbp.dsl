/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.CashDrawerCurrency;
import org.osbp.mysmartshop.entities.CashPayment;

@Entity
@Table(name = "CASH_DRAWER_SUM")
@SuppressWarnings("all")
public class CashDrawerSum extends BaseUUID implements IEntity {
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "DRAWER_ID")
  private CashDrawerCurrency drawer;
  
  @JoinColumn(name = "PAYMENTS_ID")
  @OneToMany(mappedBy = "close")
  private List<CashPayment> payments;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the drawer property or <code>null</code> if not present.
   */
  public CashDrawerCurrency getDrawer() {
    checkDisposed();
    return this.drawer;
  }
  
  /**
   * Sets the drawer property to this instance.
   * Since the reference is a container reference, the opposite reference (CashDrawerCurrency.sums)
   * of the drawer will be handled automatically and no further coding is required to keep them in sync.
   * See {@link CashDrawerCurrency#setSums(CashDrawerCurrency)}.
   */
  public void setDrawer(final CashDrawerCurrency drawer) {
    checkDisposed();
    if (this.drawer != null) {
      this.drawer.internalRemoveFromSums(this);
    }
    internalSetDrawer(drawer);
    if (this.drawer != null) {
      this.drawer.internalAddToSums(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetDrawer(final CashDrawerCurrency drawer) {
    this.drawer = drawer;
  }
  
  /**
   * @return Returns an unmodifiable list of payments.
   */
  public List<CashPayment> getPayments() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetPayments());
  }
  
  /**
   * Sets the given payments to the object. Currently contained payments instances will be removed.
   * 
   * @param payments the list of new instances
   */
  public void setPayments(final List<CashPayment> payments) {
    // remove the old cashPayment
    for(CashPayment oldElement : new ArrayList<CashPayment>(this.internalGetPayments())){
      removeFromPayments(oldElement);
    }
    
    // add the new cashPayment
    for(CashPayment newElement : payments){
      addToPayments(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>CashPayment</code>s thereby lazy initializing it.
   */
  public List<CashPayment> internalGetPayments() {
    if (this.payments == null) {
      this.payments = new ArrayList<CashPayment>();
    }
    return this.payments;
  }
  
  /**
   * Adds the given cashPayment to this object. <p>
   * Since the reference is a composition reference, the opposite reference (CashPayment.close)
   * of the cashPayment will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link CashPayment#setClose(CashPayment)}.
   * 
   */
  public void addToPayments(final CashPayment cashPayment) {
    checkDisposed();
    cashPayment.setClose(this);
  }
  
  /**
   * Removes the given cashPayment from this object. <p>
   * 
   */
  public void removeFromPayments(final CashPayment cashPayment) {
    checkDisposed();
    cashPayment.setClose(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToPayments(final CashPayment cashPayment) {
    if(cashPayment == null) {
    	return;
    }
    
    		internalGetPayments().add(cashPayment);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromPayments(final CashPayment cashPayment) {
    internalGetPayments().remove(cashPayment);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    // remove the payments
    for(CashPayment oldElement : new ArrayList<CashPayment>(this.internalGetPayments())){
      removeFromPayments(oldElement);
    }
  }
}
