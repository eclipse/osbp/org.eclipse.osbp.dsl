/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.CashSlip;

@Entity
@Table(name = "CASH_REGISTER")
@Cacheable
@SuppressWarnings("all")
public class CashRegister extends BaseUUID implements IEntity {
  @DomainKey
  @Column(name = "NUM")
  private String num;
  
  @Column(name = "IP")
  private String ip;
  
  @Column(name = "LOCATION")
  private String location;
  
  @Column(name = "CURRENT_DAY")
  private String currentDay;
  
  @JoinColumn(name = "SLIPS_ID")
  @OneToMany(mappedBy = "register")
  private List<CashSlip> slips;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the num property or <code>null</code> if not present.
   */
  public String getNum() {
    checkDisposed();
    return this.num;
  }
  
  /**
   * Sets the num property to this instance.
   */
  public void setNum(final String num) {
    checkDisposed();
    this.num = num;
  }
  
  /**
   * @return Returns the ip property or <code>null</code> if not present.
   */
  public String getIp() {
    checkDisposed();
    return this.ip;
  }
  
  /**
   * Sets the ip property to this instance.
   */
  public void setIp(final String ip) {
    checkDisposed();
    this.ip = ip;
  }
  
  /**
   * @return Returns the location property or <code>null</code> if not present.
   */
  public String getLocation() {
    checkDisposed();
    return this.location;
  }
  
  /**
   * Sets the location property to this instance.
   */
  public void setLocation(final String location) {
    checkDisposed();
    this.location = location;
  }
  
  /**
   * @return Returns the currentDay property or <code>null</code> if not present.
   */
  public String getCurrentDay() {
    checkDisposed();
    return this.currentDay;
  }
  
  /**
   * Sets the currentDay property to this instance.
   */
  public void setCurrentDay(final String currentDay) {
    checkDisposed();
    this.currentDay = currentDay;
  }
  
  /**
   * @return Returns an unmodifiable list of slips.
   */
  public List<CashSlip> getSlips() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetSlips());
  }
  
  /**
   * Sets the given slips to the object. Currently contained slips instances will be removed.
   * 
   * @param slips the list of new instances
   */
  public void setSlips(final List<CashSlip> slips) {
    // remove the old cashSlip
    for(CashSlip oldElement : new ArrayList<CashSlip>(this.internalGetSlips())){
      removeFromSlips(oldElement);
    }
    
    // add the new cashSlip
    for(CashSlip newElement : slips){
      addToSlips(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>CashSlip</code>s thereby lazy initializing it.
   */
  public List<CashSlip> internalGetSlips() {
    if (this.slips == null) {
      this.slips = new ArrayList<CashSlip>();
    }
    return this.slips;
  }
  
  /**
   * Adds the given cashSlip to this object. <p>
   * Since the reference is a composition reference, the opposite reference (CashSlip.register)
   * of the cashSlip will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link CashSlip#setRegister(CashSlip)}.
   * 
   */
  public void addToSlips(final CashSlip cashSlip) {
    checkDisposed();
    cashSlip.setRegister(this);
  }
  
  /**
   * Removes the given cashSlip from this object. <p>
   * 
   */
  public void removeFromSlips(final CashSlip cashSlip) {
    checkDisposed();
    cashSlip.setRegister(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToSlips(final CashSlip cashSlip) {
    if(cashSlip == null) {
    	return;
    }
    
    		internalGetSlips().add(cashSlip);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromSlips(final CashSlip cashSlip) {
    internalGetSlips().remove(cashSlip);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    // remove the slips
    for(CashSlip oldElement : new ArrayList<CashSlip>(this.internalGetSlips())){
      removeFromSlips(oldElement);
    }
  }
}
