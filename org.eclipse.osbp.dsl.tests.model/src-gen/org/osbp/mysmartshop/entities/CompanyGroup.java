/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.Company;

/**
 * a company group consisting of a group of companies, from one is defined as the main company
 */
@Entity
@Table(name = "COMPANY_GROUP")
@SuppressWarnings("all")
public class CompanyGroup extends BaseUUID implements IEntity {
  /**
   * name of the company group
   */
  @DomainKey
  @Column(name = "NAME")
  private String name;
  
  /**
   * more detailed description with usable information for the PIM owner
   */
  @Column(name = "DESCRIPTION")
  private String description;
  
  /**
   * the main company
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "MAIN_COMPANY_ID")
  private Company main_company;
  
  /**
   * group of companies
   */
  @JoinColumn(name = "COMPANIES_ID")
  @OneToMany(mappedBy = "company_group", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<Company> companies;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    try {
      // Dispose all the composition references.
      if (this.companies != null) {
        for (Company company : this.companies) {
          company.dispose();
        }
        this.companies = null;
      }
      
    }
    finally {
      super.dispose();
    }
    
  }
  
  /**
   * @return Returns the name property or <code>null</code> if not present.
   */
  public String getName() {
    checkDisposed();
    return this.name;
  }
  
  /**
   * Sets the name property to this instance.
   */
  public void setName(final String name) {
    checkDisposed();
    this.name = name;
  }
  
  /**
   * @return Returns the description property or <code>null</code> if not present.
   */
  public String getDescription() {
    checkDisposed();
    return this.description;
  }
  
  /**
   * Sets the description property to this instance.
   */
  public void setDescription(final String description) {
    checkDisposed();
    this.description = description;
  }
  
  /**
   * @return Returns the main_company property or <code>null</code> if not present.
   */
  public Company getMain_company() {
    checkDisposed();
    return this.main_company;
  }
  
  /**
   * Sets the main_company property to this instance.
   */
  public void setMain_company(final Company main_company) {
    checkDisposed();
    this.main_company = main_company;
  }
  
  /**
   * @return Returns an unmodifiable list of companies.
   */
  public List<Company> getCompanies() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetCompanies());
  }
  
  /**
   * Sets the given companies to the object. Currently contained companies instances will be removed.
   * 
   * @param companies the list of new instances
   */
  public void setCompanies(final List<Company> companies) {
    // remove the old company
    for(Company oldElement : new ArrayList<Company>(this.internalGetCompanies())){
      removeFromCompanies(oldElement);
    }
    
    // add the new company
    for(Company newElement : companies){
      addToCompanies(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>Company</code>s thereby lazy initializing it.
   */
  public List<Company> internalGetCompanies() {
    if (this.companies == null) {
      this.companies = new ArrayList<Company>();
    }
    return this.companies;
  }
  
  /**
   * Adds the given company to this object. <p>
   * Since the reference is a composition reference, the opposite reference (Company.company_group)
   * of the company will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Company#setCompany_group(Company)}.
   * 
   */
  public void addToCompanies(final Company company) {
    checkDisposed();
    company.setCompany_group(this);
  }
  
  /**
   * Removes the given company from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (Company.company_group)
   * of the company will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Company#setCompany_group(Company)}.
   * 
   */
  public void removeFromCompanies(final Company company) {
    checkDisposed();
    company.setCompany_group(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToCompanies(final Company company) {
    if(company == null) {
    	return;
    }
    
    		internalGetCompanies().add(company);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromCompanies(final Company company) {
    internalGetCompanies().remove(company);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
