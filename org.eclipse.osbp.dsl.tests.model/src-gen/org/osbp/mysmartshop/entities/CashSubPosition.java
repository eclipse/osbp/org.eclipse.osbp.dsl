/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.Properties;
import org.eclipse.osbp.runtime.common.annotations.Property;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.CashPosition;

@Entity
@Table(name = "CASH_SUB_POSITION")
@SuppressWarnings("all")
public class CashSubPosition extends BaseUUID implements IEntity {
  @Column(name = "QUANTITY")
  private double quantity;
  
  @Column(name = "PRICE")
  @Properties(properties = @Property(key = "decimalformat", value = "###,##0.00 &curren"))
  private Double price;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PARENT_ID")
  private CashPosition parent;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the quantity property or <code>null</code> if not present.
   */
  public double getQuantity() {
    checkDisposed();
    return this.quantity;
  }
  
  /**
   * Sets the quantity property to this instance.
   */
  public void setQuantity(final double quantity) {
    checkDisposed();
    this.quantity = quantity;
  }
  
  /**
   * @return Returns the price property or <code>null</code> if not present.
   */
  public Double getPrice() {
    checkDisposed();
    return this.price;
  }
  
  /**
   * Sets the price property to this instance.
   */
  public void setPrice(final Double price) {
    checkDisposed();
    this.price = price;
  }
  
  /**
   * @return Returns the parent property or <code>null</code> if not present.
   */
  public CashPosition getParent() {
    checkDisposed();
    return this.parent;
  }
  
  /**
   * Sets the parent property to this instance.
   * Since the reference is a container reference, the opposite reference (CashPosition.subPositions)
   * of the parent will be handled automatically and no further coding is required to keep them in sync.
   * See {@link CashPosition#setSubPositions(CashPosition)}.
   */
  public void setParent(final CashPosition parent) {
    checkDisposed();
    if (this.parent != null) {
      this.parent.internalRemoveFromSubPositions(this);
    }
    internalSetParent(parent);
    if (this.parent != null) {
      this.parent.internalAddToSubPositions(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetParent(final CashPosition parent) {
    this.parent = parent;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
