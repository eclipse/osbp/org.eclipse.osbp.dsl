/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.osbp.mysmartshop.entities.Address;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.Company;

/**
 * a companies department
 */
@Entity
@Table(name = "DEPARTMENT")
@SuppressWarnings("all")
public class Department extends BaseUUID implements IEntity {
  /**
   * the company
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "COMPANY_ID")
  private Company company;
  
  /**
   * name of the department
   */
  @DomainKey
  @Column(name = "NAME")
  private String name;
  
  /**
   * more detailed description with usable information for the PIM owner
   */
  @Column(name = "DESCRIPTION")
  private String description;
  
  /**
   * any number of addresses
   */
  @JoinColumn(name = "ADDRESS_ID")
  @OneToMany(mappedBy = "department", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<Address> address;
  
  /**
   * any number of employees
   */
  @Column(name = "DEFAULT_YEARLY_INCOME")
  private double default_yearly_income;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    try {
      // Dispose all the composition references.
      if (this.address != null) {
        for (Address address : this.address) {
          address.dispose();
        }
        this.address = null;
      }
      
    }
    finally {
      super.dispose();
    }
    
  }
  
  /**
   * @return Returns the company property or <code>null</code> if not present.
   */
  public Company getCompany() {
    checkDisposed();
    return this.company;
  }
  
  /**
   * Sets the company property to this instance.
   * Since the reference is a container reference, the opposite reference (Company.departments)
   * of the company will be handled automatically and no further coding is required to keep them in sync.
   * See {@link Company#setDepartments(Company)}.
   */
  public void setCompany(final Company company) {
    checkDisposed();
    if (this.company != null) {
      this.company.internalRemoveFromDepartments(this);
    }
    internalSetCompany(company);
    if (this.company != null) {
      this.company.internalAddToDepartments(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetCompany(final Company company) {
    this.company = company;
  }
  
  /**
   * @return Returns the name property or <code>null</code> if not present.
   */
  public String getName() {
    checkDisposed();
    return this.name;
  }
  
  /**
   * Sets the name property to this instance.
   */
  public void setName(final String name) {
    checkDisposed();
    this.name = name;
  }
  
  /**
   * @return Returns the description property or <code>null</code> if not present.
   */
  public String getDescription() {
    checkDisposed();
    return this.description;
  }
  
  /**
   * Sets the description property to this instance.
   */
  public void setDescription(final String description) {
    checkDisposed();
    this.description = description;
  }
  
  /**
   * @return Returns an unmodifiable list of address.
   */
  public List<Address> getAddress() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetAddress());
  }
  
  /**
   * Sets the given address to the object. Currently contained address instances will be removed.
   * 
   * @param address the list of new instances
   */
  public void setAddress(final List<Address> address) {
    // remove the old address
    for(Address oldElement : new ArrayList<Address>(this.internalGetAddress())){
      removeFromAddress(oldElement);
    }
    
    // add the new address
    for(Address newElement : address){
      addToAddress(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>Address</code>s thereby lazy initializing it.
   */
  public List<Address> internalGetAddress() {
    if (this.address == null) {
      this.address = new ArrayList<Address>();
    }
    return this.address;
  }
  
  /**
   * Adds the given address to this object. <p>
   * Since the reference is a composition reference, the opposite reference (Address.department)
   * of the address will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Address#setDepartment(Address)}.
   * 
   */
  public void addToAddress(final Address address) {
    checkDisposed();
    address.setDepartment(this);
  }
  
  /**
   * Removes the given address from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (Address.department)
   * of the address will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Address#setDepartment(Address)}.
   * 
   */
  public void removeFromAddress(final Address address) {
    checkDisposed();
    address.setDepartment(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToAddress(final Address address) {
    if(address == null) {
    	return;
    }
    
    		internalGetAddress().add(address);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromAddress(final Address address) {
    internalGetAddress().remove(address);
  }
  
  /**
   * @return Returns the default_yearly_income property or <code>null</code> if not present.
   */
  public double getDefault_yearly_income() {
    checkDisposed();
    return this.default_yearly_income;
  }
  
  /**
   * Sets the default_yearly_income property to this instance.
   */
  public void setDefault_yearly_income(final double default_yearly_income) {
    checkDisposed();
    this.default_yearly_income = default_yearly_income;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
