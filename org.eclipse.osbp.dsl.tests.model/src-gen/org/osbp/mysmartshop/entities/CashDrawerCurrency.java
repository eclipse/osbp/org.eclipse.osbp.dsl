/**
 * Copyright (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 *  This source was created by OSBP Softwarefactory Wizard!
 * 
 *  OSBP is (C) - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany
 * 
 * ================================================================
 * 
 *  @file           $HeadURL$
 *  @version        $Revision$
 *  @date           $Date$
 *  @author         $Author$
 */
package org.osbp.mysmartshop.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.CashDrawerSum;

@Entity
@Table(name = "CASH_DRAWER_CURRENCY")
@SuppressWarnings("all")
public class CashDrawerCurrency extends BaseUUID implements IEntity {
  @JoinColumn(name = "SUMS_ID")
  @OneToMany(mappedBy = "drawer", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<CashDrawerSum> sums;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    try {
      // Dispose all the composition references.
      if (this.sums != null) {
        for (CashDrawerSum cashDrawerSum : this.sums) {
          cashDrawerSum.dispose();
        }
        this.sums = null;
      }
      
    }
    finally {
      super.dispose();
    }
    
  }
  
  /**
   * @return Returns an unmodifiable list of sums.
   */
  public List<CashDrawerSum> getSums() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetSums());
  }
  
  /**
   * Sets the given sums to the object. Currently contained sums instances will be removed.
   * 
   * @param sums the list of new instances
   */
  public void setSums(final List<CashDrawerSum> sums) {
    // remove the old cashDrawerSum
    for(CashDrawerSum oldElement : new ArrayList<CashDrawerSum>(this.internalGetSums())){
      removeFromSums(oldElement);
    }
    
    // add the new cashDrawerSum
    for(CashDrawerSum newElement : sums){
      addToSums(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>CashDrawerSum</code>s thereby lazy initializing it.
   */
  public List<CashDrawerSum> internalGetSums() {
    if (this.sums == null) {
      this.sums = new ArrayList<CashDrawerSum>();
    }
    return this.sums;
  }
  
  /**
   * Adds the given cashDrawerSum to this object. <p>
   * Since the reference is a composition reference, the opposite reference (CashDrawerSum.drawer)
   * of the cashDrawerSum will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link CashDrawerSum#setDrawer(CashDrawerSum)}.
   * 
   */
  public void addToSums(final CashDrawerSum cashDrawerSum) {
    checkDisposed();
    cashDrawerSum.setDrawer(this);
  }
  
  /**
   * Removes the given cashDrawerSum from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (CashDrawerSum.drawer)
   * of the cashDrawerSum will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link CashDrawerSum#setDrawer(CashDrawerSum)}.
   * 
   */
  public void removeFromSums(final CashDrawerSum cashDrawerSum) {
    checkDisposed();
    cashDrawerSum.setDrawer(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToSums(final CashDrawerSum cashDrawerSum) {
    if(cashDrawerSum == null) {
    	return;
    }
    
    		internalGetSums().add(cashDrawerSum);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromSums(final CashDrawerSum cashDrawerSum) {
    internalGetSums().remove(cashDrawerSum);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
