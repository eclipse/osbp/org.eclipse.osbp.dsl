/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.osbp.mysmartshop.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.DtoTestParent;

@Entity
@Table(name = "DTO_TEST_CHILD_CONTAINMENT")
@SuppressWarnings("all")
public class DtoTestChildContainment extends BaseUUID implements IEntity {
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CONTAINER_ID")
  private DtoTestParent container;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the container property or <code>null</code> if not present.
   */
  public DtoTestParent getContainer() {
    checkDisposed();
    return this.container;
  }
  
  /**
   * Sets the container property to this instance.
   * Since the reference is a container reference, the opposite reference (DtoTestParent.containmentChilds)
   * of the container will be handled automatically and no further coding is required to keep them in sync.
   * See {@link DtoTestParent#setContainmentChilds(DtoTestParent)}.
   */
  public void setContainer(final DtoTestParent container) {
    checkDisposed();
    if (this.container != null) {
      this.container.internalRemoveFromContainmentChilds(this);
    }
    internalSetContainer(container);
    if (this.container != null) {
      this.container.internalAddToContainmentChilds(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetContainer(final DtoTestParent container) {
    this.container = container;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
