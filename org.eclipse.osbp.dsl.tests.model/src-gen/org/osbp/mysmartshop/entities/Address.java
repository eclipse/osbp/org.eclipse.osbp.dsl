/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.osbp.mysmartshop.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.osbp.mysmartshop.entities.BaseUUID;
import org.osbp.mysmartshop.entities.Company;
import org.osbp.mysmartshop.entities.Department;

/**
 * an address
 */
@Entity
@Table(name = "ADDRESS")
@SuppressWarnings("all")
public class Address extends BaseUUID implements IEntity {
  /**
   * if address for a company, the company is given
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "COMPANY_ID")
  private Company company;
  
  /**
   * if address for a department, the address is given
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "DEPARTMENT_ID")
  private Department department;
  
  /**
   * address field 1
   */
  @Column(name = "ADDRESS1")
  private String address1;
  
  /**
   * address field 2
   */
  @Column(name = "ADDRESS2")
  private String address2;
  
  /**
   * city
   */
  @Column(name = "CITY")
  private String city;
  
  /**
   * state province
   */
  @Column(name = "STATE_PROVINCE")
  private String state_province;
  
  /**
   * postal code
   */
  @Column(name = "POSTAL_CODE")
  private String postal_code;
  
  /**
   * country
   */
  @Column(name = "COUNTRY")
  private String country;
  
  /**
   * email
   */
  @Column(name = "EMAIL")
  private String email;
  
  /**
   * landline (Festnetznummer)
   */
  @Column(name = "LANDLINE")
  private String landline;
  
  /**
   * mobile
   */
  @Column(name = "MOBILE")
  private String mobile;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the company property or <code>null</code> if not present.
   */
  public Company getCompany() {
    checkDisposed();
    return this.company;
  }
  
  /**
   * Sets the company property to this instance.
   * Since the reference is a container reference, the opposite reference (Company.address)
   * of the company will be handled automatically and no further coding is required to keep them in sync.
   * See {@link Company#setAddress(Company)}.
   */
  public void setCompany(final Company company) {
    checkDisposed();
    if (this.company != null) {
      this.company.internalRemoveFromAddress(this);
    }
    internalSetCompany(company);
    if (this.company != null) {
      this.company.internalAddToAddress(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetCompany(final Company company) {
    this.company = company;
  }
  
  /**
   * @return Returns the department property or <code>null</code> if not present.
   */
  public Department getDepartment() {
    checkDisposed();
    return this.department;
  }
  
  /**
   * Sets the department property to this instance.
   * Since the reference is a container reference, the opposite reference (Department.address)
   * of the department will be handled automatically and no further coding is required to keep them in sync.
   * See {@link Department#setAddress(Department)}.
   */
  public void setDepartment(final Department department) {
    checkDisposed();
    if (this.department != null) {
      this.department.internalRemoveFromAddress(this);
    }
    internalSetDepartment(department);
    if (this.department != null) {
      this.department.internalAddToAddress(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetDepartment(final Department department) {
    this.department = department;
  }
  
  /**
   * @return Returns the address1 property or <code>null</code> if not present.
   */
  public String getAddress1() {
    checkDisposed();
    return this.address1;
  }
  
  /**
   * Sets the address1 property to this instance.
   */
  public void setAddress1(final String address1) {
    checkDisposed();
    this.address1 = address1;
  }
  
  /**
   * @return Returns the address2 property or <code>null</code> if not present.
   */
  public String getAddress2() {
    checkDisposed();
    return this.address2;
  }
  
  /**
   * Sets the address2 property to this instance.
   */
  public void setAddress2(final String address2) {
    checkDisposed();
    this.address2 = address2;
  }
  
  /**
   * @return Returns the city property or <code>null</code> if not present.
   */
  public String getCity() {
    checkDisposed();
    return this.city;
  }
  
  /**
   * Sets the city property to this instance.
   */
  public void setCity(final String city) {
    checkDisposed();
    this.city = city;
  }
  
  /**
   * @return Returns the state_province property or <code>null</code> if not present.
   */
  public String getState_province() {
    checkDisposed();
    return this.state_province;
  }
  
  /**
   * Sets the state_province property to this instance.
   */
  public void setState_province(final String state_province) {
    checkDisposed();
    this.state_province = state_province;
  }
  
  /**
   * @return Returns the postal_code property or <code>null</code> if not present.
   */
  public String getPostal_code() {
    checkDisposed();
    return this.postal_code;
  }
  
  /**
   * Sets the postal_code property to this instance.
   */
  public void setPostal_code(final String postal_code) {
    checkDisposed();
    this.postal_code = postal_code;
  }
  
  /**
   * @return Returns the country property or <code>null</code> if not present.
   */
  public String getCountry() {
    checkDisposed();
    return this.country;
  }
  
  /**
   * Sets the country property to this instance.
   */
  public void setCountry(final String country) {
    checkDisposed();
    this.country = country;
  }
  
  /**
   * @return Returns the email property or <code>null</code> if not present.
   */
  public String getEmail() {
    checkDisposed();
    return this.email;
  }
  
  /**
   * Sets the email property to this instance.
   */
  public void setEmail(final String email) {
    checkDisposed();
    this.email = email;
  }
  
  /**
   * @return Returns the landline property or <code>null</code> if not present.
   */
  public String getLandline() {
    checkDisposed();
    return this.landline;
  }
  
  /**
   * Sets the landline property to this instance.
   */
  public void setLandline(final String landline) {
    checkDisposed();
    this.landline = landline;
  }
  
  /**
   * @return Returns the mobile property or <code>null</code> if not present.
   */
  public String getMobile() {
    checkDisposed();
    return this.mobile;
  }
  
  /**
   * Sets the mobile property to this instance.
   */
  public void setMobile(final String mobile) {
    checkDisposed();
    this.mobile = mobile;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
