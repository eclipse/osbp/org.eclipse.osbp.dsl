package org.eclipse.osbp.dsl.tests.hist.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.dsl.tests.hist.dtos.HCountryDto;
import org.eclipse.osbp.dsl.tests.hist.entities.HCountry;

@SuppressWarnings("all")
public class HCountryDtoService extends AbstractDTOServiceWithMutablePersistence<HCountryDto, HCountry> {
  public HCountryDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<HCountryDto> getDtoClass() {
    return HCountryDto.class;
  }
  
  public Class<HCountry> getEntityClass() {
    return HCountry.class;
  }
  
  public Object getId(final HCountryDto dto) {
    return dto.getId();
  }
}
