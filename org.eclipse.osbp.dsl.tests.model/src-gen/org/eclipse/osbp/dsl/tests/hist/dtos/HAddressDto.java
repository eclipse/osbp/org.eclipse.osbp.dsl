package org.eclipse.osbp.dsl.tests.hist.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.HBaseUUIDHistorizedDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.HCountryDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.eclipse.osbp.runtime.common.annotations.HistorizedObject;

@HistorizedObject
@SuppressWarnings("all")
public class HAddressDto extends HBaseUUIDHistorizedDto implements IDto, Serializable, PropertyChangeListener {
  private String name;
  
  private String street;
  
  private String postalCode;
  
  @DomainReference
  @FilterDepth(depth = 0)
  private HCountryDto country;
  
  public HAddressDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link HAddress} to the dto {@link HAddressDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the name property or <code>null</code> if not present.
   */
  public String getName() {
    return this.name;
  }
  
  /**
   * Sets the <code>name</code> property to this instance.
   * 
   * @param name - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setName(final String name) {
    firePropertyChange("name", this.name, this.name = name );
  }
  
  /**
   * Returns the street property or <code>null</code> if not present.
   */
  public String getStreet() {
    return this.street;
  }
  
  /**
   * Sets the <code>street</code> property to this instance.
   * 
   * @param street - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setStreet(final String street) {
    firePropertyChange("street", this.street, this.street = street );
  }
  
  /**
   * Returns the postalCode property or <code>null</code> if not present.
   */
  public String getPostalCode() {
    return this.postalCode;
  }
  
  /**
   * Sets the <code>postalCode</code> property to this instance.
   * 
   * @param postalCode - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPostalCode(final String postalCode) {
    firePropertyChange("postalCode", this.postalCode, this.postalCode = postalCode );
  }
  
  /**
   * Returns the country property or <code>null</code> if not present.
   */
  public HCountryDto getCountry() {
    return this.country;
  }
  
  /**
   * Sets the <code>country</code> property to this instance.
   * 
   * @param country - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCountry(final HCountryDto country) {
    checkDisposed();
    firePropertyChange("country", this.country, this.country = country);
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
