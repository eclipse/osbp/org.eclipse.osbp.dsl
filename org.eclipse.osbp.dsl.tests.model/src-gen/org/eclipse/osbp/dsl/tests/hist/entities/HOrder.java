package org.eclipse.osbp.dsl.tests.hist.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.dsl.tests.hist.entities.HAddress;
import org.eclipse.osbp.dsl.tests.hist.entities.HBaseUUID;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

/**
 * ------------------------------------------------------
 * Historized tests for orders having different addresses.
 * ------------------------------------------------------
 */
@Entity
@Table(name = "H_ORDER")
@SuppressWarnings("all")
public class HOrder extends HBaseUUID implements IEntity {
  @Column(name = "ORDER_NUMBER")
  private String orderNumber;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns(value = { @JoinColumn(name = "DELIVERY_ADDRESS_ID_ID", referencedColumnName = "ID"), @JoinColumn(name = "DELIVERY_ADDRESS_ID_VALIDFROM", referencedColumnName = "VALIDFROM") })
  private HAddress deliveryAddress;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumns(value = { @JoinColumn(name = "INVOICE_ADDRESS_ID_ID", referencedColumnName = "ID"), @JoinColumn(name = "INVOICE_ADDRESS_ID_VALIDFROM", referencedColumnName = "VALIDFROM") })
  private HAddress invoiceAddress;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the orderNumber property or <code>null</code> if not present.
   */
  public String getOrderNumber() {
    checkDisposed();
    return this.orderNumber;
  }
  
  /**
   * Sets the orderNumber property to this instance.
   */
  public void setOrderNumber(final String orderNumber) {
    checkDisposed();
    this.orderNumber = orderNumber;
  }
  
  /**
   * @return Returns the deliveryAddress property or <code>null</code> if not present.
   */
  public HAddress getDeliveryAddress() {
    checkDisposed();
    return this.deliveryAddress;
  }
  
  /**
   * Sets the deliveryAddress property to this instance.
   */
  public void setDeliveryAddress(final HAddress deliveryAddress) {
    checkDisposed();
    this.deliveryAddress = deliveryAddress;
  }
  
  /**
   * @return Returns the invoiceAddress property or <code>null</code> if not present.
   */
  public HAddress getInvoiceAddress() {
    checkDisposed();
    return this.invoiceAddress;
  }
  
  /**
   * Sets the invoiceAddress property to this instance.
   */
  public void setInvoiceAddress(final HAddress invoiceAddress) {
    checkDisposed();
    this.invoiceAddress = invoiceAddress;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
