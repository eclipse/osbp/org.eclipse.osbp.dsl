package org.eclipse.osbp.dsl.tests.hist.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.dsl.tests.hist.dtos.HCountryDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.mapper.HBaseUUIDDtoMapper;
import org.eclipse.osbp.dsl.tests.hist.entities.HCountry;

/**
 * This class maps the dto {@link HCountryDto} to and from the entity {@link HCountry}.
 * 
 */
@SuppressWarnings("all")
public class HCountryDtoMapper<DTO extends HCountryDto, ENTITY extends HCountry> extends HBaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public HCountry createEntity() {
    return new HCountry();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public HCountryDto createDto() {
    return new HCountryDto();
  }
  
  /**
   * Maps the entity {@link HCountry} to the dto {@link HCountryDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final HCountryDto dto, final HCountry entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setIsoCode(toDto_isoCode(entity, context));
  }
  
  /**
   * Maps the dto {@link HCountryDto} to the entity {@link HCountry}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final HCountryDto dto, final HCountry entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setIsoCode(toEntity_isoCode(dto, entity, context));
  }
  
  /**
   * Maps the property isoCode from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_isoCode(final HCountry in, final MappingContext context) {
    return in.getIsoCode();
  }
  
  /**
   * Maps the property isoCode from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_isoCode(final HCountryDto in, final HCountry parentEntity, final MappingContext context) {
    return in.getIsoCode();
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(HCountryDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(HCountry.class, in);
  }
}
