package org.eclipse.osbp.dsl.tests.hist.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.dsl.tests.hist.entities.HBaseUUIDHistorized;
import org.eclipse.osbp.dsl.tests.hist.entities.HCountry;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@Entity
@Table(name = "H_ADDRESS")
@SuppressWarnings("all")
public class HAddress extends HBaseUUIDHistorized implements IEntity {
  @Column(name = "NAME")
  private String name;
  
  @Column(name = "STREET")
  private String street;
  
  @Column(name = "POSTAL_CODE")
  private String postalCode;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "COUNTRY_ID")
  private HCountry country;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the name property or <code>null</code> if not present.
   */
  public String getName() {
    checkDisposed();
    return this.name;
  }
  
  /**
   * Sets the name property to this instance.
   */
  public void setName(final String name) {
    checkDisposed();
    this.name = name;
  }
  
  /**
   * @return Returns the street property or <code>null</code> if not present.
   */
  public String getStreet() {
    checkDisposed();
    return this.street;
  }
  
  /**
   * Sets the street property to this instance.
   */
  public void setStreet(final String street) {
    checkDisposed();
    this.street = street;
  }
  
  /**
   * @return Returns the postalCode property or <code>null</code> if not present.
   */
  public String getPostalCode() {
    checkDisposed();
    return this.postalCode;
  }
  
  /**
   * Sets the postalCode property to this instance.
   */
  public void setPostalCode(final String postalCode) {
    checkDisposed();
    this.postalCode = postalCode;
  }
  
  /**
   * @return Returns the country property or <code>null</code> if not present.
   */
  public HCountry getCountry() {
    checkDisposed();
    return this.country;
  }
  
  /**
   * Sets the country property to this instance.
   */
  public void setCountry(final HCountry country) {
    checkDisposed();
    this.country = country;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
