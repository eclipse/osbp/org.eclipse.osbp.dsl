package org.eclipse.osbp.dsl.tests.hist.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.dsl.tests.hist.dtos.HAddressDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.HCountryDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.mapper.HBaseUUIDHistorizedDtoMapper;
import org.eclipse.osbp.dsl.tests.hist.entities.HAddress;
import org.eclipse.osbp.dsl.tests.hist.entities.HCountry;

/**
 * This class maps the dto {@link HAddressDto} to and from the entity {@link HAddress}.
 * 
 */
@SuppressWarnings("all")
public class HAddressDtoMapper<DTO extends HAddressDto, ENTITY extends HAddress> extends HBaseUUIDHistorizedDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public HAddress createEntity() {
    return new HAddress();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public HAddressDto createDto() {
    return new HAddressDto();
  }
  
  /**
   * Maps the entity {@link HAddress} to the dto {@link HAddressDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final HAddressDto dto, final HAddress entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setName(toDto_name(entity, context));
    dto.setStreet(toDto_street(entity, context));
    dto.setPostalCode(toDto_postalCode(entity, context));
    dto.setCountry(toDto_country(entity, context));
  }
  
  /**
   * Maps the dto {@link HAddressDto} to the entity {@link HAddress}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final HAddressDto dto, final HAddress entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setName(toEntity_name(dto, entity, context));
    entity.setStreet(toEntity_street(dto, entity, context));
    entity.setPostalCode(toEntity_postalCode(dto, entity, context));
    entity.setCountry(toEntity_country(dto, entity, context));
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_name(final HAddress in, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property name from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_name(final HAddressDto in, final HAddress parentEntity, final MappingContext context) {
    return in.getName();
  }
  
  /**
   * Maps the property street from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_street(final HAddress in, final MappingContext context) {
    return in.getStreet();
  }
  
  /**
   * Maps the property street from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_street(final HAddressDto in, final HAddress parentEntity, final MappingContext context) {
    return in.getStreet();
  }
  
  /**
   * Maps the property postalCode from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_postalCode(final HAddress in, final MappingContext context) {
    return in.getPostalCode();
  }
  
  /**
   * Maps the property postalCode from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_postalCode(final HAddressDto in, final HAddress parentEntity, final MappingContext context) {
    return in.getPostalCode();
  }
  
  /**
   * Maps the property country from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected HCountryDto toDto_country(final HAddress in, final MappingContext context) {
    if(in.getCountry() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<HCountryDto, HCountry> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<HCountryDto, HCountry>) getToDtoMapper(HCountryDto.class, in.getCountry().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	HCountryDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getCountry()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getCountry(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getCountry(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property country from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected HCountry toEntity_country(final HAddressDto in, final HAddress parentEntity, final MappingContext context) {
    if(in.getCountry() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<HCountryDto, HCountry> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<HCountryDto, HCountry>) getToEntityMapper(in.getCountry().getClass(), HCountry.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	HCountry entity = null;
    	entity = context.get(mapper.createEntityHash(in.getCountry()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (HCountry) context
    			.findEntityByEntityManager(HCountry.class, in.getCountry().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getCountry()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getCountry(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(HAddressDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(HAddress.class, in);
  }
}
