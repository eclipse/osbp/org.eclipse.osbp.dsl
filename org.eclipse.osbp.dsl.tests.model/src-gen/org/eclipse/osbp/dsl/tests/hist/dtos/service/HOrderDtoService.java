package org.eclipse.osbp.dsl.tests.hist.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.dsl.tests.hist.dtos.HOrderDto;
import org.eclipse.osbp.dsl.tests.hist.entities.HOrder;

@SuppressWarnings("all")
public class HOrderDtoService extends AbstractDTOServiceWithMutablePersistence<HOrderDto, HOrder> {
  public HOrderDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<HOrderDto> getDtoClass() {
    return HOrderDto.class;
  }
  
  public Class<HOrder> getEntityClass() {
    return HOrder.class;
  }
  
  public Object getId(final HOrderDto dto) {
    return dto.getId();
  }
}
