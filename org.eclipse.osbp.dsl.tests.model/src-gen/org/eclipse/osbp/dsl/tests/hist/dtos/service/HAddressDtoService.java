package org.eclipse.osbp.dsl.tests.hist.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.dsl.tests.hist.dtos.HAddressDto;
import org.eclipse.osbp.dsl.tests.hist.entities.HAddress;

@SuppressWarnings("all")
public class HAddressDtoService extends AbstractDTOServiceWithMutablePersistence<HAddressDto, HAddress> {
  public HAddressDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<HAddressDto> getDtoClass() {
    return HAddressDto.class;
  }
  
  public Class<HAddress> getEntityClass() {
    return HAddress.class;
  }
  
  public Object getId(final HAddressDto dto) {
    return dto.getId();
  }
}
