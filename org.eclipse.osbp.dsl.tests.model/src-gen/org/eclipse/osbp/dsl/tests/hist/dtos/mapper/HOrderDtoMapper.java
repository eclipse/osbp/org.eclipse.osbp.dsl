package org.eclipse.osbp.dsl.tests.hist.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.dsl.tests.hist.dtos.HAddressDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.HOrderDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.mapper.HBaseUUIDDtoMapper;
import org.eclipse.osbp.dsl.tests.hist.entities.HAddress;
import org.eclipse.osbp.dsl.tests.hist.entities.HOrder;

/**
 * This class maps the dto {@link HOrderDto} to and from the entity {@link HOrder}.
 * 
 */
@SuppressWarnings("all")
public class HOrderDtoMapper<DTO extends HOrderDto, ENTITY extends HOrder> extends HBaseUUIDDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public HOrder createEntity() {
    return new HOrder();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public HOrderDto createDto() {
    return new HOrderDto();
  }
  
  /**
   * Maps the entity {@link HOrder} to the dto {@link HOrderDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final HOrderDto dto, final HOrder entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setOrderNumber(toDto_orderNumber(entity, context));
    dto.setDeliveryAddress(toDto_deliveryAddress(entity, context));
    dto.setInvoiceAddress(toDto_invoiceAddress(entity, context));
  }
  
  /**
   * Maps the dto {@link HOrderDto} to the entity {@link HOrder}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final HOrderDto dto, final HOrder entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setOrderNumber(toEntity_orderNumber(dto, entity, context));
    entity.setDeliveryAddress(toEntity_deliveryAddress(dto, entity, context));
    entity.setInvoiceAddress(toEntity_invoiceAddress(dto, entity, context));
  }
  
  /**
   * Maps the property orderNumber from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_orderNumber(final HOrder in, final MappingContext context) {
    return in.getOrderNumber();
  }
  
  /**
   * Maps the property orderNumber from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_orderNumber(final HOrderDto in, final HOrder parentEntity, final MappingContext context) {
    return in.getOrderNumber();
  }
  
  /**
   * Maps the property deliveryAddress from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected HAddressDto toDto_deliveryAddress(final HOrder in, final MappingContext context) {
    if(in.getDeliveryAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<HAddressDto, HAddress> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<HAddressDto, HAddress>) getToDtoMapper(HAddressDto.class, in.getDeliveryAddress().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	HAddressDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getDeliveryAddress()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getDeliveryAddress(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getDeliveryAddress(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property deliveryAddress from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected HAddress toEntity_deliveryAddress(final HOrderDto in, final HOrder parentEntity, final MappingContext context) {
    if(in.getDeliveryAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<HAddressDto, HAddress> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<HAddressDto, HAddress>) getToEntityMapper(in.getDeliveryAddress().getClass(), HAddress.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	HAddress entity = null;
    	entity = context.get(mapper.createEntityHash(in.getDeliveryAddress()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (HAddress) context
    			.findEntityByEntityManager(HAddress.class, in.getDeliveryAddress().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getDeliveryAddress()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getDeliveryAddress(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property invoiceAddress from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected HAddressDto toDto_invoiceAddress(final HOrder in, final MappingContext context) {
    if(in.getInvoiceAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<HAddressDto, HAddress> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<HAddressDto, HAddress>) getToDtoMapper(HAddressDto.class, in.getInvoiceAddress().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	HAddressDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getInvoiceAddress()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getInvoiceAddress(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getInvoiceAddress(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property invoiceAddress from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected HAddress toEntity_invoiceAddress(final HOrderDto in, final HOrder parentEntity, final MappingContext context) {
    if(in.getInvoiceAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<HAddressDto, HAddress> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<HAddressDto, HAddress>) getToEntityMapper(in.getInvoiceAddress().getClass(), HAddress.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	HAddress entity = null;
    	entity = context.get(mapper.createEntityHash(in.getInvoiceAddress()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (HAddress) context
    			.findEntityByEntityManager(HAddress.class, in.getInvoiceAddress().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getInvoiceAddress()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getInvoiceAddress(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(HOrderDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(HOrder.class, in);
  }
}
