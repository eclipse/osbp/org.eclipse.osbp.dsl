package org.eclipse.osbp.dsl.tests.hist.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.HBaseUUIDDto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@SuppressWarnings("all")
public class HCountryDto extends HBaseUUIDDto implements IDto, Serializable, PropertyChangeListener {
  private String isoCode;
  
  public HCountryDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link HCountry} to the dto {@link HCountryDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the isoCode property or <code>null</code> if not present.
   */
  public String getIsoCode() {
    return this.isoCode;
  }
  
  /**
   * Sets the <code>isoCode</code> property to this instance.
   * 
   * @param isoCode - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setIsoCode(final String isoCode) {
    firePropertyChange("isoCode", this.isoCode, this.isoCode = isoCode );
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
