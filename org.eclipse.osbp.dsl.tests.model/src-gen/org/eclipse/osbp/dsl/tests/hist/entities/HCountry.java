package org.eclipse.osbp.dsl.tests.hist.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.dsl.tests.hist.entities.HBaseUUID;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@Entity
@Table(name = "H_COUNTRY")
@SuppressWarnings("all")
public class HCountry extends HBaseUUID implements IEntity {
  @Column(name = "ISO_CODE")
  private String isoCode;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the isoCode property or <code>null</code> if not present.
   */
  public String getIsoCode() {
    checkDisposed();
    return this.isoCode;
  }
  
  /**
   * Sets the isoCode property to this instance.
   */
  public void setIsoCode(final String isoCode) {
    checkDisposed();
    this.isoCode = isoCode;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
