package org.eclipse.osbp.dsl.tests.hist.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PreRemove;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.jpa.services.history.HistorizedQueryRedirector;
import org.eclipse.osbp.jpa.services.listener.EntityInfoListener;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.HistIsCurrent;
import org.eclipse.osbp.runtime.common.annotations.HistIsCustomVersion;
import org.eclipse.osbp.runtime.common.annotations.HistValidUntil;
import org.eclipse.osbp.runtime.common.annotations.HistorizedObject;
import org.eclipse.osbp.runtime.common.historized.UUIDHist;
import org.eclipse.persistence.annotations.QueryRedirectors;

@HistorizedObject
@QueryRedirectors(insert = HistorizedQueryRedirector.class, update = HistorizedQueryRedirector.class)
@MappedSuperclass
@EntityListeners(value = EntityInfoListener.class)
@SuppressWarnings("all")
public class HBaseUUIDHistorized implements IEntity {
  @Transient
  @Dispose
  private boolean disposed;
  
  @EmbeddedId
  @Column(name = "ID")
  @Valid
  private UUIDHist id;
  
  @HistValidUntil
  @Column(name = "VALID_UNTIL")
  private long validUntil;
  
  @HistIsCurrent
  @Column(name = "HIST_CURRENT")
  private boolean histCurrent;
  
  @HistIsCustomVersion
  @Column(name = "CUSTOM_VERSION")
  private boolean customVersion;
  
  @Version
  @Column(name = "VERSION")
  private int version;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the id property or <code>null</code> if not present.
   */
  public UUIDHist getId() {
    checkDisposed();
    return this.id;
  }
  
  /**
   * Sets the id property to this instance.
   */
  public void setId(final UUIDHist id) {
    checkDisposed();
    this.id = id;
  }
  
  /**
   * @return Returns the validUntil property or <code>null</code> if not present.
   */
  public long getValidUntil() {
    checkDisposed();
    return this.validUntil;
  }
  
  /**
   * Sets the validUntil property to this instance.
   */
  public void setValidUntil(final long validUntil) {
    checkDisposed();
    this.validUntil = validUntil;
  }
  
  /**
   * @return Returns the histCurrent property or <code>null</code> if not present.
   */
  public boolean getHistCurrent() {
    checkDisposed();
    return this.histCurrent;
  }
  
  /**
   * Sets the histCurrent property to this instance.
   */
  public void setHistCurrent(final boolean histCurrent) {
    checkDisposed();
    this.histCurrent = histCurrent;
  }
  
  /**
   * @return Returns the customVersion property or <code>null</code> if not present.
   */
  public boolean getCustomVersion() {
    checkDisposed();
    return this.customVersion;
  }
  
  /**
   * Sets the customVersion property to this instance.
   */
  public void setCustomVersion(final boolean customVersion) {
    checkDisposed();
    this.customVersion = customVersion;
  }
  
  /**
   * @return Returns the version property or <code>null</code> if not present.
   */
  public int getVersion() {
    checkDisposed();
    return this.version;
  }
  
  /**
   * Sets the version property to this instance.
   */
  public void setVersion(final int version) {
    checkDisposed();
    this.version = version;
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    HBaseUUIDHistorized other = (HBaseUUIDHistorized) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    if (other.version != this.version)
      return false;
    return true;
  }
  
  @Override
  public boolean equals(final Object obj) {
    return equalVersions(obj);
  }
  
  @Override
  public int hashCode() {
     int prime = 31;
    int result = 1;
    result = prime * result + ((this.id== null) ? 0 : this.id.hashCode());
    return result;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
