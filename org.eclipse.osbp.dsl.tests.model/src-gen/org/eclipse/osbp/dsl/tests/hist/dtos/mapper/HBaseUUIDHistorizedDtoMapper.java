package org.eclipse.osbp.dsl.tests.hist.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.dsl.tests.hist.dtos.HBaseUUIDHistorizedDto;
import org.eclipse.osbp.dsl.tests.hist.entities.HBaseUUIDHistorized;
import org.eclipse.osbp.runtime.common.historized.UUIDHist;

/**
 * This class maps the dto {@link HBaseUUIDHistorizedDto} to and from the entity {@link HBaseUUIDHistorized}.
 * 
 */
@SuppressWarnings("all")
public class HBaseUUIDHistorizedDtoMapper<DTO extends HBaseUUIDHistorizedDto, ENTITY extends HBaseUUIDHistorized> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public HBaseUUIDHistorized createEntity() {
    return new HBaseUUIDHistorized();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public HBaseUUIDHistorizedDto createDto() {
    return new HBaseUUIDHistorizedDto();
  }
  
  /**
   * Maps the entity {@link HBaseUUIDHistorized} to the dto {@link HBaseUUIDHistorizedDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final HBaseUUIDHistorizedDto dto, final HBaseUUIDHistorized entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    dto.setId(toDto_id(entity, context));
    dto.setValidUntil(toDto_validUntil(entity, context));
    dto.setHistCurrent(toDto_histCurrent(entity, context));
    dto.setCustomVersion(toDto_customVersion(entity, context));
    dto.setVersion(toDto_version(entity, context));
  }
  
  /**
   * Maps the dto {@link HBaseUUIDHistorizedDto} to the entity {@link HBaseUUIDHistorized}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final HBaseUUIDHistorizedDto dto, final HBaseUUIDHistorized entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    
    entity.setId(toEntity_id(dto, entity, context));
    entity.setValidUntil(toEntity_validUntil(dto, entity, context));
    entity.setHistCurrent(toEntity_histCurrent(dto, entity, context));
    entity.setCustomVersion(toEntity_customVersion(dto, entity, context));
    entity.setVersion(toEntity_version(dto, entity, context));
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected UUIDHist toDto_id(final HBaseUUIDHistorized in, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected UUIDHist toEntity_id(final HBaseUUIDHistorizedDto in, final HBaseUUIDHistorized parentEntity, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property validUntil from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected long toDto_validUntil(final HBaseUUIDHistorized in, final MappingContext context) {
    return in.getValidUntil();
  }
  
  /**
   * Maps the property validUntil from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected long toEntity_validUntil(final HBaseUUIDHistorizedDto in, final HBaseUUIDHistorized parentEntity, final MappingContext context) {
    return in.getValidUntil();
  }
  
  /**
   * Maps the property histCurrent from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_histCurrent(final HBaseUUIDHistorized in, final MappingContext context) {
    return in.getHistCurrent();
  }
  
  /**
   * Maps the property histCurrent from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_histCurrent(final HBaseUUIDHistorizedDto in, final HBaseUUIDHistorized parentEntity, final MappingContext context) {
    return in.getHistCurrent();
  }
  
  /**
   * Maps the property customVersion from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_customVersion(final HBaseUUIDHistorized in, final MappingContext context) {
    return in.getCustomVersion();
  }
  
  /**
   * Maps the property customVersion from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_customVersion(final HBaseUUIDHistorizedDto in, final HBaseUUIDHistorized parentEntity, final MappingContext context) {
    return in.getCustomVersion();
  }
  
  /**
   * Maps the property version from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toDto_version(final HBaseUUIDHistorized in, final MappingContext context) {
    return in.getVersion();
  }
  
  /**
   * Maps the property version from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toEntity_version(final HBaseUUIDHistorizedDto in, final HBaseUUIDHistorized parentEntity, final MappingContext context) {
    return in.getVersion();
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(HBaseUUIDHistorizedDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(HBaseUUIDHistorized.class, in);
  }
}
