/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.tests.carstore.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.BaseDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.CarDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.ToCycle1Dto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;

@SuppressWarnings("all")
public class ToCycle2Dto extends BaseDto implements IDto, Serializable, PropertyChangeListener {
  @DomainReference
  @FilterDepth(depth = 0)
  private ToCycle1Dto parent;
  
  @DomainReference
  @FilterDepth(depth = 0)
  private CarDto car;
  
  public ToCycle2Dto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link ToCycle2} to the dto {@link ToCycle2Dto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the parent property or <code>null</code> if not present.
   */
  public ToCycle1Dto getParent() {
    return this.parent;
  }
  
  /**
   * Sets the <code>parent</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>ToCycle1Dto#
   * cycles2</code> of the <code>parent</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link ToCycle1Dto#setCycles2(ToCycle1Dto)
   * 
   * @param parent - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setParent(final ToCycle1Dto parent) {
    checkDisposed();
    if (this.parent != null) {
    	this.parent.internalRemoveFromCycles2(this);
    }
    
    internalSetParent(parent);
    
    if (this.parent != null) {
    	this.parent.internalAddToCycles2(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetParent(final ToCycle1Dto parent) {
    firePropertyChange("parent", this.parent, this.parent = parent);
  }
  
  /**
   * Returns the car property or <code>null</code> if not present.
   */
  public CarDto getCar() {
    return this.car;
  }
  
  /**
   * Sets the <code>car</code> property to this instance.
   * 
   * @param car - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCar(final CarDto car) {
    checkDisposed();
    firePropertyChange("car", this.car, this.car = car);
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
