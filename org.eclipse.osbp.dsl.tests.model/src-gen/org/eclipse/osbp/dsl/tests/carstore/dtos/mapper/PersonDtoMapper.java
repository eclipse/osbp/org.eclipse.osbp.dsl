package org.eclipse.osbp.dsl.tests.carstore.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.dsl.tests.carstore.dtos.AddressDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.CarDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.PersonDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.mapper.BaseDtoMapper;
import org.eclipse.osbp.dsl.tests.carstore.entities.Address;
import org.eclipse.osbp.dsl.tests.carstore.entities.Car;
import org.eclipse.osbp.dsl.tests.carstore.entities.Person;

/**
 * This class maps the dto {@link PersonDto} to and from the entity {@link Person}.
 * 
 */
@SuppressWarnings("all")
public class PersonDtoMapper<DTO extends PersonDto, ENTITY extends Person> extends BaseDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public Person createEntity() {
    return new Person();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public PersonDto createDto() {
    return new PersonDto();
  }
  
  /**
   * Maps the entity {@link Person} to the dto {@link PersonDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final PersonDto dto, final Person entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setFirstname(toDto_firstname(entity, context));
    dto.setLastname(toDto_lastname(entity, context));
    dto.setHomeAddress(toDto_homeAddress(entity, context));
    dto.setWorkAddress(toDto_workAddress(entity, context));
  }
  
  /**
   * Maps the dto {@link PersonDto} to the entity {@link Person}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final PersonDto dto, final Person entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setFirstname(toEntity_firstname(dto, entity, context));
    entity.setLastname(toEntity_lastname(dto, entity, context));
    toEntity_ownsCars(dto, entity, context);
    entity.setHomeAddress(toEntity_homeAddress(dto, entity, context));
    entity.setWorkAddress(toEntity_workAddress(dto, entity, context));
  }
  
  /**
   * Maps the property firstname from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_firstname(final Person in, final MappingContext context) {
    return in.getFirstname();
  }
  
  /**
   * Maps the property firstname from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_firstname(final PersonDto in, final Person parentEntity, final MappingContext context) {
    return in.getFirstname();
  }
  
  /**
   * Maps the property lastname from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_lastname(final Person in, final MappingContext context) {
    return in.getLastname();
  }
  
  /**
   * Maps the property lastname from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_lastname(final PersonDto in, final Person parentEntity, final MappingContext context) {
    return in.getLastname();
  }
  
  /**
   * Maps the property ownsCars from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<CarDto> toDto_ownsCars(final Person in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property ownsCars from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<Car> toEntity_ownsCars(final PersonDto in, final Person parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<CarDto, Car> mapper = getToEntityMapper(CarDto.class, Car.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CarDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<CarDto>) in.internalGetOwnsCars();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToOwnsCars,
    		parentEntity::internalRemoveFromOwnsCars);
    return null;
  }
  
  /**
   * Maps the property homeAddress from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected AddressDto toDto_homeAddress(final Person in, final MappingContext context) {
    if(in.getHomeAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address>) getToDtoMapper(AddressDto.class, in.getHomeAddress().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	AddressDto dto = null;
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getHomeAddress(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property homeAddress from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Address toEntity_homeAddress(final PersonDto in, final Person parentEntity, final MappingContext context) {
    if(in.getHomeAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address>) getToEntityMapper(in.getHomeAddress().getClass(), Address.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	Address entity = mapper.createEntity();
    	mapper.mapToEntity(in.getHomeAddress(), entity, context);
    	return entity;							
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property workAddress from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected AddressDto toDto_workAddress(final Person in, final MappingContext context) {
    if(in.getWorkAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address>) getToDtoMapper(AddressDto.class, in.getWorkAddress().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	AddressDto dto = null;
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getWorkAddress(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property workAddress from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Address toEntity_workAddress(final PersonDto in, final Person parentEntity, final MappingContext context) {
    if(in.getWorkAddress() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<AddressDto, Address>) getToEntityMapper(in.getWorkAddress().getClass(), Address.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	Address entity = mapper.createEntity();
    	mapper.mapToEntity(in.getWorkAddress(), entity, context);
    	return entity;							
    } else {
    	return null;
    }
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(PersonDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(Person.class, in);
  }
}
