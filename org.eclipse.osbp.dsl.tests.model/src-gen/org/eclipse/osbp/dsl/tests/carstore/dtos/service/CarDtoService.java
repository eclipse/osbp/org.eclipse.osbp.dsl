package org.eclipse.osbp.dsl.tests.carstore.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.dsl.tests.carstore.dtos.CarDto;
import org.eclipse.osbp.dsl.tests.carstore.entities.Car;

@SuppressWarnings("all")
public class CarDtoService extends AbstractDTOServiceWithMutablePersistence<CarDto, Car> {
  public CarDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<CarDto> getDtoClass() {
    return CarDto.class;
  }
  
  public Class<Car> getEntityClass() {
    return Car.class;
  }
  
  public Object getId(final CarDto dto) {
    return dto.getUuid();
  }
}
