/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.tests.carstore.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.dsl.tests.carstore.entities.Addon;
import org.eclipse.osbp.dsl.tests.carstore.entities.Base;
import org.eclipse.osbp.dsl.tests.carstore.entities.Person;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle1;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@Entity
@Table(name = "CAR")
@SuppressWarnings("all")
public class Car extends Base implements IEntity {
  @Column(name = "NUMBER")
  private String number;
  
  @Column(name = "FINISHING_DATE")
  @Temporal(value = TemporalType.DATE)
  @Valid
  private Date finishingDate;
  
  @JoinColumn(name = "ADDONS_ID")
  @OneToMany(mappedBy = "car", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<Addon> addons;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "OWNER_ID")
  private Person owner;
  
  @JoinColumn(name = "CYCLES1_ID")
  @OneToMany(mappedBy = "car", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<ToCycle1> cycles1;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    try {
      // Dispose all the composition references.
      if (this.addons != null) {
        for (Addon addon : this.addons) {
          addon.dispose();
        }
        this.addons = null;
      }
      if (this.cycles1 != null) {
        for (ToCycle1 toCycle1 : this.cycles1) {
          toCycle1.dispose();
        }
        this.cycles1 = null;
      }
      
    }
    finally {
      super.dispose();
    }
    
  }
  
  /**
   * @return Returns the number property or <code>null</code> if not present.
   */
  public String getNumber() {
    checkDisposed();
    return this.number;
  }
  
  /**
   * Sets the number property to this instance.
   */
  public void setNumber(final String number) {
    checkDisposed();
    this.number = number;
  }
  
  /**
   * @return Returns the finishingDate property or <code>null</code> if not present.
   */
  public Date getFinishingDate() {
    checkDisposed();
    return this.finishingDate;
  }
  
  /**
   * Sets the finishingDate property to this instance.
   */
  public void setFinishingDate(final Date finishingDate) {
    checkDisposed();
    this.finishingDate = finishingDate;
  }
  
  /**
   * @return Returns an unmodifiable list of addons.
   */
  public List<Addon> getAddons() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetAddons());
  }
  
  /**
   * Sets the given addons to the object. Currently contained addons instances will be removed.
   * 
   * @param addons the list of new instances
   */
  public void setAddons(final List<Addon> addons) {
    // remove the old addon
    for(Addon oldElement : new ArrayList<Addon>(this.internalGetAddons())){
      removeFromAddons(oldElement);
    }
    
    // add the new addon
    for(Addon newElement : addons){
      addToAddons(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>Addon</code>s thereby lazy initializing it.
   */
  public List<Addon> internalGetAddons() {
    if (this.addons == null) {
      this.addons = new ArrayList<Addon>();
    }
    return this.addons;
  }
  
  /**
   * Adds the given addon to this object. <p>
   * Since the reference is a composition reference, the opposite reference (Addon.car)
   * of the addon will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Addon#setCar(Addon)}.
   * 
   */
  public void addToAddons(final Addon addon) {
    checkDisposed();
    addon.setCar(this);
  }
  
  /**
   * Removes the given addon from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (Addon.car)
   * of the addon will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Addon#setCar(Addon)}.
   * 
   */
  public void removeFromAddons(final Addon addon) {
    checkDisposed();
    addon.setCar(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToAddons(final Addon addon) {
    if(addon == null) {
    	return;
    }
    
    		internalGetAddons().add(addon);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromAddons(final Addon addon) {
    internalGetAddons().remove(addon);
  }
  
  /**
   * @return Returns the owner property or <code>null</code> if not present.
   */
  public Person getOwner() {
    checkDisposed();
    return this.owner;
  }
  
  /**
   * Sets the owner property to this instance.
   * Since the reference is a container reference, the opposite reference (Person.ownsCars)
   * of the owner will be handled automatically and no further coding is required to keep them in sync.
   * See {@link Person#setOwnsCars(Person)}.
   */
  public void setOwner(final Person owner) {
    checkDisposed();
    if (this.owner != null) {
      this.owner.internalRemoveFromOwnsCars(this);
    }
    internalSetOwner(owner);
    if (this.owner != null) {
      this.owner.internalAddToOwnsCars(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetOwner(final Person owner) {
    this.owner = owner;
  }
  
  /**
   * @return Returns an unmodifiable list of cycles1.
   */
  public List<ToCycle1> getCycles1() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetCycles1());
  }
  
  /**
   * Sets the given cycles1 to the object. Currently contained cycles1 instances will be removed.
   * 
   * @param cycles1 the list of new instances
   */
  public void setCycles1(final List<ToCycle1> cycles1) {
    // remove the old toCycle1
    for(ToCycle1 oldElement : new ArrayList<ToCycle1>(this.internalGetCycles1())){
      removeFromCycles1(oldElement);
    }
    
    // add the new toCycle1
    for(ToCycle1 newElement : cycles1){
      addToCycles1(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>ToCycle1</code>s thereby lazy initializing it.
   */
  public List<ToCycle1> internalGetCycles1() {
    if (this.cycles1 == null) {
      this.cycles1 = new ArrayList<ToCycle1>();
    }
    return this.cycles1;
  }
  
  /**
   * Adds the given toCycle1 to this object. <p>
   * Since the reference is a composition reference, the opposite reference (ToCycle1.car)
   * of the toCycle1 will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link ToCycle1#setCar(ToCycle1)}.
   * 
   */
  public void addToCycles1(final ToCycle1 toCycle1) {
    checkDisposed();
    toCycle1.setCar(this);
  }
  
  /**
   * Removes the given toCycle1 from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (ToCycle1.car)
   * of the toCycle1 will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link ToCycle1#setCar(ToCycle1)}.
   * 
   */
  public void removeFromCycles1(final ToCycle1 toCycle1) {
    checkDisposed();
    toCycle1.setCar(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToCycles1(final ToCycle1 toCycle1) {
    if(toCycle1 == null) {
    	return;
    }
    
    		internalGetCycles1().add(toCycle1);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromCycles1(final ToCycle1 toCycle1) {
    internalGetCycles1().remove(toCycle1);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
