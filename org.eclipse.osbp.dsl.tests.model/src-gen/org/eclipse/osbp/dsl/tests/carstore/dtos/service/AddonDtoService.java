package org.eclipse.osbp.dsl.tests.carstore.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.dsl.tests.carstore.dtos.AddonDto;
import org.eclipse.osbp.dsl.tests.carstore.entities.Addon;

@SuppressWarnings("all")
public class AddonDtoService extends AbstractDTOServiceWithMutablePersistence<AddonDto, Addon> {
  public AddonDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<AddonDto> getDtoClass() {
    return AddonDto.class;
  }
  
  public Class<Addon> getEntityClass() {
    return Addon.class;
  }
  
  public Object getId(final AddonDto dto) {
    return dto.getUuid();
  }
}
