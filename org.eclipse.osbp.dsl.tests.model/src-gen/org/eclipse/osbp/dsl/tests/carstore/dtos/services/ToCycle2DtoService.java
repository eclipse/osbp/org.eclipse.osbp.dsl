/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.tests.carstore.dtos.services;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;
import org.eclipse.osbp.dsl.tests.carstore.dtos.ToCycle2Dto;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle2;

@SuppressWarnings("all")
public class ToCycle2DtoService extends AbstractDTOService<ToCycle2Dto, ToCycle2> {
  public Class<ToCycle2Dto> getDtoClass() {
    return ToCycle2Dto.class;
  }
  
  public Class<ToCycle2> getEntityClass() {
    return ToCycle2.class;
  }
  
  public Object getId(final ToCycle2Dto dto) {
    return dto.getUuid();
  }
}
