package org.eclipse.osbp.dsl.tests.carstore.dtos.mapper;

import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.dsl.tests.carstore.dtos.AddonDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.CarDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.mapper.BaseDtoMapper;
import org.eclipse.osbp.dsl.tests.carstore.entities.Addon;
import org.eclipse.osbp.dsl.tests.carstore.entities.Car;

/**
 * This class maps the dto {@link AddonDto} to and from the entity {@link Addon}.
 * 
 */
@SuppressWarnings("all")
public class AddonDtoMapper<DTO extends AddonDto, ENTITY extends Addon> extends BaseDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public Addon createEntity() {
    return new Addon();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public AddonDto createDto() {
    return new AddonDto();
  }
  
  /**
   * Maps the entity {@link Addon} to the dto {@link AddonDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final AddonDto dto, final Addon entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setDescription(toDto_description(entity, context));
    dto.setCar(toDto_car(entity, context));
  }
  
  /**
   * Maps the dto {@link AddonDto} to the entity {@link Addon}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final AddonDto dto, final Addon entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setDescription(toEntity_description(dto, entity, context));
    entity.setCar(toEntity_car(dto, entity, context));
  }
  
  /**
   * Maps the property description from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_description(final Addon in, final MappingContext context) {
    return in.getDescription();
  }
  
  /**
   * Maps the property description from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_description(final AddonDto in, final Addon parentEntity, final MappingContext context) {
    return in.getDescription();
  }
  
  /**
   * Maps the property car from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected CarDto toDto_car(final Addon in, final MappingContext context) {
    if(in.getCar() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CarDto, Car> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CarDto, Car>) getToDtoMapper(CarDto.class, in.getCar().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	CarDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getCar()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getCar(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getCar(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property car from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected Car toEntity_car(final AddonDto in, final Addon parentEntity, final MappingContext context) {
    if(in.getCar() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CarDto, Car> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CarDto, Car>) getToEntityMapper(in.getCar().getClass(), Car.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	Car entity = null;
    	entity = context.get(mapper.createEntityHash(in.getCar()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (Car) context
    			.findEntityByEntityManager(Car.class, in.getCar().getUuid());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getCar()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getCar(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(AddonDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(Addon.class, in);
  }
}
