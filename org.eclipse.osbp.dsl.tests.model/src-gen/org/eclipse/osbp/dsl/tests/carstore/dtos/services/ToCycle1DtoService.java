/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.tests.carstore.dtos.services;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;
import org.eclipse.osbp.dsl.tests.carstore.dtos.ToCycle1Dto;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle1;

@SuppressWarnings("all")
public class ToCycle1DtoService extends AbstractDTOService<ToCycle1Dto, ToCycle1> {
  public Class<ToCycle1Dto> getDtoClass() {
    return ToCycle1Dto.class;
  }
  
  public Class<ToCycle1> getEntityClass() {
    return ToCycle1.class;
  }
  
  public Object getId(final ToCycle1Dto dto) {
    return dto.getUuid();
  }
}
