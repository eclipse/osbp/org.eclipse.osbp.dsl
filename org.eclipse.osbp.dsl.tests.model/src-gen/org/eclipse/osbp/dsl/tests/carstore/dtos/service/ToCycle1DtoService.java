package org.eclipse.osbp.dsl.tests.carstore.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.dsl.tests.carstore.dtos.ToCycle1Dto;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle1;

@SuppressWarnings("all")
public class ToCycle1DtoService extends AbstractDTOServiceWithMutablePersistence<ToCycle1Dto, ToCycle1> {
  public ToCycle1DtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<ToCycle1Dto> getDtoClass() {
    return ToCycle1Dto.class;
  }
  
  public Class<ToCycle1> getEntityClass() {
    return ToCycle1.class;
  }
  
  public Object getId(final ToCycle1Dto dto) {
    return dto.getUuid();
  }
}
