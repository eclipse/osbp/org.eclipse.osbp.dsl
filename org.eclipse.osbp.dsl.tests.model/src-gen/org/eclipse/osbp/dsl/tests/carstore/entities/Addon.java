/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.tests.carstore.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.dsl.tests.carstore.entities.Base;
import org.eclipse.osbp.dsl.tests.carstore.entities.Car;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@Entity
@Table(name = "ADDON")
@SuppressWarnings("all")
public class Addon extends Base implements IEntity {
  @Column(name = "DESCRIPTION")
  private String description;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CAR_ID")
  private Car car;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * @return Returns the description property or <code>null</code> if not present.
   */
  public String getDescription() {
    checkDisposed();
    return this.description;
  }
  
  /**
   * Sets the description property to this instance.
   */
  public void setDescription(final String description) {
    checkDisposed();
    this.description = description;
  }
  
  /**
   * @return Returns the car property or <code>null</code> if not present.
   */
  public Car getCar() {
    checkDisposed();
    return this.car;
  }
  
  /**
   * Sets the car property to this instance.
   * Since the reference is a container reference, the opposite reference (Car.addons)
   * of the car will be handled automatically and no further coding is required to keep them in sync.
   * See {@link Car#setAddons(Car)}.
   */
  public void setCar(final Car car) {
    checkDisposed();
    if (this.car != null) {
      this.car.internalRemoveFromAddons(this);
    }
    internalSetCar(car);
    if (this.car != null) {
      this.car.internalAddToAddons(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetCar(final Car car) {
    this.car = car;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
