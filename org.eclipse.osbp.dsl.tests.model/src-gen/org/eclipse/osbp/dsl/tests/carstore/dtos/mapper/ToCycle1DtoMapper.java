/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.tests.carstore.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.dsl.tests.carstore.dtos.CarDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.ToCycle1Dto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.ToCycle2Dto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.mapper.BaseDtoMapper;
import org.eclipse.osbp.dsl.tests.carstore.entities.Car;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle1;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle2;

/**
 * This class maps the dto {@link ToCycle1Dto} to and from the entity {@link ToCycle1}.
 * 
 */
@SuppressWarnings("all")
public class ToCycle1DtoMapper<DTO extends ToCycle1Dto, ENTITY extends ToCycle1> extends BaseDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public ToCycle1 createEntity() {
    return new ToCycle1();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public ToCycle1Dto createDto() {
    return new ToCycle1Dto();
  }
  
  /**
   * Maps the entity {@link ToCycle1} to the dto {@link ToCycle1Dto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final ToCycle1Dto dto, final ToCycle1 entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setCar(toDto_car(entity, context));
  }
  
  /**
   * Maps the dto {@link ToCycle1Dto} to the entity {@link ToCycle1}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final ToCycle1Dto dto, final ToCycle1 entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setCar(toEntity_car(dto, entity, context));
    toEntity_cycles2(dto, entity, context);
  }
  
  /**
   * Maps the property car from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected CarDto toDto_car(final ToCycle1 in, final MappingContext context) {
    if(in.getCar() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CarDto, Car> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CarDto, Car>) getToDtoMapper(CarDto.class, in.getCar().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	CarDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getCar()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getCar(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getCar(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property car from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected Car toEntity_car(final ToCycle1Dto in, final ToCycle1 parentEntity, final MappingContext context) {
    if(in.getCar() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<CarDto, Car> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<CarDto, Car>) getToEntityMapper(in.getCar().getClass(), Car.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	Car entity = null;
    	entity = context.get(mapper.createEntityHash(in.getCar()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (Car) context
    			.findEntityByEntityManager(Car.class, in.getCar().getUuid());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getCar()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getCar(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property cycles2 from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<ToCycle2Dto> toDto_cycles2(final ToCycle1 in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property cycles2 from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<ToCycle2> toEntity_cycles2(final ToCycle1Dto in, final ToCycle1 parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<ToCycle2Dto, ToCycle2> mapper = getToEntityMapper(ToCycle2Dto.class, ToCycle2.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<ToCycle2Dto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<ToCycle2Dto>) in.internalGetCycles2();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToCycles2,
    		parentEntity::internalRemoveFromCycles2);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(ToCycle1Dto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(ToCycle1.class, in);
  }
}
