package org.eclipse.osbp.dsl.tests.carstore.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.dsl.tests.carstore.dtos.PersonDto;
import org.eclipse.osbp.dsl.tests.carstore.entities.Person;

@SuppressWarnings("all")
public class PersonDtoService extends AbstractDTOServiceWithMutablePersistence<PersonDto, Person> {
  public PersonDtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<PersonDto> getDtoClass() {
    return PersonDto.class;
  }
  
  public Class<Person> getEntityClass() {
    return Person.class;
  }
  
  public Object getId(final PersonDto dto) {
    return dto.getUuid();
  }
}
