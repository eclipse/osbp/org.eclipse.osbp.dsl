package org.eclipse.osbp.dsl.tests.carstore.dtos;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.AddonDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.BaseDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.PersonDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.ToCycle1Dto;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;

@SuppressWarnings("all")
public class CarDto extends BaseDto implements IDto, Serializable, PropertyChangeListener {
  private String number;
  
  @Valid
  private Date finishingDate;
  
  @DomainReference
  @Valid
  @FilterDepth(depth = 0)
  private List<AddonDto> addons;
  
  @DomainReference
  @FilterDepth(depth = 0)
  private PersonDto owner;
  
  @DomainReference
  @Valid
  @FilterDepth(depth = 0)
  private List<ToCycle1Dto> cycles1;
  
  public CarDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link Car} to the dto {@link CarDto}.
   * 
   */
  protected void installLazyCollections() {
    super.installLazyCollections();
    addons = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(
    				org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),
    				AddonDto.class, "car.uuid",
    				(java.util.function.Supplier<Object> & Serializable) () -> this.getUuid(), this);
    cycles1 = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(
    				org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),
    				ToCycle1Dto.class, "car.uuid",
    				(java.util.function.Supplier<Object> & Serializable) () -> this.getUuid(), this);
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    super.dispose();
  }
  
  /**
   * Returns the number property or <code>null</code> if not present.
   */
  public String getNumber() {
    return this.number;
  }
  
  /**
   * Sets the <code>number</code> property to this instance.
   * 
   * @param number - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setNumber(final String number) {
    firePropertyChange("number", this.number, this.number = number );
  }
  
  /**
   * Returns the finishingDate property or <code>null</code> if not present.
   */
  public Date getFinishingDate() {
    return this.finishingDate;
  }
  
  /**
   * Sets the <code>finishingDate</code> property to this instance.
   * 
   * @param finishingDate - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setFinishingDate(final Date finishingDate) {
    firePropertyChange("finishingDate", this.finishingDate, this.finishingDate = finishingDate );
  }
  
  /**
   * Returns an unmodifiable list of addons.
   */
  public List<AddonDto> getAddons() {
    return Collections.unmodifiableList(internalGetAddons());
  }
  
  /**
   * Returns the list of <code>AddonDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<AddonDto> internalGetAddons() {
    if (this.addons == null) {
      this.addons = new java.util.ArrayList<AddonDto>();
    }
    return this.addons;
  }
  
  /**
   * Adds the given addonDto to this object. <p>
   * Since the reference is a composition reference, the opposite reference <code>AddonDto#car</code> of the <code>addonDto</code> will be handled automatically and no further coding is required to keep them in sync.<p>
   * See {@link AddonDto#setCar(AddonDto)}.
   * 
   * @param addonDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToAddons(final AddonDto addonDto) {
    checkDisposed();
    
    addonDto.setCar(this);
  }
  
  /**
   * Removes the given addonDto from this object. <p>
   * 
   * @param addonDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void removeFromAddons(final AddonDto addonDto) {
    checkDisposed();
    
    addonDto.setCar(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToAddons(final AddonDto addonDto) {
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<AddonDto> oldList = null;
    		if(internalGetAddons() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetAddons()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetAddons());
    		}
    		internalGetAddons().add(addonDto);
    		firePropertyChange("addons", oldList, internalGetAddons());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromAddons(final AddonDto addonDto) {
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<AddonDto> oldList = null;
    	if(internalGetAddons() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetAddons()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetAddons());
    	}
    	internalGetAddons().remove(addonDto);
    	firePropertyChange("addons", oldList, internalGetAddons());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetAddons().remove(addonDto);
    }
  }
  
  /**
   * Sets the <code>addons</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>AddonDto#
   * car</code> of the <code>addons</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link AddonDto#setCar(AddonDto)
   * 
   * @param addons - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setAddons(final List<AddonDto> addons) {
    checkDisposed();
    for (AddonDto dto : internalGetAddons().toArray(new AddonDto[this.addons.size()])) {
    	removeFromAddons(dto);
    }
    
    if(addons == null) {
    	return;
    }
    
    for (AddonDto dto : addons) {
    	addToAddons(dto);
    }
  }
  
  /**
   * Returns the owner property or <code>null</code> if not present.
   */
  public PersonDto getOwner() {
    return this.owner;
  }
  
  /**
   * Sets the <code>owner</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>PersonDto#
   * ownsCars</code> of the <code>owner</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link PersonDto#setOwnsCars(PersonDto)
   * 
   * @param owner - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setOwner(final PersonDto owner) {
    checkDisposed();
    if (this.owner != null) {
    	this.owner.internalRemoveFromOwnsCars(this);
    }
    
    internalSetOwner(owner);
    
    if (this.owner != null) {
    	this.owner.internalAddToOwnsCars(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetOwner(final PersonDto owner) {
    firePropertyChange("owner", this.owner, this.owner = owner);
  }
  
  /**
   * Returns an unmodifiable list of cycles1.
   */
  public List<ToCycle1Dto> getCycles1() {
    return Collections.unmodifiableList(internalGetCycles1());
  }
  
  /**
   * Returns the list of <code>ToCycle1Dto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<ToCycle1Dto> internalGetCycles1() {
    if (this.cycles1 == null) {
      this.cycles1 = new java.util.ArrayList<ToCycle1Dto>();
    }
    return this.cycles1;
  }
  
  /**
   * Adds the given toCycle1Dto to this object. <p>
   * Since the reference is a composition reference, the opposite reference <code>ToCycle1Dto#car</code> of the <code>toCycle1Dto</code> will be handled automatically and no further coding is required to keep them in sync.<p>
   * See {@link ToCycle1Dto#setCar(ToCycle1Dto)}.
   * 
   * @param toCycle1Dto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToCycles1(final ToCycle1Dto toCycle1Dto) {
    checkDisposed();
    
    toCycle1Dto.setCar(this);
  }
  
  /**
   * Removes the given toCycle1Dto from this object. <p>
   * 
   * @param toCycle1Dto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void removeFromCycles1(final ToCycle1Dto toCycle1Dto) {
    checkDisposed();
    
    toCycle1Dto.setCar(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToCycles1(final ToCycle1Dto toCycle1Dto) {
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<ToCycle1Dto> oldList = null;
    		if(internalGetCycles1() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetCycles1()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetCycles1());
    		}
    		internalGetCycles1().add(toCycle1Dto);
    		firePropertyChange("cycles1", oldList, internalGetCycles1());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromCycles1(final ToCycle1Dto toCycle1Dto) {
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<ToCycle1Dto> oldList = null;
    	if(internalGetCycles1() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetCycles1()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetCycles1());
    	}
    	internalGetCycles1().remove(toCycle1Dto);
    	firePropertyChange("cycles1", oldList, internalGetCycles1());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetCycles1().remove(toCycle1Dto);
    }
  }
  
  /**
   * Sets the <code>cycles1</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>ToCycle1Dto#
   * car</code> of the <code>cycles1</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link ToCycle1Dto#setCar(ToCycle1Dto)
   * 
   * @param cycles1 - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCycles1(final List<ToCycle1Dto> cycles1) {
    checkDisposed();
    for (ToCycle1Dto dto : internalGetCycles1().toArray(new ToCycle1Dto[this.cycles1.size()])) {
    	removeFromCycles1(dto);
    }
    
    if(cycles1 == null) {
    	return;
    }
    
    for (ToCycle1Dto dto : cycles1) {
    	addToCycles1(dto);
    }
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	super.propertyChange(event);
    }
  }
}
