/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.tests.carstore.dtos.services;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;
import org.eclipse.osbp.dsl.tests.carstore.dtos.PersonDto;
import org.eclipse.osbp.dsl.tests.carstore.entities.Person;

@SuppressWarnings("all")
public class PersonDtoService extends AbstractDTOService<PersonDto, Person> {
  public Class<PersonDto> getDtoClass() {
    return PersonDto.class;
  }
  
  public Class<Person> getEntityClass() {
    return Person.class;
  }
  
  public Object getId(final PersonDto dto) {
    return dto.getUuid();
  }
}
