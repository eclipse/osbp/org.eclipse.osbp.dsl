package org.eclipse.osbp.dsl.tests.carstore.dtos.mapper;

import java.util.Date;
import java.util.List;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.dsl.tests.carstore.dtos.AddonDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.CarDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.PersonDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.ToCycle1Dto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.mapper.BaseDtoMapper;
import org.eclipse.osbp.dsl.tests.carstore.entities.Addon;
import org.eclipse.osbp.dsl.tests.carstore.entities.Car;
import org.eclipse.osbp.dsl.tests.carstore.entities.Person;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle1;

/**
 * This class maps the dto {@link CarDto} to and from the entity {@link Car}.
 * 
 */
@SuppressWarnings("all")
public class CarDtoMapper<DTO extends CarDto, ENTITY extends Car> extends BaseDtoMapper<DTO, ENTITY> {
  /**
   * Creates a new instance of the entity
   */
  public Car createEntity() {
    return new Car();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public CarDto createDto() {
    return new CarDto();
  }
  
  /**
   * Maps the entity {@link Car} to the dto {@link CarDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final CarDto dto, final Car entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    super.mapToDTO(dto, entity, context);
    
    dto.setNumber(toDto_number(entity, context));
    dto.setFinishingDate(toDto_finishingDate(entity, context));
    dto.setOwner(toDto_owner(entity, context));
  }
  
  /**
   * Maps the dto {@link CarDto} to the entity {@link Car}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final CarDto dto, final Car entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    super.mapToEntity(dto, entity, context);
    
    entity.setNumber(toEntity_number(dto, entity, context));
    entity.setFinishingDate(toEntity_finishingDate(dto, entity, context));
    toEntity_addons(dto, entity, context);
    entity.setOwner(toEntity_owner(dto, entity, context));
    toEntity_cycles1(dto, entity, context);
  }
  
  /**
   * Maps the property number from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_number(final Car in, final MappingContext context) {
    return in.getNumber();
  }
  
  /**
   * Maps the property number from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_number(final CarDto in, final Car parentEntity, final MappingContext context) {
    return in.getNumber();
  }
  
  /**
   * Maps the property finishingDate from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Date toDto_finishingDate(final Car in, final MappingContext context) {
    return in.getFinishingDate();
  }
  
  /**
   * Maps the property finishingDate from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected Date toEntity_finishingDate(final CarDto in, final Car parentEntity, final MappingContext context) {
    return in.getFinishingDate();
  }
  
  /**
   * Maps the property addons from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<AddonDto> toDto_addons(final Car in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property addons from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<Addon> toEntity_addons(final CarDto in, final Car parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<AddonDto, Addon> mapper = getToEntityMapper(AddonDto.class, Addon.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<AddonDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<AddonDto>) in.internalGetAddons();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToAddons,
    		parentEntity::internalRemoveFromAddons);
    return null;
  }
  
  /**
   * Maps the property owner from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected PersonDto toDto_owner(final Car in, final MappingContext context) {
    if(in.getOwner() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<PersonDto, Person> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<PersonDto, Person>) getToDtoMapper(PersonDto.class, in.getOwner().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	PersonDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getOwner()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getOwner(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getOwner(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property owner from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected Person toEntity_owner(final CarDto in, final Car parentEntity, final MappingContext context) {
    if(in.getOwner() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<PersonDto, Person> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<PersonDto, Person>) getToEntityMapper(in.getOwner().getClass(), Person.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	Person entity = null;
    	entity = context.get(mapper.createEntityHash(in.getOwner()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (Person) context
    			.findEntityByEntityManager(Person.class, in.getOwner().getUuid());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getOwner()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getOwner(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property cycles1 from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<ToCycle1Dto> toDto_cycles1(final Car in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property cycles1 from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<ToCycle1> toEntity_cycles1(final CarDto in, final Car parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<ToCycle1Dto, ToCycle1> mapper = getToEntityMapper(ToCycle1Dto.class, ToCycle1.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<ToCycle1Dto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<ToCycle1Dto>) in.internalGetCycles1();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToCycles1,
    		parentEntity::internalRemoveFromCycles1);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(CarDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(Car.class, in);
  }
}
