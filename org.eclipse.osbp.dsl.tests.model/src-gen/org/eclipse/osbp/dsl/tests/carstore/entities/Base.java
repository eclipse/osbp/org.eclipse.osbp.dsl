/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.tests.carstore.entities;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PreRemove;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.jpa.services.listener.EntityInfoListener;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@MappedSuperclass
@EntityListeners(value = EntityInfoListener.class)
@SuppressWarnings("all")
public class Base implements IEntity {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Id
  @Column(name = "UUID")
  private String uuid = java.util.UUID.randomUUID().toString();
  
  @Version
  @Column(name = "VERSION")
  private long version;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the uuid property or <code>null</code> if not present.
   */
  public String getUuid() {
    checkDisposed();
    return this.uuid;
  }
  
  /**
   * Sets the uuid property to this instance.
   */
  public void setUuid(final String uuid) {
    checkDisposed();
    this.uuid = uuid;
  }
  
  /**
   * @return Returns the version property or <code>null</code> if not present.
   */
  public long getVersion() {
    checkDisposed();
    return this.version;
  }
  
  /**
   * Sets the version property to this instance.
   */
  public void setVersion(final long version) {
    checkDisposed();
    this.version = version;
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Base other = (Base) obj;
    if (this.uuid == null) {
      if (other.uuid != null)
        return false;
    } else if (!this.uuid.equals(other.uuid))
      return false;
    if (other.version != this.version)
      return false;
    return true;
  }
  
  @Override
  public boolean equals(final Object obj) {
    return equalVersions(obj);
  }
  
  @Override
  public int hashCode() {
     int prime = 31;
    int result = 1;
    result = prime * result + ((this.uuid== null) ? 0 : this.uuid.hashCode());
    return result;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
