package org.eclipse.osbp.dsl.tests.carstore.dtos.service;

import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOServiceWithMutablePersistence;
import org.eclipse.osbp.dsl.tests.carstore.dtos.ToCycle2Dto;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle2;

@SuppressWarnings("all")
public class ToCycle2DtoService extends AbstractDTOServiceWithMutablePersistence<ToCycle2Dto, ToCycle2> {
  public ToCycle2DtoService() {
    // set the default persistence ID
    setPersistenceId("testCarstore");
  }
  
  public Class<ToCycle2Dto> getDtoClass() {
    return ToCycle2Dto.class;
  }
  
  public Class<ToCycle2> getEntityClass() {
    return ToCycle2.class;
  }
  
  public Object getId(final ToCycle2Dto dto) {
    return dto.getUuid();
  }
}
