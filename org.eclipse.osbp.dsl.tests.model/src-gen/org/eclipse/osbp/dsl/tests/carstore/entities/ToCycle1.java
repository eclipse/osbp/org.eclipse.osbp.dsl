/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.tests.carstore.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.dsl.tests.carstore.entities.Base;
import org.eclipse.osbp.dsl.tests.carstore.entities.Car;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle2;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@Entity
@Table(name = "TO_CYCLE1")
@SuppressWarnings("all")
public class ToCycle1 extends Base implements IEntity {
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CAR_ID")
  private Car car;
  
  @JoinColumn(name = "CYCLES2_ID")
  @OneToMany(mappedBy = "parent", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<ToCycle2> cycles2;
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    try {
      // Dispose all the composition references.
      if (this.cycles2 != null) {
        for (ToCycle2 toCycle2 : this.cycles2) {
          toCycle2.dispose();
        }
        this.cycles2 = null;
      }
      
    }
    finally {
      super.dispose();
    }
    
  }
  
  /**
   * @return Returns the car property or <code>null</code> if not present.
   */
  public Car getCar() {
    checkDisposed();
    return this.car;
  }
  
  /**
   * Sets the car property to this instance.
   * Since the reference is a container reference, the opposite reference (Car.cycles1)
   * of the car will be handled automatically and no further coding is required to keep them in sync.
   * See {@link Car#setCycles1(Car)}.
   */
  public void setCar(final Car car) {
    checkDisposed();
    if (this.car != null) {
      this.car.internalRemoveFromCycles1(this);
    }
    internalSetCar(car);
    if (this.car != null) {
      this.car.internalAddToCycles1(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetCar(final Car car) {
    this.car = car;
  }
  
  /**
   * @return Returns an unmodifiable list of cycles2.
   */
  public List<ToCycle2> getCycles2() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetCycles2());
  }
  
  /**
   * Sets the given cycles2 to the object. Currently contained cycles2 instances will be removed.
   * 
   * @param cycles2 the list of new instances
   */
  public void setCycles2(final List<ToCycle2> cycles2) {
    // remove the old toCycle2
    for(ToCycle2 oldElement : new ArrayList<ToCycle2>(this.internalGetCycles2())){
      removeFromCycles2(oldElement);
    }
    
    // add the new toCycle2
    for(ToCycle2 newElement : cycles2){
      addToCycles2(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>ToCycle2</code>s thereby lazy initializing it.
   */
  public List<ToCycle2> internalGetCycles2() {
    if (this.cycles2 == null) {
      this.cycles2 = new ArrayList<ToCycle2>();
    }
    return this.cycles2;
  }
  
  /**
   * Adds the given toCycle2 to this object. <p>
   * Since the reference is a composition reference, the opposite reference (ToCycle2.parent)
   * of the toCycle2 will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link ToCycle2#setParent(ToCycle2)}.
   * 
   */
  public void addToCycles2(final ToCycle2 toCycle2) {
    checkDisposed();
    toCycle2.setParent(this);
  }
  
  /**
   * Removes the given toCycle2 from this object. <p>
   * Since the reference is a cascading reference, the opposite reference (ToCycle2.parent)
   * of the toCycle2 will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link ToCycle2#setParent(ToCycle2)}.
   * 
   */
  public void removeFromCycles2(final ToCycle2 toCycle2) {
    checkDisposed();
    toCycle2.setParent(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToCycles2(final ToCycle2 toCycle2) {
    if(toCycle2 == null) {
    	return;
    }
    
    		internalGetCycles2().add(toCycle2);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromCycles2(final ToCycle2 toCycle2) {
    internalGetCycles2().remove(toCycle2);
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
