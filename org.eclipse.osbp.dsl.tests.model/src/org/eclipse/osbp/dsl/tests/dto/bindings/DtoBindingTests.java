package org.eclipse.osbp.dsl.tests.dto.bindings;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.beans.IBeanListProperty;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.dsl.tests.model.AbstractJPATest;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.junit.Test;
import org.osbp.mysmartshop.dtos.CashPositionDto;
import org.osbp.mysmartshop.dtos.CashRegisterDto;
import org.osbp.mysmartshop.dtos.CashSlipDto;
import org.osbp.mysmartshop.dtos.McustomerDto;

public class DtoBindingTests extends AbstractJPATest {

	private IDTOService<McustomerDto> customerService;
	private IDTOService<CashRegisterDto> cashRegisterService;
	private IDTOService<CashSlipDto> cashSlipService;
	private IDTOService<CashPositionDto> cashSlipPosService;

	int custId = 10;
	private CashSlipDto slip;

	@SuppressWarnings("restriction")
	@Override
	public void setUp() throws Exception {
		super.setUp();

		customerService = DtoServiceAccess.getService(McustomerDto.class);
		cashRegisterService = DtoServiceAccess
				.getService(CashRegisterDto.class);
		cashSlipService = DtoServiceAccess.getService(CashSlipDto.class);
		cashSlipPosService = DtoServiceAccess.getService(CashPositionDto.class);

		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		slip = new CashSlipDto();
		slip.setCashier("Jörg");
		slip.setCustomer(cust1);
		slip.setNow(new Date());
		slip.setCurrentDay(new Date().toString());
		slip.setSerial(10l);
		slip.setTotal(100d);

		CashPositionDto pos1 = new CashPositionDto();
		pos1.setPrice(1000d);
		pos1.setQuantity(10d);
		slip.addToPositions(pos1);

		CashPositionDto pos2 = new CashPositionDto();
		pos2.setPrice(1000d);
		pos2.setQuantity(10d);
		slip.addToPositions(pos2);
		
		CashPositionDto pos3 = new CashPositionDto();
		pos3.setPrice(1000d);
		pos3.setQuantity(10d);
		slip.addToPositions(pos3);
		
		CashPositionDto pos4 = new CashPositionDto();
		pos4.setPrice(1000d);
		pos4.setQuantity(10d);
		slip.addToPositions(pos4);

		cashSlipService.update(slip);
		
		// reload the slip
		slip = cashSlipService.get(slip.getId());
	}

	@Test
	public void testListBinding() throws Exception {
		setUp();
		
		new CustomRealm();

		IBeanListProperty sourceProperty = BeanProperties.list(CashSlipDto.class, "positions", CashPositionDto.class);
		IObservableList sourceList = sourceProperty.observe(slip);
		
		WritableList targetList = new WritableList();
		
		DataBindingContext dbc = new DataBindingContext();
		dbc.bindList(targetList, sourceList);		
		dbc.updateTargets();
		
		assertEquals(4, targetList.size());

		CashPositionDto pos1 = (CashPositionDto) sourceList.get(0);
		CashPositionDto pos2 = (CashPositionDto) sourceList.get(2);
		
		slip.removeFromPositions(pos1);
		assertEquals(3, targetList.size());
		
		slip.removeFromPositions(pos2);
		assertEquals(2, targetList.size());
		
		slip.addToPositions(pos1);
		assertEquals(3, targetList.size());
		
		slip.addToPositions(pos2);
		assertEquals(4, targetList.size());
		
		slip.addToPositions(pos2);
		slip.addToPositions(pos2);
		slip.addToPositions(pos2);
		assertEquals(4, targetList.size());
	}
	
	
	public static class CustomRealm extends Realm {

		public CustomRealm() {
			setDefault(this);
		}
		
		@Override
		public boolean isCurrent() {
			return true;
		}
		
	}
}
