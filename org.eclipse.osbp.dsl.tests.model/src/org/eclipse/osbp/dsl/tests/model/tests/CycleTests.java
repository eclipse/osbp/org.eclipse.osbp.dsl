/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.dsl.tests.model.tests;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Date;

import javax.persistence.EntityManager;

import org.eclipse.osbp.dsl.dto.lib.OppositeDtoList;
import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.dsl.tests.carstore.dtos.CarDto;
import org.eclipse.osbp.dsl.tests.carstore.entities.Car;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle1;
import org.eclipse.osbp.dsl.tests.carstore.entities.ToCycle2;
import org.eclipse.osbp.dsl.tests.model.AbstractJPATest;
import org.eclipse.osbp.jpa.services.Query;
import org.eclipse.osbp.jpa.services.filters.LCompare;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("restriction")
public class CycleTests extends AbstractJPATest {

	private void createData() {

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();

		Car car1 = new Car();
		car1.setNumber("ct-001");

		ToCycle1 cycle1_1 = new ToCycle1();
		car1.addToCycles1(cycle1_1);
		ToCycle2 cycle2_1 = new ToCycle2();
		cycle2_1.setCar(car1);
		cycle1_1.addToCycles2(cycle2_1);

		ToCycle1 cycle1_2 = new ToCycle1();
		car1.addToCycles1(cycle1_2);
		ToCycle2 cycle2_2 = new ToCycle2();
		cycle2_2.setCar(car1);
		cycle1_1.addToCycles2(cycle2_2);

		ToCycle1 cycle1_3 = new ToCycle1();
		car1.addToCycles1(cycle1_3);
		ToCycle2 cycle2_3 = new ToCycle2();
		cycle2_3.setCar(car1);
		cycle1_1.addToCycles2(cycle2_3);

		em.persist(car1);

		em.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testService() throws Exception {
		super.setUp();

		createData();

		IDTOService<CarDto> carService = DtoServiceAccess
				.getService(CarDto.class);

		Collection<CarDto> cars = carService.find(new Query(new LCompare.Equal(
				"number", "ct-001")));
		CarDto car = cars.iterator().next();

		Field field = CarDto.class.getDeclaredField("cycles1");
		field.setAccessible(true);
		OppositeDtoList<CarDto> list = (OppositeDtoList<CarDto>) field
				.get(car);
		Assert.assertFalse(list.isResolved());

		car.setFinishingDate(new Date());
		carService.update(car);
		list = (OppositeDtoList<CarDto>) field.get(car);
		Assert.assertFalse(list.isResolved());

	}

}
