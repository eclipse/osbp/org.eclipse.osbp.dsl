/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.osbp.dsl.tests.model;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.spi.PersistenceProvider;
import javax.persistence.spi.PersistenceProviderResolver;
import javax.persistence.spi.PersistenceProviderResolverHolder;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;

public class AbstractJPATest {

	protected Map<String, Object> properties = new HashMap<String, Object>();
	protected static EntityManagerFactory emf;
	private static ServiceRegistration<EntityManagerFactory> reg;

	public void setUp() throws Exception {
		if (emf != null) {
			reg.unregister();
			emf.close();
		}
		PersistenceProviderResolverHolder.setPersistenceProviderResolver(new PersistenceProviderResolver() {
			private List<PersistenceProvider> providers = new ArrayList<PersistenceProvider>();

			@Override
			public List<PersistenceProvider> getPersistenceProviders() {
				org.eclipse.persistence.jpa.PersistenceProvider provider = new org.eclipse.persistence.jpa.PersistenceProvider();
				providers.add(provider);
				return providers;
			}

			@Override
			public void clearCachedProviders() {
				providers.clear();
			}
		});
		properties.put(PersistenceUnitProperties.CLASSLOADER, getClass().getClassLoader());

		// Ensure the persistence.xml provided data source are ignored for Java
		// SE testing
		properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, "");
		properties.put(PersistenceUnitProperties.JTA_DATASOURCE, "");
		properties.put(PersistenceUnitProperties.TRANSACTION_TYPE, "RESOURCE_LOCAL");
		// properties.put(PersistenceUnitProperties.SESSION_CUSTOMIZER,
		// "eclipselink.example.jpa.employee.model.HistorizedSequence");

		// Configure the use of embedded derby for the tests allowing system
		// properties of the same name to override
		// setProperty(properties, PersistenceUnitProperties.JDBC_DRIVER,
		// "org.apache.derby.jdbc.EmbeddedDriver");
		// setProperty(properties, PersistenceUnitProperties.JDBC_URL,
		// "jdbc:derby:target/derby/mysports;create=true");
		// setProperty(properties, PersistenceUnitProperties.JDBC_USER, "app");
		// setProperty(properties, PersistenceUnitProperties.JDBC_PASSWORD,
		// "app");
		//
		properties.put(PersistenceUnitProperties.JDBC_DRIVER, "com.mysql.jdbc.Driver");
		properties.put(PersistenceUnitProperties.JDBC_URL,
				"jdbc:mysql://localhost/testCarstore?allowPublicKeyRetrieval=TRUE&useSSL=FALSE&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
		properties.put(PersistenceUnitProperties.JDBC_USER, "root");
		properties.put(PersistenceUnitProperties.JDBC_PASSWORD, "DpJf?T.iZ4j(");

		// Ensure weaving is used
		properties.put(PersistenceUnitProperties.WEAVING, "false");
		properties.put(PersistenceUnitProperties.DDL_GENERATION, PersistenceUnitProperties.DROP_AND_CREATE);
		properties.put(PersistenceUnitProperties.DDL_GENERATION_MODE,
				PersistenceUnitProperties.DDL_DATABASE_GENERATION);
		
		emf = Persistence.createEntityManagerFactory("testCarstore", properties);

		Dictionary<String, Object> dict = new Hashtable<>();
		dict.put("osgi.unit.name", "testCarstore");
		Bundle bundle = FrameworkUtil.getBundle(AbstractJPATest.class);
		reg = bundle.getBundleContext().registerService(EntityManagerFactory.class, emf, dict);
	}
}
