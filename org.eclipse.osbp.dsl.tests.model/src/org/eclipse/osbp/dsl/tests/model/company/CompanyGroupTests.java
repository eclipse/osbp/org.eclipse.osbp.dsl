/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.dsl.tests.model.company;

import java.util.Date;

import javax.persistence.EntityManager;

import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.dsl.tests.model.AbstractJPATest;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.junit.Assert;
import org.junit.Test;
import org.osbp.mysmartshop.dtos.AddressDto;
import org.osbp.mysmartshop.dtos.CompanyDto;
import org.osbp.mysmartshop.dtos.CompanyGroupDto;
import org.osbp.mysmartshop.dtos.DepartmentDto;

@SuppressWarnings("restriction")
public class CompanyGroupTests extends AbstractJPATest {

	private IDTOService<CompanyDto> companyService;
	private IDTOService<CompanyGroupDto> companyGroupService;

	private long time = new Date().getTime();

	private CompanyDto currentCompany;
	private int currentCompanyIndex;
	private AddressDto currentAddress;
	private int currentAddressIndex;
	private DepartmentDto currentDepartment;
	private int currentDepartmentIndex;
	private CompanyGroupDto companyGroup;

	@Override
	public void setUp() throws Exception {
		super.setUp();

		companyService = DtoServiceAccess.getService(CompanyDto.class);
		companyGroupService = DtoServiceAccess
				.getService(CompanyGroupDto.class);

		EntityManager em = emf.createEntityManager();
		em.close();
	}

	@Test
	public void testOppositesInAddress() throws Exception {
		setUp();

		String compId;
		{
			CompanyDto comp = new CompanyDto();
			compId = comp.getId();
			DepartmentDto dep = new DepartmentDto();
			dep.setCompany(comp);

			AddressDto depAddress = new AddressDto();
			depAddress.setDepartment(dep);
			depAddress.setCompany(comp);

			companyService.update(comp);
		}

		{
			CompanyDto comp = companyService.get(compId);
			AddressDto compAddress = comp.getAddress().get(0);
			AddressDto depAddress = comp.getDepartments().get(0).getAddress()
					.get(0);
			Assert.assertSame(compAddress, depAddress);
		}
	}

	@Test
	public void testPersistCompanyGroup() throws Exception {
		setUp();

		companyGroup = new CompanyGroupDto();
		companyGroup.setId("COMP_GROUP1__" + time);
		{
			createCompanies();

			companyGroupService.update(companyGroup);

			CompanyGroupDto persCompGroup = companyGroupService
					.get(companyGroup.getId());
			Assert.assertNotNull(persCompGroup);

			Assert.assertEquals(10, persCompGroup.getCompanies().size());

			for (CompanyDto comp : persCompGroup.getCompanies()) {
				Assert.assertEquals(10, comp.getDepartments().size());

				// contains addresses from department and from company
				Assert.assertEquals(110, comp.getAddress().size());
				
				// check opposite
				Assert.assertSame(persCompGroup, comp.getCompany_group());

				for (DepartmentDto dep : comp.getDepartments()) {
					Assert.assertEquals(10, dep.getAddress().size());

					// check opposite
					Assert.assertSame(comp, dep.getCompany());

					for (AddressDto addr : dep.getAddress()) {
						// check opposite
						Assert.assertSame(dep, addr.getDepartment());
					}
				}

				for (AddressDto addr : comp.getAddress()) {

					// check opposite
					Assert.assertSame(comp, addr.getCompany());
				}
			}
		}
	}

	void createCompanies() {

		currentCompanyIndex = 0;
		for (int i = 0; i < 10; i++) {

			currentCompanyIndex++;

			CompanyDto comp = new CompanyDto();
			currentCompany = comp;
			comp.setId(createCompanyUUID());
			comp.setCompany_group(companyGroup);

			createCompanyAddresses();
			createDepartments();
		}
	}

	void createDepartments() {
		currentDepartmentIndex = 0;
		for (int i = 0; i < 10; i++) {
			currentDepartmentIndex++;

			DepartmentDto dep = new DepartmentDto();
			currentDepartment = dep;
			dep.setId(createDepartmentUUID());
			dep.setCompany(currentCompany);

			createDepartmentAddresses();
		}
	}

	void createCompanyAddresses() {
		currentAddressIndex = 0;
		for (int i = 0; i < 10; i++) {
			currentAddressIndex++;

			AddressDto addr = new AddressDto();
			addr.setId(createCompanyAddressUUID());
			addr.setCompany(currentCompany);
		}
	}

	void createDepartmentAddresses() {
		currentAddressIndex = 0;
		for (int i = 0; i < 10; i++) {
			currentAddressIndex++;

			AddressDto addr = new AddressDto();
			addr.setId(createDepartmentAddressUUID());
			addr.setCompany(currentCompany);
			addr.setDepartment(currentDepartment);
		}
	}

	private String createCompanyUUID() {
		return "COMP" + currentCompanyIndex + "__" + time;
	}

	private String createDepartmentUUID() {
		return "DEP" + currentDepartmentIndex + "__" + createCompanyUUID();
	}

	private String createCompanyAddressUUID() {
		return "ADDR" + currentAddressIndex + "__" + createCompanyUUID();
	}

	private String createDepartmentAddressUUID() {
		return "ADDR" + currentAddressIndex + "__" + createDepartmentUUID();
	}
}
