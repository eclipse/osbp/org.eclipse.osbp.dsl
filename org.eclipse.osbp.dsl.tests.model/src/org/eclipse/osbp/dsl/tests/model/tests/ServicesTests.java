/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.dsl.tests.model.tests;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.dsl.tests.carstore.dtos.AddonDto;
import org.eclipse.osbp.dsl.tests.carstore.dtos.CarDto;
import org.eclipse.osbp.dsl.tests.model.AbstractJPATest;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("restriction")
public class ServicesTests extends AbstractJPATest {

	@Test
	public void testService_CRUD() throws Exception {
		super.setUp();
		
		IDTOService<CarDto> carService = DtoServiceAccess
				.getService(CarDto.class);
 
		CarDto car = new CarDto();
		car.setNumber("112233");
		car.setFinishingDate(new Date());
 
		carService.update(car);
		car = carService.reload(car);
		Assert.assertEquals(car.getNumber(), car.getNumber());
		Assert.assertEquals(car.getUuid(), car.getUuid());
		
		CarDto rCar = carService.get(car.getUuid());
		Assert.assertEquals("112233", rCar.getNumber());
		
		carService.delete(rCar);
		
		rCar = carService.get(car.getUuid());
		Assert.assertNull(rCar);
	}
	
	@Test
	public void testService_CRUD_complex() throws Exception {
		super.setUp();
		
		IDTOService<CarDto> carService = DtoServiceAccess
				.getService(CarDto.class);

		CarDto car = new CarDto();
		car.setNumber("777777");
		car.setFinishingDate(new Date());
		
		AddonDto addon1 = new AddonDto();
		addon1.setDescription("addon1");
		car.addToAddons(addon1);
		
		AddonDto addon2 = new AddonDto();
		addon2.setDescription("addon2");
		car.addToAddons(addon2);

		carService.update(car);
		car = carService.reload(car);
		CarDto newCar = car;
		Assert.assertEquals(car.getNumber(), newCar.getNumber());
		Assert.assertEquals(car.getUuid(), newCar.getUuid());
		Assert.assertEquals(2, car.getAddons().size());
		List<String> values = new ArrayList<>();
		values.add("addon1");
		values.add("addon2");
		Assert.assertTrue(values.contains(car.getAddons().get(0).getDescription()));
		Assert.assertTrue(values.contains(car.getAddons().get(1).getDescription()));
		
		CarDto rCar = carService.get(car.getUuid());
		Assert.assertEquals("777777", rCar.getNumber());
		Assert.assertEquals(2, car.getAddons().size());
		Assert.assertTrue(values.contains(rCar.getAddons().get(0).getDescription()));
		Assert.assertTrue(values.contains(rCar.getAddons().get(1).getDescription()));
		Assert.assertSame(rCar, rCar.getAddons().get(0).getCar());
		Assert.assertSame(rCar, rCar.getAddons().get(1).getCar());
		
		carService.delete(rCar);
		
		rCar = carService.get(car.getUuid());
		Assert.assertNull(rCar);
	}

}
