/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.dsl.tests.model.lazyloading;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList;
import org.eclipse.osbp.dsl.dto.lib.OppositeDtoList;
import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.dsl.tests.model.AbstractJPATest;
import org.eclipse.osbp.jpa.services.Query;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.junit.Assert;
import org.junit.Test;
import org.osbp.mysmartshop.dtos.DtoTestChildContainmentDto;
import org.osbp.mysmartshop.dtos.DtoTestChildCrossRefDto;
import org.osbp.mysmartshop.dtos.DtoTestParentDto;

@SuppressWarnings("restriction")
public class ServicesTests extends AbstractJPATest {

	/**
	 * Tests what happens if the dto is bound to UI,... before update.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testService_Transient() throws Exception {
		super.setUp();

		IDTOService<DtoTestParentDto> parentService = DtoServiceAccess.getService(DtoTestParentDto.class);

		// create a dto with 2 containments
		DtoTestParentDto parent = new DtoTestParentDto();

		DtoTestChildContainmentDto childContainmentDto = new DtoTestChildContainmentDto();
		childContainmentDto.setContainer(parent);

		DtoTestChildContainmentDto childContainmentDto2 = new DtoTestChildContainmentDto();
		parent.addToContainmentChilds(childContainmentDto2);

		Assert.assertEquals(2, parent.getContainmentChilds().size());

		boolean found1 = false;
		boolean found2 = false;
		for (DtoTestChildContainmentDto child : parent.getContainmentChilds()) {
			if (child == childContainmentDto) {
				found1 = true;
			}

			if (child == childContainmentDto2) {
				found2 = true;
			}
		}

		Assert.assertTrue(found1 && found2);
		parentService.update(parent);

		// read the same dto from database. Then add a new dto and remove a old
		// one.
		// then lets resolve the collection

		parent = parentService.get(parent.getId());
		Assert.assertEquals(2, parent.internalGetContainmentChilds().size());

		OppositeDtoList<DtoTestChildContainmentDto> childsList = (OppositeDtoList<DtoTestChildContainmentDto>) parent
				.internalGetContainmentChilds();
		Assert.assertFalse(childsList.isResolved());

		// add a child
		DtoTestChildContainmentDto childContainmentDto3 = new DtoTestChildContainmentDto();
		// will NOT resolve the collection, since the ListDelta-Notification
		// for databinding is a copy of AbstractDtoList
		parent.addToContainmentChilds(childContainmentDto3);
		Assert.assertEquals(3, parent.internalGetContainmentChilds().size());
		Assert.assertFalse(childsList.isResolved());

		// resolved the childList
		DtoTestChildContainmentDto child1 = parent.internalGetContainmentChilds().get(0);
		DtoTestChildContainmentDto child2 = parent.internalGetContainmentChilds().get(1);
		DtoTestChildContainmentDto child3 = parent.internalGetContainmentChilds().get(2);
		// remove a child 2
		parent.removeFromContainmentChilds(child2);
		Assert.assertEquals(2, parent.internalGetContainmentChilds().size());
		Assert.assertTrue(childsList.isResolved());

		found1 = false;
		found2 = false;
		boolean found3 = false;
		for (DtoTestChildContainmentDto child : parent.getContainmentChilds()) {
			if (child == child1) {
				found1 = true;
			}

			if (child == child2) {
				found2 = true;
			}

			if (child == child3) {
				found3 = true;
			}
		}

		Assert.assertTrue(found1 && !found2 && found3);

	}

	@Test
	public void testService_CRUD() throws Exception {
		super.setUp();

		IDTOService<DtoTestParentDto> parentService = DtoServiceAccess.getService(DtoTestParentDto.class);

		DtoTestParentDto parent = new DtoTestParentDto();

		DtoTestChildContainmentDto childContainmentDto = new DtoTestChildContainmentDto();
		childContainmentDto.setContainer(parent);

		DtoTestChildContainmentDto childContainmentDto2 = new DtoTestChildContainmentDto();
		parent.addToContainmentChilds(childContainmentDto2);

		parentService.update(parent);
		parent = parentService.get(parent.getId());

		Assert.assertEquals(2, parent.getContainmentChilds().size());

		DtoTestParentDto parent2 = parentService.get(parent.getId());
		Assert.assertEquals(2, parent2.getContainmentChilds().size());

		parent2.removeFromContainmentChilds(parent2.getContainmentChilds().get(0));
		parentService.update(parent2);

		DtoTestParentDto parent3 = parentService.get(parent.getId());
		Assert.assertEquals(1, parent3.getContainmentChilds().size());
	}

	@Test
	public void testSerialize_PeristentDto() throws Exception {
		super.setUp();

		//
		// create the cross references
		//
		IDTOService<DtoTestChildCrossRefDto> crossRefService = DtoServiceAccess
				.getService(DtoTestChildCrossRefDto.class);

		List<DtoTestChildCrossRefDto> crossRefs = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			DtoTestChildCrossRefDto crossRef = new DtoTestChildCrossRefDto();
			crossRefService.update(crossRef);
			crossRefs.add(crossRef);
		}

		IDTOService<DtoTestParentDto> parentService = DtoServiceAccess.getService(DtoTestParentDto.class);

		DtoTestParentDto parent = new DtoTestParentDto();

		// add the cross references
		for (DtoTestChildCrossRefDto crossRef : crossRefs) {
			parent.addToCrossRefChilds(crossRef);
		}

		Set<String> ids = new HashSet<>();
		for (int i = 0; i < 1000; i++) {
			DtoTestChildContainmentDto childContainmentDto = new DtoTestChildContainmentDto();
			parent.addToContainmentChilds(childContainmentDto);
			if (!ids.add(childContainmentDto.getId())) {
				throw new Exception("Duplicate ID");
			}
		}

		parentService.update(parent);

		DtoTestParentDto parent2 = parentService.get(parent.getId());
		parent2.getContainmentChilds().iterator();

		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		oos = new ObjectOutputStream(fos);
		try {
			oos.writeObject(parent2);
			// not serializable
			Assert.fail();
		} catch (IOException ex) {
			// expected
		} finally {
			oos.close();
		}

	}

	@Test
	public void testSerialzeUpdateContainmentRef_Dto() throws Exception {
		super.setUp();

		//
		// create the cross references
		//
		IDTOService<DtoTestChildCrossRefDto> crossRefService = DtoServiceAccess
				.getService(DtoTestChildCrossRefDto.class);

		List<DtoTestChildCrossRefDto> crossRefs = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			DtoTestChildCrossRefDto crossRef = new DtoTestChildCrossRefDto();
			crossRefService.update(crossRef);
			crossRefs.add(crossRef);
		}

		IDTOService<DtoTestParentDto> parentService = DtoServiceAccess.getService(DtoTestParentDto.class);

		DtoTestParentDto parent = new DtoTestParentDto();

		// add the cross references
		for (DtoTestChildCrossRefDto crossRef : crossRefs) {
			parent.addToCrossRefChilds(crossRef);
		}

		for (int i = 0; i < 1000; i++) {
			DtoTestChildContainmentDto childContainmentDto = new DtoTestChildContainmentDto();
			parent.addToContainmentChilds(childContainmentDto);
		}

		parentService.update(parent);

		DtoTestParentDto parent2 = parentService.get(parent.getId());
		
		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		oos = new ObjectOutputStream(fos);
		try {
			oos.writeObject(parent2);
			// not serializable
			Assert.fail();
		} catch (IOException ex) {
			// expected
		} finally {
			oos.close();
		}
	}

	@Test
	public void testCrossReference_getByEntityManager() throws Exception {
		super.setUp();

		//
		// create the cross references
		//
		IDTOService<DtoTestChildCrossRefDto> crossRefService = DtoServiceAccess
				.getService(DtoTestChildCrossRefDto.class);

		List<DtoTestChildCrossRefDto> crossRefs = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			DtoTestChildCrossRefDto crossRef = new DtoTestChildCrossRefDto();
			crossRefService.update(crossRef);
			crossRefs.add(crossRef);
		}

		IDTOService<DtoTestParentDto> parentService = DtoServiceAccess.getService(DtoTestParentDto.class);

		DtoTestParentDto parent = new DtoTestParentDto();

		// add the cross references
		for (DtoTestChildCrossRefDto crossRef : crossRefs) {
			parent.addToCrossRefChilds(crossRef);
		}

		// remember the single cross reference
		DtoTestChildCrossRefDto crossRef = crossRefs.get(0);

		for (int i = 0; i < 1000; i++) {
			DtoTestChildContainmentDto childContainmentDto = new DtoTestChildContainmentDto();
			parent.addToContainmentChilds(childContainmentDto);
		}

		parentService.update(parent);

		DtoTestParentDto parent2 = parentService.get(parent.getId());
		// Assert.assertEquals(crossRef.getId(),
		// parent2.getCrossRefChild().getId());

		parent2.setCrossRefChild(crossRef);
		parentService.update(parent2);
	}

	/**
	 * No exception on update!
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSetup_LaterFetchForChildren() throws Exception {
		super.setUp();

		//
		// create the cross references
		//
		IDTOService<DtoTestChildCrossRefDto> crossRefService = DtoServiceAccess
				.getService(DtoTestChildCrossRefDto.class);

		for (int i = 0; i < 1000; i++) {
			DtoTestChildCrossRefDto crossRef = new DtoTestChildCrossRefDto();
			crossRefService.update(crossRef);
		}

		IDTOService<DtoTestParentDto> parentService = DtoServiceAccess.getService(DtoTestParentDto.class);

		DtoTestParentDto parent = new DtoTestParentDto();

		// add the cross references
		for (DtoTestChildCrossRefDto crossRef : crossRefService.find(new Query())) {
			parent.addToCrossRefChilds(crossRef);
		}

		parentService.update(parent);

	}

	/**
	 * No exception on update!
	 * 
	 * @throws Exception
	 */
	@Test
	public void testOrphanRemoval() throws Exception {
		super.setUp();

		// clean up
		IDTOService<DtoTestChildCrossRefDto> crossRefService = DtoServiceAccess
				.getService(DtoTestChildCrossRefDto.class);
		for (DtoTestChildCrossRefDto temp : crossRefService.find(new Query())) {
			temp = crossRefService.reload(temp);
			if(temp != null) {
				crossRefService.delete(temp);
			}
		}
		IDTOService<DtoTestParentDto> parentService = DtoServiceAccess.getService(DtoTestParentDto.class);
		for (DtoTestParentDto temp : parentService.find(new Query())) {
			parentService.delete(temp);
		}
		// test size of containments
		IDTOService<DtoTestChildContainmentDto> containmentsService = DtoServiceAccess
				.getService(DtoTestChildContainmentDto.class);
		for (DtoTestChildContainmentDto temp : containmentsService.find(new Query())) {
			containmentsService.delete(temp);
		}

		Assert.assertEquals(0, containmentsService.size(new Query()));

		// create parent with containments and cross references

		DtoTestParentDto parent = new DtoTestParentDto();

		for (int i = 0; i < 1000; i++) {
			DtoTestChildContainmentDto childContainmentDto = new DtoTestChildContainmentDto();
			parent.addToContainmentChilds(childContainmentDto);
		}

		parentService.update(parent);

		// now there are 1000 containments
		Assert.assertEquals(1000, containmentsService.size(new Query()));

		// now delete parent
		DtoTestParentDto parent2 = parentService.get(parent.getId());
		parentService.delete(parent2);

		// now there are 0 containments due to orphan removal
		Assert.assertEquals(0, containmentsService.size(new Query()));
	}

	/**
	 * No exception on update!
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCrossReference_PreRemoveAnnotation() throws Exception {
		super.setUp();

		// clean up
		IDTOService<DtoTestParentDto> parentService = DtoServiceAccess.getService(DtoTestParentDto.class);
		for (DtoTestParentDto temp : parentService.find(new Query())) {
			parentService.delete(temp);
		}
		IDTOService<DtoTestChildCrossRefDto> crossRefService = DtoServiceAccess
				.getService(DtoTestChildCrossRefDto.class);
		for (DtoTestChildCrossRefDto temp : crossRefService.find(new Query())) {
			crossRefService.delete(temp);
		}

		//
		// create the cross references
		//

		Assert.assertEquals(0, crossRefService.size(new Query()));

		List<DtoTestChildCrossRefDto> crossRefs = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			DtoTestChildCrossRefDto crossRef = new DtoTestChildCrossRefDto();
			crossRefService.update(crossRef);
			crossRefs.add(crossRef);
		}

		Assert.assertEquals(1000, crossRefService.size(new Query()));

		// test size of containments
		IDTOService<DtoTestChildContainmentDto> containmentsService = DtoServiceAccess
				.getService(DtoTestChildContainmentDto.class);
		Assert.assertEquals(0, containmentsService.size(new Query()));

		// create parent with containments and cross references

		DtoTestParentDto parent = new DtoTestParentDto();

		// add the cross references
		for (DtoTestChildCrossRefDto crossRef : crossRefs) {
			parent.addToCrossRefChilds(crossRefService.reload(crossRef));
		}

		for (int i = 0; i < 1000; i++) {
			DtoTestChildContainmentDto childContainmentDto = new DtoTestChildContainmentDto();
			parent.addToContainmentChilds(childContainmentDto);
		}

		parentService.update(parent);

		// now there are 1000 containments
		Assert.assertEquals(1000, containmentsService.size(new Query()));

		// now delete parent
		DtoTestParentDto parent2 = parentService.get(parent.getId());
		parentService.delete(parent2);

		// now there are 0 containments due to orphan removal
		Assert.assertEquals(0, containmentsService.size(new Query()));
		// and still 1000 cross references
		Assert.assertEquals(1000, crossRefService.size(new Query()));
	}

	@Test
	public void testSerialize() throws Exception {
		Serialize1 s1 = new Serialize1();
		s1.name = "S1Foo";
		s1.s2.name = "S2Bar";

		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		oos = new ObjectOutputStream(fos);
		try {
			oos.writeObject(s1);
		} finally {
			oos.close();
		}

		ByteArrayInputStream bis = new ByteArrayInputStream(fos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bis);
		Object xs1 = ois.readObject();

		System.out.println(xs1);
	}

	@Test
	public void testResolveOppositeForBigData() throws Exception {

		super.setUp();

		IDTOService<DtoTestParentDto> parentService = DtoServiceAccess.getService(DtoTestParentDto.class);
		IDTOService<DtoTestChildCrossRefDto> childCrossRefService = DtoServiceAccess.getService(DtoTestChildCrossRefDto.class);

		DtoTestParentDto parent = new DtoTestParentDto();
		for (int i = 0; i < 100; i++) {
			
			DtoTestChildCrossRefDto crossRef = new DtoTestChildCrossRefDto();
			childCrossRefService.update(crossRef);
			crossRef = childCrossRefService.reload(crossRef);
			
			parent.addToCrossRefChilds(crossRef);
			parent.addToContainmentChilds(new DtoTestChildContainmentDto());
		}

		parentService.update(parent);

		parent = parentService.get(parent.getId());
		AbstractOppositeDtoList<DtoTestChildCrossRefDto> crossRefChilds = (AbstractOppositeDtoList<DtoTestChildCrossRefDto>) parent
				.internalGetCrossRefChilds();
		AbstractOppositeDtoList<DtoTestChildContainmentDto> containmentChilds = (AbstractOppositeDtoList<DtoTestChildContainmentDto>) parent
				.internalGetContainmentChilds();

		Assert.assertFalse(crossRefChilds.isResolved());
		Assert.assertFalse(containmentChilds.isResolved());

		parent.addToContainmentChilds(new DtoTestChildContainmentDto());
		Assert.assertFalse(crossRefChilds.isResolved());
		Assert.assertFalse(containmentChilds.isResolved());

		parent.addToCrossRefChilds(new DtoTestChildCrossRefDto());
		Assert.assertFalse(crossRefChilds.isResolved());
		Assert.assertFalse(containmentChilds.isResolved());

	}

	@SuppressWarnings("serial")
	public static class Serialize1 implements Serializable {

		String name;
		Serialize2 s2;

		public Serialize1() {
			s2 = new Serialize2();
		}

	}

	@SuppressWarnings("serial")
	public static class Serialize2 implements Serializable {

		String name;
	}

}
