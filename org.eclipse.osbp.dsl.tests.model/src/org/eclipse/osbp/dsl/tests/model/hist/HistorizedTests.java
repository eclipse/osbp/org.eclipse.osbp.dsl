/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.dsl.tests.model.hist;

import javax.persistence.EntityManager;

import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.dsl.tests.hist.dtos.HAddressDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.HCountryDto;
import org.eclipse.osbp.dsl.tests.hist.dtos.HOrderDto;
import org.eclipse.osbp.dsl.tests.model.AbstractJPATest;
import org.eclipse.osbp.runtime.common.annotations.DtoUtils;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.junit.Assert;
import org.junit.Test;


@SuppressWarnings("restriction")
public class HistorizedTests extends AbstractJPATest {

	private void createData() {

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();

	}

	@Test
	public void testService() throws Exception {
		super.setUp();

		createData();

		IDTOService<HOrderDto> orderService = DtoServiceAccess.getService(HOrderDto.class);
		IDTOService<HCountryDto> countryService = DtoServiceAccess.getService(HCountryDto.class);
		IDTOService<HAddressDto> addressService = DtoServiceAccess.getService(HAddressDto.class);

		
		HCountryDto cAT = new HCountryDto();
		cAT.setIsoCode("AT");
		countryService.persist(cAT);
		cAT = countryService.reload(cAT);
		
		HCountryDto cDE = new HCountryDto();
		cDE.setIsoCode("DE");
		countryService.persist(cDE);
		cDE = countryService.reload(cDE);
		
		HAddressDto addrPrichner_v1_AT = new HAddressDto();
		addrPrichner_v1_AT.setCountry(cAT);
		addrPrichner_v1_AT.setName("Pirchner");
		addrPrichner_v1_AT.setStreet("Irgendwo");
		addrPrichner_v1_AT.setPostalCode("1010");
		addressService.update(addrPrichner_v1_AT);
		addrPrichner_v1_AT = addressService.reload(addrPrichner_v1_AT);
		
		HAddressDto addrPrichner_v2_AT = (HAddressDto) DtoUtils.newHistorizedVersionCopy(addrPrichner_v1_AT);
		addrPrichner_v2_AT.setCountry(cDE);
		addressService.update(addrPrichner_v2_AT);
		addrPrichner_v2_AT = addressService.reload(addrPrichner_v2_AT);
		
		HAddressDto addrEdler_v1_DE = new HAddressDto();
		addrEdler_v1_DE.setCountry(cDE);
		addrEdler_v1_DE.setName("Edler");
		addrEdler_v1_DE.setStreet("Berlin");
		addrEdler_v1_DE.setPostalCode("99999");
		addressService.update(addrEdler_v1_DE);
		addrEdler_v1_DE = addressService.reload(addrEdler_v1_DE);
		
		// create a default order
		//
		HOrderDto order_Flo_1 = new HOrderDto();
		order_Flo_1.setOrderNumber("Flo_1");
		order_Flo_1.setDeliveryAddress(addrPrichner_v1_AT);
		order_Flo_1.setInvoiceAddress(addrPrichner_v1_AT);
	
		orderService.update(order_Flo_1);
		order_Flo_1 = orderService.reload(order_Flo_1);
		
		Assert.assertEquals("AT", order_Flo_1.getDeliveryAddress().getCountry().getIsoCode());
		Assert.assertEquals("AT", order_Flo_1.getInvoiceAddress().getCountry().getIsoCode());
	
		order_Flo_1.setInvoiceAddress(addrPrichner_v2_AT);
		orderService.update(order_Flo_1);
		order_Flo_1 = orderService.reload(order_Flo_1);
		Assert.assertEquals("AT", order_Flo_1.getDeliveryAddress().getCountry().getIsoCode());
		Assert.assertEquals("DE", order_Flo_1.getInvoiceAddress().getCountry().getIsoCode());
	
		
		
	}

}
