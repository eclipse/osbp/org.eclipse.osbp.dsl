/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.dsl.tests.model.payment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList;
import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.dsl.tests.model.AbstractJPATest;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.junit.Assert;
import org.junit.Test;
import org.osbp.mysmartshop.dtos.CashDrawerCurrencyDto;
import org.osbp.mysmartshop.dtos.CashDrawerSumDto;
import org.osbp.mysmartshop.dtos.CashPaymentDto;
import org.osbp.mysmartshop.dtos.CashPositionDto;
import org.osbp.mysmartshop.dtos.CashRegisterDto;
import org.osbp.mysmartshop.dtos.CashSlipDto;
import org.osbp.mysmartshop.dtos.CashSubPositionDto;
import org.osbp.mysmartshop.dtos.McustomerDto;
import org.osbp.mysmartshop.dtos.service.CashPositionDtoService;
import org.osbp.mysmartshop.dtos.service.CashSlipDtoService;

@SuppressWarnings("restriction")
public class CashTests extends AbstractJPATest {

	private IDTOService<McustomerDto> customerService;
	private IDTOService<CashRegisterDto> cashRegisterService;
	private IDTOService<CashSlipDto> cashSlipService;
	private IDTOService<CashPositionDto> cashSlipPosService;
	private IDTOService<CashPaymentDto> cashPaymentService;
	private IDTOService<CashDrawerSumDto> cashDrawerSumService;
	private IDTOService<CashDrawerCurrencyDto> cashDrawerCurrencyService;

	@Override
	public void setUp() throws Exception {
		super.setUp();

		customerService = DtoServiceAccess.getService(McustomerDto.class);
		cashRegisterService = DtoServiceAccess.getService(CashRegisterDto.class);
		cashSlipService = DtoServiceAccess.getService(CashSlipDto.class);
		cashSlipPosService = DtoServiceAccess.getService(CashPositionDto.class);
		cashPaymentService = DtoServiceAccess.getService(CashPaymentDto.class);
		cashDrawerSumService = DtoServiceAccess.getService(CashDrawerSumDto.class);
		cashDrawerCurrencyService = DtoServiceAccess.getService(CashDrawerCurrencyDto.class);

	}

	@Test
	public void testPersistCash() throws Exception {
		setUp();

		int custId = 1;

		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		CashRegisterDto cashRegister = new CashRegisterDto();
		cashRegister.setNum("1000");
		cashRegisterService.update(cashRegister);
		cashRegister = cashRegisterService.get(cashRegister.getId());

		CashSlipDto slip = new CashSlipDto();
		slip.setCashier("Jörg");
		slip.setCustomer(cust1);
		slip.setNow(new Date());
		slip.setCurrentDay(new Date().toString());
		slip.setSerial(10l);
		// slip.setRegister(cashRegister);
		slip.setTotal(100d);
		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());

		CashPositionDto pos1 = new CashPositionDto();
		pos1.setPrice(1000d);
		pos1.setQuantity(10d);
		slip.addToPositions(pos1);
		cashSlipPosService.update(pos1);

		slip = cashSlipService.get(slip.getId());
		assertEquals(1, slip.getPositions().size());

		cust1 = customerService.get(custId);
		assertEquals(1, cust1.getSlips().size());

		// remove the slip position
		slip.removeFromPositions(slip.getPositions().get(0));
		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());
		assertEquals(0, slip.getPositions().size());

		slip.setCustomer(null);
		cashSlipService.update(slip);

		slip = cashSlipService.get(slip.getId());
		assertNotNull(slip);
		assertNull(slip.getCustomer());

		cust1 = customerService.get(custId);
		assertEquals(0, cust1.getSlips().size());

	}

	@Test
	public void testCascade_OneToMany_AddAndPersistDetail() throws Exception {
		setUp();

		int custId = 11;

		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		CashSlipDto slip = new CashSlipDto();
		slip.setCashier("Jörg");
		slip.setCustomer(cust1);
		slip.setNow(new Date());
		slip.setCurrentDay(new Date().toString());
		slip.setSerial(10l);
		slip.setTotal(100d);
		cashSlipService.update(slip);

		// Test slip has not positions
		slip = cashSlipService.get(slip.getId());
		assertEquals(0, slip.getPositions().size());

		// UPDATE position by positionService
		CashPositionDto pos1 = new CashPositionDto();
		pos1.setPrice(1000d);
		pos1.setQuantity(10d);
		slip.addToPositions(pos1);
		cashSlipPosService.update(pos1);

		// Test that slip was also updated
		slip = cashSlipService.get(slip.getId());
		assertEquals(1, slip.getPositions().size());

		// DELETE position by positionService
		cashSlipPosService.delete(pos1);

		// Test that slip was also updated
		slip = cashSlipService.get(slip.getId());
		assertEquals(0, slip.getPositions().size());
	}

	@Test
	public void testCascade_OneToMany_AddAndPersistContainer() throws Exception {
		setUp();

		int custId = 22;

		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		CashSlipDto slip = new CashSlipDto();
		slip.setCashier("Jörg");
		slip.setCustomer(cust1);
		slip.setNow(new Date());
		slip.setCurrentDay(new Date().toString());
		slip.setSerial(10l);
		slip.setTotal(100d);

		CashPositionDto pos1 = new CashPositionDto();
		pos1.setPrice(1000d);
		pos1.setQuantity(10d);
		slip.addToPositions(pos1);

		CashPositionDto pos2 = new CashPositionDto();
		pos2.setPrice(1000d);
		pos2.setQuantity(10d);
		slip.addToPositions(pos2);

		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());
		assertEquals(2, slip.getPositions().size());

		// remove a position
		slip.removeFromPositions(slip.getPositions().get(0));

		cashSlipService.update(slip);
		// Test that slip was also updated
		slip = cashSlipService.get(slip.getId());
		assertEquals(1, slip.getPositions().size());

		// remove a position and add a new one
		slip.removeFromPositions(slip.getPositions().get(0));

		CashPositionDto pos3 = new CashPositionDto();
		pos3.setPrice(1000d);
		pos3.setQuantity(10d);
		slip.addToPositions(pos3);

		cashSlipService.update(slip);

		slip = cashSlipService.get(slip.getId());
		assertEquals(1, slip.getPositions().size());
		assertEquals(pos3.getId(), slip.getPositions().get(0).getId());
	}

	@Test
	public void testCascade_Cascade_OneToMany_AddAndPersistContainer() throws Exception {
		setUp();

		int custId = 20;

		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		CashPaymentDto pay1 = new CashPaymentDto();
		pay1.setPayed(10d);
		pay1.setNow(new Date());
		cashPaymentService.update(pay1);
		pay1 = cashPaymentService.get(pay1.getId());

		CashPaymentDto pay2 = new CashPaymentDto();
		pay2.setPayed(10d);
		pay2.setNow(new Date());
		cashPaymentService.update(pay2);
		pay2 = cashPaymentService.get(pay2.getId());

		CashPaymentDto pay3 = new CashPaymentDto();
		pay3.setPayed(10d);
		pay3.setNow(new Date());
		cashPaymentService.update(pay3);
		pay3 = cashPaymentService.get(pay3.getId());

		CashPaymentDto pay4 = new CashPaymentDto();
		pay4.setPayed(10d);
		pay4.setNow(new Date());
		cashPaymentService.update(pay4);
		pay4 = cashPaymentService.get(pay4.getId());

		CashDrawerCurrencyDto container = new CashDrawerCurrencyDto();
		CashDrawerSumDto innerContainer = new CashDrawerSumDto();
		container.addToSums(innerContainer);

		// payments are already persisted
		//
		innerContainer.addToPayments(pay1);
		innerContainer.addToPayments(pay2);
		innerContainer.addToPayments(pay3);
		innerContainer.addToPayments(pay4);

		cashDrawerCurrencyService.update(container);
		container = cashDrawerCurrencyService.get(container.getId());

		innerContainer = container.getSums().get(0);
		assertEquals(4, innerContainer.getPayments().size());

	}

	@Test
	public void testManyToOne__setToNull() throws Exception {
		setUp();

		int custId = 1;

		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		CashSlipDto slip = new CashSlipDto();
		slip.setCashier("Jörg");
		slip.setCustomer(cust1);
		slip.setNow(new Date());
		slip.setCurrentDay(new Date().toString());
		slip.setSerial(10l);
		slip.setTotal(100d);

		CashPositionDto pos1 = new CashPositionDto();
		pos1.setPrice(1000d);
		pos1.setQuantity(10d);
		slip.addToPositions(pos1);

		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());
		assertEquals(1, slip.getPositions().size());

		cust1 = customerService.get(custId);
		assertEquals(1, cust1.getSlips().size());

		// set the customer to null and update slip
		slip.setCustomer(null);
		cashSlipService.update(slip);

		// test that the slip was removed from customer
		cust1 = customerService.get(custId);
		assertEquals(0, cust1.getSlips().size());

		slip = cashSlipService.get(slip.getId());
		assertNotNull(slip);
		assertNull(slip.getCustomer());

	}

	@Test
	public void errorRequired__testManyToOne__removeFromList() throws Exception {
		setUp();

		int custId = 1;
		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		CashSlipDto slip = new CashSlipDto();
		slip.setCashier("Jörg");
		slip.setCustomer(cust1);
		slip.setNow(new Date());
		slip.setCurrentDay(new Date().toString());
		slip.setSerial(10l);
		slip.setTotal(100d);

		CashPositionDto pos1 = new CashPositionDto();
		pos1.setPrice(1000d);
		pos1.setQuantity(10d);
		slip.addToPositions(pos1);

		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());
		assertEquals(1, slip.getPositions().size());

		cust1 = customerService.get(custId);
		assertEquals(1, cust1.getSlips().size());

		// set the customer to null and update slip
		cust1.removeFromSlips(cust1.internalGetSlips().get(0));
		customerService.update(cust1);

		slip = cashSlipService.get(slip.getId());
		assertNotNull(slip);

		/*
		 * ATTENTION!!! This is expected, since removing a slip from a customer
		 * and updating the customer has no effect!!
		 */
		try {
			assertNull(slip.getCustomer());
			fail();
		} catch (AssertionError e) {
			// expected
		}

		try {
			cust1 = customerService.get(custId);
			assertEquals(0, cust1.getSlips().size());
		} catch (AssertionError e) {
			// expected
		}
	}

	@Test
	public void test_ServiceMethod_findLast() throws Exception {
		setUp();

		int custId = 14;

		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		Date now = new Date();
		CashSlipDto slip = new CashSlipDto();
		slip.setCashier("Jörg");
		slip.setCustomer(cust1);
		slip.setNow(now);
		slip.setCurrentDay(new Date().toString());
		slip.setSerial(10l);
		slip.setTotal(100d);

		CashPositionDto pos1 = new CashPositionDto();
		pos1.setPrice(1000d);
		pos1.setQuantity(10d);
		slip.addToPositions(pos1);

		CashPositionDto pos2 = new CashPositionDto();
		pos2.setPrice(2000d);
		pos2.setQuantity(20d);
		slip.addToPositions(pos2);

		cashSlipService.update(slip);

		CashPositionDtoService castedService = (CashPositionDtoService) cashSlipPosService;
		CashPositionDto last = castedService.findLast(slip.getId());
		assertEquals(pos2.getPrice(), last.getPrice());

	}

	@Test
	public void test_ServiceMethod_calcSum() throws Exception {
		setUp();

		int custId = 15;

		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		Date now = new Date();
		CashSlipDto slip = new CashSlipDto();
		slip.setCashier("Jörg");
		slip.setCustomer(cust1);
		slip.setNow(now);
		slip.setCurrentDay(new Date().toString());
		slip.setSerial(10l);
		slip.setTotal(100d);

		CashPositionDto pos1 = new CashPositionDto();
		pos1.setPrice(1000d);
		pos1.setQuantity(10d);
		slip.addToPositions(pos1);

		CashPositionDto pos2 = new CashPositionDto();
		pos2.setPrice(2000d);
		pos2.setQuantity(20d);
		slip.addToPositions(pos2);

		cashSlipService.update(slip);

		CashSlipDtoService castedService = (CashSlipDtoService) cashSlipService;
		double result = castedService.calcSum(slip);
		assertEquals(3000d, result, 0);

	}

	@Test
	public void test_readAndUpdate() throws Exception {
		setUp();

		int custId = 16;
		String slipId = null;
		{
			// create customer
			McustomerDto cust1 = new McustomerDto();
			cust1.setId(custId);
			cust1.setFullname("customer1");
			customerService.update(cust1);
			cust1 = customerService.get(custId);

			Date now = new Date();
			CashSlipDto slip = new CashSlipDto();
			slipId = slip.getId();

			slip.setCashier("Jörg");
			slip.setCustomer(cust1);
			slip.setNow(now);
			slip.setCurrentDay(new Date().toString());
			slip.setSerial(10l);
			slip.setTotal(100d);

			CashPositionDto pos1 = new CashPositionDto();
			pos1.setPrice(1000d);
			pos1.setQuantity(10d);
			slip.addToPositions(pos1);

			CashPositionDto pos2 = new CashPositionDto();
			pos2.setPrice(2000d);
			pos2.setQuantity(20d);
			slip.addToPositions(pos2);

			cashSlipService.update(slip);
		}

		String idPos2 = null;
		{
			// do the update
			//
			CashSlipDto slip = cashSlipService.get(slipId);
			CashPositionDto pos2 = slip.getPositions().get(1);
			idPos2 = pos2.getId();
			pos2.setAmount(99999d);

			CashPositionDto pos3 = new CashPositionDto();
			pos3.setPrice(3000d);
			pos3.setQuantity(30d);
			slip.addToPositions(pos3);

			cashSlipService.update(slip);
		}

		{
			// test the update
			//
			CashSlipDto slip = cashSlipService.get(slipId);
			assertEquals(3, slip.getPositions().size());

			for (CashPositionDto dto : slip.getPositions()) {
				if (dto.getId().equals(idPos2)) {
					assertEquals(99999d, dto.getAmount(), 0);
				}
			}
		}

		{
			// do the update
			//
			CashSlipDto slip = cashSlipService.get(slipId);

			double counter = 0;
			for (CashPositionDto dto : slip.getPositions()) {
				counter++;
				dto.setAmount(counter);
				dto.setPrice(counter);
			}

			cashSlipService.update(slip);
		}

		{
			// do the update
			//
			CashSlipDto slip = cashSlipService.get(slipId);

			Set<Double> values = new HashSet<>();
			values.add(1d);
			values.add(2d);
			values.add(3d);

			for (CashPositionDto dto : slip.getPositions()) {
				assertEquals(dto.getAmount(), dto.getPrice(), 0);
				values.remove(dto.getAmount());
			}

			assertTrue(values.isEmpty());
		}
	}

	@Test
	public void testCascade_withSubDetails() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(17);
		assertSubDetailsStructure(slip);
	}

	private void assertSubDetailsStructure(CashSlipDto slip) {
		assertEquals(3, slip.getPositions().size());

		// test size without resolving
		for (CashPositionDto pos : slip.getPositions()) {
			assertEquals(3, pos.getSubPositions().size());
		}

		// test size with resolving
		for (CashPositionDto pos : slip.getPositions()) {
			// forces resolve
			pos.getSubPositions().get(0);
			assertEquals(3, pos.getSubPositions().size());
		}

		// do again
		for (CashPositionDto pos : slip.getPositions()) {
			assertEquals(3, pos.getSubPositions().size());
		}

	}

	@Test
	public void testCascade_withSubDetails_dirtyState() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(18);
		assertFalse(slip.isDirty());

		CashPositionDto pos = slip.getPositions().get(0);
		assertFalse(pos.isDirty());

		CashSubPositionDto subPos = pos.getSubPositions().get(0);
		assertFalse(subPos.isDirty());

		subPos.setQuantity(123d);
		assertTrue(slip.isDirty());
		assertTrue(pos.isDirty());
		assertTrue(subPos.isDirty());

		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());

		assertSubDetailsStructure(slip);
		pos = slip.getPositions().get(0);
		subPos = pos.getSubPositions().get(0);
		assertEquals(123d, subPos.getQuantity(), 0);
		assertFalse(subPos.isDirty());

	}

	@Test
	public void testCascade_withSubDetails_AddNew() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(19);
		assertFalse(slip.isDirty());

		CashPositionDto pos = slip.getPositions().get(0);
		assertFalse(pos.isDirty());

		CashSubPositionDto subPos = pos.getSubPositions().get(0);
		assertFalse(subPos.isDirty());

		subPos.setQuantity(123d);
		assertTrue(subPos.isDirty());
		assertTrue(pos.isDirty());
		assertTrue(slip.isDirty());

		{
			CashSubPositionDto newSubPos = new CashSubPositionDto();
			newSubPos.setPrice(1004d);
			newSubPos.setQuantity(14d);
			pos.addToSubPositions(newSubPos);
		}

		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());

		assertEquals(3, slip.getPositions().size());
		assertEquals(4, slip.getPositions().get(0).getSubPositions().size());
		assertEquals(3, slip.getPositions().get(1).getSubPositions().size());
		assertEquals(3, slip.getPositions().get(2).getSubPositions().size());

	}

	private CashSlipDto createSubDetailsStructure(int custId) {
		// create customer
		McustomerDto cust1 = new McustomerDto();
		cust1.setId(custId);
		cust1.setFullname("customer1");
		customerService.update(cust1);
		cust1 = customerService.get(custId);

		CashSlipDto slip = new CashSlipDto();
		slip.setCashier("Jörg");
		slip.setCustomer(cust1);
		slip.setNow(new Date());
		slip.setCurrentDay(new Date().toString());
		slip.setSerial(10l);
		slip.setTotal(100d);

		{
			CashPositionDto pos = new CashPositionDto();
			pos.setPrice(1000d);
			pos.setQuantity(10d);
			pos.setAmount(10000d);
			slip.addToPositions(pos);

			{
				CashSubPositionDto subPos = new CashSubPositionDto();
				subPos.setPrice(1001d);
				subPos.setQuantity(11d);
				pos.addToSubPositions(subPos);
			}

			{
				CashSubPositionDto subPos = new CashSubPositionDto();
				subPos.setPrice(1002d);
				subPos.setQuantity(12d);
				pos.addToSubPositions(subPos);
			}

			{
				CashSubPositionDto subPos = new CashSubPositionDto();
				subPos.setPrice(1003d);
				subPos.setQuantity(13d);
				pos.addToSubPositions(subPos);
			}
		}

		{
			CashPositionDto pos = new CashPositionDto();
			pos.setPrice(2000d);
			pos.setQuantity(20d);
			slip.addToPositions(pos);

			{
				CashSubPositionDto subPos = new CashSubPositionDto();
				subPos.setPrice(2001d);
				subPos.setQuantity(21d);
				pos.addToSubPositions(subPos);
			}

			{
				CashSubPositionDto subPos = new CashSubPositionDto();
				subPos.setPrice(2002d);
				subPos.setQuantity(22d);
				pos.addToSubPositions(subPos);
			}

			{
				CashSubPositionDto subPos = new CashSubPositionDto();
				subPos.setPrice(2003d);
				subPos.setQuantity(23d);
				pos.addToSubPositions(subPos);
			}
		}
		{
			CashPositionDto pos = new CashPositionDto();
			pos.setPrice(3000d);
			pos.setQuantity(30d);
			slip.addToPositions(pos);

			{
				CashSubPositionDto subPos = new CashSubPositionDto();
				subPos.setPrice(3001d);
				subPos.setQuantity(31d);
				pos.addToSubPositions(subPos);
			}

			{
				CashSubPositionDto subPos = new CashSubPositionDto();
				subPos.setPrice(3002d);
				subPos.setQuantity(32d);
				pos.addToSubPositions(subPos);
			}

			{
				CashSubPositionDto subPos = new CashSubPositionDto();
				subPos.setPrice(3003d);
				subPos.setQuantity(33d);
				pos.addToSubPositions(subPos);
			}
		}

		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());
		return slip;
	}

	@Test
	public void testOppositeContainerIsSame() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(90);

		CashPositionDto pos = slip.getPositions().get(0);
		CashSubPositionDto subPos = pos.getSubPositions().get(0);

		Assert.assertSame(slip, pos.getSlip());
		Assert.assertSame(pos, subPos.getParent());
	}

	@Test
	public void testContainsWithUnresolvedList() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(91);
		CashSlipDto otherSlip = cashSlipService.get(slip.getId());

		CashPositionDto pos = slip.getPositions().get(0);
		CashSubPositionDto subPos = pos.getSubPositions().get(0);

		// test without adding or removing
		//
		AbstractOppositeDtoList<CashPositionDto> positions = (AbstractOppositeDtoList<CashPositionDto>) otherSlip
				.internalGetPositions();
		assertFalse(positions.isResolved());
		assertTrue(positions.contains(pos));
		assertFalse(positions.isResolved());

		CashPositionDto otherPos = positions.get(0);
		assertTrue(positions.isResolved());

		AbstractOppositeDtoList<CashSubPositionDto> subs = (AbstractOppositeDtoList<CashSubPositionDto>) otherPos
				.internalGetSubPositions();
		assertFalse(subs.isResolved());
		assertTrue(subs.contains(subPos));
		assertFalse(subs.isResolved());

		// test with added dtos
		//
		otherSlip = cashSlipService.get(slip.getId());
		AbstractOppositeDtoList<CashPositionDto> otherPositions = (AbstractOppositeDtoList<CashPositionDto>) otherSlip
				.internalGetPositions();
		CashPositionDto newPos = new CashPositionDto();
		otherSlip.addToPositions(newPos);
		assertFalse(otherPositions.isResolved());
		assertTrue(otherPositions.contains(newPos));
		assertTrue(otherPositions.contains(pos));

		// remove the newPos again
		otherSlip.removeFromPositions(newPos);
		assertFalse(otherPositions.contains(newPos));
		assertTrue(otherPositions.contains(pos));

		// remove the pos again
		otherPos = otherSlip.getPositions().get(0);
		otherSlip.removeFromPositions(otherPos);
		assertFalse(otherPositions.contains(otherPos));

	}

	@Test
	public void testContainsWithResolvedList() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(92);
		CashSlipDto otherSlip = cashSlipService.get(slip.getId());

		// resolve all lists
		otherSlip.getPositions().get(0).getSubPositions().get(0);
		otherSlip.getPositions().get(1).getSubPositions().get(0);
		otherSlip.getPositions().get(2).getSubPositions().get(0);

		CashPositionDto pos = slip.getPositions().get(0);
		CashSubPositionDto subPos = pos.getSubPositions().get(0);

		// test without adding or removing
		//
		AbstractOppositeDtoList<CashPositionDto> otherPositions = (AbstractOppositeDtoList<CashPositionDto>) otherSlip
				.internalGetPositions();
		assertTrue(otherPositions.isResolved());
		assertTrue(otherPositions.contains(pos));
		assertTrue(otherPositions.isResolved());

		CashPositionDto otherPos = otherPositions.get(0);
		assertTrue(otherPositions.isResolved());

		AbstractOppositeDtoList<CashSubPositionDto> otherSubs = (AbstractOppositeDtoList<CashSubPositionDto>) otherPos
				.internalGetSubPositions();
		assertTrue(otherSubs.isResolved());
		assertTrue(otherSubs.contains(subPos));
		assertTrue(otherSubs.isResolved());

		// test with added dtos
		//
		otherSlip = cashSlipService.get(slip.getId());
		// resolve all lists
		otherSlip.getPositions().get(0).getSubPositions().get(0);
		otherSlip.getPositions().get(1).getSubPositions().get(0);
		otherSlip.getPositions().get(2).getSubPositions().get(0);
		otherPositions = (AbstractOppositeDtoList<CashPositionDto>) otherSlip.internalGetPositions();
		CashPositionDto newPos = new CashPositionDto();
		otherSlip.addToPositions(newPos);
		assertTrue(otherPositions.isResolved());
		assertTrue(otherPositions.contains(newPos));
		assertTrue(otherPositions.contains(pos));

		// remove the newPos again
		otherSlip.removeFromPositions(newPos);
		assertFalse(otherPositions.contains(newPos));
		assertTrue(otherPositions.contains(pos));

		// remove the pos again
		otherPos = otherSlip.getPositions().get(0);
		otherSlip.removeFromPositions(otherPos);
		assertFalse(otherPositions.contains(otherPos));

	}

	@Test
	public void testSizeWithUnresolvedList() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(93);

		// test without adding or removing
		//
		AbstractOppositeDtoList<CashPositionDto> positions = (AbstractOppositeDtoList<CashPositionDto>) slip
				.internalGetPositions();
		assertFalse(positions.isResolved());
		assertEquals(3, positions.size());
		assertFalse(positions.isResolved());

		CashPositionDto otherPos = positions.get(0);
		assertTrue(positions.isResolved());

		AbstractOppositeDtoList<CashSubPositionDto> subs = (AbstractOppositeDtoList<CashSubPositionDto>) otherPos
				.internalGetSubPositions();
		assertFalse(subs.isResolved());
		assertEquals(3, positions.size());
		assertFalse(subs.isResolved());

		// test with added and removed dtos
		//
		slip = cashSlipService.get(slip.getId());
		positions = (AbstractOppositeDtoList<CashPositionDto>) slip.internalGetPositions();
		CashPositionDto newPos = new CashPositionDto();
		// add will resolve the list -> PropertyChangeSupport
		slip.addToPositions(newPos);
		assertFalse(positions.isResolved());
		assertEquals(4, positions.size());

		// remove the newPos again
		slip.removeFromPositions(newPos);
		assertEquals(3, positions.size());

		// remove the pos again
		otherPos = slip.getPositions().get(0);
		slip.removeFromPositions(otherPos);
		assertEquals(2, positions.size());

	}

	@Test
	public void testSizeWithResolvedList() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(94);

		// resolve all lists
		slip.getPositions().get(0).getSubPositions().get(0);
		slip.getPositions().get(1).getSubPositions().get(0);
		slip.getPositions().get(2).getSubPositions().get(0);

		// test without adding or removing
		//
		AbstractOppositeDtoList<CashPositionDto> positions = (AbstractOppositeDtoList<CashPositionDto>) slip
				.internalGetPositions();
		assertTrue(positions.isResolved());
		assertEquals(3, positions.size());
		assertTrue(positions.isResolved());

		CashPositionDto otherPos = positions.get(0);
		assertTrue(positions.isResolved());

		AbstractOppositeDtoList<CashSubPositionDto> subs = (AbstractOppositeDtoList<CashSubPositionDto>) otherPos
				.internalGetSubPositions();
		assertTrue(subs.isResolved());
		assertEquals(3, positions.size());
		assertTrue(subs.isResolved());

		// test with added and removed dtos
		//
		slip = cashSlipService.get(slip.getId());

		// resolve all lists
		slip.getPositions().get(0).getSubPositions().get(0);
		slip.getPositions().get(1).getSubPositions().get(0);
		slip.getPositions().get(2).getSubPositions().get(0);

		positions = (AbstractOppositeDtoList<CashPositionDto>) slip.internalGetPositions();
		CashPositionDto newPos = new CashPositionDto();
		// add will resolve the list -> PropertyChangeSupport
		slip.addToPositions(newPos);
		assertTrue(positions.isResolved());
		assertEquals(4, positions.size());

		// remove the newPos again
		slip.removeFromPositions(newPos);
		assertEquals(3, positions.size());

		// remove the pos again
		otherPos = slip.getPositions().get(0);
		slip.removeFromPositions(otherPos);
		assertEquals(2, positions.size());

	}

	@Test
	public void testAddRemoveTransient() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(95);

		AbstractOppositeDtoList<CashPositionDto> positions = (AbstractOppositeDtoList<CashPositionDto>) slip
				.internalGetPositions();
		assertEquals(3, positions.size());

		CashPositionDto newPos = new CashPositionDto();

		// add
		slip.addToPositions(newPos);
		assertFalse(positions.isResolved());
		assertEquals(4, positions.size());
		assertEquals(1, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		assertEquals(0, positions.getUpdated().size());
		assertEquals(0, positions.getAddedToSuper().size());

		// update
		newPos.setAmount(100d);

		assertFalse(positions.isResolved());
		assertEquals(4, positions.size());
		assertEquals(1, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		// update is still 0 since newPos is transient
		assertEquals(0, positions.getUpdated().size());
		assertEquals(0, positions.getAddedToSuper().size());

		// remove
		slip.removeFromPositions(newPos);
		assertFalse(positions.isResolved());
		assertEquals(3, positions.size());
		assertEquals(0, positions.getAdded().size());
		// attention! removed is 0 since list not resolved.
		assertEquals(0, positions.getRemoved().size());
		assertEquals(0, positions.getUpdated().size());
		assertEquals(0, positions.getAddedToSuper().size());

		// now resolve
		//
		positions.get(0);

		// work with transient dto here
		//

		// add
		slip.addToPositions(newPos);
		assertTrue(positions.isResolved());
		assertEquals(4, positions.size());
		assertEquals(1, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		assertEquals(0, positions.getUpdated().size());
		assertEquals(4, positions.getAddedToSuper().size());

		// update
		newPos.setAmount(100d);
		assertTrue(positions.isResolved());
		assertEquals(4, positions.size());
		assertEquals(1, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		// update is still 0 since newPos is transient
		assertEquals(0, positions.getUpdated().size());
		assertEquals(4, positions.getAddedToSuper().size());

		// remove
		slip.removeFromPositions(newPos);
		assertTrue(positions.isResolved());
		assertEquals(3, positions.size());
		assertEquals(0, positions.getAdded().size());
		// attention! removed is 0 since list not resolved.
		assertEquals(0, positions.getRemoved().size());
		assertEquals(0, positions.getUpdated().size());
		assertEquals(3, positions.getAddedToSuper().size());

	}

	@Test
	public void testAddRemovePersistent() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(96);

		AbstractOppositeDtoList<CashPositionDto> positions = (AbstractOppositeDtoList<CashPositionDto>) slip
				.internalGetPositions();
		assertEquals(3, positions.size());

		// now resolve
		//
		CashPositionDto perPos = positions.get(0);

		// work with persistent dto here
		//

		// update
		perPos.setAmount(100d);
		assertTrue(positions.isResolved());
		assertEquals(3, positions.size());
		assertEquals(0, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		// update is 1 since perPos is persistent
		assertEquals(1, positions.getUpdated().size());
		assertEquals(3, positions.getAddedToSuper().size());

		// remove
		slip.removeFromPositions(perPos);
		assertTrue(positions.isResolved());
		assertEquals(2, positions.size());
		assertEquals(0, positions.getAdded().size());
		// attention! removed is 1 since dto is persistent.
		assertEquals(1, positions.getRemoved().size());
		// updated went to 0 since dto removed
		assertEquals(0, positions.getUpdated().size());
		assertEquals(2, positions.getAddedToSuper().size());

		// add again
		slip.addToPositions(perPos);
		assertTrue(positions.isResolved());
		assertEquals(3, positions.size());
		// added will be 0 since the persPos is persistent
		assertEquals(0, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		// update is 1 since perPos was removed and added again
		assertEquals(1, positions.getUpdated().size());
		assertEquals(3, positions.getAddedToSuper().size());

		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());

		perPos = slip.getPositions().get(0);
		assertEquals(100d, perPos.getAmount(), 0d);
	}

	@Test
	public void testAddRemoveMixed() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(97);

		AbstractOppositeDtoList<CashPositionDto> positions = (AbstractOppositeDtoList<CashPositionDto>) slip
				.internalGetPositions();
		assertEquals(3, positions.size());

		CashPositionDto newPos = new CashPositionDto();

		// add a transient before resolving
		slip.addToPositions(newPos);
		assertFalse(positions.isResolved());
		assertEquals(4, positions.size());
		assertEquals(1, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		// update is 1 since perPos is persistent
		assertEquals(0, positions.getUpdated().size());
		assertEquals(0, positions.getAddedToSuper().size());

		// now resolve
		//
		CashPositionDto perPos = positions.get(0);
		CashPositionDto perPos2 = positions.get(1);

		// work with persistent dto here
		//

		// update persistent
		perPos.setAmount(100d);
		assertTrue(positions.isResolved());
		assertEquals(4, positions.size());
		assertEquals(1, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		// update is 1 since perPos is persistent
		assertEquals(1, positions.getUpdated().size());
		assertEquals(4, positions.getAddedToSuper().size());

		// remove persistent
		slip.removeFromPositions(perPos);
		assertTrue(positions.isResolved());
		assertEquals(3, positions.size());
		assertEquals(1, positions.getAdded().size());
		// attention! removed is 1 since dto is persistent.
		assertEquals(1, positions.getRemoved().size());
		// updated went to 0 since dto removed
		assertEquals(0, positions.getUpdated().size());
		assertEquals(3, positions.getAddedToSuper().size());

		// add another transient
		CashPositionDto newPos2 = new CashPositionDto();
		slip.addToPositions(newPos2);
		assertTrue(positions.isResolved());
		assertEquals(4, positions.size());
		assertEquals(2, positions.getAdded().size());
		assertEquals(1, positions.getRemoved().size());
		// update is 1 since perPos is persistent
		assertEquals(0, positions.getUpdated().size());
		assertEquals(4, positions.getAddedToSuper().size());

		// add the persistent again
		slip.addToPositions(perPos);
		assertTrue(positions.isResolved());
		assertEquals(5, positions.size());
		assertEquals(3, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		assertEquals(0, positions.getUpdated().size());
		assertEquals(5, positions.getAddedToSuper().size());

		// add another transient
		CashPositionDto newPos3 = new CashPositionDto();
		slip.addToPositions(newPos3);
		assertTrue(positions.isResolved());
		assertEquals(6, positions.size());
		assertEquals(4, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		assertEquals(0, positions.getUpdated().size());
		assertEquals(6, positions.getAddedToSuper().size());

		// remove transient
		slip.removeFromPositions(newPos2);
		assertTrue(positions.isResolved());
		assertEquals(5, positions.size());
		assertEquals(3, positions.getAdded().size());
		assertEquals(0, positions.getRemoved().size());
		assertEquals(0, positions.getUpdated().size());
		assertEquals(5, positions.getAddedToSuper().size());

		// remove persistent 2
		slip.removeFromPositions(perPos2);
		assertTrue(positions.isResolved());
		assertEquals(4, positions.size());
		assertEquals(3, positions.getAdded().size());
		// attention! removed is 1 since dto is persistent.
		assertEquals(1, positions.getRemoved().size());
		// updated went to 0 since dto removed
		assertEquals(0, positions.getUpdated().size());
		assertEquals(4, positions.getAddedToSuper().size());

		Set<String> uuids = new HashSet<>();
		for (CashPositionDto pos : positions.getAddedToSuper()) {
			if (uuids.contains(pos.getId())) {
				System.out.println(pos);
			}
			uuids.add(pos.getId());
		}

		cashSlipService.update(slip);
		slip = cashSlipService.get(slip.getId());

		perPos = slip.getPositions().get(0);
		assertEquals(100d, perPos.getAmount(), 0d);
		assertEquals(4, slip.getPositions().size());
	}

	@Test
	public void testCopyList() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(98);

		AbstractOppositeDtoList<CashPositionDto> positions = (AbstractOppositeDtoList<CashPositionDto>) slip
				.internalGetPositions();

		AbstractOppositeDtoList<CashPositionDto> copy = positions.copy();
		try {
			copy.add(new CashPositionDto());
			fail("IllegalStateException required.");
		} catch (IllegalStateException ex) {

		}

		try {
			copy.remove(new CashPositionDto());
			fail("IllegalStateException required.");
		} catch (IllegalStateException ex) {

		}

		try {
			copy.addAll(Collections.singletonList(new CashPositionDto()));
			fail("IllegalStateException required.");
		} catch (IllegalStateException ex) {

		}

		try {
			copy.removeAll(Collections.singletonList(new CashPositionDto()));
			fail("IllegalStateException required.");
		} catch (IllegalStateException ex) {

		}

		try {
			copy.set(1, new CashPositionDto());
			fail("IllegalStateException required.");
		} catch (IllegalStateException ex) {

		}

		try {
			copy.remove(1);
			fail("IllegalStateException required.");
		} catch (IllegalStateException ex) {

		}

		try {
			copy.sort((o1, o2) -> -1);
			fail("IllegalStateException required.");
		} catch (IllegalStateException ex) {

		}
	}

	@Test
	public void testEqualsWithUnresolvedList() throws Exception {
		setUp();

		CashSlipDto slip = createSubDetailsStructure(99);

		// test without adding or removing
		//
		AbstractOppositeDtoList<CashPositionDto> positions = (AbstractOppositeDtoList<CashPositionDto>) slip
				.internalGetPositions();
		assertFalse(positions.isResolved());
		assertEquals(3, positions.size());

		AbstractOppositeDtoList<CashPositionDto> copy = positions.copy();
		assertFalse(copy.isResolved());

		assertTrue(copy.equals(positions));
		assertTrue(positions.equals(copy));
		assertFalse(positions.isResolved());
		assertFalse(copy.isResolved());

		CashPositionDto newDto = new CashPositionDto();
		positions.add(newDto);
		assertFalse(copy.equals(positions));
		assertFalse(positions.equals(copy));
		assertFalse(positions.isResolved());
		assertFalse(copy.isResolved());

		positions.remove(newDto);
		assertTrue(copy.equals(positions));
		assertTrue(positions.equals(copy));
		assertFalse(positions.isResolved());
		assertFalse(copy.isResolved());

		// resolve positions
		CashPositionDto perPos = positions.get(0);
		assertTrue(copy.equals(positions));
		assertTrue(positions.equals(copy));
		assertTrue(positions.isResolved());
		assertFalse(copy.isResolved());

		// resolve copy
		copy.get(0);
		assertTrue(copy.equals(positions));
		assertTrue(positions.equals(copy));
		assertTrue(positions.isResolved());
		assertTrue(copy.isResolved());

		// remove persPos
		positions.remove(perPos);
		assertFalse(copy.equals(positions));
		assertFalse(positions.equals(copy));
		assertTrue(positions.isResolved());
		assertTrue(copy.isResolved());

		// add persPos again
		positions.add(perPos);
		// false since persPos was added to updated
		assertFalse(copy.equals(positions));
		// false since persPos was added to updated
		assertFalse(positions.equals(copy));
		assertTrue(positions.isResolved());
		assertTrue(copy.isResolved());

	}

}
