/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.osbp.dsl.entity.xtext.grammar.tests;

import com.google.inject.Inject;
import java.util.Map;
import org.eclipse.osbp.dsl.entity.xtext.grammar.tests.InjectorProvider;
import org.eclipse.osbp.dsl.entity.xtext.grammar.tests.ParseHelper;
import org.eclipse.osbp.dsl.semantic.entity.LEntityModel;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(XtextRunner.class)
@InjectWith(InjectorProvider.class)
@SuppressWarnings("all")
public class GrammarTests {
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Inject
  @Extension
  private ParseHelper<LEntityModel> _parseHelper;
  
  private Map<String, Issue> result;
  
  private Map<String, Issue> result2;
  
  @Test
  public void id_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__MISSING_ID is undefined"
      + "\nThe method or field CODE__MISSING_ID is undefined"
      + "\nThe method or field CODE__DUPLICATE_ID is undefined"
      + "\nThe method or field CODE__DUPLICATE_ID is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void id_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_ID is undefined"
      + "\nThe method or field CODE__DUPLICATE_ID is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void version_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_VERSION is undefined"
      + "\nThe method or field CODE__DUPLICATE_VERSION is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void version_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_VERSION is undefined"
      + "\nThe method or field CODE__DUPLICATE_VERSION is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void featureName_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void featureName_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void featureName_3() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void featureName_4() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void featureName_5() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nThe method or field CODE__DUPLICATE_PROPERTY_NAME is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void duplicatePackage_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_LPACKAGE_IN_FILE is undefined"
      + "\nThe method or field CODE__DUPLICATE_LPACKAGE_IN_FILE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void duplicateType_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_LTYPE_IN_PROJECT is undefined"
      + "\nThe method or field CODE__DUPLICATE_LTYPE_IN_PROJECT is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void duplicateType_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_LDATATYPE_IN_PACKAGE is undefined"
      + "\nThe method or field CODE__DUPLICATE_LDATATYPE_IN_PACKAGE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void duplicateType_3() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package org.eclipse.osbp.dsl.^entity.xtext.tests1 {");
      _builder.newLine();
      _builder.newLine();
      _builder.append("datatype long jvmType Long as primitive;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("entity Test1 {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("id long xxyy;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("var long abc;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.append("package org.eclipse.osbp.dsl.^entity.xtext.tests2 {");
      _builder.newLine();
      _builder.newLine();
      _builder.append("datatype long jvmType Long as primitive;\t\t\t\t// error: duplicate type (in other package)");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("entity Test2 {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("id long cde;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("var long ghi;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final Function1<Issue, String> _function = new Function1<Issue, String>() {
        @Override
        public String apply(final Issue it) {
          return it.getCode();
        }
      };
      this.result = IterableExtensions.<String, Issue>toMap(this._validationTestHelper.validate(this._parseHelper.parse(_builder)), _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void invalidPrimitive_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__NOT_A_VALID_PRIMITIVE is undefined"
      + "\nThe method or field CODE__NOT_A_VALID_PRIMITIVE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void invalidPrimitive_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__NOT_A_VALID_PRIMITIVE is undefined"
      + "\nThe method or field CODE__NOT_A_VALID_PRIMITIVE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void javaKeyword() {
    throw new Error("Unresolved compilation problems:"
      + "\nCommonGrammarJavaValidator cannot be resolved to a type."
      + "\njavakeywords cannot be resolved");
  }
  
  @Test
  public void bidirectionalAssociation_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__MISSING_OPPOSITE_REFERENCE is undefined"
      + "\nThe method or field CODE__MISSING_OPPOSITE_REFERENCE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void bidirectionalAssociation_2() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package org.eclipse.osbp.dsl.^entity.xtext.tests {");
      _builder.newLine();
      _builder.newLine();
      _builder.append("datatype long jvmType Long as primitive;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("entity Test {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("id long id;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("ref Test2[*] mytests opposite nosuchthing;\t\t// error: type of opposite ref cannot be resolved");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("entity Test2 { ");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("id long id;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("var long mynumber;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("} ");
      _builder.newLine();
      final Function1<Issue, String> _function = new Function1<Issue, String>() {
        @Override
        public String apply(final Issue it) {
          return it.getCode();
        }
      };
      this.result = IterableExtensions.<String, Issue>toMap(this._validationTestHelper.validate(this._parseHelper.parse(_builder)), _function);
      Assert.assertSame(this.result.values().iterator().next().getSeverity(), Severity.ERROR);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void bidirectionalAssociation_3() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package org.eclipse.osbp.dsl.^entity.xtext.tests {");
      _builder.newLine();
      _builder.newLine();
      _builder.append("datatype long jvmType Long as primitive;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("entity Test {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("id long id;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("ref Test2[*]\t\t\t\t\t\t\t\t\t// error: no opposite reference given");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("bean Test2 { ");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("var long mynumber;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("} ");
      _builder.newLine();
      final Function1<Issue, String> _function = new Function1<Issue, String>() {
        @Override
        public String apply(final Issue it) {
          return it.getCode();
        }
      };
      this.result = IterableExtensions.<String, Issue>toMap(this._validationTestHelper.validate(this._parseHelper.parse(_builder)), _function);
      Assert.assertSame(this.result.values().iterator().next().getSeverity(), Severity.ERROR);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void bidirectionalAssociation_4() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package org.eclipse.osbp.dsl.^entity.xtext.tests {");
      _builder.newLine();
      _builder.newLine();
      _builder.append("datatype long jvmType Long as primitive;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("entity Test {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("id long id;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("ref Test2[*] mytests opposite nosuchthing;\t\t// error: type of opposite ref cannot be resolved");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("bean Test2 { ");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("var long mynumber;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("} ");
      _builder.newLine();
      final Function1<Issue, String> _function = new Function1<Issue, String>() {
        @Override
        public String apply(final Issue it) {
          return it.getCode();
        }
      };
      this.result = IterableExtensions.<String, Issue>toMap(this._validationTestHelper.validate(this._parseHelper.parse(_builder)), _function);
      Assert.assertSame(this.result.values().iterator().next().getSeverity(), Severity.ERROR);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void manyToMany_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__MANY_TO_MANY__NOT_SUPPORTED is undefined"
      + "\nThe method or field CODE__MANY_TO_MANY__NOT_SUPPORTED is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void manyToMany_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__MANY_TO_MANY__NOT_SUPPORTED is undefined"
      + "\nThe method or field CODE__MANY_TO_MANY__NOT_SUPPORTED is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void cascade_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__BIDIRECTIONAL_CASCADE_INVALID is undefined"
      + "\nThe method or field CODE__BIDIRECTIONAL_CASCADE_INVALID is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void cascade_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__BIDIRECTIONAL_CASCADE_INVALID is undefined"
      + "\nThe method or field CODE__BIDIRECTIONAL_CASCADE_INVALID is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void cascade_3() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__CASCADE_DIRECTION_INVALID is undefined"
      + "\nThe method or field CODE__CASCADE_DIRECTION_INVALID is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void cascade_4() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__CASCADE_DIRECTION_INVALID is undefined"
      + "\nThe method or field CODE__CASCADE_DIRECTION_INVALID is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void inheritance_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DIFFERING_INHERITANCE_FROM_SUPERTYPE is undefined"
      + "\nThe method or field CODE__DIFFERING_INHERITANCE_FROM_SUPERTYPE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void inheritance_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DIFFERING_INHERITANCE_FROM_SUPERTYPE is undefined"
      + "\nThe method or field CODE__DIFFERING_INHERITANCE_FROM_SUPERTYPE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void inheritance_3() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DIFFERING_INHERITANCE_FROM_SUPERTYPE is undefined"
      + "\nThe method or field CODE__DIFFERING_INHERITANCE_FROM_SUPERTYPE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void inheritance_4() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DIFFERING_INHERITANCE_FROM_SUPERTYPE is undefined"
      + "\nThe method or field CODE__DIFFERING_INHERITANCE_FROM_SUPERTYPE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void inheritance_5() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__INHERITANCE_DISCRIMINATOR_VALUE_NOT_UNIQUE is undefined"
      + "\nThe method or field CODE__INHERITANCE_DISCRIMINATOR_VALUE_NOT_UNIQUE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void persistence_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_PERSISTENCE is undefined"
      + "\nThe method or field CODE__DUPLICATE_PERSISTENCE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void persistence_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__DUPLICATE_PERSISTENCE is undefined"
      + "\nThe method or field CODE__DUPLICATE_PERSISTENCE is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void persistence_3() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package org.eclipse.osbp.dsl.^entity.xtext.tests {");
      _builder.newLine();
      _builder.newLine();
      _builder.append("datatype long jvmType Long as primitive;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("entity Test {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("tableName testtable;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("id long id;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("entity Test2 extends Test {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("tableName testTable;\t\t\t\t\t\t\t// no error: CamelCase recognized -> TEST_TABLE");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      final Function1<Issue, String> _function = new Function1<Issue, String>() {
        @Override
        public String apply(final Issue it) {
          return it.getCode();
        }
      };
      this.result = IterableExtensions.<String, Issue>toMap(this._validationTestHelper.validate(this._parseHelper.parse(_builder)), _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void historized_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__HISTORIZED_IN_SUBCLASS is undefined"
      + "\nThe method or field CODE__HISTORIZED_IN_SUBCLASS is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void historized_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__HISTORIZED_IN_SUBCLASS is undefined"
      + "\nThe method or field CODE__HISTORIZED_IN_SUBCLASS is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void timedependent_1() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__TIMEDEPENDENT_IN_SUBCLASS is undefined"
      + "\nThe method or field CODE__TIMEDEPENDENT_IN_SUBCLASS is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void timedependent_2() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__TIMEDEPENDENT_IN_SUBCLASS is undefined"
      + "\nThe method or field CODE__TIMEDEPENDENT_IN_SUBCLASS is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void historized_id() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__MISSING_ID_FOR_VERSIONED is undefined"
      + "\nThe method or field CODE__MISSING_ID_FOR_VERSIONED is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  @Test
  public void timedependent_id() {
    throw new Error("Unresolved compilation problems:"
      + "\nThe method or field CODE__MISSING_ID_FOR_VERSIONED is undefined"
      + "\nThe method or field CODE__MISSING_ID_FOR_VERSIONED is undefined"
      + "\nseverity cannot be resolved"
      + "\nassertSame cannot be resolved"
      + "\nlineNumber cannot be resolved"
      + "\nassertEquals cannot be resolved");
  }
  
  /**
   * Helper methods
   */
  public int lineNumber(final String code) {
    return (this.result.get(code).getLineNumber()).intValue();
  }
  
  public Severity severity(final String code) {
    return this.result.get(code).getSeverity();
  }
}
