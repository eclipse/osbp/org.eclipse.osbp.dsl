/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.extensions;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.osbp.dsl.entity.xtext.extensions.NamingExtensions;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LDiscriminatorType;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityInheritanceStrategy;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.dsl.semantic.entity.LOperation;
import org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy;
import org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy;
import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityFactory;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class ModelExtensions extends org.eclipse.osbp.dsl.common.xtext.extensions.ModelExtensions {
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  @Inject
  @Extension
  private NamingExtensions _namingExtensions;
  
  @Inject
  private TypeReferences references;
  
  protected JvmTypeReference _toTypeReference(final LEntityReference prop) {
    if (((prop.getType() == null) || prop.getType().eIsProxy())) {
      return this.references.createAnyTypeReference(prop);
    }
    return this.references.getTypeForName(this._iQualifiedNameProvider.getFullyQualifiedName(prop.getType()).toString(), prop, null);
  }
  
  protected JvmTypeReference _toTypeReference(final LEntityAttribute prop) {
    final LScalarType lType = prop.getType();
    if ((((lType instanceof LEntity) || (lType instanceof LBean)) || (lType instanceof LEnum))) {
      return this.references.getTypeForName(this._iQualifiedNameProvider.getFullyQualifiedName(lType).toString(), prop, null);
    }
    LScalarType _type = prop.getType();
    JvmTypeReference _typeReference = null;
    if (_type!=null) {
      _typeReference=this.toTypeReference(_type);
    }
    return _typeReference;
  }
  
  protected JvmTypeReference _toTypeReference(final LBeanReference prop) {
    return this.toTypeReference(prop.getType());
  }
  
  protected JvmTypeReference _toTypeReference(final LOperation prop) {
    return this._jvmTypesBuilder.cloneWithProxies(prop.getType());
  }
  
  protected boolean _isUUID(final LFeature prop) {
    return false;
  }
  
  protected boolean _isUUID(final LEntityAttribute prop) {
    return prop.isUuid();
  }
  
  protected boolean _isID(final LEntityAttribute prop) {
    return prop.isId();
  }
  
  protected LFeature _getResolvedOpposite(final LEntityReference prop) {
    LEntityReference _xifexpression = null;
    LEntityReference _opposite = prop.getOpposite();
    boolean _tripleNotEquals = (_opposite != null);
    if (_tripleNotEquals) {
      return prop.getOpposite();
    } else {
      LEntityReference _xifexpression_1 = null;
      LEntity _type = prop.getType();
      if ((_type instanceof LEntity)) {
        LEntityReference _xblockexpression = null;
        {
          LEntity _type_1 = prop.getType();
          final LEntity ref = ((LEntity) _type_1);
          final Function1<LEntityReference, Boolean> _function = (LEntityReference it) -> {
            LEntityReference _opposite_1 = it.getOpposite();
            return Boolean.valueOf(Objects.equal(_opposite_1, prop));
          };
          _xblockexpression = IterableExtensions.<LEntityReference>findFirst(ref.getReferences(), _function);
        }
        _xifexpression_1 = _xblockexpression;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  protected LFeature _getResolvedOpposite(final LBeanReference prop) {
    LBeanReference _xifexpression = null;
    LFeature _opposite = prop.getOpposite();
    boolean _tripleNotEquals = (_opposite != null);
    if (_tripleNotEquals) {
      return prop.getOpposite();
    } else {
      LBeanReference _xifexpression_1 = null;
      LType _type = prop.getType();
      if ((_type instanceof LBean)) {
        LBeanReference _xblockexpression = null;
        {
          LType _type_1 = prop.getType();
          final LBean ref = ((LBean) _type_1);
          final Function1<LBeanReference, Boolean> _function = (LBeanReference it) -> {
            LFeature _opposite_1 = it.getOpposite();
            return Boolean.valueOf(Objects.equal(_opposite_1, prop));
          };
          _xblockexpression = IterableExtensions.<LBeanReference>findFirst(ref.getReferences(), _function);
        }
        _xifexpression_1 = _xblockexpression;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  /**
   * Returns true, if toCheck can be cast to superType
   */
  protected boolean _isCastable(final LEntity toCheck, final LEntity superType) {
    final String toCheckFqn = this._iQualifiedNameProvider.getFullyQualifiedName(toCheck).toString();
    final String superTypeFqn = this._iQualifiedNameProvider.getFullyQualifiedName(superType).toString();
    boolean _equals = toCheckFqn.equals(superTypeFqn);
    if (_equals) {
      return true;
    } else {
      final LClass toCheckSuperType = toCheck.getSuperType();
      if ((toCheckSuperType != null)) {
        return this.isCastable(toCheckSuperType, superType);
      } else {
        return false;
      }
    }
  }
  
  /**
   * Returns true, if toCheck can be cast to superType
   */
  protected boolean _isCastable(final LBean toCheck, final LBean superType) {
    final String toCheckFqn = this._iQualifiedNameProvider.getFullyQualifiedName(toCheck).toString();
    final String superTypeFqn = this._iQualifiedNameProvider.getFullyQualifiedName(superType).toString();
    boolean _equals = toCheckFqn.equals(superTypeFqn);
    if (_equals) {
      return true;
    } else {
      final LClass toCheckSuperType = toCheck.getSuperType();
      if ((toCheckSuperType != null)) {
        return this.isCastable(toCheckSuperType, superType);
      } else {
        return false;
      }
    }
  }
  
  /**
   * The binary <code>+</code> operator that concatenates two strings.
   * 
   * @param a
   *            a string.
   * @param b
   *            another string.
   * @return <code>a + b</code>
   */
  public static String operator_plus(final String a, final String b) {
    return ModelExtensions.operator_plus(a, b);
  }
  
  protected String _typeName(final LEntityAttribute prop) {
    return prop.getType().getName();
  }
  
  protected String _typeName(final LEntityReference prop) {
    return prop.getType().getName();
  }
  
  protected String _typeName(final LBeanAttribute prop) {
    return prop.getType().getName();
  }
  
  protected String _typeName(final LBeanReference prop) {
    return prop.getType().getName();
  }
  
  protected LType _type(final LEntityAttribute prop) {
    return prop.getType();
  }
  
  protected LType _type(final LEntityReference prop) {
    return prop.getType();
  }
  
  protected LType _type(final LBeanAttribute prop) {
    return prop.getType();
  }
  
  protected LType _type(final LBeanReference prop) {
    return prop.getType();
  }
  
  protected LFeature _opposite(final LEntityAttribute prop) {
    return null;
  }
  
  protected LFeature _opposite(final LEntityReference prop) {
    return prop.getOpposite();
  }
  
  protected LFeature _opposite(final LBeanAttribute prop) {
    return null;
  }
  
  protected LFeature _opposite(final LBeanReference prop) {
    return prop.getOpposite();
  }
  
  public List<LEntityInheritanceStrategy> collectAllInheritanceStrategies(final LEntity entity) {
    return this.collectAllInheritanceStrategies(entity, CollectionLiterals.<LEntityInheritanceStrategy>newArrayList());
  }
  
  protected List<LEntityInheritanceStrategy> collectAllInheritanceStrategies(final LEntity entity, final List<LEntityInheritanceStrategy> result) {
    if ((entity == null)) {
      return result;
    }
    LEntityInheritanceStrategy _inheritanceStrategy = this.toInheritanceStrategy(entity);
    result.add(_inheritanceStrategy);
    return this.collectAllInheritanceStrategies(entity.getSuperType(), result);
  }
  
  public LEntityInheritanceStrategy toInheritanceStrategy(final LEntity entity) {
    LEntityInheritanceStrategy strategy = entity.getInheritanceStrategy();
    if ((strategy != null)) {
      strategy = EcoreUtil.<LEntityInheritanceStrategy>copy(strategy);
    }
    if ((strategy == null)) {
      strategy = this.findStrategyFromSuper(entity);
      if (strategy!=null) {
        this.resetInheritancePropsFromSuper(strategy, entity);
      }
    }
    if ((strategy == null)) {
      strategy = OSBPEntityFactory.eINSTANCE.createLTablePerSubclassStrategy();
    }
    this.fillInheritanceDefaults(strategy, entity);
    return strategy;
  }
  
  protected void _fillInheritanceDefaults(final LTablePerClassStrategy strategy, final LEntity entity) {
    String _discriminatorColumn = strategy.getDiscriminatorColumn();
    boolean _tripleEquals = (_discriminatorColumn == null);
    if (_tripleEquals) {
      strategy.setDiscriminatorColumn("DISC");
    }
    LDiscriminatorType _discriminatorType = strategy.getDiscriminatorType();
    boolean _equals = Objects.equal(_discriminatorType, LDiscriminatorType.INHERIT);
    if (_equals) {
      strategy.setDiscriminatorType(LDiscriminatorType.STRING);
    }
    String _discriminatorValue = strategy.getDiscriminatorValue();
    boolean _tripleEquals_1 = (_discriminatorValue == null);
    if (_tripleEquals_1) {
      strategy.setDiscriminatorValue(this._namingExtensions.toTableName(entity));
    }
  }
  
  protected void _fillInheritanceDefaults(final LTablePerSubclassStrategy strategy, final LEntity entity) {
    String _discriminatorColumn = strategy.getDiscriminatorColumn();
    boolean _tripleEquals = (_discriminatorColumn == null);
    if (_tripleEquals) {
      strategy.setDiscriminatorColumn("DISC");
    }
    LDiscriminatorType _discriminatorType = strategy.getDiscriminatorType();
    boolean _equals = Objects.equal(_discriminatorType, LDiscriminatorType.INHERIT);
    if (_equals) {
      strategy.setDiscriminatorType(LDiscriminatorType.STRING);
    }
    String _discriminatorValue = strategy.getDiscriminatorValue();
    boolean _tripleEquals_1 = (_discriminatorValue == null);
    if (_tripleEquals_1) {
      strategy.setDiscriminatorValue(this._namingExtensions.toTableName(entity));
    }
  }
  
  protected void _resetInheritancePropsFromSuper(final LTablePerClassStrategy strategy, final LEntity entity) {
    strategy.setDiscriminatorValue(null);
  }
  
  protected void _resetInheritancePropsFromSuper(final LTablePerSubclassStrategy strategy, final LEntity entity) {
    strategy.setDiscriminatorValue(null);
  }
  
  protected String _toDiscriminatorValue(final LTablePerClassStrategy strategy) {
    return strategy.getDiscriminatorValue();
  }
  
  protected String _toDiscriminatorValue(final LTablePerSubclassStrategy strategy) {
    return strategy.getDiscriminatorValue();
  }
  
  public LEntityInheritanceStrategy findStrategyFromSuper(final LEntity entity) {
    LEntityInheritanceStrategy _inheritanceStrategy = entity.getInheritanceStrategy();
    boolean _tripleNotEquals = (_inheritanceStrategy != null);
    if (_tripleNotEquals) {
      return EcoreUtil.<LEntityInheritanceStrategy>copy(entity.getInheritanceStrategy());
    }
    if (((!Objects.equal(entity, entity.getSuperType())) && (entity.getSuperType() != null))) {
      return this.findStrategyFromSuper(entity.getSuperType());
    }
    return null;
  }
  
  public boolean isStrategyFromSuperPresent(final LEntity entity) {
    LEntityInheritanceStrategy _findStrategyFromSuper = this.findStrategyFromSuper(entity);
    return (_findStrategyFromSuper != null);
  }
  
  public boolean isStrategyPerSubclass(final LEntity entity) {
    LEntityInheritanceStrategy _inheritanceStrategy = this.toInheritanceStrategy(entity);
    return (_inheritanceStrategy instanceof LTablePerSubclassStrategy);
  }
  
  public boolean checkIsMappedSuperclass(final LEntity entity) {
    boolean _xifexpression = false;
    if ((entity == null)) {
      _xifexpression = false;
    } else {
      _xifexpression = entity.isMappedSuperclass();
    }
    return _xifexpression;
  }
  
  public JvmTypeReference toTypeReference(final LAnnotationTarget prop) {
    if (prop instanceof LDataType) {
      return _toTypeReference((LDataType)prop);
    } else if (prop instanceof LBeanReference) {
      return _toTypeReference((LBeanReference)prop);
    } else if (prop instanceof LEntityAttribute) {
      return _toTypeReference((LEntityAttribute)prop);
    } else if (prop instanceof LEntityReference) {
      return _toTypeReference((LEntityReference)prop);
    } else if (prop instanceof LOperation) {
      return _toTypeReference((LOperation)prop);
    } else if (prop instanceof LAttribute) {
      return _toTypeReference((LAttribute)prop);
    } else if (prop instanceof LType) {
      return _toTypeReference((LType)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean isUUID(final LFeature prop) {
    if (prop instanceof LEntityAttribute) {
      return _isUUID((LEntityAttribute)prop);
    } else if (prop != null) {
      return _isUUID(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean isID(final LEntityAttribute prop) {
    return _isID(prop);
  }
  
  public LFeature getResolvedOpposite(final LReference prop) {
    if (prop instanceof LBeanReference) {
      return _getResolvedOpposite((LBeanReference)prop);
    } else if (prop instanceof LEntityReference) {
      return _getResolvedOpposite((LEntityReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean isCastable(final EObject toCheck, final EObject superType) {
    if (toCheck instanceof LBean
         && superType instanceof LBean) {
      return _isCastable((LBean)toCheck, (LBean)superType);
    } else if (toCheck instanceof LEntity
         && superType instanceof LEntity) {
      return _isCastable((LEntity)toCheck, (LEntity)superType);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(toCheck, superType).toString());
    }
  }
  
  public String typeName(final LFeature prop) {
    if (prop instanceof LBeanAttribute) {
      return _typeName((LBeanAttribute)prop);
    } else if (prop instanceof LBeanReference) {
      return _typeName((LBeanReference)prop);
    } else if (prop instanceof LEntityAttribute) {
      return _typeName((LEntityAttribute)prop);
    } else if (prop instanceof LEntityReference) {
      return _typeName((LEntityReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public LType type(final LFeature prop) {
    if (prop instanceof LBeanAttribute) {
      return _type((LBeanAttribute)prop);
    } else if (prop instanceof LBeanReference) {
      return _type((LBeanReference)prop);
    } else if (prop instanceof LEntityAttribute) {
      return _type((LEntityAttribute)prop);
    } else if (prop instanceof LEntityReference) {
      return _type((LEntityReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public LFeature opposite(final LFeature prop) {
    if (prop instanceof LBeanAttribute) {
      return _opposite((LBeanAttribute)prop);
    } else if (prop instanceof LBeanReference) {
      return _opposite((LBeanReference)prop);
    } else if (prop instanceof LEntityAttribute) {
      return _opposite((LEntityAttribute)prop);
    } else if (prop instanceof LEntityReference) {
      return _opposite((LEntityReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public void fillInheritanceDefaults(final LEntityInheritanceStrategy strategy, final LEntity entity) {
    if (strategy instanceof LTablePerClassStrategy) {
      _fillInheritanceDefaults((LTablePerClassStrategy)strategy, entity);
      return;
    } else if (strategy instanceof LTablePerSubclassStrategy) {
      _fillInheritanceDefaults((LTablePerSubclassStrategy)strategy, entity);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(strategy, entity).toString());
    }
  }
  
  public void resetInheritancePropsFromSuper(final LEntityInheritanceStrategy strategy, final LEntity entity) {
    if (strategy instanceof LTablePerClassStrategy) {
      _resetInheritancePropsFromSuper((LTablePerClassStrategy)strategy, entity);
      return;
    } else if (strategy instanceof LTablePerSubclassStrategy) {
      _resetInheritancePropsFromSuper((LTablePerSubclassStrategy)strategy, entity);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(strategy, entity).toString());
    }
  }
  
  public String toDiscriminatorValue(final LEntityInheritanceStrategy strategy) {
    if (strategy instanceof LTablePerClassStrategy) {
      return _toDiscriminatorValue((LTablePerClassStrategy)strategy);
    } else if (strategy instanceof LTablePerSubclassStrategy) {
      return _toDiscriminatorValue((LTablePerSubclassStrategy)strategy);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(strategy).toString());
    }
  }
}
