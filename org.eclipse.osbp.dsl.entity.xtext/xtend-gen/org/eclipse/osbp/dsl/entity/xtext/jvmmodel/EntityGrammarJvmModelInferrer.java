/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.jvmmodel;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.common.datatypes.IBean;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.dsl.common.xtext.extensions.AnnotationExtension;
import org.eclipse.osbp.dsl.entity.xtext.extensions.EntityTypesBuilder;
import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.entity.xtext.jvmmodel.AnnotationCompiler;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LState;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanFeature;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.dsl.semantic.entity.LOperation;
import org.eclipse.osbp.xtext.oxtype.logger.TimeLogger;
import org.eclipse.osbp.xtext.oxtype.resource.ExtendedModelInferrer;
import org.eclipse.xtext.common.types.JvmConstructor;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmEnumerationLiteral;
import org.eclipse.xtext.common.types.JvmEnumerationType;
import org.eclipse.xtext.common.types.JvmField;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmMember;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the main model inferrer that is automatically registered in AbstractEntityRuntimeModule.
 * It dispatches to specific model inferrers depending on the metamodel element.
 */
@SuppressWarnings("all")
public class EntityGrammarJvmModelInferrer extends ExtendedModelInferrer {
  protected final Logger log = LoggerFactory.getLogger(this.getClass());
  
  @Inject
  private AnnotationCompiler annotationCompiler;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private EntityTypesBuilder _entityTypesBuilder;
  
  @Inject
  @Extension
  private ModelExtensions _modelExtensions;
  
  @Inject
  private TypeReferences references;
  
  @Inject
  private AnnotationExtension annExt;
  
  protected void _infer(final LStateClass stateClass, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    final JvmEnumerationType type = this._entityTypesBuilder.toEnumerationType(stateClass, this._iQualifiedNameProvider.getFullyQualifiedName(stateClass).toString(), null);
    this.inferFullState(type, stateClass, acceptor, isPrelinkingPhase, "");
  }
  
  protected void _inferTypesOnly(final LStateClass stateClass, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    final JvmEnumerationType type = this._entityTypesBuilder.toEnumerationType(stateClass, this._iQualifiedNameProvider.getFullyQualifiedName(stateClass).toString(), null);
    acceptor.<JvmEnumerationType>accept(type);
    this.inferTypesOnlyByDelegates(stateClass, acceptor, isPrelinkingPhase);
  }
  
  protected void _inferFullState(final JvmDeclaredType type, final LStateClass stateClass, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase, final String selector) {
    final Procedure1<JvmDeclaredType> _function = (JvmDeclaredType it) -> {
      final TimeLogger doInferLog = TimeLogger.start(it.getClass());
      this.annotationCompiler.processAnnotation(stateClass, it);
      EObject _eContainer = stateClass.eContainer();
      this._entityTypesBuilder.setFileHeader(it, this._entityTypesBuilder.getDocumentation(((LTypedPackage) _eContainer)));
      this._entityTypesBuilder.setDocumentation(it, this._entityTypesBuilder.getDocumentation(stateClass));
      EList<LState> _states = stateClass.getStates();
      for (final LState f : _states) {
        {
          this._entityTypesBuilder.setDocumentation(it, this._entityTypesBuilder.getDocumentation(f));
          EList<JvmMember> _members = it.getMembers();
          JvmEnumerationLiteral _enumerationLiteral = this._entityTypesBuilder.toEnumerationLiteral(f, f.getName());
          this._entityTypesBuilder.<JvmEnumerationLiteral>operator_add(_members, _enumerationLiteral);
        }
      }
      String _name = stateClass.getName();
      String _plus = ("Inferring stateClass " + _name);
      doInferLog.stop(this.log, _plus);
    };
    acceptor.<JvmDeclaredType>accept(type).initializeLater(_function);
  }
  
  protected void _infer(final LEnum enumX, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    final JvmEnumerationType type = this._entityTypesBuilder.toEnumerationType(enumX, this._iQualifiedNameProvider.getFullyQualifiedName(enumX).toString(), null);
    this.inferFullState(type, enumX, acceptor, isPrelinkingPhase, "");
  }
  
  protected void _inferTypesOnly(final LEnum enumX, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    final JvmEnumerationType type = this._entityTypesBuilder.toEnumerationType(enumX, this._iQualifiedNameProvider.getFullyQualifiedName(enumX).toString(), null);
    acceptor.<JvmEnumerationType>accept(type);
    this.inferTypesOnlyByDelegates(enumX, acceptor, isPrelinkingPhase);
  }
  
  protected void _inferFullState(final JvmDeclaredType type, final LEnum enumX, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase, final String selector) {
    final Procedure1<JvmDeclaredType> _function = (JvmDeclaredType it) -> {
      final TimeLogger doInferLog = TimeLogger.start(it.getClass());
      this.annotationCompiler.processAnnotation(enumX, it);
      EObject _eContainer = enumX.eContainer();
      this._entityTypesBuilder.setFileHeader(it, this._entityTypesBuilder.getDocumentation(((LTypedPackage) _eContainer)));
      this._entityTypesBuilder.setDocumentation(it, this._entityTypesBuilder.getDocumentation(enumX));
      EList<LEnumLiteral> _literals = enumX.getLiterals();
      for (final LEnumLiteral f : _literals) {
        {
          this._entityTypesBuilder.setDocumentation(it, this._entityTypesBuilder.getDocumentation(f));
          EList<JvmMember> _members = it.getMembers();
          JvmEnumerationLiteral _enumerationLiteral = this._entityTypesBuilder.toEnumerationLiteral(f, f.getName());
          this._entityTypesBuilder.<JvmEnumerationLiteral>operator_add(_members, _enumerationLiteral);
        }
      }
      String _name = enumX.getName();
      String _plus = ("Inferring enum " + _name);
      doInferLog.stop(this.log, _plus);
    };
    acceptor.<JvmDeclaredType>accept(type).initializeLater(_function);
  }
  
  protected void _infer(final LBean bean, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    final JvmGenericType type = this._entityTypesBuilder.toJvmType(bean);
    this.inferFullState(type, bean, acceptor, isPrelinkingPhase, "bean");
  }
  
  protected void _inferTypesOnly(final LBean bean, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    final JvmGenericType type = this._entityTypesBuilder.toJvmType(bean);
    acceptor.<JvmGenericType>accept(type);
    this.inferTypesOnlyByDelegates(bean, acceptor, isPrelinkingPhase);
  }
  
  protected void _inferFullState(final JvmDeclaredType type, final LBean bean, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase, final String selector) {
    if (((selector == null) || (!selector.equals("bean")))) {
      return;
    }
    final Procedure1<JvmDeclaredType> _function = (JvmDeclaredType it) -> {
      final TimeLogger doInferLog = TimeLogger.start(it.getClass());
      this.annotationCompiler.processAnnotation(bean, it);
      EObject _eContainer = bean.eContainer();
      this._entityTypesBuilder.setFileHeader(it, this._entityTypesBuilder.getDocumentation(((LTypedPackage) _eContainer)));
      this._entityTypesBuilder.setDocumentation(it, this._entityTypesBuilder.getDocumentation(bean));
      LBean _superType = bean.getSuperType();
      boolean _tripleEquals = (_superType == null);
      if (_tripleEquals) {
        EList<JvmTypeReference> _superTypes = it.getSuperTypes();
        JvmTypeReference _typeForName = this.references.getTypeForName(Serializable.class, bean, null);
        this._entityTypesBuilder.<JvmTypeReference>operator_add(_superTypes, _typeForName);
      }
      if (((bean.getSuperType() != null) && (!this._iQualifiedNameProvider.getFullyQualifiedName(bean.getSuperType()).toString().isEmpty()))) {
        EList<JvmTypeReference> _superTypes_1 = it.getSuperTypes();
        JvmTypeReference _typeReference = this._modelExtensions.toTypeReference(bean.getSuperType());
        this._entityTypesBuilder.<JvmTypeReference>operator_add(_superTypes_1, _typeReference);
      }
      EList<JvmTypeReference> _superTypes_2 = it.getSuperTypes();
      JvmTypeReference _typeForName_1 = this.references.getTypeForName(IBean.class, bean, null);
      this._entityTypesBuilder.<JvmTypeReference>operator_add(_superTypes_2, _typeForName_1);
      EList<JvmMember> _members = it.getMembers();
      final Procedure1<JvmConstructor> _function_1 = (JvmConstructor it_1) -> {
      };
      JvmConstructor _constructor = this._entityTypesBuilder.toConstructor(bean, _function_1);
      this._entityTypesBuilder.<JvmConstructor>operator_add(_members, _constructor);
      LBean _superType_1 = bean.getSuperType();
      boolean _tripleEquals_1 = (_superType_1 == null);
      if (_tripleEquals_1) {
        EList<JvmMember> _members_1 = it.getMembers();
        JvmField _diposeField = this._entityTypesBuilder.toDiposeField(bean);
        this._entityTypesBuilder.<JvmField>operator_add(_members_1, _diposeField);
        EList<JvmMember> _members_2 = it.getMembers();
        JvmField _dirtyField = this._entityTypesBuilder.toDirtyField(bean);
        this._entityTypesBuilder.<JvmField>operator_add(_members_2, _dirtyField);
      }
      EList<LBeanFeature> _features = bean.getFeatures();
      for (final LBeanFeature f : _features) {
        boolean _matched = false;
        if (f instanceof LAttribute) {
          _matched=true;
          if ((((!((LAttribute)f).isDerived()) && (this._iQualifiedNameProvider.getFullyQualifiedName(f) != null)) && (!this._iQualifiedNameProvider.getFullyQualifiedName(f).toString().isEmpty()))) {
            EList<JvmMember> _members_3 = it.getMembers();
            JvmField _field = this._entityTypesBuilder.toField(f);
            this._entityTypesBuilder.<JvmField>operator_add(_members_3, _field);
          }
        }
        if (!_matched) {
          if (f instanceof LReference) {
            _matched=true;
            if (((this._iQualifiedNameProvider.getFullyQualifiedName(f) != null) && (!this._iQualifiedNameProvider.getFullyQualifiedName(f).toString().isEmpty()))) {
              EList<JvmMember> _members_3 = it.getMembers();
              JvmField _field = this._entityTypesBuilder.toField(f);
              this._entityTypesBuilder.<JvmField>operator_add(_members_3, _field);
            }
          }
        }
      }
      LBean _superType_2 = bean.getSuperType();
      boolean _tripleEquals_2 = (_superType_2 == null);
      if (_tripleEquals_2) {
        EList<JvmMember> _members_3 = it.getMembers();
        JvmOperation _isDisposed = this._entityTypesBuilder.toIsDisposed(bean);
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_3, _isDisposed);
        EList<JvmMember> _members_4 = it.getMembers();
        JvmOperation _isDirty = this._entityTypesBuilder.toIsDirty(bean);
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_4, _isDirty);
        EList<JvmMember> _members_5 = it.getMembers();
        JvmOperation _setDirty = this._entityTypesBuilder.toSetDirty(bean);
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_5, _setDirty);
      }
      EList<JvmMember> _members_6 = it.getMembers();
      JvmOperation _checkDisposed = this._entityTypesBuilder.toCheckDisposed(bean);
      this._entityTypesBuilder.<JvmOperation>operator_add(_members_6, _checkDisposed);
      EList<JvmMember> _members_7 = it.getMembers();
      JvmOperation _dispose = this._entityTypesBuilder.toDispose(bean);
      this._entityTypesBuilder.<JvmOperation>operator_add(_members_7, _dispose);
      EList<LBeanFeature> _features_1 = bean.getFeatures();
      for (final LBeanFeature f_1 : _features_1) {
        boolean _matched_1 = false;
        if (f_1 instanceof LAttribute) {
          _matched_1=true;
          EList<JvmMember> _members_8 = it.getMembers();
          JvmOperation _getter = this._entityTypesBuilder.toGetter(f_1);
          this._entityTypesBuilder.<JvmOperation>operator_add(_members_8, _getter);
          boolean _isDerived = ((LAttribute)f_1).isDerived();
          boolean _not = (!_isDerived);
          if (_not) {
            boolean _isToMany = this._modelExtensions.isToMany(f_1);
            if (_isToMany) {
              EList<JvmMember> _members_9 = it.getMembers();
              JvmOperation _collectionSetter = this._entityTypesBuilder.toCollectionSetter(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_9, _collectionSetter);
              EList<JvmMember> _members_10 = it.getMembers();
              JvmOperation _internalCollectionGetter = this._entityTypesBuilder.toInternalCollectionGetter(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_10, _internalCollectionGetter);
              EList<JvmMember> _members_11 = it.getMembers();
              JvmOperation _adder = this._entityTypesBuilder.toAdder(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_11, _adder);
              EList<JvmMember> _members_12 = it.getMembers();
              JvmOperation _remover = this._entityTypesBuilder.toRemover(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_12, _remover);
            } else {
              EList<JvmMember> _members_13 = it.getMembers();
              JvmOperation _setter = this._entityTypesBuilder.toSetter(f_1);
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_13, _setter);
            }
          }
        }
        if (!_matched_1) {
          if (f_1 instanceof LReference) {
            _matched_1=true;
            EList<JvmMember> _members_8 = it.getMembers();
            JvmOperation _getter = this._entityTypesBuilder.toGetter(f_1);
            this._entityTypesBuilder.<JvmOperation>operator_add(_members_8, _getter);
            boolean _isToMany = this._modelExtensions.isToMany(f_1);
            if (_isToMany) {
              EList<JvmMember> _members_9 = it.getMembers();
              JvmOperation _collectionSetter = this._entityTypesBuilder.toCollectionSetter(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_9, _collectionSetter);
              EList<JvmMember> _members_10 = it.getMembers();
              JvmOperation _internalCollectionGetter = this._entityTypesBuilder.toInternalCollectionGetter(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_10, _internalCollectionGetter);
              EList<JvmMember> _members_11 = it.getMembers();
              JvmOperation _adder = this._entityTypesBuilder.toAdder(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_11, _adder);
              EList<JvmMember> _members_12 = it.getMembers();
              JvmOperation _remover = this._entityTypesBuilder.toRemover(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_12, _remover);
              EList<JvmMember> _members_13 = it.getMembers();
              JvmOperation _internalAdder = this._entityTypesBuilder.toInternalAdder(f_1);
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_13, _internalAdder);
              EList<JvmMember> _members_14 = it.getMembers();
              JvmOperation _internalRemover = this._entityTypesBuilder.toInternalRemover(f_1);
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_14, _internalRemover);
            } else {
              EList<JvmMember> _members_15 = it.getMembers();
              JvmOperation _setter = this._entityTypesBuilder.toSetter(f_1);
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_15, _setter);
              if ((((f_1 instanceof LBeanReference) && ((LBeanReference) f_1).isCascading()) && (((LBeanReference) f_1).getOpposite() != null))) {
                EList<JvmMember> _members_16 = it.getMembers();
                JvmOperation _internalSetter = this._entityTypesBuilder.toInternalSetter(f_1);
                this._entityTypesBuilder.<JvmOperation>operator_add(_members_16, _internalSetter);
              }
            }
          }
        }
      }
      List<LOperation> _operations = bean.getOperations();
      for (final LOperation op : _operations) {
        EList<JvmMember> _members_8 = it.getMembers();
        final Procedure1<JvmOperation> _function_2 = (JvmOperation it_1) -> {
          this._entityTypesBuilder.setDocumentation(it_1, this._entityTypesBuilder.getDocumentation(op));
          EList<JvmFormalParameter> _params = op.getParams();
          for (final JvmFormalParameter p : _params) {
            EList<JvmFormalParameter> _parameters = it_1.getParameters();
            JvmFormalParameter _parameter = this._entityTypesBuilder.toParameter(p, p.getName(), p.getParameterType());
            this._entityTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
          }
          this._entityTypesBuilder.setBody(it_1, op.getBody());
        };
        JvmOperation _method = this._entityTypesBuilder.toMethod(op, op.getName(), op.getType(), _function_2);
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_8, _method);
      }
      String _name = bean.getName();
      String _plus = ("Inferring bean " + _name);
      doInferLog.stop(this.log, _plus);
    };
    acceptor.<JvmDeclaredType>accept(type).initializeLater(_function);
  }
  
  protected void _infer(final LEntity entity, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    final JvmGenericType type = this._entityTypesBuilder.toJvmType(entity);
    this.inferFullState(type, entity, acceptor, isPrelinkingPhase, "entity");
  }
  
  protected void _inferTypesOnly(final LEntity entity, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    final JvmGenericType type = this._entityTypesBuilder.toJvmType(entity);
    acceptor.<JvmGenericType>accept(type);
    this.inferTypesOnlyByDelegates(entity, acceptor, isPrelinkingPhase);
  }
  
  protected void _inferFullState(final JvmType type, final EObject element, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase, final String selector) {
  }
  
  protected void _inferFullState(final JvmDeclaredType type, final LEntity entity, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase, final String selector) {
    if (((selector == null) || (!selector.equals("entity")))) {
      return;
    }
    final Procedure1<JvmDeclaredType> _function = (JvmDeclaredType it) -> {
      final TimeLogger doInferLog = TimeLogger.start(it.getClass());
      this.annotationCompiler.processAnnotation(entity, it);
      LAttribute idAttribute = null;
      JvmField idField = null;
      JvmField versionField = null;
      EObject _eContainer = entity.eContainer();
      this._entityTypesBuilder.setFileHeader(it, this._entityTypesBuilder.getDocumentation(((LTypedPackage) _eContainer)));
      this._entityTypesBuilder.setDocumentation(it, this._entityTypesBuilder.getDocumentation(entity));
      if (((entity.getSuperType() != null) && (!this._iQualifiedNameProvider.getFullyQualifiedName(entity.getSuperType()).toString().isEmpty()))) {
        EList<JvmTypeReference> _superTypes = it.getSuperTypes();
        JvmTypeReference _typeReference = this._modelExtensions.toTypeReference(entity.getSuperType());
        this._entityTypesBuilder.<JvmTypeReference>operator_add(_superTypes, _typeReference);
      }
      EList<JvmTypeReference> _superTypes_1 = it.getSuperTypes();
      JvmTypeReference _typeForName = this.references.getTypeForName(IEntity.class, entity, null);
      this._entityTypesBuilder.<JvmTypeReference>operator_add(_superTypes_1, _typeForName);
      EList<JvmMember> _members = it.getMembers();
      final Procedure1<JvmConstructor> _function_1 = (JvmConstructor it_1) -> {
      };
      JvmConstructor _constructor = this._entityTypesBuilder.toConstructor(entity, _function_1);
      this._entityTypesBuilder.<JvmConstructor>operator_add(_members, _constructor);
      LEntity _superType = entity.getSuperType();
      boolean _tripleEquals = (_superType == null);
      if (_tripleEquals) {
        EList<JvmMember> _members_1 = it.getMembers();
        JvmField _diposeField = this._entityTypesBuilder.toDiposeField(entity);
        this._entityTypesBuilder.<JvmField>operator_add(_members_1, _diposeField);
      }
      final Function1<LEntityFeature, Boolean> _function_2 = (LEntityFeature it_1) -> {
        return Boolean.valueOf((!(it_1 instanceof LOperation)));
      };
      Iterable<LEntityFeature> _filter = IterableExtensions.<LEntityFeature>filter(entity.getFeatures(), _function_2);
      for (final LEntityFeature f : _filter) {
        boolean _matched = false;
        if (f instanceof LAttribute) {
          _matched=true;
          if ((((!((LAttribute)f).isDerived()) && (this._iQualifiedNameProvider.getFullyQualifiedName(f) != null)) && (!this._iQualifiedNameProvider.getFullyQualifiedName(f).toString().isEmpty()))) {
            if ((((LAttribute)f).isId() || ((LAttribute)f).isUuid())) {
              idAttribute = ((LAttribute)f);
              idField = this._entityTypesBuilder.toField(f);
              EList<JvmMember> _members_2 = it.getMembers();
              this._entityTypesBuilder.<JvmField>operator_add(_members_2, idField);
            } else {
              boolean _isVersion = ((LAttribute)f).isVersion();
              if (_isVersion) {
                versionField = this._entityTypesBuilder.toField(f);
                EList<JvmMember> _members_3 = it.getMembers();
                this._entityTypesBuilder.<JvmField>operator_add(_members_3, versionField);
              } else {
                EList<JvmMember> _members_4 = it.getMembers();
                JvmField _field = this._entityTypesBuilder.toField(f);
                this._entityTypesBuilder.<JvmField>operator_add(_members_4, _field);
              }
            }
          }
        }
        if (!_matched) {
          if (f instanceof LReference) {
            _matched=true;
            if (((this._iQualifiedNameProvider.getFullyQualifiedName(f) != null) && (!this._iQualifiedNameProvider.getFullyQualifiedName(f).toString().isEmpty()))) {
              EList<JvmMember> _members_2 = it.getMembers();
              JvmField _field = this._entityTypesBuilder.toField(f);
              this._entityTypesBuilder.<JvmField>operator_add(_members_2, _field);
            }
          }
        }
      }
      LStateClass _stateClass = entity.getStateClass();
      boolean _tripleNotEquals = (_stateClass != null);
      if (_tripleNotEquals) {
        EList<JvmMember> _members_2 = it.getMembers();
        JvmField _field = this._entityTypesBuilder.toField(entity.getStateClass());
        this._entityTypesBuilder.<JvmField>operator_add(_members_2, _field);
        EList<JvmMember> _members_3 = it.getMembers();
        JvmOperation _getter = this._entityTypesBuilder.toGetter(entity.getStateClass());
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_3, _getter);
        EList<JvmMember> _members_4 = it.getMembers();
        JvmOperation _setter = this._entityTypesBuilder.toSetter(entity.getStateClass());
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_4, _setter);
      }
      LEntity _superType_1 = entity.getSuperType();
      boolean _tripleEquals_1 = (_superType_1 == null);
      if (_tripleEquals_1) {
        EList<JvmMember> _members_5 = it.getMembers();
        JvmOperation _isDisposed = this._entityTypesBuilder.toIsDisposed(entity);
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_5, _isDisposed);
      }
      EList<JvmMember> _members_6 = it.getMembers();
      JvmOperation _checkDisposed = this._entityTypesBuilder.toCheckDisposed(entity);
      this._entityTypesBuilder.<JvmOperation>operator_add(_members_6, _checkDisposed);
      EList<JvmMember> _members_7 = it.getMembers();
      JvmOperation _dispose = this._entityTypesBuilder.toDispose(entity);
      this._entityTypesBuilder.<JvmOperation>operator_add(_members_7, _dispose);
      EList<LEntityFeature> _features = entity.getFeatures();
      for (final LEntityFeature f_1 : _features) {
        boolean _matched_1 = false;
        if (f_1 instanceof LAttribute) {
          _matched_1=true;
          EList<JvmMember> _members_8 = it.getMembers();
          JvmOperation _getter_1 = this._entityTypesBuilder.toGetter(f_1);
          this._entityTypesBuilder.<JvmOperation>operator_add(_members_8, _getter_1);
          boolean _isDerived = ((LAttribute)f_1).isDerived();
          boolean _not = (!_isDerived);
          if (_not) {
            boolean _isToMany = this._modelExtensions.isToMany(f_1);
            if (_isToMany) {
              EList<JvmMember> _members_9 = it.getMembers();
              JvmOperation _collectionSetter = this._entityTypesBuilder.toCollectionSetter(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_9, _collectionSetter);
              EList<JvmMember> _members_10 = it.getMembers();
              JvmOperation _internalCollectionGetter = this._entityTypesBuilder.toInternalCollectionGetter(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_10, _internalCollectionGetter);
              EList<JvmMember> _members_11 = it.getMembers();
              JvmOperation _adder = this._entityTypesBuilder.toAdder(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_11, _adder);
              EList<JvmMember> _members_12 = it.getMembers();
              JvmOperation _remover = this._entityTypesBuilder.toRemover(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_12, _remover);
            } else {
              EList<JvmMember> _members_13 = it.getMembers();
              JvmOperation _setter_1 = this._entityTypesBuilder.toSetter(f_1);
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_13, _setter_1);
            }
          }
        }
        if (!_matched_1) {
          if (f_1 instanceof LReference) {
            _matched_1=true;
            EList<JvmMember> _members_8 = it.getMembers();
            JvmOperation _getter_1 = this._entityTypesBuilder.toGetter(f_1);
            this._entityTypesBuilder.<JvmOperation>operator_add(_members_8, _getter_1);
            boolean _isToMany = this._modelExtensions.isToMany(f_1);
            if (_isToMany) {
              EList<JvmMember> _members_9 = it.getMembers();
              JvmOperation _collectionSetter = this._entityTypesBuilder.toCollectionSetter(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_9, _collectionSetter);
              EList<JvmMember> _members_10 = it.getMembers();
              JvmOperation _internalCollectionGetter = this._entityTypesBuilder.toInternalCollectionGetter(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_10, _internalCollectionGetter);
              EList<JvmMember> _members_11 = it.getMembers();
              JvmOperation _adder = this._entityTypesBuilder.toAdder(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_11, _adder);
              EList<JvmMember> _members_12 = it.getMembers();
              JvmOperation _remover = this._entityTypesBuilder.toRemover(f_1, f_1.getName());
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_12, _remover);
              EList<JvmMember> _members_13 = it.getMembers();
              JvmOperation _internalAdder = this._entityTypesBuilder.toInternalAdder(f_1);
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_13, _internalAdder);
              EList<JvmMember> _members_14 = it.getMembers();
              JvmOperation _internalRemover = this._entityTypesBuilder.toInternalRemover(f_1);
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_14, _internalRemover);
            } else {
              EList<JvmMember> _members_15 = it.getMembers();
              JvmOperation _setter_1 = this._entityTypesBuilder.toSetter(f_1);
              this._entityTypesBuilder.<JvmOperation>operator_add(_members_15, _setter_1);
              if ((((f_1 instanceof LEntityReference) && ((LEntityReference) f_1).isCascading()) || (((LEntityReference) f_1).getOpposite() != null))) {
                EList<JvmMember> _members_16 = it.getMembers();
                JvmOperation _internalSetter = this._entityTypesBuilder.toInternalSetter(f_1);
                this._entityTypesBuilder.<JvmOperation>operator_add(_members_16, _internalSetter);
              }
            }
          }
        }
      }
      List<LOperation> _operations = entity.getOperations();
      for (final LOperation op : _operations) {
        EList<JvmMember> _members_8 = it.getMembers();
        final Procedure1<JvmOperation> _function_3 = (JvmOperation it_1) -> {
          this._entityTypesBuilder.setDocumentation(it_1, this._entityTypesBuilder.getDocumentation(op));
          EList<JvmFormalParameter> _params = op.getParams();
          for (final JvmFormalParameter p : _params) {
            EList<JvmFormalParameter> _parameters = it_1.getParameters();
            JvmFormalParameter _parameter = this._entityTypesBuilder.toParameter(p, p.getName(), p.getParameterType());
            this._entityTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
          }
          this._entityTypesBuilder.setBody(it_1, op.getBody());
        };
        JvmOperation _method = this._entityTypesBuilder.toMethod(op, op.getName(), op.getType(), _function_3);
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_8, _method);
      }
      if ((idAttribute != null)) {
        EList<JvmMember> _members_9 = it.getMembers();
        JvmOperation _equalVersionsMethod = this._entityTypesBuilder.toEqualVersionsMethod(idAttribute, it, false, idField, versionField);
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_9, _equalVersionsMethod);
        EList<JvmMember> _members_10 = it.getMembers();
        JvmOperation _equalsMethod = this._entityTypesBuilder.toEqualsMethod(idAttribute, it, false, idField);
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_10, _equalsMethod);
        EList<JvmMember> _members_11 = it.getMembers();
        JvmOperation _hashCodeMethod = this._entityTypesBuilder.toHashCodeMethod(idAttribute, false, idField);
        this._entityTypesBuilder.<JvmOperation>operator_add(_members_11, _hashCodeMethod);
      }
      EList<JvmMember> _members_12 = it.getMembers();
      JvmOperation _preRemove = this._entityTypesBuilder.toPreRemove(entity);
      this._entityTypesBuilder.<JvmOperation>operator_add(_members_12, _preRemove);
      String _name = entity.getName();
      String _plus = ("Inferring entity " + _name);
      doInferLog.stop(this.log, _plus);
    };
    acceptor.<JvmDeclaredType>accept(type).initializeLater(_function);
  }
  
  public void infer(final EObject enumX, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    if (enumX instanceof LEnum) {
      _infer((LEnum)enumX, acceptor, isPrelinkingPhase);
      return;
    } else if (enumX instanceof LStateClass) {
      _infer((LStateClass)enumX, acceptor, isPrelinkingPhase);
      return;
    } else if (enumX instanceof LBean) {
      _infer((LBean)enumX, acceptor, isPrelinkingPhase);
      return;
    } else if (enumX instanceof LEntity) {
      _infer((LEntity)enumX, acceptor, isPrelinkingPhase);
      return;
    } else if (enumX != null) {
      _infer(enumX, acceptor, isPrelinkingPhase);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(enumX, acceptor, isPrelinkingPhase).toString());
    }
  }
  
  public void inferTypesOnly(final EObject enumX, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase) {
    if (enumX instanceof LEnum) {
      _inferTypesOnly((LEnum)enumX, acceptor, isPrelinkingPhase);
      return;
    } else if (enumX instanceof LStateClass) {
      _inferTypesOnly((LStateClass)enumX, acceptor, isPrelinkingPhase);
      return;
    } else if (enumX instanceof LBean) {
      _inferTypesOnly((LBean)enumX, acceptor, isPrelinkingPhase);
      return;
    } else if (enumX instanceof LEntity) {
      _inferTypesOnly((LEntity)enumX, acceptor, isPrelinkingPhase);
      return;
    } else if (enumX != null) {
      _inferTypesOnly(enumX, acceptor, isPrelinkingPhase);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(enumX, acceptor, isPrelinkingPhase).toString());
    }
  }
  
  public void inferFullState(final JvmType type, final EObject enumX, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPrelinkingPhase, final String selector) {
    if (type instanceof JvmDeclaredType
         && enumX instanceof LEnum) {
      _inferFullState((JvmDeclaredType)type, (LEnum)enumX, acceptor, isPrelinkingPhase, selector);
      return;
    } else if (type instanceof JvmDeclaredType
         && enumX instanceof LStateClass) {
      _inferFullState((JvmDeclaredType)type, (LStateClass)enumX, acceptor, isPrelinkingPhase, selector);
      return;
    } else if (type instanceof JvmDeclaredType
         && enumX instanceof LBean) {
      _inferFullState((JvmDeclaredType)type, (LBean)enumX, acceptor, isPrelinkingPhase, selector);
      return;
    } else if (type instanceof JvmDeclaredType
         && enumX instanceof LEntity) {
      _inferFullState((JvmDeclaredType)type, (LEntity)enumX, acceptor, isPrelinkingPhase, selector);
      return;
    } else if (type != null
         && enumX != null) {
      _inferFullState(type, enumX, acceptor, isPrelinkingPhase, selector);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type, enumX, acceptor, isPrelinkingPhase, selector).toString());
    }
  }
}
