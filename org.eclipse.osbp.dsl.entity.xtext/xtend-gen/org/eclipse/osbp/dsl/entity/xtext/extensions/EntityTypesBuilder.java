/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.extensions;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.PreRemove;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.common.xtext.extensions.TreeAppendableExtensions;
import org.eclipse.osbp.dsl.common.xtext.jvmmodel.CommonTypesBuilder;
import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.entity.xtext.extensions.NamingExtensions;
import org.eclipse.osbp.dsl.entity.xtext.jvmmodel.AnnotationCompiler;
import org.eclipse.osbp.dsl.entity.xtext.linker.EntityLinker;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanFeature;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.dsl.semantic.entity.LOperation;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtend2.lib.StringConcatenationClient;
import org.eclipse.xtext.common.types.JvmAnnotationReference;
import org.eclipse.xtext.common.types.JvmField;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.JvmVisibility;
import org.eclipse.xtext.common.types.TypesFactory;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.util.EcoreGenericsUtil;
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class EntityTypesBuilder extends CommonTypesBuilder {
  @Inject
  @Extension
  private ModelExtensions _modelExtensions;
  
  @Inject
  @Extension
  private NamingExtensions _namingExtensions;
  
  @Inject
  @Extension
  private TreeAppendableExtensions _treeAppendableExtensions;
  
  @Inject
  private AnnotationCompiler annotationCompiler;
  
  @Inject
  private TypesFactory typesFactory;
  
  @Inject
  private TypeReferences references;
  
  @Inject
  private EcoreGenericsUtil ecoreGenericsUtil;
  
  @Inject
  private EntityLinker linker;
  
  public String htmlCode(final CharSequence s) {
    return "<code>".concat(String.valueOf(s)).concat("</code>");
  }
  
  protected JvmOperation _toDispose(final LEntity lClass) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setReturnType(this.references.getTypeForName(Void.TYPE, lClass));
      op.setSimpleName("dispose");
      this.annotationCompiler.addDisposeFieldAnnotation(lClass, op);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Calling dispose will destroy that instance. The internal state will be ");
      _builder.newLine();
      _builder.append("set to \'disposed\' and methods of that object must not be used anymore. ");
      _builder.newLine();
      _builder.append("Each call will result in runtime exceptions.<br>");
      _builder.newLine();
      _builder.append("If this object keeps composition containments, these will be disposed too. ");
      _builder.newLine();
      _builder.append("So the whole composition containment tree will be disposed on calling this method.");
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        boolean _equals = Objects.equal(it, null);
        if (_equals) {
          return;
        }
        final ITreeAppendable p = it.trace(lClass);
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("if (isDisposed()) {");
        _builder_1.newLine();
        _builder_1.append("  ");
        _builder_1.append("return;");
        _builder_1.newLine();
        _builder_1.append("}");
        _builder_1.newLine();
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_1);
        final Function1<LEntityFeature, Boolean> _function_1 = (LEntityFeature it_1) -> {
          return Boolean.valueOf(((it_1 instanceof LEntityReference) && ((LEntityReference) it_1).isCascading()));
        };
        final Iterable<LEntityFeature> compositionContainmentProps = IterableExtensions.<LEntityFeature>filter(lClass.getFeatures(), _function_1);
        boolean _isEmpty = IterableExtensions.isEmpty(compositionContainmentProps);
        boolean _not = (!_isEmpty);
        if (_not) {
          ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, "try ");
          this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, "{");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, "// Dispose all the composition references.\n");
          for (final LEntityFeature prop : compositionContainmentProps) {
            {
              final String fieldRef = "this.".concat(StringExtensions.toFirstLower(this._modelExtensions.toName(prop)));
              final String typeName = this._modelExtensions.typeName(prop);
              final String typeVar = StringExtensions.toFirstLower(typeName);
              boolean _isToMany = this._modelExtensions.isToMany(prop);
              if (_isToMany) {
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("if (");
                _builder_2.append(fieldRef);
                _builder_2.append(" != null) {");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("  ");
                _builder_2.append("for (");
                _builder_2.append(typeName, "  ");
                _builder_2.append(" ");
                _builder_2.append(typeVar, "  ");
                _builder_2.append(" : ");
                _builder_2.append(fieldRef, "  ");
                _builder_2.append(") {");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("    ");
                _builder_2.append(typeVar, "    ");
                _builder_2.append(".dispose();");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("  ");
                _builder_2.append("}");
                _builder_2.newLine();
                _builder_2.append("  ");
                _builder_2.append(fieldRef, "  ");
                _builder_2.append(" = null;");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("}");
                _builder_2.newLine();
                this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_2);
              } else {
                StringConcatenation _builder_3 = new StringConcatenation();
                _builder_3.append("if (");
                _builder_3.append(fieldRef);
                _builder_3.append(" != null) {");
                _builder_3.newLineIfNotEmpty();
                _builder_3.append("  ");
                _builder_3.append(fieldRef, "  ");
                _builder_3.append(".dispose();");
                _builder_3.newLineIfNotEmpty();
                _builder_3.append("  ");
                _builder_3.append(fieldRef, "  ");
                _builder_3.append(" = null;");
                _builder_3.newLineIfNotEmpty();
                _builder_3.append("}");
                _builder_3.newLine();
                this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_3);
              }
            }
          }
          this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
          this._treeAppendableExtensions.operator_tripleGreaterThan(p, "finally {");
        }
        LEntity _superType = lClass.getSuperType();
        boolean _notEquals = (!Objects.equal(_superType, null));
        if (_notEquals) {
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, "super.dispose();");
        } else {
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, "disposed = true;");
        }
        boolean _isEmpty_1 = IterableExtensions.isEmpty(compositionContainmentProps);
        boolean _not_1 = (!_isEmpty_1);
        if (_not_1) {
          this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        }
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(lClass, op);
    }
    return _xblockexpression;
  }
  
  protected JvmOperation _toDispose(final LBean lClass) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setReturnType(this.references.getTypeForName(Void.TYPE, lClass));
      op.setSimpleName("dispose");
      this.annotationCompiler.addDisposeFieldAnnotation(lClass, op);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Calling dispose will destroy that instance. The internal state will be ");
      _builder.newLine();
      _builder.append("set to \'disposed\' and methods of that object must not be used anymore. ");
      _builder.newLine();
      _builder.append("Each call will result in runtime exceptions.<br>");
      _builder.newLine();
      _builder.append("If this object keeps composition containments, these will be disposed too. ");
      _builder.newLine();
      _builder.append("So the whole composition containment tree will be disposed on calling this method.");
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        boolean _equals = Objects.equal(it, null);
        if (_equals) {
          return;
        }
        final ITreeAppendable p = it.trace(lClass);
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("if (isDisposed()) {");
        _builder_1.newLine();
        _builder_1.append("  ");
        _builder_1.append("return;");
        _builder_1.newLine();
        _builder_1.append("}");
        _builder_1.newLine();
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_1);
        final Function1<LBeanFeature, Boolean> _function_1 = (LBeanFeature it_1) -> {
          return Boolean.valueOf(((it_1 instanceof LEntityReference) && ((LEntityReference) it_1).isCascading()));
        };
        final Iterable<LBeanFeature> compositionContainmentProps = IterableExtensions.<LBeanFeature>filter(lClass.getFeatures(), _function_1);
        boolean _isEmpty = IterableExtensions.isEmpty(compositionContainmentProps);
        boolean _not = (!_isEmpty);
        if (_not) {
          ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, "try ");
          this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, "{");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, "// Dispose all the composition references.\n");
          for (final LBeanFeature prop : compositionContainmentProps) {
            {
              final String fieldRef = "this.".concat(StringExtensions.toFirstLower(this._modelExtensions.toName(prop)));
              final String typeName = this._modelExtensions.typeName(prop);
              final String typeVar = StringExtensions.toFirstLower(typeName);
              boolean _isToMany = this._modelExtensions.isToMany(prop);
              if (_isToMany) {
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("if (");
                _builder_2.append(fieldRef);
                _builder_2.append(" != null) {");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("  ");
                _builder_2.append("for (");
                _builder_2.append(typeName, "  ");
                _builder_2.append(" ");
                _builder_2.append(typeVar, "  ");
                _builder_2.append(" : ");
                _builder_2.append(fieldRef, "  ");
                _builder_2.append(") {");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("    ");
                _builder_2.append(typeVar, "    ");
                _builder_2.append(".dispose();");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("  ");
                _builder_2.append("}");
                _builder_2.newLine();
                _builder_2.append("  ");
                _builder_2.append(fieldRef, "  ");
                _builder_2.append(" = null;");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("}");
                _builder_2.newLine();
                this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_2);
              } else {
                StringConcatenation _builder_3 = new StringConcatenation();
                _builder_3.append("if (");
                _builder_3.append(fieldRef);
                _builder_3.append(" != null) {");
                _builder_3.newLineIfNotEmpty();
                _builder_3.append("  ");
                _builder_3.append(fieldRef, "  ");
                _builder_3.append(".dispose();");
                _builder_3.newLineIfNotEmpty();
                _builder_3.append("  ");
                _builder_3.append(fieldRef, "  ");
                _builder_3.append(" = null;");
                _builder_3.newLineIfNotEmpty();
                _builder_3.append("}");
                _builder_3.newLine();
                this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_3);
              }
            }
          }
          this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
          this._treeAppendableExtensions.operator_tripleGreaterThan(p, "finally {");
        }
        LBean _superType = lClass.getSuperType();
        boolean _notEquals = (!Objects.equal(_superType, null));
        if (_notEquals) {
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, "super.dispose();");
        } else {
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, "disposed = true;");
        }
        boolean _isEmpty_1 = IterableExtensions.isEmpty(compositionContainmentProps);
        boolean _not_1 = (!_isEmpty_1);
        if (_not_1) {
          this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        }
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(lClass, op);
    }
    return _xblockexpression;
  }
  
  public JvmField toField(final LStateClass stateClass) {
    JvmField _xblockexpression = null;
    {
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName(StringExtensions.toFirstLower(this._modelExtensions.toName(stateClass)));
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this.cloneWithProxies(this._modelExtensions.toTypeReferenceWithMultiplicity(stateClass)));
      this.setDocumentation(jvmField, this.getDocumentation(stateClass));
      this.annotationCompiler.processAnnotation(stateClass, jvmField);
      _xblockexpression = this.<JvmField>associate(stateClass, jvmField);
    }
    return _xblockexpression;
  }
  
  @Override
  public JvmField toField(final LFeature prop) {
    return this.internalToField(prop);
  }
  
  protected JvmField _internalToField(final LBeanFeature prop) {
    JvmField _xblockexpression = null;
    {
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName(this._modelExtensions.toName(prop));
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this.cloneWithProxies(this._modelExtensions.toTypeReferenceWithMultiplicity(prop)));
      this.setDocumentation(jvmField, this.getDocumentation(prop));
      this.annotationCompiler.processAnnotation(prop, jvmField);
      _xblockexpression = this.<JvmField>associate(prop, jvmField);
    }
    return _xblockexpression;
  }
  
  protected JvmField _internalToField(final LBeanReference prop) {
    JvmField _xblockexpression = null;
    {
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName(this._modelExtensions.toName(prop));
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this._modelExtensions.toTypeReferenceWithMultiplicity(prop));
      this.setDocumentation(jvmField, this.getDocumentation(prop));
      this.annotationCompiler.processAnnotation(prop, jvmField);
      _xblockexpression = this.<JvmField>associate(prop, jvmField);
    }
    return _xblockexpression;
  }
  
  protected JvmField _internalToField(final LEntityAttribute prop) {
    JvmField _xblockexpression = null;
    {
      final LEntity entity = prop.getEntity();
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName(this._modelExtensions.toName(prop));
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this.cloneWithProxies(this._modelExtensions.toTypeReferenceWithMultiplicity(prop)));
      this.setDocumentation(jvmField, this.getDocumentation(prop));
      if ((this._modelExtensions.isUUID(prop) && (!(entity.isTimedependent() || entity.isHistorized())))) {
        final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
          if ((it == null)) {
            return;
          }
          final ITreeAppendable p = it.trace(prop);
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("java.util.UUID.randomUUID().toString()");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder);
        };
        this.setInitializer(jvmField, _function);
      } else {
        if ((this._modelExtensions.isID(prop) && (entity.isTimedependent() || entity.isHistorized()))) {
          final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it) -> {
            if ((it == null)) {
              return;
            }
            final ITreeAppendable p = it.trace(prop);
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("new UUIDHist()");
            this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder);
          };
          this.setInitializer(jvmField, _function_1);
        }
      }
      this.annotationCompiler.processAnnotation(prop, jvmField);
      _xblockexpression = this.<JvmField>associate(prop, jvmField);
    }
    return _xblockexpression;
  }
  
  protected JvmField _internalToField(final LEntityReference prop) {
    JvmField _xblockexpression = null;
    {
      final LEntity entity = prop.getEntity();
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName(this._modelExtensions.toName(prop));
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this._modelExtensions.toTypeReferenceWithMultiplicity(prop));
      this.setDocumentation(jvmField, this.getDocumentation(prop));
      if ((this._modelExtensions.isUUID(prop) || ((entity.isTimedependent() || entity.isHistorized()) && 
        this.uuidPresent(entity)))) {
        final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
          boolean _equals = Objects.equal(it, null);
          if (_equals) {
            return;
          }
          final ITreeAppendable p = it.trace(prop);
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("java.util.UUID.randomUUID().toString()");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder);
        };
        this.setInitializer(jvmField, _function);
      }
      this.annotationCompiler.processAnnotation(prop, jvmField);
      _xblockexpression = this.<JvmField>associate(prop, jvmField);
    }
    return _xblockexpression;
  }
  
  public boolean uuidPresent(final LEntity entity) {
    final Function1<LEntityAttribute, Boolean> _function = (LEntityAttribute it) -> {
      return Boolean.valueOf(it.isUuid());
    };
    return IterableExtensions.<LEntityAttribute>exists(entity.getAllAttributes(), _function);
  }
  
  public JvmOperation toMethod(final LOperation sourceElement, final String name, final JvmTypeReference returnType, final Procedure1<? super JvmOperation> initializer) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setSimpleName(name);
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setReturnType(this.cloneWithProxies(returnType));
      this.annotationCompiler.processAnnotation(sourceElement, op);
      this.<JvmOperation>associate(sourceElement, op);
      _xblockexpression = this.<JvmOperation>initializeSafely(op, initializer);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toSetter(final LEntityFeature prop) {
    boolean _isToMany = this._modelExtensions.isToMany(prop);
    if (_isToMany) {
      throw new RuntimeException("toMany-References not allowed for setters!");
    }
    final String paramName = this._namingExtensions.toMethodParamName(prop);
    final JvmTypeReference typeRef = this._modelExtensions.toTypeReference(prop);
    LFeature _xifexpression = null;
    if ((prop instanceof LEntityReference)) {
      _xifexpression = this._modelExtensions.getResolvedOpposite(((LEntityReference) prop));
    } else {
      _xifexpression = null;
    }
    final LFeature opposite = _xifexpression;
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._namingExtensions.toSetterName(prop));
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, typeRef);
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    String _plus = CommonTypesBuilder.operator_plus("Sets the ", paramName);
    String _plus_1 = CommonTypesBuilder.operator_plus(_plus, " property to this instance.");
    String _xifexpression_1 = null;
    boolean _notEquals = (!Objects.equal(opposite, null));
    if (_notEquals) {
      String _plus_2 = CommonTypesBuilder.operator_plus(
        "\nSince the reference is a container reference, the opposite reference ", "(");
      String _typeName = this._modelExtensions.typeName(prop);
      String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, _typeName);
      String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, ".");
      String _firstLower = StringExtensions.toFirstLower(opposite.getName());
      String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, _firstLower);
      String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, ")\n");
      String _plus_7 = CommonTypesBuilder.operator_plus(_plus_6, "of the ");
      String _plus_8 = CommonTypesBuilder.operator_plus(_plus_7, paramName);
      String _plus_9 = CommonTypesBuilder.operator_plus(_plus_8, 
        " will be handled automatically and no further coding is required to keep them in sync.\n");
      String _plus_10 = CommonTypesBuilder.operator_plus(_plus_9, 
        "See {@link ");
      String _typeName_1 = this._modelExtensions.typeName(prop);
      String _plus_11 = CommonTypesBuilder.operator_plus(_plus_10, _typeName_1);
      String _plus_12 = CommonTypesBuilder.operator_plus(_plus_11, "#");
      String _setterName = this._namingExtensions.toSetterName(opposite);
      String _plus_13 = CommonTypesBuilder.operator_plus(_plus_12, _setterName);
      String _plus_14 = CommonTypesBuilder.operator_plus(_plus_13, "(");
      String _typeName_2 = this._modelExtensions.typeName(prop);
      String _plus_15 = CommonTypesBuilder.operator_plus(_plus_14, _typeName_2);
      _xifexpression_1 = CommonTypesBuilder.operator_plus(_plus_15, ")}.");
    } else {
      _xifexpression_1 = "";
    }
    String _plus_16 = CommonTypesBuilder.operator_plus(_plus_1, _xifexpression_1);
    this.setDocumentation(op, _plus_16);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _checkDisposedCall);
      String _name = this._modelExtensions.toName(prop);
      final String fieldRef = CommonTypesBuilder.operator_plus("this.", _name);
      boolean _equals_1 = Objects.equal(opposite, null);
      if (_equals_1) {
        String _plus_17 = CommonTypesBuilder.operator_plus(fieldRef, " = ");
        String _plus_18 = CommonTypesBuilder.operator_plus(_plus_17, paramName);
        String _plus_19 = CommonTypesBuilder.operator_plus(_plus_18, ";");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_19);
      } else {
        String _plus_20 = CommonTypesBuilder.operator_plus("if (", fieldRef);
        String _plus_21 = CommonTypesBuilder.operator_plus(_plus_20, " != null) ");
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_21);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, "{");
        boolean _isToMany_1 = this._modelExtensions.isToMany(opposite);
        if (_isToMany_1) {
          String _plus_22 = CommonTypesBuilder.operator_plus(fieldRef, ".");
          String _collectionInternalRemoverName = this._namingExtensions.toCollectionInternalRemoverName(opposite);
          String _plus_23 = CommonTypesBuilder.operator_plus(_plus_22, _collectionInternalRemoverName);
          String _plus_24 = CommonTypesBuilder.operator_plus(_plus_23, "(this);");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_24);
        } else {
          String _plus_25 = CommonTypesBuilder.operator_plus(fieldRef, ".");
          String _internalSetterName = this._namingExtensions.toInternalSetterName(opposite);
          String _plus_26 = CommonTypesBuilder.operator_plus(_plus_25, _internalSetterName);
          String _plus_27 = CommonTypesBuilder.operator_plus(_plus_26, "(null);");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_27);
        }
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        String _firstUpper = StringExtensions.toFirstUpper(this._modelExtensions.toName(prop));
        String _plus_28 = CommonTypesBuilder.operator_plus("internalSet", _firstUpper);
        String _plus_29 = CommonTypesBuilder.operator_plus(_plus_28, "(");
        String _plus_30 = CommonTypesBuilder.operator_plus(_plus_29, paramName);
        String _plus_31 = CommonTypesBuilder.operator_plus(_plus_30, ");\n");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_31);
        String _plus_32 = CommonTypesBuilder.operator_plus("if (", fieldRef);
        String _plus_33 = CommonTypesBuilder.operator_plus(_plus_32, " != null) ");
        ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_33);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan_1, "{");
        boolean _isToMany_2 = this._modelExtensions.isToMany(opposite);
        if (_isToMany_2) {
          String _plus_34 = CommonTypesBuilder.operator_plus(fieldRef, ".");
          String _collectionInternalAdderName = this._namingExtensions.toCollectionInternalAdderName(opposite);
          String _plus_35 = CommonTypesBuilder.operator_plus(_plus_34, _collectionInternalAdderName);
          String _plus_36 = CommonTypesBuilder.operator_plus(_plus_35, "(this);");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_36);
        } else {
          String _plus_37 = CommonTypesBuilder.operator_plus(fieldRef, ".");
          String _internalSetterName_1 = this._namingExtensions.toInternalSetterName(opposite);
          String _plus_38 = CommonTypesBuilder.operator_plus(_plus_37, _internalSetterName_1);
          String _plus_39 = CommonTypesBuilder.operator_plus(_plus_38, "(this);");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_39);
        }
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
      }
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmOperation toSetter(final LStateClass prop) {
    final String paramName = StringExtensions.toFirstLower(prop.getName());
    final JvmTypeReference typeRef = this._modelExtensions.toTypeReference(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._namingExtensions.toSetterName(prop));
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, typeRef);
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    String _plus = CommonTypesBuilder.operator_plus("Sets the ", paramName);
    String _plus_1 = CommonTypesBuilder.operator_plus(_plus, " property to this instance.");
    this.setDocumentation(op, _plus_1);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _checkDisposedCall);
      String _firstLower = StringExtensions.toFirstLower(this._modelExtensions.toName(prop));
      final String fieldRef = CommonTypesBuilder.operator_plus("this.", _firstLower);
      String _plus_2 = CommonTypesBuilder.operator_plus(fieldRef, " = ");
      String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, paramName);
      String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, ";");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_4);
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmOperation toSetter(final LBeanFeature prop) {
    boolean _isToMany = this._modelExtensions.isToMany(prop);
    if (_isToMany) {
      throw new RuntimeException("toMany-References not allowed for setters!");
    }
    final String paramName = this._namingExtensions.toMethodParamName(prop);
    final JvmTypeReference typeRef = this._modelExtensions.toTypeReference(prop);
    LFeature _xifexpression = null;
    if ((prop instanceof LBeanReference)) {
      _xifexpression = this._modelExtensions.getResolvedOpposite(((LBeanReference) prop));
    } else {
      _xifexpression = null;
    }
    final LFeature opposite = _xifexpression;
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._namingExtensions.toSetterName(prop));
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, typeRef);
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    String _plus = CommonTypesBuilder.operator_plus("Sets the ", paramName);
    String _plus_1 = CommonTypesBuilder.operator_plus(_plus, " property to this instance.");
    String _xifexpression_1 = null;
    boolean _notEquals = (!Objects.equal(opposite, null));
    if (_notEquals) {
      String _plus_2 = CommonTypesBuilder.operator_plus(
        "\nSince the reference is a container reference, the opposite reference ", "(");
      String _typeName = this._modelExtensions.typeName(prop);
      String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, _typeName);
      String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, ".");
      String _firstLower = StringExtensions.toFirstLower(opposite.getName());
      String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, _firstLower);
      String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, ")\n");
      String _plus_7 = CommonTypesBuilder.operator_plus(_plus_6, "of the ");
      String _plus_8 = CommonTypesBuilder.operator_plus(_plus_7, paramName);
      String _plus_9 = CommonTypesBuilder.operator_plus(_plus_8, 
        " will be handled automatically and no further coding is required to keep them in sync.\n");
      String _plus_10 = CommonTypesBuilder.operator_plus(_plus_9, 
        "See {@link ");
      String _typeName_1 = this._modelExtensions.typeName(prop);
      String _plus_11 = CommonTypesBuilder.operator_plus(_plus_10, _typeName_1);
      String _plus_12 = CommonTypesBuilder.operator_plus(_plus_11, "#");
      String _setterName = this._namingExtensions.toSetterName(opposite);
      String _plus_13 = CommonTypesBuilder.operator_plus(_plus_12, _setterName);
      String _plus_14 = CommonTypesBuilder.operator_plus(_plus_13, "(");
      String _typeName_2 = this._modelExtensions.typeName(prop);
      String _plus_15 = CommonTypesBuilder.operator_plus(_plus_14, _typeName_2);
      _xifexpression_1 = CommonTypesBuilder.operator_plus(_plus_15, ")}.");
    } else {
      _xifexpression_1 = "";
    }
    String _plus_16 = CommonTypesBuilder.operator_plus(_plus_1, _xifexpression_1);
    this.setDocumentation(op, _plus_16);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _checkDisposedCall);
      String _name = this._modelExtensions.toName(prop);
      final String fieldRef = CommonTypesBuilder.operator_plus("this.", _name);
      boolean _equals_1 = Objects.equal(opposite, null);
      if (_equals_1) {
        String _plus_17 = CommonTypesBuilder.operator_plus(fieldRef, " = ");
        String _plus_18 = CommonTypesBuilder.operator_plus(_plus_17, paramName);
        String _plus_19 = CommonTypesBuilder.operator_plus(_plus_18, ";");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_19);
      } else {
        String _plus_20 = CommonTypesBuilder.operator_plus("if (", fieldRef);
        String _plus_21 = CommonTypesBuilder.operator_plus(_plus_20, " != null) ");
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_21);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, "{");
        boolean _isToMany_1 = this._modelExtensions.isToMany(opposite);
        if (_isToMany_1) {
          String _plus_22 = CommonTypesBuilder.operator_plus(fieldRef, ".");
          String _collectionInternalRemoverName = this._namingExtensions.toCollectionInternalRemoverName(opposite);
          String _plus_23 = CommonTypesBuilder.operator_plus(_plus_22, _collectionInternalRemoverName);
          String _plus_24 = CommonTypesBuilder.operator_plus(_plus_23, "(this);");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_24);
        } else {
          String _plus_25 = CommonTypesBuilder.operator_plus(fieldRef, ".");
          String _internalSetterName = this._namingExtensions.toInternalSetterName(opposite);
          String _plus_26 = CommonTypesBuilder.operator_plus(_plus_25, _internalSetterName);
          String _plus_27 = CommonTypesBuilder.operator_plus(_plus_26, "(null);");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_27);
        }
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        String _plus_28 = CommonTypesBuilder.operator_plus(fieldRef, " = ");
        String _plus_29 = CommonTypesBuilder.operator_plus(_plus_28, paramName);
        String _plus_30 = CommonTypesBuilder.operator_plus(_plus_29, ";\n");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_30);
        String _plus_31 = CommonTypesBuilder.operator_plus("if (", fieldRef);
        String _plus_32 = CommonTypesBuilder.operator_plus(_plus_31, " != null) ");
        ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_32);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan_1, "{");
        boolean _isToMany_2 = this._modelExtensions.isToMany(opposite);
        if (_isToMany_2) {
          String _plus_33 = CommonTypesBuilder.operator_plus(fieldRef, ".");
          String _collectionInternalAdderName = this._namingExtensions.toCollectionInternalAdderName(opposite);
          String _plus_34 = CommonTypesBuilder.operator_plus(_plus_33, _collectionInternalAdderName);
          String _plus_35 = CommonTypesBuilder.operator_plus(_plus_34, "(this);");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_35);
        } else {
          String _plus_36 = CommonTypesBuilder.operator_plus(fieldRef, ".");
          String _internalSetterName_1 = this._namingExtensions.toInternalSetterName(opposite);
          String _plus_37 = CommonTypesBuilder.operator_plus(_plus_36, _internalSetterName_1);
          String _plus_38 = CommonTypesBuilder.operator_plus(_plus_37, "(this);");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_38);
        }
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
      }
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  /**
   * Builds an adder method for a *toMany relation like
   * <code>Order.addToOrderLines(OrderLine orderLine)</code>.
   */
  protected JvmOperation _toAdder(final LEntityFeature prop, final String propertyName) {
    final String paramName = StringExtensions.toFirstLower(this._modelExtensions.typeName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._namingExtensions.toCollectionAdderName(prop));
    JvmTypeReference _typeReference = this._modelExtensions.toTypeReference(prop);
    boolean _notEquals = (!Objects.equal(_typeReference, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(prop, paramName, this._modelExtensions.toTypeReference(prop));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Adds the given ");
    _builder.append(paramName);
    _builder.append(" to this object. <p>");
    _builder.newLineIfNotEmpty();
    {
      LFeature _opposite = this._modelExtensions.opposite(prop);
      boolean _notEquals_1 = (!Objects.equal(_opposite, null));
      if (_notEquals_1) {
        _builder.append("Since the reference is a composition reference, the opposite reference (");
        String _typeName = this._modelExtensions.typeName(prop);
        _builder.append(_typeName);
        _builder.append(".");
        String _firstLower = StringExtensions.toFirstLower(this._modelExtensions.opposite(prop).getName());
        _builder.append(_firstLower);
        _builder.append(")");
        _builder.newLineIfNotEmpty();
        _builder.append("of the ");
        _builder.append(paramName);
        _builder.append(" will be handled automatically and no further coding is required to keep them in sync. ");
        _builder.newLineIfNotEmpty();
        _builder.append("See {@link ");
        String _typeName_1 = this._modelExtensions.typeName(prop);
        _builder.append(_typeName_1);
        _builder.append("#");
        String _setterName = this._namingExtensions.toSetterName(this._modelExtensions.opposite(prop));
        _builder.append(_setterName);
        _builder.append("(");
        String _typeName_2 = this._modelExtensions.typeName(prop);
        _builder.append(_typeName_2);
        _builder.append(")}.");
        _builder.newLineIfNotEmpty();
      } else {
        _builder.append("ATTENTION:<br>");
        _builder.newLine();
        _builder.append("The reference is a composition reference, but no opposite is available.");
        _builder.newLine();
        _builder.append("So the opposite will NOT be handled. Therefore you have to ensure that the parent of the reference");
        _builder.newLine();
        _builder.append("is set properly.");
        _builder.newLine();
        _builder.append("\t\t");
      }
    }
    this.setDocumentation(op, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_add(p, _checkDisposedCall);
      LFeature _opposite_1 = this._modelExtensions.opposite(prop);
      boolean _notEquals_2 = (!Objects.equal(_opposite_1, null));
      if (_notEquals_2) {
        String _plus = CommonTypesBuilder.operator_plus(paramName, ".");
        String _setterName_1 = this._namingExtensions.toSetterName(this._modelExtensions.opposite(prop));
        String _plus_1 = CommonTypesBuilder.operator_plus(_plus, _setterName_1);
        String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, "(this);");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_2);
      } else {
        String _collectionInternalGetterName = this._namingExtensions.toCollectionInternalGetterName(prop);
        String _plus_3 = CommonTypesBuilder.operator_plus("if (!", _collectionInternalGetterName);
        String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, "().contains(");
        String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, paramName);
        String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, "))");
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_6);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, "{");
        String _collectionInternalAdderName = this._namingExtensions.toCollectionInternalAdderName(prop);
        String _plus_7 = CommonTypesBuilder.operator_plus(_collectionInternalAdderName, "(");
        String _plus_8 = CommonTypesBuilder.operator_plus(_plus_7, paramName);
        String _plus_9 = CommonTypesBuilder.operator_plus(_plus_8, ");");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_9);
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
      }
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  /**
   * Builds an adder method for a *toMany relation like
   * <code>Order.addToOrderLines(OrderLine orderLine)</code>.
   */
  protected JvmOperation _toAdder(final LBeanFeature prop, final String propertyName) {
    final String paramName = StringExtensions.toFirstLower(this._modelExtensions.typeName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._namingExtensions.toCollectionAdderName(prop));
    JvmTypeReference _typeReference = this._modelExtensions.toTypeReference(prop);
    boolean _notEquals = (!Objects.equal(_typeReference, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(prop, paramName, this._modelExtensions.toTypeReference(prop));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Adds the given ");
    _builder.append(paramName);
    _builder.append(" to this object. <p>");
    _builder.newLineIfNotEmpty();
    {
      LFeature _opposite = this._modelExtensions.opposite(prop);
      boolean _notEquals_1 = (!Objects.equal(_opposite, null));
      if (_notEquals_1) {
        _builder.append("Since the reference is a composition reference, the opposite reference (");
        String _typeName = this._modelExtensions.typeName(prop);
        _builder.append(_typeName);
        _builder.append(".");
        String _firstLower = StringExtensions.toFirstLower(this._modelExtensions.opposite(prop).getName());
        _builder.append(_firstLower);
        _builder.append(")");
        _builder.newLineIfNotEmpty();
        _builder.append("of the ");
        _builder.append(paramName);
        _builder.append(" will be handled automatically and no further coding is required to keep them in sync. ");
        _builder.newLineIfNotEmpty();
        _builder.append("See {@link ");
        String _typeName_1 = this._modelExtensions.typeName(prop);
        _builder.append(_typeName_1);
        _builder.append("#");
        String _setterName = this._namingExtensions.toSetterName(this._modelExtensions.opposite(prop));
        _builder.append(_setterName);
        _builder.append("(");
        String _typeName_2 = this._modelExtensions.typeName(prop);
        _builder.append(_typeName_2);
        _builder.append(")}.");
        _builder.newLineIfNotEmpty();
      } else {
        LType _type = this._modelExtensions.type(prop);
        boolean _not = (!(_type instanceof LBean));
        if (_not) {
          _builder.append("ATTENTION:<br>");
          _builder.newLine();
          _builder.append("The reference is a composition reference, but no opposite is available.");
          _builder.newLine();
          _builder.append("So the opposite will NOT be handled. Therefore you have to ensure that the parent of the reference");
          _builder.newLine();
          _builder.append("is set properly.");
          _builder.newLine();
          _builder.append("\t\t");
        }
      }
    }
    this.setDocumentation(op, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_add(p, _checkDisposedCall);
      LFeature _opposite_1 = this._modelExtensions.opposite(prop);
      boolean _notEquals_2 = (!Objects.equal(_opposite_1, null));
      if (_notEquals_2) {
        String _plus = CommonTypesBuilder.operator_plus(paramName, ".");
        String _setterName_1 = this._namingExtensions.toSetterName(this._modelExtensions.opposite(prop));
        String _plus_1 = CommonTypesBuilder.operator_plus(_plus, _setterName_1);
        String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, "(this);");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_2);
      } else {
        String _getterName = this._namingExtensions.toGetterName(prop);
        String _plus_3 = CommonTypesBuilder.operator_plus("if (!", _getterName);
        String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, "().contains(");
        String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, paramName);
        String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, "))");
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_6);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, "{");
        String _collectionInternalAdderName = this._namingExtensions.toCollectionInternalAdderName(prop);
        String _plus_7 = CommonTypesBuilder.operator_plus(_collectionInternalAdderName, "(");
        String _plus_8 = CommonTypesBuilder.operator_plus(_plus_7, paramName);
        String _plus_9 = CommonTypesBuilder.operator_plus(_plus_8, ");");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_9);
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
      }
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  /**
   * Builds a setter method for a *toMany relation like
   * <code>Order.setOrderLines(List&lt;OrderLine&gt; orderLines)</code>.
   */
  public JvmOperation toCollectionSetter(final LFeature prop, final String propertyName) {
    final String paramName = StringExtensions.toFirstLower(this._modelExtensions.typeName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._namingExtensions.toSetterName(prop));
    JvmTypeReference _typeReference = this._modelExtensions.toTypeReference(prop);
    boolean _notEquals = (!Objects.equal(_typeReference, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(prop, propertyName, this._modelExtensions.toTypeReferenceWithMultiplicity(prop));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Sets the given ");
    _builder.append(propertyName);
    _builder.append(" to the object. Currently contained ");
    _builder.append(propertyName);
    _builder.append(" instances will be removed.");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("@param ");
    _builder.append(propertyName);
    _builder.append(" the list of new instances");
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("// remove the old ");
        _builder.append(paramName);
        _builder.newLineIfNotEmpty();
        _builder.append("for(");
        String _typeName = EntityTypesBuilder.this._modelExtensions.typeName(prop);
        _builder.append(_typeName);
        _builder.append(" oldElement : new ArrayList<");
        String _typeName_1 = EntityTypesBuilder.this._modelExtensions.typeName(prop);
        _builder.append(_typeName_1);
        _builder.append(">(this.");
        String _collectionInternalGetterName = EntityTypesBuilder.this._namingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName);
        _builder.append("())){");
        _builder.newLineIfNotEmpty();
        _builder.append("  ");
        String _collectionRemoverName = EntityTypesBuilder.this._namingExtensions.toCollectionRemoverName(prop);
        _builder.append(_collectionRemoverName, "  ");
        _builder.append("(oldElement);");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
        _builder.newLine();
        _builder.append("// add the new ");
        _builder.append(paramName);
        _builder.newLineIfNotEmpty();
        _builder.append("for(");
        String _typeName_2 = EntityTypesBuilder.this._modelExtensions.typeName(prop);
        _builder.append(_typeName_2);
        _builder.append(" newElement : ");
        _builder.append(propertyName);
        _builder.append("){");
        _builder.newLineIfNotEmpty();
        _builder.append("  ");
        String _collectionAdderName = EntityTypesBuilder.this._namingExtensions.toCollectionAdderName(prop);
        _builder.append(_collectionAdderName, "  ");
        _builder.append("(newElement);");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  /**
   * Builds a setter method for a *toMany relation like
   * <code>Order.setOrderLines(List&lt;OrderLine&gt; orderLines)</code>.
   */
  public JvmOperation toPreRemove(final LEntity entity) {
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PROTECTED);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, entity));
    op.setSimpleName("preRemove");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Iterates all cross references and removes them from the parent to avoid ConstraintViolationException");
    this.setDocumentation(op, _builder);
    EList<JvmAnnotationReference> _annotations = op.getAnnotations();
    JvmAnnotationReference _annotation = this.toAnnotation(entity, PreRemove.class);
    this.<JvmAnnotationReference>operator_add(_annotations, _annotation);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        {
          List<LEntityReference> _references = entity.getReferences();
          for(final LEntityReference ref : _references) {
            {
              if ((((!ref.isCascading()) && (!Objects.equal(ref.getOpposite(), null))) && EntityTypesBuilder.this._modelExtensions.getBounds(ref).isToMany())) {
                _builder.append("// remove the ");
                String _name = ref.getName();
                _builder.append(_name);
                _builder.newLineIfNotEmpty();
                _builder.append("for(");
                String _typeName = EntityTypesBuilder.this._modelExtensions.typeName(ref);
                _builder.append(_typeName);
                _builder.append(" oldElement : new ArrayList<");
                String _typeName_1 = EntityTypesBuilder.this._modelExtensions.typeName(ref);
                _builder.append(_typeName_1);
                _builder.append(">(this.");
                String _collectionInternalGetterName = EntityTypesBuilder.this._namingExtensions.toCollectionInternalGetterName(ref);
                _builder.append(_collectionInternalGetterName);
                _builder.append("())){");
                _builder.newLineIfNotEmpty();
                _builder.append("  ");
                String _collectionRemoverName = EntityTypesBuilder.this._namingExtensions.toCollectionRemoverName(ref);
                _builder.append(_collectionRemoverName, "  ");
                _builder.append("(oldElement);");
                _builder.newLineIfNotEmpty();
                _builder.append("}");
                _builder.newLine();
              }
            }
          }
        }
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(entity, op);
  }
  
  public JvmVisibility getInternalMethodVisibility(final LFeature ref) {
    JvmVisibility _xblockexpression = null;
    {
      EObject _eContainer = ref.eContainer();
      final LPackage ownerPackage = this._modelExtensions.getPackage(((LType) _eContainer));
      final LPackage refPackage = this._modelExtensions.getPackage(this._modelExtensions.type(ref));
      JvmVisibility _xifexpression = null;
      boolean _equals = ownerPackage.equals(refPackage);
      if (_equals) {
        _xifexpression = null;
      } else {
        _xifexpression = JvmVisibility.PUBLIC;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  @Override
  public JvmOperation toIsDisposed(final LClass lClass) {
    final JvmOperation op = super.toIsDisposed(lClass);
    this.annotationCompiler.addDisposeFieldAnnotation(lClass, op);
    return op;
  }
  
  public JvmField toDiposeField(final LType sourceElement) {
    final JvmField field = this.toPrimitiveTypeField(sourceElement, "disposed", Boolean.TYPE);
    this.<JvmField>associate(sourceElement, field);
    this.annotationCompiler.addDisposeFieldAnnotation(sourceElement, field);
    return field;
  }
  
  public JvmOperation toInternalSetter(final LFeature prop) {
    final String paramName = this._namingExtensions.toMethodParamName(prop);
    final JvmTypeReference typeRef = this._modelExtensions.toTypeReference(prop);
    final JvmOperation result = this.typesFactory.createJvmOperation();
    result.setVisibility(JvmVisibility.PUBLIC);
    result.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    result.setSimpleName(this._namingExtensions.toInternalSetterName(prop));
    EList<JvmFormalParameter> _parameters = result.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, typeRef);
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("For internal use only!");
    this.setDocumentation(result, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _name = this._modelExtensions.toName(prop);
      String _plus = CommonTypesBuilder.operator_plus("this.", _name);
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, " = ");
      String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, paramName);
      String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, ";");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_3);
    };
    this.setBody(result, _function);
    return this.<JvmOperation>associate(prop, result);
  }
  
  public JvmOperation toInternalAdder(final LFeature prop) {
    final String paramName = StringExtensions.toFirstLower(this._modelExtensions.typeName(prop));
    final JvmTypeReference typeRef = this._modelExtensions.toTypeReference(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._namingExtensions.toCollectionInternalAdderName(prop));
    final JvmFormalParameter param = this.toParameter(prop, paramName, typeRef);
    boolean _notEquals = (!Objects.equal(param, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      this.<JvmFormalParameter>operator_add(_parameters, param);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("For internal use only!");
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("if(");
        _builder.append(paramName);
        _builder.append(" == null) {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("return;");
        _builder.newLine();
        _builder.append("}");
        _builder.newLine();
        _builder.newLine();
        _builder.append("\t\t");
        String _collectionInternalGetterName = EntityTypesBuilder.this._namingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName, "\t\t");
        _builder.append("().add(");
        _builder.append(paramName, "\t\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmOperation toInternalRemover(final LFeature prop) {
    final String paramName = StringExtensions.toFirstLower(this._modelExtensions.typeName(prop));
    final JvmTypeReference typeRef = this._modelExtensions.toTypeReference(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._namingExtensions.toCollectionInternalRemoverName(prop));
    final JvmFormalParameter param = this.toParameter(prop, paramName, typeRef);
    boolean _notEquals = (!Objects.equal(param, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      this.<JvmFormalParameter>operator_add(_parameters, param);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("For internal use only!");
    this.setDocumentation(op, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _collectionInternalGetterName = this._namingExtensions.toCollectionInternalGetterName(prop);
      String _plus = CommonTypesBuilder.operator_plus(_collectionInternalGetterName, "()");
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, ".remove(");
      String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, paramName);
      String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, ");");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_3);
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmOperation toInternalCollectionGetter(final LFeature prop, final String name) {
    JvmOperation _xblockexpression = null;
    {
      final String fieldName = StringExtensions.toFirstLower(name);
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setReturnType(this._modelExtensions.toTypeReferenceWithMultiplicity(prop));
      op.setSimpleName(this._namingExtensions.toCollectionInternalGetterName(prop));
      String _htmlCode = this.htmlCode(this._modelExtensions.typeName(prop));
      String _plus = CommonTypesBuilder.operator_plus("For internal use only! Returns the list of ", _htmlCode);
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, 
        "s thereby lazy initializing it.");
      this.setDocumentation(op, _plus_1);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        boolean _equals = Objects.equal(it, null);
        if (_equals) {
          return;
        }
        ITreeAppendable p = it.trace(prop);
        final String fieldRef = CommonTypesBuilder.operator_plus("this.", fieldName);
        String _plus_2 = CommonTypesBuilder.operator_plus("if (", fieldRef);
        String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, " == null)");
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_3);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, " {");
        ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(p, fieldRef);
        ITreeAppendable _doubleGreaterThan_2 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_1, " = new ");
        JvmTypeReference _newTypeRef = this.newTypeRef(prop, ArrayList.class, this._modelExtensions.toTypeReference(prop));
        ITreeAppendable _doubleGreaterThan_3 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_2, _newTypeRef);
        this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_3, "();");
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        String _plus_4 = CommonTypesBuilder.operator_plus("return ", fieldRef);
        String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, ";");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_5);
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(prop, op);
    }
    return _xblockexpression;
  }
  
  /**
   * Builds a remover method for a *toMany relation like
   * <code>Order.removeFromOrderLines(OrderLine orderLine)</code>.
   */
  public JvmOperation toRemover(final LFeature prop, final String propertyName) {
    final String paramName = StringExtensions.toFirstLower(this._modelExtensions.typeName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._namingExtensions.toCollectionRemoverName(prop));
    JvmTypeReference _typeReference = this._modelExtensions.toTypeReference(prop);
    boolean _notEquals = (!Objects.equal(_typeReference, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(prop, paramName, this._modelExtensions.toTypeReference(prop));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    }
    LFeature _opposite = this._modelExtensions.opposite(prop);
    boolean _notEquals_1 = (!Objects.equal(_opposite, null));
    if (_notEquals_1) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Removes the given ");
      _builder.append(paramName);
      _builder.append(" from this object. <p>");
      _builder.newLineIfNotEmpty();
      {
        if (((prop instanceof LEntityReference) && ((LEntityReference) prop).isCascading())) {
          _builder.append("Since the reference is a cascading reference, the opposite reference (");
          String _typeName = this._modelExtensions.typeName(prop);
          _builder.append(_typeName);
          _builder.append(".");
          String _firstLower = StringExtensions.toFirstLower(this._modelExtensions.opposite(prop).getName());
          _builder.append(_firstLower);
          _builder.append(")");
          _builder.newLineIfNotEmpty();
          _builder.append("of the ");
          _builder.append(paramName);
          _builder.append(" will be handled automatically and no further coding is required to keep them in sync. ");
          _builder.newLineIfNotEmpty();
          _builder.append("See {@link ");
          String _typeName_1 = this._modelExtensions.typeName(prop);
          _builder.append(_typeName_1);
          _builder.append("#");
          String _setterName = this._namingExtensions.toSetterName(this._modelExtensions.opposite(prop));
          _builder.append(_setterName);
          _builder.append("(");
          String _typeName_2 = this._modelExtensions.typeName(prop);
          _builder.append(_typeName_2);
          _builder.append(")}.");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t\t");
        }
      }
      this.setDocumentation(op, _builder);
    }
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_add(p, _checkDisposedCall);
      LFeature _opposite_1 = this._modelExtensions.opposite(prop);
      boolean _notEquals_2 = (!Objects.equal(_opposite_1, null));
      if (_notEquals_2) {
        String _plus = CommonTypesBuilder.operator_plus(paramName, ".");
        String _setterName_1 = this._namingExtensions.toSetterName(this._modelExtensions.opposite(prop));
        String _plus_1 = CommonTypesBuilder.operator_plus(_plus, _setterName_1);
        String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, "(null);");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_2);
      } else {
        String _collectionInternalGetterName = this._namingExtensions.toCollectionInternalGetterName(prop);
        String _plus_3 = CommonTypesBuilder.operator_plus(_collectionInternalGetterName, "().remove(");
        String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, paramName);
        String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, ");");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_5);
      }
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmOperation toDispose(final EObject lClass) {
    if (lClass instanceof LBean) {
      return _toDispose((LBean)lClass);
    } else if (lClass instanceof LEntity) {
      return _toDispose((LEntity)lClass);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(lClass).toString());
    }
  }
  
  public JvmField internalToField(final LFeature prop) {
    if (prop instanceof LBeanReference) {
      return _internalToField((LBeanReference)prop);
    } else if (prop instanceof LEntityAttribute) {
      return _internalToField((LEntityAttribute)prop);
    } else if (prop instanceof LEntityReference) {
      return _internalToField((LEntityReference)prop);
    } else if (prop instanceof LBeanFeature) {
      return _internalToField((LBeanFeature)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public JvmOperation toAdder(final LFeature prop, final String propertyName) {
    if (prop instanceof LBeanFeature) {
      return _toAdder((LBeanFeature)prop, propertyName);
    } else if (prop instanceof LEntityFeature) {
      return _toAdder((LEntityFeature)prop, propertyName);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop, propertyName).toString());
    }
  }
}
