/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.extensions;

import com.google.inject.Inject;
import java.util.Arrays;
import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.entity.xtext.util.PersistenceNamingUtils;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityColumnPersistenceInfo;
import org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class NamingExtensions extends org.eclipse.osbp.dsl.common.xtext.extensions.NamingExtensions {
  @Inject
  @Extension
  private ModelExtensions _modelExtensions;
  
  protected String _toColumnName(final LAttribute prop) {
    String _xblockexpression = null;
    {
      String columnBaseName = prop.getName();
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(columnBaseName);
      if (_isNullOrEmpty) {
        columnBaseName = PersistenceNamingUtils.camelCaseToUpperCase(this._modelExtensions.toName(prop));
      }
      _xblockexpression = columnBaseName;
    }
    return _xblockexpression;
  }
  
  protected String _toColumnName(final LStateClass prop) {
    String _xblockexpression = null;
    {
      String columnBaseName = PersistenceNamingUtils.camelCaseToUpperCase(prop.getName());
      _xblockexpression = columnBaseName;
    }
    return _xblockexpression;
  }
  
  protected String _toColumnName(final LEntityAttribute prop) {
    String _xblockexpression = null;
    {
      LEntityColumnPersistenceInfo _persistenceInfo = prop.getPersistenceInfo();
      String _columnName = null;
      if (_persistenceInfo!=null) {
        _columnName=_persistenceInfo.getColumnName();
      }
      String columnBaseName = _columnName;
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(columnBaseName);
      if (_isNullOrEmpty) {
        columnBaseName = PersistenceNamingUtils.camelCaseToUpperCase(this._modelExtensions.toName(prop));
      } else {
        columnBaseName = PersistenceNamingUtils.camelCaseToUpperCase(this._modelExtensions.replaceCaret(columnBaseName));
      }
      _xblockexpression = columnBaseName;
    }
    return _xblockexpression;
  }
  
  protected String _toColumnName(final LEntityReference prop) {
    String _xblockexpression = null;
    {
      LEntityColumnPersistenceInfo _persistenceInfo = prop.getPersistenceInfo();
      String _columnName = null;
      if (_persistenceInfo!=null) {
        _columnName=_persistenceInfo.getColumnName();
      }
      String columnBaseName = _columnName;
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(columnBaseName);
      if (_isNullOrEmpty) {
        String _camelCaseToUpperCase = PersistenceNamingUtils.camelCaseToUpperCase(this._modelExtensions.toName(prop));
        String _plus = (_camelCaseToUpperCase + "_ID");
        columnBaseName = _plus;
      } else {
        columnBaseName = PersistenceNamingUtils.camelCaseToUpperCase(this._modelExtensions.replaceCaret(columnBaseName));
      }
      _xblockexpression = columnBaseName;
    }
    return _xblockexpression;
  }
  
  protected String _toColumnName(final LBeanReference prop) {
    String _xblockexpression = null;
    {
      String columnBaseName = prop.getName();
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(columnBaseName);
      if (_isNullOrEmpty) {
        columnBaseName = PersistenceNamingUtils.camelCaseToUpperCase(this._modelExtensions.toName(prop));
      }
      _xblockexpression = columnBaseName;
    }
    return _xblockexpression;
  }
  
  public String toTableName(final LEntity entity) {
    return this.toTableName(entity, false);
  }
  
  public String toTableName(final LEntity entity, final boolean toLowerCase) {
    String _xblockexpression = null;
    {
      LEntityPersistenceInfo _persistenceInfo = entity.getPersistenceInfo();
      String _tableName = null;
      if (_persistenceInfo!=null) {
        _tableName=_persistenceInfo.getTableName();
      }
      String tableBaseName = _tableName;
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(tableBaseName);
      if (_isNullOrEmpty) {
        String _xifexpression = null;
        if (toLowerCase) {
          _xifexpression = PersistenceNamingUtils.camelCaseToLowerCase(this._modelExtensions.toName(entity));
        } else {
          _xifexpression = PersistenceNamingUtils.camelCaseToUpperCase(this._modelExtensions.toName(entity));
        }
        tableBaseName = _xifexpression;
      } else {
        String _xifexpression_1 = null;
        if (toLowerCase) {
          _xifexpression_1 = PersistenceNamingUtils.camelCaseToLowerCase(this._modelExtensions.replaceCaret(tableBaseName));
        } else {
          _xifexpression_1 = PersistenceNamingUtils.camelCaseToUpperCase(this._modelExtensions.replaceCaret(tableBaseName));
        }
        tableBaseName = _xifexpression_1;
      }
      _xblockexpression = tableBaseName;
    }
    return _xblockexpression;
  }
  
  public String toSchemaName(final LEntity entity) {
    return this.toSchemaName(entity.getPersistenceInfo());
  }
  
  public String toSchemaName(final LEntityPersistenceInfo info) {
    String _xblockexpression = null;
    {
      String schemaName = info.getSchemaName();
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(schemaName);
      boolean _not = (!_isNullOrEmpty);
      if (_not) {
        schemaName = PersistenceNamingUtils.camelCaseToUpperCase(this._modelExtensions.replaceCaret(schemaName));
      }
      _xblockexpression = schemaName;
    }
    return _xblockexpression;
  }
  
  public String toColumnName(final LAnnotationTarget prop) {
    if (prop instanceof LStateClass) {
      return _toColumnName((LStateClass)prop);
    } else if (prop instanceof LBeanReference) {
      return _toColumnName((LBeanReference)prop);
    } else if (prop instanceof LEntityAttribute) {
      return _toColumnName((LEntityAttribute)prop);
    } else if (prop instanceof LEntityReference) {
      return _toColumnName((LEntityReference)prop);
    } else if (prop instanceof LAttribute) {
      return _toColumnName((LAttribute)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
}
