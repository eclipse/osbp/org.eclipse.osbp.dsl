/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.extensions;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.xtext.common.types.JvmAnnotationTarget;

@SuppressWarnings("all")
public class AnnotationExtension extends org.eclipse.osbp.dsl.common.xtext.extensions.AnnotationExtension {
  public boolean isEntityAnnoExcluded(final LClass lEntity) {
    return this.isExcluded(Entity.class, lEntity.getAnnotations());
  }
  
  public boolean isEntityAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, Entity.class, context);
  }
  
  public boolean isEntityAnnoRedefined(final LClass lEntity) {
    return this.isRedefined(Entity.class, lEntity.getAnnotations());
  }
  
  public LAnnotationDef getEntityAnnoRedefine(final LClass lEntity) {
    return this.getRedefined(Entity.class, lEntity.getAnnotations());
  }
  
  public boolean isOneToManyAnnoExcluded(final LFeature member) {
    return this.isExcluded(OneToMany.class, member.getAnnotations());
  }
  
  public boolean isOneToManyAnnoRedefined(final LFeature member) {
    return this.isRedefined(OneToMany.class, member.getAnnotations());
  }
  
  public boolean isOneToManyValueAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, OneToMany.class, context);
  }
  
  public LAnnotationDef getOneToManyAnnoRedefine(final LFeature member) {
    return this.getRedefined(OneToMany.class, member.getAnnotations());
  }
  
  public boolean isOneToOneAnnoExcluded(final LFeature member) {
    return this.isExcluded(OneToOne.class, member.getAnnotations());
  }
  
  public boolean isOneToOneAnnoRedefined(final LFeature member) {
    return this.isRedefined(OneToOne.class, member.getAnnotations());
  }
  
  public boolean isOneToOneAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, OneToOne.class, context);
  }
  
  public LAnnotationDef getOneToOneAnnoRedefine(final LFeature member) {
    return this.getRedefined(OneToOne.class, member.getAnnotations());
  }
  
  public boolean isManyToOneAnnoExcluded(final LFeature member) {
    return this.isExcluded(ManyToOne.class, member.getAnnotations());
  }
  
  public boolean isManyToOneAnnoRedefined(final LFeature member) {
    return this.isRedefined(ManyToOne.class, member.getAnnotations());
  }
  
  public boolean isManyToOneValueAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, ManyToOne.class, context);
  }
  
  public LAnnotationDef getManyToOneAnnoRedefine(final LFeature member) {
    return this.getRedefined(ManyToOne.class, member.getAnnotations());
  }
  
  public boolean isIdAnnoExcluded(final LFeature member) {
    return this.isExcluded(Id.class, member.getAnnotations());
  }
  
  public boolean isIdAnnoRedefined(final LFeature member) {
    return this.isRedefined(Id.class, member.getAnnotations());
  }
  
  public LAnnotationDef getIdAnnoRedefine(final LFeature member) {
    return this.getRedefined(Id.class, member.getAnnotations());
  }
  
  public boolean isIdAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    final boolean result = this.containsAnnotation(target, Id.class, context);
    return result;
  }
  
  public boolean isVersionAnnoExcluded(final LFeature member) {
    return this.isExcluded(Version.class, member.getAnnotations());
  }
  
  public boolean isVersionAnnoRedefined(final LFeature member) {
    return this.isRedefined(Version.class, member.getAnnotations());
  }
  
  public boolean isVersionAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, Version.class, context);
  }
  
  public LAnnotationDef getVersionAnnoRedefine(final LFeature member) {
    return this.getRedefined(Version.class, member.getAnnotations());
  }
  
  public boolean isGeneratedValueAnnoExcluded(final LFeature member) {
    return this.isExcluded(GeneratedValue.class, member.getAnnotations());
  }
  
  public boolean isGeneratedValueAnnoRedefined(final LFeature member) {
    return this.isRedefined(GeneratedValue.class, member.getAnnotations());
  }
  
  public boolean isGeneratedValueAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, GeneratedValue.class, context);
  }
  
  public LAnnotationDef getGeneratedValueAnnoRedefine(final LFeature member) {
    return this.getRedefined(GeneratedValue.class, member.getAnnotations());
  }
  
  public boolean isCachableAnnoExcluded(final LClass member) {
    return this.isExcluded(Cacheable.class, member.getAnnotations());
  }
  
  public boolean isCacheableAnnoRedefined(final LClass member) {
    return this.isRedefined(Cacheable.class, member.getAnnotations());
  }
  
  public LAnnotationDef getCacheableAnnoRedefine(final LClass member) {
    return this.getRedefined(Cacheable.class, member.getAnnotations());
  }
  
  public boolean isCacheableAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, Cacheable.class, context);
  }
  
  public boolean isEmbeddedAnnoExcluded(final LFeature member) {
    return this.isExcluded(Embedded.class, member.getAnnotations());
  }
  
  public boolean isEmbeddedAnnoRedefined(final LFeature member) {
    return this.isRedefined(Embedded.class, member.getAnnotations());
  }
  
  public LAnnotationDef getEmbeddedAnnoRedefine(final LFeature member) {
    return this.getRedefined(Embedded.class, member.getAnnotations());
  }
  
  public boolean isEmbeddedAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, Embedded.class, context);
  }
  
  public boolean isEmbeddableAnnoExcluded(final LClass member) {
    return this.isExcluded(Embeddable.class, member.getAnnotations());
  }
  
  public boolean isEmbeddableAnnoRedefined(final LClass member) {
    return this.isRedefined(Embeddable.class, member.getAnnotations());
  }
  
  public LAnnotationDef getEmbeddableAnnoRedefine(final LClass member) {
    return this.getRedefined(Embeddable.class, member.getAnnotations());
  }
  
  public boolean isEmbeddableAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, Embeddable.class, context);
  }
  
  public boolean isColumnAnnoExcluded(final LFeature member) {
    return this.isExcluded(Column.class, member.getAnnotations());
  }
  
  public boolean isColumnAnnoRedefined(final LFeature member) {
    return this.isRedefined(Column.class, member.getAnnotations());
  }
  
  public boolean isColumnAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, Column.class, context);
  }
  
  public LAnnotationDef getColumnAnnoRedefine(final LFeature member) {
    return this.getRedefined(Column.class, member.getAnnotations());
  }
  
  public boolean isJoinColumnAnnoExcluded(final LFeature member) {
    return this.isExcluded(JoinColumn.class, member.getAnnotations());
  }
  
  public boolean isJoinColumnAnnoRedefined(final LFeature member) {
    return this.isRedefined(JoinColumn.class, member.getAnnotations());
  }
  
  public boolean isJoinColumnAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, JoinColumn.class, context);
  }
  
  public LAnnotationDef getJoinColumnAnnoRedefine(final LFeature member) {
    return this.getRedefined(JoinColumn.class, member.getAnnotations());
  }
  
  public boolean isMappedSuperclassAnnoExcluded(final LClass entity) {
    return this.isExcluded(MappedSuperclass.class, entity.getAnnotations());
  }
  
  public boolean isMappedSuperclassAnnoRedefined(final LClass entity) {
    return this.isRedefined(MappedSuperclass.class, entity.getAnnotations());
  }
  
  public boolean isMappedSuperclassAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, MappedSuperclass.class, context);
  }
  
  public LAnnotationDef getMappedSuperclassAnnoRedefine(final LClass entity) {
    return this.getRedefined(MappedSuperclass.class, entity.getAnnotations());
  }
  
  public boolean isInheritanceAnnoExcluded(final LClass entity) {
    return this.isExcluded(Inheritance.class, entity.getAnnotations());
  }
  
  public boolean isInheritanceAnnoRedefined(final LClass entity) {
    return this.isRedefined(Inheritance.class, entity.getAnnotations());
  }
  
  public boolean isInheritanceAnnoCreated(final JvmAnnotationTarget target, final EObject context) {
    return this.containsAnnotation(target, Inheritance.class, context);
  }
  
  public LAnnotationDef getInheritanceAnnoRedefine(final LClass entity) {
    return this.getRedefined(Inheritance.class, entity.getAnnotations());
  }
}
