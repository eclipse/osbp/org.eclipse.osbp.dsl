/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.util;

import com.google.common.base.Objects;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public class PersistenceNamingUtils {
  /**
   * Converts the given <code>String</code>, that contains camel-case
   * characters, to uppercase, thereby inserting underline ("_") characters
   * before uppercase characters. <br>
   * E.g.: "camelCaseToUpperCase" -&gt; "CAMEL_CASE_TO_UPPER_CASE".
   * 
   * @param pCamelCaseString
   *            The <code>String</code> to convert.
   * @return The converted <code>String</code>.
   */
  public static String camelCaseToUpperCase(final String pCamelCaseString) {
    if ((pCamelCaseString == null)) {
      return "";
    }
    boolean _equals = pCamelCaseString.equals(pCamelCaseString.toUpperCase());
    if (_equals) {
      return pCamelCaseString;
    }
    final StringBuilder sbOut = PersistenceNamingUtils.getStringBuilderOut(pCamelCaseString);
    return sbOut.toString().toUpperCase();
  }
  
  /**
   * Converts the given <code>String</code>, that contains camel-case
   * characters, to lower case, thereby inserting underline ("_") characters
   * before lower case characters. <br>
   * E.g.: "camelCaseToLowerCase" -&gt; "camel_case_to_lower_case".
   * 
   * @param pCamelCaseString
   *            The <code>String</code> to convert.
   * @return The converted <code>String</code>.
   */
  public static String camelCaseToLowerCase(final String pCamelCaseString) {
    if ((pCamelCaseString == null)) {
      return "";
    }
    boolean _equals = pCamelCaseString.equals(pCamelCaseString.toLowerCase());
    if (_equals) {
      return pCamelCaseString;
    }
    final StringBuilder sbOut = PersistenceNamingUtils.getStringBuilderOut(pCamelCaseString);
    return sbOut.toString().toLowerCase();
  }
  
  private static StringBuilder getStringBuilderOut(final String pCamelCaseString) {
    StringBuilder sb = null;
    char _charAt = pCamelCaseString.charAt(0);
    boolean _equals = Objects.equal(Character.valueOf(_charAt), "^");
    if (_equals) {
      String _substring = pCamelCaseString.substring(1);
      StringBuilder _stringBuilder = new StringBuilder(_substring);
      sb = _stringBuilder;
    }
    StringBuilder _stringBuilder_1 = new StringBuilder(pCamelCaseString);
    sb = _stringBuilder_1;
    final StringBuilder sbOut = new StringBuilder(pCamelCaseString);
    int _length = sb.length();
    final int maxIndex = (_length - 1);
    int offset = 0;
    int _length_1 = sb.length();
    int _minus = (_length_1 - 1);
    IntegerRange _upTo = new IntegerRange(1, _minus);
    for (final Integer i : _upTo) {
      try {
        if ((((((i).intValue() < maxIndex) && (!Objects.equal(Character.valueOf(sb.charAt(((i).intValue() - 1))), "_"))) && Character.isUpperCase(sb.charAt((i).intValue()))) && 
          (!Character.isUpperCase(sb.charAt(((i).intValue() + 1)))))) {
          sbOut.insert(((i).intValue() + offset), "_");
          offset = (offset + 1);
        }
      } catch (final Throwable _t) {
        if (_t instanceof Exception) {
          final Exception ex = (Exception)_t;
          InputOutput.<Exception>println(ex);
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
    }
    return sbOut;
  }
}
