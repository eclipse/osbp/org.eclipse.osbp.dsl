/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator
import org.eclipse.xtext.generator.IFileSystemAccess
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Generator extends I18NModelGenerator {

	static final Logger LOGGER = LoggerFactory.getLogger(typeof(Generator))

//	override dispatch void internalDoGenerate(JvmDeclaredType type, IFileSystemAccess fsa) {
//		val log = TimeLogger.start(typeof(Generator));
//		super._internalDoGenerate(type, fsa)
//		log.stop(LOGGER, "generated " + type.qualifiedName)
//	}

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		super.doGenerate(input, fsa)
	}

}
