/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.valueconverter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.common.xtext.valueconverter.CommonQualifiedNameProvider;
import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.entity.xtext.extensions.NamingExtensions;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityColumnPersistenceInfo;
import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;
import org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;

import com.google.inject.Inject;

public class EntityQualifiedNameProvider extends CommonQualifiedNameProvider {

	@Inject
	private IQualifiedNameConverter qualifiedNameConverter;
	@Inject
	private ModelExtensions extensions;
	@Inject
	private NamingExtensions naming;

	@SuppressWarnings("restriction")
	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		if (obj == null) {
			return QualifiedName.create("");
		}

//		if (obj instanceof LFeature) {
//			LFeature prop = (LFeature) obj;
//			return prop.getName() != null ? qualifiedNameConverter.toQualifiedName(prop.getName()) : null;
//		} else 
//			
//		if (obj instanceof LDataType) {

		if (obj instanceof LAnnotationDef) {
			return super.getFullyQualifiedName(((LAnnotationDef) obj).getAnnotation());
		} else if (obj instanceof LEntityPersistenceInfo) {
			LEntityPersistenceInfo info = (LEntityPersistenceInfo) obj;
			LEntity entity = (LEntity) info.eContainer();
			String schemaName = naming.toSchemaName(entity.getPersistenceInfo());
			if (schemaName == null || schemaName.equals("")) {
				schemaName = "DEFAULT";
			}
			String tableName = naming.toTableName(entity);
			return QualifiedName.create(schemaName, tableName);
		} else if (obj instanceof LEntityColumnPersistenceInfo) {
			LEntityColumnPersistenceInfo info = (LEntityColumnPersistenceInfo) obj;
			LEntityFeature feature = (LEntityFeature) info.eContainer();
			LEntity entity = feature.getEntity();
			QualifiedName parentFQN = getFullyQualifiedName(entity.getPersistenceInfo());
			String columnName = naming.toColumnName(feature);
			return parentFQN.append(columnName);
		}
		return super.getFullyQualifiedName(obj);
	}
}
