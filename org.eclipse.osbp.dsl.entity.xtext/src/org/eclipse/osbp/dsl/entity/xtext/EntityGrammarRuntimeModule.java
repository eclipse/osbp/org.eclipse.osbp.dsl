/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext;

import javax.inject.Singleton;

import org.eclipse.osbp.dsl.common.xtext.scoping.ScopingInfoProvider;
import org.eclipse.osbp.dsl.entity.xtext.extensions.EntityTypesBuilder;
import org.eclipse.osbp.dsl.entity.xtext.formatting.EntityGrammarFormatter;
import org.eclipse.osbp.dsl.entity.xtext.generator.Generator;
import org.eclipse.osbp.dsl.entity.xtext.imports.ShouldImportProvider;
import org.eclipse.osbp.dsl.entity.xtext.linker.EntityJvmLinkingHelper;
import org.eclipse.osbp.dsl.entity.xtext.linker.EntityLinker;
import org.eclipse.osbp.dsl.entity.xtext.scoping.EntityGrammarScopeProvider;
import org.eclipse.osbp.dsl.entity.xtext.scoping.EntityImportSectionNamespaceScopeProvider;
import org.eclipse.osbp.dsl.entity.xtext.serializer.EntityGrammarTransientValueService;
import org.eclipse.osbp.dsl.entity.xtext.valueconverter.EntityQualifiedNameProvider;
import org.eclipse.osbp.dsl.entity.xtext.valueconverter.EntityValueConverterService;
import org.eclipse.osbp.xtext.i18n.DSLOutputConfigurationProvider;
import org.eclipse.osbp.xtext.oxtype.imports.IShouldImportProvider;
import org.eclipse.osbp.xtext.oxtype.imports.OXTypeRewritableImportSection;
import org.eclipse.osbp.xtext.oxtype.linking.JvmTypeAwareLinkingHelper;
import org.eclipse.osbp.xtext.oxtype.linking.OXTypeLinkingService;
import org.eclipse.osbp.xtext.oxtype.resource.ExtendedJvmModelAssociator;
import org.eclipse.osbp.xtext.oxtype.resource.IExtendedModelAssociator;
import org.eclipse.osbp.xtext.oxtype.resource.SemanticLoadingResource;
import org.eclipse.osbp.xtext.oxtype.scoping.IScopingInfoProvider;
import org.eclipse.xtext.conversion.IValueConverterService;
import org.eclipse.xtext.findReferences.TargetURICollector;
import org.eclipse.xtext.formatting.IFormatter;
import org.eclipse.xtext.generator.IOutputConfigurationProvider;
import org.eclipse.xtext.linking.ILinkingService;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy;
import org.eclipse.xtext.resource.persistence.IResourceStorageFacade;
import org.eclipse.xtext.scoping.IScopeProvider;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.xbase.imports.RewritableImportSection;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociations;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociator;
import org.eclipse.xtext.xbase.jvmmodel.JvmModelTargetURICollector;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.resource.BatchLinkableResourceStorageFacade;
import org.eclipse.xtext.xbase.scoping.batch.XbaseBatchScopeProvider;

import com.google.inject.Binder;
import com.google.inject.name.Names;

/**
 * Use this class to register components to be used at runtime / without the
 * Equinox extension registry.
 */
@SuppressWarnings("restriction")
public class EntityGrammarRuntimeModule extends org.eclipse.osbp.dsl.entity.xtext.AbstractEntityGrammarRuntimeModule {

	public Class<? extends IResourceStorageFacade> bindResourceStorageFacade() {
		return BatchLinkableResourceStorageFacade.class;
	}

	public Class<? extends TargetURICollector> bindTargetURICollector() {
		return JvmModelTargetURICollector.class;
	}

	public Class<? extends IQualifiedNameProvider> bindIQualifiedNameProvider() {
		return EntityQualifiedNameProvider.class;
	}

	@Override
	public Class<? extends XbaseBatchScopeProvider> bindXbaseBatchScopeProvider() {
		return EntityGrammarScopeProvider.class;
	}

	@Override
	public Class<? extends IScopeProvider> bindIScopeProvider() {
		return EntityGrammarScopeProvider.class;
	}

	@Override
	public void configureIScopeProviderDelegate(Binder binder) {
		binder.bind(IScopeProvider.class)
				.annotatedWith(Names.named("org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider.delegate"))
				.to(EntityImportSectionNamespaceScopeProvider.class);
	}

	public Class<? extends IFormatter> bindIFormatter() {
		return EntityGrammarFormatter.class;
	}

	public Class<? extends org.eclipse.osbp.dsl.common.xtext.jvmmodel.AnnotationCompiler> bindAnnotationCompiler() {
		return org.eclipse.osbp.dsl.entity.xtext.jvmmodel.AnnotationCompiler.class;
	}

	public Class<? extends JvmTypesBuilder> bindJvmTypesBuilder() {
		return EntityTypesBuilder.class;
	}

	public Class<? extends org.eclipse.osbp.dsl.common.xtext.extensions.ModelExtensions> bindModelExtensions() {
		return org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions.class;
	}

	public Class<? extends org.eclipse.xtext.linking.ILinker> bindILinker() {
		return EntityLinker.class;
	}

	public Class<? extends org.eclipse.xtext.generator.IGenerator> bindIGenerator() {
		return Generator.class;
	}

//	public Class<? extends IOutputConfigurationProvider> bindIOutputConfigurationProvider() {
//		return OutputConfigurationProvider.class;
//	}

	@Override
	public void configure(Binder binder) {
		super.configure(binder);
		binder.bind(IOutputConfigurationProvider.class).to(DSLOutputConfigurationProvider.class).in(Singleton.class);
	}

	public void configureITransientValueService(Binder binder) {
		binder.bind(ITransientValueService.class).to(EntityGrammarTransientValueService.class);
	}

	public Class<? extends org.eclipse.xtext.resource.XtextResource> bindXtextResource() {
		return SemanticLoadingResource.class;
	}

	public Class<? extends JvmTypeAwareLinkingHelper> bindJvmLinkingHelper() {
		return EntityJvmLinkingHelper.class;
	}

	public Class<? extends IValueConverterService> bindIValueConverterService() {
		return EntityValueConverterService.class;
	}

	public Class<? extends IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
		return EntityResourceDescriptionStrategy.class;
	}

	public Class<? extends ILinkingService> bindILinkingService() {
		return OXTypeLinkingService.class;
	}

	public Class<? extends org.eclipse.xtext.resource.IDerivedStateComputer> bindIDerivedStateComputer() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends IExtendedModelAssociator> bindIIndexModelAssociator() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends IJvmModelAssociator> bindIJvmModelAssociator() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends IJvmModelAssociations> bindIJvmModelAssociations() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends RewritableImportSection.Factory> bindRewritableImportSection$Factory() {
		return OXTypeRewritableImportSection.Factory.class;
	}

	public Class<? extends IScopingInfoProvider> bindIScopingInfoProvider() {
		return ScopingInfoProvider.class;
	}

	public Class<? extends IShouldImportProvider> bindIShouldImportProvider() {
		return ShouldImportProvider.class;
	}

}
