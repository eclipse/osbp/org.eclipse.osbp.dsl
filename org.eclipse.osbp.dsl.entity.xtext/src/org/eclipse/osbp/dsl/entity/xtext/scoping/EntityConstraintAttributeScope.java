/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.scoping;

import java.util.ArrayList;

import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;

public class EntityConstraintAttributeScope extends AbstractScope {
	private final LReference prop;

	public EntityConstraintAttributeScope(LReference reference) {
		super(IScope.NULLSCOPE, true);
		this.prop = reference;
	}

	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {
		ArrayList<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		if (getType() != null && getType() instanceof LClass) {
			LType propClass = getType();
			if (propClass instanceof LBean) {
				LBean bean = (LBean) propClass;
				for (LBeanAttribute property : bean.getAllAttributes()) {
					String name = property.getName();
					if (name != null) {
						result.add(new EObjectDescription(QualifiedName
								.create(name), property, null));
					}
				}
			} else if (propClass instanceof LEntity) {
				LEntity entity = (LEntity) propClass;
				for (LEntityAttribute property : entity.getAllAttributes()) {
					String name = property.getName();
					if (name != null) {
						result.add(new EObjectDescription(QualifiedName
								.create(name), property, null));
					}
				}
			}
		}
		return result;
	}

	private LType getType() {
		if (prop instanceof LBeanReference) {
			return ((LBeanReference) prop).getType();
		} else if (prop instanceof LEntityReference) {
			return ((LEntityReference) prop).getType();
		}
		return null;
	}
}