/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.scoping;

import java.util.ArrayList;

import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;

public class KanbanStateDetailsScope extends AbstractScope {
	private LEntity entity;

	public KanbanStateDetailsScope(LEntity entity) {
		super(IScope.NULLSCOPE, true);
		this.entity = entity;
	}

	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {

		LEntityAttribute stateAttribute = entity.getAllAttributes().stream().filter(e -> e.isAsKanbanState())
				.findFirst().orElseGet(null);

		ArrayList<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		if (stateAttribute != null && stateAttribute.getType() instanceof LEnum) {
			LEnum lEnum = (LEnum) stateAttribute.getType();
			for (LEnumLiteral literal : lEnum.getLiterals()) {
				result.add(new EObjectDescription(QualifiedName.create(literal.getName()), literal, null));
			}
		}
		return result;
	}
}