/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.scoping;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;

public class FeaturesForSuperIndexScope extends AbstractScope {

	private final LEntity lEntity;

	public FeaturesForSuperIndexScope(final LEntitySuperIndex context) {
		super(IScope.NULLSCOPE, true);
		this.lEntity = (LEntity) context.eContainer();
	}

	@SuppressWarnings("restriction")
	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {
		ModelExtensions ext = new ModelExtensions();
		List<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		for (LEntityFeature feature : lEntity.getAllFeatures()) {
			if (ext.isToMany(feature) || feature.getName() == null) {
				continue;
			}
			if (feature instanceof LEntityAttribute || feature instanceof LEntityReference
					|| feature instanceof LBeanAttribute || feature instanceof LBeanReference) {
				result.add(new EObjectDescription(QualifiedName.create(feature.getName()), feature, null));
			}
		}

		return result;
	}
}