/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.scoping;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;

public class DatatypesScope extends AbstractScope {

	// private final IScope parent;
	private final LAttribute context;
	@SuppressWarnings("unused")
	private final EReference reference;

	public DatatypesScope(IScope parent, final LAttribute context,
			EReference reference) {
		super(parent, false);
		// this.parent = parent;
		this.context = context;
		this.reference = reference;
	}

	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {
		// Resource resource = context.eResource();
		List<IEObjectDescription> result = new ArrayList<IEObjectDescription>();

		LTypedPackage lPkg = getPackage(context);

		for (LType lType : lPkg.getTypes()) {
			if (lType instanceof LDataType) {
				result.add(EObjectDescription.create(lType.getName(), lType));
			}
		}

		return result;
	}

	private LTypedPackage getPackage(EObject context) {
		if (context instanceof LTypedPackage) {
			return (LTypedPackage) context;
		}

		return getPackage(context.eContainer());
	}

}