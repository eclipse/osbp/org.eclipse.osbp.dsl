/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.extensions

import com.google.inject.Inject
import org.eclipse.osbp.dsl.entity.xtext.util.PersistenceNamingUtils
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference
import org.eclipse.osbp.dsl.semantic.entity.LEntity
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute
import org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass

class NamingExtensions extends org.eclipse.osbp.dsl.common.xtext.extensions.NamingExtensions {

	@Inject extension ModelExtensions;

	// ### Might move to PersistenceExtensions
	def dispatch toColumnName(LAttribute prop) {
		var columnBaseName = prop.name
		if (columnBaseName.nullOrEmpty) {
			columnBaseName = PersistenceNamingUtils::camelCaseToUpperCase(prop.toName)
		}

		// Compute the final column name using some settings. 
		// E.g. to add some prefix like the shortName of the Entity.
		// ### not yet implemented
		columnBaseName
	}

	// ### Might move to PersistenceExtensions
	def dispatch toColumnName(LStateClass prop) {
		var columnBaseName = PersistenceNamingUtils::camelCaseToUpperCase(prop.name)
		// Compute the final column name using some settings. 
		// E.g. to add some prefix like the shortName of the Entity.
		// ### not yet implemented
		columnBaseName
	}

	// ### Might move to PersistenceExtensions
	def dispatch toColumnName(LEntityAttribute prop) {
		var columnBaseName = prop.persistenceInfo?.columnName
		if (columnBaseName.nullOrEmpty) {
			columnBaseName = PersistenceNamingUtils::camelCaseToUpperCase(prop.toName)
		} else {
			columnBaseName = PersistenceNamingUtils::camelCaseToUpperCase(columnBaseName.replaceCaret)
		}

		// Compute the final column name using some settings. 
		// E.g. to add some prefix like the shortName of the Entity.
		// ### not yet implemented
		columnBaseName
	}

	// ### Might move to PersistenceExtensions
	def dispatch toColumnName(LEntityReference prop) {
		var columnBaseName = prop.persistenceInfo?.columnName
		if (columnBaseName.nullOrEmpty) {
			columnBaseName = PersistenceNamingUtils::camelCaseToUpperCase(prop.toName) + "_ID"
		} else {
			columnBaseName = PersistenceNamingUtils::camelCaseToUpperCase(columnBaseName.replaceCaret)
		}

		// Compute the final column name using some settings. 
		// E.g. to add some prefix like the shortName of the Entity.
		// ### not yet implemented
		columnBaseName
	}

	// ### Might move to PersistenceExtensions
	def dispatch toColumnName(LBeanReference prop) {
		var columnBaseName = prop.name
		if (columnBaseName.nullOrEmpty) {
			columnBaseName = PersistenceNamingUtils::camelCaseToUpperCase(prop.toName)
		}

		// Compute the final column name using some settings. 
		// E.g. to add some prefix like the shortName of the Entity.
		// ### not yet implemented
		columnBaseName
	}

	def toTableName(LEntity entity) {
		entity.toTableName(false)
	}
	
	def toTableName(LEntity entity, boolean toLowerCase) {
		var tableBaseName = entity.persistenceInfo?.tableName
		if (tableBaseName.nullOrEmpty) {
			tableBaseName = if (toLowerCase) PersistenceNamingUtils::camelCaseToLowerCase(entity.toName) else PersistenceNamingUtils::camelCaseToUpperCase(entity.toName)
		} else {
			tableBaseName = if (toLowerCase) PersistenceNamingUtils::camelCaseToLowerCase(tableBaseName.replaceCaret) else PersistenceNamingUtils::camelCaseToUpperCase(tableBaseName.replaceCaret)
		}

		// Compute the final column name using some settings. 
		// E.g. to add some prefix like the shortName of the Entity.
		// ### not yet implemented
		tableBaseName
	}

	def toSchemaName(LEntity entity) {
		entity.persistenceInfo.toSchemaName
	}

	def toSchemaName(LEntityPersistenceInfo info) {
		var schemaName = info.schemaName
		if (!schemaName.nullOrEmpty) {
			schemaName = PersistenceNamingUtils::camelCaseToUpperCase(schemaName.replaceCaret)
		}

		schemaName
	}
}
