/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.scoping;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LBeanFeature;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex;
import org.eclipse.osbp.dsl.semantic.entity.LNestedFeature;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;

public class NestedFeaturesForSuperIndexScope extends AbstractScope {

	private final LNestedFeature context;

	public NestedFeaturesForSuperIndexScope(final LNestedFeature context) {
		super(IScope.NULLSCOPE, true);
		this.context = context;
	}

	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {

		EObject container = context.eContainer();
		if (container instanceof LNestedFeature) {

			LNestedFeature nested = (LNestedFeature) container;
			LFeature feature = nested.getFeature();
			if (feature instanceof LEntityReference) {
				LEntityReference entityRef = (LEntityReference) feature;
				LEntity target = entityRef.getType();
				return collectForEntities(target);
			} else if (feature instanceof LBeanReference) {
				LBeanReference beanRef = (LBeanReference) feature;
				LType type = beanRef.getType();
				if (type instanceof LEntity) {
					return collectForEntities((LEntity) type);
				} else if (type instanceof LBean) {
					return collectForBeans((LBean) type);
				}
			} else if (feature instanceof LEntityAttribute) {
				LEntityAttribute entityAtt = (LEntityAttribute) feature;
				if (entityAtt.getType() instanceof LBean) {
					return collectForBeans((LBean) entityAtt.getType());
				}
			}
		} else if (container instanceof LEntitySuperIndex) {
			LEntity entity = (LEntity) container.eContainer();
			return collectForEntities(entity);
		}

		return IScope.NULLSCOPE.getAllElements();
	}

	@SuppressWarnings("restriction")
	private Iterable<IEObjectDescription> collectForBeans(LBean bean) {
		ModelExtensions ext = new ModelExtensions();
		List<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		for (LBeanFeature feature : bean.getAllFeatures()) {
			if (ext.isToMany(feature) || feature.getName() == null) {
				continue;
			}
			if (feature instanceof LBeanAttribute || feature instanceof LBeanReference) {
				result.add(new EObjectDescription(QualifiedName.create(feature.getName()), feature, null));
			}
		}
		return result;
	}

	@SuppressWarnings("restriction")
	private List<IEObjectDescription> collectForEntities(LEntity entity) {
		ModelExtensions ext = new ModelExtensions();
		List<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		for (LEntityFeature feature : entity.getAllFeatures()) {
			if (ext.isToMany(feature) || feature.getName() == null) {
				continue;
			}
			if (feature instanceof LEntityAttribute || feature instanceof LEntityReference) {
				result.add(new EObjectDescription(QualifiedName.create(feature.getName()), feature, null));
			}
		}
		return result;
	}
}