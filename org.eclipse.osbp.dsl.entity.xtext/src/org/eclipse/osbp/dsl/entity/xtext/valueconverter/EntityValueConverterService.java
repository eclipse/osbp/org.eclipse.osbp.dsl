/**
 * Copyright (c) 2011, 2015 - Florian Pirchner,  Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.valueconverter;

import org.eclipse.osbp.dsl.common.xtext.valueconverter.CommonValueConverterService;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.impl.KeywordAlternativeConverter;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Adds a value conversion for the QualifiedNameWithWildCard rule.
 */
@SuppressWarnings("restriction")
@Singleton
public class EntityValueConverterService extends CommonValueConverterService {

	
	@ValueConverter(rule = "QualifiedNameWithWildCard")
	public IValueConverter<String> getQualifiedNameWithWildCard() {
		return getQualifiedNameValueConverter();
	}

}
