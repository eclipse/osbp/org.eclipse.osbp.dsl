/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.linker;

import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.xtext.oxtype.linking.JvmTypeAwareLinkingHelper;

import com.google.inject.Inject;

public class EntityJvmLinkingHelper extends JvmTypeAwareLinkingHelper {

	@Inject
	private ModelExtensions extensions;

	public EntityJvmLinkingHelper() {
		// register(OSBPEntityPackage.Literals.LENTITY__SUPER_TYPE,
		// OSBPEntityPackage.Literals.LENTITY__SUPER_TYPE_JVM);
		// register(OSBPEntityPackage.Literals.LENTITY_REFERENCE__TYPE,
		// OSBPEntityPackage.Literals.LENTITY_REFERENCE__TYPE_JVM);
		// register(OSBPTypesPackage.Literals.LATTRIBUTE__TYPE,
		// OSBPTypesPackage.Literals.LATTRIBUTE__TYPE_JVM,
		// new IJvmLinkCrossRefStringEnhancer() {
		// @Override
		// public String enhance(EObject context, EStructuralFeature feature,
		// String crossRefString) {
		// LAttribute lAtt = (LAttribute) context.eContainer();
		// if (lAtt.eIsProxy()) {
		// return crossRefString;
		// }
		// LScalarType type = lAtt.getType();
		// if (type instanceof LDataType) {
		// LDataType datatype = (LDataType) type;
		// JvmTypeReference ref = extensions.toTypeReference(datatype);
		// return ref != null ? ref.getSimpleName() : crossRefString;
		// }else if(type instanceof LEnum) {
		// LEnum lEnum = (LEnum) type;
		// return lEnum.getName();
		// }
		// return crossRefString;
		// }
		// }, null);
		// register(OSBPEntityPackage.Literals.LBEAN__SUPER_TYPE,
		// OSBPEntityPackage.Literals.LBEAN__SUPER_TYPE_JVM);
		// register(OSBPEntityPackage.Literals.LBEAN_REFERENCE__TYPE,
		// OSBPEntityPackage.Literals.LBEAN_REFERENCE__TYPE_JVM);
	}

}
