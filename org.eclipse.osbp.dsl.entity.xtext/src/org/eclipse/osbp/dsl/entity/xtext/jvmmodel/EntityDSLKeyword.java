package org.eclipse.osbp.dsl.entity.xtext.jvmmodel;

import java.util.HashMap;

public enum EntityDSLKeyword {

KEY1("asKanbanOrdering"), KEY2("INHERIT"), KEY3("def"), KEY4("tableName"), KEY5("minNumber"), KEY6("protected"), KEY7("asBlob"), KEY8("else"), KEY9("id"), KEY10("catch"), KEY11("persistenceUnit"), KEY12("if"), KEY13("domainKey"), KEY14("msgCode"), KEY15("case"), KEY16("isNotNull"), KEY17("val"), KEY18("!"), KEY19("#"), KEY20("%"), KEY21("var"), KEY22("&"), KEY23("("), KEY24(")"), KEY25("index"), KEY26("opposite"), KEY27("readOnly"), KEY28("*"), KEY29("+"), KEY30(","), KEY31("version"), KEY32("-"), KEY33("decentOrder"), KEY34("."), KEY35("enum"), KEY36("/"), KEY37("0"), KEY38("1"), KEY39("regex"), KEY40("as"), KEY41("transient"), KEY42("inheritancePerSubclass"), KEY43("unique"), KEY44("notnull"), KEY45("cachable"), KEY46(":"), KEY47(";"), KEY48("<"), KEY49("!="), KEY50("="), KEY51(">"), KEY52("?"), KEY53("@"), KEY54("**"), KEY55("::"), KEY56("extension"), KEY57("discriminatorColumn"), KEY58("lazy"), KEY59("range"), KEY60("superIndex"), KEY61("CHAR"), KEY62("default"), KEY63("*="), KEY64("datatype"), KEY65("domainDescription"), KEY66("filterDepth"), KEY67("["), KEY68("]"), KEY69("key"), KEY70("typeof"), KEY71("timestamp"), KEY72("++"), KEY73("forNull"), KEY74("asGrid"), KEY75("abstract"), KEY76("extraStyle"), KEY77("asPrimitive"), KEY78("isFuture"), KEY79("filter"), KEY80("isTrue"), KEY81("+="), KEY82("onKanbanCard"), KEY83("minMaxSize"), KEY84("throw"), KEY85("isNull"), KEY86("onTab"), KEY87("STRING"), KEY88("digits"), KEY89("{"), KEY90("..<"), KEY91("|"), KEY92("}"), KEY93("entity"), KEY94("cascadeRefresh"), KEY95("return"), KEY96("inheritancePerClass"), KEY97("date"), KEY98("||"), KEY99("!=="), KEY100("synchronized"), KEY101("<="), KEY102("<>"), KEY103("msgI18nKey"), KEY104("do"), KEY105("schemaName"), KEY106("while"), KEY107("uuid"), KEY108("cascadeMergePersist"), KEY109("ref"), KEY110("asKanbanState"), KEY111("TIMESTAMP"), KEY112("==="), KEY113("bean"), KEY114("group"), KEY115("info"), KEY116("=="), KEY117("--"), KEY118("settings"), KEY119("=>"), KEY120("new"), KEY121("minDecimal"), KEY122("package"), KEY123("jvmType"), KEY124("static"), KEY125("finally"), KEY126("maxDecimal"), KEY127("timedependent"), KEY128("isFalse"), KEY129("collection"), KEY130("-="), KEY131("->"), KEY132("DATE"), KEY133("warn"), KEY134("extends"), KEY135("refers"), KEY136("null"), KEY137("final"), KEY138("%="), KEY139("true"), KEY140("try"), KEY141(">="), KEY142(".."), KEY143("&&"), KEY144("private"), KEY145("hidden"), KEY146("import"), KEY147("ns"), KEY148("for"), KEY149("error"), KEY150("asDefault"), KEY151("INT"), KEY152("states"), KEY153("?."), KEY154("switch"), KEY155("sideKick"), KEY156("mappedSuperclass"), KEY157("dateType"), KEY158("maxNumber"), KEY159("public"), KEY160("historized"), KEY161("cacheable"), KEY162("?:"), KEY163("derived"), KEY164("value"), KEY165("dto"), KEY166("cascadeRemove"), KEY167("severity"), KEY168("discriminatorType"), KEY169("false"), KEY170("/="), KEY171("instanceof"), KEY172("super"), KEY173("discriminatorValue"), KEY174("mapto"), KEY175("asTable"), KEY176("isPast"), KEY177("time"), KEY178("properties");

private static HashMap<String, EntityDSLKeyword> keywords = new HashMap<>();

static {
for(EntityDSLKeyword keyword : values()){
keywords.put(keyword.getName(), keyword);
}
}

private String name;

private EntityDSLKeyword(String name){
	this.name = name;
}

public String getName() {
	return name;
}

public static boolean isKeyword(String keyword){
if(keyword != null && !keyword.isEmpty()){
	return keywords.containsKey(keyword);
}
return false;
}

}