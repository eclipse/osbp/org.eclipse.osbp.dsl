package org.eclipse.osbp.dsl.entity.xtext.jvmmodel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import org.eclipse.osbp.dsl.entity.xtext.EntityGrammarStandaloneSetup;
import org.eclipse.osbp.dsl.entity.xtext.services.EntityGrammarGrammarAccess;
import org.eclipse.xtext.GrammarUtil;

public abstract class EntityDSLKeywordCreator {

	private static final String DIRECTORY = "src/org/eclipse/osbp/dsl/entity/xtext/jvmmodel";
	private static final String FILENAME = "EntityDSLKeyword.java";

	public static void createOrUpdateKeywordList() {
		String head = 
		"package org.eclipse.osbp.dsl.entity.xtext.jvmmodel;\n\nimport java.util.HashMap;\n\n"
		+"public enum EntityDSLKeyword {\n\n";

		String tail = 
		";\n\nprivate static HashMap<String, EntityDSLKeyword> keywords = new HashMap<>();\n\n"
		+ "static {\n"
		+ "for(EntityDSLKeyword keyword : values()){\n"
		+"keywords.put(keyword.getName(), keyword);\n}\n}\n\n"
		+ "private String name;\n\n"
		+ "private EntityDSLKeyword(String name){\n\t"
		+ "this.name = name;\n}\n\n"
		+ "public String getName() {\n\t"
		+ "return name;\n}"
		+"\n\npublic static boolean isKeyword(String keyword){\n"
		+"if(keyword != null && !keyword.isEmpty()){\n"
		+"	return keywords.containsKey(keyword);\n"
		+"}\n"
		+"return false;\n}\n\n}";
		
		File file = Paths.get(DIRECTORY).resolve(FILENAME).toFile(); 
		try (FileWriter out = new FileWriter(file)) {
			Set<String> entityDSLkeywords = GrammarUtil.getAllKeywords(EntityGrammarStandaloneSetup.doSetup().getInstance(EntityGrammarGrammarAccess.class).getGrammar());
			if( !file.exists() ){
				file = Files.createFile(Paths.get(DIRECTORY).resolve(FILENAME)).toFile();
			}
			if ( file.exists() && entityDSLkeywords != null ) {
				String result = "";
				int count = 1;
				for(String value: entityDSLkeywords){
					result += "KEY"+ count++ +"(\""+value+"\"), ";
				}
				out.write(head.concat(result.substring(0, result.length()-2).concat(tail)));
			}
		} catch (IOException e) {
			System.err.println(e);
		}
	}
	
	public static void main(String[] args) {
		createOrUpdateKeywordList();
	}
}
