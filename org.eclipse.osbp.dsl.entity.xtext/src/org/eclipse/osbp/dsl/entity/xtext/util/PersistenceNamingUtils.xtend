/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.dsl.entity.xtext.util

class PersistenceNamingUtils {

	/**
	 * Converts the given <code>String</code>, that contains camel-case
	 * characters, to uppercase, thereby inserting underline ("_") characters
	 * before uppercase characters. <br>
	 * E.g.: "camelCaseToUpperCase" -&gt; "CAMEL_CASE_TO_UPPER_CASE".
	 * 
	 * @param pCamelCaseString
	 *            The <code>String</code> to convert.
	 * @return The converted <code>String</code>.
	 */
	def static String camelCaseToUpperCase(String pCamelCaseString) {
		if (pCamelCaseString === null) {
			return "";
		}
		if (pCamelCaseString.equals(pCamelCaseString.toUpperCase())) {
			return pCamelCaseString; // Nothing to do.
		}
		val sbOut = pCamelCaseString.stringBuilderOut
		return sbOut.toString().toUpperCase();
	}
	
	/**
	 * Converts the given <code>String</code>, that contains camel-case
	 * characters, to lower case, thereby inserting underline ("_") characters
	 * before lower case characters. <br>
	 * E.g.: "camelCaseToLowerCase" -&gt; "camel_case_to_lower_case".
	 * 
	 * @param pCamelCaseString
	 *            The <code>String</code> to convert.
	 * @return The converted <code>String</code>.
	 */
	def static String camelCaseToLowerCase(String pCamelCaseString) {
		if (pCamelCaseString === null) {
			return "";
		}
		if (pCamelCaseString.equals(pCamelCaseString.toLowerCase())) {
			return pCamelCaseString; // Nothing to do.
		}
		val sbOut = pCamelCaseString.stringBuilderOut
		return sbOut.toString().toLowerCase();
	}
	
	def static private getStringBuilderOut(String pCamelCaseString){
				var StringBuilder sb = null
		if(pCamelCaseString.charAt(0) == '^') {
			sb = new StringBuilder(pCamelCaseString.substring(1));
		}
		sb = new StringBuilder(pCamelCaseString);
		val StringBuilder sbOut = new StringBuilder(pCamelCaseString);
		val maxIndex = sb.length - 1;
		var offset = 0;
		for (i : 1 .. (sb.length() - 1)) {
			try {
				if (i < maxIndex && sb.charAt(i - 1) != '_' && Character.isUpperCase(sb.charAt(i)) &&
					!Character.isUpperCase(sb.charAt(i + 1))) {
					sbOut.insert(i + offset, '_');
					offset = offset + 1;
				}
			} catch (Exception ex) {
				println(ex)
			}
		}
		return sbOut
	}

}
