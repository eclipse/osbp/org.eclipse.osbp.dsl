/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.scoping;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.dsl.common.xtext.scoping.ConstraintEnumMatchingLiteralScope;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LResultFilters;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex;
import org.eclipse.osbp.dsl.semantic.entity.LIndex;
import org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo;
import org.eclipse.osbp.dsl.semantic.entity.LKanbanStateDetail;
import org.eclipse.osbp.dsl.semantic.entity.LNestedFeature;
import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage;
import org.eclipse.xtext.scoping.IScope;

@SuppressWarnings("restriction")
public class EntityGrammarScopeProvider extends AbstractEntityGrammarScopeProvider {
	@Override
	public IScope getScope(final EObject context, EReference reference) {
		if (reference == OSBPEntityPackage.Literals.LENTITY_REFERENCE__OPPOSITE) {
			return new EntityRefOppositeScope((LEntityReference) context);
		} else if (reference == OSBPEntityPackage.Literals.LENTITY_SUPER_INDEX__ATTRIBUTES) {
			return new FeaturesForSuperIndexScope((LEntitySuperIndex) context);
		} else if (reference == OSBPEntityPackage.Literals.LNESTED_FEATURE__FEATURE) {
			return new NestedFeaturesForSuperIndexScope((LNestedFeature) context);
		} else if (reference == OSBPEntityPackage.Literals.LENTITY_ATTRIBUTE__OPPOSITE) {
			return new EntityAttOppositeScope((LEntityAttribute) context);
		} else if (reference == OSBPEntityPackage.Literals.LBEAN_REFERENCE__OPPOSITE) {
			return new BeanRefOppositeScope((LBeanReference) context);
		} else if (reference == OSBPTypesPackage.Literals.LATTRIBUTE__TYPE) {
			return new DatatypesScope(super.getScope(context, reference), (LAttribute) context, reference);
		} else if (reference == OSBPEntityPackage.Literals.LINDEX__FEATURES) {
			return new IndexScope((LIndex) context);
		}else if (reference == OSBPEntityPackage.Literals.LKANBAN_ENUM_INFO__LITERAL) {
			EObject _context = context;
			if(_context instanceof LKanbanEnumInfo){
				_context = context.eContainer();
			}
			
			if(_context instanceof LKanbanStateDetail){
				_context = _context.eContainer();
			}
			
			return new KanbanStateDetailsScope((LEntity)_context.eContainer());
		} else if (reference == OSBPTypesPackage.Literals.LATTRIBUTE_MATCHING_CONSTRAINT__ATTRIBUTE) {
			if (context instanceof LResultFilters) {
				return new EntityConstraintAttributeScope((LReference) context.eContainer());
			} else {
				return new EntityConstraintAttributeScope((LReference) context.eContainer().eContainer());
			}
		} else if (reference == OSBPTypesPackage.Literals.LATTRIBUTE_MATCHING_CONSTRAINT__MATCHING_LITERAL) {
			return new ConstraintEnumMatchingLiteralScope((LAttributeMatchingConstraint) context);
		}
		return super.getScope(context, reference);
	}
}
