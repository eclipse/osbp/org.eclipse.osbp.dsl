/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityFactory
 * @generated
 */
public interface OSBPEntityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "entity";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/dsl/entity/v1";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "entity";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OSBPEntityPackage eINSTANCE = org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityModelImpl <em>LEntity Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityModelImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityModel()
	 * @generated
	 */
	int LENTITY_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_MODEL__IMPORT_SECTION = 0;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_MODEL__PACKAGES = 1;

	/**
	 * The number of structural features of the '<em>LEntity Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>LEntity Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanImpl <em>LBean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LBeanImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLBean()
	 * @generated
	 */
	int LBEAN = 1;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__ANNOTATIONS = OSBPTypesPackage.LCLASS__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__NAME = OSBPTypesPackage.LCLASS__NAME;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__ANNOTATION_INFO = OSBPTypesPackage.LCLASS__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__ABSTRACT = OSBPTypesPackage.LCLASS__ABSTRACT;

	/**
	 * The feature id for the '<em><b>Serializable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__SERIALIZABLE = OSBPTypesPackage.LCLASS__SERIALIZABLE;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__SHORT_NAME = OSBPTypesPackage.LCLASS__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__FEATURES = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Super Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__SUPER_TYPE = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sub Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__SUB_TYPES = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Bean On Tab</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN__BEAN_ON_TAB = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>LBean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE_COUNT = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LCLASS___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___GET_RESOLVED_ANNOTATIONS = OSBPTypesPackage.LCLASS___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Is Normal Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___IS_NORMAL_ATTRIBUTE__LFEATURE = OSBPTypesPackage.LCLASS___IS_NORMAL_ATTRIBUTE__LFEATURE;

	/**
	 * The operation id for the '<em>Is Hist Current Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___IS_HIST_CURRENT_ATTRIBUTE__LFEATURE = OSBPTypesPackage.LCLASS___IS_HIST_CURRENT_ATTRIBUTE__LFEATURE;

	/**
	 * The operation id for the '<em>Get Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___GET_FEATURES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___GET_OPERATIONS = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get References</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___GET_REFERENCES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___GET_ATTRIBUTES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get All Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___GET_ALL_FEATURES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Get All Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___GET_ALL_ATTRIBUTES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Get All References</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___GET_ALL_REFERENCES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Collect All OSBP Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN___COLLECT_ALL_OSBP_FEATURES__LBEAN_LIST = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 8;

	/**
	 * The number of operations of the '<em>LBean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_OPERATION_COUNT = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 9;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityImpl <em>LEntity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntity()
	 * @generated
	 */
	int LENTITY = 2;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__ANNOTATIONS = OSBPTypesPackage.LCLASS__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__NAME = OSBPTypesPackage.LCLASS__NAME;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__ANNOTATION_INFO = OSBPTypesPackage.LCLASS__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__ABSTRACT = OSBPTypesPackage.LCLASS__ABSTRACT;

	/**
	 * The feature id for the '<em><b>Serializable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__SERIALIZABLE = OSBPTypesPackage.LCLASS__SERIALIZABLE;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__SHORT_NAME = OSBPTypesPackage.LCLASS__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Cacheable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__CACHEABLE = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Historized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__HISTORIZED = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Timedependent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__TIMEDEPENDENT = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Timedependent Date Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__TIMEDEPENDENT_DATE_TYPE = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Mapped Superclass</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__MAPPED_SUPERCLASS = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Persistence Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__PERSISTENCE_INFO = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Inheritance Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__INHERITANCE_STRATEGY = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__FEATURES = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Indexes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__INDEXES = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Super Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__SUPER_TYPE = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Sub Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__SUB_TYPES = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Persistence Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__PERSISTENCE_UNIT = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Super Index</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__SUPER_INDEX = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>State Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY__STATE_CLASS = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>LEntity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE_COUNT = OSBPTypesPackage.LCLASS_FEATURE_COUNT + 14;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LCLASS___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_RESOLVED_ANNOTATIONS = OSBPTypesPackage.LCLASS___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Is Normal Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___IS_NORMAL_ATTRIBUTE__LFEATURE = OSBPTypesPackage.LCLASS___IS_NORMAL_ATTRIBUTE__LFEATURE;

	/**
	 * The operation id for the '<em>Is Hist Current Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___IS_HIST_CURRENT_ATTRIBUTE__LFEATURE = OSBPTypesPackage.LCLASS___IS_HIST_CURRENT_ATTRIBUTE__LFEATURE;

	/**
	 * The operation id for the '<em>Get Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_FEATURES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_OPERATIONS = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get References</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_REFERENCES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_ATTRIBUTES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get All Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_ALL_FEATURES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Get All Attributes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_ALL_ATTRIBUTES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Get All References</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_ALL_REFERENCES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Collect All OSBP Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___COLLECT_ALL_OSBP_FEATURES__LENTITY_LIST = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Collect Normal OSBP Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___COLLECT_NORMAL_OSBP_FEATURES__LENTITY_LIST = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Get Primary Key Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_PRIMARY_KEY_ATTRIBUTE = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Get Id Attribute Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_ID_ATTRIBUTE_NAME = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>Get Id Attribute Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_ID_ATTRIBUTE_TYPE = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>Get Current Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_CURRENT_ATTRIBUTE = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 13;

	/**
	 * The operation id for the '<em>Get Version Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_VERSION_ATTRIBUTE = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 14;

	/**
	 * The operation id for the '<em>Get Version Attribute Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_VERSION_ATTRIBUTE_NAME = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 15;

	/**
	 * The operation id for the '<em>Get Version Attribute Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_VERSION_ATTRIBUTE_TYPE = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 16;

	/**
	 * The operation id for the '<em>Get Normal Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___GET_NORMAL_FEATURES = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 17;

	/**
	 * The operation id for the '<em>Is Historized With Parent</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___IS_HISTORIZED_WITH_PARENT = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 18;

	/**
	 * The operation id for the '<em>Is Timedependent With Parent</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___IS_TIMEDEPENDENT_WITH_PARENT = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 19;

	/**
	 * The operation id for the '<em>Is Historized Or Timedependent With Parent</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY___IS_HISTORIZED_OR_TIMEDEPENDENT_WITH_PARENT = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 20;

	/**
	 * The number of operations of the '<em>LEntity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_OPERATION_COUNT = OSBPTypesPackage.LCLASS_OPERATION_COUNT + 21;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntitySuperIndexImpl <em>LEntity Super Index</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntitySuperIndexImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntitySuperIndex()
	 * @generated
	 */
	int LENTITY_SUPER_INDEX = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_SUPER_INDEX__NAME = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_SUPER_INDEX__ATTRIBUTES = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LEntity Super Index</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_SUPER_INDEX_FEATURE_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_SUPER_INDEX___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LEntity Super Index</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_SUPER_INDEX_OPERATION_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LNestedFeatureImpl <em>LNested Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LNestedFeatureImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLNestedFeature()
	 * @generated
	 */
	int LNESTED_FEATURE = 4;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNESTED_FEATURE__FEATURE = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tail</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNESTED_FEATURE__TAIL = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LNested Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNESTED_FEATURE_FEATURE_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNESTED_FEATURE___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LNested Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNESTED_FEATURE_OPERATION_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityPersistenceInfoImpl <em>LEntity Persistence Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityPersistenceInfoImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityPersistenceInfo()
	 * @generated
	 */
	int LENTITY_PERSISTENCE_INFO = 5;

	/**
	 * The feature id for the '<em><b>Schema Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_PERSISTENCE_INFO__SCHEMA_NAME = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Table Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_PERSISTENCE_INFO__TABLE_NAME = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LEntity Persistence Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_PERSISTENCE_INFO_FEATURE_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_PERSISTENCE_INFO___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LEntity Persistence Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_PERSISTENCE_INFO_OPERATION_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityColumnPersistenceInfoImpl <em>LEntity Column Persistence Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityColumnPersistenceInfoImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityColumnPersistenceInfo()
	 * @generated
	 */
	int LENTITY_COLUMN_PERSISTENCE_INFO = 6;

	/**
	 * The feature id for the '<em><b>Column Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_COLUMN_PERSISTENCE_INFO__COLUMN_NAME = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LEntity Column Persistence Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_COLUMN_PERSISTENCE_INFO_FEATURE_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_COLUMN_PERSISTENCE_INFO___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LEntity Column Persistence Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_COLUMN_PERSISTENCE_INFO_OPERATION_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityInheritanceStrategy <em>LEntity Inheritance Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityInheritanceStrategy
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityInheritanceStrategy()
	 * @generated
	 */
	int LENTITY_INHERITANCE_STRATEGY = 7;

	/**
	 * The number of structural features of the '<em>LEntity Inheritance Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_INHERITANCE_STRATEGY_FEATURE_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_INHERITANCE_STRATEGY___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LEntity Inheritance Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_INHERITANCE_STRATEGY_OPERATION_COUNT = OSBPTypesPackage.LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LTablePerClassStrategyImpl <em>LTable Per Class Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LTablePerClassStrategyImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLTablePerClassStrategy()
	 * @generated
	 */
	int LTABLE_PER_CLASS_STRATEGY = 8;

	/**
	 * The feature id for the '<em><b>Discriminator Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_CLASS_STRATEGY__DISCRIMINATOR_COLUMN = LENTITY_INHERITANCE_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Discriminator Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_CLASS_STRATEGY__DISCRIMINATOR_TYPE = LENTITY_INHERITANCE_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Discriminator Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_CLASS_STRATEGY__DISCRIMINATOR_VALUE = LENTITY_INHERITANCE_STRATEGY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>LTable Per Class Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_CLASS_STRATEGY_FEATURE_COUNT = LENTITY_INHERITANCE_STRATEGY_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_CLASS_STRATEGY___ERESOLVE_PROXY__INTERNALEOBJECT = LENTITY_INHERITANCE_STRATEGY___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LTable Per Class Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_CLASS_STRATEGY_OPERATION_COUNT = LENTITY_INHERITANCE_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LTablePerSubclassStrategyImpl <em>LTable Per Subclass Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LTablePerSubclassStrategyImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLTablePerSubclassStrategy()
	 * @generated
	 */
	int LTABLE_PER_SUBCLASS_STRATEGY = 9;

	/**
	 * The feature id for the '<em><b>Discriminator Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_SUBCLASS_STRATEGY__DISCRIMINATOR_COLUMN = LENTITY_INHERITANCE_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Discriminator Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_SUBCLASS_STRATEGY__DISCRIMINATOR_TYPE = LENTITY_INHERITANCE_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Discriminator Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_SUBCLASS_STRATEGY__DISCRIMINATOR_VALUE = LENTITY_INHERITANCE_STRATEGY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>LTable Per Subclass Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_SUBCLASS_STRATEGY_FEATURE_COUNT = LENTITY_INHERITANCE_STRATEGY_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_SUBCLASS_STRATEGY___ERESOLVE_PROXY__INTERNALEOBJECT = LENTITY_INHERITANCE_STRATEGY___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LTable Per Subclass Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTABLE_PER_SUBCLASS_STRATEGY_OPERATION_COUNT = LENTITY_INHERITANCE_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityFeatureImpl <em>LEntity Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityFeatureImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityFeature()
	 * @generated
	 */
	int LENTITY_FEATURE = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE__ANNOTATIONS = OSBPTypesPackage.LFEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE__NAME = OSBPTypesPackage.LFEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE__MULTIPLICITY = OSBPTypesPackage.LFEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE__ANNOTATION_INFO = OSBPTypesPackage.LFEATURE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Persistence Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE__PERSISTENCE_INFO = OSBPTypesPackage.LFEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LEntity Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE_FEATURE_COUNT = OSBPTypesPackage.LFEATURE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LFEATURE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE___GET_RESOLVED_ANNOTATIONS = OSBPTypesPackage.LFEATURE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Get Entity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE___GET_ENTITY = OSBPTypesPackage.LFEATURE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LEntity Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_FEATURE_OPERATION_COUNT = OSBPTypesPackage.LFEATURE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanStateDetailImpl <em>LKanban State Detail</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanStateDetailImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLKanbanStateDetail()
	 * @generated
	 */
	int LKANBAN_STATE_DETAIL = 11;

	/**
	 * The feature id for the '<em><b>Infos</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKANBAN_STATE_DETAIL__INFOS = 0;

	/**
	 * The number of structural features of the '<em>LKanban State Detail</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKANBAN_STATE_DETAIL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>LKanban State Detail</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKANBAN_STATE_DETAIL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanEnumInfoImpl <em>LKanban Enum Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanEnumInfoImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLKanbanEnumInfo()
	 * @generated
	 */
	int LKANBAN_ENUM_INFO = 12;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKANBAN_ENUM_INFO__LITERAL = 0;

	/**
	 * The feature id for the '<em><b>I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKANBAN_ENUM_INFO__I18N_KEY = 1;

	/**
	 * The number of structural features of the '<em>LKanban Enum Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKANBAN_ENUM_INFO_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>LKanban Enum Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKANBAN_ENUM_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityAttributeImpl <em>LEntity Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityAttributeImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityAttribute()
	 * @generated
	 */
	int LENTITY_ATTRIBUTE = 13;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__ANNOTATIONS = LENTITY_FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__NAME = LENTITY_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__MULTIPLICITY = LENTITY_FEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__ANNOTATION_INFO = LENTITY_FEATURE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Persistence Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__PERSISTENCE_INFO = LENTITY_FEATURE__PERSISTENCE_INFO;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__ID = LENTITY_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__UUID = LENTITY_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__VERSION = LENTITY_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Lazy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__LAZY = LENTITY_FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Transient</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__TRANSIENT = LENTITY_FEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Derived</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__DERIVED = LENTITY_FEATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Dirty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__DIRTY = LENTITY_FEATURE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Domain Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__DOMAIN_KEY = LENTITY_FEATURE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Domain Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__DOMAIN_DESCRIPTION = LENTITY_FEATURE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__FILTERING = LENTITY_FEATURE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Range Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__RANGE_FILTERING = LENTITY_FEATURE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Unique Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__UNIQUE_ENTRY = LENTITY_FEATURE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Attribute Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__ATTRIBUTE_HIDDEN = LENTITY_FEATURE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Attribute Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__ATTRIBUTE_READ_ONLY = LENTITY_FEATURE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Extra Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__EXTRA_STYLE = LENTITY_FEATURE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Derived Getter Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__DERIVED_GETTER_EXPRESSION = LENTITY_FEATURE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__TYPE = LENTITY_FEATURE_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__PROPERTIES = LENTITY_FEATURE_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__CONSTRAINTS = LENTITY_FEATURE_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Is Grouped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__IS_GROUPED = LENTITY_FEATURE_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__GROUP_NAME = LENTITY_FEATURE_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>As Kanban State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__AS_KANBAN_STATE = LENTITY_FEATURE_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>On Kanban Card</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__ON_KANBAN_CARD = LENTITY_FEATURE_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>On Kanban Card States</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__ON_KANBAN_CARD_STATES = LENTITY_FEATURE_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>As Kanban Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__AS_KANBAN_ORDERING = LENTITY_FEATURE_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Decent Kanban Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__DECENT_KANBAN_ORDER = LENTITY_FEATURE_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__OPPOSITE = LENTITY_FEATURE_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>Typed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE__TYPED_NAME = LENTITY_FEATURE_FEATURE_COUNT + 27;

	/**
	 * The number of structural features of the '<em>LEntity Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE_FEATURE_COUNT = LENTITY_FEATURE_FEATURE_COUNT + 28;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE___ERESOLVE_PROXY__INTERNALEOBJECT = LENTITY_FEATURE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE___GET_RESOLVED_ANNOTATIONS = LENTITY_FEATURE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Get Entity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE___GET_ENTITY = LENTITY_FEATURE___GET_ENTITY;

	/**
	 * The number of operations of the '<em>LEntity Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_ATTRIBUTE_OPERATION_COUNT = LENTITY_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl <em>LEntity Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityReference()
	 * @generated
	 */
	int LENTITY_REFERENCE = 14;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__ANNOTATIONS = LENTITY_FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__NAME = LENTITY_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__MULTIPLICITY = LENTITY_FEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__ANNOTATION_INFO = LENTITY_FEATURE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Persistence Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__PERSISTENCE_INFO = LENTITY_FEATURE__PERSISTENCE_INFO;

	/**
	 * The feature id for the '<em><b>Lazy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__LAZY = LENTITY_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cascade Merge Persist</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__CASCADE_MERGE_PERSIST = LENTITY_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cascade Remove</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__CASCADE_REMOVE = LENTITY_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cascade Refresh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__CASCADE_REFRESH = LENTITY_FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__PROPERTIES = LENTITY_FEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__CONSTRAINTS = LENTITY_FEATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Is Grouped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__IS_GROUPED = LENTITY_FEATURE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__GROUP_NAME = LENTITY_FEATURE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>As Grid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__AS_GRID = LENTITY_FEATURE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>As Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__AS_TABLE = LENTITY_FEATURE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Side Kick</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__SIDE_KICK = LENTITY_FEATURE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Reference Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__REFERENCE_HIDDEN = LENTITY_FEATURE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Reference Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__REFERENCE_READ_ONLY = LENTITY_FEATURE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Historized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__HISTORIZED = LENTITY_FEATURE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__TYPE = LENTITY_FEATURE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__OPPOSITE = LENTITY_FEATURE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Result Filters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__RESULT_FILTERS = LENTITY_FEATURE_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Filter Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE__FILTER_DEPTH = LENTITY_FEATURE_FEATURE_COUNT + 17;

	/**
	 * The number of structural features of the '<em>LEntity Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE_FEATURE_COUNT = LENTITY_FEATURE_FEATURE_COUNT + 18;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE___ERESOLVE_PROXY__INTERNALEOBJECT = LENTITY_FEATURE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE___GET_RESOLVED_ANNOTATIONS = LENTITY_FEATURE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Get Entity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE___GET_ENTITY = LENTITY_FEATURE___GET_ENTITY;

	/**
	 * The operation id for the '<em>Is Cascading</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE___IS_CASCADING = LENTITY_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LEntity Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENTITY_REFERENCE_OPERATION_COUNT = LENTITY_FEATURE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanFeatureImpl <em>LBean Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LBeanFeatureImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLBeanFeature()
	 * @generated
	 */
	int LBEAN_FEATURE = 15;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE__ANNOTATIONS = OSBPTypesPackage.LFEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE__NAME = OSBPTypesPackage.LFEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE__MULTIPLICITY = OSBPTypesPackage.LFEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE__ANNOTATION_INFO = OSBPTypesPackage.LFEATURE__ANNOTATION_INFO;

	/**
	 * The number of structural features of the '<em>LBean Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE_FEATURE_COUNT = OSBPTypesPackage.LFEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LFEATURE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE___GET_RESOLVED_ANNOTATIONS = OSBPTypesPackage.LFEATURE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Get Bean</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE___GET_BEAN = OSBPTypesPackage.LFEATURE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LBean Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_FEATURE_OPERATION_COUNT = OSBPTypesPackage.LFEATURE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanAttributeImpl <em>LBean Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LBeanAttributeImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLBeanAttribute()
	 * @generated
	 */
	int LBEAN_ATTRIBUTE = 16;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__ANNOTATIONS = LBEAN_FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__NAME = LBEAN_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__MULTIPLICITY = LBEAN_FEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__ANNOTATION_INFO = LBEAN_FEATURE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__ID = LBEAN_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__UUID = LBEAN_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__VERSION = LBEAN_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Lazy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__LAZY = LBEAN_FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Transient</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__TRANSIENT = LBEAN_FEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Derived</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__DERIVED = LBEAN_FEATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Dirty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__DIRTY = LBEAN_FEATURE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Domain Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__DOMAIN_KEY = LBEAN_FEATURE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Domain Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__DOMAIN_DESCRIPTION = LBEAN_FEATURE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__FILTERING = LBEAN_FEATURE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Range Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__RANGE_FILTERING = LBEAN_FEATURE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Unique Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__UNIQUE_ENTRY = LBEAN_FEATURE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Attribute Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__ATTRIBUTE_HIDDEN = LBEAN_FEATURE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Attribute Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__ATTRIBUTE_READ_ONLY = LBEAN_FEATURE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Extra Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__EXTRA_STYLE = LBEAN_FEATURE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Derived Getter Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__DERIVED_GETTER_EXPRESSION = LBEAN_FEATURE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__TYPE = LBEAN_FEATURE_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__PROPERTIES = LBEAN_FEATURE_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__CONSTRAINTS = LBEAN_FEATURE_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Is Grouped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__IS_GROUPED = LBEAN_FEATURE_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__GROUP_NAME = LBEAN_FEATURE_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Typed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE__TYPED_NAME = LBEAN_FEATURE_FEATURE_COUNT + 21;

	/**
	 * The number of structural features of the '<em>LBean Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE_FEATURE_COUNT = LBEAN_FEATURE_FEATURE_COUNT + 22;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE___ERESOLVE_PROXY__INTERNALEOBJECT = LBEAN_FEATURE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE___GET_RESOLVED_ANNOTATIONS = LBEAN_FEATURE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Get Bean</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE___GET_BEAN = LBEAN_FEATURE___GET_BEAN;

	/**
	 * The number of operations of the '<em>LBean Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_ATTRIBUTE_OPERATION_COUNT = LBEAN_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanReferenceImpl <em>LBean Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LBeanReferenceImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLBeanReference()
	 * @generated
	 */
	int LBEAN_REFERENCE = 17;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__ANNOTATIONS = LBEAN_FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__NAME = LBEAN_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__MULTIPLICITY = LBEAN_FEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__ANNOTATION_INFO = LBEAN_FEATURE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Lazy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__LAZY = LBEAN_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cascade Merge Persist</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__CASCADE_MERGE_PERSIST = LBEAN_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cascade Remove</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__CASCADE_REMOVE = LBEAN_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cascade Refresh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__CASCADE_REFRESH = LBEAN_FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__PROPERTIES = LBEAN_FEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__CONSTRAINTS = LBEAN_FEATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Is Grouped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__IS_GROUPED = LBEAN_FEATURE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__GROUP_NAME = LBEAN_FEATURE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>As Grid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__AS_GRID = LBEAN_FEATURE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>As Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__AS_TABLE = LBEAN_FEATURE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Side Kick</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__SIDE_KICK = LBEAN_FEATURE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Reference Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__REFERENCE_HIDDEN = LBEAN_FEATURE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Reference Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__REFERENCE_READ_ONLY = LBEAN_FEATURE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Historized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__HISTORIZED = LBEAN_FEATURE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__TYPE = LBEAN_FEATURE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__OPPOSITE = LBEAN_FEATURE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Result Filters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE__RESULT_FILTERS = LBEAN_FEATURE_FEATURE_COUNT + 16;

	/**
	 * The number of structural features of the '<em>LBean Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE_FEATURE_COUNT = LBEAN_FEATURE_FEATURE_COUNT + 17;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE___ERESOLVE_PROXY__INTERNALEOBJECT = LBEAN_FEATURE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE___GET_RESOLVED_ANNOTATIONS = LBEAN_FEATURE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Get Bean</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE___GET_BEAN = LBEAN_FEATURE___GET_BEAN;

	/**
	 * The operation id for the '<em>Is Cascading</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE___IS_CASCADING = LBEAN_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LBean Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBEAN_REFERENCE_OPERATION_COUNT = LBEAN_FEATURE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LIndexImpl <em>LIndex</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LIndexImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLIndex()
	 * @generated
	 */
	int LINDEX = 18;

	/**
	 * The feature id for the '<em><b>Unique</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINDEX__UNIQUE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINDEX__NAME = 1;

	/**
	 * The feature id for the '<em><b>Features</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINDEX__FEATURES = 2;

	/**
	 * The number of structural features of the '<em>LIndex</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINDEX_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>LIndex</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINDEX_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LOperationImpl <em>LOperation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LOperationImpl
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLOperation()
	 * @generated
	 */
	int LOPERATION = 19;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__ANNOTATIONS = OSBPTypesPackage.LOPERATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__MODIFIER = OSBPTypesPackage.LOPERATION__MODIFIER;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__TYPE = OSBPTypesPackage.LOPERATION__TYPE;

	/**
	 * The feature id for the '<em><b>Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__PARAMS = OSBPTypesPackage.LOPERATION__PARAMS;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__BODY = OSBPTypesPackage.LOPERATION__BODY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__NAME = OSBPTypesPackage.LOPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__MULTIPLICITY = OSBPTypesPackage.LOPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__ANNOTATION_INFO = OSBPTypesPackage.LOPERATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Persistence Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__PERSISTENCE_INFO = OSBPTypesPackage.LOPERATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>LOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION_FEATURE_COUNT = OSBPTypesPackage.LOPERATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LOPERATION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION___GET_RESOLVED_ANNOTATIONS = OSBPTypesPackage.LOPERATION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Bean</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION___GET_BEAN = OSBPTypesPackage.LOPERATION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Entity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION___GET_ENTITY = OSBPTypesPackage.LOPERATION_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>LOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION_OPERATION_COUNT = OSBPTypesPackage.LOPERATION_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.entity.LDiscriminatorType <em>LDiscriminator Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.entity.LDiscriminatorType
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLDiscriminatorType()
	 * @generated
	 */
	int LDISCRIMINATOR_TYPE = 20;

	/**
	 * The meta object id for the '<em>Internal EObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getInternalEObject()
	 * @generated
	 */
	int INTERNAL_EOBJECT = 21;

	/**
	 * The meta object id for the '<em>Operations List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getOperationsList()
	 * @generated
	 */
	int OPERATIONS_LIST = 22;

	/**
	 * The meta object id for the '<em>Entity Feature List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getEntityFeatureList()
	 * @generated
	 */
	int ENTITY_FEATURE_LIST = 23;

	/**
	 * The meta object id for the '<em>Entity Reference List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getEntityReferenceList()
	 * @generated
	 */
	int ENTITY_REFERENCE_LIST = 24;

	/**
	 * The meta object id for the '<em>Entity Attribute List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getEntityAttributeList()
	 * @generated
	 */
	int ENTITY_ATTRIBUTE_LIST = 25;

	/**
	 * The meta object id for the '<em>Bean Feature List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getBeanFeatureList()
	 * @generated
	 */
	int BEAN_FEATURE_LIST = 26;

	/**
	 * The meta object id for the '<em>Bean Reference List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getBeanReferenceList()
	 * @generated
	 */
	int BEAN_REFERENCE_LIST = 27;

	/**
	 * The meta object id for the '<em>Bean Attribute List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getBeanAttributeList()
	 * @generated
	 */
	int BEAN_ATTRIBUTE_LIST = 28;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityModel <em>LEntity Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEntity Model</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityModel
	 * @generated
	 */
	EClass getLEntityModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityModel#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Section</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityModel#getImportSection()
	 * @see #getLEntityModel()
	 * @generated
	 */
	EReference getLEntityModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityModel#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Packages</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityModel#getPackages()
	 * @see #getLEntityModel()
	 * @generated
	 */
	EReference getLEntityModel_Packages();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LBean <em>LBean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LBean</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean
	 * @generated
	 */
	EClass getLBean();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Features</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getFeatures()
	 * @see #getLBean()
	 * @generated
	 */
	EReference getLBean_Features();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getSuperType <em>Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getSuperType()
	 * @see #getLBean()
	 * @generated
	 */
	EReference getLBean_SuperType();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getSubTypes <em>Sub Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Types</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getSubTypes()
	 * @see #getLBean()
	 * @generated
	 */
	EReference getLBean_SubTypes();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#isBeanOnTab <em>Bean On Tab</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bean On Tab</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#isBeanOnTab()
	 * @see #getLBean()
	 * @generated
	 */
	EAttribute getLBean_BeanOnTab();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getOperations() <em>Get Operations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Operations</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getOperations()
	 * @generated
	 */
	EOperation getLBean__GetOperations();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getReferences() <em>Get References</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get References</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getReferences()
	 * @generated
	 */
	EOperation getLBean__GetReferences();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getAttributes() <em>Get Attributes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Attributes</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getAttributes()
	 * @generated
	 */
	EOperation getLBean__GetAttributes();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getAllFeatures() <em>Get All Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Features</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getAllFeatures()
	 * @generated
	 */
	EOperation getLBean__GetAllFeatures();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getAllAttributes() <em>Get All Attributes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Attributes</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getAllAttributes()
	 * @generated
	 */
	EOperation getLBean__GetAllAttributes();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getAllReferences() <em>Get All References</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All References</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getAllReferences()
	 * @generated
	 */
	EOperation getLBean__GetAllReferences();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#collectAllOSBPFeatures(org.eclipse.osbp.dsl.semantic.entity.LBean, java.util.List) <em>Collect All OSBP Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Collect All OSBP Features</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#collectAllOSBPFeatures(org.eclipse.osbp.dsl.semantic.entity.LBean, java.util.List)
	 * @generated
	 */
	EOperation getLBean__CollectAllOSBPFeatures__LBean_List();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity <em>LEntity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEntity</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity
	 * @generated
	 */
	EClass getLEntity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isCacheable <em>Cacheable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cacheable</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#isCacheable()
	 * @see #getLEntity()
	 * @generated
	 */
	EAttribute getLEntity_Cacheable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isHistorized <em>Historized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Historized</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#isHistorized()
	 * @see #getLEntity()
	 * @generated
	 */
	EAttribute getLEntity_Historized();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isTimedependent <em>Timedependent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timedependent</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#isTimedependent()
	 * @see #getLEntity()
	 * @generated
	 */
	EAttribute getLEntity_Timedependent();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getTimedependentDateType <em>Timedependent Date Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timedependent Date Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getTimedependentDateType()
	 * @see #getLEntity()
	 * @generated
	 */
	EAttribute getLEntity_TimedependentDateType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isMappedSuperclass <em>Mapped Superclass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mapped Superclass</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#isMappedSuperclass()
	 * @see #getLEntity()
	 * @generated
	 */
	EAttribute getLEntity_MappedSuperclass();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getPersistenceInfo <em>Persistence Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Persistence Info</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getPersistenceInfo()
	 * @see #getLEntity()
	 * @generated
	 */
	EReference getLEntity_PersistenceInfo();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getInheritanceStrategy <em>Inheritance Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inheritance Strategy</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getInheritanceStrategy()
	 * @see #getLEntity()
	 * @generated
	 */
	EReference getLEntity_InheritanceStrategy();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Features</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getFeatures()
	 * @see #getLEntity()
	 * @generated
	 */
	EReference getLEntity_Features();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getIndexes <em>Indexes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Indexes</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getIndexes()
	 * @see #getLEntity()
	 * @generated
	 */
	EReference getLEntity_Indexes();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getSuperType <em>Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getSuperType()
	 * @see #getLEntity()
	 * @generated
	 */
	EReference getLEntity_SuperType();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getSubTypes <em>Sub Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Types</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getSubTypes()
	 * @see #getLEntity()
	 * @generated
	 */
	EReference getLEntity_SubTypes();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getPersistenceUnit <em>Persistence Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Persistence Unit</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getPersistenceUnit()
	 * @see #getLEntity()
	 * @generated
	 */
	EAttribute getLEntity_PersistenceUnit();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getSuperIndex <em>Super Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Super Index</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getSuperIndex()
	 * @see #getLEntity()
	 * @generated
	 */
	EReference getLEntity_SuperIndex();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getStateClass <em>State Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State Class</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getStateClass()
	 * @see #getLEntity()
	 * @generated
	 */
	EReference getLEntity_StateClass();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getOperations() <em>Get Operations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Operations</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getOperations()
	 * @generated
	 */
	EOperation getLEntity__GetOperations();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getReferences() <em>Get References</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get References</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getReferences()
	 * @generated
	 */
	EOperation getLEntity__GetReferences();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getAttributes() <em>Get Attributes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Attributes</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getAttributes()
	 * @generated
	 */
	EOperation getLEntity__GetAttributes();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getAllFeatures() <em>Get All Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Features</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getAllFeatures()
	 * @generated
	 */
	EOperation getLEntity__GetAllFeatures();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getAllAttributes() <em>Get All Attributes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Attributes</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getAllAttributes()
	 * @generated
	 */
	EOperation getLEntity__GetAllAttributes();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getAllReferences() <em>Get All References</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All References</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getAllReferences()
	 * @generated
	 */
	EOperation getLEntity__GetAllReferences();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#collectAllOSBPFeatures(org.eclipse.osbp.dsl.semantic.entity.LEntity, java.util.List) <em>Collect All OSBP Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Collect All OSBP Features</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#collectAllOSBPFeatures(org.eclipse.osbp.dsl.semantic.entity.LEntity, java.util.List)
	 * @generated
	 */
	EOperation getLEntity__CollectAllOSBPFeatures__LEntity_List();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#collectNormalOSBPFeatures(org.eclipse.osbp.dsl.semantic.entity.LEntity, java.util.List) <em>Collect Normal OSBP Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Collect Normal OSBP Features</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#collectNormalOSBPFeatures(org.eclipse.osbp.dsl.semantic.entity.LEntity, java.util.List)
	 * @generated
	 */
	EOperation getLEntity__CollectNormalOSBPFeatures__LEntity_List();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getPrimaryKeyAttribute() <em>Get Primary Key Attribute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Primary Key Attribute</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getPrimaryKeyAttribute()
	 * @generated
	 */
	EOperation getLEntity__GetPrimaryKeyAttribute();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getIdAttributeName() <em>Get Id Attribute Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Id Attribute Name</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getIdAttributeName()
	 * @generated
	 */
	EOperation getLEntity__GetIdAttributeName();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getIdAttributeType() <em>Get Id Attribute Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Id Attribute Type</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getIdAttributeType()
	 * @generated
	 */
	EOperation getLEntity__GetIdAttributeType();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getCurrentAttribute() <em>Get Current Attribute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Current Attribute</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getCurrentAttribute()
	 * @generated
	 */
	EOperation getLEntity__GetCurrentAttribute();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getVersionAttribute() <em>Get Version Attribute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Version Attribute</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getVersionAttribute()
	 * @generated
	 */
	EOperation getLEntity__GetVersionAttribute();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getVersionAttributeName() <em>Get Version Attribute Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Version Attribute Name</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getVersionAttributeName()
	 * @generated
	 */
	EOperation getLEntity__GetVersionAttributeName();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getVersionAttributeType() <em>Get Version Attribute Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Version Attribute Type</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getVersionAttributeType()
	 * @generated
	 */
	EOperation getLEntity__GetVersionAttributeType();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getNormalFeatures() <em>Get Normal Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Normal Features</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getNormalFeatures()
	 * @generated
	 */
	EOperation getLEntity__GetNormalFeatures();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isHistorizedWithParent() <em>Is Historized With Parent</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Historized With Parent</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#isHistorizedWithParent()
	 * @generated
	 */
	EOperation getLEntity__IsHistorizedWithParent();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isTimedependentWithParent() <em>Is Timedependent With Parent</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Timedependent With Parent</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#isTimedependentWithParent()
	 * @generated
	 */
	EOperation getLEntity__IsTimedependentWithParent();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isHistorizedOrTimedependentWithParent() <em>Is Historized Or Timedependent With Parent</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Historized Or Timedependent With Parent</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#isHistorizedOrTimedependentWithParent()
	 * @generated
	 */
	EOperation getLEntity__IsHistorizedOrTimedependentWithParent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex <em>LEntity Super Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEntity Super Index</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex
	 * @generated
	 */
	EClass getLEntitySuperIndex();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex#getName()
	 * @see #getLEntitySuperIndex()
	 * @generated
	 */
	EAttribute getLEntitySuperIndex_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex#getAttributes()
	 * @see #getLEntitySuperIndex()
	 * @generated
	 */
	EReference getLEntitySuperIndex_Attributes();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LNestedFeature <em>LNested Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LNested Feature</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LNestedFeature
	 * @generated
	 */
	EClass getLNestedFeature();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LNestedFeature#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LNestedFeature#getFeature()
	 * @see #getLNestedFeature()
	 * @generated
	 */
	EReference getLNestedFeature_Feature();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.entity.LNestedFeature#getTail <em>Tail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Tail</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LNestedFeature#getTail()
	 * @see #getLNestedFeature()
	 * @generated
	 */
	EReference getLNestedFeature_Tail();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo <em>LEntity Persistence Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEntity Persistence Info</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo
	 * @generated
	 */
	EClass getLEntityPersistenceInfo();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo#getSchemaName <em>Schema Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Schema Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo#getSchemaName()
	 * @see #getLEntityPersistenceInfo()
	 * @generated
	 */
	EAttribute getLEntityPersistenceInfo_SchemaName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo#getTableName <em>Table Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Table Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo#getTableName()
	 * @see #getLEntityPersistenceInfo()
	 * @generated
	 */
	EAttribute getLEntityPersistenceInfo_TableName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityColumnPersistenceInfo <em>LEntity Column Persistence Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEntity Column Persistence Info</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityColumnPersistenceInfo
	 * @generated
	 */
	EClass getLEntityColumnPersistenceInfo();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityColumnPersistenceInfo#getColumnName <em>Column Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Column Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityColumnPersistenceInfo#getColumnName()
	 * @see #getLEntityColumnPersistenceInfo()
	 * @generated
	 */
	EAttribute getLEntityColumnPersistenceInfo_ColumnName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityInheritanceStrategy <em>LEntity Inheritance Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEntity Inheritance Strategy</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityInheritanceStrategy
	 * @generated
	 */
	EClass getLEntityInheritanceStrategy();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy <em>LTable Per Class Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LTable Per Class Strategy</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy
	 * @generated
	 */
	EClass getLTablePerClassStrategy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy#getDiscriminatorColumn <em>Discriminator Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Discriminator Column</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy#getDiscriminatorColumn()
	 * @see #getLTablePerClassStrategy()
	 * @generated
	 */
	EAttribute getLTablePerClassStrategy_DiscriminatorColumn();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy#getDiscriminatorType <em>Discriminator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Discriminator Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy#getDiscriminatorType()
	 * @see #getLTablePerClassStrategy()
	 * @generated
	 */
	EAttribute getLTablePerClassStrategy_DiscriminatorType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy#getDiscriminatorValue <em>Discriminator Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Discriminator Value</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy#getDiscriminatorValue()
	 * @see #getLTablePerClassStrategy()
	 * @generated
	 */
	EAttribute getLTablePerClassStrategy_DiscriminatorValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy <em>LTable Per Subclass Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LTable Per Subclass Strategy</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy
	 * @generated
	 */
	EClass getLTablePerSubclassStrategy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy#getDiscriminatorColumn <em>Discriminator Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Discriminator Column</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy#getDiscriminatorColumn()
	 * @see #getLTablePerSubclassStrategy()
	 * @generated
	 */
	EAttribute getLTablePerSubclassStrategy_DiscriminatorColumn();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy#getDiscriminatorType <em>Discriminator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Discriminator Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy#getDiscriminatorType()
	 * @see #getLTablePerSubclassStrategy()
	 * @generated
	 */
	EAttribute getLTablePerSubclassStrategy_DiscriminatorType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy#getDiscriminatorValue <em>Discriminator Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Discriminator Value</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy#getDiscriminatorValue()
	 * @see #getLTablePerSubclassStrategy()
	 * @generated
	 */
	EAttribute getLTablePerSubclassStrategy_DiscriminatorValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityFeature <em>LEntity Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEntity Feature</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityFeature
	 * @generated
	 */
	EClass getLEntityFeature();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityFeature#getPersistenceInfo <em>Persistence Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Persistence Info</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityFeature#getPersistenceInfo()
	 * @see #getLEntityFeature()
	 * @generated
	 */
	EReference getLEntityFeature_PersistenceInfo();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityFeature#getEntity() <em>Get Entity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Entity</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityFeature#getEntity()
	 * @generated
	 */
	EOperation getLEntityFeature__GetEntity();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LKanbanStateDetail <em>LKanban State Detail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LKanban State Detail</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LKanbanStateDetail
	 * @generated
	 */
	EClass getLKanbanStateDetail();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LKanbanStateDetail#getInfos <em>Infos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Infos</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LKanbanStateDetail#getInfos()
	 * @see #getLKanbanStateDetail()
	 * @generated
	 */
	EReference getLKanbanStateDetail_Infos();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo <em>LKanban Enum Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LKanban Enum Info</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo
	 * @generated
	 */
	EClass getLKanbanEnumInfo();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Literal</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo#getLiteral()
	 * @see #getLKanbanEnumInfo()
	 * @generated
	 */
	EReference getLKanbanEnumInfo_Literal();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo#getI18nKey <em>I1 8n Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>I1 8n Key</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo#getI18nKey()
	 * @see #getLKanbanEnumInfo()
	 * @generated
	 */
	EAttribute getLKanbanEnumInfo_I18nKey();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute <em>LEntity Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEntity Attribute</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute
	 * @generated
	 */
	EClass getLEntityAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isAsKanbanState <em>As Kanban State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>As Kanban State</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isAsKanbanState()
	 * @see #getLEntityAttribute()
	 * @generated
	 */
	EAttribute getLEntityAttribute_AsKanbanState();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isOnKanbanCard <em>On Kanban Card</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>On Kanban Card</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isOnKanbanCard()
	 * @see #getLEntityAttribute()
	 * @generated
	 */
	EAttribute getLEntityAttribute_OnKanbanCard();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getOnKanbanCardStates <em>On Kanban Card States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>On Kanban Card States</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getOnKanbanCardStates()
	 * @see #getLEntityAttribute()
	 * @generated
	 */
	EReference getLEntityAttribute_OnKanbanCardStates();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isAsKanbanOrdering <em>As Kanban Ordering</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>As Kanban Ordering</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isAsKanbanOrdering()
	 * @see #getLEntityAttribute()
	 * @generated
	 */
	EAttribute getLEntityAttribute_AsKanbanOrdering();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isDecentKanbanOrder <em>Decent Kanban Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Decent Kanban Order</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isDecentKanbanOrder()
	 * @see #getLEntityAttribute()
	 * @generated
	 */
	EAttribute getLEntityAttribute_DecentKanbanOrder();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getOpposite <em>Opposite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getOpposite()
	 * @see #getLEntityAttribute()
	 * @generated
	 */
	EReference getLEntityAttribute_Opposite();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getTypedName <em>Typed Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Typed Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getTypedName()
	 * @see #getLEntityAttribute()
	 * @generated
	 */
	EAttribute getLEntityAttribute_TypedName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityReference <em>LEntity Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEntity Reference</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityReference
	 * @generated
	 */
	EClass getLEntityReference();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityReference#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityReference#getType()
	 * @see #getLEntityReference()
	 * @generated
	 */
	EReference getLEntityReference_Type();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityReference#getOpposite <em>Opposite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityReference#getOpposite()
	 * @see #getLEntityReference()
	 * @generated
	 */
	EReference getLEntityReference_Opposite();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityReference#getResultFilters <em>Result Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Result Filters</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityReference#getResultFilters()
	 * @see #getLEntityReference()
	 * @generated
	 */
	EReference getLEntityReference_ResultFilters();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityReference#getFilterDepth <em>Filter Depth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter Depth</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityReference#getFilterDepth()
	 * @see #getLEntityReference()
	 * @generated
	 */
	EAttribute getLEntityReference_FilterDepth();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityReference#isCascading() <em>Is Cascading</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Cascading</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityReference#isCascading()
	 * @generated
	 */
	EOperation getLEntityReference__IsCascading();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanFeature <em>LBean Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LBean Feature</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBeanFeature
	 * @generated
	 */
	EClass getLBeanFeature();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanFeature#getBean() <em>Get Bean</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Bean</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBeanFeature#getBean()
	 * @generated
	 */
	EOperation getLBeanFeature__GetBean();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute <em>LBean Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LBean Attribute</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute
	 * @generated
	 */
	EClass getLBeanAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute#getTypedName <em>Typed Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Typed Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute#getTypedName()
	 * @see #getLBeanAttribute()
	 * @generated
	 */
	EAttribute getLBeanAttribute_TypedName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference <em>LBean Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LBean Reference</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBeanReference
	 * @generated
	 */
	EClass getLBeanReference();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getType()
	 * @see #getLBeanReference()
	 * @generated
	 */
	EReference getLBeanReference_Type();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getOpposite <em>Opposite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getOpposite()
	 * @see #getLBeanReference()
	 * @generated
	 */
	EReference getLBeanReference_Opposite();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getResultFilters <em>Result Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Result Filters</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getResultFilters()
	 * @see #getLBeanReference()
	 * @generated
	 */
	EReference getLBeanReference_ResultFilters();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#isCascading() <em>Is Cascading</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Cascading</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBeanReference#isCascading()
	 * @generated
	 */
	EOperation getLBeanReference__IsCascading();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LIndex <em>LIndex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LIndex</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LIndex
	 * @generated
	 */
	EClass getLIndex();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LIndex#isUnique <em>Unique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unique</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LIndex#isUnique()
	 * @see #getLIndex()
	 * @generated
	 */
	EAttribute getLIndex_Unique();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.entity.LIndex#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LIndex#getName()
	 * @see #getLIndex()
	 * @generated
	 */
	EAttribute getLIndex_Name();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.osbp.dsl.semantic.entity.LIndex#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Features</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LIndex#getFeatures()
	 * @see #getLIndex()
	 * @generated
	 */
	EReference getLIndex_Features();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.entity.LOperation <em>LOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LOperation</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LOperation
	 * @generated
	 */
	EClass getLOperation();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.dsl.semantic.entity.LDiscriminatorType <em>LDiscriminator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>LDiscriminator Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.entity.LDiscriminatorType
	 * @generated
	 */
	EEnum getLDiscriminatorType();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.InternalEObject <em>Internal EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Internal EObject</em>'.
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @generated
	 */
	EDataType getInternalEObject();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Operations List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Operations List</em>'.
	 * @see java.util.List
	 * @generated
	 */
	EDataType getOperationsList();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Entity Feature List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Entity Feature List</em>'.
	 * @see java.util.List
	 * @generated
	 */
	EDataType getEntityFeatureList();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Entity Reference List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Entity Reference List</em>'.
	 * @see java.util.List
	 * @generated
	 */
	EDataType getEntityReferenceList();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Entity Attribute List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Entity Attribute List</em>'.
	 * @see java.util.List
	 * @generated
	 */
	EDataType getEntityAttributeList();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Bean Feature List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Bean Feature List</em>'.
	 * @see java.util.List
	 * @generated
	 */
	EDataType getBeanFeatureList();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Bean Reference List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Bean Reference List</em>'.
	 * @see java.util.List
	 * @generated
	 */
	EDataType getBeanReferenceList();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Bean Attribute List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Bean Attribute List</em>'.
	 * @see java.util.List
	 * @generated
	 */
	EDataType getBeanAttributeList();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OSBPEntityFactory getOSBPEntityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityModelImpl <em>LEntity Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityModelImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityModel()
		 * @generated
		 */
		EClass LENTITY_MODEL = eINSTANCE.getLEntityModel();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY_MODEL__IMPORT_SECTION = eINSTANCE.getLEntityModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY_MODEL__PACKAGES = eINSTANCE.getLEntityModel_Packages();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanImpl <em>LBean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LBeanImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLBean()
		 * @generated
		 */
		EClass LBEAN = eINSTANCE.getLBean();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LBEAN__FEATURES = eINSTANCE.getLBean_Features();

		/**
		 * The meta object literal for the '<em><b>Super Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LBEAN__SUPER_TYPE = eINSTANCE.getLBean_SuperType();

		/**
		 * The meta object literal for the '<em><b>Sub Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LBEAN__SUB_TYPES = eINSTANCE.getLBean_SubTypes();

		/**
		 * The meta object literal for the '<em><b>Bean On Tab</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LBEAN__BEAN_ON_TAB = eINSTANCE.getLBean_BeanOnTab();

		/**
		 * The meta object literal for the '<em><b>Get Operations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LBEAN___GET_OPERATIONS = eINSTANCE.getLBean__GetOperations();

		/**
		 * The meta object literal for the '<em><b>Get References</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LBEAN___GET_REFERENCES = eINSTANCE.getLBean__GetReferences();

		/**
		 * The meta object literal for the '<em><b>Get Attributes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LBEAN___GET_ATTRIBUTES = eINSTANCE.getLBean__GetAttributes();

		/**
		 * The meta object literal for the '<em><b>Get All Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LBEAN___GET_ALL_FEATURES = eINSTANCE.getLBean__GetAllFeatures();

		/**
		 * The meta object literal for the '<em><b>Get All Attributes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LBEAN___GET_ALL_ATTRIBUTES = eINSTANCE.getLBean__GetAllAttributes();

		/**
		 * The meta object literal for the '<em><b>Get All References</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LBEAN___GET_ALL_REFERENCES = eINSTANCE.getLBean__GetAllReferences();

		/**
		 * The meta object literal for the '<em><b>Collect All OSBP Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LBEAN___COLLECT_ALL_OSBP_FEATURES__LBEAN_LIST = eINSTANCE.getLBean__CollectAllOSBPFeatures__LBean_List();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityImpl <em>LEntity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntity()
		 * @generated
		 */
		EClass LENTITY = eINSTANCE.getLEntity();

		/**
		 * The meta object literal for the '<em><b>Cacheable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY__CACHEABLE = eINSTANCE.getLEntity_Cacheable();

		/**
		 * The meta object literal for the '<em><b>Historized</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY__HISTORIZED = eINSTANCE.getLEntity_Historized();

		/**
		 * The meta object literal for the '<em><b>Timedependent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY__TIMEDEPENDENT = eINSTANCE.getLEntity_Timedependent();

		/**
		 * The meta object literal for the '<em><b>Timedependent Date Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY__TIMEDEPENDENT_DATE_TYPE = eINSTANCE.getLEntity_TimedependentDateType();

		/**
		 * The meta object literal for the '<em><b>Mapped Superclass</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY__MAPPED_SUPERCLASS = eINSTANCE.getLEntity_MappedSuperclass();

		/**
		 * The meta object literal for the '<em><b>Persistence Info</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY__PERSISTENCE_INFO = eINSTANCE.getLEntity_PersistenceInfo();

		/**
		 * The meta object literal for the '<em><b>Inheritance Strategy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY__INHERITANCE_STRATEGY = eINSTANCE.getLEntity_InheritanceStrategy();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY__FEATURES = eINSTANCE.getLEntity_Features();

		/**
		 * The meta object literal for the '<em><b>Indexes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY__INDEXES = eINSTANCE.getLEntity_Indexes();

		/**
		 * The meta object literal for the '<em><b>Super Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY__SUPER_TYPE = eINSTANCE.getLEntity_SuperType();

		/**
		 * The meta object literal for the '<em><b>Sub Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY__SUB_TYPES = eINSTANCE.getLEntity_SubTypes();

		/**
		 * The meta object literal for the '<em><b>Persistence Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY__PERSISTENCE_UNIT = eINSTANCE.getLEntity_PersistenceUnit();

		/**
		 * The meta object literal for the '<em><b>Super Index</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY__SUPER_INDEX = eINSTANCE.getLEntity_SuperIndex();

		/**
		 * The meta object literal for the '<em><b>State Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY__STATE_CLASS = eINSTANCE.getLEntity_StateClass();

		/**
		 * The meta object literal for the '<em><b>Get Operations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_OPERATIONS = eINSTANCE.getLEntity__GetOperations();

		/**
		 * The meta object literal for the '<em><b>Get References</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_REFERENCES = eINSTANCE.getLEntity__GetReferences();

		/**
		 * The meta object literal for the '<em><b>Get Attributes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_ATTRIBUTES = eINSTANCE.getLEntity__GetAttributes();

		/**
		 * The meta object literal for the '<em><b>Get All Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_ALL_FEATURES = eINSTANCE.getLEntity__GetAllFeatures();

		/**
		 * The meta object literal for the '<em><b>Get All Attributes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_ALL_ATTRIBUTES = eINSTANCE.getLEntity__GetAllAttributes();

		/**
		 * The meta object literal for the '<em><b>Get All References</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_ALL_REFERENCES = eINSTANCE.getLEntity__GetAllReferences();

		/**
		 * The meta object literal for the '<em><b>Collect All OSBP Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___COLLECT_ALL_OSBP_FEATURES__LENTITY_LIST = eINSTANCE.getLEntity__CollectAllOSBPFeatures__LEntity_List();

		/**
		 * The meta object literal for the '<em><b>Collect Normal OSBP Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___COLLECT_NORMAL_OSBP_FEATURES__LENTITY_LIST = eINSTANCE.getLEntity__CollectNormalOSBPFeatures__LEntity_List();

		/**
		 * The meta object literal for the '<em><b>Get Primary Key Attribute</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_PRIMARY_KEY_ATTRIBUTE = eINSTANCE.getLEntity__GetPrimaryKeyAttribute();

		/**
		 * The meta object literal for the '<em><b>Get Id Attribute Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_ID_ATTRIBUTE_NAME = eINSTANCE.getLEntity__GetIdAttributeName();

		/**
		 * The meta object literal for the '<em><b>Get Id Attribute Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_ID_ATTRIBUTE_TYPE = eINSTANCE.getLEntity__GetIdAttributeType();

		/**
		 * The meta object literal for the '<em><b>Get Current Attribute</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_CURRENT_ATTRIBUTE = eINSTANCE.getLEntity__GetCurrentAttribute();

		/**
		 * The meta object literal for the '<em><b>Get Version Attribute</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_VERSION_ATTRIBUTE = eINSTANCE.getLEntity__GetVersionAttribute();

		/**
		 * The meta object literal for the '<em><b>Get Version Attribute Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_VERSION_ATTRIBUTE_NAME = eINSTANCE.getLEntity__GetVersionAttributeName();

		/**
		 * The meta object literal for the '<em><b>Get Version Attribute Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_VERSION_ATTRIBUTE_TYPE = eINSTANCE.getLEntity__GetVersionAttributeType();

		/**
		 * The meta object literal for the '<em><b>Get Normal Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___GET_NORMAL_FEATURES = eINSTANCE.getLEntity__GetNormalFeatures();

		/**
		 * The meta object literal for the '<em><b>Is Historized With Parent</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___IS_HISTORIZED_WITH_PARENT = eINSTANCE.getLEntity__IsHistorizedWithParent();

		/**
		 * The meta object literal for the '<em><b>Is Timedependent With Parent</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___IS_TIMEDEPENDENT_WITH_PARENT = eINSTANCE.getLEntity__IsTimedependentWithParent();

		/**
		 * The meta object literal for the '<em><b>Is Historized Or Timedependent With Parent</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY___IS_HISTORIZED_OR_TIMEDEPENDENT_WITH_PARENT = eINSTANCE.getLEntity__IsHistorizedOrTimedependentWithParent();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntitySuperIndexImpl <em>LEntity Super Index</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntitySuperIndexImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntitySuperIndex()
		 * @generated
		 */
		EClass LENTITY_SUPER_INDEX = eINSTANCE.getLEntitySuperIndex();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_SUPER_INDEX__NAME = eINSTANCE.getLEntitySuperIndex_Name();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY_SUPER_INDEX__ATTRIBUTES = eINSTANCE.getLEntitySuperIndex_Attributes();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LNestedFeatureImpl <em>LNested Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LNestedFeatureImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLNestedFeature()
		 * @generated
		 */
		EClass LNESTED_FEATURE = eINSTANCE.getLNestedFeature();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LNESTED_FEATURE__FEATURE = eINSTANCE.getLNestedFeature_Feature();

		/**
		 * The meta object literal for the '<em><b>Tail</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LNESTED_FEATURE__TAIL = eINSTANCE.getLNestedFeature_Tail();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityPersistenceInfoImpl <em>LEntity Persistence Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityPersistenceInfoImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityPersistenceInfo()
		 * @generated
		 */
		EClass LENTITY_PERSISTENCE_INFO = eINSTANCE.getLEntityPersistenceInfo();

		/**
		 * The meta object literal for the '<em><b>Schema Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_PERSISTENCE_INFO__SCHEMA_NAME = eINSTANCE.getLEntityPersistenceInfo_SchemaName();

		/**
		 * The meta object literal for the '<em><b>Table Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_PERSISTENCE_INFO__TABLE_NAME = eINSTANCE.getLEntityPersistenceInfo_TableName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityColumnPersistenceInfoImpl <em>LEntity Column Persistence Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityColumnPersistenceInfoImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityColumnPersistenceInfo()
		 * @generated
		 */
		EClass LENTITY_COLUMN_PERSISTENCE_INFO = eINSTANCE.getLEntityColumnPersistenceInfo();

		/**
		 * The meta object literal for the '<em><b>Column Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_COLUMN_PERSISTENCE_INFO__COLUMN_NAME = eINSTANCE.getLEntityColumnPersistenceInfo_ColumnName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityInheritanceStrategy <em>LEntity Inheritance Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.LEntityInheritanceStrategy
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityInheritanceStrategy()
		 * @generated
		 */
		EClass LENTITY_INHERITANCE_STRATEGY = eINSTANCE.getLEntityInheritanceStrategy();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LTablePerClassStrategyImpl <em>LTable Per Class Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LTablePerClassStrategyImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLTablePerClassStrategy()
		 * @generated
		 */
		EClass LTABLE_PER_CLASS_STRATEGY = eINSTANCE.getLTablePerClassStrategy();

		/**
		 * The meta object literal for the '<em><b>Discriminator Column</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LTABLE_PER_CLASS_STRATEGY__DISCRIMINATOR_COLUMN = eINSTANCE.getLTablePerClassStrategy_DiscriminatorColumn();

		/**
		 * The meta object literal for the '<em><b>Discriminator Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LTABLE_PER_CLASS_STRATEGY__DISCRIMINATOR_TYPE = eINSTANCE.getLTablePerClassStrategy_DiscriminatorType();

		/**
		 * The meta object literal for the '<em><b>Discriminator Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LTABLE_PER_CLASS_STRATEGY__DISCRIMINATOR_VALUE = eINSTANCE.getLTablePerClassStrategy_DiscriminatorValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LTablePerSubclassStrategyImpl <em>LTable Per Subclass Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LTablePerSubclassStrategyImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLTablePerSubclassStrategy()
		 * @generated
		 */
		EClass LTABLE_PER_SUBCLASS_STRATEGY = eINSTANCE.getLTablePerSubclassStrategy();

		/**
		 * The meta object literal for the '<em><b>Discriminator Column</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LTABLE_PER_SUBCLASS_STRATEGY__DISCRIMINATOR_COLUMN = eINSTANCE.getLTablePerSubclassStrategy_DiscriminatorColumn();

		/**
		 * The meta object literal for the '<em><b>Discriminator Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LTABLE_PER_SUBCLASS_STRATEGY__DISCRIMINATOR_TYPE = eINSTANCE.getLTablePerSubclassStrategy_DiscriminatorType();

		/**
		 * The meta object literal for the '<em><b>Discriminator Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LTABLE_PER_SUBCLASS_STRATEGY__DISCRIMINATOR_VALUE = eINSTANCE.getLTablePerSubclassStrategy_DiscriminatorValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityFeatureImpl <em>LEntity Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityFeatureImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityFeature()
		 * @generated
		 */
		EClass LENTITY_FEATURE = eINSTANCE.getLEntityFeature();

		/**
		 * The meta object literal for the '<em><b>Persistence Info</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY_FEATURE__PERSISTENCE_INFO = eINSTANCE.getLEntityFeature_PersistenceInfo();

		/**
		 * The meta object literal for the '<em><b>Get Entity</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY_FEATURE___GET_ENTITY = eINSTANCE.getLEntityFeature__GetEntity();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanStateDetailImpl <em>LKanban State Detail</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanStateDetailImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLKanbanStateDetail()
		 * @generated
		 */
		EClass LKANBAN_STATE_DETAIL = eINSTANCE.getLKanbanStateDetail();

		/**
		 * The meta object literal for the '<em><b>Infos</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LKANBAN_STATE_DETAIL__INFOS = eINSTANCE.getLKanbanStateDetail_Infos();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanEnumInfoImpl <em>LKanban Enum Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanEnumInfoImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLKanbanEnumInfo()
		 * @generated
		 */
		EClass LKANBAN_ENUM_INFO = eINSTANCE.getLKanbanEnumInfo();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LKANBAN_ENUM_INFO__LITERAL = eINSTANCE.getLKanbanEnumInfo_Literal();

		/**
		 * The meta object literal for the '<em><b>I1 8n Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LKANBAN_ENUM_INFO__I18N_KEY = eINSTANCE.getLKanbanEnumInfo_I18nKey();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityAttributeImpl <em>LEntity Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityAttributeImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityAttribute()
		 * @generated
		 */
		EClass LENTITY_ATTRIBUTE = eINSTANCE.getLEntityAttribute();

		/**
		 * The meta object literal for the '<em><b>As Kanban State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_ATTRIBUTE__AS_KANBAN_STATE = eINSTANCE.getLEntityAttribute_AsKanbanState();

		/**
		 * The meta object literal for the '<em><b>On Kanban Card</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_ATTRIBUTE__ON_KANBAN_CARD = eINSTANCE.getLEntityAttribute_OnKanbanCard();

		/**
		 * The meta object literal for the '<em><b>On Kanban Card States</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY_ATTRIBUTE__ON_KANBAN_CARD_STATES = eINSTANCE.getLEntityAttribute_OnKanbanCardStates();

		/**
		 * The meta object literal for the '<em><b>As Kanban Ordering</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_ATTRIBUTE__AS_KANBAN_ORDERING = eINSTANCE.getLEntityAttribute_AsKanbanOrdering();

		/**
		 * The meta object literal for the '<em><b>Decent Kanban Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_ATTRIBUTE__DECENT_KANBAN_ORDER = eINSTANCE.getLEntityAttribute_DecentKanbanOrder();

		/**
		 * The meta object literal for the '<em><b>Opposite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY_ATTRIBUTE__OPPOSITE = eINSTANCE.getLEntityAttribute_Opposite();

		/**
		 * The meta object literal for the '<em><b>Typed Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_ATTRIBUTE__TYPED_NAME = eINSTANCE.getLEntityAttribute_TypedName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl <em>LEntity Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLEntityReference()
		 * @generated
		 */
		EClass LENTITY_REFERENCE = eINSTANCE.getLEntityReference();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY_REFERENCE__TYPE = eINSTANCE.getLEntityReference_Type();

		/**
		 * The meta object literal for the '<em><b>Opposite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY_REFERENCE__OPPOSITE = eINSTANCE.getLEntityReference_Opposite();

		/**
		 * The meta object literal for the '<em><b>Result Filters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENTITY_REFERENCE__RESULT_FILTERS = eINSTANCE.getLEntityReference_ResultFilters();

		/**
		 * The meta object literal for the '<em><b>Filter Depth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENTITY_REFERENCE__FILTER_DEPTH = eINSTANCE.getLEntityReference_FilterDepth();

		/**
		 * The meta object literal for the '<em><b>Is Cascading</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LENTITY_REFERENCE___IS_CASCADING = eINSTANCE.getLEntityReference__IsCascading();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanFeatureImpl <em>LBean Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LBeanFeatureImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLBeanFeature()
		 * @generated
		 */
		EClass LBEAN_FEATURE = eINSTANCE.getLBeanFeature();

		/**
		 * The meta object literal for the '<em><b>Get Bean</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LBEAN_FEATURE___GET_BEAN = eINSTANCE.getLBeanFeature__GetBean();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanAttributeImpl <em>LBean Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LBeanAttributeImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLBeanAttribute()
		 * @generated
		 */
		EClass LBEAN_ATTRIBUTE = eINSTANCE.getLBeanAttribute();

		/**
		 * The meta object literal for the '<em><b>Typed Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LBEAN_ATTRIBUTE__TYPED_NAME = eINSTANCE.getLBeanAttribute_TypedName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanReferenceImpl <em>LBean Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LBeanReferenceImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLBeanReference()
		 * @generated
		 */
		EClass LBEAN_REFERENCE = eINSTANCE.getLBeanReference();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LBEAN_REFERENCE__TYPE = eINSTANCE.getLBeanReference_Type();

		/**
		 * The meta object literal for the '<em><b>Opposite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LBEAN_REFERENCE__OPPOSITE = eINSTANCE.getLBeanReference_Opposite();

		/**
		 * The meta object literal for the '<em><b>Result Filters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LBEAN_REFERENCE__RESULT_FILTERS = eINSTANCE.getLBeanReference_ResultFilters();

		/**
		 * The meta object literal for the '<em><b>Is Cascading</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LBEAN_REFERENCE___IS_CASCADING = eINSTANCE.getLBeanReference__IsCascading();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LIndexImpl <em>LIndex</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LIndexImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLIndex()
		 * @generated
		 */
		EClass LINDEX = eINSTANCE.getLIndex();

		/**
		 * The meta object literal for the '<em><b>Unique</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINDEX__UNIQUE = eINSTANCE.getLIndex_Unique();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINDEX__NAME = eINSTANCE.getLIndex_Name();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINDEX__FEATURES = eINSTANCE.getLIndex_Features();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.impl.LOperationImpl <em>LOperation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.LOperationImpl
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLOperation()
		 * @generated
		 */
		EClass LOPERATION = eINSTANCE.getLOperation();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.entity.LDiscriminatorType <em>LDiscriminator Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.entity.LDiscriminatorType
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getLDiscriminatorType()
		 * @generated
		 */
		EEnum LDISCRIMINATOR_TYPE = eINSTANCE.getLDiscriminatorType();

		/**
		 * The meta object literal for the '<em>Internal EObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.InternalEObject
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getInternalEObject()
		 * @generated
		 */
		EDataType INTERNAL_EOBJECT = eINSTANCE.getInternalEObject();

		/**
		 * The meta object literal for the '<em>Operations List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getOperationsList()
		 * @generated
		 */
		EDataType OPERATIONS_LIST = eINSTANCE.getOperationsList();

		/**
		 * The meta object literal for the '<em>Entity Feature List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getEntityFeatureList()
		 * @generated
		 */
		EDataType ENTITY_FEATURE_LIST = eINSTANCE.getEntityFeatureList();

		/**
		 * The meta object literal for the '<em>Entity Reference List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getEntityReferenceList()
		 * @generated
		 */
		EDataType ENTITY_REFERENCE_LIST = eINSTANCE.getEntityReferenceList();

		/**
		 * The meta object literal for the '<em>Entity Attribute List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getEntityAttributeList()
		 * @generated
		 */
		EDataType ENTITY_ATTRIBUTE_LIST = eINSTANCE.getEntityAttributeList();

		/**
		 * The meta object literal for the '<em>Bean Feature List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getBeanFeatureList()
		 * @generated
		 */
		EDataType BEAN_FEATURE_LIST = eINSTANCE.getBeanFeatureList();

		/**
		 * The meta object literal for the '<em>Bean Reference List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getBeanReferenceList()
		 * @generated
		 */
		EDataType BEAN_REFERENCE_LIST = eINSTANCE.getBeanReferenceList();

		/**
		 * The meta object literal for the '<em>Bean Attribute List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see org.eclipse.osbp.dsl.semantic.entity.impl.OSBPEntityPackageImpl#getBeanAttributeList()
		 * @generated
		 */
		EDataType BEAN_ATTRIBUTE_LIST = eINSTANCE.getBeanAttributeList();

	}

} //OSBPEntityPackage
