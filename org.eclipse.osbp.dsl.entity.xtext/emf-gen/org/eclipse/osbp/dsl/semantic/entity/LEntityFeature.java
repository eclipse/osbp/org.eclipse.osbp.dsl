/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity;

import org.eclipse.osbp.dsl.semantic.common.types.LFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LEntity Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityFeature#getPersistenceInfo <em>Persistence Info</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityFeature()
 * @generated
 */
public interface LEntityFeature extends LFeature {
	/**
	 * Returns the value of the '<em><b>Persistence Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Persistence Info</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persistence Info</em>' containment reference.
	 * @see #setPersistenceInfo(LEntityColumnPersistenceInfo)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityFeature_PersistenceInfo()
	 * @generated
	 */
	LEntityColumnPersistenceInfo getPersistenceInfo();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityFeature#getPersistenceInfo <em>Persistence Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Persistence Info</em>' containment reference.
	 * @see #getPersistenceInfo()
	 * @generated
	 */
	void setPersistenceInfo(LEntityColumnPersistenceInfo value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LEntity getEntity();

} // LEntityFeature
