/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity;

import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LEntity Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isAsKanbanState <em>As Kanban State</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isOnKanbanCard <em>On Kanban Card</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getOnKanbanCardStates <em>On Kanban Card States</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isAsKanbanOrdering <em>As Kanban Ordering</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isDecentKanbanOrder <em>Decent Kanban Order</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getOpposite <em>Opposite</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getTypedName <em>Typed Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityAttribute()
 * @generated
 */
public interface LEntityAttribute extends LEntityFeature, LAttribute {
	/**
	 * Returns the value of the '<em><b>As Kanban State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Kanban State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Kanban State</em>' attribute.
	 * @see #setAsKanbanState(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityAttribute_AsKanbanState()
	 * @generated
	 */
	boolean isAsKanbanState();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isAsKanbanState <em>As Kanban State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>As Kanban State</em>' attribute.
	 * @see #isAsKanbanState()
	 * @generated
	 */
	void setAsKanbanState(boolean value);

	/**
	 * Returns the value of the '<em><b>On Kanban Card</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Kanban Card</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Kanban Card</em>' attribute.
	 * @see #setOnKanbanCard(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityAttribute_OnKanbanCard()
	 * @generated
	 */
	boolean isOnKanbanCard();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isOnKanbanCard <em>On Kanban Card</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Kanban Card</em>' attribute.
	 * @see #isOnKanbanCard()
	 * @generated
	 */
	void setOnKanbanCard(boolean value);

	/**
	 * Returns the value of the '<em><b>On Kanban Card States</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Kanban Card States</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Kanban Card States</em>' containment reference.
	 * @see #setOnKanbanCardStates(LKanbanStateDetail)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityAttribute_OnKanbanCardStates()
	 * @generated
	 */
	LKanbanStateDetail getOnKanbanCardStates();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getOnKanbanCardStates <em>On Kanban Card States</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Kanban Card States</em>' containment reference.
	 * @see #getOnKanbanCardStates()
	 * @generated
	 */
	void setOnKanbanCardStates(LKanbanStateDetail value);

	/**
	 * Returns the value of the '<em><b>As Kanban Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Kanban Ordering</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Kanban Ordering</em>' attribute.
	 * @see #setAsKanbanOrdering(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityAttribute_AsKanbanOrdering()
	 * @generated
	 */
	boolean isAsKanbanOrdering();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isAsKanbanOrdering <em>As Kanban Ordering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>As Kanban Ordering</em>' attribute.
	 * @see #isAsKanbanOrdering()
	 * @generated
	 */
	void setAsKanbanOrdering(boolean value);

	/**
	 * Returns the value of the '<em><b>Decent Kanban Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decent Kanban Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decent Kanban Order</em>' attribute.
	 * @see #setDecentKanbanOrder(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityAttribute_DecentKanbanOrder()
	 * @generated
	 */
	boolean isDecentKanbanOrder();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#isDecentKanbanOrder <em>Decent Kanban Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decent Kanban Order</em>' attribute.
	 * @see #isDecentKanbanOrder()
	 * @generated
	 */
	void setDecentKanbanOrder(boolean value);

	/**
	 * Returns the value of the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opposite</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opposite</em>' reference.
	 * @see #setOpposite(LBeanReference)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityAttribute_Opposite()
	 * @generated
	 */
	LBeanReference getOpposite();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute#getOpposite <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opposite</em>' reference.
	 * @see #getOpposite()
	 * @generated
	 */
	void setOpposite(LBeanReference value);

	/**
	 * Returns the value of the '<em><b>Typed Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Name</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityAttribute_TypedName()
	 * @generated
	 */
	String getTypedName();

} // LEntityAttribute
