/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint;
import org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LResultFilters;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LEntity Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isLazy <em>Lazy</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isCascadeMergePersist <em>Cascade Merge Persist</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isCascadeRemove <em>Cascade Remove</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isCascadeRefresh <em>Cascade Refresh</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isIsGrouped <em>Is Grouped</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#getGroupName <em>Group Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isAsGrid <em>As Grid</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isAsTable <em>As Table</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isSideKick <em>Side Kick</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isReferenceHidden <em>Reference Hidden</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isReferenceReadOnly <em>Reference Read Only</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#isHistorized <em>Historized</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#getOpposite <em>Opposite</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#getResultFilters <em>Result Filters</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LEntityReferenceImpl#getFilterDepth <em>Filter Depth</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LEntityReferenceImpl extends LEntityFeatureImpl implements LEntityReference {
	/**
	 * The default value of the '{@link #isLazy() <em>Lazy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLazy()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LAZY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isLazy() <em>Lazy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLazy()
	 * @generated
	 * @ordered
	 */
	protected boolean lazy = LAZY_EDEFAULT;

	/**
	 * The default value of the '{@link #isCascadeMergePersist() <em>Cascade Merge Persist</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCascadeMergePersist()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CASCADE_MERGE_PERSIST_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCascadeMergePersist() <em>Cascade Merge Persist</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCascadeMergePersist()
	 * @generated
	 * @ordered
	 */
	protected boolean cascadeMergePersist = CASCADE_MERGE_PERSIST_EDEFAULT;

	/**
	 * The default value of the '{@link #isCascadeRemove() <em>Cascade Remove</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCascadeRemove()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CASCADE_REMOVE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCascadeRemove() <em>Cascade Remove</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCascadeRemove()
	 * @generated
	 * @ordered
	 */
	protected boolean cascadeRemove = CASCADE_REMOVE_EDEFAULT;

	/**
	 * The default value of the '{@link #isCascadeRefresh() <em>Cascade Refresh</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCascadeRefresh()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CASCADE_REFRESH_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCascadeRefresh() <em>Cascade Refresh</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCascadeRefresh()
	 * @generated
	 * @ordered
	 */
	protected boolean cascadeRefresh = CASCADE_REFRESH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<LKeyAndValue> properties;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<LDatatypeConstraint> constraints;

	/**
	 * The default value of the '{@link #isIsGrouped() <em>Is Grouped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsGrouped()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_GROUPED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsGrouped() <em>Is Grouped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsGrouped()
	 * @generated
	 * @ordered
	 */
	protected boolean isGrouped = IS_GROUPED_EDEFAULT;

	/**
	 * The default value of the '{@link #getGroupName() <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupName()
	 * @generated
	 * @ordered
	 */
	protected static final String GROUP_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGroupName() <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupName()
	 * @generated
	 * @ordered
	 */
	protected String groupName = GROUP_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isAsGrid() <em>As Grid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAsGrid()
	 * @generated
	 * @ordered
	 */
	protected static final boolean AS_GRID_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAsGrid() <em>As Grid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAsGrid()
	 * @generated
	 * @ordered
	 */
	protected boolean asGrid = AS_GRID_EDEFAULT;

	/**
	 * The default value of the '{@link #isAsTable() <em>As Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAsTable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean AS_TABLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAsTable() <em>As Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAsTable()
	 * @generated
	 * @ordered
	 */
	protected boolean asTable = AS_TABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #isSideKick() <em>Side Kick</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSideKick()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SIDE_KICK_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSideKick() <em>Side Kick</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSideKick()
	 * @generated
	 * @ordered
	 */
	protected boolean sideKick = SIDE_KICK_EDEFAULT;

	/**
	 * The default value of the '{@link #isReferenceHidden() <em>Reference Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReferenceHidden()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REFERENCE_HIDDEN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isReferenceHidden() <em>Reference Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReferenceHidden()
	 * @generated
	 * @ordered
	 */
	protected boolean referenceHidden = REFERENCE_HIDDEN_EDEFAULT;

	/**
	 * The default value of the '{@link #isReferenceReadOnly() <em>Reference Read Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReferenceReadOnly()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REFERENCE_READ_ONLY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isReferenceReadOnly() <em>Reference Read Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isReferenceReadOnly()
	 * @generated
	 * @ordered
	 */
	protected boolean referenceReadOnly = REFERENCE_READ_ONLY_EDEFAULT;

	/**
	 * The default value of the '{@link #isHistorized() <em>Historized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHistorized()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HISTORIZED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHistorized() <em>Historized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHistorized()
	 * @generated
	 * @ordered
	 */
	protected boolean historized = HISTORIZED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected LEntity type;

	/**
	 * The cached value of the '{@link #getOpposite() <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpposite()
	 * @generated
	 * @ordered
	 */
	protected LEntityReference opposite;

	/**
	 * The cached value of the '{@link #getResultFilters() <em>Result Filters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultFilters()
	 * @generated
	 * @ordered
	 */
	protected LResultFilters resultFilters;

	/**
	 * The default value of the '{@link #getFilterDepth() <em>Filter Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterDepth()
	 * @generated
	 * @ordered
	 */
	protected static final int FILTER_DEPTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFilterDepth() <em>Filter Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterDepth()
	 * @generated
	 * @ordered
	 */
	protected int filterDepth = FILTER_DEPTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LEntityReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSBPEntityPackage.Literals.LENTITY_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLazy() {
		return lazy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLazy(boolean newLazy) {
		boolean oldLazy = lazy;
		lazy = newLazy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__LAZY, oldLazy, lazy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCascadeMergePersist() {
		return cascadeMergePersist;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCascadeMergePersist(boolean newCascadeMergePersist) {
		boolean oldCascadeMergePersist = cascadeMergePersist;
		cascadeMergePersist = newCascadeMergePersist;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_MERGE_PERSIST, oldCascadeMergePersist, cascadeMergePersist));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCascadeRemove() {
		return cascadeRemove;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCascadeRemove(boolean newCascadeRemove) {
		boolean oldCascadeRemove = cascadeRemove;
		cascadeRemove = newCascadeRemove;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REMOVE, oldCascadeRemove, cascadeRemove));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCascadeRefresh() {
		return cascadeRefresh;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCascadeRefresh(boolean newCascadeRefresh) {
		boolean oldCascadeRefresh = cascadeRefresh;
		cascadeRefresh = newCascadeRefresh;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REFRESH, oldCascadeRefresh, cascadeRefresh));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LKeyAndValue> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<LKeyAndValue>(LKeyAndValue.class, this, OSBPEntityPackage.LENTITY_REFERENCE__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LDatatypeConstraint> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentEList<LDatatypeConstraint>(LDatatypeConstraint.class, this, OSBPEntityPackage.LENTITY_REFERENCE__CONSTRAINTS);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsGrouped() {
		return isGrouped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsGrouped(boolean newIsGrouped) {
		boolean oldIsGrouped = isGrouped;
		isGrouped = newIsGrouped;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__IS_GROUPED, oldIsGrouped, isGrouped));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroupName(String newGroupName) {
		String oldGroupName = groupName;
		groupName = newGroupName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__GROUP_NAME, oldGroupName, groupName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAsGrid() {
		return asGrid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsGrid(boolean newAsGrid) {
		boolean oldAsGrid = asGrid;
		asGrid = newAsGrid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__AS_GRID, oldAsGrid, asGrid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAsTable() {
		return asTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsTable(boolean newAsTable) {
		boolean oldAsTable = asTable;
		asTable = newAsTable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__AS_TABLE, oldAsTable, asTable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSideKick() {
		return sideKick;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSideKick(boolean newSideKick) {
		boolean oldSideKick = sideKick;
		sideKick = newSideKick;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__SIDE_KICK, oldSideKick, sideKick));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isReferenceHidden() {
		return referenceHidden;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceHidden(boolean newReferenceHidden) {
		boolean oldReferenceHidden = referenceHidden;
		referenceHidden = newReferenceHidden;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_HIDDEN, oldReferenceHidden, referenceHidden));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isReferenceReadOnly() {
		return referenceReadOnly;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceReadOnly(boolean newReferenceReadOnly) {
		boolean oldReferenceReadOnly = referenceReadOnly;
		referenceReadOnly = newReferenceReadOnly;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_READ_ONLY, oldReferenceReadOnly, referenceReadOnly));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHistorized() {
		return historized;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorized(boolean newHistorized) {
		boolean oldHistorized = historized;
		historized = newHistorized;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__HISTORIZED, oldHistorized, historized));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntity getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (LEntity)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OSBPEntityPackage.LENTITY_REFERENCE__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntity basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(LEntity newType) {
		LEntity oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityReference getOpposite() {
		if (opposite != null && opposite.eIsProxy()) {
			InternalEObject oldOpposite = (InternalEObject)opposite;
			opposite = (LEntityReference)eResolveProxy(oldOpposite);
			if (opposite != oldOpposite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OSBPEntityPackage.LENTITY_REFERENCE__OPPOSITE, oldOpposite, opposite));
			}
		}
		return opposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityReference basicGetOpposite() {
		return opposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpposite(LEntityReference newOpposite) {
		LEntityReference oldOpposite = opposite;
		opposite = newOpposite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__OPPOSITE, oldOpposite, opposite));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LResultFilters getResultFilters() {
		return resultFilters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResultFilters(LResultFilters newResultFilters, NotificationChain msgs) {
		LResultFilters oldResultFilters = resultFilters;
		resultFilters = newResultFilters;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__RESULT_FILTERS, oldResultFilters, newResultFilters);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultFilters(LResultFilters newResultFilters) {
		if (newResultFilters != resultFilters) {
			NotificationChain msgs = null;
			if (resultFilters != null)
				msgs = ((InternalEObject)resultFilters).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OSBPEntityPackage.LENTITY_REFERENCE__RESULT_FILTERS, null, msgs);
			if (newResultFilters != null)
				msgs = ((InternalEObject)newResultFilters).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OSBPEntityPackage.LENTITY_REFERENCE__RESULT_FILTERS, null, msgs);
			msgs = basicSetResultFilters(newResultFilters, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__RESULT_FILTERS, newResultFilters, newResultFilters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFilterDepth() {
		return filterDepth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilterDepth(int newFilterDepth) {
		int oldFilterDepth = filterDepth;
		filterDepth = newFilterDepth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LENTITY_REFERENCE__FILTER_DEPTH, oldFilterDepth, filterDepth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCascading() {
		return ((this.isCascadeMergePersist() || this.isCascadeRemove()) || this.isCascadeRefresh());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OSBPEntityPackage.LENTITY_REFERENCE__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case OSBPEntityPackage.LENTITY_REFERENCE__CONSTRAINTS:
				return ((InternalEList<?>)getConstraints()).basicRemove(otherEnd, msgs);
			case OSBPEntityPackage.LENTITY_REFERENCE__RESULT_FILTERS:
				return basicSetResultFilters(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OSBPEntityPackage.LENTITY_REFERENCE__LAZY:
				return isLazy();
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_MERGE_PERSIST:
				return isCascadeMergePersist();
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REMOVE:
				return isCascadeRemove();
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REFRESH:
				return isCascadeRefresh();
			case OSBPEntityPackage.LENTITY_REFERENCE__PROPERTIES:
				return getProperties();
			case OSBPEntityPackage.LENTITY_REFERENCE__CONSTRAINTS:
				return getConstraints();
			case OSBPEntityPackage.LENTITY_REFERENCE__IS_GROUPED:
				return isIsGrouped();
			case OSBPEntityPackage.LENTITY_REFERENCE__GROUP_NAME:
				return getGroupName();
			case OSBPEntityPackage.LENTITY_REFERENCE__AS_GRID:
				return isAsGrid();
			case OSBPEntityPackage.LENTITY_REFERENCE__AS_TABLE:
				return isAsTable();
			case OSBPEntityPackage.LENTITY_REFERENCE__SIDE_KICK:
				return isSideKick();
			case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_HIDDEN:
				return isReferenceHidden();
			case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_READ_ONLY:
				return isReferenceReadOnly();
			case OSBPEntityPackage.LENTITY_REFERENCE__HISTORIZED:
				return isHistorized();
			case OSBPEntityPackage.LENTITY_REFERENCE__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case OSBPEntityPackage.LENTITY_REFERENCE__OPPOSITE:
				if (resolve) return getOpposite();
				return basicGetOpposite();
			case OSBPEntityPackage.LENTITY_REFERENCE__RESULT_FILTERS:
				return getResultFilters();
			case OSBPEntityPackage.LENTITY_REFERENCE__FILTER_DEPTH:
				return getFilterDepth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OSBPEntityPackage.LENTITY_REFERENCE__LAZY:
				setLazy((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_MERGE_PERSIST:
				setCascadeMergePersist((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REMOVE:
				setCascadeRemove((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REFRESH:
				setCascadeRefresh((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__PROPERTIES:
				getProperties().clear();
				getProperties().addAll((Collection<? extends LKeyAndValue>)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection<? extends LDatatypeConstraint>)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__IS_GROUPED:
				setIsGrouped((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__GROUP_NAME:
				setGroupName((String)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__AS_GRID:
				setAsGrid((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__AS_TABLE:
				setAsTable((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__SIDE_KICK:
				setSideKick((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_HIDDEN:
				setReferenceHidden((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_READ_ONLY:
				setReferenceReadOnly((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__HISTORIZED:
				setHistorized((Boolean)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__TYPE:
				setType((LEntity)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__OPPOSITE:
				setOpposite((LEntityReference)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__RESULT_FILTERS:
				setResultFilters((LResultFilters)newValue);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__FILTER_DEPTH:
				setFilterDepth((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OSBPEntityPackage.LENTITY_REFERENCE__LAZY:
				setLazy(LAZY_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_MERGE_PERSIST:
				setCascadeMergePersist(CASCADE_MERGE_PERSIST_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REMOVE:
				setCascadeRemove(CASCADE_REMOVE_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REFRESH:
				setCascadeRefresh(CASCADE_REFRESH_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__PROPERTIES:
				getProperties().clear();
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__CONSTRAINTS:
				getConstraints().clear();
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__IS_GROUPED:
				setIsGrouped(IS_GROUPED_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__GROUP_NAME:
				setGroupName(GROUP_NAME_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__AS_GRID:
				setAsGrid(AS_GRID_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__AS_TABLE:
				setAsTable(AS_TABLE_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__SIDE_KICK:
				setSideKick(SIDE_KICK_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_HIDDEN:
				setReferenceHidden(REFERENCE_HIDDEN_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_READ_ONLY:
				setReferenceReadOnly(REFERENCE_READ_ONLY_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__HISTORIZED:
				setHistorized(HISTORIZED_EDEFAULT);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__TYPE:
				setType((LEntity)null);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__OPPOSITE:
				setOpposite((LEntityReference)null);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__RESULT_FILTERS:
				setResultFilters((LResultFilters)null);
				return;
			case OSBPEntityPackage.LENTITY_REFERENCE__FILTER_DEPTH:
				setFilterDepth(FILTER_DEPTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OSBPEntityPackage.LENTITY_REFERENCE__LAZY:
				return lazy != LAZY_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_MERGE_PERSIST:
				return cascadeMergePersist != CASCADE_MERGE_PERSIST_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REMOVE:
				return cascadeRemove != CASCADE_REMOVE_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REFRESH:
				return cascadeRefresh != CASCADE_REFRESH_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case OSBPEntityPackage.LENTITY_REFERENCE__CONSTRAINTS:
				return constraints != null && !constraints.isEmpty();
			case OSBPEntityPackage.LENTITY_REFERENCE__IS_GROUPED:
				return isGrouped != IS_GROUPED_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__GROUP_NAME:
				return GROUP_NAME_EDEFAULT == null ? groupName != null : !GROUP_NAME_EDEFAULT.equals(groupName);
			case OSBPEntityPackage.LENTITY_REFERENCE__AS_GRID:
				return asGrid != AS_GRID_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__AS_TABLE:
				return asTable != AS_TABLE_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__SIDE_KICK:
				return sideKick != SIDE_KICK_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_HIDDEN:
				return referenceHidden != REFERENCE_HIDDEN_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_READ_ONLY:
				return referenceReadOnly != REFERENCE_READ_ONLY_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__HISTORIZED:
				return historized != HISTORIZED_EDEFAULT;
			case OSBPEntityPackage.LENTITY_REFERENCE__TYPE:
				return type != null;
			case OSBPEntityPackage.LENTITY_REFERENCE__OPPOSITE:
				return opposite != null;
			case OSBPEntityPackage.LENTITY_REFERENCE__RESULT_FILTERS:
				return resultFilters != null;
			case OSBPEntityPackage.LENTITY_REFERENCE__FILTER_DEPTH:
				return filterDepth != FILTER_DEPTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == LReference.class) {
			switch (derivedFeatureID) {
				case OSBPEntityPackage.LENTITY_REFERENCE__LAZY: return OSBPTypesPackage.LREFERENCE__LAZY;
				case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_MERGE_PERSIST: return OSBPTypesPackage.LREFERENCE__CASCADE_MERGE_PERSIST;
				case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REMOVE: return OSBPTypesPackage.LREFERENCE__CASCADE_REMOVE;
				case OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REFRESH: return OSBPTypesPackage.LREFERENCE__CASCADE_REFRESH;
				case OSBPEntityPackage.LENTITY_REFERENCE__PROPERTIES: return OSBPTypesPackage.LREFERENCE__PROPERTIES;
				case OSBPEntityPackage.LENTITY_REFERENCE__CONSTRAINTS: return OSBPTypesPackage.LREFERENCE__CONSTRAINTS;
				case OSBPEntityPackage.LENTITY_REFERENCE__IS_GROUPED: return OSBPTypesPackage.LREFERENCE__IS_GROUPED;
				case OSBPEntityPackage.LENTITY_REFERENCE__GROUP_NAME: return OSBPTypesPackage.LREFERENCE__GROUP_NAME;
				case OSBPEntityPackage.LENTITY_REFERENCE__AS_GRID: return OSBPTypesPackage.LREFERENCE__AS_GRID;
				case OSBPEntityPackage.LENTITY_REFERENCE__AS_TABLE: return OSBPTypesPackage.LREFERENCE__AS_TABLE;
				case OSBPEntityPackage.LENTITY_REFERENCE__SIDE_KICK: return OSBPTypesPackage.LREFERENCE__SIDE_KICK;
				case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_HIDDEN: return OSBPTypesPackage.LREFERENCE__REFERENCE_HIDDEN;
				case OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_READ_ONLY: return OSBPTypesPackage.LREFERENCE__REFERENCE_READ_ONLY;
				case OSBPEntityPackage.LENTITY_REFERENCE__HISTORIZED: return OSBPTypesPackage.LREFERENCE__HISTORIZED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == LReference.class) {
			switch (baseFeatureID) {
				case OSBPTypesPackage.LREFERENCE__LAZY: return OSBPEntityPackage.LENTITY_REFERENCE__LAZY;
				case OSBPTypesPackage.LREFERENCE__CASCADE_MERGE_PERSIST: return OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_MERGE_PERSIST;
				case OSBPTypesPackage.LREFERENCE__CASCADE_REMOVE: return OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REMOVE;
				case OSBPTypesPackage.LREFERENCE__CASCADE_REFRESH: return OSBPEntityPackage.LENTITY_REFERENCE__CASCADE_REFRESH;
				case OSBPTypesPackage.LREFERENCE__PROPERTIES: return OSBPEntityPackage.LENTITY_REFERENCE__PROPERTIES;
				case OSBPTypesPackage.LREFERENCE__CONSTRAINTS: return OSBPEntityPackage.LENTITY_REFERENCE__CONSTRAINTS;
				case OSBPTypesPackage.LREFERENCE__IS_GROUPED: return OSBPEntityPackage.LENTITY_REFERENCE__IS_GROUPED;
				case OSBPTypesPackage.LREFERENCE__GROUP_NAME: return OSBPEntityPackage.LENTITY_REFERENCE__GROUP_NAME;
				case OSBPTypesPackage.LREFERENCE__AS_GRID: return OSBPEntityPackage.LENTITY_REFERENCE__AS_GRID;
				case OSBPTypesPackage.LREFERENCE__AS_TABLE: return OSBPEntityPackage.LENTITY_REFERENCE__AS_TABLE;
				case OSBPTypesPackage.LREFERENCE__SIDE_KICK: return OSBPEntityPackage.LENTITY_REFERENCE__SIDE_KICK;
				case OSBPTypesPackage.LREFERENCE__REFERENCE_HIDDEN: return OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_HIDDEN;
				case OSBPTypesPackage.LREFERENCE__REFERENCE_READ_ONLY: return OSBPEntityPackage.LENTITY_REFERENCE__REFERENCE_READ_ONLY;
				case OSBPTypesPackage.LREFERENCE__HISTORIZED: return OSBPEntityPackage.LENTITY_REFERENCE__HISTORIZED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OSBPEntityPackage.LENTITY_REFERENCE___IS_CASCADING:
				return isCascading();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (lazy: ");
		result.append(lazy);
		result.append(", cascadeMergePersist: ");
		result.append(cascadeMergePersist);
		result.append(", cascadeRemove: ");
		result.append(cascadeRemove);
		result.append(", cascadeRefresh: ");
		result.append(cascadeRefresh);
		result.append(", isGrouped: ");
		result.append(isGrouped);
		result.append(", groupName: ");
		result.append(groupName);
		result.append(", asGrid: ");
		result.append(asGrid);
		result.append(", asTable: ");
		result.append(asTable);
		result.append(", sideKick: ");
		result.append(sideKick);
		result.append(", referenceHidden: ");
		result.append(referenceHidden);
		result.append(", referenceReadOnly: ");
		result.append(referenceReadOnly);
		result.append(", historized: ");
		result.append(historized);
		result.append(", filterDepth: ");
		result.append(filterDepth);
		result.append(')');
		return result.toString();
	}

} //LEntityReferenceImpl
