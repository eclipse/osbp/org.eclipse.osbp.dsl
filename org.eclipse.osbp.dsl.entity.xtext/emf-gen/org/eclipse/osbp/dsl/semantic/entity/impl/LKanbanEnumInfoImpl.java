/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;

import org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo;
import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LKanban Enum Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanEnumInfoImpl#getLiteral <em>Literal</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LKanbanEnumInfoImpl#getI18nKey <em>I1 8n Key</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LKanbanEnumInfoImpl extends MinimalEObjectImpl.Container implements LKanbanEnumInfo {
	/**
	 * The cached value of the '{@link #getLiteral() <em>Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLiteral()
	 * @generated
	 * @ordered
	 */
	protected LEnumLiteral literal;

	/**
	 * The default value of the '{@link #getI18nKey() <em>I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getI18nKey()
	 * @generated
	 * @ordered
	 */
	protected static final String I18N_KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getI18nKey() <em>I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getI18nKey()
	 * @generated
	 * @ordered
	 */
	protected String i18nKey = I18N_KEY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LKanbanEnumInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSBPEntityPackage.Literals.LKANBAN_ENUM_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEnumLiteral getLiteral() {
		if (literal != null && literal.eIsProxy()) {
			InternalEObject oldLiteral = (InternalEObject)literal;
			literal = (LEnumLiteral)eResolveProxy(oldLiteral);
			if (literal != oldLiteral) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OSBPEntityPackage.LKANBAN_ENUM_INFO__LITERAL, oldLiteral, literal));
			}
		}
		return literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEnumLiteral basicGetLiteral() {
		return literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLiteral(LEnumLiteral newLiteral) {
		LEnumLiteral oldLiteral = literal;
		literal = newLiteral;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LKANBAN_ENUM_INFO__LITERAL, oldLiteral, literal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getI18nKey() {
		return i18nKey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setI18nKey(String newI18nKey) {
		String oldI18nKey = i18nKey;
		i18nKey = newI18nKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LKANBAN_ENUM_INFO__I18N_KEY, oldI18nKey, i18nKey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OSBPEntityPackage.LKANBAN_ENUM_INFO__LITERAL:
				if (resolve) return getLiteral();
				return basicGetLiteral();
			case OSBPEntityPackage.LKANBAN_ENUM_INFO__I18N_KEY:
				return getI18nKey();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OSBPEntityPackage.LKANBAN_ENUM_INFO__LITERAL:
				setLiteral((LEnumLiteral)newValue);
				return;
			case OSBPEntityPackage.LKANBAN_ENUM_INFO__I18N_KEY:
				setI18nKey((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OSBPEntityPackage.LKANBAN_ENUM_INFO__LITERAL:
				setLiteral((LEnumLiteral)null);
				return;
			case OSBPEntityPackage.LKANBAN_ENUM_INFO__I18N_KEY:
				setI18nKey(I18N_KEY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OSBPEntityPackage.LKANBAN_ENUM_INFO__LITERAL:
				return literal != null;
			case OSBPEntityPackage.LKANBAN_ENUM_INFO__I18N_KEY:
				return I18N_KEY_EDEFAULT == null ? i18nKey != null : !I18N_KEY_EDEFAULT.equals(i18nKey);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (i18nKey: ");
		result.append(i18nKey);
		result.append(')');
		return result.toString();
	}

} //LKanbanEnumInfoImpl
