/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LDateType;
import org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LEntity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Represents a JPA entity.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isCacheable <em>Cacheable</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isHistorized <em>Historized</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isTimedependent <em>Timedependent</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getTimedependentDateType <em>Timedependent Date Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isMappedSuperclass <em>Mapped Superclass</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getPersistenceInfo <em>Persistence Info</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getInheritanceStrategy <em>Inheritance Strategy</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getFeatures <em>Features</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getIndexes <em>Indexes</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getSubTypes <em>Sub Types</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getPersistenceUnit <em>Persistence Unit</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getSuperIndex <em>Super Index</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getStateClass <em>State Class</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity()
 * @generated
 */
public interface LEntity extends LClass, LFeaturesHolder {
	/**
	 * Returns the value of the '<em><b>Cacheable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cacheable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cacheable</em>' attribute.
	 * @see #setCacheable(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_Cacheable()
	 * @generated
	 */
	boolean isCacheable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isCacheable <em>Cacheable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cacheable</em>' attribute.
	 * @see #isCacheable()
	 * @generated
	 */
	void setCacheable(boolean value);

	/**
	 * Returns the value of the '<em><b>Historized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Historized</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Historized</em>' attribute.
	 * @see #setHistorized(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_Historized()
	 * @generated
	 */
	boolean isHistorized();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isHistorized <em>Historized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized</em>' attribute.
	 * @see #isHistorized()
	 * @generated
	 */
	void setHistorized(boolean value);

	/**
	 * Returns the value of the '<em><b>Timedependent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timedependent</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timedependent</em>' attribute.
	 * @see #setTimedependent(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_Timedependent()
	 * @generated
	 */
	boolean isTimedependent();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isTimedependent <em>Timedependent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timedependent</em>' attribute.
	 * @see #isTimedependent()
	 * @generated
	 */
	void setTimedependent(boolean value);

	/**
	 * Returns the value of the '<em><b>Timedependent Date Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.dsl.semantic.common.types.LDateType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timedependent Date Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timedependent Date Type</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateType
	 * @see #setTimedependentDateType(LDateType)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_TimedependentDateType()
	 * @generated
	 */
	LDateType getTimedependentDateType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getTimedependentDateType <em>Timedependent Date Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timedependent Date Type</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateType
	 * @see #getTimedependentDateType()
	 * @generated
	 */
	void setTimedependentDateType(LDateType value);

	/**
	 * Returns the value of the '<em><b>Mapped Superclass</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mapped Superclass</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapped Superclass</em>' attribute.
	 * @see #setMappedSuperclass(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_MappedSuperclass()
	 * @generated
	 */
	boolean isMappedSuperclass();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#isMappedSuperclass <em>Mapped Superclass</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mapped Superclass</em>' attribute.
	 * @see #isMappedSuperclass()
	 * @generated
	 */
	void setMappedSuperclass(boolean value);

	/**
	 * Returns the value of the '<em><b>Persistence Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Persistence Info</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persistence Info</em>' containment reference.
	 * @see #setPersistenceInfo(LEntityPersistenceInfo)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_PersistenceInfo()
	 * @generated
	 */
	LEntityPersistenceInfo getPersistenceInfo();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getPersistenceInfo <em>Persistence Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Persistence Info</em>' containment reference.
	 * @see #getPersistenceInfo()
	 * @generated
	 */
	void setPersistenceInfo(LEntityPersistenceInfo value);

	/**
	 * Returns the value of the '<em><b>Inheritance Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inheritance Strategy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inheritance Strategy</em>' containment reference.
	 * @see #setInheritanceStrategy(LEntityInheritanceStrategy)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_InheritanceStrategy()
	 * @generated
	 */
	LEntityInheritanceStrategy getInheritanceStrategy();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getInheritanceStrategy <em>Inheritance Strategy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inheritance Strategy</em>' containment reference.
	 * @see #getInheritanceStrategy()
	 * @generated
	 */
	void setInheritanceStrategy(LEntityInheritanceStrategy value);

	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.entity.LEntityFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_Features()
	 * @generated
	 */
	EList<LEntityFeature> getFeatures();

	/**
	 * Returns the value of the '<em><b>Indexes</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.entity.LIndex}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Indexes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Indexes</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_Indexes()
	 * @generated
	 */
	EList<LIndex> getIndexes();

	/**
	 * Returns the value of the '<em><b>Super Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getSubTypes <em>Sub Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Type</em>' reference.
	 * @see #setSuperType(LEntity)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_SuperType()
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getSubTypes
	 * @generated
	 */
	LEntity getSuperType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getSuperType <em>Super Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Type</em>' reference.
	 * @see #getSuperType()
	 * @generated
	 */
	void setSuperType(LEntity value);

	/**
	 * Returns the value of the '<em><b>Sub Types</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.entity.LEntity}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getSuperType <em>Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Types</em>' reference list.
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_SubTypes()
	 * @see org.eclipse.osbp.dsl.semantic.entity.LEntity#getSuperType
	 * @generated
	 */
	EList<LEntity> getSubTypes();

	/**
	 * Returns the value of the '<em><b>Persistence Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Persistence Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persistence Unit</em>' attribute.
	 * @see #setPersistenceUnit(String)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_PersistenceUnit()
	 * @generated
	 */
	String getPersistenceUnit();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getPersistenceUnit <em>Persistence Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Persistence Unit</em>' attribute.
	 * @see #getPersistenceUnit()
	 * @generated
	 */
	void setPersistenceUnit(String value);

	/**
	 * Returns the value of the '<em><b>Super Index</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Index</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Index</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_SuperIndex()
	 * @generated
	 */
	EList<LEntitySuperIndex> getSuperIndex();

	/**
	 * Returns the value of the '<em><b>State Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Class</em>' reference.
	 * @see #setStateClass(LStateClass)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntity_StateClass()
	 * @generated
	 */
	LStateClass getStateClass();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntity#getStateClass <em>State Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Class</em>' reference.
	 * @see #getStateClass()
	 * @generated
	 */
	void setStateClass(LStateClass value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of type LOperation
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LOperation> getOperations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of type LEntityReference
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LEntityReference> getReferences();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of type LEntityAttribute
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LEntityAttribute> getAttributes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of the holder and from super types
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LEntityFeature> getAllFeatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all attributes of the holder and from super types
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LEntityAttribute> getAllAttributes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all references of the holder and from super types
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LEntityReference> getAllReferences();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void collectAllOSBPFeatures(LEntity current, List<LEntityFeature> result);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void collectNormalOSBPFeatures(LEntity current, List<LEntityFeature> result);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LEntityAttribute getPrimaryKeyAttribute();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String getIdAttributeName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LScalarType getIdAttributeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LEntityAttribute getCurrentAttribute();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LEntityAttribute getVersionAttribute();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String getVersionAttributeName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LScalarType getVersionAttributeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	List<LEntityFeature> getNormalFeatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Checks this entity and its parents, whether it is historized.
	 * <!-- end-model-doc -->
	 * @generated
	 */
	boolean isHistorizedWithParent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Checks this entity and its parents, whether it is timedependent.
	 * <!-- end-model-doc -->
	 * @generated
	 */
	boolean isTimedependentWithParent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	boolean isHistorizedOrTimedependentWithParent();

} // LEntity
