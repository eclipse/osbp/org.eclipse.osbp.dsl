/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity;

import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LResultFilters;
import org.eclipse.osbp.dsl.semantic.common.types.LType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LBean Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getOpposite <em>Opposite</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getResultFilters <em>Result Filters</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLBeanReference()
 * @generated
 */
public interface LBeanReference extends LBeanFeature, LReference {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(LType)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLBeanReference_Type()
	 * @generated
	 */
	LType getType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(LType value);

	/**
	 * Returns the value of the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opposite</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opposite</em>' reference.
	 * @see #setOpposite(LFeature)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLBeanReference_Opposite()
	 * @generated
	 */
	LFeature getOpposite();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getOpposite <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opposite</em>' reference.
	 * @see #getOpposite()
	 * @generated
	 */
	void setOpposite(LFeature value);

	/**
	 * Returns the value of the '<em><b>Result Filters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result Filters</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Filters</em>' containment reference.
	 * @see #setResultFilters(LResultFilters)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLBeanReference_ResultFilters()
	 * @generated
	 */
	LResultFilters getResultFilters();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LBeanReference#getResultFilters <em>Result Filters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Filters</em>' containment reference.
	 * @see #getResultFilters()
	 * @generated
	 */
	void setResultFilters(LResultFilters value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	boolean isCascading();

} // LBeanReference
