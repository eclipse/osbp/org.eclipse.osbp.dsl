/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity.impl;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.osbp.dsl.semantic.entity.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OSBPEntityFactoryImpl extends EFactoryImpl implements OSBPEntityFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OSBPEntityFactory init() {
		try {
			OSBPEntityFactory theOSBPEntityFactory = (OSBPEntityFactory)EPackage.Registry.INSTANCE.getEFactory(OSBPEntityPackage.eNS_URI);
			if (theOSBPEntityFactory != null) {
				return theOSBPEntityFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OSBPEntityFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPEntityFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OSBPEntityPackage.LENTITY_MODEL: return createLEntityModel();
			case OSBPEntityPackage.LBEAN: return createLBean();
			case OSBPEntityPackage.LENTITY: return createLEntity();
			case OSBPEntityPackage.LENTITY_SUPER_INDEX: return createLEntitySuperIndex();
			case OSBPEntityPackage.LNESTED_FEATURE: return createLNestedFeature();
			case OSBPEntityPackage.LENTITY_PERSISTENCE_INFO: return createLEntityPersistenceInfo();
			case OSBPEntityPackage.LENTITY_COLUMN_PERSISTENCE_INFO: return createLEntityColumnPersistenceInfo();
			case OSBPEntityPackage.LTABLE_PER_CLASS_STRATEGY: return createLTablePerClassStrategy();
			case OSBPEntityPackage.LTABLE_PER_SUBCLASS_STRATEGY: return createLTablePerSubclassStrategy();
			case OSBPEntityPackage.LENTITY_FEATURE: return createLEntityFeature();
			case OSBPEntityPackage.LKANBAN_STATE_DETAIL: return createLKanbanStateDetail();
			case OSBPEntityPackage.LKANBAN_ENUM_INFO: return createLKanbanEnumInfo();
			case OSBPEntityPackage.LENTITY_ATTRIBUTE: return createLEntityAttribute();
			case OSBPEntityPackage.LENTITY_REFERENCE: return createLEntityReference();
			case OSBPEntityPackage.LBEAN_FEATURE: return createLBeanFeature();
			case OSBPEntityPackage.LBEAN_ATTRIBUTE: return createLBeanAttribute();
			case OSBPEntityPackage.LBEAN_REFERENCE: return createLBeanReference();
			case OSBPEntityPackage.LINDEX: return createLIndex();
			case OSBPEntityPackage.LOPERATION: return createLOperation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OSBPEntityPackage.LDISCRIMINATOR_TYPE:
				return createLDiscriminatorTypeFromString(eDataType, initialValue);
			case OSBPEntityPackage.INTERNAL_EOBJECT:
				return createInternalEObjectFromString(eDataType, initialValue);
			case OSBPEntityPackage.OPERATIONS_LIST:
				return createOperationsListFromString(eDataType, initialValue);
			case OSBPEntityPackage.ENTITY_FEATURE_LIST:
				return createEntityFeatureListFromString(eDataType, initialValue);
			case OSBPEntityPackage.ENTITY_REFERENCE_LIST:
				return createEntityReferenceListFromString(eDataType, initialValue);
			case OSBPEntityPackage.ENTITY_ATTRIBUTE_LIST:
				return createEntityAttributeListFromString(eDataType, initialValue);
			case OSBPEntityPackage.BEAN_FEATURE_LIST:
				return createBeanFeatureListFromString(eDataType, initialValue);
			case OSBPEntityPackage.BEAN_REFERENCE_LIST:
				return createBeanReferenceListFromString(eDataType, initialValue);
			case OSBPEntityPackage.BEAN_ATTRIBUTE_LIST:
				return createBeanAttributeListFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OSBPEntityPackage.LDISCRIMINATOR_TYPE:
				return convertLDiscriminatorTypeToString(eDataType, instanceValue);
			case OSBPEntityPackage.INTERNAL_EOBJECT:
				return convertInternalEObjectToString(eDataType, instanceValue);
			case OSBPEntityPackage.OPERATIONS_LIST:
				return convertOperationsListToString(eDataType, instanceValue);
			case OSBPEntityPackage.ENTITY_FEATURE_LIST:
				return convertEntityFeatureListToString(eDataType, instanceValue);
			case OSBPEntityPackage.ENTITY_REFERENCE_LIST:
				return convertEntityReferenceListToString(eDataType, instanceValue);
			case OSBPEntityPackage.ENTITY_ATTRIBUTE_LIST:
				return convertEntityAttributeListToString(eDataType, instanceValue);
			case OSBPEntityPackage.BEAN_FEATURE_LIST:
				return convertBeanFeatureListToString(eDataType, instanceValue);
			case OSBPEntityPackage.BEAN_REFERENCE_LIST:
				return convertBeanReferenceListToString(eDataType, instanceValue);
			case OSBPEntityPackage.BEAN_ATTRIBUTE_LIST:
				return convertBeanAttributeListToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityModel createLEntityModel() {
		LEntityModelImpl lEntityModel = new LEntityModelImpl();
		return lEntityModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LBean createLBean() {
		LBeanImpl lBean = new LBeanImpl();
		return lBean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntity createLEntity() {
		LEntityImpl lEntity = new LEntityImpl();
		return lEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntitySuperIndex createLEntitySuperIndex() {
		LEntitySuperIndexImpl lEntitySuperIndex = new LEntitySuperIndexImpl();
		return lEntitySuperIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LNestedFeature createLNestedFeature() {
		LNestedFeatureImpl lNestedFeature = new LNestedFeatureImpl();
		return lNestedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityPersistenceInfo createLEntityPersistenceInfo() {
		LEntityPersistenceInfoImpl lEntityPersistenceInfo = new LEntityPersistenceInfoImpl();
		return lEntityPersistenceInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityColumnPersistenceInfo createLEntityColumnPersistenceInfo() {
		LEntityColumnPersistenceInfoImpl lEntityColumnPersistenceInfo = new LEntityColumnPersistenceInfoImpl();
		return lEntityColumnPersistenceInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTablePerClassStrategy createLTablePerClassStrategy() {
		LTablePerClassStrategyImpl lTablePerClassStrategy = new LTablePerClassStrategyImpl();
		return lTablePerClassStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTablePerSubclassStrategy createLTablePerSubclassStrategy() {
		LTablePerSubclassStrategyImpl lTablePerSubclassStrategy = new LTablePerSubclassStrategyImpl();
		return lTablePerSubclassStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature createLEntityFeature() {
		LEntityFeatureImpl lEntityFeature = new LEntityFeatureImpl();
		return lEntityFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LKanbanStateDetail createLKanbanStateDetail() {
		LKanbanStateDetailImpl lKanbanStateDetail = new LKanbanStateDetailImpl();
		return lKanbanStateDetail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LKanbanEnumInfo createLKanbanEnumInfo() {
		LKanbanEnumInfoImpl lKanbanEnumInfo = new LKanbanEnumInfoImpl();
		return lKanbanEnumInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityAttribute createLEntityAttribute() {
		LEntityAttributeImpl lEntityAttribute = new LEntityAttributeImpl();
		return lEntityAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityReference createLEntityReference() {
		LEntityReferenceImpl lEntityReference = new LEntityReferenceImpl();
		return lEntityReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LBeanFeature createLBeanFeature() {
		LBeanFeatureImpl lBeanFeature = new LBeanFeatureImpl();
		return lBeanFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LBeanAttribute createLBeanAttribute() {
		LBeanAttributeImpl lBeanAttribute = new LBeanAttributeImpl();
		return lBeanAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LBeanReference createLBeanReference() {
		LBeanReferenceImpl lBeanReference = new LBeanReferenceImpl();
		return lBeanReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LIndex createLIndex() {
		LIndexImpl lIndex = new LIndexImpl();
		return lIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LOperation createLOperation() {
		LOperationImpl lOperation = new LOperationImpl();
		return lOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDiscriminatorType createLDiscriminatorTypeFromString(EDataType eDataType, String initialValue) {
		LDiscriminatorType result = LDiscriminatorType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLDiscriminatorTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalEObject createInternalEObjectFromString(EDataType eDataType, String initialValue) {
		return (InternalEObject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInternalEObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LOperation> createOperationsListFromString(EDataType eDataType, String initialValue) {
		return (List<LOperation>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationsListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LEntityFeature> createEntityFeatureListFromString(EDataType eDataType, String initialValue) {
		return (List<LEntityFeature>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEntityFeatureListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LEntityReference> createEntityReferenceListFromString(EDataType eDataType, String initialValue) {
		return (List<LEntityReference>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEntityReferenceListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LEntityAttribute> createEntityAttributeListFromString(EDataType eDataType, String initialValue) {
		return (List<LEntityAttribute>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEntityAttributeListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LBeanFeature> createBeanFeatureListFromString(EDataType eDataType, String initialValue) {
		return (List<LBeanFeature>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBeanFeatureListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LBeanReference> createBeanReferenceListFromString(EDataType eDataType, String initialValue) {
		return (List<LBeanReference>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBeanReferenceListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LBeanAttribute> createBeanAttributeListFromString(EDataType eDataType, String initialValue) {
		return (List<LBeanAttribute>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBeanAttributeListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPEntityPackage getOSBPEntityPackage() {
		return (OSBPEntityPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OSBPEntityPackage getPackage() {
		return OSBPEntityPackage.eINSTANCE;
	}

} //OSBPEntityFactoryImpl
