/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity.impl;

import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LBeanFeature;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LDiscriminatorType;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityColumnPersistenceInfo;
import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;
import org.eclipse.osbp.dsl.semantic.entity.LEntityInheritanceStrategy;
import org.eclipse.osbp.dsl.semantic.entity.LEntityModel;
import org.eclipse.osbp.dsl.semantic.entity.LEntityPersistenceInfo;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntitySuperIndex;
import org.eclipse.osbp.dsl.semantic.entity.LIndex;
import org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo;
import org.eclipse.osbp.dsl.semantic.entity.LKanbanStateDetail;
import org.eclipse.osbp.dsl.semantic.entity.LNestedFeature;
import org.eclipse.osbp.dsl.semantic.entity.LOperation;
import org.eclipse.osbp.dsl.semantic.entity.LTablePerClassStrategy;
import org.eclipse.osbp.dsl.semantic.entity.LTablePerSubclassStrategy;
import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityFactory;
import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage;
import org.eclipse.xtext.xtype.XtypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OSBPEntityPackageImpl extends EPackageImpl implements OSBPEntityPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lEntityModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lBeanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lEntitySuperIndexEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lNestedFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lEntityPersistenceInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lEntityColumnPersistenceInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lEntityInheritanceStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lTablePerClassStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lTablePerSubclassStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lEntityFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lKanbanStateDetailEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lKanbanEnumInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lEntityAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lEntityReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lBeanFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lBeanAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lBeanReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lIndexEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum lDiscriminatorTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType internalEObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType operationsListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType entityFeatureListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType entityReferenceListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType entityAttributeListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType beanFeatureListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType beanReferenceListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType beanAttributeListEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OSBPEntityPackageImpl() {
		super(eNS_URI, OSBPEntityFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OSBPEntityPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OSBPEntityPackage init() {
		if (isInited) return (OSBPEntityPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPEntityPackage.eNS_URI);

		// Obtain or create and register package
		OSBPEntityPackageImpl theOSBPEntityPackage = (OSBPEntityPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OSBPEntityPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OSBPEntityPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		OSBPTypesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOSBPEntityPackage.createPackageContents();

		// Initialize created meta-data
		theOSBPEntityPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOSBPEntityPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OSBPEntityPackage.eNS_URI, theOSBPEntityPackage);
		return theOSBPEntityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLEntityModel() {
		return lEntityModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntityModel_ImportSection() {
		return (EReference)lEntityModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntityModel_Packages() {
		return (EReference)lEntityModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLBean() {
		return lBeanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLBean_Features() {
		return (EReference)lBeanEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLBean_SuperType() {
		return (EReference)lBeanEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLBean_SubTypes() {
		return (EReference)lBeanEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLBean_BeanOnTab() {
		return (EAttribute)lBeanEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLBean__GetOperations() {
		return lBeanEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLBean__GetReferences() {
		return lBeanEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLBean__GetAttributes() {
		return lBeanEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLBean__GetAllFeatures() {
		return lBeanEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLBean__GetAllAttributes() {
		return lBeanEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLBean__GetAllReferences() {
		return lBeanEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLBean__CollectAllOSBPFeatures__LBean_List() {
		return lBeanEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLEntity() {
		return lEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntity_Cacheable() {
		return (EAttribute)lEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntity_Historized() {
		return (EAttribute)lEntityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntity_Timedependent() {
		return (EAttribute)lEntityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntity_TimedependentDateType() {
		return (EAttribute)lEntityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntity_MappedSuperclass() {
		return (EAttribute)lEntityEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntity_PersistenceInfo() {
		return (EReference)lEntityEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntity_InheritanceStrategy() {
		return (EReference)lEntityEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntity_Features() {
		return (EReference)lEntityEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntity_Indexes() {
		return (EReference)lEntityEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntity_SuperType() {
		return (EReference)lEntityEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntity_SubTypes() {
		return (EReference)lEntityEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntity_PersistenceUnit() {
		return (EAttribute)lEntityEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntity_SuperIndex() {
		return (EReference)lEntityEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntity_StateClass() {
		return (EReference)lEntityEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetOperations() {
		return lEntityEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetReferences() {
		return lEntityEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetAttributes() {
		return lEntityEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetAllFeatures() {
		return lEntityEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetAllAttributes() {
		return lEntityEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetAllReferences() {
		return lEntityEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__CollectAllOSBPFeatures__LEntity_List() {
		return lEntityEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__CollectNormalOSBPFeatures__LEntity_List() {
		return lEntityEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetPrimaryKeyAttribute() {
		return lEntityEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetIdAttributeName() {
		return lEntityEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetIdAttributeType() {
		return lEntityEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetCurrentAttribute() {
		return lEntityEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetVersionAttribute() {
		return lEntityEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetVersionAttributeName() {
		return lEntityEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetVersionAttributeType() {
		return lEntityEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__GetNormalFeatures() {
		return lEntityEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__IsHistorizedWithParent() {
		return lEntityEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__IsTimedependentWithParent() {
		return lEntityEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntity__IsHistorizedOrTimedependentWithParent() {
		return lEntityEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLEntitySuperIndex() {
		return lEntitySuperIndexEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntitySuperIndex_Name() {
		return (EAttribute)lEntitySuperIndexEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntitySuperIndex_Attributes() {
		return (EReference)lEntitySuperIndexEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLNestedFeature() {
		return lNestedFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLNestedFeature_Feature() {
		return (EReference)lNestedFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLNestedFeature_Tail() {
		return (EReference)lNestedFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLEntityPersistenceInfo() {
		return lEntityPersistenceInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntityPersistenceInfo_SchemaName() {
		return (EAttribute)lEntityPersistenceInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntityPersistenceInfo_TableName() {
		return (EAttribute)lEntityPersistenceInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLEntityColumnPersistenceInfo() {
		return lEntityColumnPersistenceInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntityColumnPersistenceInfo_ColumnName() {
		return (EAttribute)lEntityColumnPersistenceInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLEntityInheritanceStrategy() {
		return lEntityInheritanceStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLTablePerClassStrategy() {
		return lTablePerClassStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLTablePerClassStrategy_DiscriminatorColumn() {
		return (EAttribute)lTablePerClassStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLTablePerClassStrategy_DiscriminatorType() {
		return (EAttribute)lTablePerClassStrategyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLTablePerClassStrategy_DiscriminatorValue() {
		return (EAttribute)lTablePerClassStrategyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLTablePerSubclassStrategy() {
		return lTablePerSubclassStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLTablePerSubclassStrategy_DiscriminatorColumn() {
		return (EAttribute)lTablePerSubclassStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLTablePerSubclassStrategy_DiscriminatorType() {
		return (EAttribute)lTablePerSubclassStrategyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLTablePerSubclassStrategy_DiscriminatorValue() {
		return (EAttribute)lTablePerSubclassStrategyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLEntityFeature() {
		return lEntityFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntityFeature_PersistenceInfo() {
		return (EReference)lEntityFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntityFeature__GetEntity() {
		return lEntityFeatureEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLKanbanStateDetail() {
		return lKanbanStateDetailEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLKanbanStateDetail_Infos() {
		return (EReference)lKanbanStateDetailEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLKanbanEnumInfo() {
		return lKanbanEnumInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLKanbanEnumInfo_Literal() {
		return (EReference)lKanbanEnumInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLKanbanEnumInfo_I18nKey() {
		return (EAttribute)lKanbanEnumInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLEntityAttribute() {
		return lEntityAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntityAttribute_AsKanbanState() {
		return (EAttribute)lEntityAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntityAttribute_OnKanbanCard() {
		return (EAttribute)lEntityAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntityAttribute_OnKanbanCardStates() {
		return (EReference)lEntityAttributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntityAttribute_AsKanbanOrdering() {
		return (EAttribute)lEntityAttributeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntityAttribute_DecentKanbanOrder() {
		return (EAttribute)lEntityAttributeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntityAttribute_Opposite() {
		return (EReference)lEntityAttributeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntityAttribute_TypedName() {
		return (EAttribute)lEntityAttributeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLEntityReference() {
		return lEntityReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntityReference_Type() {
		return (EReference)lEntityReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntityReference_Opposite() {
		return (EReference)lEntityReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLEntityReference_ResultFilters() {
		return (EReference)lEntityReferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLEntityReference_FilterDepth() {
		return (EAttribute)lEntityReferenceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLEntityReference__IsCascading() {
		return lEntityReferenceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLBeanFeature() {
		return lBeanFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLBeanFeature__GetBean() {
		return lBeanFeatureEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLBeanAttribute() {
		return lBeanAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLBeanAttribute_TypedName() {
		return (EAttribute)lBeanAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLBeanReference() {
		return lBeanReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLBeanReference_Type() {
		return (EReference)lBeanReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLBeanReference_Opposite() {
		return (EReference)lBeanReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLBeanReference_ResultFilters() {
		return (EReference)lBeanReferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getLBeanReference__IsCascading() {
		return lBeanReferenceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLIndex() {
		return lIndexEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLIndex_Unique() {
		return (EAttribute)lIndexEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLIndex_Name() {
		return (EAttribute)lIndexEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLIndex_Features() {
		return (EReference)lIndexEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLOperation() {
		return lOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLDiscriminatorType() {
		return lDiscriminatorTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInternalEObject() {
		return internalEObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getOperationsList() {
		return operationsListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEntityFeatureList() {
		return entityFeatureListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEntityReferenceList() {
		return entityReferenceListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEntityAttributeList() {
		return entityAttributeListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getBeanFeatureList() {
		return beanFeatureListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getBeanReferenceList() {
		return beanReferenceListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getBeanAttributeList() {
		return beanAttributeListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPEntityFactory getOSBPEntityFactory() {
		return (OSBPEntityFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		lEntityModelEClass = createEClass(LENTITY_MODEL);
		createEReference(lEntityModelEClass, LENTITY_MODEL__IMPORT_SECTION);
		createEReference(lEntityModelEClass, LENTITY_MODEL__PACKAGES);

		lBeanEClass = createEClass(LBEAN);
		createEReference(lBeanEClass, LBEAN__FEATURES);
		createEReference(lBeanEClass, LBEAN__SUPER_TYPE);
		createEReference(lBeanEClass, LBEAN__SUB_TYPES);
		createEAttribute(lBeanEClass, LBEAN__BEAN_ON_TAB);
		createEOperation(lBeanEClass, LBEAN___GET_OPERATIONS);
		createEOperation(lBeanEClass, LBEAN___GET_REFERENCES);
		createEOperation(lBeanEClass, LBEAN___GET_ATTRIBUTES);
		createEOperation(lBeanEClass, LBEAN___GET_ALL_FEATURES);
		createEOperation(lBeanEClass, LBEAN___GET_ALL_ATTRIBUTES);
		createEOperation(lBeanEClass, LBEAN___GET_ALL_REFERENCES);
		createEOperation(lBeanEClass, LBEAN___COLLECT_ALL_OSBP_FEATURES__LBEAN_LIST);

		lEntityEClass = createEClass(LENTITY);
		createEAttribute(lEntityEClass, LENTITY__CACHEABLE);
		createEAttribute(lEntityEClass, LENTITY__HISTORIZED);
		createEAttribute(lEntityEClass, LENTITY__TIMEDEPENDENT);
		createEAttribute(lEntityEClass, LENTITY__TIMEDEPENDENT_DATE_TYPE);
		createEAttribute(lEntityEClass, LENTITY__MAPPED_SUPERCLASS);
		createEReference(lEntityEClass, LENTITY__PERSISTENCE_INFO);
		createEReference(lEntityEClass, LENTITY__INHERITANCE_STRATEGY);
		createEReference(lEntityEClass, LENTITY__FEATURES);
		createEReference(lEntityEClass, LENTITY__INDEXES);
		createEReference(lEntityEClass, LENTITY__SUPER_TYPE);
		createEReference(lEntityEClass, LENTITY__SUB_TYPES);
		createEAttribute(lEntityEClass, LENTITY__PERSISTENCE_UNIT);
		createEReference(lEntityEClass, LENTITY__SUPER_INDEX);
		createEReference(lEntityEClass, LENTITY__STATE_CLASS);
		createEOperation(lEntityEClass, LENTITY___GET_OPERATIONS);
		createEOperation(lEntityEClass, LENTITY___GET_REFERENCES);
		createEOperation(lEntityEClass, LENTITY___GET_ATTRIBUTES);
		createEOperation(lEntityEClass, LENTITY___GET_ALL_FEATURES);
		createEOperation(lEntityEClass, LENTITY___GET_ALL_ATTRIBUTES);
		createEOperation(lEntityEClass, LENTITY___GET_ALL_REFERENCES);
		createEOperation(lEntityEClass, LENTITY___COLLECT_ALL_OSBP_FEATURES__LENTITY_LIST);
		createEOperation(lEntityEClass, LENTITY___COLLECT_NORMAL_OSBP_FEATURES__LENTITY_LIST);
		createEOperation(lEntityEClass, LENTITY___GET_PRIMARY_KEY_ATTRIBUTE);
		createEOperation(lEntityEClass, LENTITY___GET_ID_ATTRIBUTE_NAME);
		createEOperation(lEntityEClass, LENTITY___GET_ID_ATTRIBUTE_TYPE);
		createEOperation(lEntityEClass, LENTITY___GET_CURRENT_ATTRIBUTE);
		createEOperation(lEntityEClass, LENTITY___GET_VERSION_ATTRIBUTE);
		createEOperation(lEntityEClass, LENTITY___GET_VERSION_ATTRIBUTE_NAME);
		createEOperation(lEntityEClass, LENTITY___GET_VERSION_ATTRIBUTE_TYPE);
		createEOperation(lEntityEClass, LENTITY___GET_NORMAL_FEATURES);
		createEOperation(lEntityEClass, LENTITY___IS_HISTORIZED_WITH_PARENT);
		createEOperation(lEntityEClass, LENTITY___IS_TIMEDEPENDENT_WITH_PARENT);
		createEOperation(lEntityEClass, LENTITY___IS_HISTORIZED_OR_TIMEDEPENDENT_WITH_PARENT);

		lEntitySuperIndexEClass = createEClass(LENTITY_SUPER_INDEX);
		createEAttribute(lEntitySuperIndexEClass, LENTITY_SUPER_INDEX__NAME);
		createEReference(lEntitySuperIndexEClass, LENTITY_SUPER_INDEX__ATTRIBUTES);

		lNestedFeatureEClass = createEClass(LNESTED_FEATURE);
		createEReference(lNestedFeatureEClass, LNESTED_FEATURE__FEATURE);
		createEReference(lNestedFeatureEClass, LNESTED_FEATURE__TAIL);

		lEntityPersistenceInfoEClass = createEClass(LENTITY_PERSISTENCE_INFO);
		createEAttribute(lEntityPersistenceInfoEClass, LENTITY_PERSISTENCE_INFO__SCHEMA_NAME);
		createEAttribute(lEntityPersistenceInfoEClass, LENTITY_PERSISTENCE_INFO__TABLE_NAME);

		lEntityColumnPersistenceInfoEClass = createEClass(LENTITY_COLUMN_PERSISTENCE_INFO);
		createEAttribute(lEntityColumnPersistenceInfoEClass, LENTITY_COLUMN_PERSISTENCE_INFO__COLUMN_NAME);

		lEntityInheritanceStrategyEClass = createEClass(LENTITY_INHERITANCE_STRATEGY);

		lTablePerClassStrategyEClass = createEClass(LTABLE_PER_CLASS_STRATEGY);
		createEAttribute(lTablePerClassStrategyEClass, LTABLE_PER_CLASS_STRATEGY__DISCRIMINATOR_COLUMN);
		createEAttribute(lTablePerClassStrategyEClass, LTABLE_PER_CLASS_STRATEGY__DISCRIMINATOR_TYPE);
		createEAttribute(lTablePerClassStrategyEClass, LTABLE_PER_CLASS_STRATEGY__DISCRIMINATOR_VALUE);

		lTablePerSubclassStrategyEClass = createEClass(LTABLE_PER_SUBCLASS_STRATEGY);
		createEAttribute(lTablePerSubclassStrategyEClass, LTABLE_PER_SUBCLASS_STRATEGY__DISCRIMINATOR_COLUMN);
		createEAttribute(lTablePerSubclassStrategyEClass, LTABLE_PER_SUBCLASS_STRATEGY__DISCRIMINATOR_TYPE);
		createEAttribute(lTablePerSubclassStrategyEClass, LTABLE_PER_SUBCLASS_STRATEGY__DISCRIMINATOR_VALUE);

		lEntityFeatureEClass = createEClass(LENTITY_FEATURE);
		createEReference(lEntityFeatureEClass, LENTITY_FEATURE__PERSISTENCE_INFO);
		createEOperation(lEntityFeatureEClass, LENTITY_FEATURE___GET_ENTITY);

		lKanbanStateDetailEClass = createEClass(LKANBAN_STATE_DETAIL);
		createEReference(lKanbanStateDetailEClass, LKANBAN_STATE_DETAIL__INFOS);

		lKanbanEnumInfoEClass = createEClass(LKANBAN_ENUM_INFO);
		createEReference(lKanbanEnumInfoEClass, LKANBAN_ENUM_INFO__LITERAL);
		createEAttribute(lKanbanEnumInfoEClass, LKANBAN_ENUM_INFO__I18N_KEY);

		lEntityAttributeEClass = createEClass(LENTITY_ATTRIBUTE);
		createEAttribute(lEntityAttributeEClass, LENTITY_ATTRIBUTE__AS_KANBAN_STATE);
		createEAttribute(lEntityAttributeEClass, LENTITY_ATTRIBUTE__ON_KANBAN_CARD);
		createEReference(lEntityAttributeEClass, LENTITY_ATTRIBUTE__ON_KANBAN_CARD_STATES);
		createEAttribute(lEntityAttributeEClass, LENTITY_ATTRIBUTE__AS_KANBAN_ORDERING);
		createEAttribute(lEntityAttributeEClass, LENTITY_ATTRIBUTE__DECENT_KANBAN_ORDER);
		createEReference(lEntityAttributeEClass, LENTITY_ATTRIBUTE__OPPOSITE);
		createEAttribute(lEntityAttributeEClass, LENTITY_ATTRIBUTE__TYPED_NAME);

		lEntityReferenceEClass = createEClass(LENTITY_REFERENCE);
		createEReference(lEntityReferenceEClass, LENTITY_REFERENCE__TYPE);
		createEReference(lEntityReferenceEClass, LENTITY_REFERENCE__OPPOSITE);
		createEReference(lEntityReferenceEClass, LENTITY_REFERENCE__RESULT_FILTERS);
		createEAttribute(lEntityReferenceEClass, LENTITY_REFERENCE__FILTER_DEPTH);
		createEOperation(lEntityReferenceEClass, LENTITY_REFERENCE___IS_CASCADING);

		lBeanFeatureEClass = createEClass(LBEAN_FEATURE);
		createEOperation(lBeanFeatureEClass, LBEAN_FEATURE___GET_BEAN);

		lBeanAttributeEClass = createEClass(LBEAN_ATTRIBUTE);
		createEAttribute(lBeanAttributeEClass, LBEAN_ATTRIBUTE__TYPED_NAME);

		lBeanReferenceEClass = createEClass(LBEAN_REFERENCE);
		createEReference(lBeanReferenceEClass, LBEAN_REFERENCE__TYPE);
		createEReference(lBeanReferenceEClass, LBEAN_REFERENCE__OPPOSITE);
		createEReference(lBeanReferenceEClass, LBEAN_REFERENCE__RESULT_FILTERS);
		createEOperation(lBeanReferenceEClass, LBEAN_REFERENCE___IS_CASCADING);

		lIndexEClass = createEClass(LINDEX);
		createEAttribute(lIndexEClass, LINDEX__UNIQUE);
		createEAttribute(lIndexEClass, LINDEX__NAME);
		createEReference(lIndexEClass, LINDEX__FEATURES);

		lOperationEClass = createEClass(LOPERATION);

		// Create enums
		lDiscriminatorTypeEEnum = createEEnum(LDISCRIMINATOR_TYPE);

		// Create data types
		internalEObjectEDataType = createEDataType(INTERNAL_EOBJECT);
		operationsListEDataType = createEDataType(OPERATIONS_LIST);
		entityFeatureListEDataType = createEDataType(ENTITY_FEATURE_LIST);
		entityReferenceListEDataType = createEDataType(ENTITY_REFERENCE_LIST);
		entityAttributeListEDataType = createEDataType(ENTITY_ATTRIBUTE_LIST);
		beanFeatureListEDataType = createEDataType(BEAN_FEATURE_LIST);
		beanReferenceListEDataType = createEDataType(BEAN_REFERENCE_LIST);
		beanAttributeListEDataType = createEDataType(BEAN_ATTRIBUTE_LIST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XtypePackage theXtypePackage = (XtypePackage)EPackage.Registry.INSTANCE.getEPackage(XtypePackage.eNS_URI);
		OSBPTypesPackage theOSBPTypesPackage = (OSBPTypesPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPTypesPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		lBeanEClass.getESuperTypes().add(theOSBPTypesPackage.getLClass());
		lBeanEClass.getESuperTypes().add(theOSBPTypesPackage.getLScalarType());
		lBeanEClass.getESuperTypes().add(theOSBPTypesPackage.getLFeaturesHolder());
		lEntityEClass.getESuperTypes().add(theOSBPTypesPackage.getLClass());
		lEntityEClass.getESuperTypes().add(theOSBPTypesPackage.getLFeaturesHolder());
		lEntitySuperIndexEClass.getESuperTypes().add(theOSBPTypesPackage.getLLazyResolver());
		lNestedFeatureEClass.getESuperTypes().add(theOSBPTypesPackage.getLLazyResolver());
		lEntityPersistenceInfoEClass.getESuperTypes().add(theOSBPTypesPackage.getLLazyResolver());
		lEntityColumnPersistenceInfoEClass.getESuperTypes().add(theOSBPTypesPackage.getLLazyResolver());
		lEntityInheritanceStrategyEClass.getESuperTypes().add(theOSBPTypesPackage.getLLazyResolver());
		lTablePerClassStrategyEClass.getESuperTypes().add(this.getLEntityInheritanceStrategy());
		lTablePerSubclassStrategyEClass.getESuperTypes().add(this.getLEntityInheritanceStrategy());
		lEntityFeatureEClass.getESuperTypes().add(theOSBPTypesPackage.getLFeature());
		lEntityAttributeEClass.getESuperTypes().add(this.getLEntityFeature());
		lEntityAttributeEClass.getESuperTypes().add(theOSBPTypesPackage.getLAttribute());
		lEntityReferenceEClass.getESuperTypes().add(this.getLEntityFeature());
		lEntityReferenceEClass.getESuperTypes().add(theOSBPTypesPackage.getLReference());
		lBeanFeatureEClass.getESuperTypes().add(theOSBPTypesPackage.getLFeature());
		lBeanAttributeEClass.getESuperTypes().add(this.getLBeanFeature());
		lBeanAttributeEClass.getESuperTypes().add(theOSBPTypesPackage.getLAttribute());
		lBeanReferenceEClass.getESuperTypes().add(this.getLBeanFeature());
		lBeanReferenceEClass.getESuperTypes().add(theOSBPTypesPackage.getLReference());
		lOperationEClass.getESuperTypes().add(theOSBPTypesPackage.getLOperation());
		lOperationEClass.getESuperTypes().add(this.getLBeanFeature());
		lOperationEClass.getESuperTypes().add(this.getLEntityFeature());

		// Initialize classes, features, and operations; add parameters
		initEClass(lEntityModelEClass, LEntityModel.class, "LEntityModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLEntityModel_ImportSection(), theXtypePackage.getXImportSection(), null, "importSection", null, 0, 1, LEntityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntityModel_Packages(), theOSBPTypesPackage.getLTypedPackage(), null, "packages", null, 0, -1, LEntityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lBeanEClass, LBean.class, "LBean", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLBean_Features(), this.getLBeanFeature(), null, "features", null, 0, -1, LBean.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLBean_SuperType(), this.getLBean(), this.getLBean_SubTypes(), "superType", null, 0, 1, LBean.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLBean_SubTypes(), this.getLBean(), this.getLBean_SuperType(), "subTypes", null, 0, -1, LBean.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLBean_BeanOnTab(), theEcorePackage.getEBoolean(), "beanOnTab", null, 0, 1, LBean.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getLBean__GetOperations(), this.getOperationsList(), "getOperations", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLBean__GetReferences(), this.getBeanReferenceList(), "getReferences", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLBean__GetAttributes(), this.getBeanAttributeList(), "getAttributes", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLBean__GetAllFeatures(), this.getBeanFeatureList(), "getAllFeatures", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLBean__GetAllAttributes(), this.getBeanAttributeList(), "getAllAttributes", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLBean__GetAllReferences(), this.getBeanReferenceList(), "getAllReferences", 0, 1, !IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getLBean__CollectAllOSBPFeatures__LBean_List(), null, "collectAllOSBPFeatures", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLBean(), "current", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getBeanFeatureList(), "result", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lEntityEClass, LEntity.class, "LEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLEntity_Cacheable(), theEcorePackage.getEBoolean(), "cacheable", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntity_Historized(), theEcorePackage.getEBoolean(), "historized", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntity_Timedependent(), theEcorePackage.getEBoolean(), "timedependent", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntity_TimedependentDateType(), theOSBPTypesPackage.getLDateType(), "timedependentDateType", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntity_MappedSuperclass(), theEcorePackage.getEBoolean(), "mappedSuperclass", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntity_PersistenceInfo(), this.getLEntityPersistenceInfo(), null, "persistenceInfo", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntity_InheritanceStrategy(), this.getLEntityInheritanceStrategy(), null, "inheritanceStrategy", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntity_Features(), this.getLEntityFeature(), null, "features", null, 0, -1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntity_Indexes(), this.getLIndex(), null, "indexes", null, 0, -1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntity_SuperType(), this.getLEntity(), this.getLEntity_SubTypes(), "superType", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntity_SubTypes(), this.getLEntity(), this.getLEntity_SuperType(), "subTypes", null, 0, -1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntity_PersistenceUnit(), theEcorePackage.getEString(), "persistenceUnit", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntity_SuperIndex(), this.getLEntitySuperIndex(), null, "superIndex", null, 0, -1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntity_StateClass(), theOSBPTypesPackage.getLStateClass(), null, "stateClass", null, 0, 1, LEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getLEntity__GetOperations(), this.getOperationsList(), "getOperations", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetReferences(), this.getEntityReferenceList(), "getReferences", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetAttributes(), this.getEntityAttributeList(), "getAttributes", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetAllFeatures(), this.getEntityFeatureList(), "getAllFeatures", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetAllAttributes(), this.getEntityAttributeList(), "getAllAttributes", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetAllReferences(), this.getEntityReferenceList(), "getAllReferences", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getLEntity__CollectAllOSBPFeatures__LEntity_List(), null, "collectAllOSBPFeatures", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLEntity(), "current", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEntityFeatureList(), "result", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getLEntity__CollectNormalOSBPFeatures__LEntity_List(), null, "collectNormalOSBPFeatures", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLEntity(), "current", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEntityFeatureList(), "result", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetPrimaryKeyAttribute(), this.getLEntityAttribute(), "getPrimaryKeyAttribute", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetIdAttributeName(), theEcorePackage.getEString(), "getIdAttributeName", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetIdAttributeType(), theOSBPTypesPackage.getLScalarType(), "getIdAttributeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetCurrentAttribute(), this.getLEntityAttribute(), "getCurrentAttribute", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetVersionAttribute(), this.getLEntityAttribute(), "getVersionAttribute", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetVersionAttributeName(), theEcorePackage.getEString(), "getVersionAttributeName", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetVersionAttributeType(), theOSBPTypesPackage.getLScalarType(), "getVersionAttributeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__GetNormalFeatures(), this.getEntityFeatureList(), "getNormalFeatures", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__IsHistorizedWithParent(), theEcorePackage.getEBoolean(), "isHistorizedWithParent", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__IsTimedependentWithParent(), theEcorePackage.getEBoolean(), "isTimedependentWithParent", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getLEntity__IsHistorizedOrTimedependentWithParent(), theEcorePackage.getEBoolean(), "isHistorizedOrTimedependentWithParent", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lEntitySuperIndexEClass, LEntitySuperIndex.class, "LEntitySuperIndex", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLEntitySuperIndex_Name(), theEcorePackage.getEString(), "name", null, 0, 1, LEntitySuperIndex.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntitySuperIndex_Attributes(), this.getLNestedFeature(), null, "attributes", null, 0, -1, LEntitySuperIndex.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lNestedFeatureEClass, LNestedFeature.class, "LNestedFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLNestedFeature_Feature(), theOSBPTypesPackage.getLFeature(), null, "feature", null, 0, 1, LNestedFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLNestedFeature_Tail(), this.getLNestedFeature(), null, "tail", null, 0, 1, LNestedFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lEntityPersistenceInfoEClass, LEntityPersistenceInfo.class, "LEntityPersistenceInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLEntityPersistenceInfo_SchemaName(), theEcorePackage.getEString(), "schemaName", null, 0, 1, LEntityPersistenceInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntityPersistenceInfo_TableName(), theEcorePackage.getEString(), "tableName", null, 0, 1, LEntityPersistenceInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lEntityColumnPersistenceInfoEClass, LEntityColumnPersistenceInfo.class, "LEntityColumnPersistenceInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLEntityColumnPersistenceInfo_ColumnName(), theEcorePackage.getEString(), "columnName", null, 0, 1, LEntityColumnPersistenceInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lEntityInheritanceStrategyEClass, LEntityInheritanceStrategy.class, "LEntityInheritanceStrategy", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(lTablePerClassStrategyEClass, LTablePerClassStrategy.class, "LTablePerClassStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLTablePerClassStrategy_DiscriminatorColumn(), theEcorePackage.getEString(), "discriminatorColumn", null, 0, 1, LTablePerClassStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLTablePerClassStrategy_DiscriminatorType(), this.getLDiscriminatorType(), "discriminatorType", null, 0, 1, LTablePerClassStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLTablePerClassStrategy_DiscriminatorValue(), theEcorePackage.getEString(), "discriminatorValue", null, 0, 1, LTablePerClassStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lTablePerSubclassStrategyEClass, LTablePerSubclassStrategy.class, "LTablePerSubclassStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLTablePerSubclassStrategy_DiscriminatorColumn(), theEcorePackage.getEString(), "discriminatorColumn", null, 0, 1, LTablePerSubclassStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLTablePerSubclassStrategy_DiscriminatorType(), this.getLDiscriminatorType(), "discriminatorType", null, 0, 1, LTablePerSubclassStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLTablePerSubclassStrategy_DiscriminatorValue(), theEcorePackage.getEString(), "discriminatorValue", null, 0, 1, LTablePerSubclassStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lEntityFeatureEClass, LEntityFeature.class, "LEntityFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLEntityFeature_PersistenceInfo(), this.getLEntityColumnPersistenceInfo(), null, "persistenceInfo", null, 0, 1, LEntityFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getLEntityFeature__GetEntity(), this.getLEntity(), "getEntity", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lKanbanStateDetailEClass, LKanbanStateDetail.class, "LKanbanStateDetail", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLKanbanStateDetail_Infos(), this.getLKanbanEnumInfo(), null, "infos", null, 0, -1, LKanbanStateDetail.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lKanbanEnumInfoEClass, LKanbanEnumInfo.class, "LKanbanEnumInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLKanbanEnumInfo_Literal(), theOSBPTypesPackage.getLEnumLiteral(), null, "literal", null, 0, 1, LKanbanEnumInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLKanbanEnumInfo_I18nKey(), theEcorePackage.getEString(), "i18nKey", null, 0, 1, LKanbanEnumInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lEntityAttributeEClass, LEntityAttribute.class, "LEntityAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLEntityAttribute_AsKanbanState(), theEcorePackage.getEBoolean(), "asKanbanState", null, 0, 1, LEntityAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntityAttribute_OnKanbanCard(), theEcorePackage.getEBoolean(), "onKanbanCard", null, 0, 1, LEntityAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntityAttribute_OnKanbanCardStates(), this.getLKanbanStateDetail(), null, "onKanbanCardStates", null, 0, 1, LEntityAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntityAttribute_AsKanbanOrdering(), theEcorePackage.getEBoolean(), "asKanbanOrdering", null, 0, 1, LEntityAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntityAttribute_DecentKanbanOrder(), theEcorePackage.getEBoolean(), "decentKanbanOrder", null, 0, 1, LEntityAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntityAttribute_Opposite(), this.getLBeanReference(), null, "opposite", null, 0, 1, LEntityAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntityAttribute_TypedName(), theEcorePackage.getEString(), "typedName", null, 0, 1, LEntityAttribute.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(lEntityReferenceEClass, LEntityReference.class, "LEntityReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLEntityReference_Type(), this.getLEntity(), null, "type", null, 0, 1, LEntityReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntityReference_Opposite(), this.getLEntityReference(), null, "opposite", null, 0, 1, LEntityReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLEntityReference_ResultFilters(), theOSBPTypesPackage.getLResultFilters(), null, "resultFilters", null, 0, 1, LEntityReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLEntityReference_FilterDepth(), theEcorePackage.getEInt(), "filterDepth", null, 0, 1, LEntityReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getLEntityReference__IsCascading(), theEcorePackage.getEBoolean(), "isCascading", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lBeanFeatureEClass, LBeanFeature.class, "LBeanFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getLBeanFeature__GetBean(), this.getLBean(), "getBean", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lBeanAttributeEClass, LBeanAttribute.class, "LBeanAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLBeanAttribute_TypedName(), theEcorePackage.getEString(), "typedName", null, 0, 1, LBeanAttribute.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(lBeanReferenceEClass, LBeanReference.class, "LBeanReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLBeanReference_Type(), theOSBPTypesPackage.getLType(), null, "type", null, 0, 1, LBeanReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLBeanReference_Opposite(), theOSBPTypesPackage.getLFeature(), null, "opposite", null, 0, 1, LBeanReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLBeanReference_ResultFilters(), theOSBPTypesPackage.getLResultFilters(), null, "resultFilters", null, 0, 1, LBeanReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getLBeanReference__IsCascading(), theEcorePackage.getEBoolean(), "isCascading", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lIndexEClass, LIndex.class, "LIndex", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLIndex_Unique(), theEcorePackage.getEBoolean(), "unique", null, 0, 1, LIndex.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLIndex_Name(), theEcorePackage.getEString(), "name", null, 0, 1, LIndex.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLIndex_Features(), this.getLEntityFeature(), null, "features", null, 0, -1, LIndex.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lOperationEClass, LOperation.class, "LOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(lDiscriminatorTypeEEnum, LDiscriminatorType.class, "LDiscriminatorType");
		addEEnumLiteral(lDiscriminatorTypeEEnum, LDiscriminatorType.INHERIT);
		addEEnumLiteral(lDiscriminatorTypeEEnum, LDiscriminatorType.STRING);
		addEEnumLiteral(lDiscriminatorTypeEEnum, LDiscriminatorType.CHAR);
		addEEnumLiteral(lDiscriminatorTypeEEnum, LDiscriminatorType.INTEGER);

		// Initialize data types
		initEDataType(internalEObjectEDataType, InternalEObject.class, "InternalEObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(operationsListEDataType, List.class, "OperationsList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.entity.LOperation>");
		initEDataType(entityFeatureListEDataType, List.class, "EntityFeatureList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.entity.LEntityFeature>");
		initEDataType(entityReferenceListEDataType, List.class, "EntityReferenceList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.entity.LEntityReference>");
		initEDataType(entityAttributeListEDataType, List.class, "EntityAttributeList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute>");
		initEDataType(beanFeatureListEDataType, List.class, "BeanFeatureList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.entity.LBeanFeature>");
		initEDataType(beanReferenceListEDataType, List.class, "BeanReferenceList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.entity.LBeanReference>");
		initEDataType(beanAttributeListEDataType, List.class, "BeanAttributeList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute>");

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "rootPackage", "entity"
		   });
	}

} //OSBPEntityPackageImpl
