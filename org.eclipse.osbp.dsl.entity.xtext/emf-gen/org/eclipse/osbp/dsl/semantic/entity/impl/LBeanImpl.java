/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity.impl;

import com.google.common.collect.Iterables;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.common.types.impl.LClassImpl;

import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LBeanFeature;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LOperation;
import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LBean</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanImpl#getFeatures <em>Features</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanImpl#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanImpl#getSubTypes <em>Sub Types</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.impl.LBeanImpl#isBeanOnTab <em>Bean On Tab</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LBeanImpl extends LClassImpl implements LBean {
	/**
	 * The cached value of the '{@link #getFeatures() <em>Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<LBeanFeature> features;

	/**
	 * The cached value of the '{@link #getSuperType() <em>Super Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperType()
	 * @generated
	 * @ordered
	 */
	protected LBean superType;

	/**
	 * The cached value of the '{@link #getSubTypes() <em>Sub Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<LBean> subTypes;

	/**
	 * The default value of the '{@link #isBeanOnTab() <em>Bean On Tab</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBeanOnTab()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BEAN_ON_TAB_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBeanOnTab() <em>Bean On Tab</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBeanOnTab()
	 * @generated
	 * @ordered
	 */
	protected boolean beanOnTab = BEAN_ON_TAB_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LBeanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSBPEntityPackage.Literals.LBEAN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LBeanFeature> getFeatures() {
		if (features == null) {
			features = new EObjectContainmentEList<LBeanFeature>(LBeanFeature.class, this, OSBPEntityPackage.LBEAN__FEATURES);
		}
		return features;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LBean getSuperType() {
		if (superType != null && superType.eIsProxy()) {
			InternalEObject oldSuperType = (InternalEObject)superType;
			superType = (LBean)eResolveProxy(oldSuperType);
			if (superType != oldSuperType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OSBPEntityPackage.LBEAN__SUPER_TYPE, oldSuperType, superType));
			}
		}
		return superType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LBean basicGetSuperType() {
		return superType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuperType(LBean newSuperType, NotificationChain msgs) {
		LBean oldSuperType = superType;
		superType = newSuperType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LBEAN__SUPER_TYPE, oldSuperType, newSuperType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperType(LBean newSuperType) {
		if (newSuperType != superType) {
			NotificationChain msgs = null;
			if (superType != null)
				msgs = ((InternalEObject)superType).eInverseRemove(this, OSBPEntityPackage.LBEAN__SUB_TYPES, LBean.class, msgs);
			if (newSuperType != null)
				msgs = ((InternalEObject)newSuperType).eInverseAdd(this, OSBPEntityPackage.LBEAN__SUB_TYPES, LBean.class, msgs);
			msgs = basicSetSuperType(newSuperType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LBEAN__SUPER_TYPE, newSuperType, newSuperType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LBean> getSubTypes() {
		if (subTypes == null) {
			subTypes = new EObjectWithInverseResolvingEList<LBean>(LBean.class, this, OSBPEntityPackage.LBEAN__SUB_TYPES, OSBPEntityPackage.LBEAN__SUPER_TYPE);
		}
		return subTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBeanOnTab() {
		return beanOnTab;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBeanOnTab(boolean newBeanOnTab) {
		boolean oldBeanOnTab = beanOnTab;
		beanOnTab = newBeanOnTab;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPEntityPackage.LBEAN__BEAN_ON_TAB, oldBeanOnTab, beanOnTab));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<LOperation> getOperations() {
		return IterableExtensions.<LOperation>toList(Iterables.<LOperation>filter(this.getFeatures(), LOperation.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<LBeanReference> getReferences() {
		return IterableExtensions.<LBeanReference>toList(Iterables.<LBeanReference>filter(this.getFeatures(), LBeanReference.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<LBeanAttribute> getAttributes() {
		return IterableExtensions.<LBeanAttribute>toList(Iterables.<LBeanAttribute>filter(this.getFeatures(), LBeanAttribute.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<LBeanFeature> getAllFeatures() {
		final List<LBeanFeature> result = CollectionLiterals.<LBeanFeature>newArrayList();
		this.collectAllOSBPFeatures(this, result);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<LBeanAttribute> getAllAttributes() {
		final List<LBeanFeature> result = CollectionLiterals.<LBeanFeature>newArrayList();
		this.collectAllOSBPFeatures(this, result);
		return IterableExtensions.<LBeanAttribute>toList(Iterables.<LBeanAttribute>filter(result, LBeanAttribute.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<LBeanReference> getAllReferences() {
		final List<LBeanFeature> result = CollectionLiterals.<LBeanFeature>newArrayList();
		this.collectAllOSBPFeatures(this, result);
		return IterableExtensions.<LBeanReference>toList(Iterables.<LBeanReference>filter(result, LBeanReference.class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void collectAllOSBPFeatures(final LBean current, final List<LBeanFeature> result) {
		if ((current == null)) {
			return;
		}
		result.addAll(current.getFeatures());
		this.collectAllOSBPFeatures(current.getSuperType(), result);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OSBPEntityPackage.LBEAN__SUPER_TYPE:
				if (superType != null)
					msgs = ((InternalEObject)superType).eInverseRemove(this, OSBPEntityPackage.LBEAN__SUB_TYPES, LBean.class, msgs);
				return basicSetSuperType((LBean)otherEnd, msgs);
			case OSBPEntityPackage.LBEAN__SUB_TYPES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubTypes()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OSBPEntityPackage.LBEAN__FEATURES:
				return ((InternalEList<?>)getFeatures()).basicRemove(otherEnd, msgs);
			case OSBPEntityPackage.LBEAN__SUPER_TYPE:
				return basicSetSuperType(null, msgs);
			case OSBPEntityPackage.LBEAN__SUB_TYPES:
				return ((InternalEList<?>)getSubTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OSBPEntityPackage.LBEAN__FEATURES:
				return getFeatures();
			case OSBPEntityPackage.LBEAN__SUPER_TYPE:
				if (resolve) return getSuperType();
				return basicGetSuperType();
			case OSBPEntityPackage.LBEAN__SUB_TYPES:
				return getSubTypes();
			case OSBPEntityPackage.LBEAN__BEAN_ON_TAB:
				return isBeanOnTab();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OSBPEntityPackage.LBEAN__FEATURES:
				getFeatures().clear();
				getFeatures().addAll((Collection<? extends LBeanFeature>)newValue);
				return;
			case OSBPEntityPackage.LBEAN__SUPER_TYPE:
				setSuperType((LBean)newValue);
				return;
			case OSBPEntityPackage.LBEAN__SUB_TYPES:
				getSubTypes().clear();
				getSubTypes().addAll((Collection<? extends LBean>)newValue);
				return;
			case OSBPEntityPackage.LBEAN__BEAN_ON_TAB:
				setBeanOnTab((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OSBPEntityPackage.LBEAN__FEATURES:
				getFeatures().clear();
				return;
			case OSBPEntityPackage.LBEAN__SUPER_TYPE:
				setSuperType((LBean)null);
				return;
			case OSBPEntityPackage.LBEAN__SUB_TYPES:
				getSubTypes().clear();
				return;
			case OSBPEntityPackage.LBEAN__BEAN_ON_TAB:
				setBeanOnTab(BEAN_ON_TAB_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OSBPEntityPackage.LBEAN__FEATURES:
				return features != null && !features.isEmpty();
			case OSBPEntityPackage.LBEAN__SUPER_TYPE:
				return superType != null;
			case OSBPEntityPackage.LBEAN__SUB_TYPES:
				return subTypes != null && !subTypes.isEmpty();
			case OSBPEntityPackage.LBEAN__BEAN_ON_TAB:
				return beanOnTab != BEAN_ON_TAB_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == LScalarType.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == LFeaturesHolder.class) {
			switch (baseOperationID) {
				case OSBPTypesPackage.LFEATURES_HOLDER___GET_FEATURES: return OSBPEntityPackage.LBEAN___GET_FEATURES;
				case OSBPTypesPackage.LFEATURES_HOLDER___GET_ALL_FEATURES: return OSBPEntityPackage.LBEAN___GET_ALL_FEATURES;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OSBPEntityPackage.LBEAN___GET_OPERATIONS:
				return getOperations();
			case OSBPEntityPackage.LBEAN___GET_REFERENCES:
				return getReferences();
			case OSBPEntityPackage.LBEAN___GET_ATTRIBUTES:
				return getAttributes();
			case OSBPEntityPackage.LBEAN___GET_ALL_FEATURES:
				return getAllFeatures();
			case OSBPEntityPackage.LBEAN___GET_ALL_ATTRIBUTES:
				return getAllAttributes();
			case OSBPEntityPackage.LBEAN___GET_ALL_REFERENCES:
				return getAllReferences();
			case OSBPEntityPackage.LBEAN___COLLECT_ALL_OSBP_FEATURES__LBEAN_LIST:
				collectAllOSBPFeatures((LBean)arguments.get(0), (List<LBeanFeature>)arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (beanOnTab: ");
		result.append(beanOnTab);
		result.append(')');
		return result.toString();
	}

} //LBeanImpl
