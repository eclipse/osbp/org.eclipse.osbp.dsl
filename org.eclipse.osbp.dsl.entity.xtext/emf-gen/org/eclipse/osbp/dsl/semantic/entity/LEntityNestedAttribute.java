/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LEntity Nested Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityNestedAttribute#getFeature <em>Feature</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LEntityNestedAttribute#getTail <em>Tail</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityNestedAttribute()
 * @generated
 */
public interface LEntityNestedAttribute extends LLazyResolver {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(LEntityFeature)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityNestedAttribute_Feature()
	 * @generated
	 */
	LEntityFeature getFeature();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityNestedAttribute#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(LEntityFeature value);

	/**
	 * Returns the value of the '<em><b>Tail</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tail</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tail</em>' containment reference.
	 * @see #setTail(LEntityNestedAttribute)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLEntityNestedAttribute_Tail()
	 * @generated
	 */
	LEntityNestedAttribute getTail();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LEntityNestedAttribute#getTail <em>Tail</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tail</em>' containment reference.
	 * @see #getTail()
	 * @generated
	 */
	void setTail(LEntityNestedAttribute value);

} // LEntityNestedAttribute
