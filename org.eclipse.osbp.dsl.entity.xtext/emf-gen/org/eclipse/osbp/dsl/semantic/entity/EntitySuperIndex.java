/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity Super Index</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getEntitySuperIndex()
 * @generated
 */
public interface EntitySuperIndex extends LLazyResolver {
} // EntitySuperIndex
