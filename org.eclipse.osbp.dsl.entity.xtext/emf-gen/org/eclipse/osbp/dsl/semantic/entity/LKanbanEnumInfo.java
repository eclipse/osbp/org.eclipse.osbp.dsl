/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LKanban Enum Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo#getLiteral <em>Literal</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo#getI18nKey <em>I1 8n Key</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLKanbanEnumInfo()
 * @generated
 */
public interface LKanbanEnumInfo extends EObject {
	/**
	 * Returns the value of the '<em><b>Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' reference.
	 * @see #setLiteral(LEnumLiteral)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLKanbanEnumInfo_Literal()
	 * @generated
	 */
	LEnumLiteral getLiteral();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo#getLiteral <em>Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal</em>' reference.
	 * @see #getLiteral()
	 * @generated
	 */
	void setLiteral(LEnumLiteral value);

	/**
	 * Returns the value of the '<em><b>I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>I1 8n Key</em>' attribute.
	 * @see #setI18nKey(String)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLKanbanEnumInfo_I18nKey()
	 * @generated
	 */
	String getI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LKanbanEnumInfo#getI18nKey <em>I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>I1 8n Key</em>' attribute.
	 * @see #getI18nKey()
	 * @generated
	 */
	void setI18nKey(String value);

} // LKanbanEnumInfo
