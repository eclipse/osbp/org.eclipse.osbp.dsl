/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.entity;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LBean</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A bean is an embeddable element that may become added to a entity using the @embedd annotation.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getFeatures <em>Features</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getSubTypes <em>Sub Types</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.entity.LBean#isBeanOnTab <em>Bean On Tab</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLBean()
 * @generated
 */
public interface LBean extends LClass, LScalarType, LFeaturesHolder {
	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.entity.LBeanFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLBean_Features()
	 * @generated
	 */
	EList<LBeanFeature> getFeatures();

	/**
	 * Returns the value of the '<em><b>Super Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getSubTypes <em>Sub Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Type</em>' reference.
	 * @see #setSuperType(LBean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLBean_SuperType()
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getSubTypes
	 * @generated
	 */
	LBean getSuperType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getSuperType <em>Super Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Type</em>' reference.
	 * @see #getSuperType()
	 * @generated
	 */
	void setSuperType(LBean value);

	/**
	 * Returns the value of the '<em><b>Sub Types</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.entity.LBean}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#getSuperType <em>Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Types</em>' reference list.
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLBean_SubTypes()
	 * @see org.eclipse.osbp.dsl.semantic.entity.LBean#getSuperType
	 * @generated
	 */
	EList<LBean> getSubTypes();

	/**
	 * Returns the value of the '<em><b>Bean On Tab</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bean On Tab</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bean On Tab</em>' attribute.
	 * @see #setBeanOnTab(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage#getLBean_BeanOnTab()
	 * @generated
	 */
	boolean isBeanOnTab();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.entity.LBean#isBeanOnTab <em>Bean On Tab</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bean On Tab</em>' attribute.
	 * @see #isBeanOnTab()
	 * @generated
	 */
	void setBeanOnTab(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of type LOperation
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LOperation> getOperations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of type LBeanReference
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LBeanReference> getReferences();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of type LBeanAttribute
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LBeanAttribute> getAttributes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of the holder and from super types
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LBeanFeature> getAllFeatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all attributes of the holder and from super types
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LBeanAttribute> getAllAttributes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all references of the holder and from super types
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LBeanReference> getAllReferences();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void collectAllOSBPFeatures(LBean current, List<LBeanFeature> result);

} // LBean
