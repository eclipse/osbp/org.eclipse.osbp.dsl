/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.basic.ui;

import java.io.InputStream;
import java.net.URL;

import org.eclipse.jface.text.templates.persistence.TemplatePersistenceData;
import org.eclipse.jface.text.templates.persistence.TemplateReaderWriter;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.slf4j.Logger;

/**
 * <ol>
 * <li><b><u><code>{your-dsl-bundle}</code></u></b></h1>
 * <ol>
 * <li>Add <i><u>optional</u></i> import package <code>org.eclipse.osbp.xtext.basic.generator</code> to the MANIFEST.MF of <code>{your-dsl-bundle}</code></li>
 * <li>Append Grammar I18n generator at the end of the MWE2 workflow
 * <pre>
 * // parse grammar and generate i18n grammar
 * fragment = org.eclipse.osbp.xtext.basic.generator.BasicDslGrammarI18nGenerator auto-inject {}
 * </pre></li>
 * <li>run the MWE2 workflow</li>
 * <li>import any required classes inside <code>{your-dsl-bundle}.generator.{your}DSLGenerator.createAppendable()</code> via <code>org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils.addImportFor()</code></li>
 * </ol></li>
 * <li><b><u><code>{your-dsl-bundle}.ui</code></u></b></h1>
 * <ol>
 * <li>Add required bundle <code>org.eclipse.osbp.xtext.basic.ui</code> to the MANIFEST.MF of <code>{your-dsl-bundle}.ui</code></li>
 * <li>If not already added, add imported package <code>org.eclipse.osbp.utils.constants</code> to the MANIFEST.MF of <code>{your-dsl-bundle}.ui</code></li>
 * <li>Generate the templates file <code>templates/templates.xml</code> to <code>{your-dsl-bundle}.ui</code></li>
 * <li>Add the directories <code>i18n</code> <code>templates</code> to the build of <code>{your-dsl-bundle}.ui</code></li>
 * <li>Generate a <code>{your-dsl-bundle}.ui.{your}DSLDocumentationTranslator</code> and extend it from <code>BasicDSLDocumentationTranslator</code>; see there</li>
 * <li>Extend <code>{your-dsl-bundle}.ui.{your}DSLEObjectHover</code> from <code>BasicDSLEObjectHover</code></li>
 * <li>Extend <code>{your-dsl-bundle}.ui.{your}DSLEObjectHoverProvider</code> from <code>BasicDSLEObjectHoverProvider</code></li>
 * <li>Extend <code>{your-dsl-bundle}.ui.{your}DSLEObjectHoverDocumentationProvider</code> from <code>BasicDSLEObjectHoverDocumentationProvider</code></li>
 * <li>Apply changes in <code>{your-dsl-bundle}.ui.contentassist.{your}DSLProposalProvider</code> as described in <code>BasicDSLProposalProviderHelper</code></li>
 * <li>Extend <code>{your-dsl-bundle}.ui.labeling.{your}DSLLabelProvider</code> from <code>BasicDSLLabelProvider</code></li>
 * <li>
 * Modify or add the following methods to your <code>{your-dsl-bundle}.ui.{your}DSLUiModule</code>, generated the corresponding classes and extend them from the corresponding <code>BasicDSL...</code> classes:
 * <pre>
 * private static Logger LOGGER = LoggerFactory.getLogger({your}DslUiModule.class);
 *
 * public {your}DslUiModule(AbstractUIPlugin plugin) {
 *     super(plugin);
 *     BasicDSLUiModuleHelper.unitTestTemplates(plugin, LOGGER);
 * }
 *
 * @Override
 * public Class&lt;? extends IEObjectHover&gt; bindIEObjectHover() {
 *     return {your}DSLEObjectHover.class;
 * }
 * 
 * @Override
 * public Class&lt;? extends IEObjectHoverProvider&gt; bindIEObjectHoverProvider() {
 *     return {your}DSLEObjectHoverProvider.class;
 * }
 * 
 * @Override
 * public Class&lt;? extends IEObjectHoverDocumentationProvider&gt; bindIEObjectHoverDocumentationProvider() {
 *      return {your}DSLEObjectHoverDocumentationProvider.class;
 * }
 * </pre>
 * </ol></li>
 * </li>
 * </ol>
 */
public final class BasicDSLUiModuleHelper {
	
	protected static class UnitTestTemplateReader extends TemplateReaderWriter {
		final Logger logger;
		final AbstractUIPlugin plugin;
		
		protected UnitTestTemplateReader(AbstractUIPlugin plugin, Logger logger) {
			this.plugin = plugin;
			this.logger = logger;
		}
		
		protected void unitTestTemplates() {
			// org.eclipse.xtext.ui.editor.templates.XtextTemplateStore#getTemplateFileURL(AbstractUIPlugin)
			URL res = plugin.getBundle().getEntry("templates/templates.xml");
			if	(res == null) {
				logger.info("'"+plugin.getClass().getSimpleName()+"': no templates available");
			}
			else {
				// org.eclipse.xtext.ui.editor.templates.XtextTemplateStore#loadContributedTemplates()
				InputStream openStream = null;
				try {
					openStream = res.openStream();
					try {
						TemplatePersistenceData[] read = read(openStream, null);
						for (TemplatePersistenceData templatePersistenceData : read) {
							if	(templatePersistenceData.getId() == null) {
								logger.error("'"+plugin.getClass().getSimpleName()+"': template has no id: name='"+templatePersistenceData.getTemplate().getName()+"'");
							}
							else if	(templatePersistenceData.isDeleted()) {
								logger.debug("'"+plugin.getClass().getSimpleName()+"': template is deleted: name='"+templatePersistenceData.getTemplate().getName()+"'");
							}
							else if	(!templatePersistenceData.isEnabled()) {
								logger.debug("'"+plugin.getClass().getSimpleName()+"': template is disabled: name='"+templatePersistenceData.getTemplate().getName()+"'");
							}
						}
					}
					catch (Exception e) {
						logger.error("'"+plugin.getClass().getSimpleName()+"': exception while loading templates - "+e.getLocalizedMessage());
					}
					finally {
						openStream.close();
					}
				}
				catch (Exception e) {
					logger.info("'"+plugin.getClass().getSimpleName()+"': template file could not be loaded - "+e.getLocalizedMessage());
				}
			}
		}
	}
	
	/**
	 * try to load any existing templates for the given DSL plugin
	 * @see   org.eclipse.xtext.ui.editor.templates.XtextTemplateStore
	 * @see   org.eclipse.jface.text.templates.persistence.TemplateReaderWriter
	 * @param plugin
	 * @param logger
	 */
	public static void unitTestTemplates(AbstractUIPlugin plugin, Logger logger) {
		(new UnitTestTemplateReader(plugin, logger)).unitTestTemplates();
	}
}
