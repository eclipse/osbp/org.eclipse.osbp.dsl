/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.basic.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.ui.hover.XbaseHoverDocumentationProvider;



/**
 * <b>For the complete to-do-list to support keyword information for DSL editors <code>org.eclipse.osbp.xtext.basic.ui.BasicDSLUiModuleHelper</code></b>
 * <hr>
 * support for DSL grammar keywords
 * <hr>
 * <ul>
 * <li>Generate <code>{your}DSLEObjectHoverDocumentationProvider</code>:
 * <pre>
 * public class {your}DSLEObjectHoverDocumentationProvider extends BasicDSLEObjectHoverDocumentationProvider {
 * 
 *     private static {your}DSLEObjectHoverDocumentationProvider INSTANCE;
 * 
 *     public static {your}DSLEObjectHoverDocumentationProvider instance() {
 *         return INSTANCE;
 *     }
 * 
 *     public {your}DSLEObjectHoverDocumentationProvider() {
 *         super();
 *         INSTANCE = this;
 *     }
 * 
 *     @Override
 *     protected BasicDSLDocumentationTranslator getTranslator() {
 *         return {your}DSLDocumentationTranslator.instance();
 *     }
 * }
 * </pre>
 * </li>
 * </ul>
 */
@SuppressWarnings("restriction")
public abstract class BasicDSLEObjectHoverDocumentationProvider extends XbaseHoverDocumentationProvider {

	/**
	 * @return the documentation translator of the DSL-UI-Bundle
	 */
	protected abstract BasicDSLDocumentationTranslator getTranslator();

	/**
	 * add additional translated documentation
	 * @see XbaseHoverDocumentationProvider#getDocumentation(EObject)
	 */
	@Override
	public String getDocumentation(EObject object) {
		String documentation = super.getDocumentation(object);
		if	((documentation != null) && documentation.isEmpty()) {
			documentation = null;
		}
		String translated = getTranslator().getDocumentation(object, true);
		if	(documentation == null) {
			return translated;
		}
		else if (translated == null) {
			return documentation;
		}
		else {
			return documentation+"<p>"+translated+"</p>";
		}
	}
}
