/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.basic.ui.labeling;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.osbp.dsl.semantic.common.types.LImport;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;
import org.eclipse.osbp.xtext.oxtype.ui.labeling.OXtypeLabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.xbase.ui.labeling.XbaseLabelProvider;

/**
 * <b>For the complete to-do-list to support keyword information for DSL editors <code>org.eclipse.osbp.xtext.basic.ui.BasicDSLUiModuleHelper</code></b>
 * <hr>
 * support for DSL grammar keywords
 * <hr>
 * <ul>
 * <li>let <code>{your}DSLLabelProvider</code> extend from <code>BasicDSLLabelProvider</code></li>
 * </ul>
 */
@SuppressWarnings("restriction")
public abstract class BasicDSLLabelProvider extends OXtypeLabelProvider {
 
	static	final	private	int		maxOutputLen	=	24;
	static	final	private	String	abbrMark		=	" ...";

	/**
	 * Currently available outline image sizes for displaying colors:
	 */
	public	enum	outlineImgSize {
		BIG		( 16, 16 ),
		SMALL	( 12, 12 );

		private final int width ;
		private final int height;

		private outlineImgSize ( int width, int height ) {
			this.width	= width ;
			this.height	= height;
		}

		public int getWidth () { return	width ; }
		public int getHeight() { return	height; }
	}

	/**
	 * enable doGetText(Object) for keywords
	 */
	@Override
	protected Object doGetText(Object element) {
		if	(element instanceof Keyword) {
			return ((Keyword) element).getValue();
		}
		return super.doGetText(element);
	}

	/**
	 * Constructor needed 
	 * @param provider
	 */
	protected BasicDSLLabelProvider(AdapterFactoryLabelProvider provider) {
		super(provider);
		fImages = new HashMap<String,Image>();
	}

	protected Map<String,Image> fImages;

	protected final Image getErrorImage() {
		return getImage("error.gif");
	}
	
	/* ********************************************************************************************** */
	/**         Returns a reference to the image instance according to the passed image filename.
	 * 
	 * 			Scans the context specific subdirectory first, if specified by overridden method
	 * 			{@code getImagePrefix()}. Falls back to common image directory if no context specific
	 * 			image file was found.
	 *
	 *	@see	getImagePrefix()
	 * 
	 *	@param	imgFileName	Name of image file (incl. extension)
	 *
	 *	@return	Reference to the image instance if image file found, {@code null} otherwise.
	 *
	 *	@since	@D yymmdd, name
	 *	@date	@D 150916, gu
	 *	@date	@D 160512, ri
	 */ /* ******************************************************************************************* */
	protected final Image getInternalImage ( String imgFileName, Class<?> clz ) {
		try {
			Image image = fImages.get( imgFileName );
			if	( image == null ) {
				ClassLoader clzLoader = clz.getClassLoader();
				InputStream stream = clzLoader.getResourceAsStream("icons/"+imgFileName);
				image = new Image( null, stream);
				fImages.put( imgFileName, image );
			}
			return image;
		}
		catch (Exception e) {
			return null;
		}
	}

	/* ********************************************************************************************** */
	/**	Overloaded version with no name provided and without limiting outline text to maximum outline
	 * 	text size.
	 * 
	 *	@since	150916, gu
	 *	@date	160803, gu
	 */ /* ******************************************************************************************* */
	protected String generateText ( Object element, String description ) {
		return	generateText( element, description, true );
	}

	/* ********************************************************************************************** */
	/**	Overloaded version with no name provided and parameter {@code showFullDesc} for limiting
	 *	outline text to maximum outline text size.
	 * 
	 *	@since	160803, gu
	 *	@date	yymmdd, name
	 */ /* ******************************************************************************************* */
	protected String generateText ( Object element, String description, boolean showFullDesc ) {
		return	generateText( element, description, null, showFullDesc );
	}

	/* ********************************************************************************************** */
	/**	Overloaded version with limiting outline text to maximum outline text size.
	 * 	@see	{@link protected String generateText( Object element, String description, String name, boolean showFullDesc )}
	 *	@since	160803, gu
	 *	@date	yymmdd, name
	 */ /* ******************************************************************************************* */
	protected String generateText ( Object element, String description, String name ) {
		return	generateText( element, description, name, false );
	}

	/* ********************************************************************************************** */
	/**	Common version of the method for generating an outline text for the passed element.
	 * 
	 * 	Appends the passed description with a hyphen (" - ") to the provided name if a name is given,
	 * 	respectively could be found. Returns the pure {@code description} if no name is provided.
	 * 
	 * 	Limits description to maximum outline text size unless parameter {@code showFullDesc} is not
	 * 	set to {@code true}.
	 * 
	 *	@param	element			Current outline element
	 *	@param	description		Description to be appended to the name
	 *	@param	name			Name of the element
	 *	@param	showFullDesc	Don't shorten description if maximum outline text size is exceeded
	 *
	 *	@return	Formatted outline text if a suitable outline text could be found, return value of
	 *			overridden superclass method otherwise.
	 *
	 *	@since	150916, gu
	 *	@date	160803, gu
	 */ /* ******************************************************************************************* */
	protected String generateText( Object element, String description, String name, boolean showFullDesc ) {
		String result = name;

		// --- BEWARE: method getText() MUST NOT be called here, for the method to be called from class
		// --- DeclarativeLabelProvider is method "text" (not getText), where DeclarativeLabelProvider's
		// --- superclass AbstractLabelProvider contains a method named "getText" - which would be called
		// --- where it mustn't (and would lead to an endless loop and further on to a stack overflow)!
		if	( result == null )
			result = (String) super.text( element );
//			result = (String) super.getText( element );

		//	Don' worry about null-pointer for 'result' (it will just stay null):
		if	( !showFullDesc ) {
			result = limitOutlineText( result );
		}

		if	( result == null ) {
			result = description;
		}

		else if ( description != null ) {
			result = result.concat( " - " ).concat( description );
		}

		return	result;
	}


	/* ********************************************************************************************** */
	/**			Limits given text to maximum outline text size ({@value maxOutputLen}) if exceeded.
	 * 			Indicates abbreviation by adding 3 dots, separated by space (" ...") to output text.
	 * 			<p>
	 * 	@note	The abbreviation marking (" ...") shortens the effective outline text.
	 * 
	 * @example	<code>
	 * 			original text = "1234567890123456789012345" (length 25)<br>
	 * 			output text   = "12345678901234567890 ..."  (length 24)
	 * 			<p>
	 * 			original text = "123456789012345678901234"  (length 24)<br>
	 * 			output text   = "123456789012345678901234"  (length 24)
	 * 			</code>
	 * 
	 *	@param	refString	Original outline text
	 *
	 *	@return	Abbreviated text if maximum size exceeded, original text otherwise.
	 *
	 *	@since	160803, gu
	 *	@date	yymmdd, name
	 */ /* ******************************************************************************************* */
	protected String limitOutlineText ( String refString ) {
		String  outString = refString;

		//	Shorten string if maximum length exceeded:
		if	( outString != null && outString.length() > maxOutputLen ) {
			outString = outString.substring( 0, maxOutputLen - abbrMark.length() ).concat( abbrMark );
		}

		return	outString;
	}

	/* ********************************************************************************************** */
	/**			Draws <b>big</b> color outline image with given color.
	 * 
	 *	@see	{@link BasicDSLLabelProvider.getColorOutlineImage(String,boolean)}
	 * 
	 *	@param	rgbString	String containing RGB value according to format "<red>,<green>,<blue>"
	 *
	 *	@return	Reference to generated image in case of success, null otherwise.
	 *
	 *	@since	@D 150929, gu
	 *	@date	@D yymmdd, name
	 */ /* ******************************************************************************************* */
	public Image getColorOutlineImage ( String rgbString ) { return	getColorOutlineImage( rgbString, false ); }


	/* ********************************************************************************************** */
	/**			Draws color outline image with given color.
	 * 
	 * 			Two sizes of images can currently be provided according to enumeration {@code outlineImgSize}:
	 * 			big: 16x16 Pixel, small: 12x12 Pixel.
	 * 
	 *	@see	{@link outlineImgSize}
	 * 
	 *	@param	rgbString	String containing RGB value according to format "<red>,<green>,<blue>"
	 *	@param	small		Draw small image
	 *
	 *	@return	Reference to generated image in case of success, null otherwise.
	 *
	 *	@since	@D 150929, gu
	 *	@date	@D yymmdd, name
	 */ /* ******************************************************************************************* */
	public Image getColorOutlineImage ( String rgbString, boolean small ) {

		Image		retImg		=	null;
		try {
			Color			imgColor	=	null;
			GC				graphic		=	null;
			String[]		rgbStrList	=	rgbString.split( "," );
			outlineImgSize	outSize		=	outlineImgSize.BIG;

			// --- Small image requested?: ---
			if	( small == true )
				outSize = outlineImgSize.SMALL;

				// --- Exactly 3 color values are expected: ---
			if	( rgbStrList.length == 3 ) {
				int		idx			=	0;
				int[]	rgbList		=	new int[ 3 ];

				// --- Cast string colors to integers: ---
				for	( String entry : rgbString.split( "," ) )
					rgbList[ idx++ ] = Integer.parseInt( entry );

				// --- Generate image with given colors: ---
				imgColor = new Color( null, rgbList[ 0 ], rgbList[ 1 ], rgbList[ 2 ] );
			}
	
			// --- If something went wrong further processing is dispensable: ---
			if	( imgColor == null )
				return	null;
	
			// --- Generate new outline image and corresponding graphic element with defined default size: ---
			retImg  = new Image( null, new Rectangle( 0, 0, outSize.getWidth(), outSize.getHeight() ) );
			graphic = new GC( retImg );

			// --- Fill and draw image with given color: --- 
			graphic.setBackground( imgColor );
			graphic.fillRectangle( 0, 0, outSize.getWidth(), outSize.getHeight() );

			// --- Clean up color and graphic elements: --- 
			graphic.dispose();
			imgColor.dispose();
		}
		catch (Exception e) {
			return null;
		}

		return	retImg;
	}


	/* ********************************************************************************************** */
	/**			Generates a common outline text for the passed outline element, suitable for all
	 * 			elements of the same type of DSLs.
	 * 
	 *	@param	element	Current outline element
	 *
	 *	@return	Formatted outline text if a suitable outline text could be found, return value of
	 *			overridden superclass method otherwise.
	 *
	 *	@since	@D 150916, gu
	 *	@date	@D yymmdd, name
	 */ /* ******************************************************************************************* */
	public Object text ( Object element ) {

		if	( element instanceof LImport )
			return	generateText( element, "import", ( (LImport) element ).getImportedNamespace(), true );

		if	( element instanceof LPackage )
			return	generateText( element, "package", ( (LPackage) element ).getName(), true );

		return	super.text( element );
	}


	/* ********************************************************************************************** */
	/**         Returns a reference to the common image instance according to the passed outline
	 * 			element, suitable for all elements of the same type of DSLs.
	 * 
	 *	@param	element	Current outline element
	 *
	 *	@return	Formatted outline text if a suitable outline text could be found, return value of
	 *			overridden superclass method otherwise.
	 *
	 *	@since	@D 150916, gu
	 *	@date	@D yymmdd, name
	 */ /* ******************************************************************************************* */
	public Object image ( Object element ) {

		if	( element instanceof LImport )
			return	getInternalImage( "dsl_import.png", getClass());

		if	( element instanceof LPackage )
			return	getInternalImage( "package.gif", getClass());

		return	super.image( element );
	}

}
