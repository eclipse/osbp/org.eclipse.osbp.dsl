/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.basic.ui.contentassist;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.ui.editor.contentassist.AbstractContentProposalProvider;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;
import org.eclipse.xtext.ui.editor.contentassist.PrefixMatcher;
import org.eclipse.xtext.util.Strings;
import org.eclipse.osbp.xtext.basic.ui.BasicDSLDocumentationTranslator;

/**
 * <b>For the complete to-do-list to support keyword information for DSL editors
 * <code>org.eclipse.osbp.xtext.basic.ui.BasicDSLUiModuleHelper</code></b>
 * <hr>
 * Helper needed inProposalProviders
 * <hr>
 * Add the following method to your <code>{your}DSLProposalProvider</code>:
 * 
 * <pre>
 * override protected StyledString getKeywordDisplayString(Keyword keyword) {
 *     return BasicDSLProposalProviderHelper.getKeywordDisplayString(keyword, {your}DSLDocumentationTranslator.instance())
 * }
 * </pre>
 */
public class BasicDSLProposalProviderHelper {

	/**
	 * get a translated documentation to be displayed in a proposal dialog in
	 * Eclipse
	 * 
	 * @param keyword
	 *            the Keyword
	 * @param translator
	 *            the translator of the DSL-UI-Bundle
	 * @return StyledString
	 */
	public static StyledString getKeywordDisplayString(Keyword keyword,
			BasicDSLDocumentationTranslator translator) {
		String translated = translator.getDocumentation(keyword, true);
		if (translated == null) {
			translated = "";
		} else {
			// --- if the documentation contains more than one line, use only
			// the first line ---
			translated = " - " + (translated.split("<br>"))[0];
		}
		return new StyledString(keyword.getValue() + translated);
	}

	public void complete_PackageName(EObject model, RuleCall ruleCall,
			final ContentAssistContext context,
			ICompletionProposalAcceptor acceptor,
			AbstractContentProposalProvider provider) {
		PrefixMatcher newMatcher = new PrefixMatcher() {
			@Override
			public boolean isCandidateMatchingPrefix(String name, String prefix) {
				String strippedName = name;
				if (name.startsWith("^") && !prefix.startsWith("^")) {
					strippedName = name.substring(1);
				}
				return context.getMatcher().isCandidateMatchingPrefix(
						strippedName, prefix);
			}
		};
		ContentAssistContext myContext = context.copy().setMatcher(newMatcher)
				.toContext();
		String feature = getAssignedFeature(ruleCall);
		String proposalPrefix = "com.example.mypackage.";
		String proposalText = feature != null ? feature.toLowerCase()
				: ruleCall.getRule().getName().toLowerCase();
		proposalText = proposalPrefix.concat(proposalText);
		String displayText = proposalText;
		if (feature != null)
			displayText = proposalText + " - dot separated "
					+ ruleCall.getRule().getName();
		proposalText = provider.getValueConverter().toString(proposalText,
				ruleCall.getRule().getName());
		ICompletionProposal proposal = provider.createCompletionProposal(
				proposalText, displayText, null, myContext);
		configureProposal(myContext, proposalText, proposal);
		acceptor.accept(proposal);
	}

	public String getAssignedFeature(RuleCall call) {
		Assignment ass = GrammarUtil.containingAssignment(call);
		if (ass != null) {
			String result = ass.getFeature();
			if (result.equals(result.toLowerCase()))
				result = Strings.toFirstUpper(result);
			return result;
		}
		return null;
	}

	public void createIntProposal(ContentAssistContext context,
			ICompletionProposalAcceptor acceptor, RuleCall ruleCall,
			String feature, int i, String displayTextInput,
			AbstractContentProposalProvider provider) {
		displayTextInput = (displayTextInput != null) ? displayTextInput : "";
		String proposalText = provider.getValueConverter().toString(i,
				ruleCall.getRule().getName());
		String displayText = proposalText.concat(" - ")
				.concat(displayTextInput).concat(" ");
		displayText = (feature != null) ? displayText.concat(" ").concat(
				feature) : displayText.concat(" ").concat(
				ruleCall.getRule().getName());
		ICompletionProposal proposal = provider.createCompletionProposal(
				proposalText, displayText, null, context);
		configureProposal(context, proposalText, proposal);
		acceptor.accept(proposal);
	}

	public void createNumberProposal(ContentAssistContext context,
			ICompletionProposalAcceptor acceptor, RuleCall ruleCall,
			AbstractContentProposalProvider provider) {
		String proposalText = "0.0";
		String displayTextSuffix = "";
		String feature = getAssignedFeature(ruleCall);
		displayTextSuffix = (feature != null) ? feature.concat(" as ").concat(
				ruleCall.getRule().getName().toLowerCase()) : ruleCall
				.getRule().getName().toLowerCase();
		String displayText = proposalText.concat(" - decimal value for ")
				.concat(displayTextSuffix);
		ICompletionProposal proposal = provider.createCompletionProposal(
				proposalText, displayText, null, context);
		configureProposal(context, proposalText, proposal);
		acceptor.accept(proposal);
	}

	private void configureProposal(ContentAssistContext context,
			String proposalText, ICompletionProposal proposal) {
		if (proposal instanceof ConfigurableCompletionProposal) {
			ConfigurableCompletionProposal configurable = (ConfigurableCompletionProposal) proposal;
			configurable.setSelectionStart(configurable.getReplacementOffset());
			configurable.setSelectionLength(proposalText.length());
			configurable.setAutoInsertable(false);
			configurable.setSimpleLinkedMode(context.getViewer(), '\t', ' ');
		}
	}

}
