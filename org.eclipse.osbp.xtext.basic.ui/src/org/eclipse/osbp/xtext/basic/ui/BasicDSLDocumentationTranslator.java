/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.basic.ui;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.TypeRef;



/**
 * <b>For the complete to-do-list to support keyword information for DSL editors <code>org.eclipse.osbp.xtext.basic.ui.BasicDSLUiModuleHelper</code></b>
 * <hr>
 * support for DSL grammar keywords
 * <hr>
 * <ul>
 * <li>generate a folder <code>i18n</code> with property files <code>I18N{_languages}.properties</code></li>
 * <li>Generate {your}DSLUiTranslator:
 * <pre>
 * public class {your}DSLDocumentationTranslator extends BasicDSLDocumentationTranslator {
 * 
 *     private static {your}DSLDocumentationTranslator INSTANCE = new {your}DSLDocumentationTranslator();
 * 
 *     public static BasicDSLDocumentationTranslator instance() {
 *         return INSTANCE;
 *     }
 * 
 *     @Override
 *     protected ResourceBundle getResourceBundle() {
 *         return java.util.ResourceBundle.getBundle(GeneratorConstants.I18N_RESOURCE_FULL_BUNDLE_NAME, getLocale(), getClass().getClassLoader());
 *     }
 * }
 * </pre>
 * </li>
 * </ul>
 */
public abstract class BasicDSLDocumentationTranslator {

	/**
	 * get the translator key for the class and item
	 * @param clazz
	 * @param item
	 * @return translator key
	 */
	public static String getTranslatorKey(Object object, String item) {
		return getTranslatorCanonicalName(object)+"."+item;
	}

	/**
	 * get the translator key for the class and item
	 * @param clazz
	 * @param item
	 * @return translator key
	 */
	public static String getTranslatorKey(Class<?> clazz, String item) {
		return getTranslatorCanonicalName(clazz)+"."+item;
	}

	/**
	 * get the translator key for the EObject
	 * @param object
	 * @return translator key
	 */
	public static String getTranslatorKey(EObject object) {
		// --- if it's a keyword ---
		if	(object instanceof Keyword) {
			ParserRule parserRule = null;
			Class<?> ruleClass = null;
			EObject group = object.eContainer();
			while ((group != null) && !(group instanceof ParserRule)) {
				group = group.eContainer();
			}
			if	(group instanceof ParserRule) {
				parserRule = (ParserRule) group;
				TypeRef type = parserRule.getType();
				EClassifier classifier = type.getClassifier();
				ruleClass = classifier.getInstanceClass();
			}
			// --- the translator key is defined by the corresponding parser rule's class and the keywords value ---
			if	(ruleClass == null) {
				return ((Keyword)object).getValue();
			}
			else {
				return getTranslatorCanonicalName(ruleClass)+"."+((Keyword)object).getValue();
			}
		}
		// --- otherwise it's the EObject's class
		else {
			return getTranslatorCanonicalName(object);
		}
	}
	
	/**
	 * @param object
	 * @return get the canonical class name used for the translator key
	 */
	private static String getTranslatorCanonicalName(Object object) {
		return getTranslatorCanonicalName(object.getClass());
	}

	private final static String IGNORE_IMPL_PREFIX = "Impl";
	private final static String IGNORE_IMPL_PACKAGE = ".impl.";
	
	/**
	 * <ul>
	 * <li>Impl-prefix and impl-package part is removed</li>
	 * <li>Inner classes are handled with "normal" package separator</li>
	 * </ul>
	 * @param clazz the class
	 * @return get the canonical class name used for the translator key
	 */
	private static String getTranslatorCanonicalName(Class<?> clazz) {
		String result = clazz.getCanonicalName();
		if	(result.endsWith(IGNORE_IMPL_PREFIX)) {
			result = result.substring(0, result.length()-IGNORE_IMPL_PREFIX.length());
		}
		return result.replace('$','.').replace(IGNORE_IMPL_PACKAGE,".");
	}
	
	/**
	 * one locale definition for all DSL-UI instances running in this Eclipse instance  
	 */
	private static Locale sLocale = Locale.getDefault();

	/**
	 * If the Eclipse instance locale should be changed, inform this translator!
	 * @param locale
	 */
	public static final void setLocale(Locale locale) {
		sLocale = locale;
	}

	protected static final Locale getLocale() {
		return sLocale;
	}
	
	/**
	 * @return the resource bundle with the translation property files of the DSL-UI-Bundle
	 */
	protected abstract ResourceBundle getResourceBundle();

	/**
	 * Get the translated documentation for the EObject
	 * @param object
	 * @param warning true, if the information about missing translation or documentation should be shown
	 * @return
	 */
	public final String getDocumentation(EObject object, boolean warning) {
		String translatorKey = getTranslatorKey(object);
		if	(!translatorKey.contains(".") && (object instanceof Keyword)) {
			warning = false;
		}
		return getDocumentation(translatorKey, warning);
	}
	
	/**
	 * Get the translated documentation for the key
	 * @param key
	 * @param warning true, if the information about missing translation or documentation should be shown
	 * @return
	 */
	public final String getDocumentation(String key, boolean warning) {
		// --- if it's just a single operation, do nothing at all
		if	(key.contains("}") || key.contains("{")) {
			return null;
		}
		// --- get the translated documentation
		try {
			String result = getResourceBundle().getString(key);
			if	(result.isEmpty()) {
				System.err.println(" - empty translation or documentation: "+key);
			}
			return result;
		}
		catch (MissingResourceException mre) {
			System.err.println("- missing translation or documentation: "+key);
			if	(warning) {
				return key+" - missing translation or documentation";
			}
			else {
				return null;
			}
		}
	}
}
