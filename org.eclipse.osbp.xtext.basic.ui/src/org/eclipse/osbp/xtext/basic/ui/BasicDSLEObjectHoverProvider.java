/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.basic.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jface.internal.text.html.HTMLPrinter;
import org.eclipse.jface.text.IRegion;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.common.types.JvmIdentifiableElement;
import org.eclipse.xtext.ui.editor.hover.html.IEObjectHoverDocumentationProvider;
import org.eclipse.xtext.ui.editor.hover.html.XtextBrowserInformationControlInput;
import org.eclipse.xtext.xbase.ui.hover.XbaseHoverProvider;
import org.eclipse.xtext.xbase.ui.hover.XbaseInformationControlInput;



/**
 * <b>For the complete to-do-list to support keyword information for DSL editors <code>org.eclipse.osbp.xtext.basic.ui.BasicDSLUiModuleHelper</code></b>
 * <hr>
 * support for DSL grammar keywords
 * <hr>
 * <ul>
 * <li>Generate <code>{your}DSLEObjectHoverProvider</code>:
 * <pre>
 * public class {your}DSLEObjectHoverProvider extends BasicDSLEObjectHoverProvider {
 * 
 *     private static {your}DSLEObjectHoverProvider INSTANCE;
 * 
 *     public static {your}DSLEObjectHoverProvider instance() {
 *         return INSTANCE;
 *     }
 * 
 *     public {your}DSLEObjectHoverProvider() {
 *         super();
 *         INSTANCE = this;
 *     }
 * 
 *     @Override
 *     public IEObjectHoverDocumentationProvider getDocumentationHoverProvider() {
 *         return {your}DSLEObjectHoverDocumentationProvider.instance();
 *     }
 * }
 * </pre>
 * </li>
 * </ul>
 */
@SuppressWarnings("restriction")
public abstract class BasicDSLEObjectHoverProvider extends XbaseHoverProvider {

	/**
	 * @return the hover documentation provider of the DSL-UI-Bundle
	 */
	public abstract IEObjectHoverDocumentationProvider getDocumentationHoverProvider();

	/**
	 * @param o
	 * @return true if documentation for this EObject exists
	 */
	private boolean hasDocumentation(EObject o) {
		return (getDocumentationHoverProvider() != null)
		    && (getDocumentationHoverProvider().getDocumentation(o) != null)
		    && !getDocumentationHoverProvider().getDocumentation(o).isEmpty();
	}

	/**
	 * necessary to support hover documentation for keywords
	 * @see XbaseHoverProvider#hasHover(EObject)
	 */
	@Override
	protected boolean hasHover(EObject o) {
		return hasDocumentation(o) || super.hasHover(o) || (o instanceof Keyword);
	}

	/**
	 * necessary to support hover documentation for keywords
	 * @see XbaseHoverProvider#getDocumentation(EObject)
	 */
	@Override
	protected String getDocumentation(EObject o) {
		// --- if it's a keyword, let the documentation hover provider do the work ---
		if	(o instanceof Keyword) {
			return getDocumentationHoverProvider().getDocumentation(o);
		}
		else {
			return super.getDocumentation(o);
		}
	}

	/**
	 * necessary to support hover documentation for keywords
	 * @see XbaseHoverProvider#getHoverInfo(EObject,IRegion,XtextBrowserInformationControlInput)
	 */
	@Override
	protected XtextBrowserInformationControlInput getHoverInfo(EObject element, IRegion hoverRegion,
			XtextBrowserInformationControlInput previous) {
		// --- if it's a keyword, do something special (without irrevelant check inside the super implementation) ---
		if 	(element instanceof Keyword) {
			EObject objectToView = getObjectToView(element);
			if(objectToView == null || objectToView.eIsProxy())
				return null;
			String html = getHoverInfoAsHtml(element, objectToView, hoverRegion);
			if (html != null) {
				StringBuffer buffer = new StringBuffer(html);
				HTMLPrinter.insertPageProlog(buffer, 0, getStyleSheet());
				HTMLPrinter.addPageEpilog(buffer);
				html = buffer.toString();
				IJavaElement javaElement = null;
				if (objectToView != element && objectToView instanceof JvmIdentifiableElement) {
					javaElement = javaElementFinder.findElementFor((JvmIdentifiableElement) objectToView);
				}
				return new XbaseInformationControlInput(previous, objectToView, javaElement, html, labelProvider);
			}
		}
		// --- otherwise do the default ---
		else {
			return super.getHoverInfo(element, hoverRegion, previous);
		}
		return null;
	}
}
