/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types.impl;

import java.util.List;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.osbp.dsl.semantic.common.types.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OSBPTypesFactoryImpl extends EFactoryImpl implements OSBPTypesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OSBPTypesFactory init() {
		try {
			OSBPTypesFactory theOSBPTypesFactory = (OSBPTypesFactory)EPackage.Registry.INSTANCE.getEFactory(OSBPTypesPackage.eNS_URI);
			if (theOSBPTypesFactory != null) {
				return theOSBPTypesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OSBPTypesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPTypesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OSBPTypesPackage.LCOMMON_MODEL: return createLCommonModel();
			case OSBPTypesPackage.LLAZY_RESOLVER: return createLLazyResolver();
			case OSBPTypesPackage.LPACKAGE: return createLPackage();
			case OSBPTypesPackage.LTYPED_PACKAGE: return createLTypedPackage();
			case OSBPTypesPackage.LIMPORT: return createLImport();
			case OSBPTypesPackage.LTYPE: return createLType();
			case OSBPTypesPackage.LANNOTATION_DEF: return createLAnnotationDef();
			case OSBPTypesPackage.LANNOTATION_TARGET: return createLAnnotationTarget();
			case OSBPTypesPackage.LSCALAR_TYPE: return createLScalarType();
			case OSBPTypesPackage.LDATA_TYPE: return createLDataType();
			case OSBPTypesPackage.LENUM: return createLEnum();
			case OSBPTypesPackage.LENUM_LITERAL: return createLEnumLiteral();
			case OSBPTypesPackage.LSTATE_CLASS: return createLStateClass();
			case OSBPTypesPackage.LSTATE: return createLState();
			case OSBPTypesPackage.LCLASS: return createLClass();
			case OSBPTypesPackage.LKEY_AND_VALUE: return createLKeyAndValue();
			case OSBPTypesPackage.LOPERATION: return createLOperation();
			case OSBPTypesPackage.LMODIFIER: return createLModifier();
			case OSBPTypesPackage.LMULTIPLICITY: return createLMultiplicity();
			case OSBPTypesPackage.LRESULT_FILTERS: return createLResultFilters();
			case OSBPTypesPackage.LATTRIBUTE_MATCHING_CONSTRAINT: return createLAttributeMatchingConstraint();
			case OSBPTypesPackage.LDT_CASSERT_FALSE: return createLDtCAssertFalse();
			case OSBPTypesPackage.LDT_CASSERT_TRUE: return createLDtCAssertTrue();
			case OSBPTypesPackage.LDT_CDECIMAL_MAX: return createLDtCDecimalMax();
			case OSBPTypesPackage.LDT_CDECIMAL_MIN: return createLDtCDecimalMin();
			case OSBPTypesPackage.LDT_CDIGITS: return createLDtCDigits();
			case OSBPTypesPackage.LDT_CFUTURE: return createLDtCFuture();
			case OSBPTypesPackage.LDT_CPAST: return createLDtCPast();
			case OSBPTypesPackage.LDT_CNUMERIC_MAX: return createLDtCNumericMax();
			case OSBPTypesPackage.LDT_CNUMERIC_MIN: return createLDtCNumericMin();
			case OSBPTypesPackage.LDT_CNOT_NULL: return createLDtCNotNull();
			case OSBPTypesPackage.LDT_CNULL: return createLDtCNull();
			case OSBPTypesPackage.LDT_CREG_EX: return createLDtCRegEx();
			case OSBPTypesPackage.LDT_CSIZE: return createLDtCSize();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OSBPTypesPackage.LDATE_TYPE:
				return createLDateTypeFromString(eDataType, initialValue);
			case OSBPTypesPackage.LVISIBILITY:
				return createLVisibilityFromString(eDataType, initialValue);
			case OSBPTypesPackage.LLOWER_BOUND:
				return createLLowerBoundFromString(eDataType, initialValue);
			case OSBPTypesPackage.LUPPER_BOUND:
				return createLUpperBoundFromString(eDataType, initialValue);
			case OSBPTypesPackage.LCOMPARATOR_TYPE:
				return createLComparatorTypeFromString(eDataType, initialValue);
			case OSBPTypesPackage.LCONSTRAINT_SEVERITY:
				return createLConstraintSeverityFromString(eDataType, initialValue);
			case OSBPTypesPackage.OPERATIONS_LIST:
				return createOperationsListFromString(eDataType, initialValue);
			case OSBPTypesPackage.FEATURES_LIST:
				return createFeaturesListFromString(eDataType, initialValue);
			case OSBPTypesPackage.ANNOTATION_LIST:
				return createAnnotationListFromString(eDataType, initialValue);
			case OSBPTypesPackage.INTERNAL_EOBJECT:
				return createInternalEObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OSBPTypesPackage.LDATE_TYPE:
				return convertLDateTypeToString(eDataType, instanceValue);
			case OSBPTypesPackage.LVISIBILITY:
				return convertLVisibilityToString(eDataType, instanceValue);
			case OSBPTypesPackage.LLOWER_BOUND:
				return convertLLowerBoundToString(eDataType, instanceValue);
			case OSBPTypesPackage.LUPPER_BOUND:
				return convertLUpperBoundToString(eDataType, instanceValue);
			case OSBPTypesPackage.LCOMPARATOR_TYPE:
				return convertLComparatorTypeToString(eDataType, instanceValue);
			case OSBPTypesPackage.LCONSTRAINT_SEVERITY:
				return convertLConstraintSeverityToString(eDataType, instanceValue);
			case OSBPTypesPackage.OPERATIONS_LIST:
				return convertOperationsListToString(eDataType, instanceValue);
			case OSBPTypesPackage.FEATURES_LIST:
				return convertFeaturesListToString(eDataType, instanceValue);
			case OSBPTypesPackage.ANNOTATION_LIST:
				return convertAnnotationListToString(eDataType, instanceValue);
			case OSBPTypesPackage.INTERNAL_EOBJECT:
				return convertInternalEObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LCommonModel createLCommonModel() {
		LCommonModelImpl lCommonModel = new LCommonModelImpl();
		return lCommonModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LLazyResolver createLLazyResolver() {
		LLazyResolverImpl lLazyResolver = new LLazyResolverImpl();
		return lLazyResolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LPackage createLPackage() {
		LPackageImpl lPackage = new LPackageImpl();
		return lPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTypedPackage createLTypedPackage() {
		LTypedPackageImpl lTypedPackage = new LTypedPackageImpl();
		return lTypedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LImport createLImport() {
		LImportImpl lImport = new LImportImpl();
		return lImport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LType createLType() {
		LTypeImpl lType = new LTypeImpl();
		return lType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LAnnotationDef createLAnnotationDef() {
		LAnnotationDefImpl lAnnotationDef = new LAnnotationDefImpl();
		return lAnnotationDef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LAnnotationTarget createLAnnotationTarget() {
		LAnnotationTargetImpl lAnnotationTarget = new LAnnotationTargetImpl();
		return lAnnotationTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LScalarType createLScalarType() {
		LScalarTypeImpl lScalarType = new LScalarTypeImpl();
		return lScalarType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDataType createLDataType() {
		LDataTypeImpl lDataType = new LDataTypeImpl();
		return lDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEnum createLEnum() {
		LEnumImpl lEnum = new LEnumImpl();
		return lEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEnumLiteral createLEnumLiteral() {
		LEnumLiteralImpl lEnumLiteral = new LEnumLiteralImpl();
		return lEnumLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LStateClass createLStateClass() {
		LStateClassImpl lStateClass = new LStateClassImpl();
		return lStateClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LState createLState() {
		LStateImpl lState = new LStateImpl();
		return lState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LClass createLClass() {
		LClassImpl lClass = new LClassImpl();
		return lClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LKeyAndValue createLKeyAndValue() {
		LKeyAndValueImpl lKeyAndValue = new LKeyAndValueImpl();
		return lKeyAndValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LOperation createLOperation() {
		LOperationImpl lOperation = new LOperationImpl();
		return lOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LModifier createLModifier() {
		LModifierImpl lModifier = new LModifierImpl();
		return lModifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LMultiplicity createLMultiplicity() {
		LMultiplicityImpl lMultiplicity = new LMultiplicityImpl();
		return lMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LResultFilters createLResultFilters() {
		LResultFiltersImpl lResultFilters = new LResultFiltersImpl();
		return lResultFilters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LAttributeMatchingConstraint createLAttributeMatchingConstraint() {
		LAttributeMatchingConstraintImpl lAttributeMatchingConstraint = new LAttributeMatchingConstraintImpl();
		return lAttributeMatchingConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCAssertFalse createLDtCAssertFalse() {
		LDtCAssertFalseImpl lDtCAssertFalse = new LDtCAssertFalseImpl();
		return lDtCAssertFalse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCAssertTrue createLDtCAssertTrue() {
		LDtCAssertTrueImpl lDtCAssertTrue = new LDtCAssertTrueImpl();
		return lDtCAssertTrue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCDecimalMax createLDtCDecimalMax() {
		LDtCDecimalMaxImpl lDtCDecimalMax = new LDtCDecimalMaxImpl();
		return lDtCDecimalMax;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCDecimalMin createLDtCDecimalMin() {
		LDtCDecimalMinImpl lDtCDecimalMin = new LDtCDecimalMinImpl();
		return lDtCDecimalMin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCDigits createLDtCDigits() {
		LDtCDigitsImpl lDtCDigits = new LDtCDigitsImpl();
		return lDtCDigits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCFuture createLDtCFuture() {
		LDtCFutureImpl lDtCFuture = new LDtCFutureImpl();
		return lDtCFuture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCPast createLDtCPast() {
		LDtCPastImpl lDtCPast = new LDtCPastImpl();
		return lDtCPast;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCNumericMax createLDtCNumericMax() {
		LDtCNumericMaxImpl lDtCNumericMax = new LDtCNumericMaxImpl();
		return lDtCNumericMax;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCNumericMin createLDtCNumericMin() {
		LDtCNumericMinImpl lDtCNumericMin = new LDtCNumericMinImpl();
		return lDtCNumericMin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCNotNull createLDtCNotNull() {
		LDtCNotNullImpl lDtCNotNull = new LDtCNotNullImpl();
		return lDtCNotNull;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCNull createLDtCNull() {
		LDtCNullImpl lDtCNull = new LDtCNullImpl();
		return lDtCNull;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCRegEx createLDtCRegEx() {
		LDtCRegExImpl lDtCRegEx = new LDtCRegExImpl();
		return lDtCRegEx;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtCSize createLDtCSize() {
		LDtCSizeImpl lDtCSize = new LDtCSizeImpl();
		return lDtCSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDateType createLDateTypeFromString(EDataType eDataType, String initialValue) {
		LDateType result = LDateType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLDateTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LVisibility createLVisibilityFromString(EDataType eDataType, String initialValue) {
		LVisibility result = LVisibility.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLVisibilityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LLowerBound createLLowerBoundFromString(EDataType eDataType, String initialValue) {
		LLowerBound result = LLowerBound.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLLowerBoundToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LUpperBound createLUpperBoundFromString(EDataType eDataType, String initialValue) {
		LUpperBound result = LUpperBound.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLUpperBoundToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LComparatorType createLComparatorTypeFromString(EDataType eDataType, String initialValue) {
		LComparatorType result = LComparatorType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLComparatorTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LConstraintSeverity createLConstraintSeverityFromString(EDataType eDataType, String initialValue) {
		LConstraintSeverity result = LConstraintSeverity.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLConstraintSeverityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LOperation> createOperationsListFromString(EDataType eDataType, String initialValue) {
		return (List<LOperation>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationsListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<? extends LFeature> createFeaturesListFromString(EDataType eDataType, String initialValue) {
		return (List<? extends LFeature>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFeaturesListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<LAnnotationDef> createAnnotationListFromString(EDataType eDataType, String initialValue) {
		return (EList<LAnnotationDef>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAnnotationListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalEObject createInternalEObjectFromString(EDataType eDataType, String initialValue) {
		return (InternalEObject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInternalEObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPTypesPackage getOSBPTypesPackage() {
		return (OSBPTypesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OSBPTypesPackage getPackage() {
		return OSBPTypesPackage.eINSTANCE;
	}

} //OSBPTypesFactoryImpl
