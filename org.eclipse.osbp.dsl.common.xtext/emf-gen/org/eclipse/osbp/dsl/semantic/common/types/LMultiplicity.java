/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LMultiplicity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getLower <em>Lower</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getUpper <em>Upper</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getToMultiplicityString <em>To Multiplicity String</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLMultiplicity()
 * @generated
 */
public interface LMultiplicity extends EObject {
	/**
	 * Returns the value of the '<em><b>Lower</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.dsl.semantic.common.types.LLowerBound}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LLowerBound
	 * @see #setLower(LLowerBound)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLMultiplicity_Lower()
	 * @generated
	 */
	LLowerBound getLower();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getLower <em>Lower</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LLowerBound
	 * @see #getLower()
	 * @generated
	 */
	void setLower(LLowerBound value);

	/**
	 * Returns the value of the '<em><b>Upper</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.dsl.semantic.common.types.LUpperBound}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LUpperBound
	 * @see #setUpper(LUpperBound)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLMultiplicity_Upper()
	 * @generated
	 */
	LUpperBound getUpper();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getUpper <em>Upper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LUpperBound
	 * @see #getUpper()
	 * @generated
	 */
	void setUpper(LUpperBound value);

	/**
	 * Returns the value of the '<em><b>To Multiplicity String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Multiplicity String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Multiplicity String</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLMultiplicity_ToMultiplicityString()
	 * @generated
	 */
	String getToMultiplicityString();

} // LMultiplicity
