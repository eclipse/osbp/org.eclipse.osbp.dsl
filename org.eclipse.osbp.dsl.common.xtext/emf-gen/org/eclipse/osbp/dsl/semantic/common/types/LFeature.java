/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LFeature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getAnnotationInfo <em>Annotation Info</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLFeature()
 * @generated
 */
public interface LFeature extends LAnnotationTarget {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLFeature_Name()
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(LMultiplicity)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLFeature_Multiplicity()
	 * @generated
	 */
	LMultiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(LMultiplicity value);

	/**
	 * Returns the value of the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Info</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Info</em>' containment reference.
	 * @see #setAnnotationInfo(LAnnotationTarget)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLFeature_AnnotationInfo()
	 * @generated
	 */
	LAnnotationTarget getAnnotationInfo();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getAnnotationInfo <em>Annotation Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Info</em>' containment reference.
	 * @see #getAnnotationInfo()
	 * @generated
	 */
	void setAnnotationInfo(LAnnotationTarget value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Delegates the annotation request to the target element.
	 * <!-- end-model-doc -->
	 * @generated
	 */
	EList<LAnnotationDef> getResolvedAnnotations();

} // LFeature
