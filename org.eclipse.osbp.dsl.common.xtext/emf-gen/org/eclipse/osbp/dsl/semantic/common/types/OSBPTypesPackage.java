/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesFactory
 * @generated
 */
public interface OSBPTypesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "types";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/dsl/common/types/v1";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "types";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OSBPTypesPackage eINSTANCE = org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LLazyResolverImpl <em>LLazy Resolver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LLazyResolverImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLLazyResolver()
	 * @generated
	 */
	int LLAZY_RESOLVER = 1;

	/**
	 * The number of structural features of the '<em>LLazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLAZY_RESOLVER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = 0;

	/**
	 * The number of operations of the '<em>LLazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLAZY_RESOLVER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LCommonModelImpl <em>LCommon Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LCommonModelImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLCommonModel()
	 * @generated
	 */
	int LCOMMON_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCOMMON_MODEL__IMPORT_SECTION = LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCOMMON_MODEL__PACKAGES = LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LCommon Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCOMMON_MODEL_FEATURE_COUNT = LLAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCOMMON_MODEL___ERESOLVE_PROXY__INTERNALEOBJECT = LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LCommon Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCOMMON_MODEL_OPERATION_COUNT = LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LPackageImpl <em>LPackage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LPackageImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLPackage()
	 * @generated
	 */
	int LPACKAGE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LPACKAGE__NAME = LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LPackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LPACKAGE_FEATURE_COUNT = LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LPACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT = LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LPackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LPACKAGE_OPERATION_COUNT = LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LTypedPackageImpl <em>LTyped Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LTypedPackageImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLTypedPackage()
	 * @generated
	 */
	int LTYPED_PACKAGE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPED_PACKAGE__NAME = LPACKAGE__NAME;

	/**
	 * The feature id for the '<em><b>Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPED_PACKAGE__TYPES = LPACKAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LTyped Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPED_PACKAGE_FEATURE_COUNT = LPACKAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPED_PACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT = LPACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resource Simple Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPED_PACKAGE___GET_RESOURCE_SIMPLE_NAME = LPACKAGE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LTyped Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPED_PACKAGE_OPERATION_COUNT = LPACKAGE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LImportImpl <em>LImport</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LImportImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLImport()
	 * @generated
	 */
	int LIMPORT = 4;

	/**
	 * The feature id for the '<em><b>Imported Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMPORT__IMPORTED_NAMESPACE = 0;

	/**
	 * The number of structural features of the '<em>LImport</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMPORT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>LImport</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIMPORT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LAnnotationTargetImpl <em>LAnnotation Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LAnnotationTargetImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLAnnotationTarget()
	 * @generated
	 */
	int LANNOTATION_TARGET = 7;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANNOTATION_TARGET__ANNOTATIONS = LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LAnnotation Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANNOTATION_TARGET_FEATURE_COUNT = LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANNOTATION_TARGET___ERESOLVE_PROXY__INTERNALEOBJECT = LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LAnnotation Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANNOTATION_TARGET_OPERATION_COUNT = LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LTypeImpl <em>LType</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LTypeImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLType()
	 * @generated
	 */
	int LTYPE = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPE__ANNOTATIONS = LANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPE__NAME = LANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPE__ANNOTATION_INFO = LANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LType</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPE_FEATURE_COUNT = LANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPE___ERESOLVE_PROXY__INTERNALEOBJECT = LANNOTATION_TARGET___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPE___GET_RESOLVED_ANNOTATIONS = LANNOTATION_TARGET_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LType</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTYPE_OPERATION_COUNT = LANNOTATION_TARGET_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LAnnotationDefImpl <em>LAnnotation Def</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LAnnotationDefImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLAnnotationDef()
	 * @generated
	 */
	int LANNOTATION_DEF = 6;

	/**
	 * The feature id for the '<em><b>Exclude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANNOTATION_DEF__EXCLUDE = LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANNOTATION_DEF__ANNOTATION = LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LAnnotation Def</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANNOTATION_DEF_FEATURE_COUNT = LLAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANNOTATION_DEF___ERESOLVE_PROXY__INTERNALEOBJECT = LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LAnnotation Def</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANNOTATION_DEF_OPERATION_COUNT = LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LScalarTypeImpl <em>LScalar Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LScalarTypeImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLScalarType()
	 * @generated
	 */
	int LSCALAR_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSCALAR_TYPE__ANNOTATIONS = LTYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSCALAR_TYPE__NAME = LTYPE__NAME;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSCALAR_TYPE__ANNOTATION_INFO = LTYPE__ANNOTATION_INFO;

	/**
	 * The number of structural features of the '<em>LScalar Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSCALAR_TYPE_FEATURE_COUNT = LTYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSCALAR_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT = LTYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSCALAR_TYPE___GET_RESOLVED_ANNOTATIONS = LTYPE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The number of operations of the '<em>LScalar Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSCALAR_TYPE_OPERATION_COUNT = LTYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDataTypeImpl <em>LData Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDataTypeImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDataType()
	 * @generated
	 */
	int LDATA_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__ANNOTATIONS = LSCALAR_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__NAME = LSCALAR_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__ANNOTATION_INFO = LSCALAR_TYPE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Jvm Type Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__JVM_TYPE_REFERENCE = LSCALAR_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>As Primitive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__AS_PRIMITIVE = LSCALAR_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__DATE = LSCALAR_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>As Blob</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__AS_BLOB = LSCALAR_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__LENGTH = LSCALAR_TYPE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Date Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__DATE_TYPE = LSCALAR_TYPE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Synthetic Flag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__SYNTHETIC_FLAG = LSCALAR_TYPE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Synthetic Selector</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__SYNTHETIC_SELECTOR = LSCALAR_TYPE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Synthetic Type Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__SYNTHETIC_TYPE_REFERENCE = LSCALAR_TYPE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Synthetic Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__SYNTHETIC_TYPE = LSCALAR_TYPE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__PROPERTIES = LSCALAR_TYPE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE__CONSTRAINTS = LSCALAR_TYPE_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>LData Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE_FEATURE_COUNT = LSCALAR_TYPE_FEATURE_COUNT + 12;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT = LSCALAR_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE___GET_RESOLVED_ANNOTATIONS = LSCALAR_TYPE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The number of operations of the '<em>LData Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATA_TYPE_OPERATION_COUNT = LSCALAR_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LEnumImpl <em>LEnum</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LEnumImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLEnum()
	 * @generated
	 */
	int LENUM = 10;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM__ANNOTATIONS = LSCALAR_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM__NAME = LSCALAR_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM__ANNOTATION_INFO = LSCALAR_TYPE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Literals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM__LITERALS = LSCALAR_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LEnum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_FEATURE_COUNT = LSCALAR_TYPE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM___ERESOLVE_PROXY__INTERNALEOBJECT = LSCALAR_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM___GET_RESOLVED_ANNOTATIONS = LSCALAR_TYPE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The number of operations of the '<em>LEnum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_OPERATION_COUNT = LSCALAR_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LEnumLiteralImpl <em>LEnum Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LEnumLiteralImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLEnumLiteral()
	 * @generated
	 */
	int LENUM_LITERAL = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_LITERAL__NAME = LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Default</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_LITERAL__DEFAULT = LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Null</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_LITERAL__NULL = LLAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_LITERAL__VALUE = LLAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_LITERAL__STRING_VALUE = LLAZY_RESOLVER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>LEnum Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_LITERAL_FEATURE_COUNT = LLAZY_RESOLVER_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_LITERAL___ERESOLVE_PROXY__INTERNALEOBJECT = LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LEnum Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENUM_LITERAL_OPERATION_COUNT = LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LStateClassImpl <em>LState Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LStateClassImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLStateClass()
	 * @generated
	 */
	int LSTATE_CLASS = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_CLASS__ANNOTATIONS = LSCALAR_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_CLASS__NAME = LSCALAR_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_CLASS__ANNOTATION_INFO = LSCALAR_TYPE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_CLASS__STATES = LSCALAR_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LState Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_CLASS_FEATURE_COUNT = LSCALAR_TYPE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_CLASS___ERESOLVE_PROXY__INTERNALEOBJECT = LSCALAR_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_CLASS___GET_RESOLVED_ANNOTATIONS = LSCALAR_TYPE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The number of operations of the '<em>LState Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_CLASS_OPERATION_COUNT = LSCALAR_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LStateImpl <em>LState</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LStateImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLState()
	 * @generated
	 */
	int LSTATE = 13;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE__ANNOTATIONS = LTYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE__NAME = LTYPE__NAME;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE__ANNOTATION_INFO = LTYPE__ANNOTATION_INFO;

	/**
	 * The number of structural features of the '<em>LState</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_FEATURE_COUNT = LTYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE___ERESOLVE_PROXY__INTERNALEOBJECT = LTYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE___GET_RESOLVED_ANNOTATIONS = LTYPE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The number of operations of the '<em>LState</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTATE_OPERATION_COUNT = LTYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LClassImpl <em>LClass</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LClassImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLClass()
	 * @generated
	 */
	int LCLASS = 14;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS__ANNOTATIONS = LTYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS__NAME = LTYPE__NAME;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS__ANNOTATION_INFO = LTYPE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS__ABSTRACT = LTYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Serializable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS__SERIALIZABLE = LTYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS__SHORT_NAME = LTYPE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>LClass</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS_FEATURE_COUNT = LTYPE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS___ERESOLVE_PROXY__INTERNALEOBJECT = LTYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS___GET_RESOLVED_ANNOTATIONS = LTYPE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Is Normal Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS___IS_NORMAL_ATTRIBUTE__LFEATURE = LTYPE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Hist Current Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS___IS_HIST_CURRENT_ATTRIBUTE__LFEATURE = LTYPE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>LClass</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCLASS_OPERATION_COUNT = LTYPE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder <em>LFeatures Holder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLFeaturesHolder()
	 * @generated
	 */
	int LFEATURES_HOLDER = 15;

	/**
	 * The number of structural features of the '<em>LFeatures Holder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURES_HOLDER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURES_HOLDER___GET_FEATURES = 0;

	/**
	 * The operation id for the '<em>Get All Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURES_HOLDER___GET_ALL_FEATURES = 1;

	/**
	 * The number of operations of the '<em>LFeatures Holder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURES_HOLDER_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature <em>LFeature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeature
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLFeature()
	 * @generated
	 */
	int LFEATURE = 16;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURE__ANNOTATIONS = LANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURE__NAME = LANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURE__MULTIPLICITY = LANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURE__ANNOTATION_INFO = LANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>LFeature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURE_FEATURE_COUNT = LANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURE___ERESOLVE_PROXY__INTERNALEOBJECT = LANNOTATION_TARGET___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURE___GET_RESOLVED_ANNOTATIONS = LANNOTATION_TARGET_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LFeature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LFEATURE_OPERATION_COUNT = LANNOTATION_TARGET_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference <em>LReference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLReference()
	 * @generated
	 */
	int LREFERENCE = 17;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__ANNOTATIONS = LFEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__NAME = LFEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__MULTIPLICITY = LFEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__ANNOTATION_INFO = LFEATURE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Lazy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__LAZY = LFEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cascade Merge Persist</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__CASCADE_MERGE_PERSIST = LFEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cascade Remove</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__CASCADE_REMOVE = LFEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cascade Refresh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__CASCADE_REFRESH = LFEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__PROPERTIES = LFEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__CONSTRAINTS = LFEATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Is Grouped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__IS_GROUPED = LFEATURE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__GROUP_NAME = LFEATURE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>As Grid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__AS_GRID = LFEATURE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>As Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__AS_TABLE = LFEATURE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Side Kick</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__SIDE_KICK = LFEATURE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Reference Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__REFERENCE_HIDDEN = LFEATURE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Reference Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__REFERENCE_READ_ONLY = LFEATURE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Historized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE__HISTORIZED = LFEATURE_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>LReference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE_FEATURE_COUNT = LFEATURE_FEATURE_COUNT + 14;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE___ERESOLVE_PROXY__INTERNALEOBJECT = LFEATURE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE___GET_RESOLVED_ANNOTATIONS = LFEATURE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The number of operations of the '<em>LReference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREFERENCE_OPERATION_COUNT = LFEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute <em>LAttribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLAttribute()
	 * @generated
	 */
	int LATTRIBUTE = 18;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__ANNOTATIONS = LFEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__NAME = LFEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__MULTIPLICITY = LFEATURE__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Annotation Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__ANNOTATION_INFO = LFEATURE__ANNOTATION_INFO;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__ID = LFEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__UUID = LFEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__VERSION = LFEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Lazy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__LAZY = LFEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Transient</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__TRANSIENT = LFEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Derived</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__DERIVED = LFEATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Dirty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__DIRTY = LFEATURE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Domain Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__DOMAIN_KEY = LFEATURE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Domain Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__DOMAIN_DESCRIPTION = LFEATURE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__FILTERING = LFEATURE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Range Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__RANGE_FILTERING = LFEATURE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Unique Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__UNIQUE_ENTRY = LFEATURE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Attribute Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__ATTRIBUTE_HIDDEN = LFEATURE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Attribute Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__ATTRIBUTE_READ_ONLY = LFEATURE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Extra Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__EXTRA_STYLE = LFEATURE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Derived Getter Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__DERIVED_GETTER_EXPRESSION = LFEATURE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__TYPE = LFEATURE_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__PROPERTIES = LFEATURE_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__CONSTRAINTS = LFEATURE_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Is Grouped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__IS_GROUPED = LFEATURE_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE__GROUP_NAME = LFEATURE_FEATURE_COUNT + 20;

	/**
	 * The number of structural features of the '<em>LAttribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE_FEATURE_COUNT = LFEATURE_FEATURE_COUNT + 21;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE___ERESOLVE_PROXY__INTERNALEOBJECT = LFEATURE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE___GET_RESOLVED_ANNOTATIONS = LFEATURE___GET_RESOLVED_ANNOTATIONS;

	/**
	 * The number of operations of the '<em>LAttribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE_OPERATION_COUNT = LFEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LKeyAndValueImpl <em>LKey And Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LKeyAndValueImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLKeyAndValue()
	 * @generated
	 */
	int LKEY_AND_VALUE = 19;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKEY_AND_VALUE__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKEY_AND_VALUE__VALUE = 1;

	/**
	 * The number of structural features of the '<em>LKey And Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKEY_AND_VALUE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>LKey And Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LKEY_AND_VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LOperationImpl <em>LOperation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LOperationImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLOperation()
	 * @generated
	 */
	int LOPERATION = 20;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__ANNOTATIONS = LANNOTATION_TARGET__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Modifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__MODIFIER = LANNOTATION_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__TYPE = LANNOTATION_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__PARAMS = LANNOTATION_TARGET_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION__BODY = LANNOTATION_TARGET_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>LOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION_FEATURE_COUNT = LANNOTATION_TARGET_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION___ERESOLVE_PROXY__INTERNALEOBJECT = LANNOTATION_TARGET___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Get Resolved Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION___GET_RESOLVED_ANNOTATIONS = LANNOTATION_TARGET_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOPERATION_OPERATION_COUNT = LANNOTATION_TARGET_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LModifierImpl <em>LModifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LModifierImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLModifier()
	 * @generated
	 */
	int LMODIFIER = 21;

	/**
	 * The feature id for the '<em><b>Final</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMODIFIER__FINAL = LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMODIFIER__STATIC = LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMODIFIER__VISIBILITY = LLAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>LModifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMODIFIER_FEATURE_COUNT = LLAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMODIFIER___ERESOLVE_PROXY__INTERNALEOBJECT = LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LModifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMODIFIER_OPERATION_COUNT = LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LMultiplicityImpl <em>LMultiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LMultiplicityImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLMultiplicity()
	 * @generated
	 */
	int LMULTIPLICITY = 22;

	/**
	 * The feature id for the '<em><b>Lower</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMULTIPLICITY__LOWER = 0;

	/**
	 * The feature id for the '<em><b>Upper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMULTIPLICITY__UPPER = 1;

	/**
	 * The feature id for the '<em><b>To Multiplicity String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMULTIPLICITY__TO_MULTIPLICITY_STRING = 2;

	/**
	 * The number of structural features of the '<em>LMultiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMULTIPLICITY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>LMultiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMULTIPLICITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LConstraint <em>LConstraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LConstraint
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLConstraint()
	 * @generated
	 */
	int LCONSTRAINT = 23;

	/**
	 * The number of structural features of the '<em>LConstraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONSTRAINT_FEATURE_COUNT = LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT = LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LConstraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONSTRAINT_OPERATION_COUNT = LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LResultFiltersImpl <em>LResult Filters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LResultFiltersImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLResultFilters()
	 * @generated
	 */
	int LRESULT_FILTERS = 24;

	/**
	 * The feature id for the '<em><b>Result Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRESULT_FILTERS__RESULT_FILTERS = LLAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LResult Filters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRESULT_FILTERS_FEATURE_COUNT = LLAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRESULT_FILTERS___ERESOLVE_PROXY__INTERNALEOBJECT = LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LResult Filters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRESULT_FILTERS_OPERATION_COUNT = LLAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LResultFilter <em>LResult Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LResultFilter
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLResultFilter()
	 * @generated
	 */
	int LRESULT_FILTER = 25;

	/**
	 * The number of structural features of the '<em>LResult Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRESULT_FILTER_FEATURE_COUNT = LCONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRESULT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT = LCONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LResult Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRESULT_FILTER_OPERATION_COUNT = LCONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LAttributeMatchingConstraintImpl <em>LAttribute Matching Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LAttributeMatchingConstraintImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLAttributeMatchingConstraint()
	 * @generated
	 */
	int LATTRIBUTE_MATCHING_CONSTRAINT = 26;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE_MATCHING_CONSTRAINT__ATTRIBUTE = LRESULT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comparator Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE_MATCHING_CONSTRAINT__COMPARATOR_TYPE = LRESULT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Matching Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE_MATCHING_CONSTRAINT__MATCHING_VALUE = LRESULT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Matching Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE_MATCHING_CONSTRAINT__MATCHING_LITERAL = LRESULT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>LAttribute Matching Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE_MATCHING_CONSTRAINT_FEATURE_COUNT = LRESULT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE_MATCHING_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT = LRESULT_FILTER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>LAttribute Matching Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LATTRIBUTE_MATCHING_CONSTRAINT_OPERATION_COUNT = LRESULT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint <em>LDatatype Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDatatypeConstraint()
	 * @generated
	 */
	int LDATATYPE_CONSTRAINT = 27;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATATYPE_CONSTRAINT__MSG_CODE = LCONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATATYPE_CONSTRAINT__MSG_I1_8N_KEY = LCONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATATYPE_CONSTRAINT__SEVERITY = LCONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>LDatatype Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATATYPE_CONSTRAINT_FEATURE_COUNT = LCONSTRAINT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATATYPE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT = LCONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATATYPE_CONSTRAINT___IS_FOR_PRIMITIVES = LCONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDatatype Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATATYPE_CONSTRAINT_OPERATION_COUNT = LCONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LStringConstraint <em>LString Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LStringConstraint
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLStringConstraint()
	 * @generated
	 */
	int LSTRING_CONSTRAINT = 28;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTRING_CONSTRAINT__MSG_CODE = LDATATYPE_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTRING_CONSTRAINT__MSG_I1_8N_KEY = LDATATYPE_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTRING_CONSTRAINT__SEVERITY = LDATATYPE_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LString Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTRING_CONSTRAINT_FEATURE_COUNT = LDATATYPE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTRING_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT = LDATATYPE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTRING_CONSTRAINT___IS_FOR_PRIMITIVES = LDATATYPE_CONSTRAINT___IS_FOR_PRIMITIVES;

	/**
	 * The number of operations of the '<em>LString Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTRING_CONSTRAINT_OPERATION_COUNT = LDATATYPE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LNumericConstraint <em>LNumeric Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LNumericConstraint
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLNumericConstraint()
	 * @generated
	 */
	int LNUMERIC_CONSTRAINT = 29;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNUMERIC_CONSTRAINT__MSG_CODE = LDATATYPE_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNUMERIC_CONSTRAINT__MSG_I1_8N_KEY = LDATATYPE_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNUMERIC_CONSTRAINT__SEVERITY = LDATATYPE_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LNumeric Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNUMERIC_CONSTRAINT_FEATURE_COUNT = LDATATYPE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNUMERIC_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT = LDATATYPE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNUMERIC_CONSTRAINT___IS_FOR_PRIMITIVES = LDATATYPE_CONSTRAINT___IS_FOR_PRIMITIVES;

	/**
	 * The number of operations of the '<em>LNumeric Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNUMERIC_CONSTRAINT_OPERATION_COUNT = LDATATYPE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDecimalConstraint <em>LDecimal Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDecimalConstraint
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDecimalConstraint()
	 * @generated
	 */
	int LDECIMAL_CONSTRAINT = 30;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDECIMAL_CONSTRAINT__MSG_CODE = LDATATYPE_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDECIMAL_CONSTRAINT__MSG_I1_8N_KEY = LDATATYPE_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDECIMAL_CONSTRAINT__SEVERITY = LDATATYPE_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LDecimal Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDECIMAL_CONSTRAINT_FEATURE_COUNT = LDATATYPE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDECIMAL_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT = LDATATYPE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDECIMAL_CONSTRAINT___IS_FOR_PRIMITIVES = LDATATYPE_CONSTRAINT___IS_FOR_PRIMITIVES;

	/**
	 * The number of operations of the '<em>LDecimal Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDECIMAL_CONSTRAINT_OPERATION_COUNT = LDATATYPE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDateConstraint <em>LDate Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateConstraint
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDateConstraint()
	 * @generated
	 */
	int LDATE_CONSTRAINT = 31;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATE_CONSTRAINT__MSG_CODE = LDATATYPE_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATE_CONSTRAINT__MSG_I1_8N_KEY = LDATATYPE_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATE_CONSTRAINT__SEVERITY = LDATATYPE_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LDate Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATE_CONSTRAINT_FEATURE_COUNT = LDATATYPE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT = LDATATYPE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATE_CONSTRAINT___IS_FOR_PRIMITIVES = LDATATYPE_CONSTRAINT___IS_FOR_PRIMITIVES;

	/**
	 * The number of operations of the '<em>LDate Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDATE_CONSTRAINT_OPERATION_COUNT = LDATATYPE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LBooleanConstraint <em>LBoolean Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LBooleanConstraint
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLBooleanConstraint()
	 * @generated
	 */
	int LBOOLEAN_CONSTRAINT = 32;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBOOLEAN_CONSTRAINT__MSG_CODE = LDATATYPE_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBOOLEAN_CONSTRAINT__MSG_I1_8N_KEY = LDATATYPE_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBOOLEAN_CONSTRAINT__SEVERITY = LDATATYPE_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LBoolean Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBOOLEAN_CONSTRAINT_FEATURE_COUNT = LDATATYPE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBOOLEAN_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT = LDATATYPE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBOOLEAN_CONSTRAINT___IS_FOR_PRIMITIVES = LDATATYPE_CONSTRAINT___IS_FOR_PRIMITIVES;

	/**
	 * The number of operations of the '<em>LBoolean Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBOOLEAN_CONSTRAINT_OPERATION_COUNT = LDATATYPE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LBlobConstraint <em>LBlob Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LBlobConstraint
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLBlobConstraint()
	 * @generated
	 */
	int LBLOB_CONSTRAINT = 33;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBLOB_CONSTRAINT__MSG_CODE = LDATATYPE_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBLOB_CONSTRAINT__MSG_I1_8N_KEY = LDATATYPE_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBLOB_CONSTRAINT__SEVERITY = LDATATYPE_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LBlob Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBLOB_CONSTRAINT_FEATURE_COUNT = LDATATYPE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBLOB_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT = LDATATYPE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBLOB_CONSTRAINT___IS_FOR_PRIMITIVES = LDATATYPE_CONSTRAINT___IS_FOR_PRIMITIVES;

	/**
	 * The number of operations of the '<em>LBlob Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LBLOB_CONSTRAINT_OPERATION_COUNT = LDATATYPE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCAssertFalseImpl <em>LDt CAssert False</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCAssertFalseImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCAssertFalse()
	 * @generated
	 */
	int LDT_CASSERT_FALSE = 34;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_FALSE__MSG_CODE = LBOOLEAN_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_FALSE__MSG_I1_8N_KEY = LBOOLEAN_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_FALSE__SEVERITY = LBOOLEAN_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LDt CAssert False</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_FALSE_FEATURE_COUNT = LBOOLEAN_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_FALSE___ERESOLVE_PROXY__INTERNALEOBJECT = LBOOLEAN_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_FALSE___IS_FOR_PRIMITIVES = LBOOLEAN_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CAssert False</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_FALSE_OPERATION_COUNT = LBOOLEAN_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCAssertTrueImpl <em>LDt CAssert True</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCAssertTrueImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCAssertTrue()
	 * @generated
	 */
	int LDT_CASSERT_TRUE = 35;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_TRUE__MSG_CODE = LBOOLEAN_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_TRUE__MSG_I1_8N_KEY = LBOOLEAN_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_TRUE__SEVERITY = LBOOLEAN_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LDt CAssert True</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_TRUE_FEATURE_COUNT = LBOOLEAN_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_TRUE___ERESOLVE_PROXY__INTERNALEOBJECT = LBOOLEAN_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_TRUE___IS_FOR_PRIMITIVES = LBOOLEAN_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CAssert True</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CASSERT_TRUE_OPERATION_COUNT = LBOOLEAN_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDecimalMaxImpl <em>LDt CDecimal Max</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDecimalMaxImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCDecimalMax()
	 * @generated
	 */
	int LDT_CDECIMAL_MAX = 36;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MAX__MSG_CODE = LDECIMAL_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MAX__MSG_I1_8N_KEY = LDECIMAL_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MAX__SEVERITY = LDECIMAL_CONSTRAINT__SEVERITY;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MAX__MAX = LDECIMAL_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LDt CDecimal Max</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MAX_FEATURE_COUNT = LDECIMAL_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MAX___ERESOLVE_PROXY__INTERNALEOBJECT = LDECIMAL_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MAX___IS_FOR_PRIMITIVES = LDECIMAL_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CDecimal Max</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MAX_OPERATION_COUNT = LDECIMAL_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDecimalMinImpl <em>LDt CDecimal Min</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDecimalMinImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCDecimalMin()
	 * @generated
	 */
	int LDT_CDECIMAL_MIN = 37;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MIN__MSG_CODE = LDECIMAL_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MIN__MSG_I1_8N_KEY = LDECIMAL_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MIN__SEVERITY = LDECIMAL_CONSTRAINT__SEVERITY;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MIN__MIN = LDECIMAL_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LDt CDecimal Min</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MIN_FEATURE_COUNT = LDECIMAL_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MIN___ERESOLVE_PROXY__INTERNALEOBJECT = LDECIMAL_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MIN___IS_FOR_PRIMITIVES = LDECIMAL_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CDecimal Min</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDECIMAL_MIN_OPERATION_COUNT = LDECIMAL_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDigitsImpl <em>LDt CDigits</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDigitsImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCDigits()
	 * @generated
	 */
	int LDT_CDIGITS = 38;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDIGITS__MSG_CODE = LDECIMAL_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDIGITS__MSG_I1_8N_KEY = LDECIMAL_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDIGITS__SEVERITY = LDECIMAL_CONSTRAINT__SEVERITY;

	/**
	 * The feature id for the '<em><b>Int Digits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDIGITS__INT_DIGITS = LDECIMAL_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fraction Digits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDIGITS__FRACTION_DIGITS = LDECIMAL_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LDt CDigits</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDIGITS_FEATURE_COUNT = LDECIMAL_CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDIGITS___ERESOLVE_PROXY__INTERNALEOBJECT = LDECIMAL_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDIGITS___IS_FOR_PRIMITIVES = LDECIMAL_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CDigits</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CDIGITS_OPERATION_COUNT = LDECIMAL_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCFutureImpl <em>LDt CFuture</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCFutureImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCFuture()
	 * @generated
	 */
	int LDT_CFUTURE = 39;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CFUTURE__MSG_CODE = LDATE_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CFUTURE__MSG_I1_8N_KEY = LDATE_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CFUTURE__SEVERITY = LDATE_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LDt CFuture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CFUTURE_FEATURE_COUNT = LDATE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CFUTURE___ERESOLVE_PROXY__INTERNALEOBJECT = LDATE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CFUTURE___IS_FOR_PRIMITIVES = LDATE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CFuture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CFUTURE_OPERATION_COUNT = LDATE_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCPastImpl <em>LDt CPast</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCPastImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCPast()
	 * @generated
	 */
	int LDT_CPAST = 40;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CPAST__MSG_CODE = LDATE_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CPAST__MSG_I1_8N_KEY = LDATE_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CPAST__SEVERITY = LDATE_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LDt CPast</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CPAST_FEATURE_COUNT = LDATE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CPAST___ERESOLVE_PROXY__INTERNALEOBJECT = LDATE_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CPAST___IS_FOR_PRIMITIVES = LDATE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CPast</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CPAST_OPERATION_COUNT = LDATE_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNumericMaxImpl <em>LDt CNumeric Max</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNumericMaxImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCNumericMax()
	 * @generated
	 */
	int LDT_CNUMERIC_MAX = 41;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MAX__MSG_CODE = LNUMERIC_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MAX__MSG_I1_8N_KEY = LNUMERIC_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MAX__SEVERITY = LNUMERIC_CONSTRAINT__SEVERITY;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MAX__MAX = LNUMERIC_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LDt CNumeric Max</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MAX_FEATURE_COUNT = LNUMERIC_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MAX___ERESOLVE_PROXY__INTERNALEOBJECT = LNUMERIC_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MAX___IS_FOR_PRIMITIVES = LNUMERIC_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CNumeric Max</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MAX_OPERATION_COUNT = LNUMERIC_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNumericMinImpl <em>LDt CNumeric Min</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNumericMinImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCNumericMin()
	 * @generated
	 */
	int LDT_CNUMERIC_MIN = 42;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MIN__MSG_CODE = LNUMERIC_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MIN__MSG_I1_8N_KEY = LNUMERIC_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MIN__SEVERITY = LNUMERIC_CONSTRAINT__SEVERITY;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MIN__MIN = LNUMERIC_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LDt CNumeric Min</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MIN_FEATURE_COUNT = LNUMERIC_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MIN___ERESOLVE_PROXY__INTERNALEOBJECT = LNUMERIC_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MIN___IS_FOR_PRIMITIVES = LNUMERIC_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CNumeric Min</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNUMERIC_MIN_OPERATION_COUNT = LNUMERIC_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNotNullImpl <em>LDt CNot Null</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNotNullImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCNotNull()
	 * @generated
	 */
	int LDT_CNOT_NULL = 43;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNOT_NULL__MSG_CODE = LBLOB_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNOT_NULL__MSG_I1_8N_KEY = LBLOB_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNOT_NULL__SEVERITY = LBLOB_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LDt CNot Null</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNOT_NULL_FEATURE_COUNT = LBLOB_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNOT_NULL___ERESOLVE_PROXY__INTERNALEOBJECT = LBLOB_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNOT_NULL___IS_FOR_PRIMITIVES = LBLOB_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CNot Null</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNOT_NULL_OPERATION_COUNT = LBLOB_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNullImpl <em>LDt CNull</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNullImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCNull()
	 * @generated
	 */
	int LDT_CNULL = 44;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNULL__MSG_CODE = LBLOB_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNULL__MSG_I1_8N_KEY = LBLOB_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNULL__SEVERITY = LBLOB_CONSTRAINT__SEVERITY;

	/**
	 * The number of structural features of the '<em>LDt CNull</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNULL_FEATURE_COUNT = LBLOB_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNULL___ERESOLVE_PROXY__INTERNALEOBJECT = LBLOB_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNULL___IS_FOR_PRIMITIVES = LBLOB_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CNull</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CNULL_OPERATION_COUNT = LBLOB_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCRegExImpl <em>LDt CReg Ex</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCRegExImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCRegEx()
	 * @generated
	 */
	int LDT_CREG_EX = 45;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CREG_EX__MSG_CODE = LSTRING_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CREG_EX__MSG_I1_8N_KEY = LSTRING_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CREG_EX__SEVERITY = LSTRING_CONSTRAINT__SEVERITY;

	/**
	 * The feature id for the '<em><b>Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CREG_EX__PATTERN = LSTRING_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LDt CReg Ex</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CREG_EX_FEATURE_COUNT = LSTRING_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CREG_EX___ERESOLVE_PROXY__INTERNALEOBJECT = LSTRING_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CREG_EX___IS_FOR_PRIMITIVES = LSTRING_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CReg Ex</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CREG_EX_OPERATION_COUNT = LSTRING_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCSizeImpl <em>LDt CSize</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCSizeImpl
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCSize()
	 * @generated
	 */
	int LDT_CSIZE = 46;

	/**
	 * The feature id for the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CSIZE__MSG_CODE = LSTRING_CONSTRAINT__MSG_CODE;

	/**
	 * The feature id for the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CSIZE__MSG_I1_8N_KEY = LSTRING_CONSTRAINT__MSG_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CSIZE__SEVERITY = LSTRING_CONSTRAINT__SEVERITY;

	/**
	 * The feature id for the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CSIZE__MIN = LSTRING_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CSIZE__MAX = LSTRING_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LDt CSize</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CSIZE_FEATURE_COUNT = LSTRING_CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CSIZE___ERESOLVE_PROXY__INTERNALEOBJECT = LSTRING_CONSTRAINT___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The operation id for the '<em>Is For Primitives</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CSIZE___IS_FOR_PRIMITIVES = LSTRING_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LDt CSize</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDT_CSIZE_OPERATION_COUNT = LSTRING_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDateType <em>LDate Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateType
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDateType()
	 * @generated
	 */
	int LDATE_TYPE = 47;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LVisibility <em>LVisibility</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LVisibility
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLVisibility()
	 * @generated
	 */
	int LVISIBILITY = 48;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LLowerBound <em>LLower Bound</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LLowerBound
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLLowerBound()
	 * @generated
	 */
	int LLOWER_BOUND = 49;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LUpperBound <em>LUpper Bound</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LUpperBound
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLUpperBound()
	 * @generated
	 */
	int LUPPER_BOUND = 50;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LComparatorType <em>LComparator Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LComparatorType
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLComparatorType()
	 * @generated
	 */
	int LCOMPARATOR_TYPE = 51;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LConstraintSeverity <em>LConstraint Severity</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LConstraintSeverity
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLConstraintSeverity()
	 * @generated
	 */
	int LCONSTRAINT_SEVERITY = 52;

	/**
	 * The meta object id for the '<em>Operations List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getOperationsList()
	 * @generated
	 */
	int OPERATIONS_LIST = 53;

	/**
	 * The meta object id for the '<em>Features List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getFeaturesList()
	 * @generated
	 */
	int FEATURES_LIST = 54;

	/**
	 * The meta object id for the '<em>Annotation List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.EList
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getAnnotationList()
	 * @generated
	 */
	int ANNOTATION_LIST = 55;

	/**
	 * The meta object id for the '<em>Internal EObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getInternalEObject()
	 * @generated
	 */
	int INTERNAL_EOBJECT = 56;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LCommonModel <em>LCommon Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LCommon Model</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LCommonModel
	 * @generated
	 */
	EClass getLCommonModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LCommonModel#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Section</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LCommonModel#getImportSection()
	 * @see #getLCommonModel()
	 * @generated
	 */
	EReference getLCommonModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LCommonModel#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Packages</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LCommonModel#getPackages()
	 * @see #getLCommonModel()
	 * @generated
	 */
	EReference getLCommonModel_Packages();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver <em>LLazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LLazy Resolver</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver
	 * @generated
	 */
	EClass getLLazyResolver();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject) <em>EResolve Proxy</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>EResolve Proxy</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject)
	 * @generated
	 */
	EOperation getLLazyResolver__EResolveProxy__InternalEObject();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LPackage <em>LPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LPackage</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LPackage
	 * @generated
	 */
	EClass getLPackage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LPackage#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LPackage#getName()
	 * @see #getLPackage()
	 * @generated
	 */
	EAttribute getLPackage_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage <em>LTyped Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LTyped Package</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage
	 * @generated
	 */
	EClass getLTypedPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Types</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage#getTypes()
	 * @see #getLTypedPackage()
	 * @generated
	 */
	EReference getLTypedPackage_Types();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage#getResourceSimpleName() <em>Get Resource Simple Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Resource Simple Name</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage#getResourceSimpleName()
	 * @generated
	 */
	EOperation getLTypedPackage__GetResourceSimpleName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LImport <em>LImport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LImport</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LImport
	 * @generated
	 */
	EClass getLImport();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LImport#getImportedNamespace <em>Imported Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Imported Namespace</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LImport#getImportedNamespace()
	 * @see #getLImport()
	 * @generated
	 */
	EAttribute getLImport_ImportedNamespace();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LType <em>LType</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LType</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LType
	 * @generated
	 */
	EClass getLType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LType#getName()
	 * @see #getLType()
	 * @generated
	 */
	EAttribute getLType_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LType#getAnnotationInfo <em>Annotation Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Info</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LType#getAnnotationInfo()
	 * @see #getLType()
	 * @generated
	 */
	EReference getLType_AnnotationInfo();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LType#getResolvedAnnotations() <em>Get Resolved Annotations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Resolved Annotations</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LType#getResolvedAnnotations()
	 * @generated
	 */
	EOperation getLType__GetResolvedAnnotations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef <em>LAnnotation Def</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LAnnotation Def</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef
	 * @generated
	 */
	EClass getLAnnotationDef();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef#isExclude <em>Exclude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exclude</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef#isExclude()
	 * @see #getLAnnotationDef()
	 * @generated
	 */
	EAttribute getLAnnotationDef_Exclude();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef#getAnnotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef#getAnnotation()
	 * @see #getLAnnotationDef()
	 * @generated
	 */
	EReference getLAnnotationDef_Annotation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget <em>LAnnotation Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LAnnotation Target</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget
	 * @generated
	 */
	EClass getLAnnotationTarget();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotations</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget#getAnnotations()
	 * @see #getLAnnotationTarget()
	 * @generated
	 */
	EReference getLAnnotationTarget_Annotations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LScalarType <em>LScalar Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LScalar Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LScalarType
	 * @generated
	 */
	EClass getLScalarType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType <em>LData Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LData Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType
	 * @generated
	 */
	EClass getLDataType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getJvmTypeReference <em>Jvm Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Jvm Type Reference</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#getJvmTypeReference()
	 * @see #getLDataType()
	 * @generated
	 */
	EReference getLDataType_JvmTypeReference();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isAsPrimitive <em>As Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>As Primitive</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#isAsPrimitive()
	 * @see #getLDataType()
	 * @generated
	 */
	EAttribute getLDataType_AsPrimitive();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#isDate()
	 * @see #getLDataType()
	 * @generated
	 */
	EAttribute getLDataType_Date();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isAsBlob <em>As Blob</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>As Blob</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#isAsBlob()
	 * @see #getLDataType()
	 * @generated
	 */
	EAttribute getLDataType_AsBlob();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getLength <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Length</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#getLength()
	 * @see #getLDataType()
	 * @generated
	 */
	EAttribute getLDataType_Length();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getDateType <em>Date Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#getDateType()
	 * @see #getLDataType()
	 * @generated
	 */
	EAttribute getLDataType_DateType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isSyntheticFlag <em>Synthetic Flag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synthetic Flag</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#isSyntheticFlag()
	 * @see #getLDataType()
	 * @generated
	 */
	EAttribute getLDataType_SyntheticFlag();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticSelector <em>Synthetic Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synthetic Selector</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticSelector()
	 * @see #getLDataType()
	 * @generated
	 */
	EAttribute getLDataType_SyntheticSelector();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticTypeReference <em>Synthetic Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synthetic Type Reference</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticTypeReference()
	 * @see #getLDataType()
	 * @generated
	 */
	EReference getLDataType_SyntheticTypeReference();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticType <em>Synthetic Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synthetic Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticType()
	 * @see #getLDataType()
	 * @generated
	 */
	EReference getLDataType_SyntheticType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#getProperties()
	 * @see #getLDataType()
	 * @generated
	 */
	EReference getLDataType_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDataType#getConstraints()
	 * @see #getLDataType()
	 * @generated
	 */
	EReference getLDataType_Constraints();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LEnum <em>LEnum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEnum</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LEnum
	 * @generated
	 */
	EClass getLEnum();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LEnum#getLiterals <em>Literals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Literals</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LEnum#getLiterals()
	 * @see #getLEnum()
	 * @generated
	 */
	EReference getLEnum_Literals();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral <em>LEnum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LEnum Literal</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral
	 * @generated
	 */
	EClass getLEnumLiteral();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#getName()
	 * @see #getLEnumLiteral()
	 * @generated
	 */
	EAttribute getLEnumLiteral_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#isDefault <em>Default</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#isDefault()
	 * @see #getLEnumLiteral()
	 * @generated
	 */
	EAttribute getLEnumLiteral_Default();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#isNull <em>Null</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Null</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#isNull()
	 * @see #getLEnumLiteral()
	 * @generated
	 */
	EAttribute getLEnumLiteral_Null();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#getValue()
	 * @see #getLEnumLiteral()
	 * @generated
	 */
	EAttribute getLEnumLiteral_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#getStringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Value</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral#getStringValue()
	 * @see #getLEnumLiteral()
	 * @generated
	 */
	EAttribute getLEnumLiteral_StringValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LStateClass <em>LState Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LState Class</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LStateClass
	 * @generated
	 */
	EClass getLStateClass();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LStateClass#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LStateClass#getStates()
	 * @see #getLStateClass()
	 * @generated
	 */
	EReference getLStateClass_States();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LState <em>LState</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LState</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LState
	 * @generated
	 */
	EClass getLState();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LClass <em>LClass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LClass</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LClass
	 * @generated
	 */
	EClass getLClass();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LClass#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LClass#isAbstract()
	 * @see #getLClass()
	 * @generated
	 */
	EAttribute getLClass_Abstract();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LClass#isSerializable <em>Serializable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Serializable</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LClass#isSerializable()
	 * @see #getLClass()
	 * @generated
	 */
	EAttribute getLClass_Serializable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LClass#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LClass#getShortName()
	 * @see #getLClass()
	 * @generated
	 */
	EAttribute getLClass_ShortName();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LClass#isNormalAttribute(org.eclipse.osbp.dsl.semantic.common.types.LFeature) <em>Is Normal Attribute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Normal Attribute</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LClass#isNormalAttribute(org.eclipse.osbp.dsl.semantic.common.types.LFeature)
	 * @generated
	 */
	EOperation getLClass__IsNormalAttribute__LFeature();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LClass#isHistCurrentAttribute(org.eclipse.osbp.dsl.semantic.common.types.LFeature) <em>Is Hist Current Attribute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Hist Current Attribute</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LClass#isHistCurrentAttribute(org.eclipse.osbp.dsl.semantic.common.types.LFeature)
	 * @generated
	 */
	EOperation getLClass__IsHistCurrentAttribute__LFeature();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder <em>LFeatures Holder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LFeatures Holder</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder
	 * @generated
	 */
	EClass getLFeaturesHolder();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder#getFeatures() <em>Get Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Features</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder#getFeatures()
	 * @generated
	 */
	EOperation getLFeaturesHolder__GetFeatures();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder#getAllFeatures() <em>Get All Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Features</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder#getAllFeatures()
	 * @generated
	 */
	EOperation getLFeaturesHolder__GetAllFeatures();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature <em>LFeature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LFeature</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeature
	 * @generated
	 */
	EClass getLFeature();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeature#getName()
	 * @see #getLFeature()
	 * @generated
	 */
	EAttribute getLFeature_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Multiplicity</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeature#getMultiplicity()
	 * @see #getLFeature()
	 * @generated
	 */
	EReference getLFeature_Multiplicity();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getAnnotationInfo <em>Annotation Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Info</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeature#getAnnotationInfo()
	 * @see #getLFeature()
	 * @generated
	 */
	EReference getLFeature_AnnotationInfo();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature#getResolvedAnnotations() <em>Get Resolved Annotations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Resolved Annotations</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeature#getResolvedAnnotations()
	 * @generated
	 */
	EOperation getLFeature__GetResolvedAnnotations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference <em>LReference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LReference</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference
	 * @generated
	 */
	EClass getLReference();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isLazy <em>Lazy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lazy</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isLazy()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_Lazy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeMergePersist <em>Cascade Merge Persist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cascade Merge Persist</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeMergePersist()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_CascadeMergePersist();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeRemove <em>Cascade Remove</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cascade Remove</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeRemove()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_CascadeRemove();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeRefresh <em>Cascade Refresh</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cascade Refresh</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeRefresh()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_CascadeRefresh();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#getProperties()
	 * @see #getLReference()
	 * @generated
	 */
	EReference getLReference_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#getConstraints()
	 * @see #getLReference()
	 * @generated
	 */
	EReference getLReference_Constraints();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isIsGrouped <em>Is Grouped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Grouped</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isIsGrouped()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_IsGrouped();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#getGroupName <em>Group Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Group Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#getGroupName()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_GroupName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isAsGrid <em>As Grid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>As Grid</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isAsGrid()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_AsGrid();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isAsTable <em>As Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>As Table</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isAsTable()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_AsTable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isSideKick <em>Side Kick</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Side Kick</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isSideKick()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_SideKick();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isReferenceHidden <em>Reference Hidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference Hidden</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isReferenceHidden()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_ReferenceHidden();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isReferenceReadOnly <em>Reference Read Only</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference Read Only</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isReferenceReadOnly()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_ReferenceReadOnly();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isHistorized <em>Historized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Historized</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference#isHistorized()
	 * @see #getLReference()
	 * @generated
	 */
	EAttribute getLReference_Historized();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute <em>LAttribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LAttribute</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute
	 * @generated
	 */
	EClass getLAttribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isId()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isUuid()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isVersion()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_Version();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isLazy <em>Lazy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lazy</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isLazy()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_Lazy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isTransient <em>Transient</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transient</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isTransient()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_Transient();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDerived <em>Derived</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDerived()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_Derived();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDirty <em>Dirty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dirty</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDirty()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_Dirty();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDomainKey <em>Domain Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domain Key</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDomainKey()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_DomainKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDomainDescription <em>Domain Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domain Description</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDomainDescription()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_DomainDescription();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isFiltering <em>Filtering</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filtering</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isFiltering()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_Filtering();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isRangeFiltering <em>Range Filtering</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range Filtering</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isRangeFiltering()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_RangeFiltering();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isUniqueEntry <em>Unique Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unique Entry</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isUniqueEntry()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_UniqueEntry();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isAttributeHidden <em>Attribute Hidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Hidden</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isAttributeHidden()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_AttributeHidden();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isAttributeReadOnly <em>Attribute Read Only</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Read Only</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isAttributeReadOnly()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_AttributeReadOnly();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getExtraStyle <em>Extra Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extra Style</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getExtraStyle()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_ExtraStyle();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getDerivedGetterExpression <em>Derived Getter Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Derived Getter Expression</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getDerivedGetterExpression()
	 * @see #getLAttribute()
	 * @generated
	 */
	EReference getLAttribute_DerivedGetterExpression();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getType()
	 * @see #getLAttribute()
	 * @generated
	 */
	EReference getLAttribute_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getProperties()
	 * @see #getLAttribute()
	 * @generated
	 */
	EReference getLAttribute_Properties();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getConstraints()
	 * @see #getLAttribute()
	 * @generated
	 */
	EReference getLAttribute_Constraints();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isIsGrouped <em>Is Grouped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Grouped</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isIsGrouped()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_IsGrouped();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getGroupName <em>Group Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Group Name</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getGroupName()
	 * @see #getLAttribute()
	 * @generated
	 */
	EAttribute getLAttribute_GroupName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue <em>LKey And Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LKey And Value</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue
	 * @generated
	 */
	EClass getLKeyAndValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue#getKey()
	 * @see #getLKeyAndValue()
	 * @generated
	 */
	EAttribute getLKeyAndValue_Key();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue#getValue()
	 * @see #getLKeyAndValue()
	 * @generated
	 */
	EAttribute getLKeyAndValue_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LOperation <em>LOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LOperation</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LOperation
	 * @generated
	 */
	EClass getLOperation();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LOperation#getModifier <em>Modifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Modifier</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LOperation#getModifier()
	 * @see #getLOperation()
	 * @generated
	 */
	EReference getLOperation_Modifier();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LOperation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LOperation#getType()
	 * @see #getLOperation()
	 * @generated
	 */
	EReference getLOperation_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LOperation#getParams <em>Params</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Params</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LOperation#getParams()
	 * @see #getLOperation()
	 * @generated
	 */
	EReference getLOperation_Params();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LOperation#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LOperation#getBody()
	 * @see #getLOperation()
	 * @generated
	 */
	EReference getLOperation_Body();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LOperation#getResolvedAnnotations() <em>Get Resolved Annotations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Resolved Annotations</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LOperation#getResolvedAnnotations()
	 * @generated
	 */
	EOperation getLOperation__GetResolvedAnnotations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LModifier <em>LModifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LModifier</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LModifier
	 * @generated
	 */
	EClass getLModifier();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LModifier#isFinal <em>Final</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LModifier#isFinal()
	 * @see #getLModifier()
	 * @generated
	 */
	EAttribute getLModifier_Final();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LModifier#isStatic <em>Static</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Static</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LModifier#isStatic()
	 * @see #getLModifier()
	 * @generated
	 */
	EAttribute getLModifier_Static();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LModifier#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LModifier#getVisibility()
	 * @see #getLModifier()
	 * @generated
	 */
	EAttribute getLModifier_Visibility();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity <em>LMultiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LMultiplicity</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity
	 * @generated
	 */
	EClass getLMultiplicity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getLower <em>Lower</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getLower()
	 * @see #getLMultiplicity()
	 * @generated
	 */
	EAttribute getLMultiplicity_Lower();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getUpper <em>Upper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getUpper()
	 * @see #getLMultiplicity()
	 * @generated
	 */
	EAttribute getLMultiplicity_Upper();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getToMultiplicityString <em>To Multiplicity String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To Multiplicity String</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity#getToMultiplicityString()
	 * @see #getLMultiplicity()
	 * @generated
	 */
	EAttribute getLMultiplicity_ToMultiplicityString();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LConstraint <em>LConstraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LConstraint</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LConstraint
	 * @generated
	 */
	EClass getLConstraint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LResultFilters <em>LResult Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LResult Filters</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LResultFilters
	 * @generated
	 */
	EClass getLResultFilters();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.dsl.semantic.common.types.LResultFilters#getResultFilters <em>Result Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Result Filters</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LResultFilters#getResultFilters()
	 * @see #getLResultFilters()
	 * @generated
	 */
	EReference getLResultFilters_ResultFilters();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LResultFilter <em>LResult Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LResult Filter</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LResultFilter
	 * @generated
	 */
	EClass getLResultFilter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint <em>LAttribute Matching Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LAttribute Matching Constraint</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint
	 * @generated
	 */
	EClass getLAttributeMatchingConstraint();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getAttribute()
	 * @see #getLAttributeMatchingConstraint()
	 * @generated
	 */
	EReference getLAttributeMatchingConstraint_Attribute();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getComparatorType <em>Comparator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comparator Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getComparatorType()
	 * @see #getLAttributeMatchingConstraint()
	 * @generated
	 */
	EAttribute getLAttributeMatchingConstraint_ComparatorType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getMatchingValue <em>Matching Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Matching Value</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getMatchingValue()
	 * @see #getLAttributeMatchingConstraint()
	 * @generated
	 */
	EAttribute getLAttributeMatchingConstraint_MatchingValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getMatchingLiteral <em>Matching Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Matching Literal</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getMatchingLiteral()
	 * @see #getLAttributeMatchingConstraint()
	 * @generated
	 */
	EReference getLAttributeMatchingConstraint_MatchingLiteral();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint <em>LDatatype Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDatatype Constraint</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint
	 * @generated
	 */
	EClass getLDatatypeConstraint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getMsgCode <em>Msg Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Msg Code</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getMsgCode()
	 * @see #getLDatatypeConstraint()
	 * @generated
	 */
	EAttribute getLDatatypeConstraint_MsgCode();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getMsgI18nKey <em>Msg I1 8n Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Msg I1 8n Key</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getMsgI18nKey()
	 * @see #getLDatatypeConstraint()
	 * @generated
	 */
	EAttribute getLDatatypeConstraint_MsgI18nKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getSeverity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Severity</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getSeverity()
	 * @see #getLDatatypeConstraint()
	 * @generated
	 */
	EAttribute getLDatatypeConstraint_Severity();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#isForPrimitives()
	 * @generated
	 */
	EOperation getLDatatypeConstraint__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LStringConstraint <em>LString Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LString Constraint</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LStringConstraint
	 * @generated
	 */
	EClass getLStringConstraint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LNumericConstraint <em>LNumeric Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LNumeric Constraint</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LNumericConstraint
	 * @generated
	 */
	EClass getLNumericConstraint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDecimalConstraint <em>LDecimal Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDecimal Constraint</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDecimalConstraint
	 * @generated
	 */
	EClass getLDecimalConstraint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDateConstraint <em>LDate Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDate Constraint</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateConstraint
	 * @generated
	 */
	EClass getLDateConstraint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LBooleanConstraint <em>LBoolean Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LBoolean Constraint</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LBooleanConstraint
	 * @generated
	 */
	EClass getLBooleanConstraint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LBlobConstraint <em>LBlob Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LBlob Constraint</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LBlobConstraint
	 * @generated
	 */
	EClass getLBlobConstraint();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertFalse <em>LDt CAssert False</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CAssert False</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertFalse
	 * @generated
	 */
	EClass getLDtCAssertFalse();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertFalse#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertFalse#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCAssertFalse__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertTrue <em>LDt CAssert True</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CAssert True</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertTrue
	 * @generated
	 */
	EClass getLDtCAssertTrue();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertTrue#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertTrue#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCAssertTrue__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMax <em>LDt CDecimal Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CDecimal Max</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMax
	 * @generated
	 */
	EClass getLDtCDecimalMax();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMax#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMax#getMax()
	 * @see #getLDtCDecimalMax()
	 * @generated
	 */
	EAttribute getLDtCDecimalMax_Max();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMax#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMax#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCDecimalMax__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMin <em>LDt CDecimal Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CDecimal Min</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMin
	 * @generated
	 */
	EClass getLDtCDecimalMin();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMin#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMin#getMin()
	 * @see #getLDtCDecimalMin()
	 * @generated
	 */
	EAttribute getLDtCDecimalMin_Min();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMin#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMin#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCDecimalMin__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDigits <em>LDt CDigits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CDigits</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDigits
	 * @generated
	 */
	EClass getLDtCDigits();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDigits#getIntDigits <em>Int Digits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Int Digits</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDigits#getIntDigits()
	 * @see #getLDtCDigits()
	 * @generated
	 */
	EAttribute getLDtCDigits_IntDigits();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDigits#getFractionDigits <em>Fraction Digits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fraction Digits</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDigits#getFractionDigits()
	 * @see #getLDtCDigits()
	 * @generated
	 */
	EAttribute getLDtCDigits_FractionDigits();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDigits#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCDigits#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCDigits__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCFuture <em>LDt CFuture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CFuture</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCFuture
	 * @generated
	 */
	EClass getLDtCFuture();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCFuture#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCFuture#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCFuture__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCPast <em>LDt CPast</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CPast</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCPast
	 * @generated
	 */
	EClass getLDtCPast();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCPast#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCPast#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCPast__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMax <em>LDt CNumeric Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CNumeric Max</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMax
	 * @generated
	 */
	EClass getLDtCNumericMax();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMax#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMax#getMax()
	 * @see #getLDtCNumericMax()
	 * @generated
	 */
	EAttribute getLDtCNumericMax_Max();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMax#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMax#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCNumericMax__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMin <em>LDt CNumeric Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CNumeric Min</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMin
	 * @generated
	 */
	EClass getLDtCNumericMin();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMin#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMin#getMin()
	 * @see #getLDtCNumericMin()
	 * @generated
	 */
	EAttribute getLDtCNumericMin_Min();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMin#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMin#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCNumericMin__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNotNull <em>LDt CNot Null</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CNot Null</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNotNull
	 * @generated
	 */
	EClass getLDtCNotNull();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNotNull#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNotNull#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCNotNull__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNull <em>LDt CNull</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CNull</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNull
	 * @generated
	 */
	EClass getLDtCNull();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCNull#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCNull#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCNull__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCRegEx <em>LDt CReg Ex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CReg Ex</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCRegEx
	 * @generated
	 */
	EClass getLDtCRegEx();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCRegEx#getPattern <em>Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pattern</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCRegEx#getPattern()
	 * @see #getLDtCRegEx()
	 * @generated
	 */
	EAttribute getLDtCRegEx_Pattern();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCRegEx#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCRegEx#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCRegEx__IsForPrimitives();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCSize <em>LDt CSize</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LDt CSize</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCSize
	 * @generated
	 */
	EClass getLDtCSize();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCSize#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCSize#getMin()
	 * @see #getLDtCSize()
	 * @generated
	 */
	EAttribute getLDtCSize_Min();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCSize#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCSize#getMax()
	 * @see #getLDtCSize()
	 * @generated
	 */
	EAttribute getLDtCSize_Max();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCSize#isForPrimitives() <em>Is For Primitives</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is For Primitives</em>' operation.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDtCSize#isForPrimitives()
	 * @generated
	 */
	EOperation getLDtCSize__IsForPrimitives();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.dsl.semantic.common.types.LDateType <em>LDate Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>LDate Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateType
	 * @generated
	 */
	EEnum getLDateType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.dsl.semantic.common.types.LVisibility <em>LVisibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>LVisibility</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LVisibility
	 * @generated
	 */
	EEnum getLVisibility();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.dsl.semantic.common.types.LLowerBound <em>LLower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>LLower Bound</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LLowerBound
	 * @generated
	 */
	EEnum getLLowerBound();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.dsl.semantic.common.types.LUpperBound <em>LUpper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>LUpper Bound</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LUpperBound
	 * @generated
	 */
	EEnum getLUpperBound();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.dsl.semantic.common.types.LComparatorType <em>LComparator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>LComparator Type</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LComparatorType
	 * @generated
	 */
	EEnum getLComparatorType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.dsl.semantic.common.types.LConstraintSeverity <em>LConstraint Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>LConstraint Severity</em>'.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LConstraintSeverity
	 * @generated
	 */
	EEnum getLConstraintSeverity();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Operations List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Operations List</em>'.
	 * @see java.util.List
	 * @generated
	 */
	EDataType getOperationsList();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Features List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Features List</em>'.
	 * @see java.util.List
	 * @generated
	 */
	EDataType getFeaturesList();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.common.util.EList <em>Annotation List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Annotation List</em>'.
	 * @see org.eclipse.emf.common.util.EList
	 * @generated
	 */
	EDataType getAnnotationList();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.InternalEObject <em>Internal EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Internal EObject</em>'.
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @generated
	 */
	EDataType getInternalEObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OSBPTypesFactory getOSBPTypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LCommonModelImpl <em>LCommon Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LCommonModelImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLCommonModel()
		 * @generated
		 */
		EClass LCOMMON_MODEL = eINSTANCE.getLCommonModel();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LCOMMON_MODEL__IMPORT_SECTION = eINSTANCE.getLCommonModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LCOMMON_MODEL__PACKAGES = eINSTANCE.getLCommonModel_Packages();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LLazyResolverImpl <em>LLazy Resolver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LLazyResolverImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLLazyResolver()
		 * @generated
		 */
		EClass LLAZY_RESOLVER = eINSTANCE.getLLazyResolver();

		/**
		 * The meta object literal for the '<em><b>EResolve Proxy</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LLAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = eINSTANCE.getLLazyResolver__EResolveProxy__InternalEObject();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LPackageImpl <em>LPackage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LPackageImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLPackage()
		 * @generated
		 */
		EClass LPACKAGE = eINSTANCE.getLPackage();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LPACKAGE__NAME = eINSTANCE.getLPackage_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LTypedPackageImpl <em>LTyped Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LTypedPackageImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLTypedPackage()
		 * @generated
		 */
		EClass LTYPED_PACKAGE = eINSTANCE.getLTypedPackage();

		/**
		 * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTYPED_PACKAGE__TYPES = eINSTANCE.getLTypedPackage_Types();

		/**
		 * The meta object literal for the '<em><b>Get Resource Simple Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LTYPED_PACKAGE___GET_RESOURCE_SIMPLE_NAME = eINSTANCE.getLTypedPackage__GetResourceSimpleName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LImportImpl <em>LImport</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LImportImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLImport()
		 * @generated
		 */
		EClass LIMPORT = eINSTANCE.getLImport();

		/**
		 * The meta object literal for the '<em><b>Imported Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIMPORT__IMPORTED_NAMESPACE = eINSTANCE.getLImport_ImportedNamespace();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LTypeImpl <em>LType</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LTypeImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLType()
		 * @generated
		 */
		EClass LTYPE = eINSTANCE.getLType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LTYPE__NAME = eINSTANCE.getLType_Name();

		/**
		 * The meta object literal for the '<em><b>Annotation Info</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTYPE__ANNOTATION_INFO = eINSTANCE.getLType_AnnotationInfo();

		/**
		 * The meta object literal for the '<em><b>Get Resolved Annotations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LTYPE___GET_RESOLVED_ANNOTATIONS = eINSTANCE.getLType__GetResolvedAnnotations();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LAnnotationDefImpl <em>LAnnotation Def</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LAnnotationDefImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLAnnotationDef()
		 * @generated
		 */
		EClass LANNOTATION_DEF = eINSTANCE.getLAnnotationDef();

		/**
		 * The meta object literal for the '<em><b>Exclude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANNOTATION_DEF__EXCLUDE = eINSTANCE.getLAnnotationDef_Exclude();

		/**
		 * The meta object literal for the '<em><b>Annotation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LANNOTATION_DEF__ANNOTATION = eINSTANCE.getLAnnotationDef_Annotation();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LAnnotationTargetImpl <em>LAnnotation Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LAnnotationTargetImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLAnnotationTarget()
		 * @generated
		 */
		EClass LANNOTATION_TARGET = eINSTANCE.getLAnnotationTarget();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LANNOTATION_TARGET__ANNOTATIONS = eINSTANCE.getLAnnotationTarget_Annotations();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LScalarTypeImpl <em>LScalar Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LScalarTypeImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLScalarType()
		 * @generated
		 */
		EClass LSCALAR_TYPE = eINSTANCE.getLScalarType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDataTypeImpl <em>LData Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDataTypeImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDataType()
		 * @generated
		 */
		EClass LDATA_TYPE = eINSTANCE.getLDataType();

		/**
		 * The meta object literal for the '<em><b>Jvm Type Reference</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LDATA_TYPE__JVM_TYPE_REFERENCE = eINSTANCE.getLDataType_JvmTypeReference();

		/**
		 * The meta object literal for the '<em><b>As Primitive</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATA_TYPE__AS_PRIMITIVE = eINSTANCE.getLDataType_AsPrimitive();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATA_TYPE__DATE = eINSTANCE.getLDataType_Date();

		/**
		 * The meta object literal for the '<em><b>As Blob</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATA_TYPE__AS_BLOB = eINSTANCE.getLDataType_AsBlob();

		/**
		 * The meta object literal for the '<em><b>Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATA_TYPE__LENGTH = eINSTANCE.getLDataType_Length();

		/**
		 * The meta object literal for the '<em><b>Date Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATA_TYPE__DATE_TYPE = eINSTANCE.getLDataType_DateType();

		/**
		 * The meta object literal for the '<em><b>Synthetic Flag</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATA_TYPE__SYNTHETIC_FLAG = eINSTANCE.getLDataType_SyntheticFlag();

		/**
		 * The meta object literal for the '<em><b>Synthetic Selector</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATA_TYPE__SYNTHETIC_SELECTOR = eINSTANCE.getLDataType_SyntheticSelector();

		/**
		 * The meta object literal for the '<em><b>Synthetic Type Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LDATA_TYPE__SYNTHETIC_TYPE_REFERENCE = eINSTANCE.getLDataType_SyntheticTypeReference();

		/**
		 * The meta object literal for the '<em><b>Synthetic Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LDATA_TYPE__SYNTHETIC_TYPE = eINSTANCE.getLDataType_SyntheticType();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LDATA_TYPE__PROPERTIES = eINSTANCE.getLDataType_Properties();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LDATA_TYPE__CONSTRAINTS = eINSTANCE.getLDataType_Constraints();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LEnumImpl <em>LEnum</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LEnumImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLEnum()
		 * @generated
		 */
		EClass LENUM = eINSTANCE.getLEnum();

		/**
		 * The meta object literal for the '<em><b>Literals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENUM__LITERALS = eINSTANCE.getLEnum_Literals();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LEnumLiteralImpl <em>LEnum Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LEnumLiteralImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLEnumLiteral()
		 * @generated
		 */
		EClass LENUM_LITERAL = eINSTANCE.getLEnumLiteral();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENUM_LITERAL__NAME = eINSTANCE.getLEnumLiteral_Name();

		/**
		 * The meta object literal for the '<em><b>Default</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENUM_LITERAL__DEFAULT = eINSTANCE.getLEnumLiteral_Default();

		/**
		 * The meta object literal for the '<em><b>Null</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENUM_LITERAL__NULL = eINSTANCE.getLEnumLiteral_Null();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENUM_LITERAL__VALUE = eINSTANCE.getLEnumLiteral_Value();

		/**
		 * The meta object literal for the '<em><b>String Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LENUM_LITERAL__STRING_VALUE = eINSTANCE.getLEnumLiteral_StringValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LStateClassImpl <em>LState Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LStateClassImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLStateClass()
		 * @generated
		 */
		EClass LSTATE_CLASS = eINSTANCE.getLStateClass();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LSTATE_CLASS__STATES = eINSTANCE.getLStateClass_States();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LStateImpl <em>LState</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LStateImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLState()
		 * @generated
		 */
		EClass LSTATE = eINSTANCE.getLState();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LClassImpl <em>LClass</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LClassImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLClass()
		 * @generated
		 */
		EClass LCLASS = eINSTANCE.getLClass();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LCLASS__ABSTRACT = eINSTANCE.getLClass_Abstract();

		/**
		 * The meta object literal for the '<em><b>Serializable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LCLASS__SERIALIZABLE = eINSTANCE.getLClass_Serializable();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LCLASS__SHORT_NAME = eINSTANCE.getLClass_ShortName();

		/**
		 * The meta object literal for the '<em><b>Is Normal Attribute</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LCLASS___IS_NORMAL_ATTRIBUTE__LFEATURE = eINSTANCE.getLClass__IsNormalAttribute__LFeature();

		/**
		 * The meta object literal for the '<em><b>Is Hist Current Attribute</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LCLASS___IS_HIST_CURRENT_ATTRIBUTE__LFEATURE = eINSTANCE.getLClass__IsHistCurrentAttribute__LFeature();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder <em>LFeatures Holder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLFeaturesHolder()
		 * @generated
		 */
		EClass LFEATURES_HOLDER = eINSTANCE.getLFeaturesHolder();

		/**
		 * The meta object literal for the '<em><b>Get Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LFEATURES_HOLDER___GET_FEATURES = eINSTANCE.getLFeaturesHolder__GetFeatures();

		/**
		 * The meta object literal for the '<em><b>Get All Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LFEATURES_HOLDER___GET_ALL_FEATURES = eINSTANCE.getLFeaturesHolder__GetAllFeatures();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LFeature <em>LFeature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LFeature
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLFeature()
		 * @generated
		 */
		EClass LFEATURE = eINSTANCE.getLFeature();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LFEATURE__NAME = eINSTANCE.getLFeature_Name();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LFEATURE__MULTIPLICITY = eINSTANCE.getLFeature_Multiplicity();

		/**
		 * The meta object literal for the '<em><b>Annotation Info</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LFEATURE__ANNOTATION_INFO = eINSTANCE.getLFeature_AnnotationInfo();

		/**
		 * The meta object literal for the '<em><b>Get Resolved Annotations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LFEATURE___GET_RESOLVED_ANNOTATIONS = eINSTANCE.getLFeature__GetResolvedAnnotations();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference <em>LReference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LReference
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLReference()
		 * @generated
		 */
		EClass LREFERENCE = eINSTANCE.getLReference();

		/**
		 * The meta object literal for the '<em><b>Lazy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__LAZY = eINSTANCE.getLReference_Lazy();

		/**
		 * The meta object literal for the '<em><b>Cascade Merge Persist</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__CASCADE_MERGE_PERSIST = eINSTANCE.getLReference_CascadeMergePersist();

		/**
		 * The meta object literal for the '<em><b>Cascade Remove</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__CASCADE_REMOVE = eINSTANCE.getLReference_CascadeRemove();

		/**
		 * The meta object literal for the '<em><b>Cascade Refresh</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__CASCADE_REFRESH = eINSTANCE.getLReference_CascadeRefresh();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LREFERENCE__PROPERTIES = eINSTANCE.getLReference_Properties();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LREFERENCE__CONSTRAINTS = eINSTANCE.getLReference_Constraints();

		/**
		 * The meta object literal for the '<em><b>Is Grouped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__IS_GROUPED = eINSTANCE.getLReference_IsGrouped();

		/**
		 * The meta object literal for the '<em><b>Group Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__GROUP_NAME = eINSTANCE.getLReference_GroupName();

		/**
		 * The meta object literal for the '<em><b>As Grid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__AS_GRID = eINSTANCE.getLReference_AsGrid();

		/**
		 * The meta object literal for the '<em><b>As Table</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__AS_TABLE = eINSTANCE.getLReference_AsTable();

		/**
		 * The meta object literal for the '<em><b>Side Kick</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__SIDE_KICK = eINSTANCE.getLReference_SideKick();

		/**
		 * The meta object literal for the '<em><b>Reference Hidden</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__REFERENCE_HIDDEN = eINSTANCE.getLReference_ReferenceHidden();

		/**
		 * The meta object literal for the '<em><b>Reference Read Only</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__REFERENCE_READ_ONLY = eINSTANCE.getLReference_ReferenceReadOnly();

		/**
		 * The meta object literal for the '<em><b>Historized</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LREFERENCE__HISTORIZED = eINSTANCE.getLReference_Historized();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute <em>LAttribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LAttribute
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLAttribute()
		 * @generated
		 */
		EClass LATTRIBUTE = eINSTANCE.getLAttribute();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__ID = eINSTANCE.getLAttribute_Id();

		/**
		 * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__UUID = eINSTANCE.getLAttribute_Uuid();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__VERSION = eINSTANCE.getLAttribute_Version();

		/**
		 * The meta object literal for the '<em><b>Lazy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__LAZY = eINSTANCE.getLAttribute_Lazy();

		/**
		 * The meta object literal for the '<em><b>Transient</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__TRANSIENT = eINSTANCE.getLAttribute_Transient();

		/**
		 * The meta object literal for the '<em><b>Derived</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__DERIVED = eINSTANCE.getLAttribute_Derived();

		/**
		 * The meta object literal for the '<em><b>Dirty</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__DIRTY = eINSTANCE.getLAttribute_Dirty();

		/**
		 * The meta object literal for the '<em><b>Domain Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__DOMAIN_KEY = eINSTANCE.getLAttribute_DomainKey();

		/**
		 * The meta object literal for the '<em><b>Domain Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__DOMAIN_DESCRIPTION = eINSTANCE.getLAttribute_DomainDescription();

		/**
		 * The meta object literal for the '<em><b>Filtering</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__FILTERING = eINSTANCE.getLAttribute_Filtering();

		/**
		 * The meta object literal for the '<em><b>Range Filtering</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__RANGE_FILTERING = eINSTANCE.getLAttribute_RangeFiltering();

		/**
		 * The meta object literal for the '<em><b>Unique Entry</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__UNIQUE_ENTRY = eINSTANCE.getLAttribute_UniqueEntry();

		/**
		 * The meta object literal for the '<em><b>Attribute Hidden</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__ATTRIBUTE_HIDDEN = eINSTANCE.getLAttribute_AttributeHidden();

		/**
		 * The meta object literal for the '<em><b>Attribute Read Only</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__ATTRIBUTE_READ_ONLY = eINSTANCE.getLAttribute_AttributeReadOnly();

		/**
		 * The meta object literal for the '<em><b>Extra Style</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__EXTRA_STYLE = eINSTANCE.getLAttribute_ExtraStyle();

		/**
		 * The meta object literal for the '<em><b>Derived Getter Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LATTRIBUTE__DERIVED_GETTER_EXPRESSION = eINSTANCE.getLAttribute_DerivedGetterExpression();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LATTRIBUTE__TYPE = eINSTANCE.getLAttribute_Type();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LATTRIBUTE__PROPERTIES = eINSTANCE.getLAttribute_Properties();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LATTRIBUTE__CONSTRAINTS = eINSTANCE.getLAttribute_Constraints();

		/**
		 * The meta object literal for the '<em><b>Is Grouped</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__IS_GROUPED = eINSTANCE.getLAttribute_IsGrouped();

		/**
		 * The meta object literal for the '<em><b>Group Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE__GROUP_NAME = eINSTANCE.getLAttribute_GroupName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LKeyAndValueImpl <em>LKey And Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LKeyAndValueImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLKeyAndValue()
		 * @generated
		 */
		EClass LKEY_AND_VALUE = eINSTANCE.getLKeyAndValue();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LKEY_AND_VALUE__KEY = eINSTANCE.getLKeyAndValue_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LKEY_AND_VALUE__VALUE = eINSTANCE.getLKeyAndValue_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LOperationImpl <em>LOperation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LOperationImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLOperation()
		 * @generated
		 */
		EClass LOPERATION = eINSTANCE.getLOperation();

		/**
		 * The meta object literal for the '<em><b>Modifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOPERATION__MODIFIER = eINSTANCE.getLOperation_Modifier();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOPERATION__TYPE = eINSTANCE.getLOperation_Type();

		/**
		 * The meta object literal for the '<em><b>Params</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOPERATION__PARAMS = eINSTANCE.getLOperation_Params();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOPERATION__BODY = eINSTANCE.getLOperation_Body();

		/**
		 * The meta object literal for the '<em><b>Get Resolved Annotations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOPERATION___GET_RESOLVED_ANNOTATIONS = eINSTANCE.getLOperation__GetResolvedAnnotations();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LModifierImpl <em>LModifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LModifierImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLModifier()
		 * @generated
		 */
		EClass LMODIFIER = eINSTANCE.getLModifier();

		/**
		 * The meta object literal for the '<em><b>Final</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LMODIFIER__FINAL = eINSTANCE.getLModifier_Final();

		/**
		 * The meta object literal for the '<em><b>Static</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LMODIFIER__STATIC = eINSTANCE.getLModifier_Static();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LMODIFIER__VISIBILITY = eINSTANCE.getLModifier_Visibility();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LMultiplicityImpl <em>LMultiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LMultiplicityImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLMultiplicity()
		 * @generated
		 */
		EClass LMULTIPLICITY = eINSTANCE.getLMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Lower</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LMULTIPLICITY__LOWER = eINSTANCE.getLMultiplicity_Lower();

		/**
		 * The meta object literal for the '<em><b>Upper</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LMULTIPLICITY__UPPER = eINSTANCE.getLMultiplicity_Upper();

		/**
		 * The meta object literal for the '<em><b>To Multiplicity String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LMULTIPLICITY__TO_MULTIPLICITY_STRING = eINSTANCE.getLMultiplicity_ToMultiplicityString();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LConstraint <em>LConstraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LConstraint
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLConstraint()
		 * @generated
		 */
		EClass LCONSTRAINT = eINSTANCE.getLConstraint();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LResultFiltersImpl <em>LResult Filters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LResultFiltersImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLResultFilters()
		 * @generated
		 */
		EClass LRESULT_FILTERS = eINSTANCE.getLResultFilters();

		/**
		 * The meta object literal for the '<em><b>Result Filters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LRESULT_FILTERS__RESULT_FILTERS = eINSTANCE.getLResultFilters_ResultFilters();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LResultFilter <em>LResult Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LResultFilter
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLResultFilter()
		 * @generated
		 */
		EClass LRESULT_FILTER = eINSTANCE.getLResultFilter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LAttributeMatchingConstraintImpl <em>LAttribute Matching Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LAttributeMatchingConstraintImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLAttributeMatchingConstraint()
		 * @generated
		 */
		EClass LATTRIBUTE_MATCHING_CONSTRAINT = eINSTANCE.getLAttributeMatchingConstraint();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LATTRIBUTE_MATCHING_CONSTRAINT__ATTRIBUTE = eINSTANCE.getLAttributeMatchingConstraint_Attribute();

		/**
		 * The meta object literal for the '<em><b>Comparator Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE_MATCHING_CONSTRAINT__COMPARATOR_TYPE = eINSTANCE.getLAttributeMatchingConstraint_ComparatorType();

		/**
		 * The meta object literal for the '<em><b>Matching Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LATTRIBUTE_MATCHING_CONSTRAINT__MATCHING_VALUE = eINSTANCE.getLAttributeMatchingConstraint_MatchingValue();

		/**
		 * The meta object literal for the '<em><b>Matching Literal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LATTRIBUTE_MATCHING_CONSTRAINT__MATCHING_LITERAL = eINSTANCE.getLAttributeMatchingConstraint_MatchingLiteral();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint <em>LDatatype Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDatatypeConstraint()
		 * @generated
		 */
		EClass LDATATYPE_CONSTRAINT = eINSTANCE.getLDatatypeConstraint();

		/**
		 * The meta object literal for the '<em><b>Msg Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATATYPE_CONSTRAINT__MSG_CODE = eINSTANCE.getLDatatypeConstraint_MsgCode();

		/**
		 * The meta object literal for the '<em><b>Msg I1 8n Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATATYPE_CONSTRAINT__MSG_I1_8N_KEY = eINSTANCE.getLDatatypeConstraint_MsgI18nKey();

		/**
		 * The meta object literal for the '<em><b>Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDATATYPE_CONSTRAINT__SEVERITY = eINSTANCE.getLDatatypeConstraint_Severity();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDATATYPE_CONSTRAINT___IS_FOR_PRIMITIVES = eINSTANCE.getLDatatypeConstraint__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LStringConstraint <em>LString Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LStringConstraint
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLStringConstraint()
		 * @generated
		 */
		EClass LSTRING_CONSTRAINT = eINSTANCE.getLStringConstraint();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LNumericConstraint <em>LNumeric Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LNumericConstraint
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLNumericConstraint()
		 * @generated
		 */
		EClass LNUMERIC_CONSTRAINT = eINSTANCE.getLNumericConstraint();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDecimalConstraint <em>LDecimal Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LDecimalConstraint
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDecimalConstraint()
		 * @generated
		 */
		EClass LDECIMAL_CONSTRAINT = eINSTANCE.getLDecimalConstraint();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDateConstraint <em>LDate Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateConstraint
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDateConstraint()
		 * @generated
		 */
		EClass LDATE_CONSTRAINT = eINSTANCE.getLDateConstraint();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LBooleanConstraint <em>LBoolean Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LBooleanConstraint
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLBooleanConstraint()
		 * @generated
		 */
		EClass LBOOLEAN_CONSTRAINT = eINSTANCE.getLBooleanConstraint();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LBlobConstraint <em>LBlob Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LBlobConstraint
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLBlobConstraint()
		 * @generated
		 */
		EClass LBLOB_CONSTRAINT = eINSTANCE.getLBlobConstraint();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCAssertFalseImpl <em>LDt CAssert False</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCAssertFalseImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCAssertFalse()
		 * @generated
		 */
		EClass LDT_CASSERT_FALSE = eINSTANCE.getLDtCAssertFalse();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CASSERT_FALSE___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCAssertFalse__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCAssertTrueImpl <em>LDt CAssert True</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCAssertTrueImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCAssertTrue()
		 * @generated
		 */
		EClass LDT_CASSERT_TRUE = eINSTANCE.getLDtCAssertTrue();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CASSERT_TRUE___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCAssertTrue__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDecimalMaxImpl <em>LDt CDecimal Max</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDecimalMaxImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCDecimalMax()
		 * @generated
		 */
		EClass LDT_CDECIMAL_MAX = eINSTANCE.getLDtCDecimalMax();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDT_CDECIMAL_MAX__MAX = eINSTANCE.getLDtCDecimalMax_Max();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CDECIMAL_MAX___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCDecimalMax__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDecimalMinImpl <em>LDt CDecimal Min</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDecimalMinImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCDecimalMin()
		 * @generated
		 */
		EClass LDT_CDECIMAL_MIN = eINSTANCE.getLDtCDecimalMin();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDT_CDECIMAL_MIN__MIN = eINSTANCE.getLDtCDecimalMin_Min();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CDECIMAL_MIN___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCDecimalMin__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDigitsImpl <em>LDt CDigits</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCDigitsImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCDigits()
		 * @generated
		 */
		EClass LDT_CDIGITS = eINSTANCE.getLDtCDigits();

		/**
		 * The meta object literal for the '<em><b>Int Digits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDT_CDIGITS__INT_DIGITS = eINSTANCE.getLDtCDigits_IntDigits();

		/**
		 * The meta object literal for the '<em><b>Fraction Digits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDT_CDIGITS__FRACTION_DIGITS = eINSTANCE.getLDtCDigits_FractionDigits();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CDIGITS___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCDigits__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCFutureImpl <em>LDt CFuture</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCFutureImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCFuture()
		 * @generated
		 */
		EClass LDT_CFUTURE = eINSTANCE.getLDtCFuture();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CFUTURE___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCFuture__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCPastImpl <em>LDt CPast</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCPastImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCPast()
		 * @generated
		 */
		EClass LDT_CPAST = eINSTANCE.getLDtCPast();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CPAST___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCPast__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNumericMaxImpl <em>LDt CNumeric Max</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNumericMaxImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCNumericMax()
		 * @generated
		 */
		EClass LDT_CNUMERIC_MAX = eINSTANCE.getLDtCNumericMax();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDT_CNUMERIC_MAX__MAX = eINSTANCE.getLDtCNumericMax_Max();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CNUMERIC_MAX___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCNumericMax__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNumericMinImpl <em>LDt CNumeric Min</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNumericMinImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCNumericMin()
		 * @generated
		 */
		EClass LDT_CNUMERIC_MIN = eINSTANCE.getLDtCNumericMin();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDT_CNUMERIC_MIN__MIN = eINSTANCE.getLDtCNumericMin_Min();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CNUMERIC_MIN___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCNumericMin__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNotNullImpl <em>LDt CNot Null</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNotNullImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCNotNull()
		 * @generated
		 */
		EClass LDT_CNOT_NULL = eINSTANCE.getLDtCNotNull();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CNOT_NULL___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCNotNull__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNullImpl <em>LDt CNull</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCNullImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCNull()
		 * @generated
		 */
		EClass LDT_CNULL = eINSTANCE.getLDtCNull();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CNULL___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCNull__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCRegExImpl <em>LDt CReg Ex</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCRegExImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCRegEx()
		 * @generated
		 */
		EClass LDT_CREG_EX = eINSTANCE.getLDtCRegEx();

		/**
		 * The meta object literal for the '<em><b>Pattern</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDT_CREG_EX__PATTERN = eINSTANCE.getLDtCRegEx_Pattern();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CREG_EX___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCRegEx__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCSizeImpl <em>LDt CSize</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.LDtCSizeImpl
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDtCSize()
		 * @generated
		 */
		EClass LDT_CSIZE = eINSTANCE.getLDtCSize();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDT_CSIZE__MIN = eINSTANCE.getLDtCSize_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LDT_CSIZE__MAX = eINSTANCE.getLDtCSize_Max();

		/**
		 * The meta object literal for the '<em><b>Is For Primitives</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LDT_CSIZE___IS_FOR_PRIMITIVES = eINSTANCE.getLDtCSize__IsForPrimitives();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDateType <em>LDate Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateType
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLDateType()
		 * @generated
		 */
		EEnum LDATE_TYPE = eINSTANCE.getLDateType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LVisibility <em>LVisibility</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LVisibility
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLVisibility()
		 * @generated
		 */
		EEnum LVISIBILITY = eINSTANCE.getLVisibility();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LLowerBound <em>LLower Bound</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LLowerBound
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLLowerBound()
		 * @generated
		 */
		EEnum LLOWER_BOUND = eINSTANCE.getLLowerBound();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LUpperBound <em>LUpper Bound</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LUpperBound
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLUpperBound()
		 * @generated
		 */
		EEnum LUPPER_BOUND = eINSTANCE.getLUpperBound();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LComparatorType <em>LComparator Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LComparatorType
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLComparatorType()
		 * @generated
		 */
		EEnum LCOMPARATOR_TYPE = eINSTANCE.getLComparatorType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.dsl.semantic.common.types.LConstraintSeverity <em>LConstraint Severity</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.dsl.semantic.common.types.LConstraintSeverity
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getLConstraintSeverity()
		 * @generated
		 */
		EEnum LCONSTRAINT_SEVERITY = eINSTANCE.getLConstraintSeverity();

		/**
		 * The meta object literal for the '<em>Operations List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getOperationsList()
		 * @generated
		 */
		EDataType OPERATIONS_LIST = eINSTANCE.getOperationsList();

		/**
		 * The meta object literal for the '<em>Features List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getFeaturesList()
		 * @generated
		 */
		EDataType FEATURES_LIST = eINSTANCE.getFeaturesList();

		/**
		 * The meta object literal for the '<em>Annotation List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.common.util.EList
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getAnnotationList()
		 * @generated
		 */
		EDataType ANNOTATION_LIST = eINSTANCE.getAnnotationList();

		/**
		 * The meta object literal for the '<em>Internal EObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.InternalEObject
		 * @see org.eclipse.osbp.dsl.semantic.common.types.impl.OSBPTypesPackageImpl#getInternalEObject()
		 * @generated
		 */
		EDataType INTERNAL_EOBJECT = eINSTANCE.getInternalEObject();

	}

} //OSBPTypesPackage
