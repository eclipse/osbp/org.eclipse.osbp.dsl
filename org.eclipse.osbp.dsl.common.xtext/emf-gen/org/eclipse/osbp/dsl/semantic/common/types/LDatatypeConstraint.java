/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LDatatype Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * DatatypeConstraints are used to create javax.validation constraint annotations.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getMsgCode <em>Msg Code</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getMsgI18nKey <em>Msg I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getSeverity <em>Severity</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDatatypeConstraint()
 * @generated
 */
public interface LDatatypeConstraint extends LConstraint {
	/**
	 * Returns the value of the '<em><b>Msg Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msg Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msg Code</em>' attribute.
	 * @see #setMsgCode(String)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDatatypeConstraint_MsgCode()
	 * @generated
	 */
	String getMsgCode();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getMsgCode <em>Msg Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msg Code</em>' attribute.
	 * @see #getMsgCode()
	 * @generated
	 */
	void setMsgCode(String value);

	/**
	 * Returns the value of the '<em><b>Msg I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msg I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msg I1 8n Key</em>' attribute.
	 * @see #setMsgI18nKey(String)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDatatypeConstraint_MsgI18nKey()
	 * @generated
	 */
	String getMsgI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getMsgI18nKey <em>Msg I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msg I1 8n Key</em>' attribute.
	 * @see #getMsgI18nKey()
	 * @generated
	 */
	void setMsgI18nKey(String value);

	/**
	 * Returns the value of the '<em><b>Severity</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.dsl.semantic.common.types.LConstraintSeverity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Severity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Severity</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LConstraintSeverity
	 * @see #setSeverity(LConstraintSeverity)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDatatypeConstraint_Severity()
	 * @generated
	 */
	LConstraintSeverity getSeverity();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint#getSeverity <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Severity</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LConstraintSeverity
	 * @see #getSeverity()
	 * @generated
	 */
	void setSeverity(LConstraintSeverity value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns true, if the constraint can be used for primitive datatypes.
	 * <!-- end-model-doc -->
	 * @generated
	 */
	boolean isForPrimitives();

} // LDatatypeConstraint
