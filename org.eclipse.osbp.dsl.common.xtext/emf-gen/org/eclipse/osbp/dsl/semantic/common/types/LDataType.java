/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types;

import org.eclipse.emf.common.util.EList;

import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LData Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getJvmTypeReference <em>Jvm Type Reference</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isAsPrimitive <em>As Primitive</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isDate <em>Date</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isAsBlob <em>As Blob</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getLength <em>Length</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getDateType <em>Date Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isSyntheticFlag <em>Synthetic Flag</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticSelector <em>Synthetic Selector</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticTypeReference <em>Synthetic Type Reference</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticType <em>Synthetic Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType()
 * @generated
 */
public interface LDataType extends LScalarType {
	/**
	 * Returns the value of the '<em><b>Jvm Type Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Jvm Type Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jvm Type Reference</em>' containment reference.
	 * @see #setJvmTypeReference(JvmTypeReference)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_JvmTypeReference()
	 * @generated
	 */
	JvmTypeReference getJvmTypeReference();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getJvmTypeReference <em>Jvm Type Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jvm Type Reference</em>' containment reference.
	 * @see #getJvmTypeReference()
	 * @generated
	 */
	void setJvmTypeReference(JvmTypeReference value);

	/**
	 * Returns the value of the '<em><b>As Primitive</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Primitive</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Primitive</em>' attribute.
	 * @see #setAsPrimitive(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_AsPrimitive()
	 * @generated
	 */
	boolean isAsPrimitive();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isAsPrimitive <em>As Primitive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>As Primitive</em>' attribute.
	 * @see #isAsPrimitive()
	 * @generated
	 */
	void setAsPrimitive(boolean value);

	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_Date()
	 * @generated
	 */
	boolean isDate();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #isDate()
	 * @generated
	 */
	void setDate(boolean value);

	/**
	 * Returns the value of the '<em><b>As Blob</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Blob</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Blob</em>' attribute.
	 * @see #setAsBlob(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_AsBlob()
	 * @generated
	 */
	boolean isAsBlob();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isAsBlob <em>As Blob</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>As Blob</em>' attribute.
	 * @see #isAsBlob()
	 * @generated
	 */
	void setAsBlob(boolean value);

	/**
	 * Returns the value of the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Length</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Length</em>' attribute.
	 * @see #setLength(int)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_Length()
	 * @generated
	 */
	int getLength();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getLength <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Length</em>' attribute.
	 * @see #getLength()
	 * @generated
	 */
	void setLength(int value);

	/**
	 * Returns the value of the '<em><b>Date Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.dsl.semantic.common.types.LDateType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Type</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateType
	 * @see #setDateType(LDateType)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_DateType()
	 * @generated
	 */
	LDateType getDateType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getDateType <em>Date Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Type</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LDateType
	 * @see #getDateType()
	 * @generated
	 */
	void setDateType(LDateType value);

	/**
	 * Returns the value of the '<em><b>Synthetic Flag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Attention: synthetic must only be used inside the same resource
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Synthetic Flag</em>' attribute.
	 * @see #setSyntheticFlag(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_SyntheticFlag()
	 * @generated
	 */
	boolean isSyntheticFlag();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#isSyntheticFlag <em>Synthetic Flag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synthetic Flag</em>' attribute.
	 * @see #isSyntheticFlag()
	 * @generated
	 */
	void setSyntheticFlag(boolean value);

	/**
	 * Returns the value of the '<em><b>Synthetic Selector</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synthetic Selector</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synthetic Selector</em>' attribute.
	 * @see #setSyntheticSelector(String)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_SyntheticSelector()
	 * @generated
	 */
	String getSyntheticSelector();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticSelector <em>Synthetic Selector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synthetic Selector</em>' attribute.
	 * @see #getSyntheticSelector()
	 * @generated
	 */
	void setSyntheticSelector(String value);

	/**
	 * Returns the value of the '<em><b>Synthetic Type Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synthetic Type Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synthetic Type Reference</em>' reference.
	 * @see #setSyntheticTypeReference(LFeature)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_SyntheticTypeReference()
	 * @generated
	 */
	LFeature getSyntheticTypeReference();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticTypeReference <em>Synthetic Type Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synthetic Type Reference</em>' reference.
	 * @see #getSyntheticTypeReference()
	 * @generated
	 */
	void setSyntheticTypeReference(LFeature value);

	/**
	 * Returns the value of the '<em><b>Synthetic Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synthetic Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synthetic Type</em>' reference.
	 * @see #setSyntheticType(LType)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_SyntheticType()
	 * @generated
	 */
	LType getSyntheticType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDataType#getSyntheticType <em>Synthetic Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synthetic Type</em>' reference.
	 * @see #getSyntheticType()
	 * @generated
	 */
	void setSyntheticType(LType value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_Properties()
	 * @generated
	 */
	EList<LKeyAndValue> getProperties();

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDataType_Constraints()
	 * @generated
	 */
	EList<LDatatypeConstraint> getConstraints();

} // LDataType
