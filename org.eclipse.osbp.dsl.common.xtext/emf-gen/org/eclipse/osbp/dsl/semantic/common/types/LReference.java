/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LReference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isLazy <em>Lazy</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeMergePersist <em>Cascade Merge Persist</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeRemove <em>Cascade Remove</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeRefresh <em>Cascade Refresh</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isIsGrouped <em>Is Grouped</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#getGroupName <em>Group Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isAsGrid <em>As Grid</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isAsTable <em>As Table</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isSideKick <em>Side Kick</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isReferenceHidden <em>Reference Hidden</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isReferenceReadOnly <em>Reference Read Only</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isHistorized <em>Historized</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference()
 * @generated
 */
public interface LReference extends LFeature {
	/**
	 * Returns the value of the '<em><b>Lazy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lazy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lazy</em>' attribute.
	 * @see #setLazy(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_Lazy()
	 * @generated
	 */
	boolean isLazy();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isLazy <em>Lazy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lazy</em>' attribute.
	 * @see #isLazy()
	 * @generated
	 */
	void setLazy(boolean value);

	/**
	 * Returns the value of the '<em><b>Cascade Merge Persist</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cascade Merge Persist</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cascade Merge Persist</em>' attribute.
	 * @see #setCascadeMergePersist(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_CascadeMergePersist()
	 * @generated
	 */
	boolean isCascadeMergePersist();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeMergePersist <em>Cascade Merge Persist</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cascade Merge Persist</em>' attribute.
	 * @see #isCascadeMergePersist()
	 * @generated
	 */
	void setCascadeMergePersist(boolean value);

	/**
	 * Returns the value of the '<em><b>Cascade Remove</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cascade Remove</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cascade Remove</em>' attribute.
	 * @see #setCascadeRemove(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_CascadeRemove()
	 * @generated
	 */
	boolean isCascadeRemove();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeRemove <em>Cascade Remove</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cascade Remove</em>' attribute.
	 * @see #isCascadeRemove()
	 * @generated
	 */
	void setCascadeRemove(boolean value);

	/**
	 * Returns the value of the '<em><b>Cascade Refresh</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cascade Refresh</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cascade Refresh</em>' attribute.
	 * @see #setCascadeRefresh(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_CascadeRefresh()
	 * @generated
	 */
	boolean isCascadeRefresh();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isCascadeRefresh <em>Cascade Refresh</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cascade Refresh</em>' attribute.
	 * @see #isCascadeRefresh()
	 * @generated
	 */
	void setCascadeRefresh(boolean value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_Properties()
	 * @generated
	 */
	EList<LKeyAndValue> getProperties();

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_Constraints()
	 * @generated
	 */
	EList<LDatatypeConstraint> getConstraints();

	/**
	 * Returns the value of the '<em><b>Is Grouped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Grouped</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Grouped</em>' attribute.
	 * @see #setIsGrouped(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_IsGrouped()
	 * @generated
	 */
	boolean isIsGrouped();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isIsGrouped <em>Is Grouped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Grouped</em>' attribute.
	 * @see #isIsGrouped()
	 * @generated
	 */
	void setIsGrouped(boolean value);

	/**
	 * Returns the value of the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Name</em>' attribute.
	 * @see #setGroupName(String)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_GroupName()
	 * @generated
	 */
	String getGroupName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#getGroupName <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Name</em>' attribute.
	 * @see #getGroupName()
	 * @generated
	 */
	void setGroupName(String value);

	/**
	 * Returns the value of the '<em><b>As Grid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Grid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Grid</em>' attribute.
	 * @see #setAsGrid(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_AsGrid()
	 * @generated
	 */
	boolean isAsGrid();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isAsGrid <em>As Grid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>As Grid</em>' attribute.
	 * @see #isAsGrid()
	 * @generated
	 */
	void setAsGrid(boolean value);

	/**
	 * Returns the value of the '<em><b>As Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Table</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Table</em>' attribute.
	 * @see #setAsTable(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_AsTable()
	 * @generated
	 */
	boolean isAsTable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isAsTable <em>As Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>As Table</em>' attribute.
	 * @see #isAsTable()
	 * @generated
	 */
	void setAsTable(boolean value);

	/**
	 * Returns the value of the '<em><b>Side Kick</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Side Kick</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Side Kick</em>' attribute.
	 * @see #setSideKick(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_SideKick()
	 * @generated
	 */
	boolean isSideKick();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isSideKick <em>Side Kick</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Side Kick</em>' attribute.
	 * @see #isSideKick()
	 * @generated
	 */
	void setSideKick(boolean value);

	/**
	 * Returns the value of the '<em><b>Reference Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * refs are never rendered by a presentation logic
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Reference Hidden</em>' attribute.
	 * @see #setReferenceHidden(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_ReferenceHidden()
	 * @generated
	 */
	boolean isReferenceHidden();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isReferenceHidden <em>Reference Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Hidden</em>' attribute.
	 * @see #isReferenceHidden()
	 * @generated
	 */
	void setReferenceHidden(boolean value);

	/**
	 * Returns the value of the '<em><b>Reference Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * refs are always readonly for a presentation logic
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Reference Read Only</em>' attribute.
	 * @see #setReferenceReadOnly(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_ReferenceReadOnly()
	 * @generated
	 */
	boolean isReferenceReadOnly();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isReferenceReadOnly <em>Reference Read Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Read Only</em>' attribute.
	 * @see #isReferenceReadOnly()
	 * @generated
	 */
	void setReferenceReadOnly(boolean value);

	/**
	 * Returns the value of the '<em><b>Historized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * if referencing a historized entity as many2one - use historized data.
	 * if not specified show the current data set
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Historized</em>' attribute.
	 * @see #setHistorized(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLReference_Historized()
	 * @generated
	 */
	boolean isHistorized();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LReference#isHistorized <em>Historized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Historized</em>' attribute.
	 * @see #isHistorized()
	 * @generated
	 */
	void setHistorized(boolean value);

} // LReference
