/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LDt CDecimal Min</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMin#getMin <em>Min</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDtCDecimalMin()
 * @generated
 */
public interface LDtCDecimalMin extends LDecimalConstraint {
	/**
	 * Returns the value of the '<em><b>Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' attribute.
	 * @see #setMin(float)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLDtCDecimalMin_Min()
	 * @generated
	 */
	float getMin();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMin#getMin <em>Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' attribute.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(float value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	boolean isForPrimitives();

} // LDtCDecimalMin
