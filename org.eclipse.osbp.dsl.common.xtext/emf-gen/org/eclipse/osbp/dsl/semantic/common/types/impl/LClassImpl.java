/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;
import org.eclipse.osbp.runtime.common.annotations.CreateAt;
import org.eclipse.osbp.runtime.common.annotations.CreateBy;
import org.eclipse.osbp.runtime.common.annotations.HistDomainKey;
import org.eclipse.osbp.runtime.common.annotations.HistIsCurrent;
import org.eclipse.osbp.runtime.common.annotations.HistIsCustomVersion;
import org.eclipse.osbp.runtime.common.annotations.HistValidUntil;
import org.eclipse.osbp.runtime.common.annotations.UpdateAt;
import org.eclipse.osbp.runtime.common.annotations.UpdateBy;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LClass</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LClassImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LClassImpl#isSerializable <em>Serializable</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LClassImpl#getShortName <em>Short Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LClassImpl extends LTypeImpl implements LClass {
	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The default value of the '{@link #isSerializable() <em>Serializable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSerializable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SERIALIZABLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSerializable() <em>Serializable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSerializable()
	 * @generated
	 * @ordered
	 */
	protected boolean serializable = SERIALIZABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getShortName() <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getShortName() <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortName()
	 * @generated
	 * @ordered
	 */
	protected String shortName = SHORT_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSBPTypesPackage.Literals.LCLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPTypesPackage.LCLASS__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSerializable() {
		return serializable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSerializable(boolean newSerializable) {
		boolean oldSerializable = serializable;
		serializable = newSerializable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPTypesPackage.LCLASS__SERIALIZABLE, oldSerializable, serializable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortName(String newShortName) {
		String oldShortName = shortName;
		shortName = newShortName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPTypesPackage.LCLASS__SHORT_NAME, oldShortName, shortName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNormalAttribute(final LFeature f) {
		if ((!(f instanceof LAttribute))) {
			return false;
		}
		LAttribute attr = ((LAttribute) f);
		if ((((attr.isId() || attr.isUuid()) || attr.isVersion()) || attr.isAttributeHidden())) {
			return false;
		}
		final Function1<LAnnotationDef, Boolean> _function = new Function1<LAnnotationDef, Boolean>() {
			public Boolean apply(final LAnnotationDef it) {
				return Boolean.valueOf((((CreateAt.class.getName().equals(it.getAnnotation().getAnnotationType().getQualifiedName()) || 
					CreateBy.class.getName().equals(it.getAnnotation().getAnnotationType().getQualifiedName())) || 
					UpdateAt.class.getName().equals(it.getAnnotation().getAnnotationType().getQualifiedName())) || 
					UpdateBy.class.getName().equals(it.getAnnotation().getAnnotationType().getQualifiedName())));
			}
		};
		boolean isNotFilter = IterableExtensions.isEmpty(IterableExtensions.<LAnnotationDef>filter(f.getAnnotationInfo().getAnnotations(), _function));
		final Function1<LAnnotationDef, Boolean> _function_1 = new Function1<LAnnotationDef, Boolean>() {
			public Boolean apply(final LAnnotationDef it) {
				return Boolean.valueOf((((HistValidUntil.class.getName().equals(it.getAnnotation().getAnnotationType().getQualifiedName()) || 
					HistIsCurrent.class.getName().equals(it.getAnnotation().getAnnotationType().getQualifiedName())) || 
					HistIsCustomVersion.class.getName().equals(it.getAnnotation().getAnnotationType().getQualifiedName())) || 
					HistDomainKey.class.getName().equals(it.getAnnotation().getAnnotationType().getQualifiedName())));
			}
		};
		boolean isNotHist = IterableExtensions.isEmpty(IterableExtensions.<LAnnotationDef>filter(f.getAnnotationInfo().getAnnotations(), _function_1));
		return (isNotFilter && isNotHist);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHistCurrentAttribute(final LFeature f) {
		if ((!(f instanceof LAttribute))) {
			return false;
		}
		LAttribute attr = ((LAttribute) f);
		final Function1<LAnnotationDef, Boolean> _function = new Function1<LAnnotationDef, Boolean>() {
			public Boolean apply(final LAnnotationDef it) {
				return Boolean.valueOf(HistIsCurrent.class.getName().equals(it.getAnnotation().getAnnotationType().getQualifiedName()));
			}
		};
		boolean _isEmpty = IterableExtensions.isEmpty(IterableExtensions.<LAnnotationDef>filter(f.getAnnotationInfo().getAnnotations(), _function));
		return (!_isEmpty);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OSBPTypesPackage.LCLASS__ABSTRACT:
				return isAbstract();
			case OSBPTypesPackage.LCLASS__SERIALIZABLE:
				return isSerializable();
			case OSBPTypesPackage.LCLASS__SHORT_NAME:
				return getShortName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OSBPTypesPackage.LCLASS__ABSTRACT:
				setAbstract((Boolean)newValue);
				return;
			case OSBPTypesPackage.LCLASS__SERIALIZABLE:
				setSerializable((Boolean)newValue);
				return;
			case OSBPTypesPackage.LCLASS__SHORT_NAME:
				setShortName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OSBPTypesPackage.LCLASS__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case OSBPTypesPackage.LCLASS__SERIALIZABLE:
				setSerializable(SERIALIZABLE_EDEFAULT);
				return;
			case OSBPTypesPackage.LCLASS__SHORT_NAME:
				setShortName(SHORT_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OSBPTypesPackage.LCLASS__ABSTRACT:
				return abstract_ != ABSTRACT_EDEFAULT;
			case OSBPTypesPackage.LCLASS__SERIALIZABLE:
				return serializable != SERIALIZABLE_EDEFAULT;
			case OSBPTypesPackage.LCLASS__SHORT_NAME:
				return SHORT_NAME_EDEFAULT == null ? shortName != null : !SHORT_NAME_EDEFAULT.equals(shortName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OSBPTypesPackage.LCLASS___IS_NORMAL_ATTRIBUTE__LFEATURE:
				return isNormalAttribute((LFeature)arguments.get(0));
			case OSBPTypesPackage.LCLASS___IS_HIST_CURRENT_ATTRIBUTE__LFEATURE:
				return isHistCurrentAttribute((LFeature)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (abstract: ");
		result.append(abstract_);
		result.append(", serializable: ");
		result.append(serializable);
		result.append(", shortName: ");
		result.append(shortName);
		result.append(')');
		return result.toString();
	}

} //LClassImpl
