/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LAttribute Matching Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getComparatorType <em>Comparator Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getMatchingValue <em>Matching Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getMatchingLiteral <em>Matching Literal</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttributeMatchingConstraint()
 * @generated
 */
public interface LAttributeMatchingConstraint extends LResultFilter {
	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' reference.
	 * @see #setAttribute(LAttribute)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttributeMatchingConstraint_Attribute()
	 * @generated
	 */
	LAttribute getAttribute();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getAttribute <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute</em>' reference.
	 * @see #getAttribute()
	 * @generated
	 */
	void setAttribute(LAttribute value);

	/**
	 * Returns the value of the '<em><b>Comparator Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.dsl.semantic.common.types.LComparatorType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comparator Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comparator Type</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LComparatorType
	 * @see #setComparatorType(LComparatorType)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttributeMatchingConstraint_ComparatorType()
	 * @generated
	 */
	LComparatorType getComparatorType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getComparatorType <em>Comparator Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comparator Type</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LComparatorType
	 * @see #getComparatorType()
	 * @generated
	 */
	void setComparatorType(LComparatorType value);

	/**
	 * Returns the value of the '<em><b>Matching Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matching Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matching Value</em>' attribute.
	 * @see #setMatchingValue(String)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttributeMatchingConstraint_MatchingValue()
	 * @generated
	 */
	String getMatchingValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getMatchingValue <em>Matching Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Matching Value</em>' attribute.
	 * @see #getMatchingValue()
	 * @generated
	 */
	void setMatchingValue(String value);

	/**
	 * Returns the value of the '<em><b>Matching Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matching Literal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matching Literal</em>' reference.
	 * @see #setMatchingLiteral(LEnumLiteral)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttributeMatchingConstraint_MatchingLiteral()
	 * @generated
	 */
	LEnumLiteral getMatchingLiteral();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint#getMatchingLiteral <em>Matching Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Matching Literal</em>' reference.
	 * @see #getMatchingLiteral()
	 * @generated
	 */
	void setMatchingLiteral(LEnumLiteral value);

} // LAttributeMatchingConstraint
