/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types;

import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.XExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LAttribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isUuid <em>Uuid</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isVersion <em>Version</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isLazy <em>Lazy</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isTransient <em>Transient</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDerived <em>Derived</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDirty <em>Dirty</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDomainKey <em>Domain Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDomainDescription <em>Domain Description</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isFiltering <em>Filtering</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isRangeFiltering <em>Range Filtering</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isUniqueEntry <em>Unique Entry</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isAttributeHidden <em>Attribute Hidden</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isAttributeReadOnly <em>Attribute Read Only</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getExtraStyle <em>Extra Style</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getDerivedGetterExpression <em>Derived Getter Expression</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isIsGrouped <em>Is Grouped</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getGroupName <em>Group Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute()
 * @generated
 */
public interface LAttribute extends LFeature {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Id()
	 * @generated
	 */
	boolean isId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #isId()
	 * @generated
	 */
	void setId(boolean value);

	/**
	 * Returns the value of the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uuid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uuid</em>' attribute.
	 * @see #setUuid(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Uuid()
	 * @generated
	 */
	boolean isUuid();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isUuid <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uuid</em>' attribute.
	 * @see #isUuid()
	 * @generated
	 */
	void setUuid(boolean value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Version()
	 * @generated
	 */
	boolean isVersion();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #isVersion()
	 * @generated
	 */
	void setVersion(boolean value);

	/**
	 * Returns the value of the '<em><b>Lazy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lazy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lazy</em>' attribute.
	 * @see #setLazy(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Lazy()
	 * @generated
	 */
	boolean isLazy();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isLazy <em>Lazy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lazy</em>' attribute.
	 * @see #isLazy()
	 * @generated
	 */
	void setLazy(boolean value);

	/**
	 * Returns the value of the '<em><b>Transient</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transient</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transient</em>' attribute.
	 * @see #setTransient(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Transient()
	 * @generated
	 */
	boolean isTransient();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isTransient <em>Transient</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transient</em>' attribute.
	 * @see #isTransient()
	 * @generated
	 */
	void setTransient(boolean value);

	/**
	 * Returns the value of the '<em><b>Derived</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived</em>' attribute.
	 * @see #setDerived(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Derived()
	 * @generated
	 */
	boolean isDerived();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDerived <em>Derived</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derived</em>' attribute.
	 * @see #isDerived()
	 * @generated
	 */
	void setDerived(boolean value);

	/**
	 * Returns the value of the '<em><b>Dirty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dirty</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dirty</em>' attribute.
	 * @see #setDirty(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Dirty()
	 * @generated
	 */
	boolean isDirty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDirty <em>Dirty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dirty</em>' attribute.
	 * @see #isDirty()
	 * @generated
	 */
	void setDirty(boolean value);

	/**
	 * Returns the value of the '<em><b>Domain Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The domainKey is a human readable value for a dto or entity
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Domain Key</em>' attribute.
	 * @see #setDomainKey(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_DomainKey()
	 * @generated
	 */
	boolean isDomainKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDomainKey <em>Domain Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain Key</em>' attribute.
	 * @see #isDomainKey()
	 * @generated
	 */
	void setDomainKey(boolean value);

	/**
	 * Returns the value of the '<em><b>Domain Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The domainKey is the human readable description for a dto or entity
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Domain Description</em>' attribute.
	 * @see #setDomainDescription(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_DomainDescription()
	 * @generated
	 */
	boolean isDomainDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isDomainDescription <em>Domain Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain Description</em>' attribute.
	 * @see #isDomainDescription()
	 * @generated
	 */
	void setDomainDescription(boolean value);

	/**
	 * Returns the value of the '<em><b>Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * This keyword allows for generic filter dialogs in the UI based on eq, gt, lt,..
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Filtering</em>' attribute.
	 * @see #setFiltering(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Filtering()
	 * @generated
	 */
	boolean isFiltering();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isFiltering <em>Filtering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filtering</em>' attribute.
	 * @see #isFiltering()
	 * @generated
	 */
	void setFiltering(boolean value);

	/**
	 * Returns the value of the '<em><b>Range Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * This keyword allows for generic filter dialogs in the UI based on eq, gt, lt,..
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Range Filtering</em>' attribute.
	 * @see #setRangeFiltering(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_RangeFiltering()
	 * @generated
	 */
	boolean isRangeFiltering();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isRangeFiltering <em>Range Filtering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Filtering</em>' attribute.
	 * @see #isRangeFiltering()
	 * @generated
	 */
	void setRangeFiltering(boolean value);

	/**
	 * Returns the value of the '<em><b>Unique Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Attributes marked with uniqueEntry must be checked against the database,
	 * whether they already exist in the database with a different id
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Unique Entry</em>' attribute.
	 * @see #setUniqueEntry(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_UniqueEntry()
	 * @generated
	 */
	boolean isUniqueEntry();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isUniqueEntry <em>Unique Entry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unique Entry</em>' attribute.
	 * @see #isUniqueEntry()
	 * @generated
	 */
	void setUniqueEntry(boolean value);

	/**
	 * Returns the value of the '<em><b>Attribute Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Attributes are never rendered by a presentation logic
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Attribute Hidden</em>' attribute.
	 * @see #setAttributeHidden(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_AttributeHidden()
	 * @generated
	 */
	boolean isAttributeHidden();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isAttributeHidden <em>Attribute Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Hidden</em>' attribute.
	 * @see #isAttributeHidden()
	 * @generated
	 */
	void setAttributeHidden(boolean value);

	/**
	 * Returns the value of the '<em><b>Attribute Read Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Attributes are always readonly for a presentation logic
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Attribute Read Only</em>' attribute.
	 * @see #setAttributeReadOnly(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_AttributeReadOnly()
	 * @generated
	 */
	boolean isAttributeReadOnly();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isAttributeReadOnly <em>Attribute Read Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Read Only</em>' attribute.
	 * @see #isAttributeReadOnly()
	 * @generated
	 */
	void setAttributeReadOnly(boolean value);

	/**
	 * Returns the value of the '<em><b>Extra Style</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Attributes always get this extra styling
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Extra Style</em>' attribute.
	 * @see #setExtraStyle(String)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_ExtraStyle()
	 * @generated
	 */
	String getExtraStyle();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getExtraStyle <em>Extra Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extra Style</em>' attribute.
	 * @see #getExtraStyle()
	 * @generated
	 */
	void setExtraStyle(String value);

	/**
	 * Returns the value of the '<em><b>Derived Getter Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Getter Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Getter Expression</em>' containment reference.
	 * @see #setDerivedGetterExpression(XExpression)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_DerivedGetterExpression()
	 * @generated
	 */
	XExpression getDerivedGetterExpression();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getDerivedGetterExpression <em>Derived Getter Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derived Getter Expression</em>' containment reference.
	 * @see #getDerivedGetterExpression()
	 * @generated
	 */
	void setDerivedGetterExpression(XExpression value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(LScalarType)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Type()
	 * @generated
	 */
	LScalarType getType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(LScalarType value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Properties()
	 * @generated
	 */
	EList<LKeyAndValue> getProperties();

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_Constraints()
	 * @generated
	 */
	EList<LDatatypeConstraint> getConstraints();

	/**
	 * Returns the value of the '<em><b>Is Grouped</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Grouped</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Grouped</em>' attribute.
	 * @see #setIsGrouped(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_IsGrouped()
	 * @generated
	 */
	boolean isIsGrouped();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#isIsGrouped <em>Is Grouped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Grouped</em>' attribute.
	 * @see #isIsGrouped()
	 * @generated
	 */
	void setIsGrouped(boolean value);

	/**
	 * Returns the value of the '<em><b>Group Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Name</em>' attribute.
	 * @see #setGroupName(String)
	 * @see org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage#getLAttribute_GroupName()
	 * @generated
	 */
	String getGroupName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.common.types.LAttribute#getGroupName <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Name</em>' attribute.
	 * @see #getGroupName()
	 * @generated
	 */
	void setGroupName(String value);

} // LAttribute
