/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.common.types.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.osbp.dsl.semantic.common.helper.Bounds;
import org.eclipse.osbp.dsl.semantic.common.types.LLowerBound;
import org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity;
import org.eclipse.osbp.dsl.semantic.common.types.LUpperBound;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LMultiplicity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LMultiplicityImpl#getLower <em>Lower</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LMultiplicityImpl#getUpper <em>Upper</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.common.types.impl.LMultiplicityImpl#getToMultiplicityString <em>To Multiplicity String</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LMultiplicityImpl extends MinimalEObjectImpl.Container implements LMultiplicity {
	/**
	 * The default value of the '{@link #getLower() <em>Lower</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLower()
	 * @generated
	 * @ordered
	 */
	protected static final LLowerBound LOWER_EDEFAULT = LLowerBound.NULL;

	/**
	 * The cached value of the '{@link #getLower() <em>Lower</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLower()
	 * @generated
	 * @ordered
	 */
	protected LLowerBound lower = LOWER_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpper() <em>Upper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpper()
	 * @generated
	 * @ordered
	 */
	protected static final LUpperBound UPPER_EDEFAULT = LUpperBound.NULL;

	/**
	 * The cached value of the '{@link #getUpper() <em>Upper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpper()
	 * @generated
	 * @ordered
	 */
	protected LUpperBound upper = UPPER_EDEFAULT;

	/**
	 * The default value of the '{@link #getToMultiplicityString() <em>To Multiplicity String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToMultiplicityString()
	 * @generated
	 * @ordered
	 */
	protected static final String TO_MULTIPLICITY_STRING_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LMultiplicityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSBPTypesPackage.Literals.LMULTIPLICITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LLowerBound getLower() {
		return lower;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLower(LLowerBound newLower) {
		LLowerBound oldLower = lower;
		lower = newLower == null ? LOWER_EDEFAULT : newLower;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPTypesPackage.LMULTIPLICITY__LOWER, oldLower, lower));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LUpperBound getUpper() {
		return upper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpper(LUpperBound newUpper) {
		LUpperBound oldUpper = upper;
		upper = newUpper == null ? UPPER_EDEFAULT : newUpper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPTypesPackage.LMULTIPLICITY__UPPER, oldUpper, upper));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToMultiplicityString() {
		return Bounds.createFor(this).toBoundsString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OSBPTypesPackage.LMULTIPLICITY__LOWER:
				return getLower();
			case OSBPTypesPackage.LMULTIPLICITY__UPPER:
				return getUpper();
			case OSBPTypesPackage.LMULTIPLICITY__TO_MULTIPLICITY_STRING:
				return getToMultiplicityString();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OSBPTypesPackage.LMULTIPLICITY__LOWER:
				setLower((LLowerBound)newValue);
				return;
			case OSBPTypesPackage.LMULTIPLICITY__UPPER:
				setUpper((LUpperBound)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OSBPTypesPackage.LMULTIPLICITY__LOWER:
				setLower(LOWER_EDEFAULT);
				return;
			case OSBPTypesPackage.LMULTIPLICITY__UPPER:
				setUpper(UPPER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OSBPTypesPackage.LMULTIPLICITY__LOWER:
				return lower != LOWER_EDEFAULT;
			case OSBPTypesPackage.LMULTIPLICITY__UPPER:
				return upper != UPPER_EDEFAULT;
			case OSBPTypesPackage.LMULTIPLICITY__TO_MULTIPLICITY_STRING:
				return TO_MULTIPLICITY_STRING_EDEFAULT == null ? getToMultiplicityString() != null : !TO_MULTIPLICITY_STRING_EDEFAULT.equals(getToMultiplicityString());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (lower: ");
		result.append(lower);
		result.append(", upper: ");
		result.append(upper);
		result.append(')');
		return result.toString();
	}

} //LMultiplicityImpl
