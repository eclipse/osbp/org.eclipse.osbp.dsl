/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 *
 * generated by Xtext 2.11.0

 */
package org.eclipse.osbp.dsl.common.xtext.serializer;

import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.osbp.dsl.common.xtext.services.CommonGrammarGrammarAccess;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef;
import org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertFalse;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCAssertTrue;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMax;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCDecimalMin;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCDigits;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCFuture;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCNotNull;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCNull;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMax;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCNumericMin;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCPast;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCRegEx;
import org.eclipse.osbp.dsl.semantic.common.types.LDtCSize;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;
import org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue;
import org.eclipse.osbp.dsl.semantic.common.types.LModifier;
import org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity;
import org.eclipse.osbp.dsl.semantic.common.types.LResultFilters;
import org.eclipse.osbp.dsl.semantic.common.types.LState;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;
import org.eclipse.osbp.xtext.oxtype.oxtype.OXImportDeclaration;
import org.eclipse.osbp.xtext.oxtype.oxtype.OXtypePackage;
import org.eclipse.osbp.xtext.oxtype.serializer.OXtypeSemanticSequencer;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericArrayTypeReference;
import org.eclipse.xtext.common.types.JvmInnerTypeReference;
import org.eclipse.xtext.common.types.JvmLowerBound;
import org.eclipse.xtext.common.types.JvmParameterizedTypeReference;
import org.eclipse.xtext.common.types.JvmTypeParameter;
import org.eclipse.xtext.common.types.JvmUpperBound;
import org.eclipse.xtext.common.types.JvmWildcardTypeReference;
import org.eclipse.xtext.common.types.TypesPackage;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.eclipse.xtext.xbase.XAssignment;
import org.eclipse.xtext.xbase.XBasicForLoopExpression;
import org.eclipse.xtext.xbase.XBinaryOperation;
import org.eclipse.xtext.xbase.XBlockExpression;
import org.eclipse.xtext.xbase.XBooleanLiteral;
import org.eclipse.xtext.xbase.XCasePart;
import org.eclipse.xtext.xbase.XCastedExpression;
import org.eclipse.xtext.xbase.XCatchClause;
import org.eclipse.xtext.xbase.XClosure;
import org.eclipse.xtext.xbase.XConstructorCall;
import org.eclipse.xtext.xbase.XDoWhileExpression;
import org.eclipse.xtext.xbase.XFeatureCall;
import org.eclipse.xtext.xbase.XForLoopExpression;
import org.eclipse.xtext.xbase.XIfExpression;
import org.eclipse.xtext.xbase.XInstanceOfExpression;
import org.eclipse.xtext.xbase.XListLiteral;
import org.eclipse.xtext.xbase.XMemberFeatureCall;
import org.eclipse.xtext.xbase.XNullLiteral;
import org.eclipse.xtext.xbase.XNumberLiteral;
import org.eclipse.xtext.xbase.XPostfixOperation;
import org.eclipse.xtext.xbase.XReturnExpression;
import org.eclipse.xtext.xbase.XSetLiteral;
import org.eclipse.xtext.xbase.XStringLiteral;
import org.eclipse.xtext.xbase.XSwitchExpression;
import org.eclipse.xtext.xbase.XSynchronizedExpression;
import org.eclipse.xtext.xbase.XThrowExpression;
import org.eclipse.xtext.xbase.XTryCatchFinallyExpression;
import org.eclipse.xtext.xbase.XTypeLiteral;
import org.eclipse.xtext.xbase.XUnaryOperation;
import org.eclipse.xtext.xbase.XVariableDeclaration;
import org.eclipse.xtext.xbase.XWhileExpression;
import org.eclipse.xtext.xbase.XbasePackage;
import org.eclipse.xtext.xbase.annotations.xAnnotations.XAnnotation;
import org.eclipse.xtext.xbase.annotations.xAnnotations.XAnnotationElementValuePair;
import org.eclipse.xtext.xbase.annotations.xAnnotations.XAnnotationsPackage;
import org.eclipse.xtext.xtype.XFunctionTypeRef;
import org.eclipse.xtext.xtype.XImportSection;
import org.eclipse.xtext.xtype.XtypePackage;

@SuppressWarnings("all")
public class CommonGrammarSemanticSequencer extends OXtypeSemanticSequencer {

	@Inject
	private CommonGrammarGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == OXtypePackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case OXtypePackage.OX_IMPORT_DECLARATION:
				sequence_XImportDeclaration(context, (OXImportDeclaration) semanticObject); 
				return; 
			}
		else if (epackage == OSBPTypesPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case OSBPTypesPackage.LANNOTATION_DEF:
				sequence_AnnotationDef(context, (LAnnotationDef) semanticObject); 
				return; 
			case OSBPTypesPackage.LATTRIBUTE_MATCHING_CONSTRAINT:
				sequence_AttributeMatchingConstraint(context, (LAttributeMatchingConstraint) semanticObject); 
				return; 
			case OSBPTypesPackage.LCLASS:
				if (rule == grammarAccess.getTypeRule()
						|| rule == grammarAccess.getClassRule()) {
					sequence_Class(context, (LClass) semanticObject); 
					return; 
				}
				else if (action == grammarAccess.getClassAccess().getLClassAnnotationInfoAction_2()) {
					sequence_Class_LClass_2(context, (LClass) semanticObject); 
					return; 
				}
				else break;
			case OSBPTypesPackage.LDATA_TYPE:
				sequence_DataType(context, (LDataType) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CASSERT_FALSE:
				sequence_DtCAssertFalse(context, (LDtCAssertFalse) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CASSERT_TRUE:
				sequence_DtCAssertTrue(context, (LDtCAssertTrue) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CDECIMAL_MAX:
				sequence_DtCDecimalMax(context, (LDtCDecimalMax) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CDECIMAL_MIN:
				sequence_DtCDecimalMin(context, (LDtCDecimalMin) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CDIGITS:
				sequence_DtCDigits(context, (LDtCDigits) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CFUTURE:
				sequence_DtCFuture(context, (LDtCFuture) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CNOT_NULL:
				sequence_DtCNotNull(context, (LDtCNotNull) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CNULL:
				sequence_DtCNull(context, (LDtCNull) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CNUMERIC_MAX:
				sequence_DtCNumericMax(context, (LDtCNumericMax) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CNUMERIC_MIN:
				sequence_DtCNumericMin(context, (LDtCNumericMin) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CPAST:
				sequence_DtCPast(context, (LDtCPast) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CREG_EX:
				sequence_DtCRegEx(context, (LDtCRegEx) semanticObject); 
				return; 
			case OSBPTypesPackage.LDT_CSIZE:
				sequence_DtCSize(context, (LDtCSize) semanticObject); 
				return; 
			case OSBPTypesPackage.LENUM:
				sequence_Enum(context, (LEnum) semanticObject); 
				return; 
			case OSBPTypesPackage.LENUM_LITERAL:
				sequence_EnumLiteral(context, (LEnumLiteral) semanticObject); 
				return; 
			case OSBPTypesPackage.LKEY_AND_VALUE:
				sequence_KeyAndValue(context, (LKeyAndValue) semanticObject); 
				return; 
			case OSBPTypesPackage.LMODIFIER:
				sequence_Modifier(context, (LModifier) semanticObject); 
				return; 
			case OSBPTypesPackage.LMULTIPLICITY:
				sequence_Multiplicity(context, (LMultiplicity) semanticObject); 
				return; 
			case OSBPTypesPackage.LRESULT_FILTERS:
				sequence_ResultFilters(context, (LResultFilters) semanticObject); 
				return; 
			case OSBPTypesPackage.LSTATE:
				sequence_State(context, (LState) semanticObject); 
				return; 
			case OSBPTypesPackage.LSTATE_CLASS:
				sequence_StateClass(context, (LStateClass) semanticObject); 
				return; 
			case OSBPTypesPackage.LTYPED_PACKAGE:
				sequence_TypedPackage(context, (LTypedPackage) semanticObject); 
				return; 
			}
		else if (epackage == TypesPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case TypesPackage.JVM_FORMAL_PARAMETER:
				if (rule == grammarAccess.getFullJvmFormalParameterRule()) {
					sequence_FullJvmFormalParameter(context, (JvmFormalParameter) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getJvmFormalParameterRule()) {
					sequence_JvmFormalParameter(context, (JvmFormalParameter) semanticObject); 
					return; 
				}
				else break;
			case TypesPackage.JVM_GENERIC_ARRAY_TYPE_REFERENCE:
				sequence_JvmTypeReference(context, (JvmGenericArrayTypeReference) semanticObject); 
				return; 
			case TypesPackage.JVM_INNER_TYPE_REFERENCE:
				sequence_JvmParameterizedTypeReference(context, (JvmInnerTypeReference) semanticObject); 
				return; 
			case TypesPackage.JVM_LOWER_BOUND:
				if (rule == grammarAccess.getJvmLowerBoundAndedRule()) {
					sequence_JvmLowerBoundAnded(context, (JvmLowerBound) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getJvmLowerBoundRule()) {
					sequence_JvmLowerBound(context, (JvmLowerBound) semanticObject); 
					return; 
				}
				else break;
			case TypesPackage.JVM_PARAMETERIZED_TYPE_REFERENCE:
				if (action == grammarAccess.getJvmParameterizedTypeReferenceAccess().getJvmInnerTypeReferenceOuterAction_1_4_0_0_0()) {
					sequence_JvmParameterizedTypeReference_JvmInnerTypeReference_1_4_0_0_0(context, (JvmParameterizedTypeReference) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getJvmTypeReferenceRule()
						|| action == grammarAccess.getJvmTypeReferenceAccess().getJvmGenericArrayTypeReferenceComponentTypeAction_0_1_0_0()
						|| rule == grammarAccess.getJvmParameterizedTypeReferenceRule()
						|| rule == grammarAccess.getJvmArgumentTypeReferenceRule()) {
					sequence_JvmParameterizedTypeReference(context, (JvmParameterizedTypeReference) semanticObject); 
					return; 
				}
				else break;
			case TypesPackage.JVM_TYPE_PARAMETER:
				sequence_JvmTypeParameter(context, (JvmTypeParameter) semanticObject); 
				return; 
			case TypesPackage.JVM_UPPER_BOUND:
				if (rule == grammarAccess.getJvmUpperBoundAndedRule()) {
					sequence_JvmUpperBoundAnded(context, (JvmUpperBound) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getJvmUpperBoundRule()) {
					sequence_JvmUpperBound(context, (JvmUpperBound) semanticObject); 
					return; 
				}
				else break;
			case TypesPackage.JVM_WILDCARD_TYPE_REFERENCE:
				sequence_JvmWildcardTypeReference(context, (JvmWildcardTypeReference) semanticObject); 
				return; 
			}
		else if (epackage == XAnnotationsPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case XAnnotationsPackage.XANNOTATION:
				sequence_XAnnotation(context, (XAnnotation) semanticObject); 
				return; 
			case XAnnotationsPackage.XANNOTATION_ELEMENT_VALUE_PAIR:
				sequence_XAnnotationElementValuePair(context, (XAnnotationElementValuePair) semanticObject); 
				return; 
			}
		else if (epackage == XbasePackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case XbasePackage.XASSIGNMENT:
				sequence_XAssignment_XMemberFeatureCall(context, (XAssignment) semanticObject); 
				return; 
			case XbasePackage.XBASIC_FOR_LOOP_EXPRESSION:
				sequence_XBasicForLoopExpression(context, (XBasicForLoopExpression) semanticObject); 
				return; 
			case XbasePackage.XBINARY_OPERATION:
				sequence_XAdditiveExpression_XAndExpression_XAssignment_XEqualityExpression_XMultiplicativeExpression_XOrExpression_XOtherOperatorExpression_XRelationalExpression(context, (XBinaryOperation) semanticObject); 
				return; 
			case XbasePackage.XBLOCK_EXPRESSION:
				if (rule == grammarAccess.getXAnnotationElementValueOrCommaListRule()
						|| action == grammarAccess.getXAnnotationElementValueOrCommaListAccess().getXListLiteralElementsAction_1_1_0()
						|| rule == grammarAccess.getXAnnotationElementValueRule()
						|| rule == grammarAccess.getXAnnotationOrExpressionRule()
						|| rule == grammarAccess.getXExpressionRule()
						|| rule == grammarAccess.getXAssignmentRule()
						|| action == grammarAccess.getXAssignmentAccess().getXBinaryOperationLeftOperandAction_1_1_0_0_0()
						|| rule == grammarAccess.getXOrExpressionRule()
						|| action == grammarAccess.getXOrExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXAndExpressionRule()
						|| action == grammarAccess.getXAndExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXEqualityExpressionRule()
						|| action == grammarAccess.getXEqualityExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXRelationalExpressionRule()
						|| action == grammarAccess.getXRelationalExpressionAccess().getXInstanceOfExpressionExpressionAction_1_0_0_0_0()
						|| action == grammarAccess.getXRelationalExpressionAccess().getXBinaryOperationLeftOperandAction_1_1_0_0_0()
						|| rule == grammarAccess.getXOtherOperatorExpressionRule()
						|| action == grammarAccess.getXOtherOperatorExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXAdditiveExpressionRule()
						|| action == grammarAccess.getXAdditiveExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXMultiplicativeExpressionRule()
						|| action == grammarAccess.getXMultiplicativeExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXUnaryOperationRule()
						|| rule == grammarAccess.getXCastedExpressionRule()
						|| action == grammarAccess.getXCastedExpressionAccess().getXCastedExpressionTargetAction_1_0_0_0()
						|| rule == grammarAccess.getXPostfixOperationRule()
						|| action == grammarAccess.getXPostfixOperationAccess().getXPostfixOperationOperandAction_1_0_0()
						|| rule == grammarAccess.getXMemberFeatureCallRule()
						|| action == grammarAccess.getXMemberFeatureCallAccess().getXAssignmentAssignableAction_1_0_0_0_0()
						|| action == grammarAccess.getXMemberFeatureCallAccess().getXMemberFeatureCallMemberCallTargetAction_1_1_0_0_0()
						|| rule == grammarAccess.getXPrimaryExpressionRule()
						|| rule == grammarAccess.getXParenthesizedExpressionRule()
						|| rule == grammarAccess.getXBlockExpressionRule()
						|| rule == grammarAccess.getXExpressionOrVarDeclarationRule()) {
					sequence_XBlockExpression(context, (XBlockExpression) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getXExpressionInClosureRule()) {
					sequence_XExpressionInClosure(context, (XBlockExpression) semanticObject); 
					return; 
				}
				else break;
			case XbasePackage.XBOOLEAN_LITERAL:
				sequence_XBooleanLiteral(context, (XBooleanLiteral) semanticObject); 
				return; 
			case XbasePackage.XCASE_PART:
				sequence_XCasePart(context, (XCasePart) semanticObject); 
				return; 
			case XbasePackage.XCASTED_EXPRESSION:
				sequence_XCastedExpression(context, (XCastedExpression) semanticObject); 
				return; 
			case XbasePackage.XCATCH_CLAUSE:
				sequence_XCatchClause(context, (XCatchClause) semanticObject); 
				return; 
			case XbasePackage.XCLOSURE:
				if (rule == grammarAccess.getXAnnotationElementValueOrCommaListRule()
						|| action == grammarAccess.getXAnnotationElementValueOrCommaListAccess().getXListLiteralElementsAction_1_1_0()
						|| rule == grammarAccess.getXAnnotationElementValueRule()
						|| rule == grammarAccess.getXAnnotationOrExpressionRule()
						|| rule == grammarAccess.getXExpressionRule()
						|| rule == grammarAccess.getXAssignmentRule()
						|| action == grammarAccess.getXAssignmentAccess().getXBinaryOperationLeftOperandAction_1_1_0_0_0()
						|| rule == grammarAccess.getXOrExpressionRule()
						|| action == grammarAccess.getXOrExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXAndExpressionRule()
						|| action == grammarAccess.getXAndExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXEqualityExpressionRule()
						|| action == grammarAccess.getXEqualityExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXRelationalExpressionRule()
						|| action == grammarAccess.getXRelationalExpressionAccess().getXInstanceOfExpressionExpressionAction_1_0_0_0_0()
						|| action == grammarAccess.getXRelationalExpressionAccess().getXBinaryOperationLeftOperandAction_1_1_0_0_0()
						|| rule == grammarAccess.getXOtherOperatorExpressionRule()
						|| action == grammarAccess.getXOtherOperatorExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXAdditiveExpressionRule()
						|| action == grammarAccess.getXAdditiveExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXMultiplicativeExpressionRule()
						|| action == grammarAccess.getXMultiplicativeExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXUnaryOperationRule()
						|| rule == grammarAccess.getXCastedExpressionRule()
						|| action == grammarAccess.getXCastedExpressionAccess().getXCastedExpressionTargetAction_1_0_0_0()
						|| rule == grammarAccess.getXPostfixOperationRule()
						|| action == grammarAccess.getXPostfixOperationAccess().getXPostfixOperationOperandAction_1_0_0()
						|| rule == grammarAccess.getXMemberFeatureCallRule()
						|| action == grammarAccess.getXMemberFeatureCallAccess().getXAssignmentAssignableAction_1_0_0_0_0()
						|| action == grammarAccess.getXMemberFeatureCallAccess().getXMemberFeatureCallMemberCallTargetAction_1_1_0_0_0()
						|| rule == grammarAccess.getXPrimaryExpressionRule()
						|| rule == grammarAccess.getXLiteralRule()
						|| rule == grammarAccess.getXClosureRule()
						|| rule == grammarAccess.getXParenthesizedExpressionRule()
						|| rule == grammarAccess.getXExpressionOrVarDeclarationRule()) {
					sequence_XClosure(context, (XClosure) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getXShortClosureRule()) {
					sequence_XShortClosure(context, (XClosure) semanticObject); 
					return; 
				}
				else break;
			case XbasePackage.XCONSTRUCTOR_CALL:
				sequence_XConstructorCall(context, (XConstructorCall) semanticObject); 
				return; 
			case XbasePackage.XDO_WHILE_EXPRESSION:
				sequence_XDoWhileExpression(context, (XDoWhileExpression) semanticObject); 
				return; 
			case XbasePackage.XFEATURE_CALL:
				sequence_XFeatureCall(context, (XFeatureCall) semanticObject); 
				return; 
			case XbasePackage.XFOR_LOOP_EXPRESSION:
				sequence_XForLoopExpression(context, (XForLoopExpression) semanticObject); 
				return; 
			case XbasePackage.XIF_EXPRESSION:
				sequence_XIfExpression(context, (XIfExpression) semanticObject); 
				return; 
			case XbasePackage.XINSTANCE_OF_EXPRESSION:
				sequence_XRelationalExpression(context, (XInstanceOfExpression) semanticObject); 
				return; 
			case XbasePackage.XLIST_LITERAL:
				if (rule == grammarAccess.getXAnnotationElementValueOrCommaListRule()) {
					sequence_XAnnotationElementValueOrCommaList_XListLiteral(context, (XListLiteral) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getXAnnotationElementValueRule()) {
					sequence_XAnnotationElementValue_XListLiteral(context, (XListLiteral) semanticObject); 
					return; 
				}
				else if (action == grammarAccess.getXAnnotationElementValueOrCommaListAccess().getXListLiteralElementsAction_1_1_0()
						|| rule == grammarAccess.getXAnnotationOrExpressionRule()
						|| rule == grammarAccess.getXExpressionRule()
						|| rule == grammarAccess.getXAssignmentRule()
						|| action == grammarAccess.getXAssignmentAccess().getXBinaryOperationLeftOperandAction_1_1_0_0_0()
						|| rule == grammarAccess.getXOrExpressionRule()
						|| action == grammarAccess.getXOrExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXAndExpressionRule()
						|| action == grammarAccess.getXAndExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXEqualityExpressionRule()
						|| action == grammarAccess.getXEqualityExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXRelationalExpressionRule()
						|| action == grammarAccess.getXRelationalExpressionAccess().getXInstanceOfExpressionExpressionAction_1_0_0_0_0()
						|| action == grammarAccess.getXRelationalExpressionAccess().getXBinaryOperationLeftOperandAction_1_1_0_0_0()
						|| rule == grammarAccess.getXOtherOperatorExpressionRule()
						|| action == grammarAccess.getXOtherOperatorExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXAdditiveExpressionRule()
						|| action == grammarAccess.getXAdditiveExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXMultiplicativeExpressionRule()
						|| action == grammarAccess.getXMultiplicativeExpressionAccess().getXBinaryOperationLeftOperandAction_1_0_0_0()
						|| rule == grammarAccess.getXUnaryOperationRule()
						|| rule == grammarAccess.getXCastedExpressionRule()
						|| action == grammarAccess.getXCastedExpressionAccess().getXCastedExpressionTargetAction_1_0_0_0()
						|| rule == grammarAccess.getXPostfixOperationRule()
						|| action == grammarAccess.getXPostfixOperationAccess().getXPostfixOperationOperandAction_1_0_0()
						|| rule == grammarAccess.getXMemberFeatureCallRule()
						|| action == grammarAccess.getXMemberFeatureCallAccess().getXAssignmentAssignableAction_1_0_0_0_0()
						|| action == grammarAccess.getXMemberFeatureCallAccess().getXMemberFeatureCallMemberCallTargetAction_1_1_0_0_0()
						|| rule == grammarAccess.getXPrimaryExpressionRule()
						|| rule == grammarAccess.getXLiteralRule()
						|| rule == grammarAccess.getXCollectionLiteralRule()
						|| rule == grammarAccess.getXListLiteralRule()
						|| rule == grammarAccess.getXParenthesizedExpressionRule()
						|| rule == grammarAccess.getXExpressionOrVarDeclarationRule()) {
					sequence_XListLiteral(context, (XListLiteral) semanticObject); 
					return; 
				}
				else break;
			case XbasePackage.XMEMBER_FEATURE_CALL:
				sequence_XMemberFeatureCall(context, (XMemberFeatureCall) semanticObject); 
				return; 
			case XbasePackage.XNULL_LITERAL:
				sequence_XNullLiteral(context, (XNullLiteral) semanticObject); 
				return; 
			case XbasePackage.XNUMBER_LITERAL:
				sequence_XNumberLiteral(context, (XNumberLiteral) semanticObject); 
				return; 
			case XbasePackage.XPOSTFIX_OPERATION:
				sequence_XPostfixOperation(context, (XPostfixOperation) semanticObject); 
				return; 
			case XbasePackage.XRETURN_EXPRESSION:
				sequence_XReturnExpression(context, (XReturnExpression) semanticObject); 
				return; 
			case XbasePackage.XSET_LITERAL:
				sequence_XSetLiteral(context, (XSetLiteral) semanticObject); 
				return; 
			case XbasePackage.XSTRING_LITERAL:
				sequence_XStringLiteral(context, (XStringLiteral) semanticObject); 
				return; 
			case XbasePackage.XSWITCH_EXPRESSION:
				sequence_XSwitchExpression(context, (XSwitchExpression) semanticObject); 
				return; 
			case XbasePackage.XSYNCHRONIZED_EXPRESSION:
				sequence_XSynchronizedExpression(context, (XSynchronizedExpression) semanticObject); 
				return; 
			case XbasePackage.XTHROW_EXPRESSION:
				sequence_XThrowExpression(context, (XThrowExpression) semanticObject); 
				return; 
			case XbasePackage.XTRY_CATCH_FINALLY_EXPRESSION:
				sequence_XTryCatchFinallyExpression(context, (XTryCatchFinallyExpression) semanticObject); 
				return; 
			case XbasePackage.XTYPE_LITERAL:
				sequence_XTypeLiteral(context, (XTypeLiteral) semanticObject); 
				return; 
			case XbasePackage.XUNARY_OPERATION:
				sequence_XUnaryOperation(context, (XUnaryOperation) semanticObject); 
				return; 
			case XbasePackage.XVARIABLE_DECLARATION:
				sequence_XVariableDeclaration(context, (XVariableDeclaration) semanticObject); 
				return; 
			case XbasePackage.XWHILE_EXPRESSION:
				sequence_XWhileExpression(context, (XWhileExpression) semanticObject); 
				return; 
			}
		else if (epackage == XtypePackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case XtypePackage.XFUNCTION_TYPE_REF:
				sequence_XFunctionTypeRef(context, (XFunctionTypeRef) semanticObject); 
				return; 
			case XtypePackage.XIMPORT_SECTION:
				sequence_XImportSection(context, (XImportSection) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     AnnotationDef returns LAnnotationDef
	 *
	 * Constraint:
	 *     annotation=XAnnotation
	 */
	protected void sequence_AnnotationDef(ISerializationContext context, LAnnotationDef semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, OSBPTypesPackage.Literals.LANNOTATION_DEF__ANNOTATION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, OSBPTypesPackage.Literals.LANNOTATION_DEF__ANNOTATION));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getAnnotationDefAccess().getAnnotationXAnnotationParserRuleCall_0_0(), semanticObject.getAnnotation());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ResultFilter returns LAttributeMatchingConstraint
	 *     AttributeMatchingConstraint returns LAttributeMatchingConstraint
	 *
	 * Constraint:
	 *     (attribute=[LAttribute|ID] comparatorType=LComparatorType (matchingValue=STRING | matchingLiteral=[LEnumLiteral|ID]))
	 */
	protected void sequence_AttributeMatchingConstraint(ISerializationContext context, LAttributeMatchingConstraint semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Type returns LClass
	 *     Class returns LClass
	 *
	 * Constraint:
	 *     annotationInfo=Class_LClass_2
	 */
	protected void sequence_Class(ISerializationContext context, LClass semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, OSBPTypesPackage.Literals.LTYPE__ANNOTATION_INFO) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, OSBPTypesPackage.Literals.LTYPE__ANNOTATION_INFO));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getClassAccess().getLClassAnnotationInfoAction_2(), semanticObject.getAnnotationInfo());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Class.LClass_2 returns LClass
	 *
	 * Constraint:
	 *     annotations+=AnnotationDef*
	 */
	protected void sequence_Class_LClass_2(ISerializationContext context, LClass semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Type returns LDataType
	 *     DataType returns LDataType
	 *     ScalarType returns LDataType
	 *
	 * Constraint:
	 *     (
	 *         name=ValidIDWithKeywords 
	 *         (
	 *             (
	 *                 jvmTypeReference=JvmTypeReference 
	 *                 asPrimitive?='asPrimitive'? 
	 *                 constraints+=DataTypeConstraint* 
	 *                 (properties+=KeyAndValue properties+=KeyAndValue*)?
	 *             ) | 
	 *             (date?='dateType' dateType=DateType constraints+=DateConstraint* (properties+=KeyAndValue properties+=KeyAndValue*)?) | 
	 *             (asBlob?='asBlob' constraints+=BlobTypeConstraint* (properties+=KeyAndValue properties+=KeyAndValue*)?)
	 *         )
	 *     )
	 */
	protected void sequence_DataType(ISerializationContext context, LDataType semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCAssertFalse
	 *     DataTypeConstraint returns LDtCAssertFalse
	 *     DtCAssertFalse returns LDtCAssertFalse
	 *
	 * Constraint:
	 *     (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*
	 */
	protected void sequence_DtCAssertFalse(ISerializationContext context, LDtCAssertFalse semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCAssertTrue
	 *     DataTypeConstraint returns LDtCAssertTrue
	 *     DtCAssertTrue returns LDtCAssertTrue
	 *
	 * Constraint:
	 *     (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*
	 */
	protected void sequence_DtCAssertTrue(ISerializationContext context, LDtCAssertTrue semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCDecimalMax
	 *     DataTypeConstraint returns LDtCDecimalMax
	 *     DtCDecimalMax returns LDtCDecimalMax
	 *
	 * Constraint:
	 *     (max=LDecimal (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*)
	 */
	protected void sequence_DtCDecimalMax(ISerializationContext context, LDtCDecimalMax semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCDecimalMin
	 *     DataTypeConstraint returns LDtCDecimalMin
	 *     DtCDecimalMin returns LDtCDecimalMin
	 *
	 * Constraint:
	 *     (min=LDecimal (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*)
	 */
	protected void sequence_DtCDecimalMin(ISerializationContext context, LDtCDecimalMin semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCDigits
	 *     DataTypeConstraint returns LDtCDigits
	 *     DtCDigits returns LDtCDigits
	 *
	 * Constraint:
	 *     (intDigits=INT fractionDigits=INT (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*)
	 */
	protected void sequence_DtCDigits(ISerializationContext context, LDtCDigits semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCFuture
	 *     DateConstraint returns LDtCFuture
	 *     DtCFuture returns LDtCFuture
	 *
	 * Constraint:
	 *     (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*
	 */
	protected void sequence_DtCFuture(ISerializationContext context, LDtCFuture semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCNotNull
	 *     DataTypeConstraint returns LDtCNotNull
	 *     DateConstraint returns LDtCNotNull
	 *     BlobTypeConstraint returns LDtCNotNull
	 *     DtCNotNull returns LDtCNotNull
	 *
	 * Constraint:
	 *     (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*
	 */
	protected void sequence_DtCNotNull(ISerializationContext context, LDtCNotNull semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCNull
	 *     DataTypeConstraint returns LDtCNull
	 *     DateConstraint returns LDtCNull
	 *     BlobTypeConstraint returns LDtCNull
	 *     DtCNull returns LDtCNull
	 *
	 * Constraint:
	 *     (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*
	 */
	protected void sequence_DtCNull(ISerializationContext context, LDtCNull semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCNumericMax
	 *     DataTypeConstraint returns LDtCNumericMax
	 *     DtCNumericMax returns LDtCNumericMax
	 *
	 * Constraint:
	 *     (max=LInt (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*)
	 */
	protected void sequence_DtCNumericMax(ISerializationContext context, LDtCNumericMax semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCNumericMin
	 *     DataTypeConstraint returns LDtCNumericMin
	 *     DtCNumericMin returns LDtCNumericMin
	 *
	 * Constraint:
	 *     (min=LInt (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*)
	 */
	protected void sequence_DtCNumericMin(ISerializationContext context, LDtCNumericMin semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCPast
	 *     DateConstraint returns LDtCPast
	 *     DtCPast returns LDtCPast
	 *
	 * Constraint:
	 *     (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*
	 */
	protected void sequence_DtCPast(ISerializationContext context, LDtCPast semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCRegEx
	 *     DataTypeConstraint returns LDtCRegEx
	 *     DtCRegEx returns LDtCRegEx
	 *
	 * Constraint:
	 *     (pattern=STRING (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*)
	 */
	protected void sequence_DtCRegEx(ISerializationContext context, LDtCRegEx semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AllConstraints returns LDtCSize
	 *     DataTypeConstraint returns LDtCSize
	 *     DtCSize returns LDtCSize
	 *
	 * Constraint:
	 *     (min=INT max=INT (msgCode=QualifiedName | msgI18nKey=QualifiedName | severity=ConstraintSeverity)*)
	 */
	protected void sequence_DtCSize(ISerializationContext context, LDtCSize semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     EnumLiteral returns LEnumLiteral
	 *
	 * Constraint:
	 *     (name=TRANSLATABLEID default?='asDefault'? (value=INT | stringValue=STRING)?)
	 */
	protected void sequence_EnumLiteral(ISerializationContext context, LEnumLiteral semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Type returns LEnum
	 *     ScalarType returns LEnum
	 *     Enum returns LEnum
	 *
	 * Constraint:
	 *     (name=ID literals+=EnumLiteral literals+=EnumLiteral*)
	 */
	protected void sequence_Enum(ISerializationContext context, LEnum semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     KeyAndValue returns LKeyAndValue
	 *
	 * Constraint:
	 *     (key=STRING value=STRING)
	 */
	protected void sequence_KeyAndValue(ISerializationContext context, LKeyAndValue semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, OSBPTypesPackage.Literals.LKEY_AND_VALUE__KEY) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, OSBPTypesPackage.Literals.LKEY_AND_VALUE__KEY));
			if (transientValues.isValueTransient(semanticObject, OSBPTypesPackage.Literals.LKEY_AND_VALUE__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, OSBPTypesPackage.Literals.LKEY_AND_VALUE__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getKeyAndValueAccess().getKeySTRINGTerminalRuleCall_2_0(), semanticObject.getKey());
		feeder.accept(grammarAccess.getKeyAndValueAccess().getValueSTRINGTerminalRuleCall_5_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Modifier returns LModifier
	 *
	 * Constraint:
	 *     (final?='final' | static?='static' | visibility=LVisibility)+
	 */
	protected void sequence_Modifier(ISerializationContext context, LModifier semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Multiplicity returns LMultiplicity
	 *
	 * Constraint:
	 *     (lower=LowerBound upper=UpperBound?)
	 */
	protected void sequence_Multiplicity(ISerializationContext context, LMultiplicity semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ResultFilters returns LResultFilters
	 *
	 * Constraint:
	 *     resultFilters+=ResultFilter*
	 */
	protected void sequence_ResultFilters(ISerializationContext context, LResultFilters semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Type returns LStateClass
	 *     StateClass returns LStateClass
	 *
	 * Constraint:
	 *     (name=TRANSLATABLEID states+=State states+=State*)
	 */
	protected void sequence_StateClass(ISerializationContext context, LStateClass semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     State returns LState
	 *
	 * Constraint:
	 *     name=TRANSLATABLEID
	 */
	protected void sequence_State(ISerializationContext context, LState semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, OSBPTypesPackage.Literals.LTYPE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, OSBPTypesPackage.Literals.LTYPE__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getStateAccess().getNameTRANSLATABLEIDParserRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     TypedPackage returns LTypedPackage
	 *
	 * Constraint:
	 *     (name=QualifiedName types+=Type*)
	 */
	protected void sequence_TypedPackage(ISerializationContext context, LTypedPackage semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
}
