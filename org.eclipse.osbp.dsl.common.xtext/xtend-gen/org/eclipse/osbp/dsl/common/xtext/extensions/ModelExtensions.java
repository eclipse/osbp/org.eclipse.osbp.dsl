/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext.extensions;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.osbp.dsl.semantic.common.helper.Bounds;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.impl.JvmParameterizedTypeReferenceImpl;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class ModelExtensions {
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  @Inject
  private TypeReferences references;
  
  protected JvmTypeReference _toTypeReference(final LType type) {
    if (((type == null) || (this._iQualifiedNameProvider.getFullyQualifiedName(type) == null))) {
      return null;
    }
    return this._jvmTypesBuilder.newTypeRef(type, this._iQualifiedNameProvider.getFullyQualifiedName(type).toString());
  }
  
  protected JvmTypeReference _toTypeReference(final LDataType type) {
    JvmTypeReference _xifexpression = null;
    boolean _isSyntheticFlag = type.isSyntheticFlag();
    if (_isSyntheticFlag) {
      return this.toSyntheticTypeReference(type);
    } else {
      JvmTypeReference _xifexpression_1 = null;
      boolean _isDate = type.isDate();
      if (_isDate) {
        return this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Date.class, type));
      } else {
        JvmTypeReference _xifexpression_2 = null;
        boolean _isAsBlob = type.isAsBlob();
        if (_isAsBlob) {
          final JvmTypeReference typeRef = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Byte.TYPE, type));
          return this.references.createArrayType(typeRef);
        } else {
          JvmTypeReference _xifexpression_3 = null;
          boolean _isAsPrimitive = type.isAsPrimitive();
          if (_isAsPrimitive) {
            JvmTypeReference _xblockexpression = null;
            {
              JvmTypeReference _jvmTypeReference = null;
              if (type!=null) {
                _jvmTypeReference=type.getJvmTypeReference();
              }
              JvmType _type = null;
              if (_jvmTypeReference!=null) {
                _type=_jvmTypeReference.getType();
              }
              QualifiedName _fullyQualifiedName = null;
              if (_type!=null) {
                _fullyQualifiedName=this._iQualifiedNameProvider.getFullyQualifiedName(_type);
              }
              final QualifiedName fqn = _fullyQualifiedName;
              JvmTypeReference _switchResult = null;
              String _string = fqn.toString();
              boolean _matched = false;
              String _name = Integer.class.getName();
              if (Objects.equal(_string, _name)) {
                _matched=true;
                _switchResult = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Integer.TYPE, type));
              }
              if (!_matched) {
                String _name_1 = Boolean.class.getName();
                if (Objects.equal(_string, _name_1)) {
                  _matched=true;
                  _switchResult = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Boolean.TYPE, type));
                }
              }
              if (!_matched) {
                String _name_2 = Short.class.getName();
                if (Objects.equal(_string, _name_2)) {
                  _matched=true;
                  _switchResult = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Short.TYPE, type));
                }
              }
              if (!_matched) {
                String _name_3 = Long.class.getName();
                if (Objects.equal(_string, _name_3)) {
                  _matched=true;
                  _switchResult = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Long.TYPE, type));
                }
              }
              if (!_matched) {
                String _name_4 = Double.class.getName();
                if (Objects.equal(_string, _name_4)) {
                  _matched=true;
                  _switchResult = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Double.TYPE, type));
                }
              }
              if (!_matched) {
                String _name_5 = Float.class.getName();
                if (Objects.equal(_string, _name_5)) {
                  _matched=true;
                  _switchResult = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Float.TYPE, type));
                }
              }
              if (!_matched) {
                String _name_6 = Character.class.getName();
                if (Objects.equal(_string, _name_6)) {
                  _matched=true;
                  _switchResult = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Character.TYPE, type));
                }
              }
              if (!_matched) {
                String _name_7 = Byte.class.getName();
                if (Objects.equal(_string, _name_7)) {
                  _matched=true;
                  _switchResult = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Byte.TYPE, type));
                }
              }
              if (!_matched) {
                String _name_8 = Boolean.class.getName();
                if (Objects.equal(_string, _name_8)) {
                  _matched=true;
                  _switchResult = this._jvmTypesBuilder.newTypeRef(this.references.findDeclaredType(Boolean.TYPE, type));
                }
              }
              if (!_matched) {
                JvmTypeReference _jvmTypeReference_1 = null;
                if (type!=null) {
                  _jvmTypeReference_1=type.getJvmTypeReference();
                }
                _switchResult = _jvmTypeReference_1;
              }
              _xblockexpression = _switchResult;
            }
            _xifexpression_3 = _xblockexpression;
          } else {
            return this._jvmTypesBuilder.cloneWithProxies(type.getJvmTypeReference());
          }
          _xifexpression_2 = _xifexpression_3;
        }
        _xifexpression_1 = _xifexpression_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public String toNullReturnValue(final LScalarType dt) {
    if ((dt == null)) {
      return "null";
    }
    if ((dt instanceof LDataType)) {
      boolean _isBool = this.isBool(((LDataType)dt));
      if (_isBool) {
        return "Boolean.FALSE";
      }
      boolean _isDecimalOrWrapperType = this.isDecimalOrWrapperType(((LDataType)dt));
      if (_isDecimalOrWrapperType) {
        return "0.0";
      }
      boolean _isNumericOrWrapperType = this.isNumericOrWrapperType(((LDataType)dt));
      if (_isNumericOrWrapperType) {
        return "0";
      }
    }
    return "null";
  }
  
  public boolean isNumericOrWrapperType(final LDataType type) {
    JvmTypeReference _jvmTypeReference = type.getJvmTypeReference();
    final JvmParameterizedTypeReferenceImpl typeRef = ((JvmParameterizedTypeReferenceImpl) _jvmTypeReference);
    if ((typeRef != null)) {
      String typeIdentifier = "";
      if ((typeRef.basicGetType().eIsProxy() && (typeRef.basicGetType() instanceof InternalEObject))) {
        JvmType _basicGetType = typeRef.basicGetType();
        typeIdentifier = ((InternalEObject) _basicGetType).eProxyURI().fragment();
      } else {
        typeIdentifier = typeRef.getIdentifier();
      }
      boolean _matched = false;
      String _name = Integer.class.getName();
      if (Objects.equal(typeIdentifier, _name)) {
        _matched=true;
        return true;
      }
      if (!_matched) {
        if (Objects.equal(typeIdentifier, "integer")) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        String _name_1 = Short.class.getName();
        if (Objects.equal(typeIdentifier, _name_1)) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        if (Objects.equal(typeIdentifier, "short")) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        String _name_2 = Long.class.getName();
        if (Objects.equal(typeIdentifier, _name_2)) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        if (Objects.equal(typeIdentifier, "long")) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        String _name_3 = Double.class.getName();
        if (Objects.equal(typeIdentifier, _name_3)) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        if (Objects.equal(typeIdentifier, "double")) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        String _name_4 = Float.class.getName();
        if (Objects.equal(typeIdentifier, _name_4)) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        if (Objects.equal(typeIdentifier, "float")) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        String _name_5 = Character.class.getName();
        if (Objects.equal(typeIdentifier, _name_5)) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        if (Objects.equal(typeIdentifier, "char")) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        String _name_6 = Byte.class.getName();
        if (Objects.equal(typeIdentifier, _name_6)) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        if (Objects.equal(typeIdentifier, "byte")) {
          _matched=true;
          return true;
        }
      }
      return false;
    }
    return false;
  }
  
  public boolean isDecimalOrWrapperType(final LDataType type) {
    JvmTypeReference _jvmTypeReference = type.getJvmTypeReference();
    final JvmParameterizedTypeReferenceImpl typeRef = ((JvmParameterizedTypeReferenceImpl) _jvmTypeReference);
    if ((typeRef != null)) {
      String typeIdentifier = "";
      if ((typeRef.basicGetType().eIsProxy() && (typeRef.basicGetType() instanceof InternalEObject))) {
        JvmType _basicGetType = typeRef.basicGetType();
        typeIdentifier = ((InternalEObject) _basicGetType).eProxyURI().fragment();
      } else {
        typeIdentifier = typeRef.getIdentifier();
      }
      boolean _matched = false;
      String _name = Double.class.getName();
      if (Objects.equal(typeIdentifier, _name)) {
        _matched=true;
        return true;
      }
      if (!_matched) {
        if (Objects.equal(typeIdentifier, "double")) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        String _name_1 = Float.class.getName();
        if (Objects.equal(typeIdentifier, _name_1)) {
          _matched=true;
          return true;
        }
      }
      if (!_matched) {
        if (Objects.equal(typeIdentifier, "float")) {
          _matched=true;
          return true;
        }
      }
      return false;
    }
    return false;
  }
  
  public boolean isBool(final LDataType type) {
    boolean _isAsPrimitive = type.isAsPrimitive();
    if (_isAsPrimitive) {
      JvmTypeReference _jvmTypeReference = null;
      if (type!=null) {
        _jvmTypeReference=type.getJvmTypeReference();
      }
      JvmType _type = null;
      if (_jvmTypeReference!=null) {
        _type=_jvmTypeReference.getType();
      }
      QualifiedName _fullyQualifiedName = null;
      if (_type!=null) {
        _fullyQualifiedName=this._iQualifiedNameProvider.getFullyQualifiedName(_type);
      }
      final QualifiedName fqn = _fullyQualifiedName;
      String _string = fqn.toString();
      boolean _matched = false;
      String _name = Boolean.class.getName();
      if (Objects.equal(_string, _name)) {
        _matched=true;
        return true;
      }
      if (!_matched) {
        if (Objects.equal(_string, "boolean")) {
          _matched=true;
          return true;
        }
      }
      return false;
    }
    return false;
  }
  
  public boolean isString(final LDataType type) {
    JvmTypeReference _jvmTypeReference = null;
    if (type!=null) {
      _jvmTypeReference=type.getJvmTypeReference();
    }
    JvmType _type = null;
    if (_jvmTypeReference!=null) {
      _type=_jvmTypeReference.getType();
    }
    String _qualifiedName = null;
    if (_type!=null) {
      _qualifiedName=_type.getQualifiedName();
    }
    final String fqn = _qualifiedName;
    return "java.lang.String".equals(fqn);
  }
  
  public boolean isDate(final LDataType type) {
    return type.isDate();
  }
  
  protected boolean _isPrimitive(final LDataType type) {
    return type.isAsPrimitive();
  }
  
  protected boolean _isPrimitive(final LScalarType type) {
    return false;
  }
  
  public boolean isBasedOnDatatype(final LFeature feature) {
    if ((feature instanceof LAttribute)) {
      LScalarType _type = ((LAttribute)feature).getType();
      return (_type instanceof LDataType);
    }
    return false;
  }
  
  public LDataType getDatatype(final LFeature feature) {
    if ((feature instanceof LAttribute)) {
      LScalarType _type = ((LAttribute)feature).getType();
      if ((_type instanceof LDataType)) {
        LScalarType _type_1 = ((LAttribute)feature).getType();
        return ((LDataType) _type_1);
      }
    }
    return null;
  }
  
  public JvmTypeReference toSyntheticTypeReference(final LDataType type) {
    return null;
  }
  
  /**
   * Creates a jvmTypeReference based on the datatype.
   */
  public JvmTypeReference toDataTypeTypeReference(final LAttribute prop) {
    LScalarType _type = prop.getType();
    final LDataType lDt = ((LDataType) _type);
    return this.toTypeReference(lDt);
  }
  
  protected JvmTypeReference _toTypeReference(final LAttribute prop) {
    JvmTypeReference _xifexpression = null;
    LScalarType _type = prop.getType();
    if ((_type instanceof LDataType)) {
      return this.toDataTypeTypeReference(prop);
    } else {
      _xifexpression = this.toTypeReference(prop.getType());
    }
    return _xifexpression;
  }
  
  /**
   * Create a type reference with respect to many multiplicity
   */
  protected JvmTypeReference _toTypeReferenceWithMultiplicity(final LAnnotationTarget context) {
    JvmTypeReference typeRef = this.toTypeReference(context);
    if (((typeRef != null) && this.getBounds(context).isToMany())) {
      typeRef = this.toListTypeReference(context, typeRef);
    }
    return typeRef;
  }
  
  public JvmTypeReference toListTypeReference(final EObject context, final JvmTypeReference jvmTypeRef) {
    return this._jvmTypesBuilder.newTypeRef(context, List.class, jvmTypeRef);
  }
  
  protected Bounds _getBounds(final LFeature prop) {
    return Bounds.createFor(prop);
  }
  
  protected Bounds _getBounds(final LAnnotationTarget context) {
    return Bounds.createZeroToOne();
  }
  
  protected Bounds _getBounds(final Void context) {
    return Bounds.createZeroToOne();
  }
  
  public boolean isToMany(final LFeature prop) {
    return this.getBounds(prop).isToMany();
  }
  
  public boolean typeIsBoolean(final LFeature prop) {
    final LDataType dt = this.getDatatype(prop);
    if ((dt == null)) {
      return false;
    }
    final JvmTypeReference typeRef = dt.getJvmTypeReference();
    return (((((typeRef != null) && (!typeRef.eIsProxy())) && (typeRef.getType() != null)) && (!typeRef.getType().eIsProxy())) && 
      "boolean".equals(typeRef.getType().getIdentifier()));
  }
  
  public boolean typeIsEnum(final LAttribute prop) {
    LScalarType _type = prop.getType();
    return (_type instanceof LEnum);
  }
  
  public boolean typeIsStateClass(final LAttribute prop) {
    LScalarType _type = prop.getType();
    return (_type instanceof LStateClass);
  }
  
  /**
   * Returns true, if toCheck can be cast to superType
   */
  public boolean nameEquals(final LClass toCheck, final LClass superType) {
    final String toCheckFqn = this._iQualifiedNameProvider.getFullyQualifiedName(toCheck).toString();
    final String superTypeFqn = this._iQualifiedNameProvider.getFullyQualifiedName(superType).toString();
    return toCheckFqn.equals(superTypeFqn);
  }
  
  /**
   * Returns the {@link LPackage} for the given type.
   * 
   * @param lType
   * @return
   */
  public LPackage getPackage(final LType lType) {
    EObject current = lType;
    while (((current != null) && (!(current instanceof LPackage)))) {
      current = current.eContainer();
    }
    return ((LPackage) current);
  }
  
  protected String _toName(final LFeature feature) {
    if (((feature == null) || (feature.getName() == null))) {
      return "";
    }
    return feature.getName().replace("^", "");
  }
  
  protected String _toName(final LClass clazz) {
    if (((clazz == null) || (clazz.getName() == null))) {
      return "";
    }
    return clazz.getName().replace("^", "");
  }
  
  public String replaceCaret(final String value) {
    if ((value == null)) {
      return value;
    }
    return value.replace("^", "");
  }
  
  protected String _toName(final LType clazz) {
    if (((clazz == null) || (clazz.getName() == null))) {
      return "";
    }
    return clazz.getName().replace("^", "");
  }
  
  public String toQualifiedName(final LType type) {
    final LPackage pkg = this.getPackage(type);
    if ((pkg == null)) {
      return "";
    }
    String _name = pkg.getName();
    String _plus = ModelExtensions.operator_plus(_name, ".");
    String _name_1 = type.getName();
    return ModelExtensions.operator_plus(_plus, _name_1);
  }
  
  /**
   * The binary <code>+</code> operator that concatenates two strings.
   * 
   * @param a
   *            a string.
   * @param b
   *            another string.
   * @return <code>a + b</code>
   */
  public static String operator_plus(final String a, final String b) {
    return a.concat(b);
  }
  
  public String toDtoPackageName(final LTypedPackage pkg) {
    if ((pkg == null)) {
      return "notDefined";
    }
    boolean _contains = pkg.getName().contains("entities");
    if (_contains) {
      return pkg.getName().replace("entities", "dtos");
    } else {
      String _name = pkg.getName();
      return ModelExtensions.operator_plus(_name, ".dtos");
    }
  }
  
  public JvmTypeReference toTypeReference(final LAnnotationTarget type) {
    if (type instanceof LDataType) {
      return _toTypeReference((LDataType)type);
    } else if (type instanceof LAttribute) {
      return _toTypeReference((LAttribute)type);
    } else if (type instanceof LType) {
      return _toTypeReference((LType)type);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type).toString());
    }
  }
  
  public boolean isPrimitive(final LScalarType type) {
    if (type instanceof LDataType) {
      return _isPrimitive((LDataType)type);
    } else if (type != null) {
      return _isPrimitive(type);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type).toString());
    }
  }
  
  public JvmTypeReference toTypeReferenceWithMultiplicity(final LAnnotationTarget context) {
    return _toTypeReferenceWithMultiplicity(context);
  }
  
  public Bounds getBounds(final LAnnotationTarget prop) {
    if (prop instanceof LFeature) {
      return _getBounds((LFeature)prop);
    } else if (prop != null) {
      return _getBounds(prop);
    } else if (prop == null) {
      return _getBounds((Void)null);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public String toName(final LAnnotationTarget clazz) {
    if (clazz instanceof LClass) {
      return _toName((LClass)clazz);
    } else if (clazz instanceof LFeature) {
      return _toName((LFeature)clazz);
    } else if (clazz instanceof LType) {
      return _toName((LType)clazz);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(clazz).toString());
    }
  }
}
