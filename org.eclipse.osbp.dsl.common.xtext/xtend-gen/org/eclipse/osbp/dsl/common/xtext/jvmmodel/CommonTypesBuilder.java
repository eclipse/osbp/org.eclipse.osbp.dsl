/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext.jvmmodel;

import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.Collections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.common.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.common.xtext.extensions.NamingExtensions;
import org.eclipse.osbp.dsl.common.xtext.extensions.TreeAppendableExtensions;
import org.eclipse.osbp.dsl.common.xtext.jvmmodel.AnnotationCompiler;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LOperation;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.osbp.runtime.common.annotations.DomainDescription;
import org.eclipse.osbp.xtext.oxtype.resource.IExtendedModelAssociator;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtend2.lib.StringConcatenationClient;
import org.eclipse.xtext.common.types.JvmAnnotationReference;
import org.eclipse.xtext.common.types.JvmAnnotationTarget;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmField;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmIdentifiableElement;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.JvmVisibility;
import org.eclipse.xtext.common.types.TypesFactory;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class CommonTypesBuilder extends JvmTypesBuilder {
  private final static Logger LOGGER = LoggerFactory.getLogger(CommonTypesBuilder.class);
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private ModelExtensions _modelExtensions;
  
  @Inject
  @Extension
  private NamingExtensions _namingExtensions;
  
  @Inject
  @Extension
  private TreeAppendableExtensions _treeAppendableExtensions;
  
  @Inject(optional = true)
  private IExtendedModelAssociator associator;
  
  @Inject
  private AnnotationCompiler annotationCompiler;
  
  @Inject
  private TypesFactory typesFactory;
  
  @Inject
  private TypeReferences references;
  
  /**
   * Methods delegating to JvmTypesBuilder.
   * (Includes methods copied from JvmTypesBuilder because its visibility is protected.)
   */
  public JvmTypeReference newTypeRef(final EObject ctx, final QualifiedName qn, final JvmTypeReference... typeArgs) {
    return this.newTypeRef(ctx, qn.toString(), typeArgs);
  }
  
  @Override
  public JvmTypeReference newTypeRef(final EObject ctx, final Class<?> typeClass, final JvmTypeReference... typeArgs) {
    return super.newTypeRef(ctx, typeClass.getName(), typeArgs);
  }
  
  @Override
  public JvmTypeReference newTypeRef(final EObject ctx, final String typeName, final JvmTypeReference... typeArgs) {
    return super.newTypeRef(ctx, typeName, typeArgs);
  }
  
  public void setDocumentation(final JvmIdentifiableElement jvmElement, final CharSequence documentation) {
    String _string = null;
    if (documentation!=null) {
      _string=documentation.toString();
    }
    super.setDocumentation(jvmElement, _string);
  }
  
  public JvmField toField(final LFeature prop) {
    JvmField _xblockexpression = null;
    {
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName(this._modelExtensions.toName(prop));
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this.cloneWithProxies(this._modelExtensions.toTypeReferenceWithMultiplicity(prop)));
      this.annotationCompiler.processAnnotation(prop, jvmField);
      _xblockexpression = this.<JvmField>associate(prop, jvmField);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toIsDisposed(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("isDisposed");
      op.setReturnType(this.references.getTypeForName(Boolean.TYPE, sourceElement, null));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("@return true, if the object is disposed. ");
      _builder.newLine();
      _builder.append("Disposed means, that it is prepared for garbage collection and may not be used anymore. ");
      _builder.newLine();
      _builder.append("Accessing objects that are already disposed will cause runtime exceptions.");
      _builder.newLine();
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(sourceElement);
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, "return this.disposed;");
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public JvmField toDirtyField(final LClass sourceElement) {
    JvmField _xblockexpression = null;
    {
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName("dirty");
      jvmField.setTransient(true);
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this.references.getTypeForName(Boolean.TYPE, sourceElement, null));
      this.annotationCompiler.toDirtyAnnotation(sourceElement, jvmField);
      _xblockexpression = this.<JvmField>associate(sourceElement, jvmField);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toIsDirty(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("isDirty");
      op.setReturnType(this.references.getTypeForName(Boolean.TYPE, sourceElement, null));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("@return true, if the object is dirty. ");
      _builder.newLine();
      this.setDocumentation(op, _builder);
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("return dirty;");
        }
      };
      this.setBody(op, _client);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toSetDirty(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("setDirty");
      op.setReturnType(this.references.getTypeForName(Void.TYPE, sourceElement));
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(sourceElement, "dirty", this.references.getTypeForName(Boolean.TYPE, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Sets the dirty state of this object.");
      _builder.newLine();
      this.setDocumentation(op, _builder);
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          String _setDirtyStatement = CommonTypesBuilder.this.toSetDirtyStatement(sourceElement);
          _builder.append(_setDirtyStatement);
        }
      };
      this.setBody(op, _client);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  /**
   * Override in subclass
   */
  public String toSetDirtyStatement(final LClass sourceElement) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("this.dirty = dirty;");
    return _builder.toString();
  }
  
  public JvmOperation toIsCopy(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("isCopy");
      op.setReturnType(this.references.getTypeForName(Boolean.TYPE, sourceElement, null));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("@return true, if the object is a copy for a different object. ");
      _builder.newLine();
      _builder.append("In that special case of \"copy\" opposite references are treated differently");
      _builder.newLine();
      _builder.append("to ensure the crossreferences untouched about changes.");
      _builder.newLine();
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(sourceElement);
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, "return this.isCopy;");
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toCheckDisposed(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PRIVATE);
      op.setReturnType(this.references.getTypeForName(Void.TYPE, sourceElement));
      op.setSimpleName("checkDisposed");
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Checks whether the object is disposed.");
      _builder.newLine();
      _builder.append("@throws RuntimeException if the object is disposed.");
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(sourceElement);
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("if (isDisposed()) {");
        _builder_1.newLine();
        _builder_1.append("  ");
        _builder_1.append("throw new RuntimeException(\"Object already disposed: \" + this);");
        _builder_1.newLine();
        _builder_1.append("}");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_1);
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public String toCheckDisposedCall(final LFeature prop) {
    return "checkDisposed();\n";
  }
  
  public String toCheckDisposedCall(final LStateClass prop) {
    return "checkDisposed();\n";
  }
  
  public JvmOperation toMethod(final LOperation sourceElement, final String name, final JvmTypeReference returnType, final Procedure1<? super JvmOperation> initializer) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setSimpleName(name);
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setReturnType(this.cloneWithProxies(returnType));
      this.annotationCompiler.processAnnotation(sourceElement, op);
      this.<JvmOperation>associate(sourceElement, op);
      _xblockexpression = this.<JvmOperation>initializeSafely(op, initializer);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toGetter(final LStateClass prop) {
    return this.toGetter(prop, this._namingExtensions.toGetterName(prop));
  }
  
  public JvmOperation toGetter(final LFeature prop) {
    return this.toGetter(prop, this._namingExtensions.toGetterName(prop));
  }
  
  protected JvmOperation _toGetter(final EObject prop, final String methodName) {
    throw new IllegalStateException("Please override");
  }
  
  protected JvmOperation _toGetter(final LReference prop, final String methodName) {
    final String propertyName = this._modelExtensions.toName(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setSimpleName(methodName);
    op.setReturnType(this.cloneWithProxies(this._modelExtensions.toTypeReferenceWithMultiplicity(prop)));
    String _xifexpression = null;
    boolean _isToMany = this._modelExtensions.isToMany(prop);
    if (_isToMany) {
      String _plus = CommonTypesBuilder.operator_plus(
        "@return Returns an unmodifiable list of ", propertyName);
      _xifexpression = CommonTypesBuilder.operator_plus(_plus, ".");
    } else {
      String _xifexpression_1 = null;
      if ((propertyName != null)) {
        String _xifexpression_2 = null;
        boolean _isRequired = this._modelExtensions.getBounds(prop).isRequired();
        if (_isRequired) {
          _xifexpression_2 = "<em>required</em> ";
        } else {
          _xifexpression_2 = "";
        }
        String _concat = "@return Returns the ".concat(_xifexpression_2).concat(propertyName).concat(" property");
        String _xifexpression_3 = null;
        boolean _isRequired_1 = this._modelExtensions.getBounds(prop).isRequired();
        boolean _not = (!_isRequired_1);
        if (_not) {
          _xifexpression_3 = " or <code>null</code> if not present";
        } else {
          _xifexpression_3 = "";
        }
        _xifexpression_1 = _concat.concat(_xifexpression_3).concat(".");
      }
      _xifexpression = _xifexpression_1;
    }
    this.setDocumentation(op, _xifexpression);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      if ((it == null)) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _checkDisposedCall);
      boolean _isToMany_1 = this._modelExtensions.isToMany(prop);
      if (_isToMany_1) {
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, "return ");
        JvmTypeReference _newTypeRef = this.newTypeRef(prop, Collections.class);
        ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan, _newTypeRef);
        ITreeAppendable _doubleGreaterThan_2 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_1, ".unmodifiableList");
        String _collectionInternalGetterName = this._namingExtensions.toCollectionInternalGetterName(prop);
        String _plus_1 = CommonTypesBuilder.operator_plus(
          "(", _collectionInternalGetterName);
        String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, "());");
        this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_2, _plus_2);
      } else {
        String _plus_3 = CommonTypesBuilder.operator_plus("return this.", propertyName);
        String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, ";");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_4);
      }
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  protected JvmOperation _toGetter(final LAttribute prop, final String methodName) {
    final String propertyName = this._modelExtensions.toName(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setSimpleName(methodName);
    op.setReturnType(this.cloneWithProxies(this._modelExtensions.toTypeReferenceWithMultiplicity(prop)));
    boolean _isDerived = prop.isDerived();
    if (_isDerived) {
      final String customDoc = this.getDocumentation(prop);
      if ((customDoc != null)) {
        this.setDocumentation(op, customDoc);
      } else {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Calculates the value for the derived property ");
        String _name = prop.getName();
        _builder.append(_name);
        _builder.newLineIfNotEmpty();
        _builder.append(" ");
        _builder.newLine();
        _builder.append("@return ");
        String _name_1 = prop.getName();
        _builder.append(_name_1);
        _builder.append(" The derived property value");
        this.setDocumentation(op, _builder);
      }
      boolean _isDomainDescription = prop.isDomainDescription();
      if (_isDomainDescription) {
        EList<JvmAnnotationReference> _annotations = op.getAnnotations();
        JvmAnnotationReference _annotation = this.toAnnotation(prop, DomainDescription.class);
        this.<JvmAnnotationReference>operator_add(_annotations, _annotation);
      }
      this.setBody(op, prop.getDerivedGetterExpression());
    } else {
      String _xifexpression = null;
      boolean _isToMany = this._modelExtensions.isToMany(prop);
      if (_isToMany) {
        String _plus = CommonTypesBuilder.operator_plus(
          "@return Returns an unmodifiable list of ", propertyName);
        _xifexpression = CommonTypesBuilder.operator_plus(_plus, ".");
      } else {
        String _xifexpression_1 = null;
        if ((propertyName != null)) {
          String _xifexpression_2 = null;
          boolean _isRequired = this._modelExtensions.getBounds(prop).isRequired();
          if (_isRequired) {
            _xifexpression_2 = "<em>required</em> ";
          } else {
            _xifexpression_2 = "";
          }
          String _concat = "@return Returns the ".concat(_xifexpression_2).concat(propertyName).concat(" property");
          String _xifexpression_3 = null;
          boolean _isRequired_1 = this._modelExtensions.getBounds(prop).isRequired();
          boolean _not = (!_isRequired_1);
          if (_not) {
            _xifexpression_3 = " or <code>null</code> if not present";
          } else {
            _xifexpression_3 = "";
          }
          _xifexpression_1 = _concat.concat(_xifexpression_3).concat(".");
        }
        _xifexpression = _xifexpression_1;
      }
      this.setDocumentation(op, _xifexpression);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(prop);
        String _checkDisposedCall = this.toCheckDisposedCall(prop);
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _checkDisposedCall);
        boolean _isToMany_1 = this._modelExtensions.isToMany(prop);
        if (_isToMany_1) {
          ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, "return ");
          JvmTypeReference _newTypeRef = this.newTypeRef(prop, Collections.class);
          ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan, _newTypeRef);
          ITreeAppendable _doubleGreaterThan_2 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_1, ".unmodifiableList");
          String _collectionInternalGetterName = this._namingExtensions.toCollectionInternalGetterName(prop);
          String _plus_1 = CommonTypesBuilder.operator_plus(
            "(", _collectionInternalGetterName);
          String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, "());");
          this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_2, _plus_2);
        } else {
          String _plus_3 = CommonTypesBuilder.operator_plus("return this.", propertyName);
          String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, ";");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_4);
        }
      };
      this.setBody(op, _function);
    }
    return this.<JvmOperation>associate(prop, op);
  }
  
  protected JvmOperation _toGetter(final LStateClass prop, final String methodName) {
    final String propertyName = StringExtensions.toFirstLower(this._modelExtensions.toName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setSimpleName(methodName);
    op.setReturnType(this.cloneWithProxies(this._modelExtensions.toTypeReferenceWithMultiplicity(prop)));
    String _xifexpression = null;
    if ((propertyName != null)) {
      String _xifexpression_1 = null;
      boolean _isRequired = this._modelExtensions.getBounds(prop).isRequired();
      if (_isRequired) {
        _xifexpression_1 = "<em>required</em> ";
      } else {
        _xifexpression_1 = "";
      }
      String _concat = "@return Returns the ".concat(_xifexpression_1).concat(propertyName).concat(" property");
      String _xifexpression_2 = null;
      boolean _isRequired_1 = this._modelExtensions.getBounds(prop).isRequired();
      boolean _not = (!_isRequired_1);
      if (_not) {
        _xifexpression_2 = " or <code>null</code> if not present";
      } else {
        _xifexpression_2 = "";
      }
      _xifexpression = _concat.concat(_xifexpression_2).concat(".");
    }
    this.setDocumentation(op, _xifexpression);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      if ((it == null)) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _checkDisposedCall);
      String _plus = CommonTypesBuilder.operator_plus("return this.", propertyName);
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, ";");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_1);
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  /**
   * The binary <code>+</code> operator that concatenates two strings.
   * 
   * @param a
   *            a string.
   * @param b
   *            another string.
   * @return <code>a + b</code>
   */
  public static String operator_plus(final String a, final String b) {
    if ((b == null)) {
      return a;
    }
    return a.concat(b);
  }
  
  public JvmField toPrimitiveTypeField(final EObject sourceElement, final String name, final Class<?> primitiveType) {
    JvmField _xblockexpression = null;
    {
      final JvmTypeReference typeRef = this.newTypeRef(sourceElement, primitiveType, null);
      final JvmField result = this.typesFactory.createJvmField();
      result.setSimpleName(name);
      result.setVisibility(JvmVisibility.PRIVATE);
      result.setType(this.cloneWithProxies(typeRef));
      _xblockexpression = this.<JvmField>associate(sourceElement, result);
    }
    return _xblockexpression;
  }
  
  public JvmField toPropertyChangeSupportField(final EObject sourceElement) {
    JvmField _xblockexpression = null;
    {
      final JvmTypeReference typeRef = this.newTypeRef(sourceElement, PropertyChangeSupport.class, null);
      final JvmField result = this.typesFactory.createJvmField();
      result.setSimpleName("propertyChangeSupport");
      result.setVisibility(JvmVisibility.PRIVATE);
      result.setType(this.cloneWithProxies(typeRef));
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(sourceElement);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("new PropertyChangeSupport(this)");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder);
      };
      this.setInitializer(result, _function);
      _xblockexpression = this.<JvmField>associate(sourceElement, result);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toAddPropertyChangeListener(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("addPropertyChangeListener");
      op.setReturnType(this.references.getTypeForName(Void.TYPE, sourceElement, null));
      final JvmTypeReference parameterRef = this.references.getTypeForName(PropertyChangeListener.class, sourceElement, null);
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(sourceElement, "listener", parameterRef);
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("@see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)");
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(sourceElement);
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, "propertyChangeSupport.addPropertyChangeListener(listener);");
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toAddPropertyChangeListenerWithProperty(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("addPropertyChangeListener");
      op.setReturnType(this.references.getTypeForName(Void.TYPE, sourceElement, null));
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(sourceElement, "propertyName", this.references.getTypeForName(String.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
      EList<JvmFormalParameter> _parameters_1 = op.getParameters();
      JvmFormalParameter _parameter_1 = this.toParameter(sourceElement, "listener", 
        this.references.getTypeForName(PropertyChangeListener.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("@see PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)");
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(sourceElement);
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, "propertyChangeSupport.addPropertyChangeListener(propertyName, listener);");
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toRemovePropertyChangeListener(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("removePropertyChangeListener");
      op.setReturnType(this.references.getTypeForName(Void.TYPE, sourceElement, null));
      final JvmTypeReference parameterRef = this.references.getTypeForName(PropertyChangeListener.class, sourceElement, null);
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(sourceElement, "listener", parameterRef);
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("@see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)");
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(sourceElement);
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, "propertyChangeSupport.removePropertyChangeListener(listener);");
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toRemovePropertyChangeListenerWithProperty(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("removePropertyChangeListener");
      op.setReturnType(this.references.getTypeForName(Void.TYPE, sourceElement, null));
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(sourceElement, "propertyName", this.references.getTypeForName(String.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
      EList<JvmFormalParameter> _parameters_1 = op.getParameters();
      JvmFormalParameter _parameter_1 = this.toParameter(sourceElement, "listener", 
        this.references.getTypeForName(PropertyChangeListener.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("@see PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)");
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(sourceElement);
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, "propertyChangeSupport.removePropertyChangeListener(propertyName, listener);");
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toFirePropertyChange(final LClass sourceElement) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("firePropertyChange");
      op.setReturnType(this.references.getTypeForName(Void.TYPE, sourceElement, null));
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(sourceElement, "propertyName", this.references.getTypeForName(String.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
      EList<JvmFormalParameter> _parameters_1 = op.getParameters();
      JvmFormalParameter _parameter_1 = this.toParameter(sourceElement, "oldValue", 
        this.references.getTypeForName(Object.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
      EList<JvmFormalParameter> _parameters_2 = op.getParameters();
      JvmFormalParameter _parameter_2 = this.toParameter(sourceElement, "newValue", 
        this.references.getTypeForName(Object.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters_2, _parameter_2);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("@see PropertyChangeSupport#firePropertyChange(String, Object, Object)");
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        if ((it == null)) {
          return;
        }
        final ITreeAppendable p = it.trace(sourceElement);
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, "propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);");
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public void translateAnnotationDefTo(final LAnnotationDef annotationDef, final JvmAnnotationTarget target) {
    if (((annotationDef == null) || (target == null))) {
      return;
    }
    final JvmAnnotationReference annotationReference = super.getJvmAnnotationReference(annotationDef.getAnnotation());
    if ((annotationReference != null)) {
      EList<JvmAnnotationReference> _annotations = target.getAnnotations();
      this.<JvmAnnotationReference>operator_add(_annotations, annotationReference);
    }
  }
  
  public JvmGenericType toJvmType(final LClass lClass) {
    JvmGenericType _xblockexpression = null;
    {
      final JvmGenericType type = this.createJvmGenericType(lClass, this._iQualifiedNameProvider.getFullyQualifiedName(lClass).toString());
      type.setAbstract(lClass.isAbstract());
      _xblockexpression = this.<JvmGenericType>associate(lClass, type);
    }
    return _xblockexpression;
  }
  
  @Override
  public JvmOperation toHashCodeMethod(final EObject sourceElement, final boolean extendsSomethingWithProperHashCode, final JvmDeclaredType declaredType) {
    if (((sourceElement == null) || (declaredType == null))) {
      return null;
    }
    return this.toHashCodeMethod(sourceElement, extendsSomethingWithProperHashCode, 
      Iterables.<JvmField>toArray(Iterables.<JvmField>filter(declaredType.getMembers(), JvmField.class), JvmField.class));
  }
  
  @Override
  public JvmOperation toHashCodeMethod(final EObject sourceElement, final boolean extendsSomethingWithProperHashCode, final JvmField... jvmFields) {
    if ((sourceElement == null)) {
      return null;
    }
    final JvmOperation result = this.toMethod(sourceElement, "hashCode", 
      this.references.getTypeForName(Integer.TYPE, sourceElement), null);
    if ((result == null)) {
      return null;
    }
    result.getAnnotations().add(this.toAnnotation(sourceElement, Override.class));
    this.setBody(result, 
      new Procedure1<ITreeAppendable>() {
        @Override
        public void apply(final ITreeAppendable p) {
          if ((p == null)) {
            return;
          }
          p.append(" int prime = 31;");
          if (extendsSomethingWithProperHashCode) {
            p.newLine().append("int result = super.hashCode();");
          } else {
            p.newLine().append("int result = 1;");
          }
          for (final JvmField field : jvmFields) {
            {
              final String typeName = field.getType().getIdentifier();
              boolean _equals = Boolean.TYPE.getName().equals(typeName);
              if (_equals) {
                ITreeAppendable _newLine = p.newLine();
                String _simpleName = field.getSimpleName();
                String _plus = CommonTypesBuilder.operator_plus(
                  "result = prime * result + (this.", _simpleName);
                String _plus_1 = CommonTypesBuilder.operator_plus(_plus, " ? 1231 : 1237);");
                _newLine.append(_plus_1);
              } else {
                if ((((Integer.TYPE.getName().equals(typeName) || Character.TYPE.getName().equals(typeName)) || 
                  Byte.TYPE.getName().equals(typeName)) || Short.TYPE.getName().equals(typeName))) {
                  ITreeAppendable _newLine_1 = p.newLine();
                  String _simpleName_1 = field.getSimpleName();
                  String _plus_2 = CommonTypesBuilder.operator_plus("result = prime * result + this.", _simpleName_1);
                  String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, ";");
                  _newLine_1.append(_plus_3);
                } else {
                  boolean _equals_1 = Long.TYPE.getName().equals(typeName);
                  if (_equals_1) {
                    ITreeAppendable _newLine_2 = p.newLine();
                    String _simpleName_2 = field.getSimpleName();
                    String _plus_4 = CommonTypesBuilder.operator_plus(
                      "result = prime * result + (int) (this.", _simpleName_2);
                    String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, " ^ (this.");
                    String _simpleName_3 = field.getSimpleName();
                    String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, _simpleName_3);
                    String _plus_7 = CommonTypesBuilder.operator_plus(_plus_6, " >>> 32));");
                    _newLine_2.append(_plus_7);
                  } else {
                    boolean _equals_2 = Float.TYPE.getName().equals(typeName);
                    if (_equals_2) {
                      ITreeAppendable _newLine_3 = p.newLine();
                      String _simpleName_4 = field.getSimpleName();
                      String _plus_8 = CommonTypesBuilder.operator_plus(
                        "result = prime * result + Float.floatToIntBits(this.", _simpleName_4);
                      String _plus_9 = CommonTypesBuilder.operator_plus(_plus_8, ");");
                      _newLine_3.append(_plus_9);
                    } else {
                      boolean _equals_3 = Double.TYPE.getName().equals(typeName);
                      if (_equals_3) {
                        ITreeAppendable _newLine_4 = p.newLine();
                        String _simpleName_5 = field.getSimpleName();
                        String _plus_10 = CommonTypesBuilder.operator_plus(
                          "result = prime * result + (int) (Double.doubleToLongBits(this.", _simpleName_5);
                        String _plus_11 = CommonTypesBuilder.operator_plus(_plus_10, 
                          ") ^ (Double.doubleToLongBits(this.");
                        String _simpleName_6 = field.getSimpleName();
                        String _plus_12 = CommonTypesBuilder.operator_plus(_plus_11, _simpleName_6);
                        String _plus_13 = CommonTypesBuilder.operator_plus(_plus_12, ") >>> 32));");
                        _newLine_4.append(_plus_13);
                      } else {
                        ITreeAppendable _newLine_5 = p.newLine();
                        String _simpleName_7 = field.getSimpleName();
                        String _plus_14 = CommonTypesBuilder.operator_plus(
                          "result = prime * result + ((this.", _simpleName_7);
                        String _plus_15 = CommonTypesBuilder.operator_plus(_plus_14, "== null) ? 0 : this.");
                        String _simpleName_8 = field.getSimpleName();
                        String _plus_16 = CommonTypesBuilder.operator_plus(_plus_15, _simpleName_8);
                        String _plus_17 = CommonTypesBuilder.operator_plus(_plus_16, ".hashCode());");
                        _newLine_5.append(_plus_17);
                      }
                    }
                  }
                }
              }
            }
          }
          p.newLine().append("return result;");
        }
      });
    return result;
  }
  
  @Override
  public JvmOperation toEqualsMethod(final EObject sourceElement, final JvmDeclaredType declaredType, final boolean isDelegateToSuperEquals) {
    if (((sourceElement == null) || (declaredType == null))) {
      return null;
    }
    return this.toEqualsMethod(sourceElement, declaredType, isDelegateToSuperEquals, 
      Iterables.<JvmField>toArray(Iterables.<JvmField>filter(declaredType.getMembers(), JvmField.class), JvmField.class));
  }
  
  @Override
  public JvmOperation toEqualsMethod(final EObject sourceElement, final JvmDeclaredType declaredType, final boolean isDelegateToSuperEquals, final JvmField... jvmFields) {
    if (((sourceElement == null) || (declaredType == null))) {
      return null;
    }
    final JvmOperation result = this.toMethod(sourceElement, "equals", 
      this.references.getTypeForName(Boolean.TYPE, sourceElement), null);
    if ((result == null)) {
      return null;
    }
    result.getAnnotations().add(this.toAnnotation(sourceElement, Override.class));
    result.getParameters().add(
      this.toParameter(sourceElement, "obj", this.references.getTypeForName(Object.class, sourceElement)));
    this.setBody(result, 
      new Procedure1<ITreeAppendable>() {
        @Override
        public void apply(final ITreeAppendable p) {
          if ((p == null)) {
            return;
          }
          p.append("return equalVersions(obj);");
        }
      });
    return result;
  }
  
  public JvmOperation toEqualVersionsMethod(final EObject sourceElement, final JvmDeclaredType declaredType, final boolean isDelegateToSuperEquals, final JvmField idField, final JvmField versionField) {
    if (((sourceElement == null) || (declaredType == null))) {
      return null;
    }
    final JvmOperation result = this.toMethod(sourceElement, "equalVersions", 
      this.references.getTypeForName(Boolean.TYPE, sourceElement), null);
    if ((result == null)) {
      return null;
    }
    result.getParameters().add(
      this.toParameter(sourceElement, "obj", this.references.getTypeForName(Object.class, sourceElement)));
    boolean _or = false;
    JvmTypeReference _type = null;
    if (idField!=null) {
      _type=idField.getType();
    }
    String _simpleName = null;
    if (_type!=null) {
      _simpleName=_type.getSimpleName();
    }
    boolean _equals = "UUIDHist".equals(_simpleName);
    if (_equals) {
      _or = true;
    } else {
      JvmTypeReference _type_1 = null;
      if (idField!=null) {
        _type_1=idField.getType();
      }
      String _simpleName_1 = null;
      if (_type_1!=null) {
        _simpleName_1=_type_1.getSimpleName();
      }
      boolean _equals_1 = "String".equals(_simpleName_1);
      _or = _equals_1;
    }
    if (_or) {
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("if (this == obj)");
          _builder.newLine();
          _builder.append("  ");
          _builder.append("return true;");
          _builder.newLine();
          _builder.append("if (obj == null)");
          _builder.newLine();
          _builder.append("  ");
          _builder.append("return false;");
          _builder.newLine();
          _builder.append("if (getClass() != obj.getClass())");
          _builder.newLine();
          _builder.append("  ");
          _builder.append("return false;");
          _builder.newLine();
          String _simpleName = declaredType.getSimpleName();
          _builder.append(_simpleName);
          _builder.append(" other = (");
          String _simpleName_1 = declaredType.getSimpleName();
          _builder.append(_simpleName_1);
          _builder.append(") obj;");
          _builder.newLineIfNotEmpty();
          _builder.append("if (this.");
          String _simpleName_2 = idField.getSimpleName();
          _builder.append(_simpleName_2);
          _builder.append(" == null) {");
          _builder.newLineIfNotEmpty();
          _builder.append("  ");
          _builder.append("if (other.");
          String _simpleName_3 = idField.getSimpleName();
          _builder.append(_simpleName_3, "  ");
          _builder.append(" != null)");
          _builder.newLineIfNotEmpty();
          _builder.append("    ");
          _builder.append("return false;");
          _builder.newLine();
          _builder.append("} else if (!this.");
          String _simpleName_4 = idField.getSimpleName();
          _builder.append(_simpleName_4);
          _builder.append(".equals(other.");
          String _simpleName_5 = idField.getSimpleName();
          _builder.append(_simpleName_5);
          _builder.append("))");
          _builder.newLineIfNotEmpty();
          _builder.append("  ");
          _builder.append("return false;");
          _builder.newLine();
          {
            if ((versionField != null)) {
              _builder.append("if (other.");
              String _simpleName_6 = versionField.getSimpleName();
              _builder.append(_simpleName_6);
              _builder.append(" != this.");
              String _simpleName_7 = versionField.getSimpleName();
              _builder.append(_simpleName_7);
              _builder.append(")");
              _builder.newLineIfNotEmpty();
              _builder.append("  ");
              _builder.append("return false;");
            }
          }
          _builder.newLineIfNotEmpty();
          _builder.append("return true;");
          _builder.newLine();
        }
      };
      this.setBody(result, _client);
    } else {
      StringConcatenationClient _client_1 = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("if (this == obj)");
          _builder.newLine();
          _builder.append("  ");
          _builder.append("return true;");
          _builder.newLine();
          _builder.append("if (obj == null)");
          _builder.newLine();
          _builder.append("  ");
          _builder.append("return false;");
          _builder.newLine();
          _builder.append("if (getClass() != obj.getClass())");
          _builder.newLine();
          _builder.append("  ");
          _builder.append("return false;");
          _builder.newLine();
          String _simpleName = declaredType.getSimpleName();
          _builder.append(_simpleName);
          _builder.append(" other = (");
          String _simpleName_1 = declaredType.getSimpleName();
          _builder.append(_simpleName_1);
          _builder.append(") obj;");
          _builder.newLineIfNotEmpty();
          _builder.append("if (this.");
          String _simpleName_2 = idField.getSimpleName();
          _builder.append(_simpleName_2);
          _builder.append(" != other.");
          String _simpleName_3 = idField.getSimpleName();
          _builder.append(_simpleName_3);
          _builder.append(" || (other.");
          String _simpleName_4 = idField.getSimpleName();
          _builder.append(_simpleName_4);
          _builder.append(" == 0 && this.");
          String _simpleName_5 = idField.getSimpleName();
          _builder.append(_simpleName_5);
          _builder.append(" == 0))");
          _builder.newLineIfNotEmpty();
          _builder.append("  ");
          _builder.append("return false;");
          _builder.newLine();
          {
            if ((versionField != null)) {
              _builder.append("if (other.");
              String _simpleName_6 = versionField.getSimpleName();
              _builder.append(_simpleName_6);
              _builder.append(" != this.");
              String _simpleName_7 = versionField.getSimpleName();
              _builder.append(_simpleName_7);
              _builder.append(")");
              _builder.newLineIfNotEmpty();
              _builder.append("  ");
              _builder.append("return false;");
            }
          }
          _builder.newLineIfNotEmpty();
          _builder.append("return true;");
          _builder.newLine();
        }
      };
      this.setBody(result, _client_1);
    }
    return result;
  }
  
  public JvmGenericType getByPostfix(final EObject sourceElement, final String selector) {
    if ((this.associator != null)) {
      return this.associator.getByPostfix(sourceElement, selector);
    }
    CommonTypesBuilder.LOGGER.error("Associator is null!");
    return null;
  }
  
  public JvmOperation toGetter(final EObject prop, final String methodName) {
    if (prop instanceof LStateClass) {
      return _toGetter((LStateClass)prop, methodName);
    } else if (prop instanceof LAttribute) {
      return _toGetter((LAttribute)prop, methodName);
    } else if (prop instanceof LReference) {
      return _toGetter((LReference)prop, methodName);
    } else if (prop != null) {
      return _toGetter(prop, methodName);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop, methodName).toString());
    }
  }
}
