/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext.jvmmodel;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextSyntaxDiagnostic;
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Infers a JVM model from the source model.</p>
 * 
 * <p>The JVM model should contain all elements that would appear in the Java code
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>
 */
@SuppressWarnings("all")
public class CommonGrammarJvmModelInferrer extends AbstractModelInferrer {
  protected final Logger log = LoggerFactory.getLogger(this.getClass());
  
  /**
   * Returns true if the resource of the specified object has syntax errors.
   * This method allows an eary abort of inferring that would cause NPEs because of not loaded
   * Ecore proxy instances.
   * Inspired by DefaultFoldingStructureProvider.modelChanged().
   */
  public boolean hasSyntaxErrors(final EObject o) {
    final Function1<Resource.Diagnostic, Boolean> _function = (Resource.Diagnostic it) -> {
      return Boolean.valueOf((it instanceof XtextSyntaxDiagnostic));
    };
    final boolean hasSyntaxErrors = IterableExtensions.<Resource.Diagnostic>exists(o.eResource().getErrors(), _function);
    if (hasSyntaxErrors) {
      this.log.warn(("Abort inferring due to syntax errors: " + o));
    }
    return hasSyntaxErrors;
  }
}
