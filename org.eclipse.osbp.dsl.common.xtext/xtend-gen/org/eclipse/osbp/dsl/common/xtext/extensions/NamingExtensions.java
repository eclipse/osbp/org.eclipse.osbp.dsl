/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext.extensions;

import com.google.inject.Inject;
import org.eclipse.osbp.dsl.common.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class NamingExtensions {
  @Inject
  @Extension
  private ModelExtensions _modelExtensions;
  
  public String toSetterName(final LFeature prop) {
    String _xblockexpression = null;
    {
      if (((prop == null) || (this._modelExtensions.toName(prop) == null))) {
        return "setMISSING_NAME";
      }
      _xblockexpression = "set".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(prop)));
    }
    return _xblockexpression;
  }
  
  public String toGetterName(final LFeature prop) {
    String _xblockexpression = null;
    {
      if (((prop == null) || (this._modelExtensions.toName(prop) == null))) {
        return "getMISSING_NAME";
      }
      String _xifexpression = null;
      boolean _typeIsBoolean = this._modelExtensions.typeIsBoolean(prop);
      if (_typeIsBoolean) {
        _xifexpression = "is".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(prop)));
      } else {
        _xifexpression = "get".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(prop)));
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public String toSetterName(final LStateClass prop) {
    String _xblockexpression = null;
    {
      if (((prop == null) || (this._modelExtensions.toName(prop) == null))) {
        return "setMISSING_NAME";
      }
      _xblockexpression = "set".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(prop)));
    }
    return _xblockexpression;
  }
  
  public String toGetterName(final LStateClass prop) {
    String _xblockexpression = null;
    {
      if (((prop == null) || (this._modelExtensions.toName(prop) == null))) {
        return "getMISSING_NAME";
      }
      _xblockexpression = "get".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(prop)));
    }
    return _xblockexpression;
  }
  
  public String toBooleanGetterName(final LFeature prop) {
    if (((prop == null) || (this._modelExtensions.toName(prop) == null))) {
      return "isMISSING_NAME";
    }
    final String propName = StringExtensions.toFirstLower(this._modelExtensions.toName(prop));
    if ((propName.startsWith("is") || propName.startsWith("has"))) {
      return propName;
    }
    return "is".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(prop)));
  }
  
  public String toCollectionAdderName(final LFeature collectionProp) {
    return "addTo".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(collectionProp)));
  }
  
  public String toCollectionRemoverName(final LFeature collectionProp) {
    return "removeFrom".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(collectionProp)));
  }
  
  public String toCollectionInternalGetterName(final LFeature collectionProp) {
    return "internalGet".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(collectionProp)));
  }
  
  public String toCollectionInternalAdderName(final LFeature collectionProp) {
    return "internalAddTo".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(collectionProp)));
  }
  
  public String toCollectionInternalRemoverName(final LFeature collectionProp) {
    return "internalRemoveFrom".concat(StringExtensions.toFirstUpper(this._modelExtensions.toName(collectionProp)));
  }
  
  public String toInternalSetterName(final LFeature ref) {
    String _name = this._modelExtensions.toName(ref);
    String _firstUpper = null;
    if (_name!=null) {
      _firstUpper=StringExtensions.toFirstUpper(_name);
    }
    return "internalSet".concat(_firstUpper);
  }
  
  /**
   * Returns the property name that is used for method signatures.
   */
  public String toMethodParamName(final LFeature prop) {
    return this.toGeneratorDefaultMethodParamName(prop);
  }
  
  /**
   * Returns the generator default method param name.
   */
  public String toGeneratorDefaultMethodParamName(final LFeature sourceElement) {
    return this.toMethodParamName(this._modelExtensions.toName(sourceElement));
  }
  
  /**
   * Returns the property name that is used for method signatures.
   */
  public String toMethodParamName(final String name) {
    return String.format("%s", name);
  }
}
