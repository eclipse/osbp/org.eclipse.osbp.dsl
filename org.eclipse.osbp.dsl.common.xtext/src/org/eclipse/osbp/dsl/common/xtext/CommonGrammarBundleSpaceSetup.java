/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext;

import org.eclipse.osbp.xtext.builder.xbase.setups.XbaseWithAnnotationsBundleSpaceStandaloneSetup;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Initialization support for running Xtext languages without equinox extension
 * registry
 */
@SuppressWarnings("restriction")
public class CommonGrammarBundleSpaceSetup extends CommonGrammarStandaloneSetup {

	public static void doSetup() {
		new CommonGrammarBundleSpaceSetup()
				.createInjectorAndDoEMFRegistration();
	}

	public Injector createInjectorAndDoEMFRegistration() {
		XbaseWithAnnotationsBundleSpaceStandaloneSetup.doSetup();

		Injector injector = createInjector();
		register(injector);
		return injector;
	}

	public Injector createInjector() {
		return Guice
				.createInjector(new org.eclipse.osbp.dsl.common.xtext.CommonGrammarBundleSpaceModule());
	}

}
