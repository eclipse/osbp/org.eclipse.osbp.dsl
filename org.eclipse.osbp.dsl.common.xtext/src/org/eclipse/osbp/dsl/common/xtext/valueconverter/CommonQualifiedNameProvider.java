/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext.valueconverter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.common.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.scoping.XbaseQualifiedNameProvider;

import com.google.inject.Inject;

@SuppressWarnings("restriction")
public class CommonQualifiedNameProvider extends XbaseQualifiedNameProvider {

	@Inject
	private IQualifiedNameConverter qualifiedNameConverter;
	@Inject
	private ModelExtensions extensions;

	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		if (obj == null) {
			return QualifiedName.create("");
		}

		// if (obj instanceof LClass) {
		// LPackage pkg = extensions.getPackage((LClass) obj);
		// if (pkg != null) {
		// final String qualifiedName = String.format("%s.%s",
		// pkg.getName(), ((LClass) obj).getName());
		// if (qualifiedName == null)
		// return null;
		// return qualifiedNameConverter.toQualifiedName(qualifiedName);
		// } else {
		// return QualifiedName.create("");
		// }
		// } else if (obj instanceof LEnum) {
		// LPackage pkg = extensions.getPackage((LEnum) obj);
		// if (pkg != null) {
		// final String qualifiedName = String.format("%s.%s",
		// pkg.getName(), ((LEnum) obj).getName());
		// if (qualifiedName == null)
		// return null;
		// return qualifiedNameConverter.toQualifiedName(qualifiedName);
		// } else {
		// return QualifiedName.create("");
		// }
		// } else if (obj instanceof LFeature) {
		// LFeature prop = (LFeature) obj;
		// String name = prop.getName();
		// return name != null ? qualifiedNameConverter.toQualifiedName(name)
		// : null;
		// } else if (obj instanceof LDataType) {
		// LPackage pkg = extensions.getPackage((LDataType) obj);
		// if (pkg != null) {
		// final String qualifiedName = String.format("%s.%s",
		// pkg.getName(), ((LDataType) obj).getName());
		// if (qualifiedName == null)
		// return null;
		// return qualifiedNameConverter.toQualifiedName(qualifiedName);
		// } else {
		// return QualifiedName.create("");
		// }
		// } else
		//
		if (obj instanceof LAnnotationDef) {
			return super.getFullyQualifiedName(((LAnnotationDef) obj).getAnnotation());
		}
		return super.getFullyQualifiedName(obj);
	}
}
