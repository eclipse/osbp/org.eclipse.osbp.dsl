package org.eclipse.osbp.dsl.common.xtext.scoping;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;
import org.eclipse.osbp.xtext.oxtype.scoping.AbstractScopingInfoProvider;
import org.eclipse.xtext.xtype.XImportSection;

public class ScopingInfoProvider extends AbstractScopingInfoProvider {

	@Override
	public String getPackageName(EObject context) {
		if (context instanceof LPackage) {
			return ((LPackage) context).getName();
		}

		if (context.eContainer() == null) {
			return "";
		}

		return getPackageName(context.eContainer());
	}

}
