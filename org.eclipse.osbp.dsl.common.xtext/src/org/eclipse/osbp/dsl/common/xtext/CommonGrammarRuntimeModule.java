/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext;

import org.eclipse.xtext.findReferences.TargetURICollector;
import org.eclipse.xtext.linking.ILinkingService;
import org.eclipse.xtext.resource.persistence.IResourceStorageFacade;
import org.eclipse.xtext.xbase.jvmmodel.JvmModelTargetURICollector;
import org.eclipse.xtext.xbase.resource.BatchLinkableResourceStorageFacade;
import org.eclipse.osbp.xtext.oxtype.linking.JvmTypeEnhancingLinkingService;
import org.eclipse.osbp.xtext.oxtype.resource.SemanticLoadingResource;

/**
 * Use this class to register components to be used at runtime / without the
 * Equinox extension registry.
 */
public class CommonGrammarRuntimeModule extends
		org.eclipse.osbp.dsl.common.xtext.AbstractCommonGrammarRuntimeModule {

	public Class<? extends IResourceStorageFacade> bindResourceStorageFacade() {
		return BatchLinkableResourceStorageFacade.class;
	}
	
	public Class<? extends TargetURICollector> bindTargetURICollector() {
		return JvmModelTargetURICollector.class;
	}
	
	public Class<? extends org.eclipse.xtext.resource.XtextResource> bindXtextResource() {
		return SemanticLoadingResource.class;
	}
	
	public Class<? extends ILinkingService> bindILinkingService() {
		return JvmTypeEnhancingLinkingService.class;
	}

}
