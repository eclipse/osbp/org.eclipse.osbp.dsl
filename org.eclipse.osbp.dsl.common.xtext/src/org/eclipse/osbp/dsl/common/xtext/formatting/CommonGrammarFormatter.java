/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext.formatting;

import org.eclipse.osbp.dsl.common.xtext.services.CommonGrammarGrammarAccess;
import org.eclipse.osbp.xtext.oxtype.formatting.OXtypeFormatter;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter;
import org.eclipse.xtext.formatting.impl.FormattingConfig;

import com.google.inject.Inject;

// TODO: Auto-generated Javadoc
/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation/latest/xtext.html#formatting
 * on how and when to use it
 * 
 * Also see org.eclipse.xtext.xtext.XtextFormattingTokenSerializer as an example
 */
@SuppressWarnings({ "deprecation", "restriction" })
public class CommonGrammarFormatter extends AbstractDeclarativeFormatter {

	/** The xbase formatter. */

	@Inject
	private OXtypeFormatter oxTypeFormatter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter#
	 * configureFormatting(org.eclipse.xtext.formatting.impl.FormattingConfig)
	 */
	@Override
	protected void configureFormatting(FormattingConfig c) {
		configure(c, (CommonGrammarGrammarAccess) getGrammarAccess());
	}

	/**
	 * Configure.
	 *
	 * @param c
	 *            the c
	 * @param ga
	 *            the ga
	 */
	public void configure(FormattingConfig c, CommonGrammarGrammarAccess ga) {
		oxTypeFormatter.configure(c, ga.getOXtypeGrammarAccess());

		CommonGrammarGrammarAccess f = (CommonGrammarGrammarAccess) ga;
		c.setAutoLinewrap(120);

		configurePackageDef(c, f.getTypedPackageAccess());
		configureDataTypeDef(c, f.getDataTypeAccess());
		configureAnnotationDef(c, f.getAnnotationDefAccess());
		configureConstraints(c, f.getResultFiltersAccess());
		configureAttributeMatchingConstraint(c, f.getAttributeMatchingConstraintAccess());

		c.setLinewrap(0, 1, 2).before(f.getSL_COMMENTRule());
		c.setLinewrap(0, 1, 2).after(f.getSL_COMMENTRule());
		c.setLinewrap(0, 1, 2).before(f.getML_COMMENTRule());
		c.setLinewrap(1, 1, 1).after(f.getML_COMMENTRule());

		for (Keyword kw : ga.findKeywords("@")) {
			c.setNoSpace().after(kw);
		}
		for (Keyword kw : ga.findKeywords("(")) {
			c.setNoSpace().after(kw);
		}
		for (Keyword kw : ga.findKeywords("(")) {
			c.setNoSpace().before(kw);
		}
		for (Keyword kw : ga.findKeywords(")")) {
			c.setNoSpace().before(kw);
		}
		for (Keyword kw : ga.findKeywords(";")) {
			c.setNoSpace().before(kw);
		}
		for (Keyword kw : ga.findKeywords(",")) {
			c.setNoSpace().before(kw);
		}
	}

	/**
	 * Configure package def.
	 *
	 * @param c
	 *            the c
	 * @param ele
	 *            the ele
	 */
	protected void configurePackageDef(FormattingConfig c, CommonGrammarGrammarAccess.TypedPackageElements ele) {

		c.setLinewrap(1, 1, 2).around(ele.getRule());
		c.setLinewrap(1, 1, 2).after(ele.getLeftCurlyBracketKeyword_3_0());
		c.setIndentationIncrement().after(ele.getLeftCurlyBracketKeyword_3_0());
		c.setIndentationDecrement().before(ele.getRightCurlyBracketKeyword_3_2());
	}

	/**
	 * Configure data type def.
	 *
	 * @param c
	 *            the c
	 * @param ele
	 *            the ele
	 */
	protected void configureDataTypeDef(FormattingConfig c, CommonGrammarGrammarAccess.DataTypeElements ele) {
		c.setLinewrap().before(ele.getRule());
	}

	/**
	 * Configure annotation def.
	 *
	 * @param c
	 *            the c
	 * @param ele
	 *            the ele
	 */
	protected void configureAnnotationDef(FormattingConfig c, CommonGrammarGrammarAccess.AnnotationDefElements ele) {
		c.setLinewrap().around(ele.getRule());
	}

	/**
	 * Configure constraints.
	 *
	 * @param c
	 *            the c
	 * @param ele
	 *            the ele
	 */
	protected void configureConstraints(FormattingConfig c, CommonGrammarGrammarAccess.ResultFiltersElements ele) {

		c.setLinewrap().after(ele.getLeftCurlyBracketKeyword_2());
		c.setLinewrap().before(ele.getRightCurlyBracketKeyword_4());
		c.setIndentationIncrement().after(ele.getLeftCurlyBracketKeyword_2());
		c.setIndentationDecrement().before(ele.getRightCurlyBracketKeyword_4());
	}

	/**
	 * Configure attribute matching constraint.
	 *
	 * @param c
	 *            the c
	 * @param ele
	 *            the ele
	 */
	protected void configureAttributeMatchingConstraint(FormattingConfig c,
			CommonGrammarGrammarAccess.AttributeMatchingConstraintElements ele) {
	}
}
