/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.dsl.common.xtext.extensions

import com.google.inject.Inject
import java.beans.Transient
import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationDef
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute
import org.eclipse.osbp.dsl.semantic.common.types.LFeature
import org.eclipse.osbp.runtime.common.annotations.HistValidFrom
import org.eclipse.xtext.common.types.JvmAnnotationReference
import org.eclipse.xtext.common.types.JvmAnnotationTarget
import org.eclipse.xtext.common.types.JvmAnnotationType
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.common.types.JvmEnumerationLiteral
import org.eclipse.xtext.common.types.JvmOperation
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.common.types.TypesFactory
import org.eclipse.xtext.common.types.util.TypeReferences
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder
import org.eclipse.osbp.runtime.common.annotations.HistValidUntil
import org.eclipse.osbp.runtime.common.annotations.HistIsCurrent
import org.eclipse.osbp.runtime.common.annotations.HistIsCustomVersion

@SuppressWarnings("restriction")
class AnnotationExtension {

	@Inject extension JvmTypesBuilder
	@Inject
	private TypesFactory typesFactory;
	@Inject
	private TypeReferences references;

	/**
	 * Creates an enum annotation value and adds it the the given annotation reference
	 */
	def dispatch addAnnAttr(JvmAnnotationReference annRef, EObject context, String name, Enum<?>... enums) {

		// create the parameter
		val value = typesFactory.createJvmEnumAnnotationValue
		annRef.explicitValues += value

		// create the enum type
		val declaredType = references.findDeclaredType(enums.get(0).declaringClass, context) as JvmDeclaredType
		for (Enum<?> enumxx : enums) {

			// create the operation
			val JvmOperation op = typesFactory.createJvmOperation
			op.setSimpleName(name)
			op.setDeclaringType(declaredType)
			value.setOperation(op)

			// create the literal
			val JvmEnumerationLiteral literal = typesFactory.createJvmEnumerationLiteral
			literal.setDeclaringType(declaredType)
			literal.setSimpleName(enumxx.name)
			value.values += literal
		}

		return value
	}

	/**
	 * Creates an enum annotation value and adds it the the given type reference
	 */
	def dispatch addAnnAttr(JvmAnnotationReference annRef, EObject context, String name,
		JvmTypeReference typeReference) {

		// create the parameter
		val value = typesFactory.createJvmTypeAnnotationValue
		annRef.explicitValues += value

		// create the enum type
		val declaredType = references.findDeclaredType(typeof(JvmTypeReference), context) as JvmDeclaredType

		// create the operation
		val JvmOperation op = typesFactory.createJvmOperation
		op.setSimpleName(name)
		op.setDeclaringType(declaredType)
		value.setOperation(op)

		value.values += typeReference

		return value
	}

	/**
	 * Creates an enum annotation value and adds it the the given annotation reference
	 */
	def dispatch addAnnAttr(JvmAnnotationReference annRef, EObject context, String name, Enum<?> enumX) {

		// create the parameter
		val value = typesFactory.createJvmEnumAnnotationValue
		annRef.explicitValues += value

		// create the enum type
		val declaredType = references.findDeclaredType(enumX.declaringClass, context) as JvmDeclaredType

		// create the operation
		val JvmOperation op = typesFactory.createJvmOperation
		op.setSimpleName(name)
		op.setDeclaringType(declaredType)
		value.setOperation(op)

		// create the literal
		val JvmEnumerationLiteral literal = typesFactory.createJvmEnumerationLiteral
		literal.setDeclaringType(declaredType)
		literal.setSimpleName(enumX.name)
		value.values += literal

		return value
	}

	/**
	 * Creates a boolean annotation value and adds it the the given annotation reference
	 */
	def dispatch addAnnAttr(JvmAnnotationReference annRef, EObject context, String name, boolean booleanValue) {

		// create the parameter
		val value = typesFactory.createJvmBooleanAnnotationValue
		annRef.explicitValues += value

		// create the enum type
		val declaredType = references.findDeclaredType(typeof(Boolean), context) as JvmDeclaredType

		// create the operation
		val JvmOperation op = typesFactory.createJvmOperation
		op.setSimpleName(name)
		op.setDeclaringType(declaredType)
		value.setOperation(op)
		value.values += booleanValue

		return value
	}

	/**
	 * Creates a string annotation value and adds it the the given annotation reference
	 */
	def dispatch addAnnAttr(JvmAnnotationReference annRef, EObject context, String name, String stringValue) {

		// create the parameter
		val value = typesFactory.createJvmStringAnnotationValue
		annRef.explicitValues += value

		// create the enum type
		val declaredType = references.findDeclaredType(typeof(String), context) as JvmDeclaredType

		// create the operation
		val JvmOperation op = typesFactory.createJvmOperation
		op.setSimpleName(name)
		op.setDeclaringType(declaredType)
		value.setOperation(op)
		value.values += stringValue

		return value
	}

	def addAnno(LAnnotationTarget target, JvmAnnotationTarget jvmType, JvmAnnotationReference anno) {
		val annoDef = target.annotations.findFirst[annotation.annotationType == anno.annotation]
		if (annoDef === null || !annoDef.exclude) {
			jvmType.annotations += anno
		}
	}

	/**
	 * Creates a string annotation value and adds it the the given annotation reference
	 */
	def dispatch addAnnAttr(JvmAnnotationReference annRef, EObject context, String name, int intValue) {

		// create the parameter
		val value = typesFactory.createJvmIntAnnotationValue
		annRef.explicitValues += value

		// create the enum type
		val declaredType = references.findDeclaredType(typeof(Integer), context) as JvmDeclaredType

		// create the operation
		val JvmOperation op = typesFactory.createJvmOperation
		op.setSimpleName(name)
		op.setDeclaringType(declaredType)
		value.setOperation(op)
		value.values += intValue

		return value
	}

	/**
	 * Creates a string annotation value and adds it the the given annotation reference
	 */
	def dispatch addAnnAttr(JvmAnnotationReference annRef, EObject context, String name,
		JvmAnnotationReference annotationValue) {

		// create the parameter
		val value = typesFactory.createJvmAnnotationAnnotationValue
		annRef.explicitValues += value

		// create the enum type
		val declaredType = references.findDeclaredType(typeof(JvmAnnotationReference), context) as JvmDeclaredType

		// create the operation
		val JvmOperation op = typesFactory.createJvmOperation
		op.setSimpleName(name)
		op.setDeclaringType(declaredType)
		value.setOperation(op)
		value.values += annotationValue

		return value
	}

	/**
	 * Creates a string annotation value and adds it the the given string annotation values
	 */
	def dispatch addAnnAttr(JvmAnnotationReference annRef, EObject context, String name, List<String> stringValues) {

		// create the parameter
		val value = typesFactory.createJvmStringAnnotationValue
		annRef.explicitValues += value

		// create the enum type
		val declaredType = references.findDeclaredType(typeof(JvmAnnotationReference), context) as JvmDeclaredType

		// create the operation
		val JvmOperation op = typesFactory.createJvmOperation
		op.setSimpleName(name)
		op.setDeclaringType(declaredType)
		value.setOperation(op)
		for (stringValue : stringValues) {
			value.values += stringValue
		}

		return value
	}

	/**
	 * Creates a string annotation value and adds it the the given annotation reference
	 */
	def dispatch addAnnAttr(JvmAnnotationReference annRef, EObject context, String name,
		JvmAnnotationReference... annotationValues) {

		// create the parameter
		val value = typesFactory.createJvmAnnotationAnnotationValue
		annRef.explicitValues += value

		// create the enum type
		val declaredType = references.findDeclaredType(typeof(JvmAnnotationReference), context) as JvmDeclaredType

		// create the operation
		val JvmOperation op = typesFactory.createJvmOperation
		op.setSimpleName(name)
		op.setDeclaringType(declaredType)
		value.setOperation(op)
		value.values.addAll(annotationValues)

		return value
	}

	/**
	 * Returns a map with all excluded types
	 */
	def Map<String, LAnnotationDef> excludedTypes(List<LAnnotationDef> defs) {
		return defs.filter([exclude]).toMap(
			[
			val anno = it.annotation
			if (anno.annotationType != null) {
				return anno.annotationType.qualifiedName
			}
			return ""
		])
	}

	/**
	 * Returns a map with all included types
	 */
	def Map<String, LAnnotationDef> redefinedTypes(List<LAnnotationDef> defs) {
		return defs.filter([!exclude]).toMap(
			[
			val anno = it.annotation
			if (anno.annotationType != null) {
				return anno.annotationType.qualifiedName
			}
			return ""
		])
	}

	/**
	 * Returns true, if the clazz.canonicalName exists in the excluded types of defs
	 */
	def isExcluded(Class<?> clazz, List<LAnnotationDef> defs) {
		if (defs == null) {
			return false;
		}
		return defs.excludedTypes.containsKey(clazz.canonicalName)
	}

	/**
	 * Returns true, if the clazz.canonicalName exists in the included types of defs
	 */
	def isRedefined(Class<?> clazz, List<LAnnotationDef> defs) {
		if (defs == null) {
			return false;
		}
		return clazz.getRedefined(defs) != null
	}

	/**
	 * Returns true, if the clazz.canonicalName exists in the included types of defs
	 */
	def getRedefined(Class<?> clazz, List<LAnnotationDef> defs) {
		if (defs == null) {
			return null;
		}
		return defs.redefinedTypes.get(clazz.canonicalName)
	}

	def boolean isTransientAnnoExcluded(LFeature member) {
		return typeof(Transient).isExcluded(member.annotations)
	}

	def boolean isTransientAnnoRedefined(LFeature member) {
		return typeof(Transient).isRedefined(member.annotations)
	}

	def boolean isTransientAnnoCreated(JvmAnnotationTarget target, EObject context) {
		return target.containsAnnotation(typeof(Transient), context)
	}

	def LAnnotationDef getTransientAnnoRedefine(LFeature member) {
		return typeof(Transient).getRedefined(member.annotations)
	}

	def boolean isHistorizedValidFromAnnotation(LAttribute member) {
		val result = member.annotationInfo.annotations.findFirst [
			val type = it.annotation.annotationType
			val ref = references.findDeclaredType(typeof(HistValidFrom), member)
			return type === ref
		]
		return result !== null
	}
	
	def boolean isHistorizedValidUntilAnnotation(LAttribute member) {
		val result = member.annotationInfo.annotations.findFirst [
			val type = it.annotation.annotationType
			val ref = references.findDeclaredType(typeof(HistValidUntil), member)
			return type === ref
		]
		return result !== null
	}
	
	def boolean isHistorizedIsCurrentAnnotation(LAttribute member) {
		val result = member.annotationInfo.annotations.findFirst [
			val type = it.annotation.annotationType
			val ref = references.findDeclaredType(typeof(HistIsCurrent), member)
			return type === ref
		]
		return result !== null
	}
	
		def boolean isHistorizedIsCustomVersionAnnotation(LAttribute member) {
		val result = member.annotationInfo.annotations.findFirst [
			val type = it.annotation.annotationType
			val ref = references.findDeclaredType(typeof(HistIsCustomVersion), member)
			return type === ref
		]
		return result !== null
	}

	def boolean containsAnnotation(JvmAnnotationTarget target, Class<?> type, EObject context) {
		for (anno : target.annotations) {
			var JvmAnnotationType resolved
			val JvmAnnotationType xan = anno.annotation;
			if (xan !== null) {
				if (xan.eIsProxy) {
					resolved = EcoreUtil::resolve(xan, context) as JvmAnnotationType
				} else {
					resolved = xan
				}
				if (resolved.qualifiedName !== null) {
					if (resolved.qualifiedName.equals(type.canonicalName)) {
						return true;
					}
				}
			}
		}
	}
}
