/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext.scoping;

import java.util.ArrayList;

import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LAttributeMatchingConstraint;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LEnumLiteral;
import org.eclipse.osbp.dsl.semantic.common.types.LType;

public class ConstraintEnumMatchingLiteralScope extends AbstractScope {
	private final LAttributeMatchingConstraint constraint;

	public ConstraintEnumMatchingLiteralScope(
			LAttributeMatchingConstraint constraint) {
		super(IScope.NULLSCOPE, true);
		this.constraint = constraint;
	}

	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {

		ArrayList<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		LAttribute att = constraint.getAttribute();
		if (att != null) {
			LType type = att.getType();
			if (type instanceof LEnum) {
				LEnum enumx = (LEnum) type;

				for (LEnumLiteral literal : enumx.getLiterals()) {
					result.add(new EObjectDescription(QualifiedName
							.create(literal.getName()), literal, null));
				}
			}
		}
		return result;
	}
}