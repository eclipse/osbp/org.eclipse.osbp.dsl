/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext.extensions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.common.types.JvmParameterizedTypeReference;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.xbase.compiler.ImportManager;
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable;

// TODO: Auto-generated Javadoc
/**
 * This is an extension library for {@link ITreeAppendable}s. It provides some
 * overloaded operators especially for handling automatic type imports.
 *
 * @author Robert Handschmann
 * @see ITreeAppendable
 * @see ImportManager
 */
@SuppressWarnings("restriction")
public class TreeAppendableExtensions {

	/**
	 * The operator {@code +=} appends the specified string.
	 *
	 * @param p
	 *            the p
	 * @param s
	 *            the s
	 * @return the i tree appendable
	 */
	public ITreeAppendable operator_add(ITreeAppendable p, CharSequence s) {
		if (s == null){
			return p;
		}
		
		return p.append(s);
	}
	
	/**
	 * The operator {@code >>} appends the specified string.
	 *
	 * @param p
	 *            the p
	 * @param s
	 *            the s
	 * @return the i tree appendable
	 */
	public ITreeAppendable operator_doubleGreaterThan(ITreeAppendable p, CharSequence s) {
		if (s == null){
			return p;
		}
		return p.append(s);
	}
	
	/**
	 * The operator {@code >>>} is used to begin a code block. It is usually
	 * called with a "{". It appends the specified string then increases the
	 * indent and adds a new line.
	 *
	 * @param p
	 *            the p
	 * @param s
	 *            the s
	 * @return the i tree appendable
	 */
	public ITreeAppendable operator_tripleGreaterThan(ITreeAppendable p, CharSequence s) {
		if (s == null){
			return p;
		}
		
		p.append(s);
		p.increaseIndentation().newLine(); 
		return p;
	}
	
	/**
	 * The operator {@code <<<} is used to end a code block. It is usually
	 * called with a "}". It decreases the indent, adds a new line, then appends
	 * the specified string and adds a final new line.
	 *
	 * @param p
	 *            the p
	 * @param s
	 *            the s
	 * @return the i tree appendable
	 */
	public ITreeAppendable operator_tripleLessThan(ITreeAppendable p, CharSequence s) {
		if (s == null) {
			return p;
		}
		p.decreaseIndentation().newLine();
		p.append(s).newLine();
		return p;
	}

	/**
	 * The operator {@code +=} appends the specified {@link JvmType} and thereby
	 * imports it.
	 *
	 * @param p
	 *            the p
	 * @param jvmType
	 *            the jvm type
	 * @return the i tree appendable
	 */
	public ITreeAppendable operator_add(ITreeAppendable p, JvmType jvmType) {
		return p.append(jvmType);
	}
	
	/**
	 * The operator {@code >>} appends the specified {@link JvmType} and thereby
	 * imports it.
	 *
	 * @param p
	 *            the p
	 * @param jvmType
	 *            the jvm type
	 * @return the i tree appendable
	 */
	public ITreeAppendable operator_doubleGreaterThan(ITreeAppendable p, JvmType jvmType) {
		return p.append(jvmType);
	}

	/**
	 * The operator {@code +=} appends the specified {@link JvmTypeReference}
	 * and thereby imports it. A {@link JvmParameterizedTypeReference} is
	 * handled as well, i.e. appends "Map&lt;Foo, Bar&gt;" and imports
	 * "java.util.Map" and both "Foo" and "Bar".
	 *
	 * @param p
	 *            the p
	 * @param jvmTypeRef
	 *            the jvm type ref
	 * @return the i tree appendable
	 */
	public ITreeAppendable operator_add(ITreeAppendable p, JvmTypeReference jvmTypeRef) {
		p.append(jvmTypeRef.getType());
		if (jvmTypeRef instanceof JvmParameterizedTypeReference) {
			JvmParameterizedTypeReference ptf = (JvmParameterizedTypeReference) jvmTypeRef;
			EList<JvmTypeReference> params = ptf.getArguments();
			if (!params.isEmpty()) {
				p.append("<");
				String sep = "";
				for (JvmTypeReference param: params) {
					p.append(sep);
					p.append(param.getType());
					sep = ", ";
				}
				p.append(">");
			}
		}
		return p;
	}
	
	/**
	 * Operator_double greater than.
	 *
	 * @param p
	 *            the p
	 * @param jvmTypeRef
	 *            the jvm type ref
	 * @return the i tree appendable
	 */
	public ITreeAppendable operator_doubleGreaterThan(ITreeAppendable p, JvmTypeReference jvmTypeRef) {
		return operator_add(p, jvmTypeRef);
	}


	
}
