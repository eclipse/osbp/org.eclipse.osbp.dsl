/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.common.xtext.valueconverter;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractValueConverter;
import org.eclipse.xtext.conversion.impl.IDValueConverter;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.xbase.conversion.XbaseValueConverterService;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@SuppressWarnings("restriction")
@Singleton
public class CommonValueConverterService extends XbaseValueConverterService {

	@Inject
	private LDecimalValueConverter lDecimalConverter;

	@Inject
	private LIntegerValueConverter lIntConverter;

	@Inject
	private TypeCrossReferenceValueConverter crossRefConverter;

	@Inject
	private NonEscapingIDValueConverter validIDConverter;

	@ValueConverter(rule = "QualifiedNameWithWildCard")
	public IValueConverter<String> getQualifiedNameWithWildCard() {
		return getQualifiedNameValueConverter();
	}

	@ValueConverter(rule = "LDecimal")
	public IValueConverter<Float> getLDecimalConverter() {
		return lDecimalConverter;
	}

	@ValueConverter(rule = "LInt")
	public IValueConverter<Integer> getLIntConverter() {
		return lIntConverter;
	}

	@ValueConverter(rule = "TYPE_CROSS_REFERENCE")
	public TypeCrossReferenceValueConverter getTypeCrossReferenceValueConverter() {
		return crossRefConverter;
	}

	@ValueConverter(rule = "TRANSLATABLEID")
	public IValueConverter<String> getValidIDConverter() {
		return validIDConverter;
	}

	@ValueConverter(rule = "ValidIDWithKeywords")
	public IValueConverter<String> getValidIDWithKeywords() {
		return validIDConverter;
	}

	public static class LDecimalValueConverter extends AbstractValueConverter<Float> {

		public Float toValue(String string, INode node) throws ValueConverterException {
			return Float.valueOf(string);
		}

		public String toString(Float value) throws ValueConverterException {
			return value.toString();
		}

	}

	public static class NonEscapingIDValueConverter extends IDValueConverter {
		Set<String> unescapedIDs = new HashSet<>();

		public NonEscapingIDValueConverter() {
			unescapedIDs.add("id");

		}

		@Override
		protected boolean mustEscape(String value) {
			return unescapedIDs.contains(value) ? false : super.mustEscape(value);
		}

	};

	public static class LIntegerValueConverter extends AbstractValueConverter<Integer> {

		public Integer toValue(String string, INode node) throws ValueConverterException {
			return Integer.valueOf(string);
		}

		public String toString(Integer value) throws ValueConverterException {
			return value.toString();
		}

	}

	/**
	 * Cuts the String from the last '.'
	 */
	public static class TypeCrossReferenceValueConverter extends AbstractValueConverter<String> {

		@Override
		public String toString(String value) {
			if (value.contains(".")) {
				value = value.substring(value.lastIndexOf(".") + 1, value.length());
			}

			return value;
		}

		@Override
		public String toValue(String string, INode node) throws ValueConverterException {
			return toString(string);
		}

	}
}
