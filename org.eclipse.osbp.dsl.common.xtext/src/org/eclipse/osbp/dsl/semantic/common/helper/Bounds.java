/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.semantic.common.helper;

import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LLowerBound;
import org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity;
import org.eclipse.osbp.dsl.semantic.common.types.LUpperBound;
 
// TODO: Auto-generated Javadoc
/**
 * Keeps lower and upper bound.
 */
public class Bounds {

	/** The lower. */
	private final LLowerBound lower;
	
	/** The upper. */
	private final LUpperBound upper;
	
	/**
	 * Creates the zero to one.
	 *
	 * @return the bounds
	 */
	public static Bounds createZeroToOne(){
		return new Bounds(LLowerBound.ZERO, LUpperBound.ONE);
	}
	
	/**
	 * Creates the for.
	 *
	 * @param prop
	 *            the prop
	 * @return the bounds
	 */
	public static Bounds createFor(LFeature prop) {
		if (prop == null) {
			return new Bounds(LLowerBound.ZERO, LUpperBound.ONE);
		}
 
		return createFor(prop.getMultiplicity());
	}

	public static Bounds createFor(LMultiplicity multiplicity) {
		LLowerBound lb = null;
		LUpperBound ub = null;

		if (multiplicity != null) {
			lb = multiplicity.getLower();
			ub = multiplicity.getUpper();
		}

		LLowerBound lower = LLowerBound.ZERO;
		LUpperBound upper = LUpperBound.ONE;

		
		if (lb != null) {
			if (ub != null && ub != LUpperBound.NULL) {
				// Lower and upper bound [x..y] where x = 0/1/?/+/* and y = 1/*
				switch (lb) {
				case ZERO: // [0..]
				case OPTIONAL: // [?..] -> same as [0..] -> should be warning
					lower = LLowerBound.ZERO;
					break;
				case ONE: // [1..]
				case ATLEASTONE: // [+..] -> same as [1..] -> should be warning
					lower = LLowerBound.ONE;
					break;
				case MANY: // [*..]
					lower = LLowerBound.MANY;
					// [*..*]
					// [*..1] -> error: upper must be greather than lower bound!
					break;
				case NULL:
					throw new IllegalArgumentException("unexpected NULL value");
				}
				upper = ub;
			} else {
				// No upper bound.
				switch (lb) {
				case ZERO: // [0] -> makes only sense with upper bound
				case OPTIONAL: // [?]
					lower = LLowerBound.ZERO;
					upper = LUpperBound.ONE;
					break;
				case ONE: // [1] -> implies notnull
					lower = LLowerBound.ONE;
					upper = LUpperBound.ONE;
					break;
				case ATLEASTONE: // [+]
					lower = LLowerBound.ONE;
					upper = LUpperBound.MANY;
					break;
				case MANY: // [*]
					lower = LLowerBound.ZERO;
					upper = LUpperBound.MANY;
					break;
				case NULL:
					lower = LLowerBound.ZERO;
					upper = LUpperBound.MANY;
					break;
				}
			}
		}

		return new Bounds(lower, upper);
	}

	/**
	 * Instantiates a new bounds.
	 *
	 * @param lower
	 *            the lower
	 * @param upper
	 *            the upper
	 */
	public Bounds(LLowerBound lower, LUpperBound upper) {
		super();
		this.lower = lower;
		this.upper = upper;
	}

	/**
	 * Gets the lower.
	 *
	 * @return the lower
	 */
	public LLowerBound getLower() {
		return lower;
	}

	/**
	 * Gets the upper.
	 *
	 * @return the upper
	 */
	public LUpperBound getUpper() {
		return upper;
	}

	/**
	 * Returns true if the upper bound is many.
	 *
	 * @return true, if is to many
	 */
	public boolean isToMany() {
		return upper == LUpperBound.MANY;
	}

	/**
	 * Returns true if the lower bound is one.
	 *
	 * @return true, if is required
	 */
	public boolean isRequired() {
		return lower == LLowerBound.ONE;
	}

	/**
	 * Returns true if the lower bound is zero.
	 *
	 * @return true, if is optional
	 */
	public boolean isOptional() {
		return lower == LLowerBound.ZERO;
	}
	
	public String toBoundsString() {
		StringBuilder result = new StringBuilder();
		switch (lower) {
			case NULL: 
				result.append("0");
			break;
			case MANY: 
				result.append("1");
				break;
			case OPTIONAL: 
				result.append("0");
				break;
			case ATLEASTONE: 
				result.append("1");
				break;
			case ZERO: 
				result.append("0");
				break;
			case ONE:
				result.append("1");
				break;
			default: {
				result.append("undefined");
				break;
			}
		}
		result.append("..");
		switch (upper) {
			case NULL: 
				result.append("0");
				break;
			case MANY: 
				result.append("*");
				break;
			case ONE: 
				result.append("1");
				break;
			default: 
				result.append("undefined");
				break;
			}
		return result.toString();
	
	}
}
