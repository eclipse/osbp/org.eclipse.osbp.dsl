/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.ui.builder;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class PersistenceXMLSerializer {
  public static class PersistenceXML {
    private Map<String, PersistenceXMLSerializer.PersistenceUnit> pus = CollectionLiterals.<String, PersistenceXMLSerializer.PersistenceUnit>newHashMap();
    
    private List<LBean> beans = CollectionLiterals.<LBean>newArrayList();
    
    private List<LEnum> enums = CollectionLiterals.<LEnum>newArrayList();
    
    public boolean shouldGenerate() {
      boolean _isEmpty = this.pus.isEmpty();
      return (!_isEmpty);
    }
    
    public PersistenceXMLSerializer.PersistenceUnit getPU(final String puIn) {
      String name = puIn;
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(name);
      if (_isNullOrEmpty) {
        name = "DEFAULT";
      }
      boolean _containsKey = this.pus.containsKey(name);
      boolean _not = (!_containsKey);
      if (_not) {
        PersistenceXMLSerializer.PersistenceUnit _persistenceUnit = new PersistenceXMLSerializer.PersistenceUnit(name);
        this.pus.put(name, _persistenceUnit);
      }
      return this.pus.get(name);
    }
    
    public void add(final LBean bean) {
      this.beans.add(bean);
    }
    
    public void add(final LEnum enumx) {
      this.enums.add(enumx);
    }
    
    /**
     * Adds the beans and enums to all persistence units.
     */
    public void finish() {
      Collection<PersistenceXMLSerializer.PersistenceUnit> _values = this.pus.values();
      for (final PersistenceXMLSerializer.PersistenceUnit pu : _values) {
        {
          pu.beans.addAll(this.beans);
          pu.enums.addAll(this.enums);
        }
      }
    }
  }
  
  public static class PersistenceUnit {
    private final String name;
    
    private List<LEntity> entities = CollectionLiterals.<LEntity>newArrayList();
    
    private List<LBean> beans = CollectionLiterals.<LBean>newArrayList();
    
    private List<LEnum> enums = CollectionLiterals.<LEnum>newArrayList();
    
    public PersistenceUnit(final String name) {
      this.name = name;
    }
    
    public void add(final LEntity entity) {
      this.entities.add(entity);
    }
  }
  
  public void doGenerate(final PersistenceXMLSerializer.PersistenceXML xml, final IFileSystemAccess fsa) {
    fsa.generateFile("persistence.xml", this.doSerialize(xml));
  }
  
  public String doSerialize(final PersistenceXMLSerializer.PersistenceXML xml) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<persistence xmlns=\"http://java.sun.com/xml/ns/persistence\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("xsi:schemaLocation=\"http://java.sun.com/xml/ns/persistence persistence_1_0.xsd\"");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("version=\"1.0\">");
    _builder.newLine();
    {
      Collection<PersistenceXMLSerializer.PersistenceUnit> _values = xml.pus.values();
      for(final PersistenceXMLSerializer.PersistenceUnit pu : _values) {
        _builder.append("\t");
        _builder.append("<persistence-unit name=\"");
        _builder.append(pu.name, "\t");
        _builder.append("\" ");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("transaction-type=\"RESOURCE_LOCAL\">");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<provider>org.eclipse.persistence.jpa.PersistenceProvider</provider>");
        _builder.newLine();
        {
          for(final LEntity entity : pu.entities) {
            _builder.append("\t");
            _builder.append("\t");
            _builder.append("<class>");
            String _qualifiedName = this.toQualifiedName(entity);
            _builder.append(_qualifiedName, "\t\t");
            _builder.append("</class>");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<exclude-unlisted-classes>true</exclude-unlisted-classes>");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("</persistence-unit>");
        _builder.newLine();
      }
    }
    _builder.append("</persistence>");
    _builder.newLine();
    return _builder.toString();
  }
  
  public String toQualifiedName(final LEntity type) {
    EObject _eContainer = type.eContainer();
    final LTypedPackage pkg = ((LTypedPackage) _eContainer);
    String _name = pkg.getName();
    String _plus = (_name + ".");
    String _name_1 = type.getName();
    return (_plus + _name_1);
  }
}
