/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
 
package org.eclipse.osbp.dsl.entity.xtext.ui.builder

import java.util.List
import java.util.Map
import org.eclipse.osbp.dsl.semantic.common.types.LEnum
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage
import org.eclipse.osbp.dsl.semantic.entity.LBean
import org.eclipse.osbp.dsl.semantic.entity.LEntity
import org.eclipse.xtext.generator.IFileSystemAccess

class PersistenceXMLSerializer {

	def void doGenerate(PersistenceXML xml, IFileSystemAccess fsa) {
		fsa.generateFile("persistence.xml", xml.doSerialize)
	}

	def String doSerialize(PersistenceXML xml) '''
		<persistence xmlns="http://java.sun.com/xml/ns/persistence"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://java.sun.com/xml/ns/persistence persistence_1_0.xsd"
			version="1.0">
			«FOR pu : xml.pus.values»
				<persistence-unit name="«pu.name»" 
					transaction-type="RESOURCE_LOCAL">
					<provider>org.eclipse.persistence.jpa.PersistenceProvider</provider>
					«FOR entity : pu.entities»
						<class>«entity.toQualifiedName»</class>
					«ENDFOR»
«««					«FOR bean : pu.beans»
«««						<class>«bean.toQualifiedName»</class>
«««					«ENDFOR»
«««					«FOR enumx : pu.enums»
«««						<class>«enumx.toQualifiedName»</class>
«««					«ENDFOR»
				
					<exclude-unlisted-classes>true</exclude-unlisted-classes>
				</persistence-unit>
			«ENDFOR»
		</persistence>
	'''

	def toQualifiedName(LEntity type) {
		val LTypedPackage pkg = type.eContainer as LTypedPackage
		return pkg.getName + "." + type.getName
	}

	public static class PersistenceXML {
		Map<String, PersistenceUnit> pus = newHashMap

		// cache them temporary
		List<LBean> beans = newArrayList();
		List<LEnum> enums = newArrayList();

		def boolean shouldGenerate(){
			return !pus.empty
		}

		def PersistenceUnit getPU(String puIn) {
			
			var name = puIn
			if(name.nullOrEmpty) {
				name = "DEFAULT"
			}
			
			if (!pus.containsKey(name)) {
				pus.put(name, new PersistenceUnit(name));
			}
			return pus.get(name);
		}

		def void add(LBean bean) {
			beans.add(bean)
		}

		def void add(LEnum enumx) {
			enums.add(enumx)
		}

		/**
		 * Adds the beans and enums to all persistence units.
		 */
		def void finish() {

			//			val configs = ProductConfiguration.prefs.persistenceUnits.toMap[r|r.name]
			for (PersistenceUnit pu : pus.values()) {
				pu.beans.addAll(beans);
				pu.enums.addAll(enums);

			// TODO - maybe used later. For now there is no need
			//				val config = configs.get(pu.name)
			//				pu.addProp("jndiName", config.jndiName)
			//
			//				if (!config.batchWriting.isNullOrEmpty) {
			//					pu.addProp("batchWriting", config.batchWriting)
			//					pu.addProp("batchWritingSize", String.valueOf(config.batchWritingSize))
			//				}
			//				pu.addProp("cacheStatements", String.valueOf(config.cacheStatements))
			//				pu.addProp("cacheStatementsSize", String.valueOf(config.cacheStatementsSize))
			//				if (!config.ddlGeneration.isNullOrEmpty) {
			//					pu.addProp("ddlGeneration", config.ddlGeneration)
			//				}
			//				pu.addProp("deployOnStartup", String.valueOf(config.deployOnStartup))
			//				pu.addProp("persistenceXMLPath", config.persistenceXMLPath)
			//				pu.addProp("queryCache", String.valueOf(config.queryCache))
			//				pu.addProp("schemaName", config.schemaName)
			//				pu.addProp("weaving", config.weaving)
			}
		}
	}

	public static class PersistenceUnit {
		final String name;
		List<LEntity> entities = newArrayList();
		List<LBean> beans = newArrayList();
		List<LEnum> enums = newArrayList();

		//		List<Pair<String, String>> props = newArrayList()
		new(String name) {
			this.name = name;
		}

		def void add(LEntity entity) {
			entities.add(entity)
		}

	//		def addProp(String key, String value) {
	//			props += new Pair(key, value)
	//		}
	}
}
