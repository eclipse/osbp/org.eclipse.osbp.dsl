/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.ui.quickfix;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.osbp.dsl.common.xtext.ui.quickfix.CommonGrammarQuickfixProvider;
import org.eclipse.osbp.dsl.entity.xtext.validation.EntityGrammarValidator;
import org.eclipse.xtext.diagnostics.Diagnostic;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.model.edit.IModification;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.xtext.xbase.annotations.ui.quickfix.XbaseWithAnnotationsQuickfixProvider;

@SuppressWarnings("restriction")
public class EntityGrammarQuickfixProvider extends
		CommonGrammarQuickfixProvider {

	@Fix(EntityGrammarValidator.CODE__CASCADE_DIRECTION_INVALID)
	public void capitalizeName(final Issue issue,
			IssueResolutionAcceptor acceptor) {
		acceptor.accept(issue, "Remove cascade", "Removes the cascade keyword",
				null, new IModification() {
					public void apply(IModificationContext context)
							throws BadLocationException {
						IXtextDocument xtextDocument = context
								.getXtextDocument();
						xtextDocument.replace(issue.getOffset(),
								"cascade ".length(), "");
					}
				});
	}

}
