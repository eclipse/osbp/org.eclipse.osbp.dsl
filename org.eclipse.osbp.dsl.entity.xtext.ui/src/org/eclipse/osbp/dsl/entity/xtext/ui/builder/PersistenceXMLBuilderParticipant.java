/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.ui.builder;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess;
import org.eclipse.xtext.builder.IXtextBuilderParticipant;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.osbp.dsl.entity.xtext.ui.builder.PersistenceXMLSerializer.PersistenceUnit;
import org.eclipse.osbp.dsl.entity.xtext.ui.builder.PersistenceXMLSerializer.PersistenceXML;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityModel;
import org.eclipse.osbp.xtext.oxtype.resource.ISemanticLoadingResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

@SuppressWarnings("deprecation")
public class PersistenceXMLBuilderParticipant implements
		IXtextBuilderParticipant {

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PersistenceXMLBuilderParticipant.class);

	@Inject
	private Provider<EclipseResourceFileSystemAccess> fileAccessProvider;

	public void build(IBuildContext context, IProgressMonitor monitor)
			throws CoreException {
		final IProject builtProject = context.getBuiltProject();
		IJavaProject javaProject = JavaCore.create(builtProject);
		if (!javaProject.exists()) {
			return;
		}

		// if persistence.xml exists, then check if any entity resource has
		// changed
		if (getPersistenceXML(builtProject).exists()) {
			boolean changedResource = context
					.getDeltas()
					.stream()
					.anyMatch(
							d -> {
								return d.haveEObjectDescriptionsChanged()
										&& d.getUri().fileExtension()
												.equals("entity");
							});
			if (!changedResource) {
				return;
			}
		}

		// delete the persistence.xml
		getPersistenceXML(builtProject).delete(true, null);

		// create the xml model
		PersistenceXML generatorModel = buildPersistenceXML(
				context.getBuiltProject(), context.getResourceSet());

		if (generatorModel.shouldGenerate()) {
			// serialize the stuff
			PersistenceXMLSerializer serializer = new PersistenceXMLSerializer();
			serializer
					.doGenerate(
							generatorModel,
							getConfiguredFileSystemAccess(getManifestFolder(builtProject)));
		}
	}

	protected IFolder getManifestFolder(final IProject builtProject) {
		return builtProject.getFolder("META-INF");
	}

	protected IFolder getPersistenceXML(final IProject builtProject) {
		return builtProject.getFolder("META-INF/persistence.xml");
	}

	protected IFileSystemAccess getConfiguredFileSystemAccess(
			IFolder srcGenFolder) {
		EclipseResourceFileSystemAccess access = fileAccessProvider.get();
		access.setOutputPath(srcGenFolder.getFullPath().toString());
		return access;
	}

	/**
	 * Builds the persistence xml model.
	 * 
	 * @param builtProject
	 * @param resourceSet
	 * @return
	 */
	private PersistenceXML buildPersistenceXML(IProject builtProject,
			ResourceSet resourceSet) {

		PersistenceXML persistence = new PersistenceXML();

		List<URI> uris = collectFiles(builtProject);
		for (URI uri : uris) {
			ISemanticLoadingResource resource = (ISemanticLoadingResource) resourceSet
					.getResource(uri, true);
			LEntityModel m = (LEntityModel) resource.getSemanticElement();
			if (m == null) {
				continue;
			}
			for (LTypedPackage pkg : m.getPackages()) {
				for (LType type : pkg.getTypes()) {
					if (type instanceof LEntity) {
						LEntity entity = (LEntity) type;
						PersistenceUnit pu = persistence.getPU(entity
								.getPersistenceUnit());
						pu.add(entity);
					} else if (type instanceof LBean) {
						persistence.add((LBean) type);
					} else if (type instanceof LEnum) {
						persistence.add((LEnum) type);
					}
				}
			}
		}

		// finish the data collection
		persistence.finish();

		return persistence;
	}

	/**
	 * Collects all "entity" URIs contained in the given project.
	 * 
	 * @param project
	 * @return
	 */
	protected List<URI> collectFiles(IProject project) {
		List<URI> uris = new ArrayList<>();
		try {
			project.accept(new IResourceVisitor() {
				public boolean visit(IResource curr) throws CoreException {
					if (curr instanceof IContainer) {
						String folder = curr.getProjectRelativePath()
								.toString();
						if (folder.equals("bin")
								|| folder.equals("target/classes")) {
							return false;
						}
						return true;
					} else if (curr instanceof IFile) {
						if ("entity".equals(curr.getProjectRelativePath()
								.getFileExtension())) {
							uris.add(URI.createPlatformResourceURI(curr
									.getFullPath().toString(), true));
							return false;
						}
					}
					return true;
				}
			}, IResource.DEPTH_INFINITE, false);
		} catch (CoreException e) {
			e.printStackTrace();
		}
		return uris;
	}

}
