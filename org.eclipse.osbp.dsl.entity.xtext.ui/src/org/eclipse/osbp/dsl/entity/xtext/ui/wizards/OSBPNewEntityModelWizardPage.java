/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.entity.xtext.ui.wizards;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osbp.dsl.entity.xtext.ui.internal.XtextActivator;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

// TODO: Auto-generated Javadoc
/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one (organization).
 */
public class OSBPNewEntityModelWizardPage extends WizardNewFileCreationPage {

	/**
	 * Constructor for SampleNewWizardPage.
	 *
	 * @param selection
	 *            the selection
	 */
	public OSBPNewEntityModelWizardPage(IStructuredSelection selection) {
		super("OSBP", selection);
		setTitle("Entity Model File");
		setFileName("new_entity_model");
		setFileExtension("entitymodel");
		setDescription("This wizard creates a new file with *.entityModel extension that can be opened by a multi-page editor.");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.dialogs.WizardNewFileCreationPage#getInitialContents()
	 */
	@Override
	protected InputStream getInitialContents() {
		try {
			return XtextActivator.getInstance().getBundle()
					.getEntry("/resources/initialmodel.entitymodel").openStream();
		} catch (IOException e) {
			return null; // ignore and create empty comments
		}
	}
}