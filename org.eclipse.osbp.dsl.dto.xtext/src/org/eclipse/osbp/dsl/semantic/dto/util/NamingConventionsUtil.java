/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.semantic.dto.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.osbp.dsl.semantic.dto.LDto;

/**
 * This util is used to convert between entity packages and dto packages.
 */
public class NamingConventionsUtil {

	/** The Constant toDtoPathReplacements. */
	private static final List<Pair> toDtoPathReplacements;
	static {
		toDtoPathReplacements = new ArrayList<Pair>();
		toDtoPathReplacements.add(new Pair(".entitymodel", ".dtos"));
		toDtoPathReplacements.add(new Pair("/entity/", "/dto/"));
		toDtoPathReplacements.add(new Pair("/entities/", "/dtos/"));
		toDtoPathReplacements.add(new Pair(".entities", ".dtos"));
		toDtoPathReplacements.add(new Pair(".entity", ".dto"));
	}

	/** The Constant toEntityPathReplacements. */
	private static final List<Pair> toEntityPathReplacements;
	static {
		toEntityPathReplacements = new ArrayList<Pair>();
		toEntityPathReplacements.add(new Pair(".dtos.", ".entities."));
		toEntityPathReplacements.add(new Pair(".dtos", ".entitymodel"));
		toEntityPathReplacements.add(new Pair("/dto/", "/entity/"));
		toEntityPathReplacements.add(new Pair("/dtos/", "/entities/"));
		toEntityPathReplacements.add(new Pair(".dto", ".entity"));
	}

	/**
	 * Replaces package fragments and returns the package name used for Auto-DTOs.
	 *
	 * @param packageName
	 *            the package name
	 * @return the string
	 */
	public static String toDtoPackage(String packageName) {
		if (packageName == null) {
			return "";
		}
		String result = packageName;
		for (Pair pair : toDtoPathReplacements) {
			result = pair.replace(result);
		}
		return result;
	}

	/**
	 * Replaces package fragments and returns the full qualified name used for Auto-Entity-DTOs including the 'Dto'-Prefix.
	 *
	 * @param qualifiedEntityName
	 *            the qualified entity name
	 * @return the string
	 */
	public static String toDtoQualifiedName(String qualifiedEntityName) {
		return toDtoName(toDtoQualifiedNameForEnum(qualifiedEntityName));
	}

	/**
	 * Replaces package fragments and returns the full qualified name used for Auto-Enum-DTOs without the 'Dto'-Prefix.
	 *
	 * @param qualifiedEnumName
	 *            the qualified enum name
	 * @return the string
	 */
	public static String toDtoQualifiedNameForEnum(String qualifiedEnumName) {
		if (qualifiedEnumName == null) {
			return "";
		}
		String result = qualifiedEnumName;
		for (Pair pair : toDtoPathReplacements) {
			result = pair.replace(result);
		}
		return result;
	}

	/**
	 * Replaces package fragments and returns the package name used for Auto-DTOs.
	 *
	 * @param lPackage
	 *            the l package
	 * @return the string
	 */
	public static String toDtoPackage(LTypedPackage lPackage) {
		return toDtoPackage(lPackage.getName());
	}

	/**
	 * Returns the name of the dto for the given type.
	 *
	 * @param lType
	 *            the l type
	 * @return the string
	 */
	public static String toDtoName(LType lType) {
		if(lType instanceof LEnum) {
			return lType.getName();
		} else if(lType instanceof LStateClass) {
			return lType.getName();
		}
		return toDtoName(lType.getName());
	}

	/**
	 * To first character lowercase.
	 *
	 * @param s the string to modify
	 * @return the string with first character lowercase
	 */
	public static String toFirstLower(String s) {
		if (s == null || s.length() == 0)
			return s;
		if (Character.isLowerCase(s.charAt(0)))
			return s;
		if (s.length() == 1)
			return s.toLowerCase();
		return s.substring(0, 1).toLowerCase() + s.substring(1);
	}
	
	/**
	 * To first character uppercase.
	 *
	 * @param s the string to modify
	 * @return the string with first character uppercase
	 */
	public static String toFirstUpper(String s) {
		if (s == null || s.length() == 0)
			return s;
		if (Character.isUpperCase(s.charAt(0)))
			return s;
		if (s.length() == 1)
			return s.toUpperCase();
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}
	
	/**
	 * Returns the name of the dto for the given type.
	 *
	 * @param prefix
	 *            the prefix
	 * @return the string
	 */
	public static String toDtoName(String prefix) {
		return prefix + "Dto";
	}

	/**
	 * Replaces package fragments of the Auto-DTOs and returns the package name used for the Entity.
	 *
	 * @param packageName
	 *            the package name
	 * @return the string
	 */
	public static String toEntityPackage(String packageName) {
		if (packageName == null) {
			return "";
		}
		String result = packageName;
		for (Pair pair : toEntityPathReplacements) {
			result = pair.replace(result);
		}
		return result;
	}

	/**
	 * Replaces package fragments of the Auto-DTOs and returns the package name used for the Entity.
	 *
	 * @param lPackage
	 *            the l package
	 * @return the string
	 */
	public static String toEntityPackage(LTypedPackage lPackage) {
		return toEntityPackage(lPackage.getName());
	}

	/**
	 * Returns the name of the entity for the given type.
	 *
	 * @param lType
	 *            the l type
	 * @return the string
	 */
	public static String toEntityName(LType lType) {
		return (lType instanceof LEnum) ? lType.getName() : toEntityName(lType.getName());
	}

	/**
	 * Returns the name of the entity for the given type.
	 *
	 * @param prefix
	 *            the prefix
	 * @return the string
	 */
	public static String toEntityName(String prefix) {
		int idx = prefix.lastIndexOf("Dto");
		if(idx == -1){
			return "NoDto";
		}
		return prefix.substring(0, idx);
	}

	/**
	 * Returns the name of the entity for the given LDto.
	 *
	 * @param prefix
	 *            the prefix
	 * @return the string
	 */
	public static String toEntityName(LDto dto) {
		String prefix = dto.getName();
		int idx = prefix.lastIndexOf("Dto");
		return prefix.substring(0, idx);
	}

	/**
	 * Returns the full qualified name of the entity for the given type.
	 *
	 * @param fqnDto
	 *            the fqn dto
	 * @return the string
	 */
	public static String toFqnEntityName(String fqnDto) {
		int idx = fqnDto.lastIndexOf(".");
		String pckgName = fqnDto.substring(0, idx + 1);
		String dtoName = fqnDto.substring(idx + 1);
		return toEntityPackage(pckgName).concat(toEntityName(dtoName));
	}

	/**
	 * Returns the full qualified name of the entity for the given LDto.
	 *
	 * @param dto
	 *            the LDto object
	 * @return the full qualified name of an entity as string
	 */
	public static String toFqnEntityName(LDto dto) {
		if (dto.eContainer() != null && dto.eContainer() instanceof LTypedPackage) {
			String pckgName = ((LTypedPackage) dto.eContainer()).getName();
			String dtoName = dto.getName();
			return toEntityPackage(pckgName).concat(toEntityName(dtoName));
		} else {
			throw new IllegalStateException();
		}
	}

	/**
	 * The Class Pair.
	 */
	private static class Pair {

		/** The source. */
		private final String source;

		/** The target. */
		private final String target;

		/**
		 * Instantiates a new pair.
		 *
		 * @param source
		 *            the source
		 * @param target
		 *            the target
		 */
		public Pair(String source, String target) {
			super();
			this.source = source;
			this.target = target;
		}

		/**
		 * Replace.
		 *
		 * @param value
		 *            the value
		 * @return the string
		 */
		public String replace(String value) {
			return value.replace(source, target);
		}
	}

}
