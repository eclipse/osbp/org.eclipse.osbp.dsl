/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.extensions

import com.google.inject.Inject
import org.eclipse.osbp.dsl.common.xtext.extensions.NamingExtensions
import org.eclipse.osbp.dsl.semantic.common.types.LType
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage
import org.eclipse.osbp.dsl.semantic.dto.LDto
import org.eclipse.osbp.dsl.semantic.dto.LDtoFeature
import org.eclipse.osbp.dsl.semantic.dto.util.NamingConventionsUtil
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.xbase.XExpression

class MethodNamingExtensions extends NamingExtensions {

	@Inject extension DtoModelExtensions;
	@Inject extension IQualifiedNameProvider;

	def toMapPropertyToDto(LDtoFeature prop) {
		"toDto_" + prop.toName
	}

	def toMapPropertyToEntity(LDtoFeature prop) {
		"toEntity_" + prop.toName
	}

	def toMapperFieldName(LDtoFeature prop) {
		prop.toName + "Mapper"
	}

	def toMapperName(LType dto) {
		if (dto == null || dto.toName == null) {
			return "setMISSING_NAME"
		}
		dto.toName.toMapperName
	}

	def toMapperName(String name) {
		name + "Mapper"
	}

	def toFqnMapperName(LType dto) {
		if (dto == null || dto.toName == null) {
			return "setMISSING_NAME"
		}
		dto.toMapperNamePackage + "." + dto.toMapperName
	}

	def toMapperNamePackage(LType dto) {
		if (dto == null || dto.fullyQualifiedName == null) {
			return "setMISSING_NAME"
		}
		dto.fullyQualifiedName.skipLast(1).append("mapper").toString
	}

	def String toDTOBeanFullyQualifiedName(LType type) {
		val LTypedPackage pkg = type.package as LTypedPackage
		if (type instanceof LDto) {
			return pkg.name + "." + type.toDTOBeanSimpleName
		} else {
			return pkg.toDtoPackageName + "." + type.toDTOBeanSimpleName
		}
	}

	def String toDTOBeanSimpleName(LType type) {
		val name = type?.name
		if (type instanceof LDto) {
			return name
		} else {
			return name.toDTOBeanSimpleName
		}
	}

	def String toDTOEnumFullyQualifiedName(LType type) {
		if (type.eResource.URI.toString.contains("datatype")) {
			return type.toQualifiedName
		} else {
			val LTypedPackage pkg = type.package as LTypedPackage
			if (type instanceof LDto) {
				return pkg.name + "." + type.toDTOEnumSimpleName
			} else {
				return pkg.toDtoPackageName + "." + type.toDTOEnumSimpleName
			}
		}
	}

	def String toDTOStateClassFullyQualifiedName(LType type) {
		val LTypedPackage pkg = type.package as LTypedPackage
		return pkg.name + "." + type?.name
	}

	def String toDTOEnumSimpleName(LType type) {
		return type?.name
	}

	def String toDTOBeanSimpleName(String name) {
		return NamingConventionsUtil.toDtoName(name)
	}

	def XExpression toMapToEntityExpression(LDtoFeature prop) {
		if(prop.mapper != null) return prop.mapper.fromDTO else null
	}

	def XExpression toMapToDtoExpression(LDtoFeature prop) {
		if(prop.mapper != null) return prop.mapper.toDTO else null
	}

}
