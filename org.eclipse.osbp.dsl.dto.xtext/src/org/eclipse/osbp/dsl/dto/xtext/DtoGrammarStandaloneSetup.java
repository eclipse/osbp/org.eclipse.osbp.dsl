
/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
 
package org.eclipse.osbp.dsl.dto.xtext;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;
import org.eclipse.osbp.dsl.semantic.dto.impl.DtoFactoryImplCustom;
import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage;
import org.eclipse.osbp.dsl.semantic.entity.impl.EntityFactoryImplCustom;

import com.google.inject.Injector;




/**
 * Initialization support for running Xtext languages without equinox extension
 * registry
 */
public class DtoGrammarStandaloneSetup extends
		DtoGrammarStandaloneSetupGenerated {

	public static void doSetup() {
		new DtoGrammarStandaloneSetup().createInjectorAndDoEMFRegistration();
	}

	public void register(Injector injector) {
		if (!EPackage.Registry.INSTANCE
				.containsKey("http://osbp.eclipse.org/dto/v1")) {
			EPackage.Registry.INSTANCE.put(
					"http://osbp.eclipse.org/dsl/dto/v1", OSBPDtoPackage.eINSTANCE);
			OSBPDtoPackage.eINSTANCE
					.setEFactoryInstance(new DtoFactoryImplCustom());
		}

		if (!EPackage.Registry.INSTANCE
				.containsKey("http://osbp.eclipse.org/entity/v1")) {
			EPackage.Registry.INSTANCE.put(
					"http://osbp.eclipse.org/dsl/entity/v1",
					OSBPEntityPackage.eINSTANCE);
			OSBPEntityPackage.eINSTANCE
					.setEFactoryInstance(new EntityFactoryImplCustom());
		}

		if (!EPackage.Registry.INSTANCE
				.containsKey("http://osbp.eclipse.org/dsl/common/types/v1")) {
			EPackage.Registry.INSTANCE.put(
					"http://osbp.eclipse.org/dsl/common/types/v1",
					OSBPTypesPackage.eINSTANCE);
		}

		super.register(injector);

	}
}
