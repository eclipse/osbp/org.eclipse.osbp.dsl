/**
 * Copyright (c) 2011, 2015 - Florian Pirchner,  Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.valueconverter;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.common.xtext.valueconverter.CommonQualifiedNameProvider;
import org.eclipse.osbp.dsl.dto.xtext.extensions.DtoModelExtensions;
import org.eclipse.osbp.dsl.semantic.dto.LDtoFeature;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.xtext.linking.impl.LinkingHelper;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.nodemodel.ILeafNode;
import org.eclipse.xtext.nodemodel.impl.CompositeNodeWithSemanticElement;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

import com.google.inject.Inject;

public class DtoQualifiedNameProvider extends CommonQualifiedNameProvider {

	@Inject
	private IQualifiedNameConverter qualifiedNameConverter;
	@Inject
	private DtoModelExtensions extensions;
	@Inject
	private LinkingHelper linkingHelper;

	@SuppressWarnings("restriction")
	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		if (obj == null) {
			return QualifiedName.create("");
		}

		if (obj instanceof LDtoFeature) {
			LDtoFeature feature = (LDtoFeature) obj;

			QualifiedName parentFQN = getFullyQualifiedName(feature.eContainer());

			String name = "";
			if (extensions.inherited(feature)) {
				CompositeNodeWithSemanticElement node = (CompositeNodeWithSemanticElement) NodeModelUtils
						.getNode(feature);
				if (node == null) {
					// the model is beeing serialized. No grammar available yet
					if (feature instanceof LDtoInheritedAttribute) {
						name = ((LDtoInheritedAttribute) feature).getInheritedFeature().getName();
					} else {
						name = ((LDtoInheritedReference) feature).getInheritedFeature().getName();
					}
				} else {
					List<ILeafNode> leafs = IterableExtensions.toList(node.getLeafNodes());
					name = linkingHelper.getCrossRefNodeAsString(leafs.get(leafs.size() - 1), false);
				}
			} else {
				name = feature.getName();
			}

			return name != null ? parentFQN.append(name) : parentFQN.append("missing");
		}
		return super.getFullyQualifiedName(obj);
	}

}
