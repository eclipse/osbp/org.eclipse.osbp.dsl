/**
 * Copyright (c) 2011 - 2012, Lunifera GmbH (Gross Enzersdorf, Austria), Hans Georg Glöckler, Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Hans Georg Glöckler - Initial implementation
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.scoping;

import org.eclipse.osbp.dsl.common.xtext.scoping.CommonImportSectionNamespaceScopeProvider;

public class DtoImportSectionNamespaceScopeProvider extends
		CommonImportSectionNamespaceScopeProvider {

}
