/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.generator

import com.google.inject.Inject
import java.util.List
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.osbp.dsl.dto.xtext.extensions.MethodNamingExtensions
import org.eclipse.osbp.dsl.semantic.common.types.LType
import org.eclipse.osbp.dsl.semantic.dto.LDto
import org.eclipse.osbp.dsl.semantic.entity.LBean
import org.eclipse.osbp.dsl.semantic.entity.LEntity

class ComponentGenerator {

	@Inject extension MethodNamingExtensions
	@Inject extension IQualifiedNameProvider
 
	def getServiceContent(LDto dto) '''
		<?xml version="1.0" encoding="UTF-8"?>
		<scr:component xmlns:scr="http://www.osgi.org/xmlns/scr/v1.1.0" name="«dto.toFqnMapperName.toLowerCase»">
		       <implementation class="«dto.toFqnMapperName»"/>
		       <service>
				<provide interface="org.eclipse.osbp.dsl.dto.lib.IMapper"/>
				  </service>
				  
				«val tempDtos = dto.withParent»
				<property name="fordto.from.entity" type="String" value="«dto.wrappedType.fullyQualifiedName.toString»"/>
				«IF tempDtos.length > 1»
					<property name="fordto.to.dto" type="String">«tempDtos.toOrString»
					</property>
				«ELSE»
					<property name="fordto.to.dto" type="String" value="«dto.fullyQualifiedName.toString»"/>
				«ENDIF»

				<property name="forentity.from.dto" type="String" value="«dto.fullyQualifiedName.toString»"/>
				«val tempEntities = dto.wrappedType.withParent»
				«IF tempEntities.length > 1»
					<property name="forentity.to.entity" type="String">«tempEntities.toOrString»
					</property>
				«ELSE»
					<property name="forentity.to.entity" type="String" value="«dto.wrappedType.fullyQualifiedName.toString»"/>
				«ENDIF»
				
				<property name="service.pid" type="String" value="«dto.toFqnMapperName.toLowerCase»"/>
				<reference name="mapperAccess" interface="org.eclipse.osbp.dsl.dto.lib.IMapperAccess" 
						cardinality="1..1" policy="static" bind="bindMapperAccess" unbind="unbindMapperAccess"/>
		</scr:component>
	'''

	/**
	 * Properties will be returned as a String that OSGi will interpret as a list.
	 */
	def String toOrString(List<LType> types) {
		val StringBuilder b = new StringBuilder

		types.forEach [
			if (b.length > 0) {
				b.append("\n")
			}
			b.append(fullyQualifiedName.toString)
		]

		return b.toString
	}

	def dispatch List<LType> getWithParent(LEntity entity) {
		val result = <LType>newArrayList()
		collectWithParent(result, entity)
		return result
	}

	def void collectWithParent(List<LType> result, LEntity entity) {
		result += entity

		if (entity.superType != null) {
			collectWithParent(result, entity.superType)
		}
	}

	def dispatch List<LType> getWithParent(LBean bean) {
		val result = <LType>newArrayList()
		collectWithParent(result, bean)
		return result
	}

	def void collectWithParent(List<LType> result, LBean bean) {
		result += bean

		if (bean.superType != null) {
			collectWithParent(result, bean.superType)
		}
	}

	def dispatch List<LType> getWithParent(LDto dto) {
		val result = <LType>newArrayList()
		collectWithParent(result, dto)
		return result
	}

	def void collectWithParent(List<LType> result, LDto dto) {
		result += dto

		if (dto.superType != null) {
			collectWithParent(result, dto.superType)
		}
	}
}
