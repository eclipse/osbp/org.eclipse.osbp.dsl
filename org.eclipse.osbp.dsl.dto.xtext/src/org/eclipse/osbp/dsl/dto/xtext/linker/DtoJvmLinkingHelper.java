/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.linker;

import org.eclipse.osbp.dsl.dto.xtext.extensions.DtoModelExtensions;
import org.eclipse.osbp.dsl.dto.xtext.extensions.MethodNamingExtensions;
import org.eclipse.osbp.xtext.oxtype.linking.JvmTypeAwareLinkingHelper;

import com.google.inject.Inject;

public class DtoJvmLinkingHelper extends JvmTypeAwareLinkingHelper {

	@Inject
	private MethodNamingExtensions namingExtension;

	@Inject
	private DtoModelExtensions extensions;

	public DtoJvmLinkingHelper() {
		// register(OSBPDtoPackage.Literals.LDTO__SUPER_TYPE,
		// OSBPDtoPackage.Literals.LDTO__SUPER_TYPE_JVM, new
		// IJvmLinkCrossRefStringEnhancer() {
		// @Override
		// public String enhance(EObject context, EStructuralFeature feature,
		// String crossRefString) {
		// return crossRefString;
		// }
		// }, null);
		// register(OSBPDtoPackage.Literals.LDTO__WRAPPED_TYPE,
		// OSBPDtoPackage.Literals.LDTO__WRAPPED_TYPE_JVM);

		// register(OSBPTypesPackage.Literals.LATTRIBUTE__TYPE,
		// OSBPTypesPackage.Literals.LATTRIBUTE__TYPE_JVM,
		// new IJvmLinkCrossRefStringEnhancer() {
		// @SuppressWarnings("restriction")
		// @Override
		// public String enhance(EObject context, EStructuralFeature feature,
		// String crossRefString) {
		// LDtoAttribute lAtt = (LDtoAttribute) context.eContainer();
		// if (lAtt.eIsProxy()) {
		// return crossRefString;
		// }
		// LScalarType type = lAtt.getType();
		// if (type instanceof LDataType) {
		// LDataType datatype = (LDataType) type;
		// JvmTypeReference ref = extensions.toTypeReference(datatype);
		// return ref != null ? ref.getSimpleName() : crossRefString;
		// }else if(type instanceof LEnum) {
		// LEnum lEnum = (LEnum) type;
		// return lEnum.getName();
		// }
		// return crossRefString;
		// }
		// }, null);

		// register(OSBPDtoPackage.Literals.LDTO_ABSTRACT_REFERENCE__TYPE,
		// OSBPDtoPackage.Literals.LDTO_ABSTRACT_REFERENCE__TYPE_JVM);
		//
		// register(OSBPDtoPackage.Literals.LDTO_INHERITED_ATTRIBUTE__INHERITED_FEATURE,
		// OSBPDtoPackage.Literals.LDTO_INHERITED_ATTRIBUTE__INHERITED_FEATURE_TYPE_JVM,
		// new IJvmLinkCrossRefStringEnhancer() {
		// @SuppressWarnings("restriction")
		// @Override
		// public String enhance(EObject context, EStructuralFeature feature,
		// String crossRefString) {
		// LDtoInheritedAttribute lAtt = (LDtoInheritedAttribute)
		// context.eContainer();
		// LAttribute lInhAtt = lAtt.getInheritedFeature();
		// if (lInhAtt.eIsProxy()) {
		// return crossRefString;
		// }
		// if (lInhAtt.getType() instanceof LBean) {
		// return
		// namingExtension.toDTOBeanSimpleName(lInhAtt.getType().getName());
		// } else {
		// LScalarType type = lInhAtt.getType();
		// if (type instanceof LDataType) {
		// LDataType datatype = (LDataType) type;
		// JvmTypeReference ref = extensions.toTypeReference(datatype);
		// return ref != null ? ref.getSimpleName() : crossRefString;
		// }else if(type instanceof LEnum) {
		// LEnum lEnum = (LEnum) type;
		// return lEnum.getName();
		// }
		// }
		// return crossRefString;
		// }
		// }, null);
		//
		// register(OSBPDtoPackage.Literals.LDTO__WRAPPED_TYPE,
		// OSBPDtoPackage.Literals.LDTO__WRAPPED_TYPE_JVM);
		//
		// register(OSBPDtoPackage.Literals.LDTO_INHERITED_REFERENCE__INHERITED_FEATURE,
		// OSBPDtoPackage.Literals.LDTO_INHERITED_REFERENCE__INHERITED_FEATURE_TYPE_JVM,
		// new IJvmLinkCrossRefStringEnhancer() {
		// @Override
		// public String enhance(EObject context, EStructuralFeature feature,
		// String crossRefString) {
		// LDtoInheritedReference lRef = (LDtoInheritedReference)
		// context.eContainer();
		// LReference lInhRef = lRef.getInheritedFeature();
		// if (lInhRef.eIsProxy()) {
		// return crossRefString;
		// }
		// if (lInhRef instanceof LEntityReference) {
		// LEntityReference lEntityRef = (LEntityReference) lInhRef;
		// if (lEntityRef.getType() instanceof LEntity) {
		// return
		// namingExtension.toDTOBeanSimpleName(lEntityRef.getType().getName());
		// } else {
		// return lEntityRef.getType().getName();
		// }
		// } else if (lInhRef instanceof LBeanReference) {
		// LBeanReference lBeanRef = (LBeanReference) lInhRef;
		// if (lBeanRef.getType() instanceof LBean) {
		// return
		// namingExtension.toDTOBeanSimpleName(lBeanRef.getType().getName());
		// } else {
		// return lBeanRef.getType().getName();
		// }
		// }
		//
		// throw new IllegalArgumentException(feature + " ----- " + context);
		// }
		// }, null);
		//
		// // Also register the need of a proxy for the jvmType of a mapper. But
		// // therefore a different crossRefString needs to become used.
		// register(OSBPDtoPackage.Literals.LDTO__SUPER_TYPE,
		// OSBPDtoPackage.Literals.LDTO__SUPER_TYPE_MAPPER_JVM,
		// new IJvmLinkCrossRefStringEnhancer() {
		// @Override
		// public String enhance(EObject context, EStructuralFeature feature,
		// String crossRefString) {
		// return namingExtension.toMapperName(crossRefString);
		// }
		// }, new IJvmTypeRefFinisher() {
		// @Override
		// public void finish(EStructuralFeature jvmLinkFeature,
		// JvmTypeReference typeRef) {
		// typeRef.eAdapters().add(new
		// SuppressWarningAdapter(SuppressWarningAdapter.RAW_TYPE));
		// }
		// });
	}
}
