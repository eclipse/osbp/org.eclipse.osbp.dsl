/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.scoping;

import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.FilteringScope;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.xtext.oxtype.resource.EcoreUtil3;

import com.google.common.base.Predicate;

public class InheritTypesFilterScope extends FilteringScope {

	public InheritTypesFilterScope(LType context, IScope scope) {
		super(scope, createPredicate(context));
	}

	private static Predicate<IEObjectDescription> createPredicate(
			final LType context) {
		return new Predicate<IEObjectDescription>() {

			@Override
			public boolean apply(IEObjectDescription input) {
				LType type = (LType) input.getEObjectOrProxy();
				if (type.eIsProxy()) {
					type = (LType) EcoreUtil3.resolve(type, context.eResource()
							.getResourceSet());
				}
				if (type != context && type.getName() != null) {
					return true;
				}
				return false;
			}
		};
	}
}
