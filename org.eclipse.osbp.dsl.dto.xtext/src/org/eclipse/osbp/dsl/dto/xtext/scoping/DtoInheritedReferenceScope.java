/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.scoping;

import java.util.ArrayList;

import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;

public class DtoInheritedReferenceScope extends AbstractScope {
	private final LDtoInheritedReference prop;

	public DtoInheritedReferenceScope(LDtoInheritedReference prop) {
		super(IScope.NULLSCOPE, true);
		this.prop = prop;
	}

	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {
		ArrayList<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		LDto container = prop.getDTO();
		LType type = container.getWrappedType();

		if (type instanceof LEntity) {
			LEntity entity = (LEntity) type;
			for (LEntityReference entityRef : entity.getAllReferences()) {
				result.add(new EObjectDescription(QualifiedName
						.create(entityRef.getName()), entityRef, null));
			}
		} else if (type instanceof LBean) {
			LBean bean = (LBean) type;
			for (LBeanReference beanRef : bean.getAllReferences()) {
				result.add(new EObjectDescription(QualifiedName.create(beanRef
						.getName()), beanRef, null));
			}
		}

		return result;
	}
}