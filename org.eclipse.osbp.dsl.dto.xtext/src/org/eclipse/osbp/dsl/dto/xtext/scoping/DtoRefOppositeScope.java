/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.scoping;

import java.util.ArrayList;

import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.AbstractScope;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoReference;

public class DtoRefOppositeScope extends AbstractScope {
	private final LDtoReference prop;

	public DtoRefOppositeScope(LDtoReference prop) {
		super(IScope.NULLSCOPE, true);
		this.prop = prop;
	}
 
	@Override
	protected Iterable<IEObjectDescription> getAllLocalElements() {
		ArrayList<IEObjectDescription> result = new ArrayList<IEObjectDescription>();
		if (prop.getType() != null) {
			LDto propClass = prop.getDTO();
			LDto type = prop.getType();
			for (LDtoAbstractReference oppositeProp : type.getReferences()) {
				if (oppositeProp.getType() == propClass) {
					String name = oppositeProp.getName();
					if (name != null) {
						result.add(new EObjectDescription(QualifiedName
								.create(name), oppositeProp, null));
					}
				}
			}
		}
		return result;
	}
}