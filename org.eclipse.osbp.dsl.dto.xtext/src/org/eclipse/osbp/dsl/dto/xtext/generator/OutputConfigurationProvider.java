/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.dsl.dto.xtext.generator;

import java.util.Set;

import org.eclipse.osbp.xtext.oxtype.hooks.DelegatingOutputConfigurationProvider;
import org.eclipse.xtext.generator.OutputConfiguration;

public class OutputConfigurationProvider extends
		DelegatingOutputConfigurationProvider {

	/**
	 * @return a set of {@link OutputConfiguration} available for the generator
	 */
	public Set<OutputConfiguration> getOutputConfigurations() {
		Set<OutputConfiguration> configs = super.getOutputConfigurations();

		OutputConfiguration componentOutput = new OutputConfiguration(
				"OSGI-INF");
		componentOutput.setDescription("OSGi-Services");
		componentOutput.setOutputDirectory("./OSGI-INF");
		componentOutput.setOverrideExistingResources(true);
		componentOutput.setCreateOutputDirectory(true);
		componentOutput.setCleanUpDerivedResources(true);
		componentOutput.setSetDerivedProperty(true);
		componentOutput.setKeepLocalHistory(true);
		configs.add(componentOutput);

		OutputConfiguration dtoMapperOutput = new OutputConfiguration(
				"Dto-Mappers");
		dtoMapperOutput.setDescription("Dto Mappers");
		dtoMapperOutput.setOutputDirectory("./src-map");
		dtoMapperOutput.setOverrideExistingResources(true);
		dtoMapperOutput.setCreateOutputDirectory(true);
		dtoMapperOutput.setCleanUpDerivedResources(true);
		dtoMapperOutput.setSetDerivedProperty(true);
		dtoMapperOutput.setKeepLocalHistory(true);
		configs.add(dtoMapperOutput);

//		OutputConfiguration servicesOutput = new OutputConfiguration(
//				"Services-DSL");
//		servicesOutput.setDescription("Services-DSL");
//		servicesOutput.setOutputDirectory("./service-models");
//		servicesOutput.setOverrideExistingResources(true);
//		servicesOutput.setCreateOutputDirectory(true);
//		servicesOutput.setKeepLocalHistory(true);
//		// IMPORTANT - do not change!
//		servicesOutput.setCanClearOutputDirectory(false);
//		servicesOutput.setCleanUpDerivedResources(false);
//		servicesOutput.setSetDerivedProperty(false);
//		configs.add(servicesOutput);

		OutputConfiguration binOutput = new OutputConfiguration("ModelBin");
		binOutput.setDescription("ModelBin");
		binOutput.setOutputDirectory("./modelsbin");
		binOutput.setOverrideExistingResources(true);
		binOutput.setCreateOutputDirectory(true);
		binOutput.setCleanUpDerivedResources(true);
		binOutput.setSetDerivedProperty(true);
		binOutput.setKeepLocalHistory(true);
		configs.add(binOutput);

		return configs;
	}

}
