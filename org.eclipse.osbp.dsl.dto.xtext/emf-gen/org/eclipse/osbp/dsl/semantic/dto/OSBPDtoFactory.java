/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage
 * @generated
 */
public interface OSBPDtoFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OSBPDtoFactory eINSTANCE = org.eclipse.osbp.dsl.semantic.dto.impl.OSBPDtoFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>LDto Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDto Model</em>'.
	 * @generated
	 */
	LDtoModel createLDtoModel();

	/**
	 * Returns a new object of class '<em>LDto</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDto</em>'.
	 * @generated
	 */
	LDto createLDto();

	/**
	 * Returns a new object of class '<em>LAuto Inherit Dto</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LAuto Inherit Dto</em>'.
	 * @generated
	 */
	LAutoInheritDto createLAutoInheritDto();

	/**
	 * Returns a new object of class '<em>LDto Mapper</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDto Mapper</em>'.
	 * @generated
	 */
	LDtoMapper createLDtoMapper();

	/**
	 * Returns a new object of class '<em>LDto Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDto Feature</em>'.
	 * @generated
	 */
	LDtoFeature createLDtoFeature();

	/**
	 * Returns a new object of class '<em>LDto Inherited Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDto Inherited Attribute</em>'.
	 * @generated
	 */
	LDtoInheritedAttribute createLDtoInheritedAttribute();

	/**
	 * Returns a new object of class '<em>LDto Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDto Attribute</em>'.
	 * @generated
	 */
	LDtoAttribute createLDtoAttribute();

	/**
	 * Returns a new object of class '<em>LDto Inherited Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDto Inherited Reference</em>'.
	 * @generated
	 */
	LDtoInheritedReference createLDtoInheritedReference();

	/**
	 * Returns a new object of class '<em>LDto Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDto Reference</em>'.
	 * @generated
	 */
	LDtoReference createLDtoReference();

	/**
	 * Returns a new object of class '<em>LDto Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDto Operation</em>'.
	 * @generated
	 */
	LDtoOperation createLDtoOperation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OSBPDtoPackage getOSBPDtoPackage();

} //OSBPDtoFactory
