/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.osbp.dsl.semantic.dto.LAutoInheritDto;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LAuto Inherit Dto</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LAutoInheritDtoImpl extends LDtoImpl implements LAutoInheritDto {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LAutoInheritDtoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSBPDtoPackage.Literals.LAUTO_INHERIT_DTO;
	}

} //LAutoInheritDtoImpl
