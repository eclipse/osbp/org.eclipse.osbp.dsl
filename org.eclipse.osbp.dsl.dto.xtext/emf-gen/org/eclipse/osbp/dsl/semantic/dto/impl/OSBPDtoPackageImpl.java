/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto.impl;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.dto.LAutoInheritDto;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoFeature;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoMapper;
import org.eclipse.osbp.dsl.semantic.dto.LDtoModel;
import org.eclipse.osbp.dsl.semantic.dto.LDtoOperation;
import org.eclipse.osbp.dsl.semantic.dto.LDtoReference;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoFactory;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;
import org.eclipse.xtext.xbase.XbasePackage;
import org.eclipse.xtext.xtype.XtypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OSBPDtoPackageImpl extends EPackageImpl implements OSBPDtoPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lAutoInheritDtoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoMapperEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoAbstractAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoInheritedAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoAbstractReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoInheritedReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lDtoOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dtoFeatureListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType operationsListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dtoReferenceListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dtoAttributeListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dtoAbstractAttributeListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType internalEObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType propertiesListEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OSBPDtoPackageImpl() {
		super(eNS_URI, OSBPDtoFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OSBPDtoPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OSBPDtoPackage init() {
		if (isInited) return (OSBPDtoPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPDtoPackage.eNS_URI);

		// Obtain or create and register package
		OSBPDtoPackageImpl theOSBPDtoPackage = (OSBPDtoPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OSBPDtoPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OSBPDtoPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		OSBPTypesPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOSBPDtoPackage.createPackageContents();

		// Initialize created meta-data
		theOSBPDtoPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOSBPDtoPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OSBPDtoPackage.eNS_URI, theOSBPDtoPackage);
		return theOSBPDtoPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoModel() {
		return lDtoModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDtoModel_ImportSection() {
		return (EReference)lDtoModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDtoModel_Packages() {
		return (EReference)lDtoModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDto() {
		return lDtoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDto_Features() {
		return (EReference)lDtoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDto_SuperType() {
		return (EReference)lDtoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDto_SubTypes() {
		return (EReference)lDtoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDto_WrappedType() {
		return (EReference)lDtoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLAutoInheritDto() {
		return lAutoInheritDtoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoMapper() {
		return lDtoMapperEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDtoMapper_ToDTO() {
		return (EReference)lDtoMapperEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDtoMapper_FromDTO() {
		return (EReference)lDtoMapperEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoFeature() {
		return lDtoFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDtoFeature_Mapper() {
		return (EReference)lDtoFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoAbstractAttribute() {
		return lDtoAbstractAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoInheritedAttribute() {
		return lDtoInheritedAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDtoInheritedAttribute_InheritedFeature() {
		return (EReference)lDtoInheritedAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoAttribute() {
		return lDtoAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoAbstractReference() {
		return lDtoAbstractReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDtoAbstractReference_Type() {
		return (EReference)lDtoAbstractReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoInheritedReference() {
		return lDtoInheritedReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDtoInheritedReference_InheritedFeature() {
		return (EReference)lDtoInheritedReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoReference() {
		return lDtoReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLDtoReference_Opposite() {
		return (EReference)lDtoReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLDtoOperation() {
		return lDtoOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDtoFeatureList() {
		return dtoFeatureListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getOperationsList() {
		return operationsListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDtoReferenceList() {
		return dtoReferenceListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDtoAttributeList() {
		return dtoAttributeListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDtoAbstractAttributeList() {
		return dtoAbstractAttributeListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInternalEObject() {
		return internalEObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPropertiesList() {
		return propertiesListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPDtoFactory getOSBPDtoFactory() {
		return (OSBPDtoFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		lDtoModelEClass = createEClass(LDTO_MODEL);
		createEReference(lDtoModelEClass, LDTO_MODEL__IMPORT_SECTION);
		createEReference(lDtoModelEClass, LDTO_MODEL__PACKAGES);

		lDtoEClass = createEClass(LDTO);
		createEReference(lDtoEClass, LDTO__FEATURES);
		createEReference(lDtoEClass, LDTO__SUPER_TYPE);
		createEReference(lDtoEClass, LDTO__SUB_TYPES);
		createEReference(lDtoEClass, LDTO__WRAPPED_TYPE);

		lAutoInheritDtoEClass = createEClass(LAUTO_INHERIT_DTO);

		lDtoMapperEClass = createEClass(LDTO_MAPPER);
		createEReference(lDtoMapperEClass, LDTO_MAPPER__TO_DTO);
		createEReference(lDtoMapperEClass, LDTO_MAPPER__FROM_DTO);

		lDtoFeatureEClass = createEClass(LDTO_FEATURE);
		createEReference(lDtoFeatureEClass, LDTO_FEATURE__MAPPER);

		lDtoAbstractAttributeEClass = createEClass(LDTO_ABSTRACT_ATTRIBUTE);

		lDtoInheritedAttributeEClass = createEClass(LDTO_INHERITED_ATTRIBUTE);
		createEReference(lDtoInheritedAttributeEClass, LDTO_INHERITED_ATTRIBUTE__INHERITED_FEATURE);

		lDtoAttributeEClass = createEClass(LDTO_ATTRIBUTE);

		lDtoAbstractReferenceEClass = createEClass(LDTO_ABSTRACT_REFERENCE);
		createEReference(lDtoAbstractReferenceEClass, LDTO_ABSTRACT_REFERENCE__TYPE);

		lDtoInheritedReferenceEClass = createEClass(LDTO_INHERITED_REFERENCE);
		createEReference(lDtoInheritedReferenceEClass, LDTO_INHERITED_REFERENCE__INHERITED_FEATURE);

		lDtoReferenceEClass = createEClass(LDTO_REFERENCE);
		createEReference(lDtoReferenceEClass, LDTO_REFERENCE__OPPOSITE);

		lDtoOperationEClass = createEClass(LDTO_OPERATION);

		// Create data types
		dtoFeatureListEDataType = createEDataType(DTO_FEATURE_LIST);
		operationsListEDataType = createEDataType(OPERATIONS_LIST);
		dtoReferenceListEDataType = createEDataType(DTO_REFERENCE_LIST);
		dtoAttributeListEDataType = createEDataType(DTO_ATTRIBUTE_LIST);
		dtoAbstractAttributeListEDataType = createEDataType(DTO_ABSTRACT_ATTRIBUTE_LIST);
		internalEObjectEDataType = createEDataType(INTERNAL_EOBJECT);
		propertiesListEDataType = createEDataType(PROPERTIES_LIST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XtypePackage theXtypePackage = (XtypePackage)EPackage.Registry.INSTANCE.getEPackage(XtypePackage.eNS_URI);
		OSBPTypesPackage theOSBPTypesPackage = (OSBPTypesPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPTypesPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		XbasePackage theXbasePackage = (XbasePackage)EPackage.Registry.INSTANCE.getEPackage(XbasePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		lDtoEClass.getESuperTypes().add(theOSBPTypesPackage.getLClass());
		lDtoEClass.getESuperTypes().add(theOSBPTypesPackage.getLFeaturesHolder());
		lDtoEClass.getESuperTypes().add(theOSBPTypesPackage.getLScalarType());
		lAutoInheritDtoEClass.getESuperTypes().add(this.getLDto());
		lDtoMapperEClass.getESuperTypes().add(theOSBPTypesPackage.getLLazyResolver());
		lDtoFeatureEClass.getESuperTypes().add(theOSBPTypesPackage.getLFeature());
		lDtoAbstractAttributeEClass.getESuperTypes().add(this.getLDtoFeature());
		lDtoAbstractAttributeEClass.getESuperTypes().add(theOSBPTypesPackage.getLAttribute());
		lDtoInheritedAttributeEClass.getESuperTypes().add(this.getLDtoAbstractAttribute());
		lDtoAttributeEClass.getESuperTypes().add(this.getLDtoAbstractAttribute());
		lDtoAbstractReferenceEClass.getESuperTypes().add(this.getLDtoFeature());
		lDtoAbstractReferenceEClass.getESuperTypes().add(theOSBPTypesPackage.getLReference());
		lDtoInheritedReferenceEClass.getESuperTypes().add(this.getLDtoAbstractReference());
		lDtoReferenceEClass.getESuperTypes().add(this.getLDtoAbstractReference());
		lDtoOperationEClass.getESuperTypes().add(theOSBPTypesPackage.getLOperation());
		lDtoOperationEClass.getESuperTypes().add(this.getLDtoFeature());

		// Initialize classes and features; add operations and parameters
		initEClass(lDtoModelEClass, LDtoModel.class, "LDtoModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLDtoModel_ImportSection(), theXtypePackage.getXImportSection(), null, "importSection", null, 0, 1, LDtoModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLDtoModel_Packages(), theOSBPTypesPackage.getLTypedPackage(), null, "packages", null, 0, -1, LDtoModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(lDtoModelEClass, theEcorePackage.getEObject(), "eResolveProxy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInternalEObject(), "proxy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lDtoEClass, LDto.class, "LDto", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLDto_Features(), this.getLDtoFeature(), null, "features", null, 0, -1, LDto.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLDto_SuperType(), this.getLDto(), this.getLDto_SubTypes(), "superType", null, 0, 1, LDto.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLDto_SubTypes(), this.getLDto(), this.getLDto_SuperType(), "subTypes", null, 0, -1, LDto.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLDto_WrappedType(), theOSBPTypesPackage.getLType(), null, "wrappedType", null, 0, 1, LDto.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(lDtoEClass, this.getOperationsList(), "getOperations", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, this.getDtoReferenceList(), "getReferences", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, this.getDtoAttributeList(), "getAttributes", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, theOSBPTypesPackage.getFeaturesList(), "getAllFeatures", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, theOSBPTypesPackage.getLAttribute(), "getIdAttribute", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = addEOperation(lDtoEClass, null, "collectAllOSBPFeatures", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLDto(), "current", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDtoFeatureList(), "result", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, this.getLDtoAbstractAttribute(), "getPrimaryKeyAttribute", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, theEcorePackage.getEString(), "getIdAttributeName", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, theOSBPTypesPackage.getLScalarType(), "getIdAttributeType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, theEcorePackage.getEBoolean(), "isHistorized", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, theEcorePackage.getEBoolean(), "isTimedependent", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, theEcorePackage.getEBoolean(), "isHistorizedOrTimedependent", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoEClass, this.getLDtoAbstractAttribute(), "getHistCurrentAttribute", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lAutoInheritDtoEClass, LAutoInheritDto.class, "LAutoInheritDto", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(lDtoMapperEClass, LDtoMapper.class, "LDtoMapper", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLDtoMapper_ToDTO(), theXbasePackage.getXExpression(), null, "toDTO", null, 0, 1, LDtoMapper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLDtoMapper_FromDTO(), theXbasePackage.getXExpression(), null, "fromDTO", null, 0, 1, LDtoMapper.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lDtoFeatureEClass, LDtoFeature.class, "LDtoFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLDtoFeature_Mapper(), this.getLDtoMapper(), null, "mapper", null, 0, 1, LDtoFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(lDtoFeatureEClass, this.getLDto(), "getDTO", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lDtoAbstractAttributeEClass, LDtoAbstractAttribute.class, "LDtoAbstractAttribute", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(lDtoAbstractAttributeEClass, this.getPropertiesList(), "getResolvedProperties", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoAbstractAttributeEClass, theEcorePackage.getEBoolean(), "isVersionAttr", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lDtoInheritedAttributeEClass, LDtoInheritedAttribute.class, "LDtoInheritedAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLDtoInheritedAttribute_InheritedFeature(), theOSBPTypesPackage.getLAttribute(), null, "inheritedFeature", null, 0, 1, LDtoInheritedAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(lDtoInheritedAttributeEClass, theOSBPTypesPackage.getLScalarType(), "getInheritedType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoInheritedAttributeEClass, theOSBPTypesPackage.getLMultiplicity(), "getInheritedMultiplicity", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoInheritedAttributeEClass, theEcorePackage.getEString(), "getName", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lDtoAttributeEClass, LDtoAttribute.class, "LDtoAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(lDtoAbstractReferenceEClass, LDtoAbstractReference.class, "LDtoAbstractReference", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLDtoAbstractReference_Type(), this.getLDto(), null, "type", null, 0, 1, LDtoAbstractReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(lDtoAbstractReferenceEClass, theEcorePackage.getEBoolean(), "isCascading", 0, 1, !IS_UNIQUE, IS_ORDERED);

		addEOperation(lDtoAbstractReferenceEClass, this.getPropertiesList(), "getResolvedProperties", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lDtoInheritedReferenceEClass, LDtoInheritedReference.class, "LDtoInheritedReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLDtoInheritedReference_InheritedFeature(), theOSBPTypesPackage.getLReference(), null, "inheritedFeature", null, 0, 1, LDtoInheritedReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(lDtoInheritedReferenceEClass, theOSBPTypesPackage.getLMultiplicity(), "getInheritedMultiplicity", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(lDtoReferenceEClass, LDtoReference.class, "LDtoReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLDtoReference_Opposite(), this.getLDtoReference(), null, "opposite", null, 0, 1, LDtoReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lDtoOperationEClass, LDtoOperation.class, "LDtoOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize data types
		initEDataType(dtoFeatureListEDataType, List.class, "DtoFeatureList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.dto.LDtoFeature>");
		initEDataType(operationsListEDataType, List.class, "OperationsList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.dto.LDtoOperation>");
		initEDataType(dtoReferenceListEDataType, List.class, "DtoReferenceList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference>");
		initEDataType(dtoAttributeListEDataType, List.class, "DtoAttributeList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractAttribute>");
		initEDataType(dtoAbstractAttributeListEDataType, List.class, "DtoAbstractAttributeList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractAttribute>");
		initEDataType(internalEObjectEDataType, InternalEObject.class, "InternalEObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(propertiesListEDataType, List.class, "PropertiesList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue>");

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "rootPackage", "dto"
		   });
	}

} //OSBPDtoPackageImpl
