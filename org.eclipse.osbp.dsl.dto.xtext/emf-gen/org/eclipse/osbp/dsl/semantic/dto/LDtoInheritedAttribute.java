/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto;

import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LDto Inherited Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute#getInheritedFeature <em>Inherited Feature</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDtoInheritedAttribute()
 * @generated
 */
public interface LDtoInheritedAttribute extends LDtoAbstractAttribute {
	/**
	 * Returns the value of the '<em><b>Inherited Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inherited Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inherited Feature</em>' reference.
	 * @see #setInheritedFeature(LAttribute)
	 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDtoInheritedAttribute_InheritedFeature()
	 * @generated
	 */
	LAttribute getInheritedFeature();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute#getInheritedFeature <em>Inherited Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inherited Feature</em>' reference.
	 * @see #getInheritedFeature()
	 * @generated
	 */
	void setInheritedFeature(LAttribute value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LScalarType getInheritedType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LMultiplicity getInheritedMultiplicity();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String getName();

} // LDtoInheritedAttribute
