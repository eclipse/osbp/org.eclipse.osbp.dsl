/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;

import org.eclipse.xtext.xbase.XExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LDto Mapper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.LDtoMapper#getToDTO <em>To DTO</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.LDtoMapper#getFromDTO <em>From DTO</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDtoMapper()
 * @generated
 */
public interface LDtoMapper extends LLazyResolver {
	/**
	 * Returns the value of the '<em><b>To DTO</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To DTO</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To DTO</em>' containment reference.
	 * @see #setToDTO(XExpression)
	 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDtoMapper_ToDTO()
	 * @generated
	 */
	XExpression getToDTO();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.dto.LDtoMapper#getToDTO <em>To DTO</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To DTO</em>' containment reference.
	 * @see #getToDTO()
	 * @generated
	 */
	void setToDTO(XExpression value);

	/**
	 * Returns the value of the '<em><b>From DTO</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From DTO</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From DTO</em>' containment reference.
	 * @see #setFromDTO(XExpression)
	 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDtoMapper_FromDTO()
	 * @generated
	 */
	XExpression getFromDTO();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.dto.LDtoMapper#getFromDTO <em>From DTO</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From DTO</em>' containment reference.
	 * @see #getFromDTO()
	 * @generated
	 */
	void setFromDTO(XExpression value);

} // LDtoMapper
