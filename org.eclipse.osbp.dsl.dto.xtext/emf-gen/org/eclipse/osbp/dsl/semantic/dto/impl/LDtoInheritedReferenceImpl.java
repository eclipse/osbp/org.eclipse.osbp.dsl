/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.dsl.semantic.common.types.LMultiplicity;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;

import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LDto Inherited Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoInheritedReferenceImpl#getInheritedFeature <em>Inherited Feature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LDtoInheritedReferenceImpl extends LDtoAbstractReferenceImpl implements LDtoInheritedReference {
	/**
	 * The cached value of the '{@link #getInheritedFeature() <em>Inherited Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInheritedFeature()
	 * @generated
	 * @ordered
	 */
	protected LReference inheritedFeature;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LDtoInheritedReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSBPDtoPackage.Literals.LDTO_INHERITED_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LReference getInheritedFeature() {
		if (inheritedFeature != null && inheritedFeature.eIsProxy()) {
			InternalEObject oldInheritedFeature = (InternalEObject)inheritedFeature;
			inheritedFeature = (LReference)eResolveProxy(oldInheritedFeature);
			if (inheritedFeature != oldInheritedFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OSBPDtoPackage.LDTO_INHERITED_REFERENCE__INHERITED_FEATURE, oldInheritedFeature, inheritedFeature));
			}
		}
		return inheritedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LReference basicGetInheritedFeature() {
		return inheritedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInheritedFeature(LReference newInheritedFeature) {
		LReference oldInheritedFeature = inheritedFeature;
		inheritedFeature = newInheritedFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_INHERITED_REFERENCE__INHERITED_FEATURE, oldInheritedFeature, inheritedFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LMultiplicity getInheritedMultiplicity() {
		return this.getInheritedFeature().getMultiplicity();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OSBPDtoPackage.LDTO_INHERITED_REFERENCE__INHERITED_FEATURE:
				if (resolve) return getInheritedFeature();
				return basicGetInheritedFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OSBPDtoPackage.LDTO_INHERITED_REFERENCE__INHERITED_FEATURE:
				setInheritedFeature((LReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OSBPDtoPackage.LDTO_INHERITED_REFERENCE__INHERITED_FEATURE:
				setInheritedFeature((LReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OSBPDtoPackage.LDTO_INHERITED_REFERENCE__INHERITED_FEATURE:
				return inheritedFeature != null;
		}
		return super.eIsSet(featureID);
	}

} //LDtoInheritedReferenceImpl
