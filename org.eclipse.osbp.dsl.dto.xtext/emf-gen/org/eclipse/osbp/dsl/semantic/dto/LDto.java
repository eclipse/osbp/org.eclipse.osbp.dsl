/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto;

import java.util.List;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LFeaturesHolder;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.common.types.LType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LDto</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A bean is an embeddable element that may become added to a entity using the @embedd annotation.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.LDto#getFeatures <em>Features</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.LDto#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.LDto#getSubTypes <em>Sub Types</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.LDto#getWrappedType <em>Wrapped Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDto()
 * @generated
 */
public interface LDto extends LClass, LFeaturesHolder, LScalarType {
	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.dto.LDtoFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDto_Features()
	 * @generated
	 */
	EList<LDtoFeature> getFeatures();

	/**
	 * Returns the value of the '<em><b>Super Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.dsl.semantic.dto.LDto#getSubTypes <em>Sub Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Type</em>' reference.
	 * @see #setSuperType(LDto)
	 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDto_SuperType()
	 * @see org.eclipse.osbp.dsl.semantic.dto.LDto#getSubTypes
	 * @generated
	 */
	LDto getSuperType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.dto.LDto#getSuperType <em>Super Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Type</em>' reference.
	 * @see #getSuperType()
	 * @generated
	 */
	void setSuperType(LDto value);

	/**
	 * Returns the value of the '<em><b>Sub Types</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.dto.LDto}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.dsl.semantic.dto.LDto#getSuperType <em>Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Types</em>' reference list.
	 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDto_SubTypes()
	 * @see org.eclipse.osbp.dsl.semantic.dto.LDto#getSuperType
	 * @generated
	 */
	EList<LDto> getSubTypes();

	/**
	 * Returns the value of the '<em><b>Wrapped Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wrapped Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wrapped Type</em>' reference.
	 * @see #setWrappedType(LType)
	 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDto_WrappedType()
	 * @generated
	 */
	LType getWrappedType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.dto.LDto#getWrappedType <em>Wrapped Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wrapped Type</em>' reference.
	 * @see #getWrappedType()
	 * @generated
	 */
	void setWrappedType(LType value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of type LOperation
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LDtoOperation> getOperations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of type LDtoReference
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LDtoAbstractReference> getReferences();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of type LDtoAttribute
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<LDtoAbstractAttribute> getAttributes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns all features of the holder and from super types
	 * <!-- end-model-doc -->
	 * @generated
	 */
	List<? extends LFeature> getAllFeatures();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LAttribute getIdAttribute();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	void collectAllOSBPFeatures(LDto current, List<LDtoFeature> result);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LDtoAbstractAttribute getPrimaryKeyAttribute();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String getIdAttributeName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LScalarType getIdAttributeType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns true, if the wrapped entity is historized.
	 * <!-- end-model-doc -->
	 * @generated
	 */
	boolean isHistorized();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns true, if the wrapped entity is timedependent.
	 * <!-- end-model-doc -->
	 * @generated
	 */
	boolean isTimedependent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns true, if the wrapped entity is historized or timedependent.
	 * <!-- end-model-doc -->
	 * @generated
	 */
	boolean isHistorizedOrTimedependent();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LDtoAbstractAttribute getHistCurrentAttribute();

} // LDto
