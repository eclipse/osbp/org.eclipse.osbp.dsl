/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LDatatypeConstraint;
import org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;

import org.eclipse.xtext.xbase.XExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LDto Abstract Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isUuid <em>Uuid</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isVersion <em>Version</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isLazy <em>Lazy</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isTransient <em>Transient</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isDerived <em>Derived</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isDirty <em>Dirty</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isDomainKey <em>Domain Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isDomainDescription <em>Domain Description</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isFiltering <em>Filtering</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isRangeFiltering <em>Range Filtering</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isUniqueEntry <em>Unique Entry</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isAttributeHidden <em>Attribute Hidden</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isAttributeReadOnly <em>Attribute Read Only</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#getExtraStyle <em>Extra Style</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#getDerivedGetterExpression <em>Derived Getter Expression</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#isIsGrouped <em>Is Grouped</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.dto.impl.LDtoAbstractAttributeImpl#getGroupName <em>Group Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class LDtoAbstractAttributeImpl extends LDtoFeatureImpl implements LDtoAbstractAttribute {
	/**
	 * The default value of the '{@link #isId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isId()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ID_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isId()
	 * @generated
	 * @ordered
	 */
	protected boolean id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUuid()
	 * @generated
	 * @ordered
	 */
	protected static final boolean UUID_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUuid()
	 * @generated
	 * @ordered
	 */
	protected boolean uuid = UUID_EDEFAULT;

	/**
	 * The default value of the '{@link #isVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVersion()
	 * @generated
	 * @ordered
	 */
	protected static final boolean VERSION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVersion()
	 * @generated
	 * @ordered
	 */
	protected boolean version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #isLazy() <em>Lazy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLazy()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LAZY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isLazy() <em>Lazy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLazy()
	 * @generated
	 * @ordered
	 */
	protected boolean lazy = LAZY_EDEFAULT;

	/**
	 * The default value of the '{@link #isTransient() <em>Transient</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransient()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TRANSIENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTransient() <em>Transient</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransient()
	 * @generated
	 * @ordered
	 */
	protected boolean transient_ = TRANSIENT_EDEFAULT;

	/**
	 * The default value of the '{@link #isDerived() <em>Derived</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDerived()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DERIVED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDerived() <em>Derived</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDerived()
	 * @generated
	 * @ordered
	 */
	protected boolean derived = DERIVED_EDEFAULT;

	/**
	 * The default value of the '{@link #isDirty() <em>Dirty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDirty()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DIRTY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDirty() <em>Dirty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDirty()
	 * @generated
	 * @ordered
	 */
	protected boolean dirty = DIRTY_EDEFAULT;

	/**
	 * The default value of the '{@link #isDomainKey() <em>Domain Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDomainKey()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DOMAIN_KEY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDomainKey() <em>Domain Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDomainKey()
	 * @generated
	 * @ordered
	 */
	protected boolean domainKey = DOMAIN_KEY_EDEFAULT;

	/**
	 * The default value of the '{@link #isDomainDescription() <em>Domain Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDomainDescription()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DOMAIN_DESCRIPTION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDomainDescription() <em>Domain Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDomainDescription()
	 * @generated
	 * @ordered
	 */
	protected boolean domainDescription = DOMAIN_DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #isFiltering() <em>Filtering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFiltering()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FILTERING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFiltering() <em>Filtering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFiltering()
	 * @generated
	 * @ordered
	 */
	protected boolean filtering = FILTERING_EDEFAULT;

	/**
	 * The default value of the '{@link #isRangeFiltering() <em>Range Filtering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRangeFiltering()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RANGE_FILTERING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRangeFiltering() <em>Range Filtering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRangeFiltering()
	 * @generated
	 * @ordered
	 */
	protected boolean rangeFiltering = RANGE_FILTERING_EDEFAULT;

	/**
	 * The default value of the '{@link #isUniqueEntry() <em>Unique Entry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUniqueEntry()
	 * @generated
	 * @ordered
	 */
	protected static final boolean UNIQUE_ENTRY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUniqueEntry() <em>Unique Entry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUniqueEntry()
	 * @generated
	 * @ordered
	 */
	protected boolean uniqueEntry = UNIQUE_ENTRY_EDEFAULT;

	/**
	 * The default value of the '{@link #isAttributeHidden() <em>Attribute Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAttributeHidden()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ATTRIBUTE_HIDDEN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAttributeHidden() <em>Attribute Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAttributeHidden()
	 * @generated
	 * @ordered
	 */
	protected boolean attributeHidden = ATTRIBUTE_HIDDEN_EDEFAULT;

	/**
	 * The default value of the '{@link #isAttributeReadOnly() <em>Attribute Read Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAttributeReadOnly()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ATTRIBUTE_READ_ONLY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAttributeReadOnly() <em>Attribute Read Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAttributeReadOnly()
	 * @generated
	 * @ordered
	 */
	protected boolean attributeReadOnly = ATTRIBUTE_READ_ONLY_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtraStyle() <em>Extra Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtraStyle()
	 * @generated
	 * @ordered
	 */
	protected static final String EXTRA_STYLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtraStyle() <em>Extra Style</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtraStyle()
	 * @generated
	 * @ordered
	 */
	protected String extraStyle = EXTRA_STYLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDerivedGetterExpression() <em>Derived Getter Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedGetterExpression()
	 * @generated
	 * @ordered
	 */
	protected XExpression derivedGetterExpression;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected LScalarType type;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<LKeyAndValue> properties;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<LDatatypeConstraint> constraints;

	/**
	 * The default value of the '{@link #isIsGrouped() <em>Is Grouped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsGrouped()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_GROUPED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsGrouped() <em>Is Grouped</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsGrouped()
	 * @generated
	 * @ordered
	 */
	protected boolean isGrouped = IS_GROUPED_EDEFAULT;

	/**
	 * The default value of the '{@link #getGroupName() <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupName()
	 * @generated
	 * @ordered
	 */
	protected static final String GROUP_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGroupName() <em>Group Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupName()
	 * @generated
	 * @ordered
	 */
	protected String groupName = GROUP_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LDtoAbstractAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSBPDtoPackage.Literals.LDTO_ABSTRACT_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(boolean newId) {
		boolean oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUuid() {
		return uuid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUuid(boolean newUuid) {
		boolean oldUuid = uuid;
		uuid = newUuid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UUID, oldUuid, uuid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(boolean newVersion) {
		boolean oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLazy() {
		return lazy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLazy(boolean newLazy) {
		boolean oldLazy = lazy;
		lazy = newLazy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__LAZY, oldLazy, lazy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTransient() {
		return transient_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransient(boolean newTransient) {
		boolean oldTransient = transient_;
		transient_ = newTransient;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TRANSIENT, oldTransient, transient_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDerived() {
		return derived;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDerived(boolean newDerived) {
		boolean oldDerived = derived;
		derived = newDerived;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED, oldDerived, derived));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDirty() {
		return dirty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirty(boolean newDirty) {
		boolean oldDirty = dirty;
		dirty = newDirty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DIRTY, oldDirty, dirty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDomainKey() {
		return domainKey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomainKey(boolean newDomainKey) {
		boolean oldDomainKey = domainKey;
		domainKey = newDomainKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_KEY, oldDomainKey, domainKey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDomainDescription() {
		return domainDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomainDescription(boolean newDomainDescription) {
		boolean oldDomainDescription = domainDescription;
		domainDescription = newDomainDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_DESCRIPTION, oldDomainDescription, domainDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFiltering() {
		return filtering;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFiltering(boolean newFiltering) {
		boolean oldFiltering = filtering;
		filtering = newFiltering;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__FILTERING, oldFiltering, filtering));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRangeFiltering() {
		return rangeFiltering;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRangeFiltering(boolean newRangeFiltering) {
		boolean oldRangeFiltering = rangeFiltering;
		rangeFiltering = newRangeFiltering;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__RANGE_FILTERING, oldRangeFiltering, rangeFiltering));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUniqueEntry() {
		return uniqueEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniqueEntry(boolean newUniqueEntry) {
		boolean oldUniqueEntry = uniqueEntry;
		uniqueEntry = newUniqueEntry;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UNIQUE_ENTRY, oldUniqueEntry, uniqueEntry));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAttributeHidden() {
		return attributeHidden;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeHidden(boolean newAttributeHidden) {
		boolean oldAttributeHidden = attributeHidden;
		attributeHidden = newAttributeHidden;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_HIDDEN, oldAttributeHidden, attributeHidden));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAttributeReadOnly() {
		return attributeReadOnly;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeReadOnly(boolean newAttributeReadOnly) {
		boolean oldAttributeReadOnly = attributeReadOnly;
		attributeReadOnly = newAttributeReadOnly;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_READ_ONLY, oldAttributeReadOnly, attributeReadOnly));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExtraStyle() {
		return extraStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtraStyle(String newExtraStyle) {
		String oldExtraStyle = extraStyle;
		extraStyle = newExtraStyle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__EXTRA_STYLE, oldExtraStyle, extraStyle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XExpression getDerivedGetterExpression() {
		return derivedGetterExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDerivedGetterExpression(XExpression newDerivedGetterExpression, NotificationChain msgs) {
		XExpression oldDerivedGetterExpression = derivedGetterExpression;
		derivedGetterExpression = newDerivedGetterExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION, oldDerivedGetterExpression, newDerivedGetterExpression);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDerivedGetterExpression(XExpression newDerivedGetterExpression) {
		if (newDerivedGetterExpression != derivedGetterExpression) {
			NotificationChain msgs = null;
			if (derivedGetterExpression != null)
				msgs = ((InternalEObject)derivedGetterExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION, null, msgs);
			if (newDerivedGetterExpression != null)
				msgs = ((InternalEObject)newDerivedGetterExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION, null, msgs);
			msgs = basicSetDerivedGetterExpression(newDerivedGetterExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION, newDerivedGetterExpression, newDerivedGetterExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LScalarType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (LScalarType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LScalarType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(LScalarType newType) {
		LScalarType oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LKeyAndValue> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<LKeyAndValue>(LKeyAndValue.class, this, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LDatatypeConstraint> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentEList<LDatatypeConstraint>(LDatatypeConstraint.class, this, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__CONSTRAINTS);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsGrouped() {
		return isGrouped;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsGrouped(boolean newIsGrouped) {
		boolean oldIsGrouped = isGrouped;
		isGrouped = newIsGrouped;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__IS_GROUPED, oldIsGrouped, isGrouped));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroupName(String newGroupName) {
		String oldGroupName = groupName;
		groupName = newGroupName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__GROUP_NAME, oldGroupName, groupName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<LKeyAndValue> getResolvedProperties() {
		if ((this instanceof LDtoInheritedAttribute)) {
			final LDtoInheritedAttribute temp = ((LDtoInheritedAttribute) this);
			LAttribute _inheritedFeature = temp.getInheritedFeature();
			boolean _tripleEquals = (_inheritedFeature == null);
			if (_tripleEquals) {
				return Collections.<LKeyAndValue>emptyList();
			}
			EList<LKeyAndValue> _properties = temp.getInheritedFeature().getProperties();
			final ArrayList<LKeyAndValue> result = new ArrayList<LKeyAndValue>(_properties);
			result.addAll(this.getProperties());
			return result;
		}
		else {
			return this.getProperties();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVersionAttr() {
		if ((this instanceof LDtoInheritedAttribute)) {
			final LDtoInheritedAttribute temp = ((LDtoInheritedAttribute) this);
			return temp.getInheritedFeature().isVersion();
		}
		else {
			return this.isVersion();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION:
				return basicSetDerivedGetterExpression(null, msgs);
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__CONSTRAINTS:
				return ((InternalEList<?>)getConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ID:
				return isId();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UUID:
				return isUuid();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__VERSION:
				return isVersion();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__LAZY:
				return isLazy();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TRANSIENT:
				return isTransient();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED:
				return isDerived();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DIRTY:
				return isDirty();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_KEY:
				return isDomainKey();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_DESCRIPTION:
				return isDomainDescription();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__FILTERING:
				return isFiltering();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__RANGE_FILTERING:
				return isRangeFiltering();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UNIQUE_ENTRY:
				return isUniqueEntry();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_HIDDEN:
				return isAttributeHidden();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_READ_ONLY:
				return isAttributeReadOnly();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__EXTRA_STYLE:
				return getExtraStyle();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION:
				return getDerivedGetterExpression();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__PROPERTIES:
				return getProperties();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__CONSTRAINTS:
				return getConstraints();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__IS_GROUPED:
				return isIsGrouped();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__GROUP_NAME:
				return getGroupName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ID:
				setId((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UUID:
				setUuid((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__VERSION:
				setVersion((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__LAZY:
				setLazy((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TRANSIENT:
				setTransient((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED:
				setDerived((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DIRTY:
				setDirty((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_KEY:
				setDomainKey((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_DESCRIPTION:
				setDomainDescription((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__FILTERING:
				setFiltering((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__RANGE_FILTERING:
				setRangeFiltering((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UNIQUE_ENTRY:
				setUniqueEntry((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_HIDDEN:
				setAttributeHidden((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_READ_ONLY:
				setAttributeReadOnly((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__EXTRA_STYLE:
				setExtraStyle((String)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION:
				setDerivedGetterExpression((XExpression)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TYPE:
				setType((LScalarType)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__PROPERTIES:
				getProperties().clear();
				getProperties().addAll((Collection<? extends LKeyAndValue>)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection<? extends LDatatypeConstraint>)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__IS_GROUPED:
				setIsGrouped((Boolean)newValue);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__GROUP_NAME:
				setGroupName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ID:
				setId(ID_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UUID:
				setUuid(UUID_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__LAZY:
				setLazy(LAZY_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TRANSIENT:
				setTransient(TRANSIENT_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED:
				setDerived(DERIVED_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DIRTY:
				setDirty(DIRTY_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_KEY:
				setDomainKey(DOMAIN_KEY_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_DESCRIPTION:
				setDomainDescription(DOMAIN_DESCRIPTION_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__FILTERING:
				setFiltering(FILTERING_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__RANGE_FILTERING:
				setRangeFiltering(RANGE_FILTERING_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UNIQUE_ENTRY:
				setUniqueEntry(UNIQUE_ENTRY_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_HIDDEN:
				setAttributeHidden(ATTRIBUTE_HIDDEN_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_READ_ONLY:
				setAttributeReadOnly(ATTRIBUTE_READ_ONLY_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__EXTRA_STYLE:
				setExtraStyle(EXTRA_STYLE_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION:
				setDerivedGetterExpression((XExpression)null);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TYPE:
				setType((LScalarType)null);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__PROPERTIES:
				getProperties().clear();
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__CONSTRAINTS:
				getConstraints().clear();
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__IS_GROUPED:
				setIsGrouped(IS_GROUPED_EDEFAULT);
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__GROUP_NAME:
				setGroupName(GROUP_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ID:
				return id != ID_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UUID:
				return uuid != UUID_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__VERSION:
				return version != VERSION_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__LAZY:
				return lazy != LAZY_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TRANSIENT:
				return transient_ != TRANSIENT_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED:
				return derived != DERIVED_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DIRTY:
				return dirty != DIRTY_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_KEY:
				return domainKey != DOMAIN_KEY_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_DESCRIPTION:
				return domainDescription != DOMAIN_DESCRIPTION_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__FILTERING:
				return filtering != FILTERING_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__RANGE_FILTERING:
				return rangeFiltering != RANGE_FILTERING_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UNIQUE_ENTRY:
				return uniqueEntry != UNIQUE_ENTRY_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_HIDDEN:
				return attributeHidden != ATTRIBUTE_HIDDEN_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_READ_ONLY:
				return attributeReadOnly != ATTRIBUTE_READ_ONLY_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__EXTRA_STYLE:
				return EXTRA_STYLE_EDEFAULT == null ? extraStyle != null : !EXTRA_STYLE_EDEFAULT.equals(extraStyle);
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION:
				return derivedGetterExpression != null;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TYPE:
				return type != null;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__CONSTRAINTS:
				return constraints != null && !constraints.isEmpty();
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__IS_GROUPED:
				return isGrouped != IS_GROUPED_EDEFAULT;
			case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__GROUP_NAME:
				return GROUP_NAME_EDEFAULT == null ? groupName != null : !GROUP_NAME_EDEFAULT.equals(groupName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == LAttribute.class) {
			switch (derivedFeatureID) {
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ID: return OSBPTypesPackage.LATTRIBUTE__ID;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UUID: return OSBPTypesPackage.LATTRIBUTE__UUID;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__VERSION: return OSBPTypesPackage.LATTRIBUTE__VERSION;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__LAZY: return OSBPTypesPackage.LATTRIBUTE__LAZY;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TRANSIENT: return OSBPTypesPackage.LATTRIBUTE__TRANSIENT;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED: return OSBPTypesPackage.LATTRIBUTE__DERIVED;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DIRTY: return OSBPTypesPackage.LATTRIBUTE__DIRTY;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_KEY: return OSBPTypesPackage.LATTRIBUTE__DOMAIN_KEY;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_DESCRIPTION: return OSBPTypesPackage.LATTRIBUTE__DOMAIN_DESCRIPTION;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__FILTERING: return OSBPTypesPackage.LATTRIBUTE__FILTERING;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__RANGE_FILTERING: return OSBPTypesPackage.LATTRIBUTE__RANGE_FILTERING;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UNIQUE_ENTRY: return OSBPTypesPackage.LATTRIBUTE__UNIQUE_ENTRY;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_HIDDEN: return OSBPTypesPackage.LATTRIBUTE__ATTRIBUTE_HIDDEN;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_READ_ONLY: return OSBPTypesPackage.LATTRIBUTE__ATTRIBUTE_READ_ONLY;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__EXTRA_STYLE: return OSBPTypesPackage.LATTRIBUTE__EXTRA_STYLE;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION: return OSBPTypesPackage.LATTRIBUTE__DERIVED_GETTER_EXPRESSION;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TYPE: return OSBPTypesPackage.LATTRIBUTE__TYPE;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__PROPERTIES: return OSBPTypesPackage.LATTRIBUTE__PROPERTIES;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__CONSTRAINTS: return OSBPTypesPackage.LATTRIBUTE__CONSTRAINTS;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__IS_GROUPED: return OSBPTypesPackage.LATTRIBUTE__IS_GROUPED;
				case OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__GROUP_NAME: return OSBPTypesPackage.LATTRIBUTE__GROUP_NAME;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == LAttribute.class) {
			switch (baseFeatureID) {
				case OSBPTypesPackage.LATTRIBUTE__ID: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ID;
				case OSBPTypesPackage.LATTRIBUTE__UUID: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UUID;
				case OSBPTypesPackage.LATTRIBUTE__VERSION: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__VERSION;
				case OSBPTypesPackage.LATTRIBUTE__LAZY: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__LAZY;
				case OSBPTypesPackage.LATTRIBUTE__TRANSIENT: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TRANSIENT;
				case OSBPTypesPackage.LATTRIBUTE__DERIVED: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED;
				case OSBPTypesPackage.LATTRIBUTE__DIRTY: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DIRTY;
				case OSBPTypesPackage.LATTRIBUTE__DOMAIN_KEY: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_KEY;
				case OSBPTypesPackage.LATTRIBUTE__DOMAIN_DESCRIPTION: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DOMAIN_DESCRIPTION;
				case OSBPTypesPackage.LATTRIBUTE__FILTERING: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__FILTERING;
				case OSBPTypesPackage.LATTRIBUTE__RANGE_FILTERING: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__RANGE_FILTERING;
				case OSBPTypesPackage.LATTRIBUTE__UNIQUE_ENTRY: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__UNIQUE_ENTRY;
				case OSBPTypesPackage.LATTRIBUTE__ATTRIBUTE_HIDDEN: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_HIDDEN;
				case OSBPTypesPackage.LATTRIBUTE__ATTRIBUTE_READ_ONLY: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__ATTRIBUTE_READ_ONLY;
				case OSBPTypesPackage.LATTRIBUTE__EXTRA_STYLE: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__EXTRA_STYLE;
				case OSBPTypesPackage.LATTRIBUTE__DERIVED_GETTER_EXPRESSION: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__DERIVED_GETTER_EXPRESSION;
				case OSBPTypesPackage.LATTRIBUTE__TYPE: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__TYPE;
				case OSBPTypesPackage.LATTRIBUTE__PROPERTIES: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__PROPERTIES;
				case OSBPTypesPackage.LATTRIBUTE__CONSTRAINTS: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__CONSTRAINTS;
				case OSBPTypesPackage.LATTRIBUTE__IS_GROUPED: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__IS_GROUPED;
				case OSBPTypesPackage.LATTRIBUTE__GROUP_NAME: return OSBPDtoPackage.LDTO_ABSTRACT_ATTRIBUTE__GROUP_NAME;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", uuid: ");
		result.append(uuid);
		result.append(", version: ");
		result.append(version);
		result.append(", lazy: ");
		result.append(lazy);
		result.append(", transient: ");
		result.append(transient_);
		result.append(", derived: ");
		result.append(derived);
		result.append(", dirty: ");
		result.append(dirty);
		result.append(", domainKey: ");
		result.append(domainKey);
		result.append(", domainDescription: ");
		result.append(domainDescription);
		result.append(", filtering: ");
		result.append(filtering);
		result.append(", rangeFiltering: ");
		result.append(rangeFiltering);
		result.append(", uniqueEntry: ");
		result.append(uniqueEntry);
		result.append(", attributeHidden: ");
		result.append(attributeHidden);
		result.append(", attributeReadOnly: ");
		result.append(attributeReadOnly);
		result.append(", extraStyle: ");
		result.append(extraStyle);
		result.append(", isGrouped: ");
		result.append(isGrouped);
		result.append(", groupName: ");
		result.append(groupName);
		result.append(')');
		return result.toString();
	}

} //LDtoAbstractAttributeImpl
