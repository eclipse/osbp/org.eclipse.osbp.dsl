/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto;

import java.util.List;

import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LDto Abstract Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage#getLDtoAbstractAttribute()
 * @generated
 */
public interface LDtoAbstractAttribute extends LDtoFeature, LAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	List<LKeyAndValue> getResolvedProperties();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	boolean isVersionAttr();

} // LDtoAbstractAttribute
