/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto.impl;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue;

import org.eclipse.osbp.dsl.semantic.dto.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OSBPDtoFactoryImpl extends EFactoryImpl implements OSBPDtoFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OSBPDtoFactory init() {
		try {
			OSBPDtoFactory theOSBPDtoFactory = (OSBPDtoFactory)EPackage.Registry.INSTANCE.getEFactory(OSBPDtoPackage.eNS_URI);
			if (theOSBPDtoFactory != null) {
				return theOSBPDtoFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OSBPDtoFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPDtoFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OSBPDtoPackage.LDTO_MODEL: return createLDtoModel();
			case OSBPDtoPackage.LDTO: return createLDto();
			case OSBPDtoPackage.LAUTO_INHERIT_DTO: return createLAutoInheritDto();
			case OSBPDtoPackage.LDTO_MAPPER: return createLDtoMapper();
			case OSBPDtoPackage.LDTO_FEATURE: return createLDtoFeature();
			case OSBPDtoPackage.LDTO_INHERITED_ATTRIBUTE: return createLDtoInheritedAttribute();
			case OSBPDtoPackage.LDTO_ATTRIBUTE: return createLDtoAttribute();
			case OSBPDtoPackage.LDTO_INHERITED_REFERENCE: return createLDtoInheritedReference();
			case OSBPDtoPackage.LDTO_REFERENCE: return createLDtoReference();
			case OSBPDtoPackage.LDTO_OPERATION: return createLDtoOperation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OSBPDtoPackage.DTO_FEATURE_LIST:
				return createDtoFeatureListFromString(eDataType, initialValue);
			case OSBPDtoPackage.OPERATIONS_LIST:
				return createOperationsListFromString(eDataType, initialValue);
			case OSBPDtoPackage.DTO_REFERENCE_LIST:
				return createDtoReferenceListFromString(eDataType, initialValue);
			case OSBPDtoPackage.DTO_ATTRIBUTE_LIST:
				return createDtoAttributeListFromString(eDataType, initialValue);
			case OSBPDtoPackage.DTO_ABSTRACT_ATTRIBUTE_LIST:
				return createDtoAbstractAttributeListFromString(eDataType, initialValue);
			case OSBPDtoPackage.INTERNAL_EOBJECT:
				return createInternalEObjectFromString(eDataType, initialValue);
			case OSBPDtoPackage.PROPERTIES_LIST:
				return createPropertiesListFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OSBPDtoPackage.DTO_FEATURE_LIST:
				return convertDtoFeatureListToString(eDataType, instanceValue);
			case OSBPDtoPackage.OPERATIONS_LIST:
				return convertOperationsListToString(eDataType, instanceValue);
			case OSBPDtoPackage.DTO_REFERENCE_LIST:
				return convertDtoReferenceListToString(eDataType, instanceValue);
			case OSBPDtoPackage.DTO_ATTRIBUTE_LIST:
				return convertDtoAttributeListToString(eDataType, instanceValue);
			case OSBPDtoPackage.DTO_ABSTRACT_ATTRIBUTE_LIST:
				return convertDtoAbstractAttributeListToString(eDataType, instanceValue);
			case OSBPDtoPackage.INTERNAL_EOBJECT:
				return convertInternalEObjectToString(eDataType, instanceValue);
			case OSBPDtoPackage.PROPERTIES_LIST:
				return convertPropertiesListToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtoModel createLDtoModel() {
		LDtoModelImpl lDtoModel = new LDtoModelImpl();
		return lDtoModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDto createLDto() {
		LDtoImpl lDto = new LDtoImpl();
		return lDto;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LAutoInheritDto createLAutoInheritDto() {
		LAutoInheritDtoImpl lAutoInheritDto = new LAutoInheritDtoImpl();
		return lAutoInheritDto;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtoMapper createLDtoMapper() {
		LDtoMapperImpl lDtoMapper = new LDtoMapperImpl();
		return lDtoMapper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtoFeature createLDtoFeature() {
		LDtoFeatureImpl lDtoFeature = new LDtoFeatureImpl();
		return lDtoFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtoInheritedAttribute createLDtoInheritedAttribute() {
		LDtoInheritedAttributeImpl lDtoInheritedAttribute = new LDtoInheritedAttributeImpl();
		return lDtoInheritedAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtoAttribute createLDtoAttribute() {
		LDtoAttributeImpl lDtoAttribute = new LDtoAttributeImpl();
		return lDtoAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtoInheritedReference createLDtoInheritedReference() {
		LDtoInheritedReferenceImpl lDtoInheritedReference = new LDtoInheritedReferenceImpl();
		return lDtoInheritedReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtoReference createLDtoReference() {
		LDtoReferenceImpl lDtoReference = new LDtoReferenceImpl();
		return lDtoReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtoOperation createLDtoOperation() {
		LDtoOperationImpl lDtoOperation = new LDtoOperationImpl();
		return lDtoOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LDtoFeature> createDtoFeatureListFromString(EDataType eDataType, String initialValue) {
		return (List<LDtoFeature>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDtoFeatureListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LDtoOperation> createOperationsListFromString(EDataType eDataType, String initialValue) {
		return (List<LDtoOperation>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperationsListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LDtoAbstractReference> createDtoReferenceListFromString(EDataType eDataType, String initialValue) {
		return (List<LDtoAbstractReference>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDtoReferenceListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LDtoAbstractAttribute> createDtoAttributeListFromString(EDataType eDataType, String initialValue) {
		return (List<LDtoAbstractAttribute>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDtoAttributeListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LDtoAbstractAttribute> createDtoAbstractAttributeListFromString(EDataType eDataType, String initialValue) {
		return (List<LDtoAbstractAttribute>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDtoAbstractAttributeListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalEObject createInternalEObjectFromString(EDataType eDataType, String initialValue) {
		return (InternalEObject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInternalEObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<LKeyAndValue> createPropertiesListFromString(EDataType eDataType, String initialValue) {
		return (List<LKeyAndValue>)super.createFromString(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPropertiesListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPDtoPackage getOSBPDtoPackage() {
		return (OSBPDtoPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OSBPDtoPackage getPackage() {
		return OSBPDtoPackage.eINSTANCE;
	}

} //OSBPDtoFactoryImpl
