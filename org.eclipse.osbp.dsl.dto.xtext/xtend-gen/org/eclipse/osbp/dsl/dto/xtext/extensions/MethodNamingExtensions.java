/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.extensions;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import org.eclipse.osbp.dsl.common.xtext.extensions.NamingExtensions;
import org.eclipse.osbp.dsl.dto.xtext.extensions.DtoModelExtensions;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoFeature;
import org.eclipse.osbp.dsl.semantic.dto.LDtoMapper;
import org.eclipse.osbp.dsl.semantic.dto.util.NamingConventionsUtil;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.XExpression;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class MethodNamingExtensions extends NamingExtensions {
  @Inject
  @Extension
  private DtoModelExtensions _dtoModelExtensions;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  public String toMapPropertyToDto(final LDtoFeature prop) {
    String _name = this._dtoModelExtensions.toName(prop);
    return ("toDto_" + _name);
  }
  
  public String toMapPropertyToEntity(final LDtoFeature prop) {
    String _name = this._dtoModelExtensions.toName(prop);
    return ("toEntity_" + _name);
  }
  
  public String toMapperFieldName(final LDtoFeature prop) {
    String _name = this._dtoModelExtensions.toName(prop);
    return (_name + "Mapper");
  }
  
  public String toMapperName(final LType dto) {
    String _xblockexpression = null;
    {
      if ((Objects.equal(dto, null) || Objects.equal(this._dtoModelExtensions.toName(dto), null))) {
        return "setMISSING_NAME";
      }
      _xblockexpression = this.toMapperName(this._dtoModelExtensions.toName(dto));
    }
    return _xblockexpression;
  }
  
  public String toMapperName(final String name) {
    return (name + "Mapper");
  }
  
  public String toFqnMapperName(final LType dto) {
    String _xblockexpression = null;
    {
      if ((Objects.equal(dto, null) || Objects.equal(this._dtoModelExtensions.toName(dto), null))) {
        return "setMISSING_NAME";
      }
      String _mapperNamePackage = this.toMapperNamePackage(dto);
      String _plus = (_mapperNamePackage + ".");
      String _mapperName = this.toMapperName(dto);
      _xblockexpression = (_plus + _mapperName);
    }
    return _xblockexpression;
  }
  
  public String toMapperNamePackage(final LType dto) {
    String _xblockexpression = null;
    {
      if ((Objects.equal(dto, null) || Objects.equal(this._iQualifiedNameProvider.getFullyQualifiedName(dto), null))) {
        return "setMISSING_NAME";
      }
      _xblockexpression = this._iQualifiedNameProvider.getFullyQualifiedName(dto).skipLast(1).append("mapper").toString();
    }
    return _xblockexpression;
  }
  
  public String toDTOBeanFullyQualifiedName(final LType type) {
    LPackage _package = this._dtoModelExtensions.getPackage(type);
    final LTypedPackage pkg = ((LTypedPackage) _package);
    if ((type instanceof LDto)) {
      String _name = pkg.getName();
      String _plus = (_name + ".");
      String _dTOBeanSimpleName = this.toDTOBeanSimpleName(type);
      return (_plus + _dTOBeanSimpleName);
    } else {
      String _dtoPackageName = this._dtoModelExtensions.toDtoPackageName(pkg);
      String _plus_1 = (_dtoPackageName + ".");
      String _dTOBeanSimpleName_1 = this.toDTOBeanSimpleName(type);
      return (_plus_1 + _dTOBeanSimpleName_1);
    }
  }
  
  public String toDTOBeanSimpleName(final LType type) {
    String _name = null;
    if (type!=null) {
      _name=type.getName();
    }
    final String name = _name;
    if ((type instanceof LDto)) {
      return name;
    } else {
      return this.toDTOBeanSimpleName(name);
    }
  }
  
  public String toDTOEnumFullyQualifiedName(final LType type) {
    boolean _contains = type.eResource().getURI().toString().contains("datatype");
    if (_contains) {
      return this._dtoModelExtensions.toQualifiedName(type);
    } else {
      LPackage _package = this._dtoModelExtensions.getPackage(type);
      final LTypedPackage pkg = ((LTypedPackage) _package);
      if ((type instanceof LDto)) {
        String _name = pkg.getName();
        String _plus = (_name + ".");
        String _dTOEnumSimpleName = this.toDTOEnumSimpleName(type);
        return (_plus + _dTOEnumSimpleName);
      } else {
        String _dtoPackageName = this._dtoModelExtensions.toDtoPackageName(pkg);
        String _plus_1 = (_dtoPackageName + ".");
        String _dTOEnumSimpleName_1 = this.toDTOEnumSimpleName(type);
        return (_plus_1 + _dTOEnumSimpleName_1);
      }
    }
  }
  
  public String toDTOStateClassFullyQualifiedName(final LType type) {
    LPackage _package = this._dtoModelExtensions.getPackage(type);
    final LTypedPackage pkg = ((LTypedPackage) _package);
    String _name = pkg.getName();
    String _plus = (_name + ".");
    String _name_1 = null;
    if (type!=null) {
      _name_1=type.getName();
    }
    return (_plus + _name_1);
  }
  
  public String toDTOEnumSimpleName(final LType type) {
    String _name = null;
    if (type!=null) {
      _name=type.getName();
    }
    return _name;
  }
  
  public String toDTOBeanSimpleName(final String name) {
    return NamingConventionsUtil.toDtoName(name);
  }
  
  public XExpression toMapToEntityExpression(final LDtoFeature prop) {
    Object _xifexpression = null;
    LDtoMapper _mapper = prop.getMapper();
    boolean _notEquals = (!Objects.equal(_mapper, null));
    if (_notEquals) {
      return prop.getMapper().getFromDTO();
    } else {
      _xifexpression = null;
    }
    return ((XExpression)_xifexpression);
  }
  
  public XExpression toMapToDtoExpression(final LDtoFeature prop) {
    Object _xifexpression = null;
    LDtoMapper _mapper = prop.getMapper();
    boolean _notEquals = (!Objects.equal(_mapper, null));
    if (_notEquals) {
      return prop.getMapper().getToDTO();
    } else {
      _xifexpression = null;
    }
    return ((XExpression)_xifexpression);
  }
}
