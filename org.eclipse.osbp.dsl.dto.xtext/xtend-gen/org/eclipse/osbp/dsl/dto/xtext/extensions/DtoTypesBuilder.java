/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.extensions;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.common.xtext.extensions.TreeAppendableExtensions;
import org.eclipse.osbp.dsl.common.xtext.jvmmodel.CommonTypesBuilder;
import org.eclipse.osbp.dsl.dto.lib.ICrossReference;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;
import org.eclipse.osbp.dsl.dto.xtext.extensions.AnnotationExtension;
import org.eclipse.osbp.dsl.dto.xtext.extensions.DtoModelExtensions;
import org.eclipse.osbp.dsl.dto.xtext.extensions.MethodNamingExtensions;
import org.eclipse.osbp.dsl.dto.xtext.jvmmodel.AnnotationCompiler;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LStateClass;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.dto.LAutoInheritDto;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoFeature;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoMapper;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LOperation;
import org.eclipse.osbp.runtime.common.annotations.DomainDescription;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtend2.lib.StringConcatenationClient;
import org.eclipse.xtext.common.types.JvmAnnotationReference;
import org.eclipse.xtext.common.types.JvmField;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmParameterizedTypeReference;
import org.eclipse.xtext.common.types.JvmTypeParameter;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.JvmVisibility;
import org.eclipse.xtext.common.types.TypesFactory;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.XExpression;
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociations;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociator;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class DtoTypesBuilder extends CommonTypesBuilder {
  @Inject
  @Extension
  private DtoModelExtensions _dtoModelExtensions;
  
  @Inject
  @Extension
  private MethodNamingExtensions _methodNamingExtensions;
  
  @Inject
  @Extension
  private TreeAppendableExtensions _treeAppendableExtensions;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private AnnotationExtension _annotationExtension;
  
  @Inject
  private IJvmModelAssociator associator;
  
  @Inject
  private AnnotationCompiler annotationCompiler;
  
  @Inject
  private TypesFactory typesFactory;
  
  @Inject
  private TypeReferences references;
  
  public String htmlCode(final CharSequence s) {
    return "<code>".concat(String.valueOf(s)).concat("</code>");
  }
  
  public JvmField toDiposeField(final LDto sourceElement) {
    final JvmField field = this.toPrimitiveTypeField(sourceElement, "disposed", Boolean.TYPE);
    this.<JvmField>associate(sourceElement, field);
    this.annotationCompiler.addDisposeFieldAnnotation(sourceElement, field);
    return field;
  }
  
  public JvmOperation toDispose(final LDto lClass) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setReturnType(this.references.getTypeForName(Void.TYPE, lClass));
      op.setSimpleName("dispose");
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Calling dispose will destroy that instance. The internal state will be ");
      _builder.newLine();
      _builder.append("set to \'disposed\' and methods of that object must not be used anymore. ");
      _builder.newLine();
      _builder.append("Each call will result in runtime exceptions.<br/>");
      _builder.newLine();
      _builder.append("If this object keeps composition containments, these will be disposed too. ");
      _builder.newLine();
      _builder.append("So the whole composition containment tree will be disposed on calling this method.");
      this.setDocumentation(op, _builder);
      this.annotationCompiler.addDisposeFieldAnnotation(lClass, op);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        boolean _equals = Objects.equal(it, null);
        if (_equals) {
          return;
        }
        final ITreeAppendable p = it.trace(lClass);
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("if (isDisposed()) {");
        _builder_1.newLine();
        _builder_1.append("  ");
        _builder_1.append("return;");
        _builder_1.newLine();
        _builder_1.append("}");
        _builder_1.newLine();
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_1);
        final Function1<LDtoFeature, Boolean> _function_1 = (LDtoFeature it_1) -> {
          return Boolean.valueOf(((it_1 instanceof LDtoAbstractReference) && ((LDtoAbstractReference) it_1).isCascading()));
        };
        final Iterable<LDtoFeature> compositionContainmentProps = IterableExtensions.<LDtoFeature>filter(lClass.getFeatures(), _function_1);
        boolean _isEmpty = IterableExtensions.isEmpty(compositionContainmentProps);
        boolean _not = (!_isEmpty);
        if (_not) {
          ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, "try ");
          this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, "{");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, "// Dispose all the composition references.\n");
          for (final LDtoFeature prop : compositionContainmentProps) {
            {
              final String fieldRef = "this.".concat(StringExtensions.toFirstLower(this._dtoModelExtensions.toName(prop)));
              final String typeName = this._dtoModelExtensions.toTypeName(prop);
              final String typeVar = StringExtensions.toFirstLower(typeName);
              boolean _isToMany = this._dtoModelExtensions.isToMany(prop);
              if (_isToMany) {
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("if (");
                _builder_2.append(fieldRef);
                _builder_2.append(" != null) {");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("  ");
                _builder_2.append("for (");
                _builder_2.append(typeName, "  ");
                _builder_2.append(" ");
                _builder_2.append(typeVar, "  ");
                _builder_2.append(" : ");
                _builder_2.append(fieldRef, "  ");
                _builder_2.append(") {");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("    ");
                _builder_2.append(typeVar, "    ");
                _builder_2.append(".dispose();");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("  ");
                _builder_2.append("}");
                _builder_2.newLine();
                _builder_2.append("  ");
                _builder_2.append(fieldRef, "  ");
                _builder_2.append(" = null;");
                _builder_2.newLineIfNotEmpty();
                _builder_2.append("}");
                _builder_2.newLine();
                this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_2);
              } else {
                StringConcatenation _builder_3 = new StringConcatenation();
                _builder_3.append("if (");
                _builder_3.append(fieldRef);
                _builder_3.append(" != null) {");
                _builder_3.newLineIfNotEmpty();
                _builder_3.append("  ");
                _builder_3.append(fieldRef, "  ");
                _builder_3.append(".dispose();");
                _builder_3.newLineIfNotEmpty();
                _builder_3.append("  ");
                _builder_3.append(fieldRef, "  ");
                _builder_3.append(" = null;");
                _builder_3.newLineIfNotEmpty();
                _builder_3.append("}");
                _builder_3.newLine();
                this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder_3);
              }
            }
          }
          this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
          this._treeAppendableExtensions.operator_tripleGreaterThan(p, "finally {");
        }
        LDto _superType = lClass.getSuperType();
        boolean _notEquals = (!Objects.equal(_superType, null));
        if (_notEquals) {
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, "super.dispose();");
        } else {
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, "firePropertyChange(\"disposed\", this.disposed, this.disposed = true);");
        }
        boolean _isEmpty_1 = IterableExtensions.isEmpty(compositionContainmentProps);
        boolean _not_1 = (!_isEmpty_1);
        if (_not_1) {
          this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        }
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(lClass, op);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toMethod(final LOperation sourceElement, final String name, final JvmTypeReference returnType, final Procedure1<? super JvmOperation> initializer) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setSimpleName(name);
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setReturnType(this.cloneWithProxies(returnType));
      this.annotationCompiler.processAnnotation(sourceElement, op);
      this.<JvmOperation>associate(sourceElement, op);
      _xblockexpression = this.<JvmOperation>initializeSafely(op, initializer);
    }
    return _xblockexpression;
  }
  
  protected JvmOperation _toSetter(final LDtoAbstractAttribute prop) {
    final String paramName = this._methodNamingExtensions.toMethodParamName(prop);
    final JvmTypeReference typeRef = this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toSetterName(prop));
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, typeRef);
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Sets the <code>");
    _builder.append(paramName);
    _builder.append("</code> property to this instance.");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("@param ");
    _builder.append(paramName);
    _builder.append(" - the property");
    _builder.newLineIfNotEmpty();
    _builder.append("@throws RuntimeException if instance is <code>disposed</code>");
    _builder.newLine();
    this.setDocumentation(op, _builder);
    boolean _isToMany = this._dtoModelExtensions.isToMany(prop);
    boolean _not = (!_isToMany);
    if (_not) {
      boolean _isBean = this._dtoModelExtensions.isBean(this._dtoModelExtensions.toRawType(prop));
      if (_isBean) {
        StringConcatenationClient _client = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("// ensure that embedded beans will notify their parent about changes");
            _builder.newLine();
            _builder.append("// so their dirty state can be handled properly");
            _builder.newLine();
            _builder.append("if (this.");
            _builder.append(paramName);
            _builder.append(" != null) {");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("this.");
            _builder.append(paramName, "\t");
            _builder.append(".removePropertyChangeListener(this);");
            _builder.newLineIfNotEmpty();
            _builder.append("}");
            _builder.newLine();
            _builder.newLine();
            _builder.append("firePropertyChange(\"");
            _builder.append(paramName);
            _builder.append("\", this.");
            _builder.append(paramName);
            _builder.append(", this.");
            _builder.append(paramName);
            _builder.append(" = ");
            _builder.append(paramName);
            _builder.append(" );");
            _builder.newLineIfNotEmpty();
            _builder.newLine();
            _builder.append("if (this.");
            _builder.append(paramName);
            _builder.append(" != null) {");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("this.");
            _builder.append(paramName, "\t");
            _builder.append(".addPropertyChangeListener(this);");
            _builder.newLineIfNotEmpty();
            _builder.append("}");
            _builder.newLine();
          }
        };
        this.setBody(op, _client);
      } else {
        StringConcatenationClient _client_1 = new StringConcatenationClient() {
          @Override
          protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
            _builder.append("firePropertyChange(\"");
            _builder.append(paramName);
            _builder.append("\", this.");
            _builder.append(paramName);
            _builder.append(", this.");
            _builder.append(paramName);
            _builder.append(" = ");
            _builder.append(paramName);
            _builder.append(" );");
            _builder.newLineIfNotEmpty();
            {
              boolean _isIDorUUID = DtoTypesBuilder.this._dtoModelExtensions.isIDorUUID(prop);
              if (_isIDorUUID) {
                _builder.append("\t\t\t\t");
                _builder.append("installLazyCollections();");
                _builder.newLine();
              }
            }
          }
        };
        this.setBody(op, _client_1);
      }
    } else {
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        boolean _equals = Objects.equal(it, null);
        if (_equals) {
          return;
        }
        final ITreeAppendable p = it.trace(prop);
        String _checkDisposedCall = this.toCheckDisposedCall(prop);
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _checkDisposedCall);
        String _name = this._dtoModelExtensions.toName(prop);
        final String fieldRef = CommonTypesBuilder.operator_plus("this.", _name);
        String _simpleName = this._dtoModelExtensions.toDtoTypeReference(prop).getSimpleName();
        String _plus = CommonTypesBuilder.operator_plus("for (", _simpleName);
        String _plus_1 = CommonTypesBuilder.operator_plus(_plus, " dto : ");
        String _collectionInternalGetterName = this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, _collectionInternalGetterName);
        String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, "().toArray(new ");
        String _simpleName_1 = this._dtoModelExtensions.toDtoTypeReference(prop).getSimpleName();
        String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, _simpleName_1);
        String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, "[");
        String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, fieldRef);
        String _plus_7 = CommonTypesBuilder.operator_plus(_plus_6, ".size()])) ");
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_7);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, "{");
        String _collectionRemoverName = this._methodNamingExtensions.toCollectionRemoverName(prop);
        String _plus_8 = CommonTypesBuilder.operator_plus(_collectionRemoverName, "(dto);");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_8);
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        String _plus_9 = CommonTypesBuilder.operator_plus("if(", paramName);
        String _plus_10 = CommonTypesBuilder.operator_plus(_plus_9, " == null)");
        ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_10);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan_1, "{");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, "return;");
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        String _simpleName_2 = this._dtoModelExtensions.toDtoTypeReference(prop).getSimpleName();
        String _plus_11 = CommonTypesBuilder.operator_plus("for (", _simpleName_2);
        String _plus_12 = CommonTypesBuilder.operator_plus(_plus_11, " dto : ");
        String _plus_13 = CommonTypesBuilder.operator_plus(_plus_12, paramName);
        String _plus_14 = CommonTypesBuilder.operator_plus(_plus_13, ") ");
        ITreeAppendable _doubleGreaterThan_2 = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_14);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan_2, "{");
        String _collectionAdderName = this._methodNamingExtensions.toCollectionAdderName(prop);
        String _plus_15 = CommonTypesBuilder.operator_plus(_collectionAdderName, "(dto);");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_15);
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
      };
      this.setBody(op, _function);
    }
    return this.<JvmOperation>associate(prop, op);
  }
  
  @Override
  public String toSetDirtyStatement(final LClass sourceElement) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("firePropertyChange(\"dirty\", this.dirty, this.dirty = dirty );");
    return _builder.toString();
  }
  
  /**
   * Creates a new UUIDHist id and updates the validFrom.
   */
  public JvmOperation toNewIdVersion(final LType type, final String idProp) {
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, type));
    op.setSimpleName("newIdVersion");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Creates a copy of this.id type UUIDHist and sets a new validFrom.");
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("this.id = ");
        _builder.append(idProp);
        _builder.append(".newVersion();");
        _builder.newLineIfNotEmpty();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(type, op);
  }
  
  public JvmOperation toVersionSetter(final LDtoAbstractAttribute prop) {
    final String paramName = this._methodNamingExtensions.toMethodParamName(prop);
    final JvmTypeReference typeRef = this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toSetterName(prop));
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, typeRef);
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Sets the <code>");
    _builder.append(paramName);
    _builder.append("</code> property to this instance.");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("@param ");
    _builder.append(paramName);
    _builder.append(" - the property");
    _builder.newLineIfNotEmpty();
    _builder.append("@throws RuntimeException if instance is <code>disposed</code>");
    _builder.newLine();
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("firePropertyChange(\"");
        _builder.append(paramName);
        _builder.append("\", this.");
        _builder.append(paramName);
        _builder.append(", this.");
        _builder.append(paramName);
        _builder.append(" = ");
        _builder.append(paramName);
        _builder.append(" );");
        _builder.newLineIfNotEmpty();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  protected JvmOperation _toSetter(final LDtoAbstractReference prop) {
    final String paramName = this._methodNamingExtensions.toMethodParamName(prop);
    final JvmTypeReference typeRef = this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(prop);
    final LFeature opposite = this._dtoModelExtensions.opposite(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toSetterName(prop));
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, typeRef);
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Sets the <code>");
    _builder.append(paramName);
    _builder.append("</code> property to this instance.");
    _builder.newLineIfNotEmpty();
    {
      boolean _notEquals = (!Objects.equal(opposite, null));
      if (_notEquals) {
        _builder.append("Since the reference has an opposite reference, the opposite <code>");
        String _typeName = this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName);
        _builder.append("#");
        _builder.newLineIfNotEmpty();
        String _firstLower = StringExtensions.toFirstLower(opposite.getName());
        _builder.append(_firstLower);
        _builder.append("</code> of the <code>");
        _builder.append(paramName);
        _builder.append("</code> will be handled automatically and no ");
        _builder.newLineIfNotEmpty();
        _builder.append("further coding is required to keep them in sync.<p>");
        _builder.newLine();
        _builder.append("See {@link ");
        String _typeName_1 = this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName_1);
        _builder.append("#");
        String _setterName = this._methodNamingExtensions.toSetterName(opposite);
        _builder.append(_setterName);
        _builder.append("(");
        String _typeName_2 = this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName_2);
        _builder.append(")");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("@param ");
    _builder.append(paramName);
    _builder.append(" - the property");
    _builder.newLineIfNotEmpty();
    _builder.append("@throws RuntimeException if instance is <code>disposed</code>");
    _builder.newLine();
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("checkDisposed();");
        _builder.newLine();
        {
          boolean _equals = Objects.equal(opposite, null);
          if (_equals) {
            {
              boolean _isToMany = DtoTypesBuilder.this._dtoModelExtensions.isToMany(prop);
              boolean _not = (!_isToMany);
              if (_not) {
                _builder.append("firePropertyChange(\"");
                _builder.append(paramName);
                _builder.append("\", this.");
                _builder.append(paramName);
                _builder.append(", this.");
                _builder.append(paramName);
                _builder.append(" = ");
                _builder.append(paramName);
                _builder.append(");");
                _builder.newLineIfNotEmpty();
              } else {
                _builder.append("for (");
                String _simpleName = DtoTypesBuilder.this._dtoModelExtensions.toDtoTypeReference(prop).getSimpleName();
                _builder.append(_simpleName);
                _builder.append(" dto : ");
                String _collectionInternalGetterName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
                _builder.append(_collectionInternalGetterName);
                _builder.append("().toArray(new ");
                String _simpleName_1 = DtoTypesBuilder.this._dtoModelExtensions.toDtoTypeReference(prop).getSimpleName();
                _builder.append(_simpleName_1);
                _builder.append("[this.");
                String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
                _builder.append(_name);
                _builder.append(".size()])) {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                String _collectionRemoverName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionRemoverName(prop);
                _builder.append(_collectionRemoverName, "\t");
                _builder.append("(dto);");
                _builder.newLineIfNotEmpty();
                _builder.append("}");
                _builder.newLine();
                _builder.newLine();
                _builder.append("if(");
                _builder.append(paramName);
                _builder.append(" == null) {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("return;");
                _builder.newLine();
                _builder.append("}");
                _builder.newLine();
                _builder.newLine();
                _builder.append("for (");
                String _simpleName_2 = DtoTypesBuilder.this._dtoModelExtensions.toDtoTypeReference(prop).getSimpleName();
                _builder.append(_simpleName_2);
                _builder.append(" dto : ");
                _builder.append(paramName);
                _builder.append(") {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                String _collectionAdderName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionAdderName(prop);
                _builder.append(_collectionAdderName, "\t");
                _builder.append("(dto);");
                _builder.newLineIfNotEmpty();
                _builder.append("}");
                _builder.newLine();
              }
            }
          } else {
            {
              boolean _isToMany_1 = DtoTypesBuilder.this._dtoModelExtensions.isToMany(prop);
              boolean _not_1 = (!_isToMany_1);
              if (_not_1) {
                _builder.append("if (this.");
                String _name_1 = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
                _builder.append(_name_1);
                _builder.append(" != null) {");
                _builder.newLineIfNotEmpty();
                {
                  boolean _isToMany_2 = DtoTypesBuilder.this._dtoModelExtensions.isToMany(opposite);
                  if (_isToMany_2) {
                    _builder.append("\t");
                    _builder.append("this.");
                    String _name_2 = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
                    _builder.append(_name_2, "\t");
                    _builder.append(".");
                    String _collectionInternalRemoverName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalRemoverName(opposite);
                    _builder.append(_collectionInternalRemoverName, "\t");
                    _builder.append("(this);");
                    _builder.newLineIfNotEmpty();
                  } else {
                    _builder.append("\t");
                    _builder.append("this.");
                    String _name_3 = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
                    _builder.append(_name_3, "\t");
                    _builder.append(".");
                    String _internalSetterName = DtoTypesBuilder.this._methodNamingExtensions.toInternalSetterName(opposite);
                    _builder.append(_internalSetterName, "\t");
                    _builder.append("(null);");
                    _builder.newLineIfNotEmpty();
                  }
                }
                _builder.append("}");
                _builder.newLine();
                _builder.newLine();
                String _internalSetterName_1 = DtoTypesBuilder.this._methodNamingExtensions.toInternalSetterName(prop);
                _builder.append(_internalSetterName_1);
                _builder.append("(");
                _builder.append(paramName);
                _builder.append(");");
                _builder.newLineIfNotEmpty();
                _builder.newLine();
                _builder.append("if (this.");
                String _name_4 = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
                _builder.append(_name_4);
                _builder.append(" != null) {");
                _builder.newLineIfNotEmpty();
                {
                  boolean _isToMany_3 = DtoTypesBuilder.this._dtoModelExtensions.isToMany(opposite);
                  if (_isToMany_3) {
                    _builder.append("\t");
                    _builder.append("this.");
                    String _name_5 = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
                    _builder.append(_name_5, "\t");
                    _builder.append(".");
                    String _collectionInternalAdderName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalAdderName(opposite);
                    _builder.append(_collectionInternalAdderName, "\t");
                    _builder.append("(this);");
                    _builder.newLineIfNotEmpty();
                  } else {
                    _builder.append("\t");
                    _builder.append("this.");
                    String _name_6 = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
                    _builder.append(_name_6, "\t");
                    _builder.append(".");
                    String _internalSetterName_2 = DtoTypesBuilder.this._methodNamingExtensions.toInternalSetterName(opposite);
                    _builder.append(_internalSetterName_2, "\t");
                    _builder.append("(this);");
                    _builder.newLineIfNotEmpty();
                  }
                }
                _builder.append("}");
                _builder.newLine();
              } else {
                _builder.append("for (");
                String _simpleName_3 = DtoTypesBuilder.this._dtoModelExtensions.toDtoTypeReference(prop).getSimpleName();
                _builder.append(_simpleName_3);
                _builder.append(" dto : ");
                String _collectionInternalGetterName_1 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
                _builder.append(_collectionInternalGetterName_1);
                _builder.append("().toArray(new ");
                String _simpleName_4 = DtoTypesBuilder.this._dtoModelExtensions.toDtoTypeReference(prop).getSimpleName();
                _builder.append(_simpleName_4);
                _builder.append("[this.");
                String _name_7 = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
                _builder.append(_name_7);
                _builder.append(".size()])) {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                String _collectionRemoverName_1 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionRemoverName(prop);
                _builder.append(_collectionRemoverName_1, "\t");
                _builder.append("(dto);");
                _builder.newLineIfNotEmpty();
                _builder.append("}");
                _builder.newLine();
                _builder.newLine();
                _builder.append("if(");
                _builder.append(paramName);
                _builder.append(" == null) {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("return;");
                _builder.newLine();
                _builder.append("}");
                _builder.newLine();
                _builder.newLine();
                _builder.append("for (");
                String _simpleName_5 = DtoTypesBuilder.this._dtoModelExtensions.toDtoTypeReference(prop).getSimpleName();
                _builder.append(_simpleName_5);
                _builder.append(" dto : ");
                _builder.append(paramName);
                _builder.append(") {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                String _collectionAdderName_1 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionAdderName(prop);
                _builder.append(_collectionAdderName_1, "\t");
                _builder.append("(dto);");
                _builder.newLineIfNotEmpty();
                _builder.append("}");
                _builder.newLine();
              }
            }
          }
        }
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  protected JvmOperation _toGetter(final LDtoAbstractAttribute prop, final String methodName) {
    final String propertyName = this._dtoModelExtensions.toName(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setSimpleName(methodName);
    op.setReturnType(this.cloneWithProxies(this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(prop)));
    boolean _isDerived = prop.isDerived();
    if (_isDerived) {
      final String customDoc = this.getDocumentation(prop);
      boolean _notEquals = (!Objects.equal(customDoc, null));
      if (_notEquals) {
        this.setDocumentation(op, customDoc);
      } else {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Calculates the value for the derived property ");
        String _name = prop.getName();
        _builder.append(_name);
        _builder.newLineIfNotEmpty();
        _builder.append(" ");
        _builder.newLine();
        _builder.append("@return ");
        String _name_1 = prop.getName();
        _builder.append(_name_1);
        _builder.append(" The derived property value");
        this.setDocumentation(op, _builder);
      }
      boolean _isDomainDescription = prop.isDomainDescription();
      if (_isDomainDescription) {
        EList<JvmAnnotationReference> _annotations = op.getAnnotations();
        JvmAnnotationReference _annotation = this.toAnnotation(prop, DomainDescription.class);
        this.<JvmAnnotationReference>operator_add(_annotations, _annotation);
      }
      this.setBody(op, prop.getDerivedGetterExpression());
    } else {
      String _xifexpression = null;
      boolean _isToMany = this._dtoModelExtensions.isToMany(prop);
      if (_isToMany) {
        String _plus = CommonTypesBuilder.operator_plus(
          "Returns an unmodifiable list of ", propertyName);
        _xifexpression = CommonTypesBuilder.operator_plus(_plus, ".");
      } else {
        String _xifexpression_1 = null;
        boolean _notEquals_1 = (!Objects.equal(propertyName, null));
        if (_notEquals_1) {
          String _xifexpression_2 = null;
          boolean _isRequired = this._dtoModelExtensions.getBounds(prop).isRequired();
          if (_isRequired) {
            _xifexpression_2 = "<em>required</em> ";
          } else {
            _xifexpression_2 = "";
          }
          String _concat = "Returns the ".concat(_xifexpression_2).concat(propertyName).concat(" property");
          String _xifexpression_3 = null;
          if (((!this._dtoModelExtensions.isBean(this._dtoModelExtensions.toRawType(prop))) && (!this._dtoModelExtensions.getBounds(prop).isRequired()))) {
            _xifexpression_3 = " or <code>null</code> if not present";
          } else {
            _xifexpression_3 = "";
          }
          _xifexpression_1 = _concat.concat(_xifexpression_3).concat(".");
        }
        _xifexpression = _xifexpression_1;
      }
      this.setDocumentation(op, _xifexpression);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        boolean _equals = Objects.equal(it, null);
        if (_equals) {
          return;
        }
        final ITreeAppendable p = it.trace(prop);
        boolean _isToMany_1 = this._dtoModelExtensions.isToMany(prop);
        if (_isToMany_1) {
          ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, "return ");
          JvmTypeReference _newTypeRef = this.newTypeRef(prop, Collections.class);
          ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan, _newTypeRef);
          ITreeAppendable _doubleGreaterThan_2 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_1, ".unmodifiableList");
          String _collectionInternalGetterName = this._methodNamingExtensions.toCollectionInternalGetterName(prop);
          String _plus_1 = CommonTypesBuilder.operator_plus(
            "(", _collectionInternalGetterName);
          String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, "());");
          this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_2, _plus_2);
        } else {
          boolean _isBean = this._dtoModelExtensions.isBean(this._dtoModelExtensions.toRawType(prop));
          if (_isBean) {
            String _plus_3 = CommonTypesBuilder.operator_plus("if(this.", propertyName);
            String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, "== null)");
            ITreeAppendable _doubleGreaterThan_3 = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_4);
            this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan_3, "{");
            String _plus_5 = CommonTypesBuilder.operator_plus("this.", propertyName);
            String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, " = new ");
            String _dTOBeanSimpleName = this._methodNamingExtensions.toDTOBeanSimpleName(this._dtoModelExtensions.toRawType(prop));
            String _plus_7 = CommonTypesBuilder.operator_plus(_plus_6, _dTOBeanSimpleName);
            String _plus_8 = CommonTypesBuilder.operator_plus(_plus_7, "();");
            this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_8);
            this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
          }
          String _plus_9 = CommonTypesBuilder.operator_plus("return this.", propertyName);
          String _plus_10 = CommonTypesBuilder.operator_plus(_plus_9, ";");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_10);
        }
      };
      this.setBody(op, _function);
    }
    return this.<JvmOperation>associate(prop, op);
  }
  
  protected JvmOperation _toGetter(final LDtoAbstractReference prop, final String methodName) {
    final String propertyName = this._dtoModelExtensions.toName(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setSimpleName(methodName);
    op.setReturnType(this.cloneWithProxies(this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(prop)));
    String _xifexpression = null;
    boolean _isToMany = this._dtoModelExtensions.isToMany(prop);
    if (_isToMany) {
      String _plus = CommonTypesBuilder.operator_plus(
        "Returns an unmodifiable list of ", propertyName);
      _xifexpression = CommonTypesBuilder.operator_plus(_plus, ".");
    } else {
      String _xifexpression_1 = null;
      boolean _notEquals = (!Objects.equal(propertyName, null));
      if (_notEquals) {
        String _xifexpression_2 = null;
        boolean _isRequired = this._dtoModelExtensions.getBounds(prop).isRequired();
        if (_isRequired) {
          _xifexpression_2 = "<em>required</em> ";
        } else {
          _xifexpression_2 = "";
        }
        String _concat = "Returns the ".concat(_xifexpression_2).concat(propertyName).concat(" property");
        String _xifexpression_3 = null;
        if (((!this._dtoModelExtensions.isBean(this._dtoModelExtensions.toRawType(prop))) && (!this._dtoModelExtensions.getBounds(prop).isRequired()))) {
          _xifexpression_3 = " or <code>null</code> if not present";
        } else {
          _xifexpression_3 = "";
        }
        _xifexpression_1 = _concat.concat(_xifexpression_3).concat(".");
      }
      _xifexpression = _xifexpression_1;
    }
    this.setDocumentation(op, _xifexpression);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      boolean _isToMany_1 = this._dtoModelExtensions.isToMany(prop);
      if (_isToMany_1) {
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, "return ");
        JvmTypeReference _newTypeRef = this.newTypeRef(prop, Collections.class);
        ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan, _newTypeRef);
        ITreeAppendable _doubleGreaterThan_2 = this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_1, ".unmodifiableList");
        String _collectionInternalGetterName = this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        String _plus_1 = CommonTypesBuilder.operator_plus(
          "(", _collectionInternalGetterName);
        String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, "());");
        this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_2, _plus_2);
      } else {
        boolean _isBean = this._dtoModelExtensions.isBean(this._dtoModelExtensions.toRawType(prop));
        if (_isBean) {
          String _plus_3 = CommonTypesBuilder.operator_plus("if(this.", propertyName);
          String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, "== null)");
          ITreeAppendable _doubleGreaterThan_3 = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_4);
          this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan_3, "{");
          String _plus_5 = CommonTypesBuilder.operator_plus("this.", propertyName);
          String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, " = new ");
          String _dTOBeanSimpleName = this._methodNamingExtensions.toDTOBeanSimpleName(this._dtoModelExtensions.toRawType(prop));
          String _plus_7 = CommonTypesBuilder.operator_plus(_plus_6, _dTOBeanSimpleName);
          String _plus_8 = CommonTypesBuilder.operator_plus(_plus_7, "();");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_8);
          String _plus_9 = CommonTypesBuilder.operator_plus("this.", propertyName);
          String _plus_10 = CommonTypesBuilder.operator_plus(_plus_9, ".addPropertyChangeListener(this);");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_10);
          this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        }
        String _plus_11 = CommonTypesBuilder.operator_plus("return this.", propertyName);
        String _plus_12 = CommonTypesBuilder.operator_plus(_plus_11, ";");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_12);
      }
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmOperation toProxySetter(final LDtoAbstractReference prop) {
    boolean _isToMany = this._dtoModelExtensions.isToMany(prop);
    if (_isToMany) {
      throw new RuntimeException("toMany-References not allowed for setters!");
    }
    final String paramName = this._methodNamingExtensions.toMethodParamName(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toSetterName(prop));
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, this.references.getTypeForName(ICrossReference.class, prop));
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Sets the proxy of the <code>");
    _builder.append(paramName);
    _builder.append("</code> property for lazy cross reference loading.");
    _builder.newLineIfNotEmpty();
    this.setDocumentation(op, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _checkDisposedCall);
      String _plus = CommonTypesBuilder.operator_plus("firePropertyChange(\"", paramName);
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, "\", this.");
      String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, paramName);
      String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, ", this.");
      String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, paramName);
      String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, " = ");
      String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, paramName);
      String _plus_7 = CommonTypesBuilder.operator_plus(_plus_6, ");");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_7);
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmOperation toProxyGetter(final LDtoAbstractReference prop) {
    boolean _isToMany = this._dtoModelExtensions.isToMany(prop);
    if (_isToMany) {
      throw new RuntimeException("toMany-References not allowed for setters!");
    }
    final String paramName = this._methodNamingExtensions.toMethodParamName(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(ICrossReference.class, prop));
    op.setSimpleName(this._methodNamingExtensions.toGetterName(prop));
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Returns the proxy of the <code>");
    _builder.append(paramName);
    _builder.append("</code>.");
    _builder.newLineIfNotEmpty();
    this.setDocumentation(op, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      if ((it == null)) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _checkDisposedCall);
      String _plus = CommonTypesBuilder.operator_plus("return ", paramName);
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, ";");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_1);
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  /**
   * Builds an adder method for a *toMany relation like
   * <code>Order.addToOrderLines(OrderLine orderLine)</code>.
   */
  public JvmOperation toAdder(final LDtoFeature prop, final String propertyName) {
    final String paramName = StringExtensions.toFirstLower(this._dtoModelExtensions.toTypeName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toCollectionAdderName(prop));
    JvmTypeReference _dtoTypeReference = this._dtoModelExtensions.toDtoTypeReference(prop);
    boolean _tripleNotEquals = (_dtoTypeReference != null);
    if (_tripleNotEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(prop, paramName, this._dtoModelExtensions.toDtoTypeReference(prop));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Adds the given ");
    _builder.append(paramName);
    _builder.append(" to this object. <p>");
    _builder.newLineIfNotEmpty();
    {
      LFeature _opposite = this._dtoModelExtensions.opposite(prop);
      boolean _tripleNotEquals_1 = (_opposite != null);
      if (_tripleNotEquals_1) {
        _builder.append("Since the reference is a composition reference, the opposite reference <code>");
        String _typeName = this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName);
        _builder.append("#");
        String _firstLower = StringExtensions.toFirstLower(this._dtoModelExtensions.opposite(prop).getName());
        _builder.append(_firstLower);
        _builder.append("</code> of the <code>");
        _builder.append(paramName);
        _builder.append("</code> will be handled automatically and no further coding is required to keep them in sync.<p>");
        _builder.newLineIfNotEmpty();
        _builder.append("See {@link ");
        String _typeName_1 = this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName_1);
        _builder.append("#");
        String _setterName = this._methodNamingExtensions.toSetterName(this._dtoModelExtensions.opposite(prop));
        _builder.append(_setterName);
        _builder.append("(");
        String _typeName_2 = this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName_2);
        _builder.append(")}.");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("@param ");
    _builder.append(paramName);
    _builder.append(" - the property");
    _builder.newLineIfNotEmpty();
    _builder.append("@throws RuntimeException if instance is <code>disposed</code>");
    _builder.newLine();
    this.setDocumentation(op, _builder);
    final LFeature opposite = this._dtoModelExtensions.opposite(prop);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("checkDisposed();");
        _builder.newLine();
        _builder.newLine();
        {
          boolean _notEquals = (!Objects.equal(opposite, null));
          if (_notEquals) {
            _builder.append(paramName);
            _builder.append(".");
            String _setterName = DtoTypesBuilder.this._methodNamingExtensions.toSetterName(DtoTypesBuilder.this._dtoModelExtensions.opposite(prop));
            _builder.append(_setterName);
            _builder.append("(this);");
            _builder.newLineIfNotEmpty();
          } else {
            String _collectionInternalAdderName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalAdderName(prop);
            _builder.append(_collectionInternalAdderName);
            _builder.append("(");
            _builder.append(paramName);
            _builder.append(");");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  /**
   * Builds an adder method for a *toMany relation like
   * <code>Order.addToOrderLines(OrderLine orderLine)</code>.
   */
  public JvmOperation toProxyAdder(final LDtoAbstractReference prop, final String propertyName) {
    final String paramName = StringExtensions.toFirstLower(this._dtoModelExtensions.toTypeName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toCollectionAdderName(prop));
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, this.references.getTypeForName(ICrossReference.class, prop));
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Adds the cross reference proxy ");
    _builder.append(paramName);
    _builder.append(". <p>");
    this.setDocumentation(op, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_add(p, _checkDisposedCall);
      String _getterName = this._methodNamingExtensions.toGetterName(prop);
      String _plus = CommonTypesBuilder.operator_plus(_getterName, "().add(");
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, paramName);
      String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, ");");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_2);
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmVisibility getInternalMethodVisibility(final LDtoFeature ref) {
    JvmVisibility _xblockexpression = null;
    {
      EObject _eContainer = ref.eContainer();
      final LPackage ownerPackage = this._dtoModelExtensions.getPackage(((LType) _eContainer));
      final LPackage refPackage = this._dtoModelExtensions.getPackage(this._dtoModelExtensions.toRawType(ref));
      JvmVisibility _xifexpression = null;
      boolean _equals = ownerPackage.equals(refPackage);
      if (_equals) {
        _xifexpression = null;
      } else {
        _xifexpression = JvmVisibility.PUBLIC;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected JvmOperation _toInternalSetter(final LDtoAbstractReference prop) {
    final String paramName = this._methodNamingExtensions.toMethodParamName(prop);
    final JvmTypeReference typeRef = this._dtoModelExtensions.toDtoTypeReference(prop);
    final JvmOperation result = this.typesFactory.createJvmOperation();
    result.setVisibility(JvmVisibility.PUBLIC);
    result.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    result.setSimpleName(this._methodNamingExtensions.toInternalSetterName(prop));
    EList<JvmFormalParameter> _parameters = result.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, typeRef);
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("For internal use only!");
    this.setDocumentation(result, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _plus = CommonTypesBuilder.operator_plus("firePropertyChange(\"", paramName);
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, "\", this.");
      String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, paramName);
      String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, ", this.");
      String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, paramName);
      String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, " = ");
      String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, paramName);
      String _plus_7 = CommonTypesBuilder.operator_plus(_plus_6, ");");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_7);
    };
    this.setBody(result, _function);
    return this.<JvmOperation>associate(prop, result);
  }
  
  @Override
  public JvmField toField(final LFeature prop) {
    JvmField _xblockexpression = null;
    {
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName(this._dtoModelExtensions.toName(prop));
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(((LDtoFeature) prop)));
      jvmField.setTransient(this._dtoModelExtensions.isTransient(prop));
      if (((prop instanceof LAttribute) && this.isCreateUuid(((LAttribute) prop)))) {
        final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
          boolean _equals = Objects.equal(it, null);
          if (_equals) {
            return;
          }
          final ITreeAppendable p = it.trace(prop);
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("java.util.UUID.randomUUID().toString()");
          this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder);
        };
        this.setInitializer(jvmField, _function);
      } else {
        if ((prop instanceof LDtoAbstractAttribute)) {
          if ((this._dtoModelExtensions.isIDorUUID(((LAttribute)prop)) && ((LDtoAbstractAttribute)prop).getDTO().isHistorizedOrTimedependent())) {
            final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it) -> {
              boolean _equals = Objects.equal(it, null);
              if (_equals) {
                return;
              }
              final ITreeAppendable p = it.trace(prop);
              StringConcatenation _builder = new StringConcatenation();
              _builder.append("new UUIDHist()");
              this._treeAppendableExtensions.operator_doubleGreaterThan(p, _builder);
            };
            this.setInitializer(jvmField, _function_1);
          }
        }
      }
      this.annotationCompiler.processAnnotation(prop, jvmField);
      _xblockexpression = this.<JvmField>associate(prop, jvmField);
    }
    return _xblockexpression;
  }
  
  public boolean isCreateUuid(final LAttribute att) {
    boolean _xifexpression = false;
    if ((att instanceof LDtoInheritedAttribute)) {
      final LDtoInheritedAttribute iAtt = ((LDtoInheritedAttribute) att);
      return iAtt.getInheritedFeature().isUuid();
    } else {
      _xifexpression = att.isUuid();
    }
    return _xifexpression;
  }
  
  public boolean uuidPresent(final LEntity entity) {
    final Function1<LEntityAttribute, Boolean> _function = (LEntityAttribute it) -> {
      return Boolean.valueOf(it.isUuid());
    };
    return IterableExtensions.<LEntityAttribute>exists(entity.getAllAttributes(), _function);
  }
  
  public JvmField toProxyField(final LFeature prop) {
    JvmField _xblockexpression = null;
    {
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName(this._dtoModelExtensions.toName(prop));
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this.references.getTypeForName(ICrossReference.class, prop));
      this.annotationCompiler.processAnnotation(prop, jvmField);
      _xblockexpression = this.<JvmField>associate(prop, jvmField);
    }
    return _xblockexpression;
  }
  
  protected JvmOperation _toInternalSetter(final LDtoAbstractAttribute prop) {
    final String paramName = this._methodNamingExtensions.toMethodParamName(prop);
    final JvmTypeReference typeRef = this._dtoModelExtensions.toDtoTypeReference(prop);
    final JvmOperation result = this.typesFactory.createJvmOperation();
    result.setVisibility(JvmVisibility.PUBLIC);
    result.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    result.setSimpleName(this._methodNamingExtensions.toInternalSetterName(prop));
    EList<JvmFormalParameter> _parameters = result.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, typeRef);
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("For internal use only!");
    this.setDocumentation(result, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _plus = CommonTypesBuilder.operator_plus("firePropertyChange(\"", paramName);
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, "\", this.");
      String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, paramName);
      String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, ", this.");
      String _plus_4 = CommonTypesBuilder.operator_plus(_plus_3, paramName);
      String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, " = ");
      String _plus_6 = CommonTypesBuilder.operator_plus(_plus_5, paramName);
      String _plus_7 = CommonTypesBuilder.operator_plus(_plus_6, ");");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_7);
    };
    this.setBody(result, _function);
    return this.<JvmOperation>associate(prop, result);
  }
  
  protected JvmOperation _toInternalAdder(final LDtoAbstractReference prop) {
    final String paramName = StringExtensions.toFirstLower(this._dtoModelExtensions.toTypeName(prop));
    final JvmTypeReference typeRef = this._dtoModelExtensions.toDtoTypeReference(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toCollectionInternalAdderName(prop));
    final JvmFormalParameter param = this.toParameter(prop, paramName, typeRef);
    boolean _notEquals = (!Objects.equal(param, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      this.<JvmFormalParameter>operator_add(_parameters, param);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("For internal use only!");
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        {
          boolean _isBean = DtoTypesBuilder.this._dtoModelExtensions.isBean(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
          if (_isBean) {
            _builder.append("// add this as property change listener for embeddable beans");
            _builder.newLine();
            _builder.append(paramName);
            _builder.append(".addPropertyChangeListener(this);");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.newLine();
        _builder.append("if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("List<");
        String _typeName = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName, "\t\t");
        _builder.append("> oldList = null;");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("if(");
        String _collectionInternalGetterName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName, "\t\t");
        _builder.append("() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t\t");
        _builder.append("oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) ");
        String _collectionInternalGetterName_1 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_1, "\t\t\t");
        _builder.append("()).copy();");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("} else {");
        _builder.newLine();
        _builder.append("\t\t\t");
        _builder.append("oldList = new java.util.ArrayList<>(");
        String _collectionInternalGetterName_2 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_2, "\t\t\t");
        _builder.append("());");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("}");
        _builder.newLine();
        _builder.append("\t\t");
        String _collectionInternalGetterName_3 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_3, "\t\t");
        _builder.append("().add(");
        _builder.append(paramName, "\t\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("firePropertyChange(\"");
        String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
        _builder.append(_name, "\t\t");
        _builder.append("\", oldList, ");
        String _collectionInternalGetterName_4 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_4, "\t\t");
        _builder.append("());");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  protected JvmOperation _toInternalAdder(final LDtoAbstractAttribute prop) {
    final String paramName = StringExtensions.toFirstLower(this._dtoModelExtensions.toTypeName(prop));
    final JvmTypeReference typeRef = this._dtoModelExtensions.toDtoTypeReference(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toCollectionInternalAdderName(prop));
    final JvmFormalParameter param = this.toParameter(prop, paramName, typeRef);
    boolean _notEquals = (!Objects.equal(param, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      this.<JvmFormalParameter>operator_add(_parameters, param);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("For internal use only!");
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        {
          boolean _isBean = DtoTypesBuilder.this._dtoModelExtensions.isBean(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
          if (_isBean) {
            _builder.append("// add this as property change listener for embeddable beans");
            _builder.newLine();
            _builder.append(paramName);
            _builder.append(".addPropertyChangeListener(this);");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.newLine();
        _builder.append("List<");
        String _typeName = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName);
        _builder.append("> oldList = null;");
        _builder.newLineIfNotEmpty();
        _builder.append("if(");
        String _collectionInternalGetterName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName);
        _builder.append("() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) ");
        String _collectionInternalGetterName_1 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_1, "\t");
        _builder.append("()).copy();");
        _builder.newLineIfNotEmpty();
        _builder.append("} else {");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("oldList = new java.util.ArrayList<>(");
        String _collectionInternalGetterName_2 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_2, "\t");
        _builder.append("());");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
        String _collectionInternalGetterName_3 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_3);
        _builder.append("().add(");
        _builder.append(paramName);
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("firePropertyChange(\"");
        String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
        _builder.append(_name);
        _builder.append("\", oldList, ");
        String _collectionInternalGetterName_4 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_4);
        _builder.append("());");
        _builder.newLineIfNotEmpty();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  protected JvmOperation _toInternalRemover(final LDtoAbstractReference prop) {
    final String paramName = StringExtensions.toFirstLower(this._dtoModelExtensions.toTypeName(prop));
    final JvmTypeReference typeRef = this._dtoModelExtensions.toDtoTypeReference(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toCollectionInternalRemoverName(prop));
    final JvmFormalParameter param = this.toParameter(prop, paramName, typeRef);
    boolean _notEquals = (!Objects.equal(param, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      this.<JvmFormalParameter>operator_add(_parameters, param);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("For internal use only!");
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        {
          boolean _isBean = DtoTypesBuilder.this._dtoModelExtensions.isBean(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
          if (_isBean) {
            _builder.append("// remove this as property change listener from the embeddable bean");
            _builder.newLine();
            _builder.append(paramName);
            _builder.append(".removePropertyChangeListener(this);");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("List<");
        String _typeName = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName, "\t");
        _builder.append("> oldList = null;");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("if(");
        String _collectionInternalGetterName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName, "\t");
        _builder.append("() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) ");
        String _collectionInternalGetterName_1 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_1, "\t\t");
        _builder.append("()).copy();");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("} else {");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("oldList = new java.util.ArrayList<>(");
        String _collectionInternalGetterName_2 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_2, "\t\t");
        _builder.append("());");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("}");
        _builder.newLine();
        _builder.append("\t");
        String _collectionInternalGetterName_3 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_3, "\t");
        _builder.append("().remove(");
        _builder.append(paramName, "\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("firePropertyChange(\"");
        String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
        _builder.append(_name, "\t");
        _builder.append("\", oldList, ");
        String _collectionInternalGetterName_4 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_4, "\t");
        _builder.append("());\t");
        _builder.newLineIfNotEmpty();
        _builder.append("}else{");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("// in mapping mode, we do NOT resolve any collection");
        _builder.newLine();
        _builder.append("\t");
        String _collectionInternalGetterName_5 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_5, "\t");
        _builder.append("().remove(");
        _builder.append(paramName, "\t");
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  protected JvmOperation _toInternalRemover(final LDtoAbstractAttribute prop) {
    final String paramName = StringExtensions.toFirstLower(this._dtoModelExtensions.toTypeName(prop));
    final JvmTypeReference typeRef = this._dtoModelExtensions.toDtoTypeReference(prop);
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toCollectionInternalRemoverName(prop));
    final JvmFormalParameter param = this.toParameter(prop, paramName, typeRef);
    boolean _notEquals = (!Objects.equal(param, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      this.<JvmFormalParameter>operator_add(_parameters, param);
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("For internal use only!");
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        {
          boolean _isBean = DtoTypesBuilder.this._dtoModelExtensions.isBean(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
          if (_isBean) {
            _builder.append("// remove this as property change listener from the embeddable bean");
            _builder.newLine();
            _builder.append(paramName);
            _builder.append(".removePropertyChangeListener(this);");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("List<");
        String _typeName = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
        _builder.append(_typeName);
        _builder.append("> oldList = null;");
        _builder.newLineIfNotEmpty();
        _builder.append("if(");
        String _collectionInternalGetterName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName);
        _builder.append("() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) ");
        String _collectionInternalGetterName_1 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_1, "\t");
        _builder.append("()).copy();");
        _builder.newLineIfNotEmpty();
        _builder.append("} else {");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("oldList = new java.util.ArrayList<>(");
        String _collectionInternalGetterName_2 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_2, "\t");
        _builder.append("());");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
        String _collectionInternalGetterName_3 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_3);
        _builder.append("().remove(");
        _builder.append(paramName);
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("firePropertyChange(\"");
        String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(prop);
        _builder.append(_name);
        _builder.append("\", oldList, ");
        String _collectionInternalGetterName_4 = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalGetterName(prop);
        _builder.append(_collectionInternalGetterName_4);
        _builder.append("());");
        _builder.newLineIfNotEmpty();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  protected JvmOperation _toInternalCollectionGetter(final LDtoAbstractReference prop, final String name) {
    JvmOperation _xblockexpression = null;
    {
      final String fieldName = StringExtensions.toFirstLower(name);
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setReturnType(this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(prop));
      op.setSimpleName(this._methodNamingExtensions.toCollectionInternalGetterName(prop));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Returns the list of ");
      String _htmlCode = this.htmlCode(this._dtoModelExtensions.toTypeName(prop));
      _builder.append(_htmlCode);
      _builder.append("s thereby lazy initializing it. For internal use only!");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("@return list - the resulting list");
      _builder.newLine();
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        boolean _equals = Objects.equal(it, null);
        if (_equals) {
          return;
        }
        ITreeAppendable p = it.trace(prop);
        final String fieldRef = CommonTypesBuilder.operator_plus("this.", fieldName);
        String _plus = CommonTypesBuilder.operator_plus("if (", fieldRef);
        String _plus_1 = CommonTypesBuilder.operator_plus(_plus, " == null)");
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_1);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, " {");
        ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(p, fieldRef);
        String _typeName = this._dtoModelExtensions.toTypeName(prop);
        String _plus_2 = CommonTypesBuilder.operator_plus(" = new java.util.ArrayList<", _typeName);
        String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, ">();");
        this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_1, _plus_3);
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        String _plus_4 = CommonTypesBuilder.operator_plus("return ", fieldRef);
        String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, ";");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_5);
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(prop, op);
    }
    return _xblockexpression;
  }
  
  protected JvmOperation _toInternalCollectionGetter(final LDtoAbstractAttribute prop, final String name) {
    JvmOperation _xblockexpression = null;
    {
      final String fieldName = StringExtensions.toFirstLower(name);
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setReturnType(this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(prop));
      op.setSimpleName(this._methodNamingExtensions.toCollectionInternalGetterName(prop));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Returns the list of ");
      String _htmlCode = this.htmlCode(this._dtoModelExtensions.toTypeName(prop));
      _builder.append(_htmlCode);
      _builder.append("s thereby lazy initializing it. For internal use only!");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("@return list - the resulting list");
      _builder.newLine();
      this.setDocumentation(op, _builder);
      final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
        boolean _equals = Objects.equal(it, null);
        if (_equals) {
          return;
        }
        ITreeAppendable p = it.trace(prop);
        final String fieldRef = CommonTypesBuilder.operator_plus("this.", fieldName);
        String _plus = CommonTypesBuilder.operator_plus("if (", fieldRef);
        String _plus_1 = CommonTypesBuilder.operator_plus(_plus, " == null)");
        ITreeAppendable _doubleGreaterThan = this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_1);
        this._treeAppendableExtensions.operator_tripleGreaterThan(_doubleGreaterThan, " {");
        ITreeAppendable _doubleGreaterThan_1 = this._treeAppendableExtensions.operator_doubleGreaterThan(p, fieldRef);
        String _qualifiedName = this._dtoModelExtensions.toDtoTypeReference(prop).getQualifiedName();
        String _plus_2 = CommonTypesBuilder.operator_plus(" = new java.util.ArrayList<", _qualifiedName);
        String _plus_3 = CommonTypesBuilder.operator_plus(_plus_2, ">();");
        this._treeAppendableExtensions.operator_doubleGreaterThan(_doubleGreaterThan_1, _plus_3);
        this._treeAppendableExtensions.operator_tripleLessThan(p, "}");
        String _plus_4 = CommonTypesBuilder.operator_plus("return ", fieldRef);
        String _plus_5 = CommonTypesBuilder.operator_plus(_plus_4, ";");
        this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_5);
      };
      this.setBody(op, _function);
      _xblockexpression = this.<JvmOperation>associate(prop, op);
    }
    return _xblockexpression;
  }
  
  /**
   * Builds a remover method for a *toMany relation like
   * <code>Order.removeFromOrderLines(OrderLine orderLine)</code>.
   */
  protected JvmOperation _toRemover(final LDtoAbstractReference prop, final String propertyName) {
    final String paramName = StringExtensions.toFirstLower(this._dtoModelExtensions.toTypeName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toCollectionRemoverName(prop));
    JvmTypeReference _dtoTypeReference = this._dtoModelExtensions.toDtoTypeReference(prop);
    boolean _notEquals = (!Objects.equal(_dtoTypeReference, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(prop, paramName, this._dtoModelExtensions.toDtoTypeReference(prop));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    }
    LFeature _opposite = this._dtoModelExtensions.opposite(prop);
    boolean _notEquals_1 = (!Objects.equal(_opposite, null));
    if (_notEquals_1) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Removes the given ");
      _builder.append(paramName);
      _builder.append(" from this object. <p>");
      _builder.newLineIfNotEmpty();
      {
        boolean _isCascading = prop.isCascading();
        if (_isCascading) {
          _builder.append("Since the reference is a cascading reference, the opposite reference (");
          String _typeName = this._dtoModelExtensions.toTypeName(prop);
          _builder.append(_typeName);
          _builder.append(".");
          String _firstLower = StringExtensions.toFirstLower(this._dtoModelExtensions.opposite(prop).getName());
          _builder.append(_firstLower);
          _builder.append(")");
          _builder.newLineIfNotEmpty();
          _builder.append("of the ");
          _builder.append(paramName);
          _builder.append(" will be handled automatically and no further coding is required to keep them in sync. ");
          _builder.newLineIfNotEmpty();
          _builder.append("See {@link ");
          String _typeName_1 = this._dtoModelExtensions.toTypeName(prop);
          _builder.append(_typeName_1);
          _builder.append("#");
          String _setterName = this._methodNamingExtensions.toSetterName(this._dtoModelExtensions.opposite(prop));
          _builder.append(_setterName);
          _builder.append("(");
          String _typeName_2 = this._dtoModelExtensions.toTypeName(prop);
          _builder.append(_typeName_2);
          _builder.append(")}.");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("@param ");
      _builder.append(paramName);
      _builder.append(" - the property");
      _builder.newLineIfNotEmpty();
      _builder.append("@throws RuntimeException if instance is <code>disposed</code>");
      _builder.newLine();
      this.setDocumentation(op, _builder);
    }
    final LFeature opposite = this._dtoModelExtensions.opposite(prop);
    final boolean cascading = prop.isCascading();
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("checkDisposed();");
        _builder.newLine();
        _builder.newLine();
        {
          boolean _notEquals = (!Objects.equal(opposite, null));
          if (_notEquals) {
            _builder.append(paramName);
            _builder.append(".");
            String _setterName = DtoTypesBuilder.this._methodNamingExtensions.toSetterName(DtoTypesBuilder.this._dtoModelExtensions.opposite(prop));
            _builder.append(_setterName);
            _builder.append("(null);");
            _builder.newLineIfNotEmpty();
          } else {
            String _collectionInternalRemoverName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalRemoverName(prop);
            _builder.append(_collectionInternalRemoverName);
            _builder.append("(");
            _builder.append(paramName);
            _builder.append(");");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmOperation toProxyRemover(final LDtoAbstractReference prop, final String propertyName) {
    final String paramName = StringExtensions.toFirstLower(this._dtoModelExtensions.toTypeName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toCollectionRemoverName(prop));
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(prop, paramName, this.references.getTypeForName(ICrossReference.class, prop));
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Removes the proxy cross reference ");
    _builder.append(paramName);
    _builder.append(" from this object. <p>");
    _builder.newLineIfNotEmpty();
    this.setDocumentation(op, _builder);
    final Procedure1<ITreeAppendable> _function = (ITreeAppendable it) -> {
      boolean _equals = Objects.equal(it, null);
      if (_equals) {
        return;
      }
      final ITreeAppendable p = it.trace(prop);
      String _checkDisposedCall = this.toCheckDisposedCall(prop);
      this._treeAppendableExtensions.operator_add(p, _checkDisposedCall);
      String _getterName = this._methodNamingExtensions.toGetterName(prop);
      String _plus = CommonTypesBuilder.operator_plus(_getterName, "().remove(");
      String _plus_1 = CommonTypesBuilder.operator_plus(_plus, paramName);
      String _plus_2 = CommonTypesBuilder.operator_plus(_plus_1, ");");
      this._treeAppendableExtensions.operator_doubleGreaterThan(p, _plus_2);
    };
    this.setBody(op, _function);
    return this.<JvmOperation>associate(prop, op);
  }
  
  /**
   * Builds a remover method for a *toMany relation like
   * <code>Order.removeFromOrderLines(OrderLine orderLine)</code>.
   */
  protected JvmOperation _toRemover(final LDtoAbstractAttribute prop, final String propertyName) {
    final String paramName = StringExtensions.toFirstLower(this._dtoModelExtensions.toTypeName(prop));
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, prop));
    op.setSimpleName(this._methodNamingExtensions.toCollectionRemoverName(prop));
    JvmTypeReference _dtoTypeReference = this._dtoModelExtensions.toDtoTypeReference(prop);
    boolean _notEquals = (!Objects.equal(_dtoTypeReference, null));
    if (_notEquals) {
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(prop, paramName, this._dtoModelExtensions.toDtoTypeReference(prop));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    }
    LFeature _opposite = this._dtoModelExtensions.opposite(prop);
    boolean _notEquals_1 = (!Objects.equal(_opposite, null));
    if (_notEquals_1) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Removes the given ");
      _builder.append(paramName);
      _builder.append(" from this object. <p>");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("@param ");
      _builder.append(paramName);
      _builder.append(" - the property");
      _builder.newLineIfNotEmpty();
      _builder.append("@throws RuntimeException if instance is <code>disposed</code>");
      _builder.newLine();
      this.setDocumentation(op, _builder);
    } else {
    }
    final LFeature opposite = this._dtoModelExtensions.opposite(prop);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("checkDisposed();");
        _builder.newLine();
        _builder.newLine();
        {
          boolean _notEquals = (!Objects.equal(opposite, null));
          if (_notEquals) {
            _builder.append(paramName);
            _builder.append(".");
            String _setterName = DtoTypesBuilder.this._methodNamingExtensions.toSetterName(DtoTypesBuilder.this._dtoModelExtensions.opposite(prop));
            _builder.append(_setterName);
            _builder.append("(null);");
            _builder.newLineIfNotEmpty();
          } else {
            String _collectionInternalRemoverName = DtoTypesBuilder.this._methodNamingExtensions.toCollectionInternalRemoverName(prop);
            _builder.append(_collectionInternalRemoverName);
            _builder.append("(");
            _builder.append(paramName);
            _builder.append(");");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(prop, op);
  }
  
  public JvmOperation toMapperBindMethod(final LDto dto) {
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PROTECTED);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, dto));
    op.setSimpleName("bindMapperAccess");
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(dto, "mapperAccess", this.references.getTypeForName(IMapperAccess.class, dto, null));
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Called by OSGi-DS. Binds the mapper access service.");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@param service - The mapper access service");
    _builder.newLine();
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("this.mapperAccess = mapperAccess;");
        _builder.newLine();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(dto, op);
  }
  
  public JvmOperation toMapperUnbindMethod(final LDto dto) {
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PROTECTED);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, dto));
    op.setSimpleName("unbindMapperAccess");
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(dto, "mapperAccess", this.references.getTypeForName(IMapperAccess.class, dto, null));
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Called by OSGi-DS. Binds the mapper access service.");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@param service - The mapper access service");
    _builder.newLine();
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("this.mapperAccess = null;");
        _builder.newLine();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(dto, op);
  }
  
  public JvmField toMapperField(final LDtoAbstractReference prop) {
    JvmField _xblockexpression = null;
    {
      final JvmField jvmField = this.typesFactory.createJvmField();
      jvmField.setSimpleName(this._methodNamingExtensions.toMapperFieldName(prop));
      jvmField.setVisibility(JvmVisibility.PRIVATE);
      jvmField.setType(this.cloneWithProxies(this._dtoModelExtensions.toMapperTypeReference(prop.getType())));
      this.annotationCompiler.processAnnotation(prop, jvmField);
      _xblockexpression = this.<JvmField>associate(prop, jvmField);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toGetToDtoMapperAccess(final LDto dto) {
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PROTECTED);
    final JvmTypeParameter dTypeParam = this.typesFactory.createJvmTypeParameter();
    dTypeParam.setName("D");
    EList<JvmTypeParameter> _typeParameters = op.getTypeParameters();
    this.<JvmTypeParameter>operator_add(_typeParameters, dTypeParam);
    final JvmTypeParameter eTypeParam = this.typesFactory.createJvmTypeParameter();
    eTypeParam.setName("E");
    EList<JvmTypeParameter> _typeParameters_1 = op.getTypeParameters();
    this.<JvmTypeParameter>operator_add(_typeParameters_1, eTypeParam);
    final JvmGenericType dType = this.createJvmGenericType(this.typesFactory, "D");
    final JvmParameterizedTypeReference dTypeRef = this.typesFactory.createJvmParameterizedTypeReference();
    dTypeRef.setType(dType);
    final JvmGenericType eType = this.createJvmGenericType(this.typesFactory, "E");
    final JvmParameterizedTypeReference eTypeRef = this.typesFactory.createJvmParameterizedTypeReference();
    eTypeRef.setType(eType);
    op.setReturnType(this.references.getTypeForName(IMapper.class, dto, this.cloneWithProxies(dTypeRef), 
      this.cloneWithProxies(eTypeRef)));
    op.setSimpleName("getToDtoMapper");
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(dto, "dtoClass", this.references.getTypeForName(Class.class, dto, this.cloneWithProxies(dTypeRef)));
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    EList<JvmFormalParameter> _parameters_1 = op.getParameters();
    JvmFormalParameter _parameter_1 = this.toParameter(dto, "entityClass", this.references.getTypeForName(Class.class, dto, this.cloneWithProxies(eTypeRef)));
    this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@param dtoClass - the class of the dto that should be mapped");
    _builder.newLine();
    _builder.append("@param entityClass - the class of the entity that should be mapped");
    _builder.newLine();
    _builder.append("@return the mapper instance or <code>null</code>");
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("return mapperAccess.getToDtoMapper(dtoClass, entityClass);");
        _builder.newLine();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(dto, op);
  }
  
  public JvmOperation toGetToEntityMapperAccess(final LDto dto) {
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PROTECTED);
    final JvmTypeParameter dTypeParam = this.typesFactory.createJvmTypeParameter();
    dTypeParam.setName("D");
    EList<JvmTypeParameter> _typeParameters = op.getTypeParameters();
    this.<JvmTypeParameter>operator_add(_typeParameters, dTypeParam);
    final JvmTypeParameter eTypeParam = this.typesFactory.createJvmTypeParameter();
    eTypeParam.setName("E");
    EList<JvmTypeParameter> _typeParameters_1 = op.getTypeParameters();
    this.<JvmTypeParameter>operator_add(_typeParameters_1, eTypeParam);
    final JvmGenericType dType = this.createJvmGenericType(this.typesFactory, "D");
    final JvmParameterizedTypeReference dTypeRef = this.typesFactory.createJvmParameterizedTypeReference();
    dTypeRef.setType(dType);
    final JvmGenericType eType = this.createJvmGenericType(this.typesFactory, "E");
    final JvmParameterizedTypeReference eTypeRef = this.typesFactory.createJvmParameterizedTypeReference();
    eTypeRef.setType(eType);
    op.setReturnType(this.references.getTypeForName(IMapper.class, dto, this.cloneWithProxies(dTypeRef), 
      this.cloneWithProxies(eTypeRef)));
    op.setSimpleName("getToEntityMapper");
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(dto, "dtoClass", this.references.getTypeForName(Class.class, dto, this.cloneWithProxies(dTypeRef)));
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    EList<JvmFormalParameter> _parameters_1 = op.getParameters();
    JvmFormalParameter _parameter_1 = this.toParameter(dto, "entityClass", this.references.getTypeForName(Class.class, dto, this.cloneWithProxies(eTypeRef)));
    this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@param dtoClass - the class of the dto that should be mapped");
    _builder.newLine();
    _builder.append("@param entityClass - the class of the entity that should be mapped");
    _builder.newLine();
    _builder.append("@return the mapper instance or <code>null</code>");
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("return mapperAccess.getToEntityMapper(dtoClass, entityClass);");
        _builder.newLine();
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(dto, op);
  }
  
  public JvmOperation toMapToDto(final LDto dto) {
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, dto));
    op.setSimpleName("mapToDTO");
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(dto, "dto", this.findDtoTypeReference(dto));
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    EList<JvmFormalParameter> _parameters_1 = op.getParameters();
    LType _wrappedType = dto.getWrappedType();
    JvmTypeReference _typeReference = null;
    if (_wrappedType!=null) {
      _typeReference=this._dtoModelExtensions.toTypeReference(_wrappedType);
    }
    JvmFormalParameter _parameter_1 = this.toParameter(dto, "entity", _typeReference);
    this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
    EList<JvmFormalParameter> _parameters_2 = op.getParameters();
    JvmFormalParameter _parameter_2 = this.toParameter(dto, "context", this.references.getTypeForName(MappingContext.class, dto));
    this.<JvmFormalParameter>operator_add(_parameters_2, _parameter_2);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Maps the entity {@link ");
    String _name = this._dtoModelExtensions.toName(dto.getWrappedType());
    _builder.append(_name);
    _builder.append("} to the dto {@link ");
    String _name_1 = this._dtoModelExtensions.toName(dto);
    _builder.append(_name_1);
    _builder.append("}.");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("@param dto - The target dto");
    _builder.newLine();
    _builder.append("@param entity - The source entity");
    _builder.newLine();
    _builder.append("@param context - The context to get information about depth,...");
    _builder.newLine();
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("if(context == null){");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("throw new IllegalArgumentException(\"Please pass a context!\");");
        _builder.newLine();
        _builder.append("}");
        _builder.newLine();
        {
          boolean _isBean = DtoTypesBuilder.this._dtoModelExtensions.isBean(dto.getWrappedType());
          boolean _not = (!_isBean);
          if (_not) {
            _builder.append("context.register(createDtoHash(entity), dto);");
            _builder.newLine();
            _builder.newLine();
          }
        }
        {
          LDto _superType = dto.getSuperType();
          boolean _notEquals = (!Objects.equal(_superType, null));
          if (_notEquals) {
            _builder.append("super.mapToDTO(dto, entity, context);");
            _builder.newLine();
            _builder.newLine();
          }
        }
        {
          final Function1<LDtoFeature, Boolean> _function = (LDtoFeature it) -> {
            boolean _or = false;
            boolean _or_1 = false;
            boolean _inherited = DtoTypesBuilder.this._dtoModelExtensions.inherited(it);
            if (_inherited) {
              _or_1 = true;
            } else {
              LDtoMapper _mapper = it.getMapper();
              XExpression _toDTO = null;
              if (_mapper!=null) {
                _toDTO=_mapper.getToDTO();
              }
              boolean _notEquals_1 = (!Objects.equal(_toDTO, null));
              _or_1 = _notEquals_1;
            }
            if (_or_1) {
              _or = true;
            } else {
              _or = ((it instanceof LDtoAbstractAttribute) && DtoTypesBuilder.this._dtoModelExtensions.typeIsStateClass(((LDtoAbstractAttribute) it)));
            }
            return Boolean.valueOf(_or);
          };
          Iterable<LDtoFeature> _filter = IterableExtensions.<LDtoFeature>filter(dto.getFeatures(), _function);
          for(final LDtoFeature f : _filter) {
            {
              boolean _isToMany = DtoTypesBuilder.this._dtoModelExtensions.getBounds(f).isToMany();
              boolean _not_1 = (!_isToMany);
              if (_not_1) {
                _builder.append("dto.set");
                String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(f));
                _builder.append(_firstUpper);
                _builder.append("(");
                String _mapPropertyToDto = DtoTypesBuilder.this._methodNamingExtensions.toMapPropertyToDto(f);
                _builder.append(_mapPropertyToDto);
                _builder.append("(entity, context));");
                _builder.newLineIfNotEmpty();
              } else {
              }
            }
          }
        }
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(dto, op);
  }
  
  public JvmOperation toInstallLazyCollections(final LDto dto) {
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PROTECTED);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, dto));
    op.setSimpleName("installLazyCollections");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Installs lazy collection resolving for entity {@link ");
    String _name = this._dtoModelExtensions.toName(dto.getWrappedType());
    _builder.append(_name);
    _builder.append("} to the dto {@link ");
    String _name_1 = this._dtoModelExtensions.toName(dto);
    _builder.append(_name_1);
    _builder.append("}.");
    _builder.newLineIfNotEmpty();
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        {
          LDto _superType = dto.getSuperType();
          boolean _tripleNotEquals = (_superType != null);
          if (_tripleNotEquals) {
            _builder.append("super.installLazyCollections();");
            _builder.newLine();
          }
        }
        {
          final Function1<LDtoAbstractReference, Boolean> _function = (LDtoAbstractReference it) -> {
            return Boolean.valueOf((DtoTypesBuilder.this._dtoModelExtensions.inherited(it) && (!DtoTypesBuilder.this._dtoModelExtensions.isBean(dto.getWrappedType()))));
          };
          Iterable<LDtoAbstractReference> _filter = IterableExtensions.<LDtoAbstractReference>filter(dto.getReferences(), _function);
          for(final LDtoAbstractReference f : _filter) {
            {
              boolean _isToMany = DtoTypesBuilder.this._dtoModelExtensions.getBounds(f).isToMany();
              if (_isToMany) {
                {
                  boolean _isContainmentReference = DtoTypesBuilder.this._dtoModelExtensions.isContainmentReference(f);
                  if (_isContainmentReference) {
                    String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(f);
                    _builder.append(_name);
                    _builder.append(" = new org.eclipse.osbp.dsl.dto.lib.OppositeContainmentDtoList<>(");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t\t\t\t");
                    _builder.append("org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),");
                    _builder.newLine();
                    _builder.append("\t\t\t\t");
                    String _simpleName = DtoTypesBuilder.this._dtoModelExtensions.toDtoTypeReference(f).getSimpleName();
                    _builder.append(_simpleName, "\t\t\t\t");
                    _builder.append(".class, this, \"");
                    String _name_1 = DtoTypesBuilder.this._dtoModelExtensions.opposite(f).getName();
                    _builder.append(_name_1, "\t\t\t\t");
                    _builder.append(".");
                    LFeature _opposite = DtoTypesBuilder.this._dtoModelExtensions.opposite(f);
                    String _dtoIdAttribute = DtoTypesBuilder.this.toDtoIdAttribute(((LReference) _opposite));
                    _builder.append(_dtoIdAttribute, "\t\t\t\t");
                    _builder.append("\",");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t\t\t\t");
                    _builder.append("(java.util.function.Supplier<Object> & Serializable) () -> this.get");
                    String _firstUpper = StringExtensions.toFirstUpper(dto.getIdAttribute().getName());
                    _builder.append(_firstUpper, "\t\t\t\t");
                    _builder.append("(), this);");
                    _builder.newLineIfNotEmpty();
                  } else {
                    String _name_2 = DtoTypesBuilder.this._dtoModelExtensions.toName(f);
                    _builder.append(_name_2);
                    _builder.append(" = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t\t\t\t");
                    _builder.append("org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),");
                    _builder.newLine();
                    _builder.append("\t\t\t\t");
                    String _simpleName_1 = DtoTypesBuilder.this._dtoModelExtensions.toDtoTypeReference(f).getSimpleName();
                    _builder.append(_simpleName_1, "\t\t\t\t");
                    _builder.append(".class, \"");
                    String _name_3 = DtoTypesBuilder.this._dtoModelExtensions.opposite(f).getName();
                    _builder.append(_name_3, "\t\t\t\t");
                    _builder.append(".");
                    LFeature _opposite_1 = DtoTypesBuilder.this._dtoModelExtensions.opposite(f);
                    String _dtoIdAttribute_1 = DtoTypesBuilder.this.toDtoIdAttribute(((LReference) _opposite_1));
                    _builder.append(_dtoIdAttribute_1, "\t\t\t\t");
                    _builder.append("\",");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t\t\t\t");
                    _builder.append("(java.util.function.Supplier<Object> & Serializable) () -> this.get");
                    String _firstUpper_1 = StringExtensions.toFirstUpper(dto.getIdAttribute().getName());
                    _builder.append(_firstUpper_1, "\t\t\t\t");
                    _builder.append("(), this);");
                    _builder.newLineIfNotEmpty();
                  }
                }
              }
            }
          }
        }
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(dto, op);
  }
  
  public String toDtoIdAttribute(final LReference ref) {
    return this._dtoModelExtensions.idAttribute(this._dtoModelExtensions.toRawType(ref)).getName();
  }
  
  public JvmOperation toFirePropertyChange(final LClass sourceElement, final boolean useVersion) {
    JvmOperation _xblockexpression = null;
    {
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PUBLIC);
      op.setSimpleName("firePropertyChange");
      op.setReturnType(this.references.getTypeForName(Void.TYPE, sourceElement, null));
      EList<JvmFormalParameter> _parameters = op.getParameters();
      JvmFormalParameter _parameter = this.toParameter(sourceElement, "propertyName", this.references.getTypeForName(String.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters, _parameter);
      EList<JvmFormalParameter> _parameters_1 = op.getParameters();
      JvmFormalParameter _parameter_1 = this.toParameter(sourceElement, "oldValue", 
        this.references.getTypeForName(Object.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
      EList<JvmFormalParameter> _parameters_2 = op.getParameters();
      JvmFormalParameter _parameter_2 = this.toParameter(sourceElement, "newValue", 
        this.references.getTypeForName(Object.class, sourceElement, null));
      this.<JvmFormalParameter>operator_add(_parameters_2, _parameter_2);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("@see PropertyChangeSupport#firePropertyChange(String, Object, Object)");
      this.setDocumentation(op, _builder);
      StringConcatenationClient _client = new StringConcatenationClient() {
        @Override
        protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
          _builder.append("propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);");
          _builder.newLine();
        }
      };
      this.setBody(op, _client);
      _xblockexpression = this.<JvmOperation>associate(sourceElement, op);
    }
    return _xblockexpression;
  }
  
  public JvmOperation toMapToEntity(final LDto dto) {
    final JvmOperation op = this.typesFactory.createJvmOperation();
    op.setVisibility(JvmVisibility.PUBLIC);
    op.setReturnType(this.references.getTypeForName(Void.TYPE, dto));
    op.setSimpleName("mapToEntity");
    EList<JvmFormalParameter> _parameters = op.getParameters();
    JvmFormalParameter _parameter = this.toParameter(dto, "dto", this.findDtoTypeReference(dto));
    this.<JvmFormalParameter>operator_add(_parameters, _parameter);
    EList<JvmFormalParameter> _parameters_1 = op.getParameters();
    LType _wrappedType = dto.getWrappedType();
    JvmTypeReference _typeReference = null;
    if (_wrappedType!=null) {
      _typeReference=this._dtoModelExtensions.toTypeReference(_wrappedType);
    }
    JvmFormalParameter _parameter_1 = this.toParameter(dto, "entity", _typeReference);
    this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
    EList<JvmFormalParameter> _parameters_2 = op.getParameters();
    JvmFormalParameter _parameter_2 = this.toParameter(dto, "context", this.references.getTypeForName(MappingContext.class, dto));
    this.<JvmFormalParameter>operator_add(_parameters_2, _parameter_2);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Maps the dto {@link ");
    String _name = this._dtoModelExtensions.toName(dto);
    _builder.append(_name);
    _builder.append("} to the entity {@link ");
    String _name_1 = this._dtoModelExtensions.toName(dto.getWrappedType());
    _builder.append(_name_1);
    _builder.append("}.");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("@param dto - The source dto");
    _builder.newLine();
    _builder.append("@param entity - The target entity");
    _builder.newLine();
    _builder.append("@param context - The context to get information about depth,...");
    _builder.newLine();
    this.setDocumentation(op, _builder);
    StringConcatenationClient _client = new StringConcatenationClient() {
      @Override
      protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
        _builder.append("if(context == null){");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("throw new IllegalArgumentException(\"Please pass a context!\");");
        _builder.newLine();
        _builder.append("}");
        _builder.newLine();
        _builder.newLine();
        {
          boolean _isBean = DtoTypesBuilder.this._dtoModelExtensions.isBean(dto.getWrappedType());
          boolean _not = (!_isBean);
          if (_not) {
            _builder.append("context.register(createEntityHash(dto), entity);");
            _builder.newLine();
            _builder.append("context.registerMappingRoot(createEntityHash(dto), dto);");
            _builder.newLine();
          }
        }
        {
          LDto _superType = dto.getSuperType();
          boolean _notEquals = (!Objects.equal(_superType, null));
          if (_notEquals) {
            _builder.append("super.mapToEntity(dto, entity, context);");
            _builder.newLine();
          }
        }
        _builder.newLine();
        {
          final Function1<LDtoFeature, Boolean> _function = (LDtoFeature it) -> {
            boolean _or = false;
            boolean _or_1 = false;
            boolean _inherited = DtoTypesBuilder.this._dtoModelExtensions.inherited(it);
            if (_inherited) {
              _or_1 = true;
            } else {
              LDtoMapper _mapper = it.getMapper();
              XExpression _fromDTO = null;
              if (_mapper!=null) {
                _fromDTO=_mapper.getFromDTO();
              }
              boolean _notEquals_1 = (!Objects.equal(_fromDTO, null));
              _or_1 = _notEquals_1;
            }
            if (_or_1) {
              _or = true;
            } else {
              _or = ((it instanceof LDtoAbstractAttribute) && DtoTypesBuilder.this._dtoModelExtensions.typeIsStateClass(((LDtoAbstractAttribute) it)));
            }
            return Boolean.valueOf(_or);
          };
          Iterable<LDtoFeature> _filter = IterableExtensions.<LDtoFeature>filter(dto.getFeatures(), _function);
          for(final LDtoFeature f : _filter) {
            {
              boolean _isToMany = DtoTypesBuilder.this._dtoModelExtensions.getBounds(f).isToMany();
              boolean _not_1 = (!_isToMany);
              if (_not_1) {
                {
                  if ((((f instanceof LDtoAbstractAttribute) && DtoTypesBuilder.this._dtoModelExtensions.typeIsStateClass(((LDtoAbstractAttribute) f))) || (!DtoTypesBuilder.this._dtoModelExtensions.derived(f)))) {
                    _builder.append("entity.set");
                    String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(f));
                    _builder.append(_firstUpper);
                    _builder.append("(");
                    String _mapPropertyToEntity = DtoTypesBuilder.this._methodNamingExtensions.toMapPropertyToEntity(f);
                    _builder.append(_mapPropertyToEntity);
                    _builder.append("(dto, entity, context));");
                    _builder.newLineIfNotEmpty();
                  }
                }
              } else {
                String _mapPropertyToEntity_1 = DtoTypesBuilder.this._methodNamingExtensions.toMapPropertyToEntity(f);
                _builder.append(_mapPropertyToEntity_1);
                _builder.append("(dto, entity, context);");
                _builder.newLineIfNotEmpty();
              }
            }
          }
        }
      }
    };
    this.setBody(op, _client);
    return this.<JvmOperation>associate(dto, op);
  }
  
  /**
   * returns the mapper class type
   */
  public JvmGenericType toMapperJvmType(final LDto lDto) {
    JvmGenericType _xblockexpression = null;
    {
      final JvmGenericType type = this.createJvmGenericType(lDto, this._methodNamingExtensions.toFqnMapperName(lDto));
      _xblockexpression = this.<JvmGenericType>associate(lDto, type);
    }
    return _xblockexpression;
  }
  
  protected JvmOperation _toMapToDtoProperty(final LDtoAbstractAttribute prop) {
    JvmOperation _xblockexpression = null;
    {
      EObject _eContainer = prop.eContainer();
      final LDto dto = ((LDto) _eContainer);
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PROTECTED);
      op.setReturnType(this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(prop));
      op.setSimpleName(this._methodNamingExtensions.toMapPropertyToDto(prop));
      this.<JvmOperation>associate(prop, op);
      final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        LType _wrappedType = dto.getWrappedType();
        JvmTypeReference _typeReference = null;
        if (_wrappedType!=null) {
          _typeReference=this._dtoModelExtensions.toTypeReference(_wrappedType);
        }
        JvmFormalParameter _parameter = this.toParameter(prop, "in", _typeReference);
        this.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this.toParameter(prop, "context", this.references.getTypeForName(MappingContext.class, prop));
        this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Maps the property ");
        String _name = this._dtoModelExtensions.toName(prop);
        _builder.append(_name);
        _builder.append(" from the given entity to dto property.");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("@param in - The source entity");
        _builder.newLine();
        _builder.append("@param context - The context to get information about depth,...");
        _builder.newLine();
        _builder.append("@return the mapped value");
        _builder.newLine();
        this.setDocumentation(it, _builder);
        final XExpression mapExpression = this._methodNamingExtensions.toMapToDtoExpression(prop);
        boolean _notEquals = (!Objects.equal(mapExpression, null));
        if (_notEquals) {
          this.setBody(it, mapExpression);
        } else {
          LType _rawType = this._dtoModelExtensions.toRawType(prop);
          if ((_rawType instanceof LBean)) {
            boolean _isToMany = this._dtoModelExtensions.getBounds(prop).isToMany();
            if (_isToMany) {
              StringConcatenationClient _client = new StringConcatenationClient() {
                @Override
                protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                  _builder.append("org.eclipse.osbp.dsl.dto.lib.IMapper<");
                  String _dTOBeanSimpleName = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName);
                  _builder.append(", ");
                  String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name);
                  _builder.append("> mapper = getToDtoMapper(");
                  String _dTOBeanSimpleName_1 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_1);
                  _builder.append(".class, ");
                  String _name_1 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_1);
                  _builder.append(".class);");
                  _builder.newLineIfNotEmpty();
                  _builder.append("if(mapper == null) {");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("throw new IllegalStateException(\"Mapper must not be null!\");");
                  _builder.newLine();
                  _builder.append("} ");
                  _builder.newLine();
                  _builder.newLine();
                  _builder.append("if (context.isDetectRecursion(");
                  String _dTOBeanSimpleName_2 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_2);
                  _builder.append(".class)) {");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("return java.util.Collections.emptyList();");
                  _builder.newLine();
                  _builder.append("}");
                  _builder.newLine();
                  _builder.newLine();
                  _builder.append("context.increaseLevel();");
                  _builder.newLine();
                  _builder.append("List<");
                  String _dTOBeanSimpleName_3 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_3);
                  _builder.append("> results = new java.util.ArrayList<");
                  String _dTOBeanSimpleName_4 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_4);
                  _builder.append(">();");
                  _builder.newLineIfNotEmpty();
                  _builder.append("for (");
                  String _name_2 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_2);
                  _builder.append(" _entity : in.");
                  String _getterName = DtoTypesBuilder.this._methodNamingExtensions.toGetterName(prop);
                  _builder.append(_getterName);
                  _builder.append("()) {");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  String _qualifiedName = DtoTypesBuilder.this._dtoModelExtensions.toDtoTypeReference(prop).getQualifiedName();
                  _builder.append(_qualifiedName, "\t");
                  _builder.append(" _dto = context.get(mapper.createDtoHash(_entity));");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("if (_dto == null) {");
                  _builder.newLine();
                  _builder.append("\t\t");
                  _builder.append("_dto = mapper.createDto();");
                  _builder.newLine();
                  _builder.append("\t\t");
                  _builder.append("mapper.mapToDTO(_dto, _entity, context);");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("} else {");
                  _builder.newLine();
                  _builder.append("\t\t");
                  _builder.append("if(context.isRefresh()){");
                  _builder.newLine();
                  _builder.append("\t\t\t");
                  _builder.append("mapper.mapToDTO(_dto, _entity, context);");
                  _builder.newLine();
                  _builder.append("\t\t");
                  _builder.append("}");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("}");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("results.add(_dto);");
                  _builder.newLine();
                  _builder.append("}");
                  _builder.newLine();
                  _builder.append("context.decreaseLevel();");
                  _builder.newLine();
                  _builder.append("return results;");
                }
              };
              this.setBody(it, _client);
            } else {
              StringConcatenationClient _client_1 = new StringConcatenationClient() {
                @Override
                protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                  _builder.append("if(in.get");
                  String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                  _builder.append(_firstUpper);
                  _builder.append("() != null) {");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("// find a mapper that knows how to map the concrete input type.");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("org.eclipse.osbp.dsl.dto.lib.IMapper<");
                  String _dTOBeanSimpleName = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName, "\t");
                  _builder.append(", ");
                  String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name, "\t");
                  _builder.append("> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<");
                  String _dTOBeanSimpleName_1 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_1, "\t");
                  _builder.append(", ");
                  String _name_1 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_1, "\t");
                  _builder.append(">) getToDtoMapper(");
                  String _dTOBeanSimpleName_2 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_2, "\t");
                  _builder.append(".class, in.get");
                  String _firstUpper_1 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                  _builder.append(_firstUpper_1, "\t");
                  _builder.append("().getClass());");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("if(mapper == null) {");
                  _builder.newLine();
                  _builder.append("\t\t");
                  _builder.append("throw new IllegalStateException(\"Mapper must not be null!\");");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("}");
                  _builder.newLine();
                  _builder.newLine();
                  _builder.append("\t");
                  String _dTOBeanSimpleName_3 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_3, "\t");
                  _builder.append(" dto = null;");
                  _builder.newLineIfNotEmpty();
                  {
                    boolean _isBean = DtoTypesBuilder.this._dtoModelExtensions.isBean(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                    boolean _not = (!_isBean);
                    if (_not) {
                      _builder.append("dto = context.get(mapper.createDtoHash(in.");
                      String _getterName = DtoTypesBuilder.this._methodNamingExtensions.toGetterName(prop);
                      _builder.append(_getterName);
                      _builder.append("()));");
                      _builder.newLineIfNotEmpty();
                      _builder.append("if(dto != null) {");
                      _builder.newLine();
                      _builder.append("\t");
                      _builder.append("if(context.isRefresh()){");
                      _builder.newLine();
                      _builder.append("\t\t");
                      _builder.append("mapper.mapToDTO(dto, in.");
                      String _getterName_1 = DtoTypesBuilder.this._methodNamingExtensions.toGetterName(prop);
                      _builder.append(_getterName_1, "\t\t");
                      _builder.append("(), context);");
                      _builder.newLineIfNotEmpty();
                      _builder.append("\t");
                      _builder.append("}");
                      _builder.newLine();
                      _builder.append("\t");
                      _builder.append("return dto;");
                      _builder.newLine();
                      _builder.append("}");
                      _builder.newLine();
                      _builder.newLine();
                    }
                  }
                  _builder.append("\t");
                  _builder.append("context.increaseLevel();");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("dto = mapper.createDto();");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("mapper.mapToDTO(dto, in.");
                  String _getterName_2 = DtoTypesBuilder.this._methodNamingExtensions.toGetterName(prop);
                  _builder.append(_getterName_2, "\t");
                  _builder.append("(), context);");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("context.decreaseLevel();");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("return dto;");
                  _builder.newLine();
                  _builder.append("} else {");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("return null;");
                  _builder.newLine();
                  _builder.append("}");
                  _builder.newLine();
                }
              };
              this.setBody(it, _client_1);
            }
          } else {
            boolean _typeIsBoolean = this._dtoModelExtensions.typeIsBoolean(prop);
            if (_typeIsBoolean) {
              StringConcatenationClient _client_2 = new StringConcatenationClient() {
                @Override
                protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                  _builder.append("return in.is");
                  String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                  _builder.append(_firstUpper);
                  _builder.append("();");
                }
              };
              this.setBody(it, _client_2);
            } else {
              boolean _typeIsEnum = this._dtoModelExtensions.typeIsEnum(prop);
              if (_typeIsEnum) {
                StringConcatenationClient _client_3 = new StringConcatenationClient() {
                  @Override
                  protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                    _builder.append("if(in.get");
                    String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                    _builder.append(_firstUpper);
                    _builder.append("() != null) {");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("return ");
                    String _dTOEnumFullyQualifiedName = DtoTypesBuilder.this._methodNamingExtensions.toDTOEnumFullyQualifiedName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                    _builder.append(_dTOEnumFullyQualifiedName, "\t");
                    _builder.append(".valueOf(in.get");
                    String _firstUpper_1 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                    _builder.append(_firstUpper_1, "\t");
                    _builder.append("().name());");
                    _builder.newLineIfNotEmpty();
                    _builder.append("} else {");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("return null;");
                    _builder.newLine();
                    _builder.append("}");
                    _builder.newLine();
                  }
                };
                this.setBody(it, _client_3);
              } else {
                boolean _typeIsStateClass = this._dtoModelExtensions.typeIsStateClass(prop);
                if (_typeIsStateClass) {
                  StringConcatenationClient _client_4 = new StringConcatenationClient() {
                    @Override
                    protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                      _builder.append("if(in.get");
                      String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                      _builder.append(_firstUpper);
                      _builder.append("() != null) {");
                      _builder.newLineIfNotEmpty();
                      _builder.append("\t");
                      _builder.append("return ");
                      String _dTOStateClassFullyQualifiedName = DtoTypesBuilder.this._methodNamingExtensions.toDTOStateClassFullyQualifiedName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                      _builder.append(_dTOStateClassFullyQualifiedName, "\t");
                      _builder.append(".valueOf(in.get");
                      String _firstUpper_1 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                      _builder.append(_firstUpper_1, "\t");
                      _builder.append("().name());");
                      _builder.newLineIfNotEmpty();
                      _builder.append("} else {");
                      _builder.newLine();
                      _builder.append("\t");
                      _builder.append("return null;");
                      _builder.newLine();
                      _builder.append("}");
                      _builder.newLine();
                    }
                  };
                  this.setBody(it, _client_4);
                } else {
                  StringConcatenationClient _client_5 = new StringConcatenationClient() {
                    @Override
                    protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                      _builder.append("return in.get");
                      String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                      _builder.append(_firstUpper);
                      _builder.append("();");
                    }
                  };
                  this.setBody(it, _client_5);
                }
              }
            }
          }
        }
      };
      _xblockexpression = this.<JvmOperation>initializeSafely(op, _function);
    }
    return _xblockexpression;
  }
  
  protected JvmOperation _toMapToDtoProperty(final LDtoAbstractReference prop) {
    JvmOperation _xblockexpression = null;
    {
      EObject _eContainer = prop.eContainer();
      final LDto dto = ((LDto) _eContainer);
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PROTECTED);
      op.setReturnType(this._dtoModelExtensions.toDtoTypeReferenceWithMultiplicity(prop));
      op.setSimpleName(this._methodNamingExtensions.toMapPropertyToDto(prop));
      this.<JvmOperation>associate(prop, op);
      final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        LType _wrappedType = dto.getWrappedType();
        JvmTypeReference _typeReference = null;
        if (_wrappedType!=null) {
          _typeReference=this._dtoModelExtensions.toTypeReference(_wrappedType);
        }
        JvmFormalParameter _parameter = this.toParameter(prop, "in", _typeReference);
        this.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this.toParameter(prop, "context", this.references.getTypeForName(MappingContext.class, prop));
        this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Maps the property ");
        String _name = this._dtoModelExtensions.toName(prop);
        _builder.append(_name);
        _builder.append(" from the given entity to the dto.");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("@param in - The source entity");
        _builder.newLine();
        _builder.append("@param context - The context to get information about depth,...");
        _builder.newLine();
        _builder.append("@return ");
        {
          boolean _isToMany = this._dtoModelExtensions.getBounds(prop).isToMany();
          boolean _not = (!_isToMany);
          if (_not) {
            _builder.append("the mapped dto");
          } else {
            _builder.append("A list of mapped dtos");
          }
        }
        _builder.newLineIfNotEmpty();
        this.setDocumentation(it, _builder);
        final XExpression mapExpression = this._methodNamingExtensions.toMapToDtoExpression(prop);
        boolean _notEquals = (!Objects.equal(mapExpression, null));
        if (_notEquals) {
          this.setBody(it, mapExpression);
        } else {
          boolean _isToMany_1 = this._dtoModelExtensions.getBounds(prop).isToMany();
          if (_isToMany_1) {
            StringConcatenationClient _client = new StringConcatenationClient() {
              @Override
              protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                _builder.append("// nothing to do here. Mapping is done by OppositeLists");
                _builder.newLine();
                _builder.append("return null;");
              }
            };
            this.setBody(it, _client);
          } else {
            StringConcatenationClient _client_1 = new StringConcatenationClient() {
              @Override
              protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                _builder.append("if(in.get");
                String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                _builder.append(_firstUpper);
                _builder.append("() != null) {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("// find a mapper that knows how to map the concrete input type.");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("org.eclipse.osbp.dsl.dto.lib.IMapper<");
                String _typeName = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName, "\t");
                _builder.append(", ");
                String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                _builder.append(_name, "\t");
                _builder.append("> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<");
                String _typeName_1 = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName_1, "\t");
                _builder.append(", ");
                String _name_1 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                _builder.append(_name_1, "\t");
                _builder.append(">) getToDtoMapper(");
                String _typeName_2 = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName_2, "\t");
                _builder.append(".class, in.get");
                String _firstUpper_1 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                _builder.append(_firstUpper_1, "\t");
                _builder.append("().getClass());");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("if(mapper == null) {");
                _builder.newLine();
                _builder.append("\t\t");
                _builder.append("throw new IllegalStateException(\"Mapper must not be null!\");");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("}");
                _builder.newLine();
                _builder.append("\t");
                String _typeName_3 = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName_3, "\t");
                _builder.append(" dto = null;");
                _builder.newLineIfNotEmpty();
                {
                  boolean _isBean = DtoTypesBuilder.this._dtoModelExtensions.isBean(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  boolean _not = (!_isBean);
                  if (_not) {
                    _builder.append("\t");
                    _builder.append("dto = context.get(mapper.createDtoHash(in.get");
                    String _firstUpper_2 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                    _builder.append(_firstUpper_2, "\t");
                    _builder.append("()));");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("if(dto != null) {");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("\t");
                    _builder.append("if(context.isRefresh()){");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("\t\t");
                    _builder.append("mapper.mapToDTO(dto, in.");
                    String _getterName = DtoTypesBuilder.this._methodNamingExtensions.toGetterName(prop);
                    _builder.append(_getterName, "\t\t\t");
                    _builder.append("(), context);");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("\t");
                    _builder.append("}");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("\t");
                    _builder.append("return dto;");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("}");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.newLine();
                  }
                }
                _builder.append("\t");
                _builder.append("context.increaseLevel();");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("dto = mapper.createDto();");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("mapper.mapToDTO(dto, in.get");
                String _firstUpper_3 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                _builder.append(_firstUpper_3, "\t");
                _builder.append("(), context);");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("context.decreaseLevel();");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("return dto;");
                _builder.newLine();
                _builder.append("} else {");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("return null;");
                _builder.newLine();
                _builder.append("}");
                _builder.newLine();
              }
            };
            this.setBody(it, _client_1);
          }
        }
      };
      _xblockexpression = this.<JvmOperation>initializeSafely(op, _function);
    }
    return _xblockexpression;
  }
  
  public JvmGenericType findJvmType(final LDto lDto) {
    EObject _primaryJvmElement = ((IJvmModelAssociations) this.associator).getPrimaryJvmElement(lDto);
    return ((JvmGenericType) _primaryJvmElement);
  }
  
  protected JvmOperation _toMapToEntityProperty(final LDtoAbstractAttribute prop) {
    JvmOperation _xblockexpression = null;
    {
      EObject _eContainer = prop.eContainer();
      final LDto dto = ((LDto) _eContainer);
      final JvmGenericType dtoJvmType = this.findJvmType(dto);
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PROTECTED);
      op.setReturnType(this._dtoModelExtensions.toRawTypeReferenceWithMultiplicity(prop));
      op.setSimpleName(this._methodNamingExtensions.toMapPropertyToEntity(prop));
      this.<JvmOperation>associate(prop, op);
      final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this.toParameter(prop, "in", this.newTypeRef(dtoJvmType, null));
        this.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        LType _wrappedType = dto.getWrappedType();
        JvmTypeReference _typeReference = null;
        if (_wrappedType!=null) {
          _typeReference=this._dtoModelExtensions.toTypeReference(_wrappedType);
        }
        JvmFormalParameter _parameter_1 = this.toParameter(prop, "parentEntity", _typeReference);
        this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        EList<JvmFormalParameter> _parameters_2 = it.getParameters();
        JvmFormalParameter _parameter_2 = this.toParameter(prop, "context", this.references.getTypeForName(MappingContext.class, prop));
        this.<JvmFormalParameter>operator_add(_parameters_2, _parameter_2);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Maps the property ");
        String _name = this._dtoModelExtensions.toName(prop);
        _builder.append(_name);
        _builder.append(" from the given entity to dto property.");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("@param in - The source entity");
        _builder.newLine();
        _builder.append("@param parentEntity - The parentEntity");
        _builder.newLine();
        _builder.append("@param context - The context to get information about depth,...");
        _builder.newLine();
        _builder.append("@return the mapped value");
        _builder.newLine();
        this.setDocumentation(it, _builder);
        final XExpression mapExpression = this._methodNamingExtensions.toMapToEntityExpression(prop);
        boolean _notEquals = (!Objects.equal(mapExpression, null));
        if (_notEquals) {
          this.setBody(it, mapExpression);
        } else {
          LType _rawType = this._dtoModelExtensions.toRawType(prop);
          if ((_rawType instanceof LBean)) {
            boolean _isToMany = this._dtoModelExtensions.getBounds(prop).isToMany();
            if (_isToMany) {
              StringConcatenationClient _client = new StringConcatenationClient() {
                @Override
                protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                  _builder.append("org.eclipse.osbp.dsl.dto.lib.IMapper<");
                  String _dTOBeanSimpleName = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName);
                  _builder.append(", ");
                  String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name);
                  _builder.append("> mapper = getToEntityMapper(");
                  String _dTOBeanSimpleName_1 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_1);
                  _builder.append(".class, ");
                  String _name_1 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_1);
                  _builder.append(".class);");
                  _builder.newLineIfNotEmpty();
                  _builder.append("if(mapper == null) {");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("throw new IllegalStateException(\"Mapper must not be null!\");");
                  _builder.newLine();
                  _builder.append("}");
                  _builder.newLine();
                  _builder.newLine();
                  _builder.append("List<");
                  String _name_2 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_2);
                  _builder.append("> results = new java.util.ArrayList<");
                  String _name_3 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_3);
                  _builder.append(">();");
                  _builder.newLineIfNotEmpty();
                  _builder.append("for (");
                  String _dTOBeanSimpleName_2 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_2);
                  _builder.append(" _dto : in.");
                  String _getterName = DtoTypesBuilder.this._methodNamingExtensions.toGetterName(prop);
                  _builder.append(_getterName);
                  _builder.append("()) {");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  String _name_4 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_4, "\t");
                  _builder.append(" _entity = mapper.createEntity();");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("mapper.mapToEntity(_dto, _entity, context);");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("results.add(_entity);");
                  _builder.newLine();
                  _builder.append("}");
                  _builder.newLine();
                  _builder.append("return results;");
                }
              };
              this.setBody(it, _client);
            } else {
              StringConcatenationClient _client_1 = new StringConcatenationClient() {
                @Override
                protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                  _builder.append("if(in.get");
                  String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                  _builder.append(_firstUpper);
                  _builder.append("() != null) {");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("// find a mapper that knows how to map the concrete input type.");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("org.eclipse.osbp.dsl.dto.lib.IMapper<");
                  String _dTOBeanSimpleName = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName, "\t");
                  _builder.append(", ");
                  String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name, "\t");
                  _builder.append("> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<");
                  String _dTOBeanSimpleName_1 = DtoTypesBuilder.this._methodNamingExtensions.toDTOBeanSimpleName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_dTOBeanSimpleName_1, "\t");
                  _builder.append(", ");
                  String _name_1 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_1, "\t");
                  _builder.append(">) getToEntityMapper(in.get");
                  String _firstUpper_1 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                  _builder.append(_firstUpper_1, "\t");
                  _builder.append("().getClass(), ");
                  String _name_2 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_2, "\t");
                  _builder.append(".class);");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("if(mapper == null) {");
                  _builder.newLine();
                  _builder.append("\t\t");
                  _builder.append("throw new IllegalStateException(\"Mapper must not be null!\");");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("}");
                  _builder.newLine();
                  _builder.newLine();
                  _builder.append("\t");
                  String _name_3 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  _builder.append(_name_3, "\t");
                  _builder.append(" entity = mapper.createEntity();");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("mapper.mapToEntity(in.");
                  String _getterName = DtoTypesBuilder.this._methodNamingExtensions.toGetterName(prop);
                  _builder.append(_getterName, "\t");
                  _builder.append("(), entity, context);");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t");
                  _builder.append("return entity;\t\t\t\t\t\t\t");
                  _builder.newLine();
                  _builder.append("} else {");
                  _builder.newLine();
                  _builder.append("\t");
                  _builder.append("return null;");
                  _builder.newLine();
                  _builder.append("}");
                }
              };
              this.setBody(it, _client_1);
            }
          } else {
            boolean _typeIsBoolean = this._dtoModelExtensions.typeIsBoolean(prop);
            if (_typeIsBoolean) {
              StringConcatenationClient _client_2 = new StringConcatenationClient() {
                @Override
                protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                  _builder.append("return in.is");
                  String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                  _builder.append(_firstUpper);
                  _builder.append("();");
                }
              };
              this.setBody(it, _client_2);
            } else {
              boolean _typeIsEnum = this._dtoModelExtensions.typeIsEnum(prop);
              if (_typeIsEnum) {
                StringConcatenationClient _client_3 = new StringConcatenationClient() {
                  @Override
                  protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                    _builder.append("if(in.get");
                    String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                    _builder.append(_firstUpper);
                    _builder.append("() != null) {");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("return ");
                    String _string = DtoTypesBuilder.this._iQualifiedNameProvider.getFullyQualifiedName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop)).toString();
                    _builder.append(_string, "\t");
                    _builder.append(".valueOf(in.get");
                    String _firstUpper_1 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                    _builder.append(_firstUpper_1, "\t");
                    _builder.append("().name());");
                    _builder.newLineIfNotEmpty();
                    _builder.append("} else {");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("return null;");
                    _builder.newLine();
                    _builder.append("}");
                    _builder.newLine();
                  }
                };
                this.setBody(it, _client_3);
              } else {
                boolean _typeIsStateClass = this._dtoModelExtensions.typeIsStateClass(prop);
                if (_typeIsStateClass) {
                  EObject _eContainer_1 = prop.eContainer();
                  String _string = this._iQualifiedNameProvider.getFullyQualifiedName(((LAutoInheritDto) _eContainer_1).getWrappedType()).skipLast(1).toString();
                  String _plus = CommonTypesBuilder.operator_plus(_string, ".");
                  String _firstUpper = StringExtensions.toFirstUpper(this._dtoModelExtensions.toName(prop));
                  final String name = CommonTypesBuilder.operator_plus(_plus, _firstUpper);
                  op.setReturnType(this.references.getTypeForName(name, prop));
                  StringConcatenationClient _client_4 = new StringConcatenationClient() {
                    @Override
                    protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                      _builder.append("if(in.get");
                      String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                      _builder.append(_firstUpper);
                      _builder.append("() != null) {");
                      _builder.newLineIfNotEmpty();
                      _builder.append("\t");
                      _builder.append("return ");
                      _builder.append(name, "\t");
                      _builder.append(".valueOf(in.get");
                      String _firstUpper_1 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                      _builder.append(_firstUpper_1, "\t");
                      _builder.append("().name());");
                      _builder.newLineIfNotEmpty();
                      _builder.append("} else {");
                      _builder.newLine();
                      _builder.append("\t");
                      _builder.append("return null;");
                      _builder.newLine();
                      _builder.append("}");
                      _builder.newLine();
                    }
                  };
                  this.setBody(it, _client_4);
                } else {
                  StringConcatenationClient _client_5 = new StringConcatenationClient() {
                    @Override
                    protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                      _builder.append("return in.get");
                      String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                      _builder.append(_firstUpper);
                      _builder.append("();");
                    }
                  };
                  this.setBody(it, _client_5);
                }
              }
            }
          }
        }
      };
      _xblockexpression = this.<JvmOperation>initializeSafely(op, _function);
    }
    return _xblockexpression;
  }
  
  protected JvmOperation _toMapToEntityProperty(final LDtoAbstractReference prop) {
    JvmOperation _xblockexpression = null;
    {
      EObject _eContainer = prop.eContainer();
      final LDto dto = ((LDto) _eContainer);
      final JvmOperation op = this.typesFactory.createJvmOperation();
      op.setVisibility(JvmVisibility.PROTECTED);
      op.setReturnType(this._dtoModelExtensions.toRawTypeReferenceWithMultiplicity(prop));
      op.setSimpleName(this._methodNamingExtensions.toMapPropertyToEntity(prop));
      this.<JvmOperation>associate(prop, op);
      final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this.toParameter(prop, "in", this.findDtoTypeReference(dto));
        this.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        LType _wrappedType = dto.getWrappedType();
        JvmTypeReference _typeReference = null;
        if (_wrappedType!=null) {
          _typeReference=this._dtoModelExtensions.toTypeReference(_wrappedType);
        }
        JvmFormalParameter _parameter_1 = this.toParameter(prop, "parentEntity", _typeReference);
        this.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        EList<JvmFormalParameter> _parameters_2 = it.getParameters();
        JvmFormalParameter _parameter_2 = this.toParameter(prop, "context", this.references.getTypeForName(MappingContext.class, prop));
        this.<JvmFormalParameter>operator_add(_parameters_2, _parameter_2);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("Maps the property ");
        String _name = this._dtoModelExtensions.toName(prop);
        _builder.append(_name);
        _builder.append(" from the given dto to the entity.");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
        _builder.append("@param in - The source dto");
        _builder.newLine();
        _builder.append("@param parentEntity - The parent entity");
        _builder.newLine();
        _builder.append("@param context - The context to get information about depth,...");
        _builder.newLine();
        _builder.append("@return ");
        {
          boolean _isToMany = this._dtoModelExtensions.getBounds(prop).isToMany();
          boolean _not = (!_isToMany);
          if (_not) {
            _builder.append("the mapped entity");
          } else {
            _builder.append("A list of mapped entities");
          }
        }
        _builder.newLineIfNotEmpty();
        this.setDocumentation(it, _builder);
        final XExpression mapExpression = this._methodNamingExtensions.toMapToEntityExpression(prop);
        boolean _notEquals = (!Objects.equal(mapExpression, null));
        if (_notEquals) {
          this.setBody(it, mapExpression);
        } else {
          boolean _isToMany_1 = this._dtoModelExtensions.getBounds(prop).isToMany();
          if (_isToMany_1) {
            StringConcatenationClient _client = new StringConcatenationClient() {
              @Override
              protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                _builder.append("org.eclipse.osbp.dsl.dto.lib.IMapper<");
                String _typeName = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName);
                _builder.append(", ");
                String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                _builder.append(_name);
                _builder.append("> mapper = getToEntityMapper(");
                String _typeName_1 = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName_1);
                _builder.append(".class, ");
                String _name_1 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                _builder.append(_name_1);
                _builder.append(".class);");
                _builder.newLineIfNotEmpty();
                _builder.append("if(mapper == null) {");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("throw new IllegalStateException(\"Mapper must not be null!\");");
                _builder.newLine();
                _builder.append("}");
                _builder.newLine();
                _builder.newLine();
                _builder.append("org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<");
                String _typeName_2 = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName_2);
                _builder.append("> childsList = ");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<");
                String _typeName_3 = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName_3, "\t");
                _builder.append(">) in.internalGet");
                String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                _builder.append(_firstUpper, "\t");
                _builder.append("();");
                _builder.newLineIfNotEmpty();
                _builder.newLine();
                _builder.append("// if entities are being added, then they are passed to");
                _builder.newLine();
                _builder.append("// #addToContainerChilds of the parent entity. So the container ref is setup");
                _builder.newLine();
                _builder.append("// properly!");
                _builder.newLine();
                _builder.append("// if entities are being removed, then they are passed to the");
                _builder.newLine();
                _builder.append("// #internalRemoveFromChilds method of the parent entity. So they are");
                _builder.newLine();
                _builder.append("// removed directly from the list of entities.");
                _builder.newLine();
                _builder.append("if ( childsList != null ) childsList.mapToEntity(mapper,");
                _builder.newLine();
                _builder.append("\t\t");
                _builder.append("parentEntity::addTo");
                String _firstUpper_1 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                _builder.append(_firstUpper_1, "\t\t");
                _builder.append(",");
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t");
                _builder.append("parentEntity::internalRemoveFrom");
                String _firstUpper_2 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                _builder.append(_firstUpper_2, "\t\t");
                _builder.append(");");
                _builder.newLineIfNotEmpty();
                _builder.append("return null;");
                _builder.newLine();
              }
            };
            this.setBody(it, _client);
          } else {
            StringConcatenationClient _client_1 = new StringConcatenationClient() {
              @Override
              protected void appendTo(StringConcatenationClient.TargetStringConcatenation _builder) {
                _builder.append("if(in.get");
                String _firstUpper = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                _builder.append(_firstUpper);
                _builder.append("() != null) {");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("// find a mapper that knows how to map the concrete input type.");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("org.eclipse.osbp.dsl.dto.lib.IMapper<");
                String _typeName = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName, "\t");
                _builder.append(", ");
                String _name = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                _builder.append(_name, "\t");
                _builder.append("> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<");
                String _typeName_1 = DtoTypesBuilder.this._dtoModelExtensions.toTypeName(prop);
                _builder.append(_typeName_1, "\t");
                _builder.append(", ");
                String _name_1 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                _builder.append(_name_1, "\t");
                _builder.append(">) getToEntityMapper(in.get");
                String _firstUpper_1 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                _builder.append(_firstUpper_1, "\t");
                _builder.append("().getClass(), ");
                String _name_2 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                _builder.append(_name_2, "\t");
                _builder.append(".class);");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("if(mapper == null) {");
                _builder.newLine();
                _builder.append("\t\t");
                _builder.append("throw new IllegalStateException(\"Mapper must not be null!\");");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("}");
                _builder.newLine();
                _builder.newLine();
                _builder.append("\t");
                String _name_3 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                _builder.append(_name_3, "\t");
                _builder.append(" entity = null;");
                _builder.newLineIfNotEmpty();
                {
                  boolean _isBean = DtoTypesBuilder.this._dtoModelExtensions.isBean(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                  boolean _not = (!_isBean);
                  if (_not) {
                    _builder.append("\t");
                    _builder.append("entity = context.get(mapper.createEntityHash(in.get");
                    String _firstUpper_2 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                    _builder.append(_firstUpper_2, "\t");
                    _builder.append("()));");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("if(entity != null) {");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("\t");
                    _builder.append("return entity;");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("} else {");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("\t");
                    _builder.append("entity = (");
                    String _name_4 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                    _builder.append(_name_4, "\t\t");
                    _builder.append(") context");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("\t\t");
                    _builder.append(".findEntityByEntityManager(");
                    String _name_5 = DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop));
                    _builder.append(_name_5, "\t\t\t");
                    _builder.append(".class, in.get");
                    String _firstUpper_3 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                    _builder.append(_firstUpper_3, "\t\t\t");
                    _builder.append("().get");
                    String _firstUpper_4 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(DtoTypesBuilder.this._dtoModelExtensions.idAttribute(DtoTypesBuilder.this._dtoModelExtensions.toRawType(prop))));
                    _builder.append(_firstUpper_4, "\t\t\t");
                    _builder.append("());");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("\t");
                    _builder.append("if (entity != null) {");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("\t\t");
                    _builder.append("context.register(mapper.createEntityHash(in.get");
                    String _firstUpper_5 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                    _builder.append(_firstUpper_5, "\t\t\t");
                    _builder.append("()), entity);");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t");
                    _builder.append("\t\t");
                    _builder.append("return entity;");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("\t");
                    _builder.append("}");
                    _builder.newLine();
                    _builder.append("\t");
                    _builder.append("}");
                    _builder.newLine();
                    _builder.newLine();
                  }
                }
                _builder.append("\t");
                _builder.append("entity = mapper.createEntity();");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("mapper.mapToEntity(in.get");
                String _firstUpper_6 = StringExtensions.toFirstUpper(DtoTypesBuilder.this._dtoModelExtensions.toName(prop));
                _builder.append(_firstUpper_6, "\t");
                _builder.append("(), entity, context);\t");
                _builder.newLineIfNotEmpty();
                _builder.append("\t");
                _builder.append("return entity;");
                _builder.newLine();
                _builder.append("} else {");
                _builder.newLine();
                _builder.append("\t");
                _builder.append("return null;");
                _builder.newLine();
                _builder.append("}\t");
                _builder.newLine();
              }
            };
            this.setBody(it, _client_1);
          }
        }
      };
      _xblockexpression = this.<JvmOperation>initializeSafely(op, _function);
    }
    return _xblockexpression;
  }
  
  public JvmTypeReference findDtoTypeReference(final LDto dto) {
    return this.newTypeRef(this.getByPostfix(dto, "Dto"), null);
  }
  
  public JvmTypeReference findDtoMapperTypeReference(final LDto dto) {
    return this.newTypeRef(this.getByPostfix(dto, "DtoMapper"), null);
  }
  
  public JvmParameterizedTypeReference findSuperDtoMapperType(final LDto dto, final JvmTypeReference dtoType, final JvmTypeReference entityType) {
    JvmGenericType type = this.getByPostfix(dto.getSuperType(), "DtoMapper");
    if ((type == null)) {
      QualifiedName superTypeFQN = this._iQualifiedNameProvider.getFullyQualifiedName(dto.getSuperType());
      superTypeFQN = superTypeFQN.append("Mapper");
      final List<String> list = CollectionLiterals.<String>newArrayList();
      list.addAll(superTypeFQN.getSegments());
      int _size = list.size();
      int _minus = (_size - 1);
      list.add(_minus, "mapper");
      final QualifiedName mapperFQN = QualifiedName.create(list);
      JvmTypeReference _typeForName = this.references.getTypeForName(mapperFQN.toString(), dto, null);
      final JvmParameterizedTypeReference ref = ((JvmParameterizedTypeReference) _typeForName);
      EList<JvmTypeReference> _arguments = ref.getArguments();
      this.<JvmTypeReference>operator_add(_arguments, dtoType);
      EList<JvmTypeReference> _arguments_1 = ref.getArguments();
      this.<JvmTypeReference>operator_add(_arguments_1, entityType);
      return ref;
    } else {
      JvmTypeReference _newTypeRef = this.newTypeRef(type, null);
      final JvmParameterizedTypeReference ref_1 = ((JvmParameterizedTypeReference) _newTypeRef);
      ref_1.getArguments().clear();
      EList<JvmTypeReference> _arguments_2 = ref_1.getArguments();
      this.<JvmTypeReference>operator_add(_arguments_2, dtoType);
      EList<JvmTypeReference> _arguments_3 = ref_1.getArguments();
      this.<JvmTypeReference>operator_add(_arguments_3, entityType);
      return ref_1;
    }
  }
  
  public JvmOperation toSetter(final LDtoFeature prop) {
    if (prop instanceof LDtoAbstractAttribute) {
      return _toSetter((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toSetter((LDtoAbstractReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public JvmOperation toGetter(final EObject prop, final String methodName) {
    if (prop instanceof LStateClass) {
      return _toGetter((LStateClass)prop, methodName);
    } else if (prop instanceof LDtoAbstractAttribute) {
      return _toGetter((LDtoAbstractAttribute)prop, methodName);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toGetter((LDtoAbstractReference)prop, methodName);
    } else if (prop instanceof LAttribute) {
      return _toGetter((LAttribute)prop, methodName);
    } else if (prop instanceof LReference) {
      return _toGetter((LReference)prop, methodName);
    } else if (prop != null) {
      return _toGetter(prop, methodName);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop, methodName).toString());
    }
  }
  
  public JvmOperation toInternalSetter(final LDtoFeature prop) {
    if (prop instanceof LDtoAbstractAttribute) {
      return _toInternalSetter((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toInternalSetter((LDtoAbstractReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public JvmOperation toInternalAdder(final LDtoFeature prop) {
    if (prop instanceof LDtoAbstractAttribute) {
      return _toInternalAdder((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toInternalAdder((LDtoAbstractReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public JvmOperation toInternalRemover(final LDtoFeature prop) {
    if (prop instanceof LDtoAbstractAttribute) {
      return _toInternalRemover((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toInternalRemover((LDtoAbstractReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public JvmOperation toInternalCollectionGetter(final LDtoFeature prop, final String name) {
    if (prop instanceof LDtoAbstractAttribute) {
      return _toInternalCollectionGetter((LDtoAbstractAttribute)prop, name);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toInternalCollectionGetter((LDtoAbstractReference)prop, name);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop, name).toString());
    }
  }
  
  public JvmOperation toRemover(final LDtoFeature prop, final String propertyName) {
    if (prop instanceof LDtoAbstractAttribute) {
      return _toRemover((LDtoAbstractAttribute)prop, propertyName);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toRemover((LDtoAbstractReference)prop, propertyName);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop, propertyName).toString());
    }
  }
  
  public JvmOperation toMapToDtoProperty(final LDtoFeature prop) {
    if (prop instanceof LDtoAbstractAttribute) {
      return _toMapToDtoProperty((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toMapToDtoProperty((LDtoAbstractReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public JvmOperation toMapToEntityProperty(final LDtoFeature prop) {
    if (prop instanceof LDtoAbstractAttribute) {
      return _toMapToEntityProperty((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toMapToEntityProperty((LDtoAbstractReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
}
