/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.extensions;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.common.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.dto.xtext.extensions.MethodNamingExtensions;
import org.eclipse.osbp.dsl.semantic.common.helper.Bounds;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LEnum;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoFeature;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoOperation;
import org.eclipse.osbp.dsl.semantic.dto.LDtoReference;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.TypesFactory;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class DtoModelExtensions extends ModelExtensions {
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  @Inject
  @Extension
  private MethodNamingExtensions _methodNamingExtensions;
  
  @Inject
  private TypeReferences references;
  
  /**
   * Creates a type reference with respect to mappings to DTOs. For instance "Item"-Entity is mapped to ItemDTO
   */
  protected JvmTypeReference _toDtoTypeReference(final LDtoAbstractReference prop) {
    return this.toTypeReference(prop.getType());
  }
  
  /**
   * Creates a type reference with respect to mappings to DTOs. For instance "Item"-Entity is mapped to ItemDTO
   */
  protected JvmTypeReference _toDtoTypeReference(final LDtoAbstractAttribute prop) {
    return this.toTypeReference(prop);
  }
  
  /**
   * Creates a type reference with respect to mappings to DTOs. For instance "Item"-Entity is mapped to ItemDTO
   */
  protected JvmTypeReference _toDtoTypeReference(final LEntityAttribute prop, final LDtoInheritedAttribute dtoAtt) {
    LScalarType _type = prop.getType();
    if ((_type instanceof LEnum)) {
      LScalarType _type_1 = prop.getType();
      final LEnum type = ((LEnum) _type_1);
      final String fqn = this._methodNamingExtensions.toDTOEnumFullyQualifiedName(type).toString();
      return this.references.getTypeForName(fqn, type, null);
    } else {
      if (((prop.getType() instanceof LBean) || (prop.getType() instanceof LEntity))) {
        return this.toTypeReference(dtoAtt.getInheritedFeature());
      } else {
        return super.toTypeReference(prop);
      }
    }
  }
  
  protected JvmTypeReference _toDtoTypeReference(final LDtoInheritedAttribute prop, final LDtoInheritedAttribute dtoAtt) {
    return null;
  }
  
  /**
   * Creates a type reference with respect to mappings to DTOs. For instance "Item"-Entity is mapped to ItemDTO
   */
  protected JvmTypeReference _toDtoTypeReference(final LBeanAttribute prop, final LDtoInheritedAttribute dtoAtt) {
    LScalarType _type = prop.getType();
    if ((_type instanceof LEnum)) {
      LScalarType _type_1 = prop.getType();
      final LEnum type = ((LEnum) _type_1);
      final String fqn = this._methodNamingExtensions.toDTOEnumFullyQualifiedName(type).toString();
      return this.references.getTypeForName(fqn, type, null);
    } else {
      if (((prop.getType() instanceof LBean) || (prop.getType() instanceof LEntity))) {
        return this.toTypeReference(dtoAtt.getInheritedFeature());
      } else {
        return super.toTypeReference(prop);
      }
    }
  }
  
  /**
   * Creates a type reference with respect to mappings to DTOs. For instance "Item"-Entity is mapped to ItemDTO
   */
  protected JvmTypeReference _toDtoTypeReference(final LEntityReference prop, final LDtoInheritedReference dtoRef) {
    return this.toTypeReference(dtoRef.getInheritedFeature());
  }
  
  /**
   * Creates a type reference with respect to mappings to DTOs. For instance "Item"-Entity is mapped to ItemDTO
   */
  protected JvmTypeReference _toDtoTypeReference(final LBeanReference prop, final LDtoInheritedAttribute dtoRef) {
    return this.toTypeReference(dtoRef.getInheritedFeature());
  }
  
  /**
   * Creates a type reference with respect to mappings to DTOs. For instance "Item"-Entity is mapped to ItemDTO
   */
  protected JvmTypeReference _toDtoTypeReference(final LDtoInheritedAttribute prop) {
    LScalarType _type = prop.getType();
    boolean _tripleNotEquals = (_type != null);
    if (_tripleNotEquals) {
      return this.toTypeReference(prop.getType());
    }
    LAttribute _inheritedFeature = prop.getInheritedFeature();
    JvmTypeReference _dtoTypeReference = null;
    if (_inheritedFeature!=null) {
      _dtoTypeReference=this.toDtoTypeReference(_inheritedFeature, prop);
    }
    return this._jvmTypesBuilder.cloneWithProxies(_dtoTypeReference);
  }
  
  /**
   * Creates a type reference with respect to mappings to DTOs. For instance "Item"-Entity is mapped to ItemDTO
   */
  protected JvmTypeReference _toDtoTypeReference(final LDtoInheritedReference prop) {
    LDto _type = prop.getType();
    boolean _tripleNotEquals = (_type != null);
    if (_tripleNotEquals) {
      return this.toTypeReference(prop.getType());
    }
    LReference _inheritedFeature = prop.getInheritedFeature();
    JvmTypeReference _dtoTypeReference = null;
    if (_inheritedFeature!=null) {
      _dtoTypeReference=this.toDtoTypeReference(_inheritedFeature, prop);
    }
    return this._jvmTypesBuilder.cloneWithProxies(_dtoTypeReference);
  }
  
  /**
   * Creates a type reference with respect to mappings to DTOs. For instance "Item"-Entity is mapped to ItemDTO
   */
  public JvmTypeReference toDtoTypeReferenceWithMultiplicity(final LDtoFeature prop) {
    JvmTypeReference ref = this.toDtoTypeReference(prop);
    if (((ref != null) && this.getBounds(prop).isToMany())) {
      ref = this.references.getTypeForName(List.class, prop, ref);
    }
    return ref;
  }
  
  /**
   * Creates a type reference with respect to multiplicity
   */
  public JvmTypeReference toRawTypeReferenceWithMultiplicity(final LDtoFeature prop) {
    JvmTypeReference ref = this.toRawTypeRefernce(prop);
    boolean _isToMany = this.getBounds(prop).isToMany();
    if (_isToMany) {
      ref = this.references.getTypeForName(List.class, prop, ref);
    }
    return ref;
  }
  
  protected boolean _isCascading(final LDtoOperation prop) {
    return false;
  }
  
  public LDtoAbstractReference getResolvedOpposite(final LDtoReference prop) {
    LDtoAbstractReference _xifexpression = null;
    LDtoReference _opposite = prop.getOpposite();
    boolean _tripleNotEquals = (_opposite != null);
    if (_tripleNotEquals) {
      return prop.getOpposite();
    } else {
      LDtoAbstractReference _xifexpression_1 = null;
      LDto _type = prop.getType();
      if ((_type instanceof LDto)) {
        LDtoAbstractReference _xblockexpression = null;
        {
          LDto _type_1 = prop.getType();
          final LDto ref = ((LDto) _type_1);
          final Function1<LDtoAbstractReference, Boolean> _function = (LDtoAbstractReference it) -> {
            LFeature _opposite_1 = this.opposite(it);
            return Boolean.valueOf((_opposite_1 == prop));
          };
          _xblockexpression = IterableExtensions.<LDtoAbstractReference>findFirst(ref.getReferences(), _function);
        }
        _xifexpression_1 = _xblockexpression;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  /**
   * Returns true, if toCheck can be cast to superType
   */
  public boolean isCastable(final LDto toCheck, final LDto superType) {
    final String toCheckFqn = this._iQualifiedNameProvider.getFullyQualifiedName(toCheck).toString();
    final String superTypeFqn = this._iQualifiedNameProvider.getFullyQualifiedName(superType).toString();
    boolean _equals = toCheckFqn.equals(superTypeFqn);
    if (_equals) {
      return true;
    } else {
      final LDto toCheckSuperType = toCheck.getSuperType();
      if ((toCheckSuperType != null)) {
        return this.isCastable(toCheckSuperType, superType);
      } else {
        return false;
      }
    }
  }
  
  protected boolean _inherited(final LFeature prop) {
    return false;
  }
  
  protected boolean _inherited(final LDtoInheritedReference prop) {
    return true;
  }
  
  protected boolean _inherited(final LDtoInheritedAttribute prop) {
    return true;
  }
  
  protected boolean _derived(final LDtoInheritedReference prop) {
    return false;
  }
  
  protected boolean _derived(final LDtoInheritedAttribute prop) {
    boolean _xifexpression = false;
    LAttribute _inheritedFeature = prop.getInheritedFeature();
    boolean _notEquals = (!Objects.equal(_inheritedFeature, null));
    if (_notEquals) {
      _xifexpression = prop.getInheritedFeature().isDerived();
    } else {
      _xifexpression = false;
    }
    return _xifexpression;
  }
  
  /**
   * The binary <code>+</code> operator that concatenates two strings.
   * 
   * @param a
   *            a string.
   * @param b
   *            another string.
   * @return <code>a + b</code>
   */
  public static String operator_plus(final String a, final String b) {
    if (((a == null) || (b == null))) {
      return "";
    }
    return DtoModelExtensions.operator_plus(a, b);
  }
  
  protected String _toTypeName(final LDtoAttribute prop) {
    return prop.getType().getName();
  }
  
  protected String _toTypeName(final LDtoInheritedAttribute prop) {
    String _xifexpression = null;
    LScalarType _type = prop.getType();
    boolean _tripleNotEquals = (_type != null);
    if (_tripleNotEquals) {
      _xifexpression = prop.getType().getName();
    } else {
      _xifexpression = prop.getInheritedFeature().getType().getName();
    }
    return _xifexpression;
  }
  
  protected String _toTypeName(final LDtoReference prop) {
    LDto _type = prop.getType();
    String _name = null;
    if (_type!=null) {
      _name=_type.getName();
    }
    return _name;
  }
  
  protected String _toTypeName(final LDtoInheritedReference prop) {
    LDto _type = prop.getType();
    String _name = null;
    if (_type!=null) {
      _name=_type.getName();
    }
    return _name;
  }
  
  protected String _toQualifiedTypeName(final LType type) {
    return this._iQualifiedNameProvider.getFullyQualifiedName(type).toString();
  }
  
  protected String _toQualifiedTypeName(final LDtoAttribute prop) {
    return this._iQualifiedNameProvider.getFullyQualifiedName(prop.getType()).toString();
  }
  
  protected String _toQualifiedTypeName(final LDtoInheritedAttribute prop) {
    String _xifexpression = null;
    LScalarType _type = prop.getType();
    boolean _tripleNotEquals = (_type != null);
    if (_tripleNotEquals) {
      _xifexpression = this._iQualifiedNameProvider.getFullyQualifiedName(prop.getType()).toString();
    } else {
      _xifexpression = this._iQualifiedNameProvider.getFullyQualifiedName(prop.getInheritedFeature().getType()).toString();
    }
    return _xifexpression;
  }
  
  protected String _toQualifiedTypeName(final LDtoReference prop) {
    LDto _type = prop.getType();
    QualifiedName _fullyQualifiedName = null;
    if (_type!=null) {
      _fullyQualifiedName=this._iQualifiedNameProvider.getFullyQualifiedName(_type);
    }
    return _fullyQualifiedName.toString();
  }
  
  protected String _toQualifiedTypeName(final LDtoInheritedReference prop) {
    LDto _type = prop.getType();
    QualifiedName _fullyQualifiedName = null;
    if (_type!=null) {
      _fullyQualifiedName=this._iQualifiedNameProvider.getFullyQualifiedName(_type);
    }
    return _fullyQualifiedName.toString();
  }
  
  protected LType _toRawType(final LFeature prop) {
    throw new IllegalStateException("not a valid call");
  }
  
  /**
   * Returns the type of the property or reference without any mappings
   */
  protected LType _toRawType(final LDtoAttribute prop) {
    return prop.getType();
  }
  
  /**
   * Returns the type of the property or reference without any mappings
   */
  protected LType _toRawType(final LDtoInheritedAttribute prop) {
    LAttribute _inheritedFeature = prop.getInheritedFeature();
    LScalarType _type = null;
    if (_inheritedFeature!=null) {
      _type=_inheritedFeature.getType();
    }
    return _type;
  }
  
  /**
   * Returns the type of the property or reference without any mappings
   */
  protected LType _toRawType(final LDtoReference prop) {
    return prop.getType();
  }
  
  /**
   * Returns the type of the property or reference without any mappings
   */
  protected LType _toRawType(final LDtoInheritedReference prop) {
    LReference _inheritedFeature = prop.getInheritedFeature();
    LType _rawType = null;
    if (_inheritedFeature!=null) {
      _rawType=this.toRawType(_inheritedFeature);
    }
    return _rawType;
  }
  
  /**
   * Returns the type of the property or reference without any mappings
   */
  protected LType _toRawType(final LEntityReference prop) {
    return prop.getType();
  }
  
  /**
   * Returns the type of the property or reference without any mappings
   */
  protected LType _toRawType(final LEntityAttribute prop) {
    return prop.getType();
  }
  
  /**
   * Returns the type of the property or reference without any mappings
   */
  protected LType _toRawType(final LBeanReference prop) {
    return prop.getType();
  }
  
  /**
   * Returns the type of the property or reference without any mappings
   */
  protected LType _toRawType(final LBeanAttribute prop) {
    return prop.getType();
  }
  
  /**
   * Returns the type reference of the property or reference without any mappings
   */
  protected JvmTypeReference _toRawTypeRefernce(final LDtoFeature prop) {
    LType _rawType = this.toRawType(prop);
    JvmTypeReference _typeReference = null;
    if (_rawType!=null) {
      _typeReference=this.toTypeReference(_rawType);
    }
    return _typeReference;
  }
  
  /**
   * Returns the type reference of the property or reference without any mappings
   */
  protected JvmTypeReference _toRawTypeRefernce(final LDtoInheritedReference prop) {
    final LReference ref = prop.getInheritedFeature();
    if ((ref instanceof LEntityReference)) {
      return this.toTypeReference(((LEntityReference)ref).getType());
    } else {
      if ((ref instanceof LBeanReference)) {
        return this.toTypeReference(((LBeanReference)ref).getType());
      }
    }
    return TypesFactory.eINSTANCE.createJvmUnknownTypeReference();
  }
  
  /**
   * Returns the type reference of the property or reference without any mappings
   */
  protected JvmTypeReference _toRawTypeRefernce(final LDtoAbstractReference prop) {
    LType _rawType = this.toRawType(prop);
    JvmTypeReference _typeReference = null;
    if (_rawType!=null) {
      _typeReference=this.toTypeReference(_rawType);
    }
    return _typeReference;
  }
  
  /**
   * Returns the type reference of the property or reference without any mappings
   */
  public String toRawTypeName(final LDtoFeature prop) {
    LType _rawType = this.toRawType(prop);
    String _name = null;
    if (_rawType!=null) {
      _name=_rawType.getName();
    }
    return _name;
  }
  
  protected LFeature _opposite(final LDtoFeature prop) {
    return null;
  }
  
  protected LFeature _opposite(final LDtoReference prop) {
    return prop.getOpposite();
  }
  
  protected LFeature _opposite(final LDtoInheritedReference prop) {
    if ((this.inherited(prop) && (prop.getInheritedFeature() != null))) {
      return this.opposite(prop.getInheritedFeature());
    } else {
      return null;
    }
  }
  
  protected LFeature _opposite(final LEntityReference prop) {
    return prop.getOpposite();
  }
  
  protected LFeature _opposite(final LBeanReference prop) {
    return prop.getOpposite();
  }
  
  protected Bounds _getBounds(final LDtoFeature prop) {
    boolean _inherited = this.inherited(prop);
    if (_inherited) {
      return Bounds.createFor(this.inheritedFeature(prop));
    } else {
      return Bounds.createFor(prop);
    }
  }
  
  protected String _toName(final LDtoFeature feature) {
    if (((feature == null) || ((!this.inherited(feature)) && (feature.getName() == null)))) {
      return "";
    }
    boolean _inherited = this.inherited(feature);
    if (_inherited) {
      LFeature _inheritedFeature = this.inheritedFeature(feature);
      String _name = null;
      if (_inheritedFeature!=null) {
        _name=_inheritedFeature.getName();
      }
      return _name;
    }
    return feature.getName().replace("^", "");
  }
  
  protected String _toName(final LDto dto) {
    if (((dto == null) || (dto.getName() == null))) {
      return "";
    }
    return dto.getName().replace("^", "");
  }
  
  protected String _toName(final Void param) {
    return "";
  }
  
  protected LReference _inheritedFeature(final LDtoFeature prop) {
    return null;
  }
  
  protected LReference _inheritedFeature(final LDtoInheritedReference prop) {
    return prop.getInheritedFeature();
  }
  
  protected LAttribute _inheritedFeature(final LDtoInheritedAttribute prop) {
    return prop.getInheritedFeature();
  }
  
  @Override
  public boolean isToMany(final LFeature prop) {
    if ((prop == null)) {
      return false;
    }
    return this.internalIsToMany(prop);
  }
  
  protected boolean _isAbstract(final LType context) {
    return false;
  }
  
  protected boolean _isAbstract(final LEntity context) {
    return context.isAbstract();
  }
  
  protected boolean _isAbstract(final LDto context) {
    return context.isAbstract();
  }
  
  protected boolean _isTransient(final EObject context) {
    return false;
  }
  
  protected boolean _isTransient(final LDtoAttribute context) {
    return context.isTransient();
  }
  
  protected boolean _internalIsToMany(final LFeature prop) {
    return this.getBounds(prop).isToMany();
  }
  
  protected boolean _internalIsToMany(final LDtoFeature prop) {
    if ((this.inherited(prop) && (this.inheritedFeature(prop) != null))) {
      return this.internalIsToMany(this.inheritedFeature(prop));
    } else {
      return this.getBounds(prop).isToMany();
    }
  }
  
  public boolean isAttribute(final LDtoFeature prop) {
    return (prop instanceof LAttribute);
  }
  
  public boolean isContainmentReference(final LDtoFeature prop) {
    return ((prop instanceof LDtoAbstractReference) && ((LDtoAbstractReference) prop).isCascading());
  }
  
  public boolean isCrossReference(final LDtoFeature prop) {
    return ((prop instanceof LDtoAbstractReference) && ((LDtoAbstractReference) prop).isCascading());
  }
  
  protected boolean _isContainerReference(final LDtoAbstractAttribute prop) {
    return false;
  }
  
  protected boolean _isContainerReference(final LDtoOperation prop) {
    return false;
  }
  
  protected boolean _isContainerReference(final LDtoReference prop) {
    final LDtoReference opposite = prop.getOpposite();
    if (((opposite != null) && opposite.isCascading())) {
      return true;
    } else {
      if ((((!this.getBounds(prop).isToMany()) && (!Objects.equal(opposite, null))) && this.getBounds(opposite).isToMany())) {
        return true;
      }
    }
    return false;
  }
  
  protected boolean _isContainerReference(final LDtoInheritedReference prop) {
    final LFeature opposite = this.opposite(prop.getInheritedFeature());
    if (((opposite instanceof LDtoAbstractReference) && ((LDtoAbstractReference) opposite).isCascading())) {
      return true;
    } else {
      if ((((!this.getBounds(prop).isToMany()) && (!Objects.equal(opposite, null))) && this.getBounds(opposite).isToMany())) {
        return true;
      }
    }
    return false;
  }
  
  public JvmTypeReference toMapperTypeReference(final LType type) {
    return this.references.getTypeForName(this._methodNamingExtensions.toFqnMapperName(type), type, null);
  }
  
  public JvmTypeReference toMapperTypeReference(final LDtoAbstractReference ref) {
    return this.toMapperTypeReference(ref.getType());
  }
  
  protected boolean _isIDorUUID(final LAttribute prop) {
    return false;
  }
  
  protected boolean _isIDorUUID(final LDtoAttribute prop) {
    return (prop.isId() || prop.isUuid());
  }
  
  protected boolean _isIDorUUID(final LDtoInheritedAttribute prop) {
    return (prop.getInheritedFeature().isId() || prop.getInheritedFeature().isUuid());
  }
  
  /**
   * Returns all containment features that need to be copied.
   */
  public Iterable<LDtoFeature> getContainmentReferencesToCopy(final LDto dto) {
    final Function1<LDtoFeature, Boolean> _function = (LDtoFeature it) -> {
      return Boolean.valueOf(this.isContainmentReference(it));
    };
    return IterableExtensions.<LDtoFeature>filter(dto.getFeatures(), _function);
  }
  
  @Override
  public boolean isBasedOnDatatype(final LFeature feature) {
    if ((feature instanceof LDtoInheritedAttribute)) {
      boolean _xifexpression = false;
      LAttribute _inheritedFeature = ((LDtoInheritedAttribute)feature).getInheritedFeature();
      boolean _tripleNotEquals = (_inheritedFeature != null);
      if (_tripleNotEquals) {
        _xifexpression = this.isBasedOnDatatype(((LDtoInheritedAttribute)feature).getInheritedFeature());
      } else {
        _xifexpression = false;
      }
      return _xifexpression;
    }
    return super.isBasedOnDatatype(feature);
  }
  
  @Override
  public LDataType getDatatype(final LFeature feature) {
    if ((feature instanceof LDtoInheritedAttribute)) {
      LDataType _xifexpression = null;
      LAttribute _inheritedFeature = ((LDtoInheritedAttribute)feature).getInheritedFeature();
      boolean _tripleNotEquals = (_inheritedFeature != null);
      if (_tripleNotEquals) {
        _xifexpression = this.getDatatype(((LDtoInheritedAttribute)feature).getInheritedFeature());
      } else {
        _xifexpression = null;
      }
      return _xifexpression;
    }
    return super.getDatatype(feature);
  }
  
  @Override
  public boolean typeIsEnum(final LAttribute prop) {
    if ((prop instanceof LDtoInheritedAttribute)) {
      boolean _xifexpression = false;
      LAttribute _inheritedFeature = ((LDtoInheritedAttribute)prop).getInheritedFeature();
      boolean _tripleNotEquals = (_inheritedFeature != null);
      if (_tripleNotEquals) {
        _xifexpression = this.typeIsEnum(((LDtoInheritedAttribute)prop).getInheritedFeature());
      } else {
        _xifexpression = false;
      }
      return _xifexpression;
    }
    return super.typeIsEnum(prop);
  }
  
  @Override
  public boolean typeIsStateClass(final LAttribute prop) {
    return super.typeIsStateClass(prop);
  }
  
  /**
   * Returns all attributes that need to be copied.
   */
  public Iterable<LDtoFeature> getAttributesToCopy(final LDto dto) {
    final Function1<LDtoFeature, Boolean> _function = (LDtoFeature it) -> {
      return Boolean.valueOf(this.isAttribute(it));
    };
    return IterableExtensions.<LDtoFeature>filter(dto.getFeatures(), _function);
  }
  
  /**
   * Returns all crossreferences that need to be copied.
   */
  public Iterable<LDtoFeature> getCrossReferencesToCopy(final LDto dto) {
    final Function1<LDtoFeature, Boolean> _function = (LDtoFeature it) -> {
      return Boolean.valueOf(((!this.isContainerReference(it)) && this.isCrossReference(it)));
    };
    return IterableExtensions.<LDtoFeature>filter(dto.getFeatures(), _function);
  }
  
  public boolean isBean(final LType type) {
    if ((type instanceof LDto)) {
      LType _wrappedType = ((LDto)type).getWrappedType();
      return (_wrappedType instanceof LBean);
    }
    return (type instanceof LBean);
  }
  
  protected LAttribute _idAttribute(final LDto dto) {
    Iterable<LDtoAbstractAttribute> _collectAllAttributes = this.collectAllAttributes(dto);
    for (final LDtoAbstractAttribute prop : _collectAllAttributes) {
      if ((this.inherited(prop) && (this.inheritedFeature(prop) != null))) {
        LFeature _inheritedFeature = this.inheritedFeature(prop);
        final LAttribute attribute = ((LAttribute) _inheritedFeature);
        if ((attribute.isId() || attribute.isUuid())) {
          return attribute;
        }
      } else {
        if ((prop.isId() || prop.isUuid())) {
          return prop;
        }
      }
    }
    return null;
  }
  
  protected LAttribute _idAttribute(final LEntity entity) {
    Iterable<LEntityAttribute> _collectAllAttributes = this.collectAllAttributes(entity);
    for (final LEntityAttribute prop : _collectAllAttributes) {
      if ((prop.isId() || prop.isUuid())) {
        return prop;
      }
    }
    return null;
  }
  
  public Iterable<LDtoAbstractAttribute> collectAllAttributes(final LDto dto) {
    final Function1<LFeature, Boolean> _function = (LFeature it) -> {
      return Boolean.valueOf((it instanceof LDtoAbstractAttribute));
    };
    final Function1<LFeature, LDtoAbstractAttribute> _function_1 = (LFeature it) -> {
      return ((LDtoAbstractAttribute) it);
    };
    return IterableExtensions.map(IterableExtensions.filter(dto.getAllFeatures(), _function), _function_1);
  }
  
  public Iterable<LEntityAttribute> collectAllAttributes(final LEntity entity) {
    final Function1<LEntityFeature, Boolean> _function = (LEntityFeature it) -> {
      return Boolean.valueOf((it instanceof LEntityAttribute));
    };
    final Function1<LEntityFeature, LEntityAttribute> _function_1 = (LEntityFeature it) -> {
      return ((LEntityAttribute) it);
    };
    return IterableExtensions.<LEntityFeature, LEntityAttribute>map(IterableExtensions.<LEntityFeature>filter(entity.getAllFeatures(), _function), _function_1);
  }
  
  public JvmTypeReference toDtoTypeReference(final LDtoFeature prop) {
    if (prop instanceof LDtoInheritedAttribute) {
      return _toDtoTypeReference((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _toDtoTypeReference((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoAbstractAttribute) {
      return _toDtoTypeReference((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toDtoTypeReference((LDtoAbstractReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public JvmTypeReference toDtoTypeReference(final LFeature prop, final LDtoFeature dtoAtt) {
    if (prop instanceof LDtoInheritedAttribute
         && dtoAtt instanceof LDtoInheritedAttribute) {
      return _toDtoTypeReference((LDtoInheritedAttribute)prop, (LDtoInheritedAttribute)dtoAtt);
    } else if (prop instanceof LBeanAttribute
         && dtoAtt instanceof LDtoInheritedAttribute) {
      return _toDtoTypeReference((LBeanAttribute)prop, (LDtoInheritedAttribute)dtoAtt);
    } else if (prop instanceof LBeanReference
         && dtoAtt instanceof LDtoInheritedAttribute) {
      return _toDtoTypeReference((LBeanReference)prop, (LDtoInheritedAttribute)dtoAtt);
    } else if (prop instanceof LEntityAttribute
         && dtoAtt instanceof LDtoInheritedAttribute) {
      return _toDtoTypeReference((LEntityAttribute)prop, (LDtoInheritedAttribute)dtoAtt);
    } else if (prop instanceof LEntityReference
         && dtoAtt instanceof LDtoInheritedReference) {
      return _toDtoTypeReference((LEntityReference)prop, (LDtoInheritedReference)dtoAtt);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop, dtoAtt).toString());
    }
  }
  
  public boolean isCascading(final LDtoOperation prop) {
    return _isCascading(prop);
  }
  
  public boolean inherited(final LFeature prop) {
    if (prop instanceof LDtoInheritedAttribute) {
      return _inherited((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _inherited((LDtoInheritedReference)prop);
    } else if (prop != null) {
      return _inherited(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean derived(final LDtoFeature prop) {
    if (prop instanceof LDtoInheritedAttribute) {
      return _derived((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _derived((LDtoInheritedReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public String toTypeName(final LDtoFeature prop) {
    if (prop instanceof LDtoAttribute) {
      return _toTypeName((LDtoAttribute)prop);
    } else if (prop instanceof LDtoInheritedAttribute) {
      return _toTypeName((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _toTypeName((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoReference) {
      return _toTypeName((LDtoReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public String toQualifiedTypeName(final LAnnotationTarget prop) {
    if (prop instanceof LDtoAttribute) {
      return _toQualifiedTypeName((LDtoAttribute)prop);
    } else if (prop instanceof LDtoInheritedAttribute) {
      return _toQualifiedTypeName((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _toQualifiedTypeName((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoReference) {
      return _toQualifiedTypeName((LDtoReference)prop);
    } else if (prop instanceof LType) {
      return _toQualifiedTypeName((LType)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public LType toRawType(final LFeature prop) {
    if (prop instanceof LDtoAttribute) {
      return _toRawType((LDtoAttribute)prop);
    } else if (prop instanceof LDtoInheritedAttribute) {
      return _toRawType((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _toRawType((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoReference) {
      return _toRawType((LDtoReference)prop);
    } else if (prop instanceof LBeanAttribute) {
      return _toRawType((LBeanAttribute)prop);
    } else if (prop instanceof LBeanReference) {
      return _toRawType((LBeanReference)prop);
    } else if (prop instanceof LEntityAttribute) {
      return _toRawType((LEntityAttribute)prop);
    } else if (prop instanceof LEntityReference) {
      return _toRawType((LEntityReference)prop);
    } else if (prop != null) {
      return _toRawType(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public JvmTypeReference toRawTypeRefernce(final LDtoFeature prop) {
    if (prop instanceof LDtoInheritedReference) {
      return _toRawTypeRefernce((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toRawTypeRefernce((LDtoAbstractReference)prop);
    } else if (prop != null) {
      return _toRawTypeRefernce(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public LFeature opposite(final LFeature prop) {
    if (prop instanceof LDtoInheritedReference) {
      return _opposite((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoReference) {
      return _opposite((LDtoReference)prop);
    } else if (prop instanceof LBeanReference) {
      return _opposite((LBeanReference)prop);
    } else if (prop instanceof LEntityReference) {
      return _opposite((LEntityReference)prop);
    } else if (prop instanceof LDtoFeature) {
      return _opposite((LDtoFeature)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public Bounds getBounds(final LAnnotationTarget prop) {
    if (prop instanceof LDtoFeature) {
      return _getBounds((LDtoFeature)prop);
    } else if (prop instanceof LFeature) {
      return _getBounds((LFeature)prop);
    } else if (prop != null) {
      return _getBounds(prop);
    } else if (prop == null) {
      return _getBounds((Void)null);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public String toName(final LAnnotationTarget dto) {
    if (dto instanceof LDto) {
      return _toName((LDto)dto);
    } else if (dto instanceof LClass) {
      return _toName((LClass)dto);
    } else if (dto instanceof LDtoFeature) {
      return _toName((LDtoFeature)dto);
    } else if (dto instanceof LFeature) {
      return _toName((LFeature)dto);
    } else if (dto instanceof LType) {
      return _toName((LType)dto);
    } else if (dto == null) {
      return _toName((Void)null);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(dto).toString());
    }
  }
  
  public LFeature inheritedFeature(final LDtoFeature prop) {
    if (prop instanceof LDtoInheritedAttribute) {
      return _inheritedFeature((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _inheritedFeature((LDtoInheritedReference)prop);
    } else if (prop != null) {
      return _inheritedFeature(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean isAbstract(final LType context) {
    if (context instanceof LDto) {
      return _isAbstract((LDto)context);
    } else if (context instanceof LEntity) {
      return _isAbstract((LEntity)context);
    } else if (context != null) {
      return _isAbstract(context);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(context).toString());
    }
  }
  
  public boolean isTransient(final EObject context) {
    if (context instanceof LDtoAttribute) {
      return _isTransient((LDtoAttribute)context);
    } else if (context != null) {
      return _isTransient(context);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(context).toString());
    }
  }
  
  public boolean internalIsToMany(final LFeature prop) {
    if (prop instanceof LDtoFeature) {
      return _internalIsToMany((LDtoFeature)prop);
    } else if (prop != null) {
      return _internalIsToMany(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean isContainerReference(final LDtoFeature prop) {
    if (prop instanceof LDtoInheritedReference) {
      return _isContainerReference((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoReference) {
      return _isContainerReference((LDtoReference)prop);
    } else if (prop instanceof LDtoAbstractAttribute) {
      return _isContainerReference((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoOperation) {
      return _isContainerReference((LDtoOperation)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean isIDorUUID(final LAttribute prop) {
    if (prop instanceof LDtoAttribute) {
      return _isIDorUUID((LDtoAttribute)prop);
    } else if (prop instanceof LDtoInheritedAttribute) {
      return _isIDorUUID((LDtoInheritedAttribute)prop);
    } else if (prop != null) {
      return _isIDorUUID(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public LAttribute idAttribute(final EObject dto) {
    if (dto instanceof LDto) {
      return _idAttribute((LDto)dto);
    } else if (dto instanceof LEntity) {
      return _idAttribute((LEntity)dto);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(dto).toString());
    }
  }
}
