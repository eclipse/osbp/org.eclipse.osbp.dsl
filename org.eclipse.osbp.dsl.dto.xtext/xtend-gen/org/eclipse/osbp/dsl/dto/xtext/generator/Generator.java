/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.generator;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.xtext.extensions.MethodNamingExtensions;
import org.eclipse.osbp.dsl.dto.xtext.generator.ComponentGenerator;
import org.eclipse.osbp.dsl.dto.xtext.generator.SuperTypeCollector;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.xtext.oxtype.hooks.DelegatingGenerator;
import org.eclipse.osbp.xtext.oxtype.logger.TimeLogger;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.compiler.DisableCodeGenerationAdapter;
import org.eclipse.xtext.xbase.compiler.IGeneratorConfigProvider;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class Generator extends DelegatingGenerator {
  private final static Logger LOGGER = LoggerFactory.getLogger(Generator.class);
  
  @Inject
  @Extension
  private MethodNamingExtensions _methodNamingExtensions;
  
  @Inject
  @Extension
  private ComponentGenerator _componentGenerator;
  
  @Inject
  @Extension
  private SuperTypeCollector _superTypeCollector;
  
  @Inject
  private TypeReferences typeRefs;
  
  @Inject
  private IGeneratorConfigProvider generatorConfigProvider;
  
  @Override
  protected void _internalDoGenerate(final JvmDeclaredType type, final IFileSystemAccess fsa) {
    boolean _isDisabled = DisableCodeGenerationAdapter.isDisabled(type);
    if (_isDisabled) {
      return;
    }
    String _qualifiedName = type.getQualifiedName();
    boolean _notEquals = (!Objects.equal(_qualifiedName, null));
    if (_notEquals) {
      final TimeLogger log = TimeLogger.start(Generator.class);
      final JvmType mapper = this.typeRefs.getTypeForName(IMapper.class, type, null).getType();
      if (((mapper instanceof JvmDeclaredType) && this._superTypeCollector.isSuperType(type, ((JvmDeclaredType) mapper)))) {
        String _replace = type.getQualifiedName().replace(".", "/");
        String _plus = (_replace + ".java");
        fsa.generateFile(_plus, "Dto-Mappers", 
          this.generateType(type, this.generatorConfigProvider.get(type)));
      } else {
        final TimeLogger log2 = TimeLogger.start(Generator.class);
        final CharSequence output = this.generateType(type, this.generatorConfigProvider.get(type));
        String _qualifiedName_1 = type.getQualifiedName();
        String _plus_1 = ("raw class generation for " + _qualifiedName_1);
        log2.stop(Generator.LOGGER, _plus_1);
        String _replace_1 = type.getQualifiedName().replace(".", "/");
        String _plus_2 = (_replace_1 + ".java");
        fsa.generateFile(_plus_2, output);
      }
      String _qualifiedName_2 = type.getQualifiedName();
      String _plus_3 = ("generated " + _qualifiedName_2);
      log.stop(Generator.LOGGER, _plus_3);
    }
  }
  
  @Override
  public void doGenerate(final Resource input, final IFileSystemAccess fsa) {
    super.doGenerate(input, fsa);
    final Function1<EObject, Boolean> _function = (EObject it) -> {
      boolean _xifexpression = false;
      if ((it instanceof LDto)) {
        LType _wrappedType = ((LDto)it).getWrappedType();
        _xifexpression = (!Objects.equal(_wrappedType, null));
      } else {
        _xifexpression = false;
      }
      return Boolean.valueOf(_xifexpression);
    };
    List<EObject> _list = IteratorExtensions.<EObject>toList(IteratorExtensions.<EObject>filter(input.getAllContents(), _function));
    for (final EObject tmp : _list) {
      {
        final LDto dto = ((LDto) tmp);
        final TimeLogger log = TimeLogger.start(Generator.class);
        fsa.deleteFile(this.toServiceComponentName(dto));
        fsa.generateFile(this.toServiceComponentName(dto), "OSGI-INF", this._componentGenerator.getServiceContent(dto));
        String _serviceComponentName = this.toServiceComponentName(dto);
        String _plus = ("generated " + _serviceComponentName);
        log.stop(Generator.LOGGER, _plus);
      }
    }
  }
  
  public String toServiceComponentName(final LDto dto) {
    String _fqnMapperName = this._methodNamingExtensions.toFqnMapperName(dto);
    return (_fqnMapperName + ".xml");
  }
  
  public String toServiceName(final Resource input) {
    final String r = input.getURI().lastSegment().replace("dtos", "services");
    return r;
  }
  
  public void internalDoGenerate(final EObject type, final IFileSystemAccess fsa) {
    if (type instanceof JvmDeclaredType) {
      _internalDoGenerate((JvmDeclaredType)type, fsa);
      return;
    } else if (type != null) {
      _internalDoGenerate(type, fsa);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type, fsa).toString());
    }
  }
}
