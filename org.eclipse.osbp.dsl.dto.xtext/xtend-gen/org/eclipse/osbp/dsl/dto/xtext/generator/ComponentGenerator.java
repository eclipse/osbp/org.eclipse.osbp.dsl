/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.dto.xtext.generator;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.dto.xtext.extensions.MethodNamingExtensions;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.entity.LBean;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class ComponentGenerator {
  @Inject
  @Extension
  private MethodNamingExtensions _methodNamingExtensions;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  public CharSequence getServiceContent(final LDto dto) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<scr:component xmlns:scr=\"http://www.osgi.org/xmlns/scr/v1.1.0\" name=\"");
    String _lowerCase = this._methodNamingExtensions.toFqnMapperName(dto).toLowerCase();
    _builder.append(_lowerCase);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    _builder.append("       ");
    _builder.append("<implementation class=\"");
    String _fqnMapperName = this._methodNamingExtensions.toFqnMapperName(dto);
    _builder.append(_fqnMapperName, "       ");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    _builder.append("       ");
    _builder.append("<service>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<provide interface=\"org.eclipse.osbp.dsl.dto.lib.IMapper\"/>");
    _builder.newLine();
    _builder.append("\t\t  ");
    _builder.append("</service>");
    _builder.newLine();
    _builder.append("\t\t  ");
    _builder.newLine();
    _builder.append("\t\t");
    final List<LType> tempDtos = this.getWithParent(dto);
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("<property name=\"fordto.from.entity\" type=\"String\" value=\"");
    String _string = this._iQualifiedNameProvider.getFullyQualifiedName(dto.getWrappedType()).toString();
    _builder.append(_string, "\t\t");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    {
      int _length = ((Object[])Conversions.unwrapArray(tempDtos, Object.class)).length;
      boolean _greaterThan = (_length > 1);
      if (_greaterThan) {
        _builder.append("\t\t");
        _builder.append("<property name=\"fordto.to.dto\" type=\"String\">");
        String _orString = this.toOrString(tempDtos);
        _builder.append(_orString, "\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("</property>");
        _builder.newLine();
      } else {
        _builder.append("\t\t");
        _builder.append("<property name=\"fordto.to.dto\" type=\"String\" value=\"");
        String _string_1 = this._iQualifiedNameProvider.getFullyQualifiedName(dto).toString();
        _builder.append(_string_1, "\t\t");
        _builder.append("\"/>");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<property name=\"forentity.from.dto\" type=\"String\" value=\"");
    String _string_2 = this._iQualifiedNameProvider.getFullyQualifiedName(dto).toString();
    _builder.append(_string_2, "\t\t");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    final List<LType> tempEntities = this.getWithParent(dto.getWrappedType());
    _builder.newLineIfNotEmpty();
    {
      int _length_1 = ((Object[])Conversions.unwrapArray(tempEntities, Object.class)).length;
      boolean _greaterThan_1 = (_length_1 > 1);
      if (_greaterThan_1) {
        _builder.append("\t\t");
        _builder.append("<property name=\"forentity.to.entity\" type=\"String\">");
        String _orString_1 = this.toOrString(tempEntities);
        _builder.append(_orString_1, "\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("</property>");
        _builder.newLine();
      } else {
        _builder.append("\t\t");
        _builder.append("<property name=\"forentity.to.entity\" type=\"String\" value=\"");
        String _string_3 = this._iQualifiedNameProvider.getFullyQualifiedName(dto.getWrappedType()).toString();
        _builder.append(_string_3, "\t\t");
        _builder.append("\"/>");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<property name=\"service.pid\" type=\"String\" value=\"");
    String _lowerCase_1 = this._methodNamingExtensions.toFqnMapperName(dto).toLowerCase();
    _builder.append(_lowerCase_1, "\t\t");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("<reference name=\"mapperAccess\" interface=\"org.eclipse.osbp.dsl.dto.lib.IMapperAccess\" ");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("cardinality=\"1..1\" policy=\"static\" bind=\"bindMapperAccess\" unbind=\"unbindMapperAccess\"/>");
    _builder.newLine();
    _builder.append("</scr:component>");
    _builder.newLine();
    return _builder;
  }
  
  /**
   * Properties will be returned as a String that OSGi will interpret as a list.
   */
  public String toOrString(final List<LType> types) {
    final StringBuilder b = new StringBuilder();
    final Consumer<LType> _function = (LType it) -> {
      int _length = b.length();
      boolean _greaterThan = (_length > 0);
      if (_greaterThan) {
        b.append("\n");
      }
      b.append(this._iQualifiedNameProvider.getFullyQualifiedName(it).toString());
    };
    types.forEach(_function);
    return b.toString();
  }
  
  protected List<LType> _getWithParent(final LEntity entity) {
    final ArrayList<LType> result = CollectionLiterals.<LType>newArrayList();
    this.collectWithParent(result, entity);
    return result;
  }
  
  public void collectWithParent(final List<LType> result, final LEntity entity) {
    result.add(entity);
    LEntity _superType = entity.getSuperType();
    boolean _notEquals = (!Objects.equal(_superType, null));
    if (_notEquals) {
      this.collectWithParent(result, entity.getSuperType());
    }
  }
  
  protected List<LType> _getWithParent(final LBean bean) {
    final ArrayList<LType> result = CollectionLiterals.<LType>newArrayList();
    this.collectWithParent(result, bean);
    return result;
  }
  
  public void collectWithParent(final List<LType> result, final LBean bean) {
    result.add(bean);
    LBean _superType = bean.getSuperType();
    boolean _notEquals = (!Objects.equal(_superType, null));
    if (_notEquals) {
      this.collectWithParent(result, bean.getSuperType());
    }
  }
  
  protected List<LType> _getWithParent(final LDto dto) {
    final ArrayList<LType> result = CollectionLiterals.<LType>newArrayList();
    this.collectWithParent(result, dto);
    return result;
  }
  
  public void collectWithParent(final List<LType> result, final LDto dto) {
    result.add(dto);
    LDto _superType = dto.getSuperType();
    boolean _notEquals = (!Objects.equal(_superType, null));
    if (_notEquals) {
      this.collectWithParent(result, dto.getSuperType());
    }
  }
  
  public List<LType> getWithParent(final EObject dto) {
    if (dto instanceof LDto) {
      return _getWithParent((LDto)dto);
    } else if (dto instanceof LBean) {
      return _getWithParent((LBean)dto);
    } else if (dto instanceof LEntity) {
      return _getWithParent((LEntity)dto);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(dto).toString());
    }
  }
}
