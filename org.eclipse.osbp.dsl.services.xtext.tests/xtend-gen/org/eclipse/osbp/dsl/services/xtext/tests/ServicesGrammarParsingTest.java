/**
 * generated by Xtext 2.11.0
 */
package org.eclipse.osbp.dsl.services.xtext.tests;

import com.google.inject.Inject;
import org.eclipse.osbp.dsl.semantic.service.LServiceModel;
import org.eclipse.osbp.dsl.services.xtext.tests.ServicesGrammarInjectorProvider;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(XtextRunner.class)
@InjectWith(ServicesGrammarInjectorProvider.class)
@SuppressWarnings("all")
public class ServicesGrammarParsingTest {
  @Inject
  private ParseHelper<LServiceModel> parseHelper;
  
  @Test
  public void loadModel() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Hello Xtext!");
      _builder.newLine();
      final LServiceModel result = this.parseHelper.parse(_builder);
      Assert.assertNotNull(result);
      Assert.assertTrue(result.eResource().getErrors().isEmpty());
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
