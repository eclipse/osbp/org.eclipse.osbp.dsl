/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *    
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation 
 *  
 */
package org.eclipse.osbp.dsl.semantic.dto.provider;


import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesFactory;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference;
import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LDtoAbstractReferenceItemProvider extends LDtoFeatureItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDtoAbstractReferenceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLazyPropertyDescriptor(object);
			addCascadeMergePersistPropertyDescriptor(object);
			addCascadeRemovePropertyDescriptor(object);
			addCascadeRefreshPropertyDescriptor(object);
			addIsGroupedPropertyDescriptor(object);
			addGroupNamePropertyDescriptor(object);
			addAsGridPropertyDescriptor(object);
			addAsTablePropertyDescriptor(object);
			addSideKickPropertyDescriptor(object);
			addReferenceHiddenPropertyDescriptor(object);
			addReferenceReadOnlyPropertyDescriptor(object);
			addHistorizedPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Lazy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLazyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_lazy_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_lazy_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__LAZY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Cascade Merge Persist feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCascadeMergePersistPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_cascadeMergePersist_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_cascadeMergePersist_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__CASCADE_MERGE_PERSIST,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Cascade Remove feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCascadeRemovePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_cascadeRemove_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_cascadeRemove_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__CASCADE_REMOVE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Cascade Refresh feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCascadeRefreshPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_cascadeRefresh_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_cascadeRefresh_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__CASCADE_REFRESH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Grouped feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsGroupedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_isGrouped_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_isGrouped_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__IS_GROUPED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Group Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGroupNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_groupName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_groupName_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__GROUP_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the As Grid feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsGridPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_asGrid_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_asGrid_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__AS_GRID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the As Table feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsTablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_asTable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_asTable_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__AS_TABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Side Kick feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSideKickPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_sideKick_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_sideKick_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__SIDE_KICK,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Reference Hidden feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReferenceHiddenPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_referenceHidden_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_referenceHidden_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__REFERENCE_HIDDEN,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Reference Read Only feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReferenceReadOnlyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_referenceReadOnly_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_referenceReadOnly_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__REFERENCE_READ_ONLY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Historized feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHistorizedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LReference_historized_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LReference_historized_feature", "_UI_LReference_type"),
				 OSBPTypesPackage.Literals.LREFERENCE__HISTORIZED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LDtoAbstractReference_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LDtoAbstractReference_type_feature", "_UI_LDtoAbstractReference_type"),
				 OSBPDtoPackage.Literals.LDTO_ABSTRACT_REFERENCE__TYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(OSBPTypesPackage.Literals.LREFERENCE__PROPERTIES);
			childrenFeatures.add(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((LDtoAbstractReference)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_LDtoAbstractReference_type") :
			getString("_UI_LDtoAbstractReference_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LDtoAbstractReference.class)) {
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__LAZY:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__CASCADE_MERGE_PERSIST:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__CASCADE_REMOVE:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__CASCADE_REFRESH:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__IS_GROUPED:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__GROUP_NAME:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__AS_GRID:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__AS_TABLE:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__SIDE_KICK:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__REFERENCE_HIDDEN:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__REFERENCE_READ_ONLY:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__HISTORIZED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__PROPERTIES:
			case OSBPDtoPackage.LDTO_ABSTRACT_REFERENCE__CONSTRAINTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__PROPERTIES,
				 OSBPTypesFactory.eINSTANCE.createLKeyAndValue()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCAssertFalse()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCAssertTrue()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCDecimalMax()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCDecimalMin()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCDigits()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCFuture()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCPast()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCNumericMax()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCNumericMin()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCNotNull()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCNull()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCRegEx()));

		newChildDescriptors.add
			(createChildParameter
				(OSBPTypesPackage.Literals.LREFERENCE__CONSTRAINTS,
				 OSBPTypesFactory.eINSTANCE.createLDtCSize()));
	}

}
