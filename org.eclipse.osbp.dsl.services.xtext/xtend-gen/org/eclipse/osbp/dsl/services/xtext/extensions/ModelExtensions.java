/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.services.xtext.extensions;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.semantic.common.helper.Bounds;
import org.eclipse.osbp.dsl.semantic.common.types.LAnnotationTarget;
import org.eclipse.osbp.dsl.semantic.common.types.LAttribute;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LFeature;
import org.eclipse.osbp.dsl.semantic.common.types.LReference;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.common.types.LType;
import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAbstractReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoFeature;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedAttribute;
import org.eclipse.osbp.dsl.semantic.dto.LDtoInheritedReference;
import org.eclipse.osbp.dsl.semantic.dto.LDtoOperation;
import org.eclipse.osbp.dsl.semantic.dto.LDtoReference;
import org.eclipse.osbp.dsl.semantic.entity.LBeanAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LBeanReference;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.dsl.semantic.service.LCardinality;
import org.eclipse.osbp.dsl.semantic.service.LInjectedService;
import org.eclipse.osbp.dsl.services.xtext.extensions.MethodNamingExtensions;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class ModelExtensions extends org.eclipse.osbp.dsl.common.xtext.extensions.ModelExtensions {
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  @Inject
  @Extension
  private MethodNamingExtensions _methodNamingExtensions;
  
  @Inject
  private TypeReferences references;
  
  protected JvmTypeReference _toTypeReference(final LDtoAbstractReference prop) {
    JvmTypeReference jvmTypeRef = this.toDtoTypeParameterReference(prop);
    return jvmTypeRef;
  }
  
  protected JvmTypeReference _toTypeReference(final LDtoAbstractAttribute prop) {
    JvmTypeReference jvmTypeRef = this.toDtoTypeParameterReference(prop);
    return jvmTypeRef;
  }
  
  /**
   * Creates a type references with respect to inherited features
   */
  protected JvmTypeReference _toDtoTypeParameterReference(final LDtoAbstractReference prop) {
    LDto _type = prop.getType();
    JvmTypeReference _typeReference = null;
    if (_type!=null) {
      _typeReference=this.toTypeReference(_type);
    }
    return this._jvmTypesBuilder.cloneWithProxies(_typeReference);
  }
  
  /**
   * Creates a type references with respect to inherited features
   */
  protected JvmTypeReference _toDtoTypeParameterReference(final LDtoAbstractAttribute prop) {
    LScalarType _type = prop.getType();
    JvmTypeReference _typeReference = null;
    if (_type!=null) {
      _typeReference=this.toTypeReference(_type);
    }
    return this._jvmTypesBuilder.cloneWithProxies(_typeReference);
  }
  
  /**
   * Creates a type references with respect to inherited features
   */
  protected JvmTypeReference _toDtoTypeParameterReference(final LDtoInheritedAttribute prop) {
    LScalarType _type = prop.getType();
    boolean _notEquals = (!Objects.equal(_type, null));
    if (_notEquals) {
      LScalarType _type_1 = prop.getType();
      JvmTypeReference _typeReference = null;
      if (_type_1!=null) {
        _typeReference=this.toTypeReference(_type_1);
      }
      return this._jvmTypesBuilder.cloneWithProxies(_typeReference);
    }
    LAttribute _inheritedFeature = prop.getInheritedFeature();
    JvmTypeReference _typeReference_1 = null;
    if (_inheritedFeature!=null) {
      _typeReference_1=this.toTypeReference(_inheritedFeature);
    }
    return this._jvmTypesBuilder.cloneWithProxies(_typeReference_1);
  }
  
  /**
   * Creates a type references with respect to inherited features
   */
  protected JvmTypeReference _toDtoTypeParameterReference(final LDtoInheritedReference prop) {
    LDto _type = prop.getType();
    JvmTypeReference _typeReference = null;
    if (_type!=null) {
      _typeReference=this.toTypeReference(_type);
    }
    return this._jvmTypesBuilder.cloneWithProxies(_typeReference);
  }
  
  /**
   * Creates a type reference with respect to multiplicity
   */
  public JvmTypeReference toDtoTypeParameterReferenceWithMultiplicity(final LDtoFeature prop) {
    JvmTypeReference ref = this.toDtoTypeParameterReference(prop);
    if (((!Objects.equal(ref, null)) && this.getBounds(prop).isToMany())) {
      ref = this.references.getTypeForName(List.class, prop, ref);
    }
    return ref;
  }
  
  /**
   * Creates a type reference with respect to multiplicity
   */
  public JvmTypeReference toRawTypeReferenceWithMultiplicity(final LDtoFeature prop) {
    JvmTypeReference ref = this.toRawTypeRefernce(prop);
    boolean _isToMany = this.getBounds(prop).isToMany();
    if (_isToMany) {
      ref = this.references.getTypeForName(List.class, prop, ref);
    }
    return ref;
  }
  
  protected boolean _isCascading(final LDtoOperation prop) {
    return false;
  }
  
  public LDtoAbstractReference getResolvedOpposite(final LDtoReference prop) {
    LDtoAbstractReference _xifexpression = null;
    LDtoReference _opposite = prop.getOpposite();
    boolean _notEquals = (!Objects.equal(_opposite, null));
    if (_notEquals) {
      return prop.getOpposite();
    } else {
      LDtoAbstractReference _xifexpression_1 = null;
      LDto _type = prop.getType();
      if ((_type instanceof LDto)) {
        LDtoAbstractReference _xblockexpression = null;
        {
          LDto _type_1 = prop.getType();
          final LDto ref = ((LDto) _type_1);
          final Function1<LDtoAbstractReference, Boolean> _function = (LDtoAbstractReference it) -> {
            LReference _opposite_1 = this.opposite(it);
            return Boolean.valueOf(Objects.equal(_opposite_1, prop));
          };
          _xblockexpression = IterableExtensions.<LDtoAbstractReference>findFirst(ref.getReferences(), _function);
        }
        _xifexpression_1 = _xblockexpression;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  /**
   * Returns true, if toCheck can be cast to superType
   */
  public boolean isCastable(final LDto toCheck, final LDto superType) {
    final String toCheckFqn = this._iQualifiedNameProvider.getFullyQualifiedName(toCheck).toString();
    final String superTypeFqn = this._iQualifiedNameProvider.getFullyQualifiedName(superType).toString();
    boolean _equals = toCheckFqn.equals(superTypeFqn);
    if (_equals) {
      return true;
    } else {
      final LDto toCheckSuperType = toCheck.getSuperType();
      boolean _notEquals = (!Objects.equal(toCheckSuperType, null));
      if (_notEquals) {
        return this.isCastable(toCheckSuperType, superType);
      } else {
        return false;
      }
    }
  }
  
  protected boolean _inherited(final LFeature prop) {
    return false;
  }
  
  protected boolean _inherited(final LDtoInheritedReference prop) {
    return true;
  }
  
  protected boolean _inherited(final LDtoInheritedAttribute prop) {
    return true;
  }
  
  /**
   * The binary <code>+</code> operator that concatenates two strings.
   * 
   * @param a
   *            a string.
   * @param b
   *            another string.
   * @return <code>a + b</code>
   */
  public static String operator_plus(final String a, final String b) {
    if ((Objects.equal(a, null) || Objects.equal(b, null))) {
      return "";
    }
    return ModelExtensions.operator_plus(a, b);
  }
  
  protected String _toTypeName(final LDtoAttribute prop) {
    return prop.getType().getName();
  }
  
  protected String _toTypeName(final LDtoInheritedAttribute prop) {
    String _xifexpression = null;
    LScalarType _type = prop.getType();
    boolean _notEquals = (!Objects.equal(_type, null));
    if (_notEquals) {
      _xifexpression = prop.getType().getName();
    } else {
      _xifexpression = prop.getInheritedFeature().getType().getName();
    }
    return _xifexpression;
  }
  
  protected String _toTypeName(final LDtoReference prop) {
    LDto _type = prop.getType();
    String _name = null;
    if (_type!=null) {
      _name=_type.getName();
    }
    return _name;
  }
  
  protected String _toTypeName(final LDtoInheritedReference prop) {
    LDto _type = prop.getType();
    String _name = null;
    if (_type!=null) {
      _name=_type.getName();
    }
    return _name;
  }
  
  protected LType _toRawType(final LFeature prop) {
    throw new IllegalStateException("not a valid call");
  }
  
  protected LType _toRawType(final LDtoAttribute prop) {
    return prop.getType();
  }
  
  protected LType _toRawType(final LDtoInheritedAttribute prop) {
    LAttribute _inheritedFeature = prop.getInheritedFeature();
    LScalarType _type = null;
    if (_inheritedFeature!=null) {
      _type=_inheritedFeature.getType();
    }
    return _type;
  }
  
  protected LType _toRawType(final LDtoReference prop) {
    return prop.getType();
  }
  
  protected LType _toRawType(final LDtoInheritedReference prop) {
    LReference _inheritedFeature = prop.getInheritedFeature();
    LType _rawType = null;
    if (_inheritedFeature!=null) {
      _rawType=this.toRawType(_inheritedFeature);
    }
    return _rawType;
  }
  
  protected LType _toRawType(final LEntityReference prop) {
    LEntity _type = prop.getType();
    return ((LType) _type);
  }
  
  protected LType _toRawType(final LEntityAttribute prop) {
    LScalarType _type = prop.getType();
    return ((LType) _type);
  }
  
  protected LType _toRawType(final LBeanReference prop) {
    LType _type = prop.getType();
    return ((LType) _type);
  }
  
  protected LType _toRawType(final LBeanAttribute prop) {
    LScalarType _type = prop.getType();
    return ((LType) _type);
  }
  
  public JvmTypeReference toRawTypeRefernce(final LDtoFeature prop) {
    LType _rawType = this.toRawType(prop);
    JvmTypeReference _typeReference = null;
    if (_rawType!=null) {
      _typeReference=this.toTypeReference(_rawType);
    }
    return _typeReference;
  }
  
  public String toRawTypeName(final LDtoFeature prop) {
    LType _rawType = this.toRawType(prop);
    String _name = null;
    if (_rawType!=null) {
      _name=_rawType.getName();
    }
    return _name;
  }
  
  protected LReference _opposite(final LDtoFeature prop) {
    return null;
  }
  
  protected LReference _opposite(final LDtoReference prop) {
    return prop.getOpposite();
  }
  
  protected LReference _opposite(final LDtoInheritedReference prop) {
    if ((this.inherited(prop) && (!Objects.equal(prop.getInheritedFeature(), null)))) {
      return this.opposite(prop.getInheritedFeature());
    } else {
      return null;
    }
  }
  
  protected LReference _opposite(final LEntityReference prop) {
    LEntityReference _opposite = prop.getOpposite();
    return ((LReference) _opposite);
  }
  
  protected LReference _opposite(final LBeanReference prop) {
    LFeature _opposite = prop.getOpposite();
    return ((LReference) _opposite);
  }
  
  protected Bounds _getBounds(final LDtoFeature prop) {
    boolean _inherited = this.inherited(prop);
    if (_inherited) {
      return Bounds.createFor(this.inheritedFeature(prop));
    } else {
      return Bounds.createFor(prop);
    }
  }
  
  protected String _toName(final LDtoFeature feature) {
    if ((Objects.equal(feature, null) || ((!this.inherited(feature)) && Objects.equal(feature.getName(), null)))) {
      return "";
    }
    boolean _inherited = this.inherited(feature);
    if (_inherited) {
      LFeature _inheritedFeature = this.inheritedFeature(feature);
      String _name = null;
      if (_inheritedFeature!=null) {
        _name=this.toName(_inheritedFeature);
      }
      return _name;
    }
    return feature.getName().replace("^", "");
  }
  
  protected String _toName(final LDto dto) {
    if ((Objects.equal(dto, null) || Objects.equal(dto.getName(), null))) {
      return "";
    }
    return dto.getName().replace("^", "");
  }
  
  protected LReference _inheritedFeature(final LDtoFeature prop) {
    return null;
  }
  
  protected LReference _inheritedFeature(final LDtoInheritedReference prop) {
    return prop.getInheritedFeature();
  }
  
  protected LAttribute _inheritedFeature(final LDtoInheritedAttribute prop) {
    return prop.getInheritedFeature();
  }
  
  @Override
  public boolean isToMany(final LFeature prop) {
    boolean _equals = Objects.equal(prop, null);
    if (_equals) {
      return false;
    }
    return this.internalIsToMany(prop);
  }
  
  protected boolean _isTransient(final EObject context) {
    return false;
  }
  
  protected boolean _isTransient(final LDtoAttribute context) {
    return context.isTransient();
  }
  
  protected boolean _internalIsToMany(final LFeature prop) {
    return this.getBounds(prop).isToMany();
  }
  
  protected boolean _internalIsToMany(final LDtoFeature prop) {
    if ((this.inherited(prop) && (!Objects.equal(this.inheritedFeature(prop), null)))) {
      return this.internalIsToMany(this.inheritedFeature(prop));
    } else {
      return this.getBounds(prop).isToMany();
    }
  }
  
  protected boolean _isCascading(final LDtoAbstractReference prop) {
    if ((this.inherited(prop) && (this.inheritedFeature(prop) instanceof LEntityReference))) {
      LFeature _inheritedFeature = this.inheritedFeature(prop);
      return ((LEntityReference) _inheritedFeature).isCascading();
    } else {
      return prop.isCascading();
    }
  }
  
  protected boolean _isCascading(final LDtoAbstractAttribute prop) {
    if ((this.inherited(prop) && (this.inheritedFeature(prop) instanceof LEntityReference))) {
      LFeature _inheritedFeature = this.inheritedFeature(prop);
      return ((LEntityReference) _inheritedFeature).isCascading();
    } else {
      return this.isCascading(prop);
    }
  }
  
  public boolean isCrossReference(final LDtoFeature prop) {
    return ((prop instanceof LReference) && (!this.isCascading(prop)));
  }
  
  public boolean isMany(final LInjectedService service) {
    return (Objects.equal(service.getCardinality(), LCardinality.ZERO_TO_MANY) || Objects.equal(service.getCardinality(), LCardinality.ONE_TO_MANY));
  }
  
  public boolean basedOnEntity(final LDto dto) {
    return ((!Objects.equal(dto.getWrappedType(), null)) && (dto.getWrappedType() instanceof LEntity));
  }
  
  public Iterable<LDtoAbstractAttribute> collectAllAttributes(final LDto dto) {
    final Function1<LFeature, Boolean> _function = (LFeature it) -> {
      return Boolean.valueOf((it instanceof LDtoAbstractAttribute));
    };
    final Function1<LFeature, LDtoAbstractAttribute> _function_1 = (LFeature it) -> {
      return ((LDtoAbstractAttribute) it);
    };
    return IterableExtensions.map(IterableExtensions.filter(dto.getAllFeatures(), _function), _function_1);
  }
  
  public LAttribute idAttribute(final LDto dto) {
    Iterable<LDtoAbstractAttribute> _collectAllAttributes = this.collectAllAttributes(dto);
    for (final LDtoAbstractAttribute prop : _collectAllAttributes) {
      if ((this.inherited(prop) && (!Objects.equal(this.inheritedFeature(prop), null)))) {
        LFeature _inheritedFeature = this.inheritedFeature(prop);
        final LAttribute attribute = ((LAttribute) _inheritedFeature);
        if ((attribute.isId() || attribute.isUuid())) {
          return attribute;
        }
      } else {
        if ((prop.isId() || prop.isUuid())) {
          return prop;
        }
      }
    }
    return null;
  }
  
  public LEntity wrappedEntity(final LDto dto) {
    boolean _basedOnEntity = this.basedOnEntity(dto);
    if (_basedOnEntity) {
      LType _wrappedType = dto.getWrappedType();
      return ((LEntity) _wrappedType);
    }
    return null;
  }
  
  public String wrappedEntityName(final LDto dto) {
    final LEntity entity = this.wrappedEntity(dto);
    boolean _notEquals = (!Objects.equal(entity, null));
    if (_notEquals) {
      return entity.getName();
    }
    return null;
  }
  
  public JvmTypeReference toTypeReference(final LAnnotationTarget prop) {
    if (prop instanceof LDataType) {
      return _toTypeReference((LDataType)prop);
    } else if (prop instanceof LDtoAbstractAttribute) {
      return _toTypeReference((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toTypeReference((LDtoAbstractReference)prop);
    } else if (prop instanceof LAttribute) {
      return _toTypeReference((LAttribute)prop);
    } else if (prop instanceof LType) {
      return _toTypeReference((LType)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public JvmTypeReference toDtoTypeParameterReference(final LDtoFeature prop) {
    if (prop instanceof LDtoInheritedAttribute) {
      return _toDtoTypeParameterReference((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _toDtoTypeParameterReference((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoAbstractAttribute) {
      return _toDtoTypeParameterReference((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _toDtoTypeParameterReference((LDtoAbstractReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean isCascading(final LDtoFeature prop) {
    if (prop instanceof LDtoAbstractAttribute) {
      return _isCascading((LDtoAbstractAttribute)prop);
    } else if (prop instanceof LDtoAbstractReference) {
      return _isCascading((LDtoAbstractReference)prop);
    } else if (prop instanceof LDtoOperation) {
      return _isCascading((LDtoOperation)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean inherited(final LFeature prop) {
    if (prop instanceof LDtoInheritedAttribute) {
      return _inherited((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _inherited((LDtoInheritedReference)prop);
    } else if (prop != null) {
      return _inherited(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public String toTypeName(final LDtoFeature prop) {
    if (prop instanceof LDtoAttribute) {
      return _toTypeName((LDtoAttribute)prop);
    } else if (prop instanceof LDtoInheritedAttribute) {
      return _toTypeName((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _toTypeName((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoReference) {
      return _toTypeName((LDtoReference)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public LType toRawType(final LFeature prop) {
    if (prop instanceof LDtoAttribute) {
      return _toRawType((LDtoAttribute)prop);
    } else if (prop instanceof LDtoInheritedAttribute) {
      return _toRawType((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _toRawType((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoReference) {
      return _toRawType((LDtoReference)prop);
    } else if (prop instanceof LBeanAttribute) {
      return _toRawType((LBeanAttribute)prop);
    } else if (prop instanceof LBeanReference) {
      return _toRawType((LBeanReference)prop);
    } else if (prop instanceof LEntityAttribute) {
      return _toRawType((LEntityAttribute)prop);
    } else if (prop instanceof LEntityReference) {
      return _toRawType((LEntityReference)prop);
    } else if (prop != null) {
      return _toRawType(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public LReference opposite(final LFeature prop) {
    if (prop instanceof LDtoInheritedReference) {
      return _opposite((LDtoInheritedReference)prop);
    } else if (prop instanceof LDtoReference) {
      return _opposite((LDtoReference)prop);
    } else if (prop instanceof LBeanReference) {
      return _opposite((LBeanReference)prop);
    } else if (prop instanceof LEntityReference) {
      return _opposite((LEntityReference)prop);
    } else if (prop instanceof LDtoFeature) {
      return _opposite((LDtoFeature)prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public Bounds getBounds(final LAnnotationTarget prop) {
    if (prop instanceof LDtoFeature) {
      return _getBounds((LDtoFeature)prop);
    } else if (prop instanceof LFeature) {
      return _getBounds((LFeature)prop);
    } else if (prop != null) {
      return _getBounds(prop);
    } else if (prop == null) {
      return _getBounds((Void)null);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public String toName(final LAnnotationTarget dto) {
    if (dto instanceof LDto) {
      return _toName((LDto)dto);
    } else if (dto instanceof LClass) {
      return _toName((LClass)dto);
    } else if (dto instanceof LDtoFeature) {
      return _toName((LDtoFeature)dto);
    } else if (dto instanceof LFeature) {
      return _toName((LFeature)dto);
    } else if (dto instanceof LType) {
      return _toName((LType)dto);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(dto).toString());
    }
  }
  
  public LFeature inheritedFeature(final LDtoFeature prop) {
    if (prop instanceof LDtoInheritedAttribute) {
      return _inheritedFeature((LDtoInheritedAttribute)prop);
    } else if (prop instanceof LDtoInheritedReference) {
      return _inheritedFeature((LDtoInheritedReference)prop);
    } else if (prop != null) {
      return _inheritedFeature(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
  
  public boolean isTransient(final EObject context) {
    if (context instanceof LDtoAttribute) {
      return _isTransient((LDtoAttribute)context);
    } else if (context != null) {
      return _isTransient(context);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(context).toString());
    }
  }
  
  public boolean internalIsToMany(final LFeature prop) {
    if (prop instanceof LDtoFeature) {
      return _internalIsToMany((LDtoFeature)prop);
    } else if (prop != null) {
      return _internalIsToMany(prop);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(prop).toString());
    }
  }
}
