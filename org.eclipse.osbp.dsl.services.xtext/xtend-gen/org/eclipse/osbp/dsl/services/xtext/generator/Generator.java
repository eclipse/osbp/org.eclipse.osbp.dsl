/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.services.xtext.generator;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.osbp.dsl.semantic.service.LService;
import org.eclipse.osbp.dsl.services.xtext.extensions.MethodNamingExtensions;
import org.eclipse.osbp.dsl.services.xtext.generator.ComponentGenerator;
import org.eclipse.osbp.xtext.oxtype.hooks.DelegatingGenerator;
import org.eclipse.osbp.xtext.oxtype.logger.TimeLogger;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class Generator extends DelegatingGenerator {
  private final static Logger LOGGER = LoggerFactory.getLogger(Generator.class);
  
  @Inject
  @Extension
  private MethodNamingExtensions _methodNamingExtensions;
  
  @Inject
  @Extension
  private ComponentGenerator _componentGenerator;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Override
  protected void _internalDoGenerate(final JvmDeclaredType type, final IFileSystemAccess fsa) {
    final TimeLogger log = TimeLogger.start(Generator.class);
    super._internalDoGenerate(type, fsa);
    String _qualifiedName = type.getQualifiedName();
    String _plus = ("generated " + _qualifiedName);
    log.stop(Generator.LOGGER, _plus);
  }
  
  @Override
  public void doGenerate(final Resource input, final IFileSystemAccess fsa) {
    super.doGenerate(input, fsa);
    final Function1<EObject, Boolean> _function = (EObject it) -> {
      return Boolean.valueOf((it instanceof LService));
    };
    List<EObject> _list = IteratorExtensions.<EObject>toList(IteratorExtensions.<EObject>filter(input.getAllContents(), _function));
    for (final EObject tmp : _list) {
      {
        final LService service = ((LService) tmp);
        String _name = service.getName();
        boolean _notEquals = (!Objects.equal(_name, null));
        if (_notEquals) {
          final TimeLogger log = TimeLogger.start(Generator.class);
          fsa.deleteFile(this.toServiceComponentName(service));
          fsa.generateFile(this.toServiceComponentName(service), "OSGI-INF", this._componentGenerator.getServiceContent(service));
          String _serviceComponentName = this.toServiceComponentName(service);
          String _plus = ("generated " + _serviceComponentName);
          log.stop(Generator.LOGGER, _plus);
        }
      }
    }
  }
  
  public String toServiceComponentName(final LService service) {
    String _string = this._iQualifiedNameProvider.getFullyQualifiedName(service).toString();
    return (_string + ".xml");
  }
  
  public void internalDoGenerate(final EObject type, final IFileSystemAccess fsa) {
    if (type instanceof JvmDeclaredType) {
      _internalDoGenerate((JvmDeclaredType)type, fsa);
      return;
    } else if (type != null) {
      _internalDoGenerate(type, fsa);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type, fsa).toString());
    }
  }
}
