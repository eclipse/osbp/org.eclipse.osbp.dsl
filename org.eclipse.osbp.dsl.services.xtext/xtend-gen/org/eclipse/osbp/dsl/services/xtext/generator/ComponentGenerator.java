/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.services.xtext.generator;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.dsl.dto.xtext.extensions.MethodNamingExtensions;
import org.eclipse.osbp.dsl.semantic.service.LCardinality;
import org.eclipse.osbp.dsl.semantic.service.LDTOService;
import org.eclipse.osbp.dsl.semantic.service.LInjectedService;
import org.eclipse.osbp.dsl.semantic.service.LService;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class ComponentGenerator {
  @Inject
  @Extension
  private MethodNamingExtensions _methodNamingExtensions;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  protected CharSequence _getServiceContent(final LDTOService service) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<scr:component xmlns:scr=\"http://www.osgi.org/xmlns/scr/v1.1.0\" name=\"");
    QualifiedName _lowerCase = this._iQualifiedNameProvider.getFullyQualifiedName(service).toLowerCase();
    _builder.append(_lowerCase);
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    _builder.append("       ");
    _builder.append("<implementation class=\"");
    QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(service);
    _builder.append(_fullyQualifiedName, "       ");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    _builder.append("       ");
    _builder.append("<service>");
    _builder.newLine();
    _builder.append("       \t");
    _builder.append("<provide interface=\"org.eclipse.osbp.runtime.common.filter.IDTOService\"/>");
    _builder.newLine();
    _builder.append("       ");
    _builder.append("</service>");
    _builder.newLine();
    _builder.append("       ");
    _builder.append("<property name=\"dto\" type=\"String\" value=\"");
    String _string = this._iQualifiedNameProvider.getFullyQualifiedName(service.getDto()).toString();
    _builder.append(_string, "       ");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    _builder.append("       ");
    _builder.append("<property name=\"service.pid\" type=\"String\" value=\"");
    QualifiedName _lowerCase_1 = this._iQualifiedNameProvider.getFullyQualifiedName(service).toLowerCase();
    _builder.append(_lowerCase_1, "       ");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    {
      if (((!Objects.equal(service.getInjectedServices(), null)) && (!service.getInjectedServices().getServices().isEmpty()))) {
        {
          EList<LInjectedService> _services = service.getInjectedServices().getServices();
          for(final LInjectedService ref : _services) {
            _builder.append("       ");
            _builder.append("<reference name=\"");
            String _firstLower = StringExtensions.toFirstLower(ref.getAttributeName());
            _builder.append(_firstLower, "       ");
            _builder.append("\" interface=\"");
            JvmType _type = ref.getService().getType();
            String _qualifiedName = null;
            if (_type!=null) {
              _qualifiedName=_type.getQualifiedName();
            }
            String _string_1 = _qualifiedName.toString();
            _builder.append(_string_1, "       ");
            _builder.append("\" ");
            _builder.newLineIfNotEmpty();
            _builder.append("       ");
            _builder.append("\t");
            _builder.append("cardinality=\"");
            String _cardinalityString = this.cardinalityString(ref.getCardinality());
            _builder.append(_cardinalityString, "       \t");
            _builder.append("\" policy=\"dynamic\" bind=\"bind");
            String _firstUpper = StringExtensions.toFirstUpper(ref.getAttributeName());
            _builder.append(_firstUpper, "       \t");
            _builder.append("\" unbind=\"unbind");
            String _firstUpper_1 = StringExtensions.toFirstUpper(ref.getAttributeName());
            _builder.append(_firstUpper_1, "       \t");
            _builder.append("/>");
            _builder.newLineIfNotEmpty();
          }
        }
      } else {
        {
          boolean _isMutablePersistenceId = service.isMutablePersistenceId();
          boolean _not = (!_isMutablePersistenceId);
          if (_not) {
            _builder.append("<reference name=\"emf\" interface=\"javax.persistence.EntityManagerFactory\" cardinality=\"1..1\" ");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("policy=\"dynamic\" bind=\"bindEmf\" unbind=\"unbindEmf\" ");
            {
              if (((!Objects.equal(service.getPersistenceId(), null)) && 
                (!service.getPersistenceId().equals("")))) {
                _builder.append("target=\"(osgi.unit.name=");
                String _persistenceId = service.getPersistenceId();
                _builder.append(_persistenceId, "\t");
                _builder.append(")\"");
              }
            }
            _builder.append("/>");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("<reference name=\"mapperAccess\" interface=\"org.eclipse.osbp.dsl.dto.lib.IMapperAccess\" cardinality=\"1..1\" ");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("policy=\"dynamic\" bind=\"bindMapperAccess\" unbind=\"unbindMapperAccess\"/>");
        _builder.newLine();
        _builder.append("<reference name=\"filterEnhancers\" interface=\"org.eclipse.osbp.runtime.common.filter.IFilterEnhancer\" cardinality=\"0..n\"");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("policy=\"dynamic\" bind=\"addFilterEnhancer\" unbind=\"removeFilterEnhancer\"/>");
        _builder.newLine();
        _builder.append("<reference name=\"sessionManager\" interface=\"org.eclipse.osbp.runtime.common.session.ISessionManager\" cardinality=\"1..1\"");
        _builder.newLine();
        _builder.append("\t\t\t\t\t");
        _builder.append("policy=\"dynamic\" bind=\"bindSessionManager\" unbind=\"unbindSessionManager\"/>");
        _builder.newLine();
        _builder.append("<reference name=\"validators\" interface=\"org.eclipse.osbp.runtime.common.validation.IValidationParticipant\" cardinality=\"0..n\"");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("policy=\"dynamic\" bind=\"addValidationParticipant\" unbind=\"removeValidationParticipant\"/>");
        _builder.newLine();
      }
    }
    _builder.append("</scr:component>");
    _builder.newLine();
    return _builder;
  }
  
  public String cardinalityString(final LCardinality cardinality) {
    if (cardinality != null) {
      switch (cardinality) {
        case ZERO_TO_ONE:
          return "0..1";
        case ZERO_TO_MANY:
          return "0..n";
        case ONE_TO_ONE:
          return "1..1";
        case ONE_TO_MANY:
          return "1..n";
        default:
          return "0..1";
      }
    } else {
      return "0..1";
    }
  }
  
  protected CharSequence _getServiceContent(final LService service) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  public CharSequence getServiceContent(final LService service) {
    if (service instanceof LDTOService) {
      return _getServiceContent((LDTOService)service);
    } else if (service != null) {
      return _getServiceContent(service);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(service).toString());
    }
  }
}
