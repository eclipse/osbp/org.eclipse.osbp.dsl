@GenModel(fileExtensions="service", modelDirectory= "/org.eclipse.osbp.dsl.services.xtext/emf-gen", editDirectory="/org.eclipse.osbp.dsl.services.xtext.edit/src", childCreationExtenders="true")
@GenModel(modelName="Service")
@GenModel(prefix="OSBPService")
@GenModel(suppressEMFModelTags="true")
@GenModel(runtimeVersion="2.9")
@GenModel(loadInitialization="false")
@GenModel(literalsInterface="true")
@GenModel(copyrightText="Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 All rights reserved. This program and the accompanying materials 
 are made available under the terms of the Eclipse Public License 2.0  
 which accompanies this distribution, and is available at 
 https://www.eclipse.org/legal/epl-2.0/ 
 
 SPDX-License-Identifier: EPL-2.0 

 Based on ideas from Xtext, Xtend, Xcore
  
 Contributors:  
 		Florian Pirchner - Initial implementation
 ")
@GenModel(operationReflection="false")
@Ecore(nsURI="http://osbp.eclipse.org/dsl/services/v1")
@Ecore(nsPrefix="services")
@Ecore(rootPackage="services") 
package org.eclipse.osbp.dsl.semantic.service

import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.osbp.dsl.semantic.common.types.LClass
import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver
import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage
import org.eclipse.osbp.dsl.semantic.dto.LDto
import org.eclipse.osbp.dsl.semantic.dto.LDtoFeature
import org.eclipse.xtext.xtype.XImportSection

class LServiceModel extends LLazyResolver {
	contains XImportSection importSection
	contains LTypedPackage[] packages
}

abstract class LService extends LClass {
	contains LInjectedServices injectedServices
	contains LServiceOperation[*] operations
}

class LInjectedServices extends LLazyResolver {
	contains LInjectedService[*] services
}

class LInjectedService extends LLazyResolver {
	LCardinality cardinality
	String attributeName
	contains JvmTypeReference service
}

class LDTOService extends LService {
	refers LDto dto
	contains  JvmTypeReference dtoJvm
	boolean mutablePersistenceId
	String persistenceId
	contains LFilterableAttributes filterable opposite parent
	contains LSortableAttributes sortable opposite parent
	
}

class LFilterableAttributes extends LLazyResolver {
	refers LDTOService parent opposite filterable
	refers LDtoFeature[*] filterableFeatures
}

class LSortableAttributes extends LLazyResolver {
	refers LDTOService parent opposite sortable
	refers LDtoFeature[*] sortableFeatures
}

class LChartService extends LService {
}

class LFreeService extends LService {
}

enum LCardinality {
	ZERO_TO_ONE
	, ONE_TO_ONE
	, ZERO_TO_MANY
	, ONE_TO_MANY
}

class LServiceOperation extends org.eclipse.osbp.dsl.semantic.common.types.LOperation, LDtoFeature {
}
