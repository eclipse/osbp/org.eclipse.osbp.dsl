/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.semantic.service.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.osbp.dsl.semantic.dto.LDto;

// TODO: Auto-generated Javadoc
/**
 * This util is used to convert between entity packages and service packages.
 */
public class NamingConventionsUtil {

	/** The Constant pathReplacements. */
	private static final List<Pair> pathReplacements;

	static {
		pathReplacements = new ArrayList<Pair>();
		pathReplacements.add(new Pair(".dtos", ".services"));
		pathReplacements.add(new Pair("/dto/", "/service/"));
		pathReplacements.add(new Pair("/dtos/", "/services/"));
		pathReplacements.add(new Pair(".dto", ".service"));
	}

	/**
	 * Replaces package fragments and returns the package name used for
	 * Auto-Services.
	 *
	 * @param packageName
	 *            the package name
	 * @return the string
	 */
	public static String toServicePackage(String packageName) {
		if (packageName == null) {
			return "";
		}
		String result = packageName;
		for (Pair pair : pathReplacements) {
			result = pair.replace(result);
		}
		return result;
	}

	/**
	 * Replaces package fragments and returns the package name used for
	 * Auto-Services.
	 *
	 * @param lPackage
	 *            the l package
	 * @return the string
	 */
	public static String toServicePackage(LTypedPackage lPackage) {
		return toServicePackage(lPackage.getName());
	}

	/**
	 * Returns the name of the service for the given type.
	 *
	 * @param lType
	 *            the l type
	 * @return the string
	 */
	public static String toServiceName(LDto lType) {
		return toServiceName(lType.getName());
	}

	/**
	 * Returns the name of the service for the given type.
	 *
	 * @param prefix
	 *            the prefix
	 * @return the string
	 */
	public static String toServiceName(String prefix) {
		return prefix + "Service";
	}

	/**
	 * The Class Pair.
	 */
	private static class Pair {
		
		/** The source. */
		private final String source;
		
		/** The target. */
		private final String target;

		/**
		 * Instantiates a new pair.
		 *
		 * @param source
		 *            the source
		 * @param target
		 *            the target
		 */
		public Pair(String source, String target) {
			super();
			this.source = source;
			this.target = target;
		}

		/**
		 * Replace.
		 *
		 * @param value
		 *            the value
		 * @return the string
		 */
		public String replace(String value) {
			return value.replace(source, target);
		}
	}

}
