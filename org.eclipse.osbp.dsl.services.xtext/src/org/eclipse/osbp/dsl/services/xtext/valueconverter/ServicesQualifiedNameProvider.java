/**
 * Copyright (c) 2011, 2015 - Florian Pirchner,  Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.services.xtext.valueconverter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.dsl.common.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.common.xtext.valueconverter.CommonQualifiedNameProvider;
import org.eclipse.osbp.dsl.semantic.service.LFilterableAttributes;
import org.eclipse.osbp.dsl.semantic.service.LService;
import org.eclipse.osbp.dsl.semantic.service.LSortableAttributes;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;

import com.google.inject.Inject;

public class ServicesQualifiedNameProvider extends CommonQualifiedNameProvider {

	@Inject
	private IQualifiedNameConverter qualifiedNameConverter;

	@Inject
	ModelExtensions extensions;

	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		if (obj == null) {
			return QualifiedName.create("");
		}

		if (obj instanceof LFilterableAttributes) {
			LFilterableAttributes filters = (LFilterableAttributes) obj;
			LService service = filters.getParent();
			QualifiedName name = getFullyQualifiedName(service);
			return name.append("supportedFilter");
		} else if (obj instanceof LSortableAttributes) {
			LSortableAttributes sortable = (LSortableAttributes) obj;
			LService service = sortable.getParent();
			QualifiedName name = getFullyQualifiedName(service);
			return name.append("supportedFilter");
		}

		return super.getFullyQualifiedName(obj);
	}

}
