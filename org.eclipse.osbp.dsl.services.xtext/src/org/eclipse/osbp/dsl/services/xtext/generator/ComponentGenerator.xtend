/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.services.xtext.generator

import com.google.inject.Inject
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.osbp.dsl.dto.xtext.extensions.MethodNamingExtensions
import org.eclipse.osbp.dsl.semantic.service.LCardinality
import org.eclipse.osbp.dsl.semantic.service.LDTOService
import org.eclipse.osbp.dsl.semantic.service.LService

class ComponentGenerator {

	@Inject extension MethodNamingExtensions
	@Inject extension IQualifiedNameProvider

	def dispatch getServiceContent(LDTOService service) '''
		<?xml version="1.0" encoding="UTF-8"?>
		<scr:component xmlns:scr="http://www.osgi.org/xmlns/scr/v1.1.0" name="«service.fullyQualifiedName.toLowerCase»">
		       <implementation class="«service.fullyQualifiedName»"/>
		       <service>
		       	<provide interface="org.eclipse.osbp.runtime.common.filter.IDTOService"/>
		       </service>
		       <property name="dto" type="String" value="«service.dto.fullyQualifiedName.toString»"/>
		       <property name="service.pid" type="String" value="«service.fullyQualifiedName.toLowerCase»"/>
		       «IF service.injectedServices != null && !service.injectedServices.services.empty»
		       	«FOR ref : service.injectedServices.services»
		       		<reference name="«ref.attributeName.toFirstLower»" interface="«ref.service.type?.qualifiedName.toString»" 
		       			cardinality="«ref.cardinality.cardinalityString»" policy="dynamic" bind="bind«ref.attributeName.
			toFirstUpper»" unbind="unbind«ref.attributeName.toFirstUpper»/>
		       	«ENDFOR»
		       «ELSE»
		       	«IF !service.mutablePersistenceId»
				<reference name="emf" interface="javax.persistence.EntityManagerFactory" cardinality="1..1" 
					policy="dynamic" bind="bindEmf" unbind="unbindEmf" «IF (service.persistenceId != null &&
			!service.persistenceId.equals(""))»target="(osgi.unit.name=«service.persistenceId»)"«ENDIF»/>
			«ENDIF»
			<reference name="mapperAccess" interface="org.eclipse.osbp.dsl.dto.lib.IMapperAccess" cardinality="1..1" 
				policy="dynamic" bind="bindMapperAccess" unbind="unbindMapperAccess"/>
			<reference name="filterEnhancers" interface="org.eclipse.osbp.runtime.common.filter.IFilterEnhancer" cardinality="0..n"
					policy="dynamic" bind="addFilterEnhancer" unbind="removeFilterEnhancer"/>
			<reference name="sessionManager" interface="org.eclipse.osbp.runtime.common.session.ISessionManager" cardinality="1..1"
								policy="dynamic" bind="bindSessionManager" unbind="unbindSessionManager"/>
			<reference name="validators" interface="org.eclipse.osbp.runtime.common.validation.IValidationParticipant" cardinality="0..n"
				policy="dynamic" bind="addValidationParticipant" unbind="removeValidationParticipant"/>
			«ENDIF»
		</scr:component>
	'''

	def String cardinalityString(LCardinality cardinality) {
		switch (cardinality) {
			case ZERO_TO_ONE:
				return "0..1"
			case ZERO_TO_MANY:
				return "0..n"
			case ONE_TO_ONE:
				return "1..1"
			case ONE_TO_MANY:
				return "1..n"
			default:
				return "0..1"
		}
	}

	def dispatch getServiceContent(LService service) '''
«««		<?xml version="1.0" encoding="UTF-8"?>
«««		<components xmlns:scr="http://www.osgi.org/xmlns/scr/v1.0.0">
«««		   <scr:component name="«service.fullyQualifiedName.toLowerCase»">
«««		       <implementation class="«service.fullyQualifiedName»"/>
«««		       <service>
«««		       	<provide interface="org.eclipse.osbp.dsl.service.lib.IDTOService"/>
«««		       </service>
«««		       <property name="dto" value="«service.dto.fullyQualifiedName.toString»"/>
«««		       <property name="service.pid" value="«service.fullyQualifiedName.toLowerCase»"/>
«««		       «FOR ref : service.injectedServices.services»
«««		       	<reference name="«ref.attributeName.toFirstLower»" interface="«ref.service.qualifiedName.toString»" 
«««		       			cardinality="«ref.cardinality.cardinalityString»" policy="dynamic" bind="bind«ref.attributeName.
«««			toFirstUpper»" unbind="unbind«ref.attributeName.toFirstUpper»"/>
«««		       «ENDFOR»
«««		   </scr:component>
«««		</components>
	'''

}
