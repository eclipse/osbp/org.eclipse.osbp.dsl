

/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.dsl.services.xtext.extensions

import com.google.inject.Inject
import org.eclipse.osbp.dsl.common.xtext.extensions.NamingExtensions
import org.eclipse.xtext.naming.IQualifiedNameProvider

class MethodNamingExtensions extends NamingExtensions {

	@Inject extension ModelExtensions;
	@Inject extension IQualifiedNameProvider;


}
