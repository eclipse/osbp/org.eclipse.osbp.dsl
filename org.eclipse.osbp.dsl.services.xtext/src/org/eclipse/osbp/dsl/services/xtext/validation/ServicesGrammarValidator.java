/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.services.xtext.validation;

import java.util.List;

import org.eclipse.osbp.dsl.semantic.common.types.LTypedPackage;
import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("restriction")
public class ServicesGrammarValidator
		extends
		org.eclipse.osbp.dsl.services.xtext.validation.AbstractServicesGrammarValidator {

	@Check
	public void checkClassPath(LTypedPackage model) {
		TypeReferences typeReferences = getServices().getTypeReferences();
		final JvmGenericType listType = (JvmGenericType) typeReferences
				.findDeclaredType(List.class, model);
		if (listType == null || listType.getTypeParameters().isEmpty()) {
			error("Couldn't find a JDK 1.5 or higher on the project's classpath.",
					model, OSBPTypesPackage.Literals.LPACKAGE__NAME,
					CODE__MISSING__JDK_1_5);
		}
		if (typeReferences.findDeclaredType("org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess", model) == null) {
			error("Couldn't find the mandatory library 'org.eclipse.osbp.dsl.dto.lib' on the project's classpath.",
					model, OSBPTypesPackage.Literals.LPACKAGE__NAME, CODE__MISSING__DTO_LIB);
		}
		if (typeReferences.findDeclaredType(
				"org.eclipse.osbp.runtime.common.annotations.Dispose", model) == null) {
			error("Couldn't find the mandatory library 'org.eclipse.osbp.runtime.common' on the project's classpath.",
					model, OSBPTypesPackage.Literals.LPACKAGE__NAME,
					CODE__MISSING__L_RUNTIME_COMMON);
		}
		if (typeReferences.findDeclaredType(Extension.class, model) == null) {
			error("Couldn't find the mandatory library 'org.eclipse.xtext.xbase.lib' 2.11.0 or higher on the project's classpath.",
					model, OSBPTypesPackage.Literals.LPACKAGE__NAME,
					CODE__MISSING__XBASE_LIB);
		}
		if (typeReferences.findDeclaredType("javax.persistence.Persistence",
				model) == null) {
			warning("Couldn't find the optional library 'javax.persistence' 2.1.0 or higher on the project's classpath. If you are using JPA-Dto-Services, the library is mandatory.",
					model, OSBPTypesPackage.Literals.LPACKAGE__NAME,
					CODE__MISSING__JAVAX_PERSISTENCE);
		}
		if (typeReferences.findDeclaredType(
				"org.eclipse.osbp.dsl.common.datatypes.IDatatypeConstants", model) == null) {
			warning("Couldn't find the optional library 'org.eclipse.osbp.dsl.datatype.lib' on the project's classpath. This may cause resolving problems.",
					model, OSBPTypesPackage.Literals.LPACKAGE__NAME,
					CODE__MISSING__DATATYPE_LIB);
		}
		if (typeReferences.findDeclaredType("javax.validation.Valid", model) == null) {
			error("Couldn't find the library 'javax.validation' on the project's classpath. This may cause resolving problems.",
					model, OSBPTypesPackage.Literals.LPACKAGE__NAME,
					CODE__MISSING__DATATYPE_LIB);
		}
		if (typeReferences
				.findDeclaredType(
						"org.eclipse.osbp.runtime.jsr303.validation.common.JSR303ValidationParticipant",
						model) == null) {
			warning("Couldn't find the library 'org.eclipse.osbp.runtime.jsr303.validation' on the project's classpath. This may cause resolving problems.",
					model, OSBPTypesPackage.Literals.LPACKAGE__NAME,
					CODE__MISSING__DATATYPE_LIB);
		}
	}
}
