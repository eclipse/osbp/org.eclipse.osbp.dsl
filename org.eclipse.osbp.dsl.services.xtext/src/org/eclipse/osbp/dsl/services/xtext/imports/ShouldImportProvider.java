/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation

 */
package org.eclipse.osbp.dsl.services.xtext.imports;

import org.eclipse.osbp.xtext.oxtype.imports.DefaultShouldImportProvider;

public class ShouldImportProvider extends DefaultShouldImportProvider {

}
