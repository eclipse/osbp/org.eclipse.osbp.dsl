/**
 * Copyright (c) 2011, 2015 - Florian Pirchner,  Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.services.xtext.scoping;

import org.eclipse.osbp.dsl.common.xtext.scoping.CommonImportSectionNamespaceScopeProvider;

public class ServicesImportedNamespaceAwareLocalScopeProvider extends
		CommonImportSectionNamespaceScopeProvider {
}
