
/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */



package org.eclipse.osbp.dsl.services.xtext.generator

import com.google.inject.Inject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.osbp.dsl.semantic.service.LService
import org.eclipse.osbp.dsl.services.xtext.extensions.MethodNamingExtensions
import org.eclipse.osbp.xtext.oxtype.hooks.DelegatingGenerator
import org.eclipse.osbp.xtext.oxtype.logger.TimeLogger
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Generator extends DelegatingGenerator {
	
	static final Logger LOGGER = LoggerFactory.getLogger(typeof(Generator))

	@Inject extension MethodNamingExtensions
	@Inject extension ComponentGenerator
   	@Inject extension IQualifiedNameProvider
   	
   	override dispatch void internalDoGenerate(JvmDeclaredType type, IFileSystemAccess fsa) {
		val log = TimeLogger.start(typeof(Generator));
		super._internalDoGenerate(type, fsa)
		log.stop(LOGGER, "generated " + type.qualifiedName)
	}
   
	override doGenerate(Resource input, IFileSystemAccess fsa) {
		super.doGenerate(input, fsa)
		
		for (tmp : input.allContents.filter[it instanceof LService].toList) {
			val LService service = tmp as LService
			if(service.name != null) {
				val log = TimeLogger.start(typeof(Generator));
				fsa.deleteFile(service.toServiceComponentName);
				fsa.generateFile(service.toServiceComponentName, "OSGI-INF", service.getServiceContent);
				log.stop(LOGGER, "generated " + service.toServiceComponentName)
			}
		}
	}

	def toServiceComponentName(LService service) {
		service.fullyQualifiedName.toString + ".xml"
	}
}
