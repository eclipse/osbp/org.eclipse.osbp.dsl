/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.dsl.services.xtext.scoping;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.dsl.semantic.service.LFilterableAttributes;
import org.eclipse.osbp.dsl.semantic.service.LSortableAttributes;
import org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage;
import org.eclipse.osbp.dsl.services.xtext.scoping.AbstractServicesGrammarScopeProvider;
import org.eclipse.xtext.scoping.IScope;

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation/latest/xtext.html#scoping on
 * how and when to use it
 * 
 */
@SuppressWarnings("restriction")
public class ServicesGrammarScopeProvider extends AbstractServicesGrammarScopeProvider {

	@Override
	public IScope getScope(final EObject context, EReference reference) {
		if (reference == OSBPServicePackage.Literals.LFILTERABLE_ATTRIBUTES__FILTERABLE_FEATURES) {
			return new ServicesFilterableScope((LFilterableAttributes) context);
		} else if (reference == OSBPServicePackage.Literals.LSORTABLE_ATTRIBUTES__SORTABLE_FEATURES) {
			return new ServicesSortableScope((LSortableAttributes) context);
		}

		// } else if (reference ==
		// OSBPDtoPackage.Literals.LDTO_INHERITED_ATTRIBUTE__INHERITED_FEATURE) {
		// return new ServicesInheritedAttributeScope(
		// (LDtoInheritedAttribute) context);
		// } else if (reference ==
		// OSBPDtoPackage.Literals.LDTO_REFERENCE__OPPOSITE) {
		// return new DtoRefOppositeScope((LDtoReference) context);
		// } else if (reference == OSBPDtoPackage.Literals.LDTO__WRAPPED_TYPE) {
		// return new WrappedTypeFilterScope(
		// super.getScope(context, reference));
		// } else if (reference == OSBPDtoPackage.Literals.LDTO__SUPER_TYPE) {
		// return new InheritTypesFilterScope((LType) context, super.getScope(
		// context, reference));
		// } else if (reference == OSBPTypesPackage.Literals.LATTRIBUTE__TYPE) {
		// LAttribute att = (LAttribute) context;
		// if (att.isId() || att.isVersion() || att.isUuid()) {
		// return new DatatypesFilterScope(super.getScope(context,
		// reference));
		// } else if (att.isUuid()) {
		// return new UUIdFilterScope(super.getScope(context, reference));
		// }
		// }
		return super.getScope(context, reference);
	}
}
