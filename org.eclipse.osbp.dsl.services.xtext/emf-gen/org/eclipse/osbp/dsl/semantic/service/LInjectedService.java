/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation
 *  
 */
package org.eclipse.osbp.dsl.semantic.service;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LInjected Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LInjectedService#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LInjectedService#getAttributeName <em>Attribute Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LInjectedService#getService <em>Service</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLInjectedService()
 * @generated
 */
public interface LInjectedService extends LLazyResolver {
	/**
	 * Returns the value of the '<em><b>Cardinality</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.dsl.semantic.service.LCardinality}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cardinality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cardinality</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.service.LCardinality
	 * @see #setCardinality(LCardinality)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLInjectedService_Cardinality()
	 * @generated
	 */
	LCardinality getCardinality();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LInjectedService#getCardinality <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cardinality</em>' attribute.
	 * @see org.eclipse.osbp.dsl.semantic.service.LCardinality
	 * @see #getCardinality()
	 * @generated
	 */
	void setCardinality(LCardinality value);

	/**
	 * Returns the value of the '<em><b>Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Name</em>' attribute.
	 * @see #setAttributeName(String)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLInjectedService_AttributeName()
	 * @generated
	 */
	String getAttributeName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LInjectedService#getAttributeName <em>Attribute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Name</em>' attribute.
	 * @see #getAttributeName()
	 * @generated
	 */
	void setAttributeName(String value);

	/**
	 * Returns the value of the '<em><b>Service</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Service</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service</em>' containment reference.
	 * @see #setService(JvmTypeReference)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLInjectedService_Service()
	 * @generated
	 */
	JvmTypeReference getService();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LInjectedService#getService <em>Service</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Service</em>' containment reference.
	 * @see #getService()
	 * @generated
	 */
	void setService(JvmTypeReference value);

} // LInjectedService
