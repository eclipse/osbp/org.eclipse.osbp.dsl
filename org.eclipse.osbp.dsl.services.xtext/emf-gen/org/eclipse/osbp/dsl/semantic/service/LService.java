/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation
 *  
 */
package org.eclipse.osbp.dsl.semantic.service;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.dsl.semantic.common.types.LClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LService</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LService#getInjectedServices <em>Injected Services</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LService#getOperations <em>Operations</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLService()
 * @generated
 */
public interface LService extends LClass {
	/**
	 * Returns the value of the '<em><b>Injected Services</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Injected Services</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Injected Services</em>' containment reference.
	 * @see #setInjectedServices(LInjectedServices)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLService_InjectedServices()
	 * @generated
	 */
	LInjectedServices getInjectedServices();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LService#getInjectedServices <em>Injected Services</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Injected Services</em>' containment reference.
	 * @see #getInjectedServices()
	 * @generated
	 */
	void setInjectedServices(LInjectedServices value);

	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.dsl.semantic.service.LServiceOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLService_Operations()
	 * @generated
	 */
	EList<LServiceOperation> getOperations();

} // LService
