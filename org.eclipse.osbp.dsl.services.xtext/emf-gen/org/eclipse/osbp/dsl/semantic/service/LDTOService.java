/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation
 *  
 */
package org.eclipse.osbp.dsl.semantic.service;

import org.eclipse.osbp.dsl.semantic.dto.LDto;
import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LDTO Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getDto <em>Dto</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getDtoJvm <em>Dto Jvm</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#isMutablePersistenceId <em>Mutable Persistence Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getPersistenceId <em>Persistence Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getFilterable <em>Filterable</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getSortable <em>Sortable</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLDTOService()
 * @generated
 */
public interface LDTOService extends LService {
	/**
	 * Returns the value of the '<em><b>Dto</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dto</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dto</em>' reference.
	 * @see #setDto(LDto)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLDTOService_Dto()
	 * @generated
	 */
	LDto getDto();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getDto <em>Dto</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dto</em>' reference.
	 * @see #getDto()
	 * @generated
	 */
	void setDto(LDto value);

	/**
	 * Returns the value of the '<em><b>Dto Jvm</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dto Jvm</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dto Jvm</em>' containment reference.
	 * @see #setDtoJvm(JvmTypeReference)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLDTOService_DtoJvm()
	 * @generated
	 */
	JvmTypeReference getDtoJvm();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getDtoJvm <em>Dto Jvm</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dto Jvm</em>' containment reference.
	 * @see #getDtoJvm()
	 * @generated
	 */
	void setDtoJvm(JvmTypeReference value);

	/**
	 * Returns the value of the '<em><b>Mutable Persistence Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mutable Persistence Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mutable Persistence Id</em>' attribute.
	 * @see #setMutablePersistenceId(boolean)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLDTOService_MutablePersistenceId()
	 * @generated
	 */
	boolean isMutablePersistenceId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#isMutablePersistenceId <em>Mutable Persistence Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mutable Persistence Id</em>' attribute.
	 * @see #isMutablePersistenceId()
	 * @generated
	 */
	void setMutablePersistenceId(boolean value);

	/**
	 * Returns the value of the '<em><b>Persistence Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Persistence Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persistence Id</em>' attribute.
	 * @see #setPersistenceId(String)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLDTOService_PersistenceId()
	 * @generated
	 */
	String getPersistenceId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getPersistenceId <em>Persistence Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Persistence Id</em>' attribute.
	 * @see #getPersistenceId()
	 * @generated
	 */
	void setPersistenceId(String value);

	/**
	 * Returns the value of the '<em><b>Filterable</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.dsl.semantic.service.LFilterableAttributes#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filterable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filterable</em>' containment reference.
	 * @see #setFilterable(LFilterableAttributes)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLDTOService_Filterable()
	 * @see org.eclipse.osbp.dsl.semantic.service.LFilterableAttributes#getParent
	 * @generated
	 */
	LFilterableAttributes getFilterable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getFilterable <em>Filterable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filterable</em>' containment reference.
	 * @see #getFilterable()
	 * @generated
	 */
	void setFilterable(LFilterableAttributes value);

	/**
	 * Returns the value of the '<em><b>Sortable</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.dsl.semantic.service.LSortableAttributes#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sortable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sortable</em>' containment reference.
	 * @see #setSortable(LSortableAttributes)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLDTOService_Sortable()
	 * @see org.eclipse.osbp.dsl.semantic.service.LSortableAttributes#getParent
	 * @generated
	 */
	LSortableAttributes getSortable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LDTOService#getSortable <em>Sortable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sortable</em>' containment reference.
	 * @see #getSortable()
	 * @generated
	 */
	void setSortable(LSortableAttributes value);

} // LDTOService
