/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation
 *  
 */
package org.eclipse.osbp.dsl.semantic.service;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage
 * @generated
 */
public interface OSBPServiceFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OSBPServiceFactory eINSTANCE = org.eclipse.osbp.dsl.semantic.service.impl.OSBPServiceFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>LService Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LService Model</em>'.
	 * @generated
	 */
	LServiceModel createLServiceModel();

	/**
	 * Returns a new object of class '<em>LInjected Services</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LInjected Services</em>'.
	 * @generated
	 */
	LInjectedServices createLInjectedServices();

	/**
	 * Returns a new object of class '<em>LInjected Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LInjected Service</em>'.
	 * @generated
	 */
	LInjectedService createLInjectedService();

	/**
	 * Returns a new object of class '<em>LDTO Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LDTO Service</em>'.
	 * @generated
	 */
	LDTOService createLDTOService();

	/**
	 * Returns a new object of class '<em>LFilterable Attributes</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LFilterable Attributes</em>'.
	 * @generated
	 */
	LFilterableAttributes createLFilterableAttributes();

	/**
	 * Returns a new object of class '<em>LSortable Attributes</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LSortable Attributes</em>'.
	 * @generated
	 */
	LSortableAttributes createLSortableAttributes();

	/**
	 * Returns a new object of class '<em>LChart Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LChart Service</em>'.
	 * @generated
	 */
	LChartService createLChartService();

	/**
	 * Returns a new object of class '<em>LFree Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LFree Service</em>'.
	 * @generated
	 */
	LFreeService createLFreeService();

	/**
	 * Returns a new object of class '<em>LService Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LService Operation</em>'.
	 * @generated
	 */
	LServiceOperation createLServiceOperation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OSBPServicePackage getOSBPServicePackage();

} //OSBPServiceFactory
