/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation
 *  
 */
package org.eclipse.osbp.dsl.semantic.service.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.osbp.dsl.semantic.service.LChartService;
import org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LChart Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LChartServiceImpl extends LServiceImpl implements LChartService {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LChartServiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OSBPServicePackage.Literals.LCHART_SERVICE;
	}

} //LChartServiceImpl
