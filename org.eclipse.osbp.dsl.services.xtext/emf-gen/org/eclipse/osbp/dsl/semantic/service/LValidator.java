/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation
 *  
 */
package org.eclipse.osbp.dsl.semantic.service;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.xtext.common.types.JvmTypeReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LValidator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LValidator#getValidatorId <em>Validator Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.dsl.semantic.service.LValidator#getValidatorType <em>Validator Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLValidator()
 * @generated
 */
public interface LValidator extends LLazyResolver {
	/**
	 * Returns the value of the '<em><b>Validator Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validator Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validator Id</em>' attribute.
	 * @see #setValidatorId(String)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLValidator_ValidatorId()
	 * @generated
	 */
	String getValidatorId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LValidator#getValidatorId <em>Validator Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validator Id</em>' attribute.
	 * @see #getValidatorId()
	 * @generated
	 */
	void setValidatorId(String value);

	/**
	 * Returns the value of the '<em><b>Validator Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validator Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validator Type</em>' containment reference.
	 * @see #setValidatorType(JvmTypeReference)
	 * @see org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage#getLValidator_ValidatorType()
	 * @generated
	 */
	JvmTypeReference getValidatorType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.dsl.semantic.service.LValidator#getValidatorType <em>Validator Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validator Type</em>' containment reference.
	 * @see #getValidatorType()
	 * @generated
	 */
	void setValidatorType(JvmTypeReference value);

} // LValidator
