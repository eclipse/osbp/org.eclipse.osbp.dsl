/**
 * Copyright (c) 2011, 2016 - Lunifera GmbH (Wien), Loetz GmbH&Co.KG (Heidelberg)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		Florian Pirchner - Initial implementation
 *  
 */
package org.eclipse.osbp.dsl.semantic.service.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.osbp.dsl.semantic.service.LCardinality;
import org.eclipse.osbp.dsl.semantic.service.LChartService;
import org.eclipse.osbp.dsl.semantic.service.LDTOService;
import org.eclipse.osbp.dsl.semantic.service.LFilterableAttributes;
import org.eclipse.osbp.dsl.semantic.service.LFreeService;
import org.eclipse.osbp.dsl.semantic.service.LInjectedService;
import org.eclipse.osbp.dsl.semantic.service.LInjectedServices;
import org.eclipse.osbp.dsl.semantic.service.LServiceModel;
import org.eclipse.osbp.dsl.semantic.service.LServiceOperation;
import org.eclipse.osbp.dsl.semantic.service.LSortableAttributes;
import org.eclipse.osbp.dsl.semantic.service.OSBPServiceFactory;
import org.eclipse.osbp.dsl.semantic.service.OSBPServicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OSBPServiceFactoryImpl extends EFactoryImpl implements OSBPServiceFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OSBPServiceFactory init() {
		try {
			OSBPServiceFactory theOSBPServiceFactory = (OSBPServiceFactory)EPackage.Registry.INSTANCE.getEFactory(OSBPServicePackage.eNS_URI);
			if (theOSBPServiceFactory != null) {
				return theOSBPServiceFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OSBPServiceFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPServiceFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OSBPServicePackage.LSERVICE_MODEL: return createLServiceModel();
			case OSBPServicePackage.LINJECTED_SERVICES: return createLInjectedServices();
			case OSBPServicePackage.LINJECTED_SERVICE: return createLInjectedService();
			case OSBPServicePackage.LDTO_SERVICE: return createLDTOService();
			case OSBPServicePackage.LFILTERABLE_ATTRIBUTES: return createLFilterableAttributes();
			case OSBPServicePackage.LSORTABLE_ATTRIBUTES: return createLSortableAttributes();
			case OSBPServicePackage.LCHART_SERVICE: return createLChartService();
			case OSBPServicePackage.LFREE_SERVICE: return createLFreeService();
			case OSBPServicePackage.LSERVICE_OPERATION: return createLServiceOperation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OSBPServicePackage.LCARDINALITY:
				return createLCardinalityFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OSBPServicePackage.LCARDINALITY:
				return convertLCardinalityToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LServiceModel createLServiceModel() {
		LServiceModelImpl lServiceModel = new LServiceModelImpl();
		return lServiceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LInjectedServices createLInjectedServices() {
		LInjectedServicesImpl lInjectedServices = new LInjectedServicesImpl();
		return lInjectedServices;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LInjectedService createLInjectedService() {
		LInjectedServiceImpl lInjectedService = new LInjectedServiceImpl();
		return lInjectedService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDTOService createLDTOService() {
		LDTOServiceImpl ldtoService = new LDTOServiceImpl();
		return ldtoService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LFilterableAttributes createLFilterableAttributes() {
		LFilterableAttributesImpl lFilterableAttributes = new LFilterableAttributesImpl();
		return lFilterableAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LSortableAttributes createLSortableAttributes() {
		LSortableAttributesImpl lSortableAttributes = new LSortableAttributesImpl();
		return lSortableAttributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LChartService createLChartService() {
		LChartServiceImpl lChartService = new LChartServiceImpl();
		return lChartService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LFreeService createLFreeService() {
		LFreeServiceImpl lFreeService = new LFreeServiceImpl();
		return lFreeService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LServiceOperation createLServiceOperation() {
		LServiceOperationImpl lServiceOperation = new LServiceOperationImpl();
		return lServiceOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LCardinality createLCardinalityFromString(EDataType eDataType, String initialValue) {
		LCardinality result = LCardinality.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLCardinalityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSBPServicePackage getOSBPServicePackage() {
		return (OSBPServicePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OSBPServicePackage getPackage() {
		return OSBPServicePackage.eINSTANCE;
	}

} //OSBPServiceFactoryImpl
