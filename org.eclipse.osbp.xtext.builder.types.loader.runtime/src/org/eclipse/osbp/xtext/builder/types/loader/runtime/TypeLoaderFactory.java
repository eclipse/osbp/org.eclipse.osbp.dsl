/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.builder.types.loader.runtime;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpace;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoader;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoaderFactory;

import com.google.inject.Singleton;

@SuppressWarnings("restriction")
@Singleton
public class TypeLoaderFactory implements ITypeLoaderFactory {

	public ITypeLoader createTypeLoader(ResourceSet resourceSet) {
		if (resourceSet == null)
			throw new IllegalArgumentException("resourceSet may not be null.");

		XtextResourceSet xtextRS = (XtextResourceSet) resourceSet;
		Object context = xtextRS.getClasspathURIContext();
		if (!(context instanceof BundleSpace)) {
			throw new IllegalArgumentException(
					"No bundlespace available to load classes.");
		}

		return new TypeLoader((BundleSpace) context);
	}

}
