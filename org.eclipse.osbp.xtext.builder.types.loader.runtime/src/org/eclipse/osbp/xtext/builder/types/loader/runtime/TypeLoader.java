/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.builder.types.loader.runtime;

import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpace;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("restriction")
public class TypeLoader implements ITypeLoader {

	private static final Logger logger = LoggerFactory
			.getLogger(TypeLoader.class);

	private final BundleSpace bundleSpace;

	/**
	 * @noreference This constructor is not intended to be referenced by
	 *              clients.
	 */
	public TypeLoader(BundleSpace bundleSpace) {
		if (bundleSpace == null)
			throw new IllegalArgumentException("bundleSpace must not be null");
		this.bundleSpace = bundleSpace;
	}

	public Class<?> findTypeByName(String fullyQualifiedName) {
		if (fullyQualifiedName == null || fullyQualifiedName.equals("")) {
			return null;
		}

		try {
			return bundleSpace.forName(fullyQualifiedName);
		} catch (ClassNotFoundException e) {
			logger.error("{}", e);
		}
		return null;
	}

	@Override
	public void dispose() {
	}
}
